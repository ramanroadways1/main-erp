<?php 
require_once './connection.php';

$date = date('Y-m-d');
$timestamp = date('Y-m-d H:i:s');

$vou_date =  escapeString($conn,strtoupper($_POST['vou_date']));        
$company =  escapeString($conn,strtoupper($_POST['company']));        
$tno =  escapeString($conn,strtoupper($_POST['tno']));        
$payment_mode =  escapeString($conn,strtoupper($_POST['payment_mode']));        
$driver_name =  escapeString($conn,strtoupper($_POST['driver_name']));        
$lrno =  escapeString($conn,strtoupper($_POST['lrno']));        
$amount =  escapeString($conn,strtoupper($_POST['amount']));
$amount_word =  escapeString($conn,strtoupper($_POST['amount_word']));
$cheque_no =  escapeString($conn,strtoupper($_POST['cheque_no']));
$bank =  escapeString($conn,strtoupper($_POST['bank']));        
$pan =  escapeString($conn,strtoupper($_POST['pan']));        
$acname =  escapeString($conn,strtoupper($_POST['acname']));        
$acno =  escapeString($conn,strtoupper($_POST['acno']));        
$bank_name =  escapeString($conn,strtoupper($_POST['bank_name']));        
$ifsc =  escapeString($conn,strtoupper($_POST['ifsc']));        
$narration =  escapeString($conn,strtoupper($_POST['narration']));        
$amount_narration =  escapeString($conn,strtoupper($_POST['amount_narration']));        
$other_narration =  escapeString($conn,strtoupper($_POST['other_narration']));        
$signature =  escapeString($conn,($_POST['signature']));
$signature2 =  escapeString($conn,($_POST['signature2']));

if($signature2=='')
{
	echo "<script type='text/javascript'>
		alert('Please upload signature.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

$img1=$signature;
$img1=str_replace('data:image/png;base64,', '', $img1);
$img1=str_replace(' ', '+', $img1);
$data1=base64_decode($img1);

$img2=$signature2;
$img2=str_replace('data:image/png;base64,', '', $img2);
$img2=str_replace(' ', '+', $img2);
$data2=base64_decode($img2);

		$user2=substr($branch,0,3)."T".date("dmY");  
        $ramanc=Qry($conn,"SELECT DISTINCT tdvid FROM mk_tdv where tdvid like '$user2%' ORDER BY id DESC LIMIT 1");
		if(!$ramanc){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
			Redirect("Error while processing Request","./");
			exit();
		}
		
        $row2 = fetchArray($ramanc);
        if(numRows($ramanc) > 0)
		{
			$count = $row2['tdvid'];
			$newstring = substr($count,12);
			$newstr = ++$newstring;
			$vou_id=$user2.$newstr;
		}
		else
		{
			$count2=1;
			$vou_id=$user2.$count2;
		}

$chk_vou_no = Qry($conn,"SELECT id FROM mk_tdv WHERE tdvid='$vou_id'");
if(!$chk_vou_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_vou_no)>0)
{
	echo "<script>
		alert('Duplicate Voucher No: $vou_id. Try again !');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

	$check_bal=Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");
	if(!$check_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash=$row_bal['balance'];
	$rr_cash=$row_bal['balance2'];
	$email=$row_bal['email'];

if($payment_mode=='CASH')
{
	if($company=='RRPL' && $rrpl_cash>=$amount)
	{
		$new_amount = $rrpl_cash-$amount;
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
	{
		$new_amount = $rr_cash-$amount;
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Insufficient balance in company : $company.');
			window.location.href='./ftruck.php';
		</script>";
		exit();
	}
}
	
StartCommit($conn);
$flag = true;
$rtgs_flag = false;
	
if($payment_mode=='CASH')
{
	$update_balance = Qry($conn,"update user set `$balance_col`='$new_amount' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$branch','$date','$vou_date','$company','$vou_id','Truck_Voucher','TruckNo: $tno, LRNo: $lrno.','$amount','$new_amount','$timestamp')");

	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='CHEQUE')
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit_col`,timestamp) VALUES 
	('$branch','$vou_id','$date','$vou_date','$company','Truck_Voucher','TruckNo: $tno, LRNo: $lrno.','$cheque_no','$amount','$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$insert_cheque_book=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) VALUES 
('$vou_id','Truck_Voucher','$vou_date','$amount','$cheque_no','$date','$branch','$timestamp')");

	if(!$insert_cheque_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='NEFT')
{
	$chk_neft = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$vou_id'");
	if(!$chk_neft){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-T",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	else
	{
		$get_Crn = GetCRN("RR-T",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	
if(numRows($chk_neft)==0)		
{
	$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,tno,type,email,crn,timestamp) 
		VALUES ('$vou_id','$branch','$company','$amount','$amount','$acname','$acno','$bank_name','$ifsc','$pan','$date','$vou_date',
		'$tno','TRUCK_ADVANCE','$email','$crnnew','$timestamp')");			

	if(!$qry_rtgs){
		$flag = false;
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Duplicate Rtgs Voucher : $vou_id",$conn,$page_name,__LINE__);
}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode selected.",$conn,$page_name,__LINE__);
}

file_put_contents('./sign/truck_vou/cashier/'.$vou_id.'.png',$data1);
file_put_contents('./sign/truck_vou/rcvr/'.$vou_id.'.png',$data2);
$cash_sign="sign/truck_vou/cashier/$vou_id.png";
$rcvr_sign="sign/truck_vou/rcvr/$vou_id.png";	
		
$insert_voucher = Qry($conn,"INSERT INTO mk_tdv(user,branch_user,company,tdvid,date,newdate,truckno,dname,amt,amtw,mode,chq_no,chq_bank,ac_name,ac_no,
bank,ifsc,pan,dest,nar,naro,cash_sign,rcvr_sign,timestamp) VALUES ('$branch','$_SESSION[user_code]','$company','$vou_id','$date','$vou_date','$tno',
'$driver_name','$amount','$amount_word','$payment_mode','$cheque_no','$bank','$acname','$acno','$bank_name','$ifsc','$pan','$narration',
'$amount_narration','$other_narration','$cash_sign','$rcvr_sign','$timestamp')");

if(!$insert_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$TodayData = Qry($conn,"UPDATE today_data SET truck_vou=truck_vou+1,truck_vou_amount=truck_vou_amount+'$amount' WHERE branch='$branch'");

if(!$TodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}


if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Voucher: $vou_id Created Successfully !!');
		window.location.href='./ftruck.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#submit_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
?>