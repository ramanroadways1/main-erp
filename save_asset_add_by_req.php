<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$req_code = escapeString($conn,strtoupper($_POST['token_no']));
$asset_category = escapeString($conn,strtoupper($_POST['d_asset_cat_id'])); // cat id
$maker_name = escapeString($conn,strtoupper($_POST['maker_name']));
$model = escapeString($conn,strtoupper($_POST['model']));
$invoice_date = escapeString($conn,strtoupper($_POST['invoice_date']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$gst_selection = escapeString($conn,strtoupper($_POST['gst_selection']));
$gst_type = escapeString($conn,strtoupper($_POST['gst_type']));
$gst_value = escapeString($conn,strtoupper($_POST['gst_value']));
$gst_amount = escapeString($conn,strtoupper($_POST['gst_amount']));
$total_amount = escapeString($conn,strtoupper($_POST['total_amount']));
$discount = escapeString($conn,strtoupper($_POST['discount']));
$asset_for = escapeString($conn,strtoupper($_POST['asset_for']));

$chk_payment_amount = Qry($conn,"SELECT payment_amount,vou_no FROM asset_request WHERE req_code='$req_code'");
if(!$chk_payment_amount){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./asset_vehicle_view.php");
	exit();
}

if(numRows($chk_payment_amount)==0){
	echo "<script>
		alert('Asset request not found !');
		window.location.href='./assets_view.php';
	</script>";
	exit();
}

$row_amount = fetchArray($chk_payment_amount);
$vou_no = $row_amount['vou_no'];

if($gst_selection=='YES'){
	$gst_amountDb = sprintf("%.2f",($amount*$gst_value/100));
}
else{
	$gst_amountDb = 0;
}

if($gst_amountDb!=$gst_amount)
{
	echo "<script>
			alert('Error : Please Check GST Amount !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($total_amount!=round($amount+$gst_amountDb-$discount))
{
	echo "<script>
			alert('Error : Please Check Total Amount !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
}


if($row_amount['payment_amount']!=$total_amount)
{
	echo "<script>
			alert('Error : amount not verified !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
}

// if($row_amount['payment_amount']<=0)
// {
	// echo "<script>
			// alert('Error : Invalid payment amount !');
			// $('#btn_asset_add').attr('disabled', false);
			// $('#loadicon').hide();
		// </script>";
	// exit();
// }

if($asset_for=="SPECIFIC"){
	$employee = escapeString($conn,strtoupper($_POST['employee']));
}
else{
	$employee = $branch;
}

if(sprintf("%.2f",$row_amount['payment_amount'])!=sprintf("%.2f",$total_amount))
{
	echo "<script>
			alert('Error : Payment amount not verified !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png","application/pdf");
	
	if(!in_array($_FILES['invoice_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image and PDF Upload Allowed.');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide(); 
		</script>";
		exit();
	}

$sourcePath = $_FILES['invoice_copy']['tmp_name'];

$file_name_1 = date('dmYHis').mt_rand();
$targetPath = "asset_upload/asset_invoice/".$file_name_1.".".pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION);
$targetPath2 = "upload/".$file_name_1.".".pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION)!="pdf")
{
	ImageUpload(1000,700,$sourcePath);
}
		
StartCommit($conn);
$flag = true;
	
$insert_Asset = Qry($conn,"INSERT INTO asset_main(req_code,category,asset_company,asset_model,invoice_date,typeof,holder,branch,
branch_user,invoice,amount,gst_invoice,gst_type,gst_value,gst_amount,discount,total_amount,add_type,timestamp) VALUES ('$req_code','$asset_category',
'$maker_name','$model','$invoice_date','$asset_for','$employee','$branch','$branch_sub_user','$targetPath','$amount','$gst_selection',
'$gst_type','$gst_value','$gst_amount','$discount','$total_amount','1','$timestamp')");

if(!$insert_Asset){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_added = Qry($conn,"UPDATE asset_request SET asset_added='1' WHERE req_code='$req_code'");

if(!$update_added){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_vou_invoice_file = Qry($conn,"UPDATE mk_venf SET upload='$targetPath2' WHERE vno='$vou_no'");

if(!$update_vou_invoice_file){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($sourcePath, $targetPath)) 
{
	$flag = false;
	errorLog("File upload failed.1",$conn,$page_name,__LINE__);
}

if(!copy($targetPath,$targetPath2)) 
{
	$flag = false;
	errorLog("File upload failed.2",$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
			alert('Asset Added Successfully !');
			window.location.href='assets_view.php';
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	unlink($targetPath);
	unlink($targetPath2);
	
	echo "<script>
			alert('Error : Error While Processing Request !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
	
	// Redirect("Error While Processing Request.","./assets_view.php");
	// exit();
}
?>