<?php
require_once("./connection.php");

$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$type = escapeString($conn,strtoupper($_POST['type']));

if($type=='BAL'){
	$input_id = "claim";
}
else if($type=='ADV'){
	$input_id = "claim_amount";
}
else{
	Redirect("Error while processing Request","./");
	exit();
}

$get_cache = Qry($conn,"SELECT id,lrno,vou_no,claim_vou_no,amount,timestamp FROM claim_cache WHERE vou_no='$vou_no' AND adv_bal='$type'");

if(!$get_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_cache)==0)
{
	echo "<br><center><font color='red'>Record not added yet..</font></center>";
	echo "<script>
		$('#$input_id').val('0');
	</script>";
}
else
{
	echo "
	<table class='table table-bordered table-striped' style='font-size:12px'>
			<tr>
				<th>Id</th>
				<th>LR_No</th>
				<th>Vou_no</th>
				<th>Amount</th>
				<th>Added_On</th>
				<th>#</th>
			</tr>";
	$sn=1;		
		$claim_amount=0;
		while($row = fetchArray($get_cache))
		{
			$time2 = date("d/m/y h:i A",strtotime($row['timestamp']));
			$claim_amount = $claim_amount+$row['amount'];
			echo "<tr>
				<td>$sn</td>
				<td>$row[lrno]</td>
				<td>$row[claim_vou_no]</td>
				<td>$row[amount]</td>
				<td>$time2</td>
				<td><button onclick='DeleteClaim($row[id])' class='btn btn-danger btn-xs'>delete</button></td>
			</tr>";
		$sn++;	
		}
	echo "</table>";
	
	echo "<script>  
		$('#$input_id').val('$claim_amount');
	</script>";
}

echo "<script>
	SumDefault();
	$('#loadicon').hide();
</script>";
?>