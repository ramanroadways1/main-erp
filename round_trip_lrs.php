<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<?php
include '_header3.php';
include './disable_right_click_for_index.php';
?>

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Round Trip LRs (pending) :</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>Round Trip LR_No</th>
					<th>Under LR_No</th>
					<th>Vehicle_No</th>
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>From <span class="glyphicon glyphicon-filter"></span></th>
					<th>To <span class="glyphicon glyphicon-filter"></span></th>
					<th>##</th>
				</tr> 
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<div id="edjsakld"></div>

<script type="text/javascript">
function FetchData(){
$("#loadicon").show();
var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		// "excel",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{
        targets: 7, // edit column order
        data: null,
        mRender: function(data, type, full){ 
         return '<button type="button" class="btn-xs btn-primary" onclick="CreateLR('+full[1]+')" id="create_btn_'+full[1]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create LR</button>';
        }
      },
	], 
        "serverSide": true,
        "ajax": "get_round_trip_lrs.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchData();
</script>

<script type="text/javascript">
function CreateLR(lrno)
{
	alert(lrno);
	// $('#ewb_id').val(id);
	// $('#ewb_no_html').html(ewbno+" , LR Number: "+lrno);
	// $('#modal_btn')[0].click();
}
</script>

<div id="function_result"></div>