<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<script>
  $(function() {  
      $("#tno_wheeler").autocomplete({
			source: function(request, response) { 
				$.ajax({
                  url: "./autofill/get_tno_wheeler.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
			 
               $('#tno_wheeler').val(ui.item.value); 
			   $('#wheeler_name').val(ui.item.name);  			   
				$('#wheeler_mobile').val(ui.item.mo1);  
				
			   if(ui.item.wheeler=='')
			   {
					$('#wheeler_set').val(""); 
					$('#updateWheelerBtn').attr('disabled',false);
			   }
			   else
			   {
					$('#wheeler_set').val(ui.item.wheeler);  
					$('#updateWheelerBtn').attr('disabled',false);				
			   }	

               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$('#wheeler_name').val("");  			   
				$('#wheeler_mobile').val("");  			   
                $(event.target).focus();
				$('#updateWheelerBtn').attr('disabled',true);
                alert('Vehicle No does not exist !'); 
              } 
              },
			}); 
      }); 
</script> 

<script>
  $(function() {  
      $("#tno_dec").autocomplete({
			source: function(request, response) { 
				$.ajax({
                  url: "./autofill/get_tno_declaration.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
			 
               $('#tno_dec').val(ui.item.value); 
			   $('#dec_name').val(ui.item.name);  			   
				$('#dec_mobile').val(ui.item.mo1);  
				
			   if(ui.item.dec=='')
			   {
					$('#DecAlert').hide(); 
					$('#DecUploadId').attr("disabled",false); 
					$('#updateDecBtn').attr('disabled',false);
			   }
			   else
			   {
				   $('#DecAlert').show(); 
					$('#DecUploadId').attr("disabled",true); 
					$('#updateDecBtn').attr('disabled',true);				
			   }	

               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$('#dec_name').val("");  			   
				$('#dec_mobile').val("");  			   
                $(event.target).focus();
				$('#updateDecBtn').attr('disabled',true);
                alert('Vehicle No does not exist !'); 
              } 
              },
			}); 
      }); 


  $(function() {  
      $("#tno_mobile_update").autocomplete({
			source: function(request, response) { 
				$.ajax({
                  url: "./autofill/get_tno_all_func.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
			 
               $('#tno_mobile_update').val(ui.item.value);  			   
               $('#update_mobile_owner_name').val(ui.item.name);  			   
				$('#update_owner_mobile').val(ui.item.mo1);  
			  $('#owner_mobile_id').val(ui.item.oid);  
			  
			  if(ui.item.branch_update=='0'){
					$('#MobileAlertOwner').hide(); 
					$('#update_owner_mobile').attr("readonly",false); 
					$('#updateOwnerMobileBtn').attr('disabled',false);
			   }
			   else{
					$('#MobileAlertOwner').show(); 
					$('#update_owner_mobile').attr("readonly",true); 
					$('#updateOwnerMobileBtn').attr('disabled',true);				
				}
			   
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$('#tno_mobile_update').val("");  			   
				$('#update_mobile_owner_name').val("");  			   
				$('#update_owner_mobile').val("");  			   
				$('#owner_mobile_id').val("");  			   
                $(event.target).focus();
				$('#updateOwnerMobileBtn').attr('disabled',true);
                alert('Vehicle No does not exist !'); 
              } 
              },
			}); 
      }); 	  
	
  $(function() {  
      $("#broker_name_mobile_update").autocomplete({
			source: function(request, response) { 
				$.ajax({
                  url: "./autofill/get_broker_all_func.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
			 
               $('#broker_name_mobile_update').val(ui.item.value);  			   
               $('#update_mobile_broker_pan').val(ui.item.pan);  			   
				$('#update_broker_mobile').val(ui.item.mo1);  
			  $('#broker_mobile_id').val(ui.item.bid);  
			  
			   if(ui.item.branch_update=='0'){
					$('#MobileAlertBroker').hide(); 
					$('#update_broker_mobile').attr("readonly",false); 
					$('#updateBrokerMobileBtn').attr('disabled',false);
			   }
			   else{
					$('#MobileAlertBroker').show(); 
					$('#update_broker_mobile').attr("readonly",true); 
					$('#updateBrokerMobileBtn').attr('disabled',true);				
				}
				
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$('#broker_name_mobile_update').val("");  			   
				$('#update_mobile_broker_pan').val("");  			   
				$('#update_broker_mobile').val("");  			   
				$('#broker_mobile_id').val("");  			   
                $(event.target).focus();
				$('#updateBrokerMobileBtn').attr('disabled',true);
                alert('Broker does not exist !'); 
              } 
              },
			});  
      });	
</script> 

<style>
label{
	color:#000;
	font-size:12px !important;
}

input,select,textarea,.selectpicker{
	border:1px solid gray !important;
	font-size:12px !important;
	text-transform:uppercase;
}

.btn-default{
	border:1px solid #000 !important;
}

.selectpicker { width:auto; font-size: 12px !important;}
</style>

<link href="css/styles.css" rel="stylesheet">

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important"  onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">

<br />

	<div class="row">
		<div class="form-group col-md-12">
			<h3 style="color:#FFF">All Functions : </h3>
		</div>
	</div>

<br />

		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#AddBrokerModal">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/broker1.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px; padding-top:6px;">ADD New Broker</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#AddTruckModal">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/truck_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">ADD New Truck</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#AddDriverModal">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/driver.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">ADD New Driver</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#ModalWheeler">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/wheeler.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">Update Wheeler</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#ModalConsignor">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/party.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">ADD Consignor</div>
						</div>
					</div>
				</div>
			</div>	
			
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#ModalConsignee">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/party.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">ADD Consignee</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#DecModal">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/paper.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">Update Declaration</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#OwnerChangeModal">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:80%" src="svg/owner_change.png">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">Owner Change Req.</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#ModalUpdateMobile">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/mobile_phone.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">Update Owner Mob.
							<br><span style="color:red;font-size:9px">(one time only)</span></div>
						</div>
					</div>
				</div>
			</div>
		
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#ModalUpdateMobileBroker">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/mobile_phone.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">Update Broker Mob.
							<br><span style="color:red;font-size:9px">(one time only)</span></div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#AddOwnTruckDriverModal">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/driver.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:12px;padding-top:6px;">ADD Own Truck<br>Driver</div>
						</div>
					</div>
				</div>
			</div>	
			
    </div>

</div>
</div>

<?php
include("./modal_add_broker.php");
include("./modal_add_driver.php");
include("./modal_add_own_truck_driver.php");
include("./modal_add_truck.php");
include("./modal_update_wheeler.php");
include("./modal_add_consignor.php");
include("./modal_add_consignee.php");
include("./modal_update_declaration.php");
include("./modal_update_owner_mobile.php");
include("./modal_update_broker_mobile.php");
include("./modal_owner_change.php");

include("./_footer.php");
?>