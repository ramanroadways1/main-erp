<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

	$amount = strtoupper(escapeString($conn,$_POST['amount']));
	$company = strtoupper(escapeString($conn,$_POST['company2']));
	$confirm = strtoupper(escapeString($conn,$_POST['confirm_by_db']));
	$otp = strtoupper(escapeString($conn,$_POST['otp']));
	$narration = strtoupper(escapeString($conn,$_POST['narration']));
	
	$narration = "Confirmed By- ".$confirm.", ".$narration;
	
if($confirm==""){
	Redirect("Confirmation not found.","./");
	exit();
}
	
$qry_fetch=Qry($conn,"SELECT mobile,name FROM ho_confirm WHERE username='$confirm'");

if(!$qry_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
				
if(numRows($qry_fetch)>0)
{
	$row_fetch = fetchArray($qry_fetch);
	$mobile=$row_fetch['mobile'];
}
else
{
	echo "<script>
		alert('No approval authority found.');
		window.location.href='./credit.php';
	</script>";
	exit();
}
	
	if($company=='')
	{
		echo "<script>
			alert('Unable to fetch Company.');
			window.location.href='./credit.php';
		</script>";
		exit();	
	}
	
// For Verify otp

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => "$mobile",
    // 'otp' => "$otp"
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// window.location.href='./credit.php';
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#cr_ho_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#cr_ho_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"cr_ho_button");

	// if($otp=='')
	// {
		// echo "<script>
				// alert('Error : Please Enter OTP.');
				// $('#cr_ho_button').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
		// exit();	
	// }
	// else
	// {
		// if($otp!=$_SESSION['session_otp'])
		// {
			// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#cr_ho_button').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
			// exit();	
		// }
	// }
	
StartCommit($conn);
$flag = true;
	
	$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	if(!$fetch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
     $row_bal = fetchArray($fetch_bal);
     $rrpl_cash = $row_bal['balance'];
     $rr_cash = $row_bal['balance2'];
		
	if($company=='RRPL') 
	{
		$credit_col="credit";
		$balance_col="balance";
		$newbal =  $amount + $rrpl_cash;
	}	
	else
	{
		$credit_col="credit2";
		$balance_col="balance2";
		$newbal =  $amount + $rr_cash;
	}
	
	$update_balance = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook = Qry($conn,"INSERT INTO cashbook(user_code,vou_type,`$credit_col`,desct,user,`$balance_col`,comp,date,timestamp) VALUES 
		('$_SESSION[user_code]','CREDIT-HO','$amount','$narration','$branch','$newbal','$company','$date','$timestamp')");
		
	if(!$insert_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$cash_id = getInsertID($conn);
	
$insert_credit = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
('CASH','$cash_id','HO','$branch','$_SESSION[user_code]','$company','$amount','$narration','$date','$timestamp')");		
	
	if(!$insert_credit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('HO Credited Successfully !!');
		window.location.href='./credit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit.php");
	exit();
}
?>