<?php
require_once("./connection.php");

$dest_loc = escapeString($conn,strtoupper($_POST['dest_loc']));
$dest_loc_id = escapeString($conn,strtoupper($_POST['dest_loc_id']));
$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$id = escapeString($conn,strtoupper($_POST['table_id']));
$page_name1 = escapeString($conn,($_POST['page_name1']));

$update = Qry($conn,"UPDATE lr_pre SET tstation='$dest_loc',to_id='$dest_loc_id' WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Destination Updated to : $dest_loc. LR Number : $lrno');
	window.location.href='./$page_name1';
</script>";
exit();
?>