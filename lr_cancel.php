<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CancelLRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#button1").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_cancel_lr.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#function_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div id="function_result"></div>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" id="CancelLRForm" method="POST">	
	
<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">LR - Cancel</h4></center>
	</div>

	<div class="form-group col-md-12">
		<label>LR Number <font color="red">*</font></label>
		<input name="lrno" type="text" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'');" required="required" />
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" 
		name="submit" value="Confirm Cancel" />
	</div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>