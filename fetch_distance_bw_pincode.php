<?php
require_once("./connection.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

echo "<script>
	$('#ext_val_ewb_btn_save').attr('disabled',true);
	$('#ewb_extd_verify_btn').attr('disabled',true);
	$('#loadicon_ewb_val').show();
</script>";

$dest_pincode = $_SESSION['modal_ewb_ext_dest_pincode'];
$pincode = escapeString($conn,($_POST['pincode']));

if(strlen($dest_pincode)!=6)
{
	echo "<span style='color:red;font-size:13px'>Destination pincode not found !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#ewb_extd_verify_btn').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($pincode)!=6)
{
	echo "<span style='color:red;font-size:13px'>Please enter valid pincode !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#ewb_extd_verify_btn').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$chk_pincode = Qry($conn,"SELECT distance,travel_hr,travel_min,travel_sec FROM distance_master_pincode 
WHERE from_pincode='$pincode' AND to_pincode='$dest_pincode'");

if(!$chk_pincode){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error !');$('#ewb_extd_verify_btn').attr('disabled',false);$('#ext_val_ewb_btn_save').attr('disabled',true);$('#loadicon_ewb_val').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_pincode)>0)
{
	$row_distance = fetchArray($chk_pincode);
	
	// if($row_distance['distance']<100){
		// $distance_remain = 100;
	// }
	// else{
		$distance_remain = round($row_distance['distance'] + ($row_distance['distance']*8/100));
	// }
	
	if($distance_remain==0)
	{
		$distance_remain=100;
	}
	
	echo "<span style='color:maroon;font-size:13px'>Distance remaining: $row_distance[distance] KMs. Travel time: $row_distance[travel_hr] hr $row_distance[travel_min] min</span>";
	echo "<script>
		$('#modal_ewb_ext_curr_loc_pincode').attr('readonly',true);
		$('#modal_ewb_ext_distance_remaining').attr('readonly',false);
		$('#modal_ewb_ext_distance_remaining').attr('min','$distance_remain');
		$('#modal_ewb_ext_distance_remaining').attr('max','$distance_remain');
		$('#modal_ewb_ext_distance_remaining').val('$distance_remain');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#ewb_extd_verify_btn').attr('disabled',true);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$origin = $pincode;
$destination = $dest_pincode;

$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=".$google_api_key."";
$api = file_get_contents($url);
$data = json_decode($api);
			
$api_status = $data->rows[0]->elements[0]->status;
	
if($api_status=='NOT_FOUND' AND $pincode==$dest_pincode)
{
	$distance_remain = 100;
	// echo "<span style='color:maroon;font-size:13px'>Distance remaining: 100 KMs.</span>";
	echo "<script>
		$('#modal_ewb_ext_curr_loc_pincode').attr('readonly',true);
		$('#modal_ewb_ext_distance_remaining').attr('readonly',false);
		$('#modal_ewb_ext_distance_remaining').val('$distance_remain');
		$('#modal_ewb_ext_distance_remaining').attr('min','$distance_remain');
		$('#modal_ewb_ext_distance_remaining').attr('max','$distance_remain');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#ewb_extd_verify_btn').attr('disabled',true); 
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if($api_status=='NOT_FOUND') 
{
	// $distance_remain = 100;
	// echo "<span style='color:maroon;font-size:13px'>Distance remaining: 100 KMs.</span>";
	echo "<script>
		$('#modal_ewb_ext_curr_loc_pincode').attr('readonly',true);
		$('#modal_ewb_ext_distance_remaining').attr('readonly',false);
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#ewb_extd_verify_btn').attr('disabled',true);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if($api_status!='OK')
{
	echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#ewb_extd_verify_btn').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}
			
$dest_addr = $data->destination_addresses[0];
$origin_addr = $data->origin_addresses[0];
$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
$travel_time = $data->rows[0]->elements[0]->duration->text;
$travel_time_value = $data->rows[0]->elements[0]->duration->value;
$travel_hrs = gmdate("H", $travel_time_value);
$travel_minutes = gmdate("i", $travel_time_value);
$travel_seconds = gmdate("s", $travel_time_value);

$insert_pincode_distance = Qry($conn,"INSERT INTO distance_master_pincode(from_pincode,from_addr,to_pincode,to_addr,distance,
travel_hr,travel_min,travel_sec,branch,branch_user,timestamp) VALUES ('$pincode','$origin_addr','$dest_pincode','$dest_addr','$distance',
'$travel_hrs','$travel_minutes','$travel_seconds','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_pincode_distance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error !');$('#ewb_extd_verify_btn').attr('disabled',false);$('#ext_val_ewb_btn_save').attr('disabled',true);$('#loadicon_ewb_val').fadeOut('slow');</script>";
	exit();
}

// if($distance<100){
	// $distance_remain = 100;
// }
// else{
	$distance_remain = round($distance + ($distance*8/100));
// }

$distance_remain_max_value = $distance_remain+30;

if($distance_remain==0)
	{
		$distance_remain=100;
	}
	
echo "<span style='color:maroon;font-size:13px'>Distance remaining: $distance KMs. Travel time: $travel_hrs hr $travel_minutes min</span>";
	echo "<script>
		$('#modal_ewb_ext_curr_loc_pincode').attr('readonly',true);
		$('#modal_ewb_ext_distance_remaining').attr('readonly',false);
		$('#modal_ewb_ext_distance_remaining').val('$distance_remain');
		$('#modal_ewb_ext_distance_remaining').attr('min','$distance_remain');
		$('#modal_ewb_ext_distance_remaining').attr('max','$distance_remain_max_value');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#ewb_extd_verify_btn').attr('disabled',true);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
?>