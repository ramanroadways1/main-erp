<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$reg_no = trim(escapeString($conn,strtoupper($_POST['reg_no'])));
$reg_date = escapeString($conn,strtoupper($_POST['reg_date']));
$rc_holder = escapeString($conn,strtoupper($_POST['rc_holder']));
$veh_type = escapeString($conn,strtoupper($_POST['veh_type']));
$fuel_type = escapeString($conn,strtoupper($_POST['fuel_type']));
$veh_class = trim(escapeString($conn,strtoupper($_POST['veh_class'])));
$maker_name = trim(escapeString($conn,strtoupper($_POST['maker_name'])));
$model = trim(escapeString($conn,strtoupper($_POST['model'])));
$engine_no = escapeString($conn,strtoupper($_POST['engine_no']));
$chasis_no = escapeString($conn,strtoupper($_POST['chasis_no']));
$veh_for = escapeString($conn,strtoupper($_POST['veh_for']));

// REQ. DATA

$req_code = escapeString($conn,strtoupper($_POST['token_no']));
$invoice_date = escapeString($conn,strtoupper($_POST['invoice_date']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$gst_selection = escapeString($conn,strtoupper($_POST['gst_selection']));
$gst_type = escapeString($conn,strtoupper($_POST['gst_type']));
$gst_value = escapeString($conn,strtoupper($_POST['gst_value']));
$gst_amount = escapeString($conn,strtoupper($_POST['gst_amount']));
$discount_amount = escapeString($conn,strtoupper($_POST['discount_amount']));
$total_amount = escapeString($conn,strtoupper($_POST['total_amount']));

$chk_payment_amount = Qry($conn,"SELECT payment_amount,vou_no FROM asset_vehicle_req WHERE req_code='$req_code'");

if(!$chk_payment_amount){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./asset_vehicle_view.php");
	exit();
}
 
if(numRows($chk_payment_amount)==0){
	echo "<script>
		alert('Asset request not found !');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$row_amount = fetchArray($chk_payment_amount);
$vou_no = $row_amount['vou_no'];

$chk_veh = Qry($conn,"SELECT id FROM asset_vehicle WHERE reg_no='$reg_no'");
if(!$chk_veh){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_veh)>0){
	echo "<script>
		alert('Duplicate Registration Number : $reg_no.');
		$('#btn_asset_add').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($gst_selection=='YES'){
	$gst_amountDb =  sprintf("%.2f",($amount*$gst_value/100));
}
else{
	$gst_amountDb = 0;
}

if($veh_for=='SPECIFIC'){
	$employee = trim(escapeString($conn,strtoupper($_POST['employee'])));
}
else{
	$employee = $branch;
}

if($gst_amountDb!=$gst_amount)
{
	echo "<script>
			alert('Error : Please Check GST Amount !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if($total_amount!=($amount+$gst_amountDb))
{
	echo "<script>
			alert('Error : Please Check Total Amount !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if($row_amount['payment_amount']<=0)
{
	echo "<script>
			alert('Error : Invalid payment amount !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if(sprintf("%.2f",$row_amount['payment_amount'])!=sprintf("%.2f",$total_amount))
{
	echo "<script>
			alert('Error : Payment amount should be $row_amount[payment_amount] !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	if(!in_array($_FILES['invoice_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed.');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
$sourcePath = $_FILES['invoice_copy']['tmp_name'];

$file_name_1 = date('dmYHis').mt_rand();
$targetPath = "asset_upload/asset_veh_invoice/".$file_name_1.".".pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION);
$targetPath2 = "upload/".$file_name_1.".".pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION)!="pdf")
{
	ImageUpload(1000,700,$sourcePath);
}

// ImageUpload(1000,700,$sourcePath);
	
StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO asset_vehicle(vehicle_holder,token_no,reg_no,branch,branch_user,asset_type,veh_type,reg_date,owner_name,
veh_class,chasis_no,engine_no,fuel_type,maker,model,add_type,active,ho_approval,ho_approval_time,date,invoice_date,invoice,amount,gst_invoice,
gst_type,gst_value,gst_amount,total_amount,timestamp) VALUES ('$employee','$req_code','$reg_no','$branch','$branch_sub_user','$veh_for',
'$veh_type','$reg_date','$rc_holder','$veh_class','$chasis_no','$engine_no','$fuel_type','$maker_name','$model','1','1','$timestamp',
'$date','$invoice_date','$targetPath','$amount','$gst_selection','$gst_type','$gst_value','$gst_amount','$total_amount','$timestamp')");

if(!$insert){
	// unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertId = getInsertID($conn);

$insert2 = Qry($conn,"INSERT INTO asset_vehicle_docs(veh_id,timestamp) VALUES ('$insertId','$timestamp')");

if(!$insert2){
	// unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_added = Qry($conn,"UPDATE asset_vehicle_req SET asset_added='1' WHERE req_code='$req_code'");

if(!$update_added){
	// unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_vou_invoice_file = Qry($conn,"UPDATE mk_venf SET upload='$targetPath2' WHERE vno='$vou_no'");

if(!$update_vou_invoice_file){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($sourcePath, $targetPath)) 
{
	$flag = false;
	errorLog("File upload failed.1",$conn,$page_name,__LINE__);
}

if(!copy($targetPath,$targetPath2)) 
{
	$flag = false;
	errorLog("File upload failed.2",$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Vehicle: $reg_no added successfully !');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	unlink($targetPath);
	unlink($targetPath2);
	
	echo "<script>
			alert('Error : Error While Processing Request !');
			$('#btn_asset_add').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	exit();
	// Redirect("Error While Processing Request.","./asset_vehicle_view.php");
	exit();
}	
?>