<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>

<a target="_blank" href="./late_pod_download.php"><button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
class="btn-sm pull-right btn btn-warning" type="button">
<span class="fa fa-download"></span> Export excel</button></a>

<button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
class="btn-sm pull-right btn btn-warning" onclick="RefreshPOD()" type="button">
<span class="fa fa-refresh"></span> Refresh PODs Data</button>


<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid"> 
<div class="form-group col-md-12">
<br />	
	<div class="row">	
		<div class="form-group col-md-4">
			<h4><span class="fa fa-exclamation-triangle"></span> &nbsp; Pending PODs (45 days older): <font color="blue"><?php echo $branch; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12" id="result_pod">
	</div>
		
	</div>
</div>
</div>

<script>
function Load_pods(id)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "./_load_clear_45_days_lrs.php",
		data: 'branch=' + '<?php echo $branch; ?>',
		type: "POST",
		success: function(data){
			$("#result_pod").html(data);
		},
		error: function() {}
		});
}

function RefreshPOD()
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "./clear_45_days_lrs_refresh.php",
		data: 'branch=' + '<?php echo $branch; ?>',
		type: "POST",
		success: function(data){
			$("#func_result").html(data);
		},
		error: function() {}
		});
}

function RcvPOD(id,vou_id)
{
	var lrno = $('#lrno_'+id).val();
	var frno = $('#frno_'+id).val();
	
	$('#lrno_set').val(lrno);
	$('#frno_set').val('<?php echo $branch; ?>'+'_'+frno);
	
	$('#podForm').submit();
	// $("#loadicon").show();
		// jQuery.ajax({
		// url: "./_load_clear_45_days_lrs.php",
		// data: 'branch=' + '<?php echo $branch; ?>',
		// type: "POST",
		// success: function(data){
			// $("#result_pod").html(data);
		// },
		// error: function() {}
		// });
}
</script>

<form action="./pod/" target="_blank" method="POST" id="podForm">
	<input type="hidden" name="lrno_set" id="lrno_set">
	<input type="hidden" name="frno_set" id="frno_set">
</form>

<div id="func_result"></div>

<script type="text/javascript">
Load_pods();
</script>
