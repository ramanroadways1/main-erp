<?php
function mysqlQuery($link, $query) {
	$result = mysqli_query($link,$query);
	
	return $result;
}

function numRows($queryResult)
{
	$num = mysqli_num_rows($queryResult);
	return $num;
}

function fetchArray($queryResult)
{
	$arr = array();
	while($row = mysqli_fetch_array($queryResult,MYSQLI_BOTH))
	{
		$arr[] = $row;
	}
	
	return $arr;
}

function getAffectedNumRows($link)
{
	$noRowsAffected = mysqli_affected_rows($link);
	return $noRowsAffected;
}

function getLastInsertID($link)
{
    return mysqli_insert_id($link);
}

function closeConnection($link)
{
	mysqli_close($link);
}

function getMySQLError($link)
{
    return mysqli_error($link);
}


function getCurrentDateTime()
{
    $now = date('Y-m-d H:i:s');
    return $now;
}

function getCurrentDate() {
    $now = date('Y-m-d');
    return $now;
}


function escapeString($link, $str) {
    return mysqli_real_escape_string($link, $str);
}

?>