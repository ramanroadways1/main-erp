<?php
require_once("./connection.php");

$vou_id = escapeString($conn,strtoupper($_POST['vou_id']));

$delte_old_cache = Qry($conn,"DELETE FROM lr_pre WHERE timestamp < (NOW() - INTERVAL 30 MINUTE) AND frno!='$vou_id'");

$getLrs = Qry($conn,"SELECT id,d_id,company,truck_no,date,lrno,lr_type,lr_id,fstation,tstation,consignor,consignee,wt12,weight,
crossing,cross_to,brk_id FROM lr_pre WHERE frno='$vou_id'");

if(!$getLrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

// $get_sum_of_weight = Qry($conn,"SELECT ,SUM(wt12) as total_actual,SUM(weight) as total_charge FROM lr_pre WHERE 
// frno='$vou_id' GROUP BY frno");

// if(!$get_sum_of_weight){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// $row_sum = fetchArray($get_sum_of_weight);
?>
<script>
$(function() {
		$("#broker_name").autocomplete({
		source: 'autofill/get_broker.php',
		select: function (event, ui) { 
           $('#broker_name').val(ui.item.value);   
           $('#broker_id').val(ui.item.id);     
           $('#broker_mobile').html(ui.item.mobile);     
           $('#broker_pan').html(ui.item.pan);     
           $('#broker_pan_1').val(ui.item.pan);     
           $('#broker_branch_name').val(ui.item.branch);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Broker does not exists.');
			$("#broker_name").val('');
			$("#broker_name").focus();
			$("#broker_id").val('');
			$("#broker_branch_name").val('');
			$("#broker_mobile").html('');
			$("#broker_pan").html('');
			$("#broker_pan_1").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
		
$(function() {
		$("#driver_name").autocomplete({
		source: 'autofill/get_driver.php',
		select: function (event, ui) { 
           $('#driver_name').val(ui.item.value);   
           $('#driver_id').val(ui.item.id);     
           $('#driver_mobile').html(ui.item.mobile);     
           $('#driver_pan').html(ui.item.pan);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Driver does not exists.');
			$("#driver_name").val('');
			$("#driver_name").focus();
			$("#driver_id").val('');
			$("#driver_mobile").html('');
			$("#driver_pan").html('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});		
</script>

<div class="form-group col-md-12 table-responsive" style="border:0;overflow:auto">
<?php
if(isset($_SESSION['freight_memo']))
{
?>
<div class="row">
	<div class="col-md-4 pull-right">
		<button onclick="ClearCache();" style="margin-left:4px;" type="button" class="btn btn-danger btn-sm pull-right">Clear Cache</button>
		<button type="button" style="margin-left:4px;" data-toggle="modal" data-target="#CrossingLRModal" class="btn btn-primary btn-sm pull-right">
			ADD Crossing LR
		</button>
		<?php
		$chk_break_branch = Qry($conn,"SELECT id FROM break_branch WHERE branch='$branch'");
		if(numRows($chk_break_branch)>0)
		{
		?>
<button type="button" style="margin-left:4px;" data-toggle="modal" data-target="#BreakingLRModal" class="btn btn-warning btn-sm pull-right">ADD Breaking LR</button>
		<?php
		}
		?>
	</div>
</div>
<?php
}
?>

<style>
tr:hover{
	background:lightblue;
}
</style>

	<table class="table table-bordered" style="font-size:12px;background">
		<tr>
			<th>#</th>
			<th>LR<br>Date</th>
			<th>LR<br>Number</th>
			<th>LR Type</th>
			<th>From<br>Location</th>
			<th>To<br>Location</th>
			<th>Consignor & Consignee</th>
			<th>Actual<br>Weight</th>
			<th>Charge<br>Weight</th>
			<th>Crossing</th>
			<th>Cross Station</th>
			<th>#</th>
		</tr>	
<?php

if(numRows($getLrs)==0){
	echo "<tr>
			<td colspan='13'><center><font color='red'>NO LR FOUND !!!</font></center></td>
		</tr>";
		
	$act_total = 0;
	$charge_total = 0;
	$truck_no_db="";
	$company_db = "";
	$dl_id="0";
}
else{
$sn=1;
	
	$act_total = 0;
	$charge_total = 0;

	while($row = fetchArray($getLrs))
	{
		$act_total=$act_total+$row['wt12'];
		$charge_total=$charge_total+$row['weight'];
		$truck_no_db = $row['truck_no'];
		$company_db = $row['company'];
		$dl_id = $row['d_id'];
		
		if($row['lr_type']==0){
			$crossing_lr = "Same Veh.";
		}else{
			$crossing_lr = "<font color='red'>Other Veh.</font>";
		}
	
		if($row['crossing']=='YES'){
			$checkbox_set = "checked";
		}else{
			$checkbox_set = "";
		}
		
		// if($row['lr_type']==0 AND $row['brk_id']=='')
		// {
			// $delete_button="";
		// }
		// else
		// {
			$delete_button="<button onclick='DeleteLrFromFm($row[id])' type='button' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-trash'></span></button>";
		// }
		
		echo "<tr>
				<td>$sn</td>
				<td>".convertDate('d/m/y',$row['date'])."</td>
				<td>$row[lrno]</td>
				<td>$crossing_lr</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td>
				Consignor: $row[consignor]
				<br>
				Consignee: $row[consignee]
				</td>
				<td>$row[wt12]</td>
				<td>$row[weight]</td>
				<td><input type='checkbox' $checkbox_set id='cross$row[id]' value='$row[id]' /></td>
				<td>$row[cross_to]</td>
				<input type='hidden' id='get_lrno$row[id]' value='$row[lrno]'>
				<td>$delete_button</td>
			</tr>";
	$sn++;		
	}
}
?>
	<tr>
		<td colspan="7"><b>Total Calculation :</b></td>
		<td><?php echo sprintf("%.3f",$act_total); ?></td>
		<td><?php echo sprintf("%.3f",$charge_total); ?></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</div>

<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
			var myId = $(this).val();
			var lrno = $('#get_lrno'+myId).val();
			
            if($(this).is(":checked"))
			{
				$('#cross_lrno').html(lrno);
				$('#cross_lrno2').val(lrno);
				$('#checkboxId').val(myId);
				$('#open_crossing_lr').click();
            }
            else if($(this).is(":not(:checked)"))
			{
              $("#loadicon").show();
				jQuery.ajax({
					url: "./undo_crossing.php",
					data: 'lrno=' + lrno + '&id=' + myId + '&page_name=' + 'smemo.php',
					type: "POST",
					success: function(data){
						$("#get_lr_result").html(data);
					},
					error: function() {}
			  });
            }
        });
    });
	
$(function() {
		$("#cross_location").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#cross_location').val(ui.item.value);   
           $('#cross_location_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#cross_location").val('');
			$("#cross_location").focus();
			$("#cross_location_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

function UnCheck(id)
{
	$('#cross'+id).attr('checked', false);
}

function GetCrossingInfo()
{
	var lrno = $('#lrno_crossing_1').val();
	
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_crossing_info_by_lrno.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		$('#crossing_lr_add_btn').atrr('disabled',true);
	}
}
</script>

<?php
if(numRows($getLrs)>0){
	
if($dl_id!=0)
{
	$get_driver_data = Qry($conn,"SELECT name,pan as lic_no,mo1 FROM mk_driver WHERE id='$dl_id'");
	if(!$get_driver_data){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$row_driver_data = fetchArray($get_driver_data);
}

$get_vehicle_info = Qry($conn,"SELECT id,wheeler,name,mo1,pan FROM mk_truck WHERE tno='$truck_no_db'");
if(!$get_vehicle_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_vehicle_info)==0){
	errorLog("Market Vehicle not found. TruckNo : $truck_no_db.",$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_vehicle_info = fetchArray($get_vehicle_info);

$wheeler_db = $row_vehicle_info['wheeler'];

$wheeler_array = array("4","6","10","12","14","16","18","22");

if(!in_array($wheeler_db, $wheeler_array))
{
    include("./modal_wheeler_update.php");
	echo "<script>$('#open_update_wheeler').click();</script>";
	HideLoadicon();
	exit();
}
else
{
	$max_weight_Qry = Qry($conn,"SELECT max_weight FROM weight_lock_wheeler WHERE wheeler='$wheeler_db'");
	
	if(!$max_weight_Qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
	}

	if(numRows($max_weight_Qry)==0){
		errorLog("Wheeler not found in database. Wheeler $wheeler_db.",$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_max_weight = fetchArray($max_weight_Qry);
	
	$max_weight = $row_max_weight['max_weight'];
}

	if($charge_total>$max_weight)
	{
		$max_weight = $charge_total;
	}
	
	$allow_max_weight_chk = Qry($conn,"SELECT id FROM weight_lock WHERE frno='$vou_id' AND lock_set='1'");
	
	if(!$allow_max_weight_chk){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
	}
	
	if(numRows($allow_max_weight_chk)>0)
	{
		$max_weight="90";
	}
?>

<form id="freightMemo" method="POST" style="font-size:13px;">

	<div class="form-group col-md-4">	
		<label>Truck Number <font color="red">*</font></label>
		<input style="width:70%" class="form-control" value="<?php echo $truck_no_db; ?>" name="tno" type="text" required readonly />
		<label style="padding-top:8px;font-size:12px;">Owner Name : <span style="color:red"><?php echo $row_vehicle_info['name']; ?></span></label>
		<br>
		<label style="font-size:12px;">Mobile No. : <span style="color:red"><?php echo $row_vehicle_info['mo1']; ?></span></label>
		<br>
		<label style="font-size:12px;">PAN No. : <span style="color:red"><?php echo $row_vehicle_info['pan']; ?></span></label>
	</div>
	
	<div class="form-group col-md-4">	
		<label>Broker Name <font color="red">*</font>
		<a data-toggle="modal" data-target="#AddBrokerModal">Add New</a>
		</label>
		<input style="width:70%" onchange="CheckBrokerBranch()" id="broker_name" class="form-control" type="text" required />
		<label style="padding-top:8px;font-size:12px;">PAN No. : <span style="color:red" id="broker_pan"></span></label>
		<br>
		<input type="hidden" value="<?php echo $row_vehicle_info['pan']; ?>" name="owner_pan_1">
		<input type="hidden" id="broker_pan_1" name="broker_pan_1">
		<label style="font-size:12px;">Mobile No. : <span style="color:red" id="broker_mobile"></span></label>
	</div>
	
<script>
function CheckBrokerBranch()
{
	var branch_my = '<?php echo $branch; ?>';
	var broker_branch_name = $('#broker_branch_name').val();
	
	if(branch_my!=broker_branch_name)
	{
		alert('Selected Broker Belongs to '+broker_branch_name+' Branch !');
	}
}
</script>
	
	<input type="hidden" id="broker_branch_name">
	<input type="hidden" id="broker_id" name="broker_id">
	<input type="hidden" id="driver_id" <?php if($dl_id!=0){ echo "value='$dl_id'"; } ?> name="driver_id">
	<input type="hidden" id="owner_id" name="owner_id" value="<?php echo $row_vehicle_info['id']; ?>">
	
	<div class="form-group col-md-4">	
		<label>Driver Name <font color="red">*</font> 
		<a data-toggle="modal" data-target="#AddDriverModal">Add New</a>
		</label>
		<input style="width:70%" id="driver_name" <?php if($dl_id!=0){ echo "value='$row_driver_data[name]' readonly='readonly'"; } ?> class="form-control" type="text" required />
		<label style="padding-top:8px;font-size:12px;">Lic No. : <span style="color:red" id="driver_pan"><?php if($dl_id!=0){ echo "$row_driver_data[lic_no]"; } ?></span></label>
		<br>
		<label style="font-size:12px;">Mobile No. : <span style="color:red" id="driver_mobile"><?php if($dl_id!=0){ echo "$row_driver_data[mo1]"; } ?></span></label>
	</div>
	
	<div class="col-md-12">	
		<hr>
	</div>
	
	<div class="form-group col-md-2">	
		<label>Company <font color="red">*</font></label>
		<input class="form-control" value="<?php echo $company_db; ?>" name="company" type="text" required readonly />
	</div>
	
	<div class="form-group col-md-3" id="start_location_div">	
		<label>Start Location <font color="red">*</font></label>
		<select name="start_location" id="start_location" onchange="ResetInput()" class="form-control" required="required">
			<option value="">--Select Location--</option>
			<?php
			$get_from_loc = Qry($conn,"SELECT DISTINCT fstation,from_id FROM lr_pre WHERE frno='$vou_id'");
			if(numRows($get_from_loc)>0)
			{
				while($row_from = fetchArray($get_from_loc))
				{
					echo "<option value='$row_from[fstation]_$row_from[from_id]'>$row_from[fstation]</option>";
				}
			}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-3" id="last_location_div">	
		<label>Last Location <font color="red">*</font></label>
		<select name="last_location" id="last_location" onchange="ResetInput()" class="form-control" required="required">
			<option value="">--Select Location--</option>
			<?php
			$get_to_loc = Qry($conn,"SELECT DISTINCT tstation,to_id FROM lr_pre WHERE frno='$vou_id'");
			if(numRows($get_to_loc)>0)
			{
				while($row_to = fetchArray($get_to_loc))
				{
					echo "<option value='$row_to[tstation]_$row_to[to_id]'>$row_to[tstation]</option>";
				}
			}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-2">	
		<label>Actual Weight <font color="red">*</font></label>
		<input name="actual_weight_db" value="<?php echo $act_total; ?>" class="form-control" readonly required />
	</div>
	
	<div class="form-group col-md-2">	
		<label>Charge Weight <font color="red">*</font></label>
		<input max="<?php echo $max_weight; ?>" class="form-control" id="weight_g" onblur="MyFunc1();" oninput="MyFunc1();" name="weight_g" type="number" step="any" min="0.100" required />
	</div>
	
	<div class="form-group col-md-2">
		<label>Rate/FIX <font color="red">*</font></label>
		<select id="known" onchange="Known(this.value)" name="known" required="required" class="form-control">
			<option value="">Select an option</option>
			<option value="FIX">Fix Freight</option>
			<option value="RATE">Rate</option>
			<option value="ZERO">ZERO Freight</option>
		</select>
	</div>
	
	<div class="form-group col-md-2">
       <label>Rate <font color="red">*</font></label>
       <input step="any" min="0" onblur="new1();" name="rate" type="number" id="rate" class="form-control" required readonly />
    </div>
										
    <div class="form-group col-md-2">
       <label>Freight <font color="red">*</font></label>
       <input type="number" onblur="new1();" min="0" name="actual" id="actual" class="form-control" readonly required />
    </div>
	
	<div class="form-group col-md-12">
	</div>
	
	<div class="form-group col-md-5">
		<label>Cashier Signature <font color="red"><sup>*</sup></font></label>
		<div id="newsign">
			<div class="m-signature-pad--body">
				<canvas id="put1" style="border:1px solid #aaa;touch-action:none;width:100%;background:#FFF"></canvas>
			</div>
			<div class="m-signature-pad--footer" style="padding-top:10px;">
				<button type="button" class="btn btn-primary" data-action="clear">Clear</button>
				<button type="button" class="btn btn-primary" data-action="save">Save</button>
			</div>
		</div>
	</div>

	<div class="form-group col-md-4 col-md-offset-2">
		<label>Signature Preview</label>
		<br>
		 <script src="js/signature_pad.js"></script>
		 <script src="js/app_exp_vou.js"></script>
		<img id="myImg" name="myImg" style="width:40%;" />
		<input type="hidden" id="sign" name="signature" />
	</div>
	
	<div class="form-group col-md-12">
		<button id="freight_memo_button" type="submit" style="color:#FFF" class="btn btn-success btn-lg pull-right">
		<span class="glyphicon glyphicon-edit"></span> &nbsp;Create Freight Memo
		</button>
	</div>	
	
	<div class="form-group col-md-12">&nbsp;</div>
	<div class="form-group col-md-12">&nbsp;</div>

</form>	

<script type="text/javascript">
$(document).ready(function (e) {
	$("#freightMemo").on('submit',(function(e) {
		$("#loadicon").show();
		$("#freight_memo_button").attr("disabled", true);
		e.preventDefault();
		$.ajax({
        	url: "./save_freight_memo.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#get_lr_result").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<?php
}

HideLoadicon();
?>