<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
<a href="./employee_add_new.php"><button style="margin-top:10px;margin-right:10px" class="pull-right btn-sm btn btn-success">
<span class="fa fa-plus-circle"></span> Add NEW Employee</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid"> 
<div class="form-group col-md-12">
<br />		
	<div class="row">	
		<div class="form-group col-md-4">
			<h4><span class="glyphicon glyphicon-user"></span> &nbsp; All Employee : <font color="blue"><?php echo $branch; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining<br>Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>Alternate<br>Mobile</th>
					<th>WhatsApp<br>Mobile</th>
					<th>PAN</th>
					<th>Update<br>Mobile</th>
					<th>View</th>
					<th>Transfer</th>
					<th>Terminate</th>
					<th>#</th>
				</tr>	
<?php
$view_emp = Qry($conn,"SELECT id,edit,name,code,join_date,father_name,mobile_no,alternate_mobile,whatsapp_mobile,acc_pan,status,branchtransfer 
FROM emp_attendance WHERE branch='$branch' AND status IN('2','3','-1')");

// 2 : Tranfer initiated , 3 : Approved , -1 :Terminated

if(!$view_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_emp)>0)
{
	$sn=1;
	while($row = fetchArray($view_emp))
	{
		if($row['join_date']==0){
			$join_date = "NULL";
		}
		else
		{
			$join_date = convertDate("d-m-y",$row["join_date"]);
		}
		
		if($row['status']=="2"){
			$disable_transfer="disabled";
			$name_btn_transfer="Transferred";
		}
		else{
			$disable_transfer="";
			$name_btn_transfer="Transfer";
		}
		
		if($row['status']=="3"){
			$disable_tremiate="";
			$name_btn_tremiate="Terminate";
			
			if($row['mobile_no']=='')
			{
				$mobile_no_emp = "<button type='button' onclick='EmpUpdateFunc($row[id])' class='btn btn-xs btn-default'>
				<span class='glyphicon glyphicon-edit'></span> Update</button>";
			}
			else
			{
				$mobile_no_emp = "<font color='green'><b>Updated</b></font>";
			}
		}
		else{
			$disable_tremiate="disabled";
			$name_btn_tremiate="In-Active";
			$mobile_no_emp = "";
		}
		
	echo "<input type='hidden' value='$row[name]' id='emp_name$row[id]'>";
	echo "<input type='hidden' value='$row[code]' id='emp_code$row[id]'>";	
	
		echo "<tr>
			<td>$sn</td>
			<td>$row[name]</td>
			<td>$row[code]</td>
			<td>$join_date</td>
			<td>$row[father_name]</td>
			<td>$row[mobile_no]</td>
			<td>$row[alternate_mobile]</td>
			<td id='whatsapp_mobile_col_$row[id]'>";
			
			if($row['whatsapp_mobile']=='' AND $row['code']==$_SESSION['user_code'])
			{
			echo "
			<script>
			function WhatsAppMob$row[id](elem,elemid)
			{
				$('#whatsapp_mobile_other_input_'+elem).val('');
				
				if(elem=='OTHER')
				{
					$('#whatsapp_mobile_other_input_'+elemid).show();
				}
				else
				{
					$('#whatsapp_mobile_other_input_'+elemid).hide();
				}
			}
			</script>
			<div class='form-inline'>
			<select class='form-control' onchange='WhatsAppMob$row[id](this.value,$row[id])' style='width:120px;font-size:12px;height:30px;' id='whatsapp_mobile_sel_$row[id]'>
				<option value=''>--select--</option>
				<option value='$row[mobile_no]'>$row[mobile_no]</option>
				";
				if($row['alternate_mobile']!='')
				{
					echo "<option value='$row[alternate_mobile]'>$row[alternate_mobile]</option>";
				}
			echo "<option value='OTHER'>Other</option>";	
			echo "	
			</select>
			<input type='text' class='form-control' style='display:none;width:120px;font-size:12px;height:30px;' maxlength='10' oninput='this.value=this.value.replace(/[^0-9]/,\"\")' id='whatsapp_mobile_other_input_$row[id]' required />
			<button type='button' id='whatsapp_mobile_save_btn_$row[id]' onclick='SaveWhatsappMobile($row[id])' 
			class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button>
			</div>";
			}
			else
			{
				echo "$row[whatsapp_mobile]";
			}
			echo "</td>
			<td>$row[acc_pan]</td>
			<td><input type='hidden' value='$row[mobile_no]' id='EmpMobileIp$row[id]'>$mobile_no_emp</td>
			<td><a style='color:#000' href='employee_view_full.php?id=$row[id]' class='btn btn-sm btn-default'>
			<span class='glyphicon glyphicon-list-alt'></span></a></td>
			<td>";
			if($row['status']=="2")
			{
				echo "Transferred to <font color='red'>$row[branchtransfer]</font><br><font color='blue'>(Manager approval pending)</font>";
			}
			else if($row['status']=="3")
			{
					echo "
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='branch_name$row[id]'>
				<option value=''>--branch--</option>"; 
				$get_all_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('$branch','HEAD','DUMMY') 
				ORDER by username ASC");

				if(!$get_all_branch){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}

				if(numRows($get_all_branch)>0)
				{
					while($row_branch = fetchArray($get_all_branch))
					{
						echo "<option value='$row_branch[username]'>$row_branch[username]</option>"; 
					}
				}
				else
				{
					echo "<option value=''>--select branch--</option>";
				}
			echo "
			</select>
			<button type='button' $disable_transfer id='transfer_button$row[id]' onclick='Transfer($row[id])' 
			class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>";
			}
			else
			{
				echo "";
			}
		echo "</td>";
		if($row['status']=="3")
		{
			echo "<td><button type='button' $disable_tremiate id='terminate_button$row[id]' onclick='Terminate($row[id])' 
			class='btn btn-sm btn-danger'><span class='fa fa-power-off'></span></button></td>";
		}
		else		
		{
			echo "<td></td>";
		}
		
		if($row['edit']=="0")
		{
			// if($_SESSION['user_code']=='032')
			// {
				echo "<td><a href='./edit_employee.php?id=$row[id]'><button type='button' class='btn btn-sm btn-warning'>
				<span class='fa fa-pencil-square-o'></span></button></a></td>";
			// }
			// else //if($branch=='GOPALPUR' AND $_SESSION['user_code']=='200')
			// {
				// echo "<td><a href='./edit_employee.php?id=$row[id]'><button type='button' class='btn btn-sm btn-warning'>
				// <span class='fa fa-pencil-square-o'></span></button></a></td>";
			// }
			// else if($row['code']==$_SESSION['user_code'])
			// {
				// echo "<td><a href='./edit_employee.php?id=$row[id]'><button type='button' class='btn btn-sm btn-warning'>
				// <span class='fa fa-pencil-square-o'></span></button></a></td>";
			// }
			// else
			// {
				// echo "<td><a href='#'><button disabled type='button' class='btn btn-sm btn-warning'>
				// <span class='fa fa-pencil-square-o'></span></button></a></td>";
			// }
		}
		else		
		{
			echo "<td></td>";
		}
		
		echo "</tr>";
		
	$sn++;	
	}
}
else
{
	echo "<tr><td colspan='8'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
</div>
</div>

<script>
function Transfer(id)
{
	var emp_name = $('#emp_name'+id).val();
	
	if(confirm("Do you really want to transfer: "+emp_name+" ?") == true)
	{
		var branch = $('#branch_name'+id).val();
		var mobile_no = $('#EmpMobileIp'+id).val();
		
		if(mobile_no=='')
		{
			alert('Update Employee\'s Mobile Number First !');
		}
		else
		{
			if(branch!='')
			{
				$('#transfer_button'+id).attr('disabled',true);
				$("#loadicon").show();
				jQuery.ajax({
				url: "./save_emp_transfer.php", 
				data: 'branch=' + branch + '&id=' + id,
				type: "POST",
				success: function(data){
					$("#func_result").html(data);
				},
				error: function() {}
				});
			}
			else
			{
				alert('Select Branch First !');
			}
		}
	}
}

function Terminate(id)
{
	var emp_name = $('#emp_name'+id).val();
	
	if(confirm("Do you really want to terminate: "+emp_name+" ?") == true)
	{
		$('#terminate_button'+id).attr('disabled',true);
		var mobile_no = $('#EmpMobileIp'+id).val();
		
		if(mobile_no=='')
		{
			alert('Update Employee\'s Mobile Number First !');
		}
		else
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_terminate.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}
	}
}

function EmpUpdateFunc(id)
{
	var emp_name = $('#emp_name'+id).val();
	var code = $('#emp_code'+id).val();
	
	if(emp_name=='' || code=='')
	{
		alert('Empty values !');
		window.location.href='./employee_view.php';
	}
	else
	{
		$('#emp_name_html').html(emp_name);
		$('#emp_name_update').val(emp_name);
		$('#emp_code_update').val(code);
		
		jQuery.noConflict(); 
		$("#UpdateEmpModal").modal()
	}
}

function SaveWhatsappMobile(id)
{
	var sel_option = $('#whatsapp_mobile_sel_'+id).val();
	var other_mobile = $('#whatsapp_mobile_other_input_'+id).val();
	
	if(sel_option=='')
	{
		alert('Select mobile number first !');
	}
	else
	{
		$('#whatsapp_mobile_save_btn_'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "./save_emp_whatsapp_mobile.php",
		data: 'id=' + id + '&sel_option=' + sel_option + '&other_mobile=' + other_mobile,
		type: "POST",
		success: function(data){
				$("#func_result").html(data);
		},
			error: function() {}
		});
	}
}
</script>

<div id="func_result"></div>

<?php
include("./modal_emp_data_update.php");
?>