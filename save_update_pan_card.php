<?php
require_once 'connection.php';

$type = escapeString($conn,strtoupper($_POST['type']));
$id = escapeString($conn,strtoupper($_POST['id']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$page_name11 = escapeString($conn,($_POST['page_name']));
$timestamp = date("Y-m-d H:i:s");
	
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	if(!in_array($_FILES['pan_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed.');
			$('#loadicon').hide();
			$('#pan_update_button').attr('disabled',false);
		</script>";
		exit();
	}
    
	if(($_FILES['pan_copy']['size']/1024/1024)>4)
	{
		echo "<script>
			alert('Error : MAX Upload size allowed is : 4MB.');
			$('#loadicon').hide();
			$('#pan_update_button').attr('disabled',false);
		</script>";
		exit();	
	}
	
if($type=='OWNER')
{
	$get_ext_data = Qry($conn,"SELECT pan,up4 as pan_path FROM mk_truck WHERE id='$id'");
}
else
{
	$get_ext_data = Qry($conn,"SELECT pan,up5 as pan_path FROM mk_broker WHERE id='$id'");
}	

if(!$get_ext_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error.');
		$('#loadicon').hide();
		$('#pan_update_button').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($get_ext_data)==0)
{
	echo "<script>
			alert('Party not found.');
			$('#loadicon').hide();
			$('#pan_update_button').attr('disabled',false);
		</script>";
	exit();
}

$row_ext_data = fetchArray($get_ext_data);

$ext_pan_no = $row_ext_data['pan'];
$ext_pan_path = $row_ext_data['pan_path'];

$sourcePath = $_FILES['pan_copy']['tmp_name'];

if($sourcePath!="")
{
	$fix_name=date('dmYHis').mt_rand();
	$targetPath = "broker_pan/".$fix_name.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);

	ImageUpload(1000,700,$sourcePath);

	if(!move_uploaded_file($sourcePath, $targetPath)) 
	{
		echo "<script>
			alert('Unable to upload PAN Copy.');
			$('#loadicon').hide();
			$('#pan_update_button').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	echo "<script>
			alert('No attachment found.');
			$('#loadicon').hide();
			$('#pan_update_button').attr('disabled',false);
		</script>";
	exit();
}

if($type=='OWNER')
{
	$update = Qry($conn,"UPDATE mk_truck SET pan='$pan_no',up4='$targetPath' WHERE id='$id'");
}
else
{
	$update = Qry($conn,"UPDATE mk_broker SET pan='$pan_no',up5='$targetPath' WHERE id='$id'");
}

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$pan_edit_desc="$ext_pan_no to $pan_no, Copy: $ext_pan_path to $targetPath";

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$id','PAN_UPDATE','$pan_edit_desc','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_log){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($page_name11=='pay_balance.php')
{
	echo '<script>
			alert("PAN Card Updated Successfully !");
			document.getElementById("RefreshForm").submit();
		</script>';
	exit();
}
else
{
	echo '<script>
			alert("PAN Card Updated Successfully !");
			document.getElementById("reloadAdvForm").submit();
		</script>';
	exit();
}
?>