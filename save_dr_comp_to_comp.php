<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$amount = strtoupper(escapeString($conn,$_POST['amount']));
$to_company = strtoupper(escapeString($conn,$_POST['to_company']));
$from_company = strtoupper(escapeString($conn,$_POST['from_company']));
$narration = strtoupper(escapeString($conn,$_POST['narration']));
	
if($to_company=="" || $from_company==""){
	Redirect("Company not found.","./debit.php");
	exit();
}

if($to_company==$from_company)
{
	echo "<script>
		alert('Company Can\'t be same !');
		$('#loadicon').hide();
	</script>";
	exit();	
}	

	$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	
	if(!$fetch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
     $row_bal = fetchArray($fetch_bal);
     $rrpl_cash = $row_bal['balance'];
     $rr_cash = $row_bal['balance2'];
		
	if($from_company=='RRPL' AND $rrpl_cash>=$amount) 
	{
		StartCommit($conn);
		$flag = true;

		$update_balance = Qry($conn,"update user set balance=(balance-'$amount'),balance2=(balance2+'$amount') where username='$branch'");
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
			
		$insert_debit = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,debit,balance,timestamp) VALUES 
			('$branch','$branch_sub_user','$date','RRPL','DEBIT-ITR','$narration','$amount','".($rrpl_cash-$amount)."','$timestamp')");
			
		if(!$insert_debit){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$debit_cash_id = getInsertID($conn);
		
		$insert_credit = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,credit2,balance2,timestamp) VALUES 
			('$branch','$branch_sub_user','$date','RAMAN_ROADWAYS','CREDIT-ITR','$narration','$amount','".($rr_cash+$amount)."','$timestamp')");
			
		if(!$insert_credit){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$credit_cash_id = getInsertID($conn);
		
		$insert_debit_table = Qry($conn,"INSERT INTO debit(debit_by,cash_id,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
			('CASH','$debit_cash_id','ITR','$branch','$_SESSION[user_code]','RRPL','$amount','$narration','$date','$timestamp')");
			
		if(!$insert_debit_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$debit_tab_id = getInsertID($conn);
		
		$insert_credit_table = Qry($conn,"INSERT INTO credit(credit_by,cash_id,debit_table_id,section,branch,branch_user,company,amount,narr,date,
		timestamp) VALUES ('CASH','$credit_cash_id','$debit_tab_id','ITR','$branch','$_SESSION[user_code]','RAMAN_ROADWAYS','$amount','$narration','$date',
		'$timestamp')");
			
		if(!$insert_credit_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		if($flag)
		{
			MySQLCommit($conn);
			closeConnection($conn);
			echo "<script> 	
				alert('Transfer RRPL to RAMAN_ROADWAYS Completed Successfully !');
				window.location.href='./debit.php';
			</script>";
			exit();
		}
		else
		{
			MySQLRollBack($conn);
			closeConnection($conn);
			Redirect("Error While Processing Request.","./debit.php");
			exit();
		}
		
	}	
	else if($from_company=='RAMAN_ROADWAYS' AND $rr_cash>=$amount)
	{
		StartCommit($conn);
		$flag = true;

		$update_balance = Qry($conn,"update user set balance=(balance+'$amount'),balance2=(balance2-'$amount') where username='$branch'");
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
			
		$insert_debit = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,debit2,balance2,timestamp) VALUES 
			('$branch','$branch_sub_user','$date','RAMAN_ROADWAYS','DEBIT-ITR','$narration','$amount','".($rr_cash-$amount)."','$timestamp')");
			
		if(!$insert_debit){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$debit_cash_id = getInsertID($conn);
		
		$insert_credit = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,credit,balance,timestamp) VALUES 
			('$branch','$branch_sub_user','$date','RRPL','CREDIT-ITR','$narration','$amount','".($rrpl_cash+$amount)."','$timestamp')");
			
		if(!$insert_credit){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$credit_cash_id = getInsertID($conn);
		
		$insert_debit_table = Qry($conn,"INSERT INTO debit(debit_by,cash_id,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
			('CASH','$debit_cash_id','ITR','$branch','$_SESSION[user_code]','RAMAN_ROADWAYS','$amount','$narration','$date','$timestamp')");
			
		if(!$insert_debit_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$debit_tab_id = getInsertID($conn);
		
		$insert_credit_table = Qry($conn,"INSERT INTO credit(credit_by,cash_id,debit_table_id,section,branch,branch_user,company,amount,narr,date,
		timestamp) VALUES ('CASH','$credit_cash_id','$debit_tab_id','ITR','$branch','$_SESSION[user_code]','RRPL','$amount','$narration','$date',
		'$timestamp')");
			
		if(!$insert_credit_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		if($flag)
		{
			MySQLCommit($conn);
			closeConnection($conn);
			echo "<script> 	
				alert('Transfer RAMAN_ROADWAYS to RRPL Completed Successfully !');
				window.location.href='./debit.php';
			</script>";
			exit();
		}
		else
		{
			MySQLRollBack($conn);
			closeConnection($conn);
			Redirect("Error While Processing Request.","./debit.php");
			exit();
		}
	}
	else
	{
		echo "<script>
			alert('Insufficient Balance in company : $from_company !');
			window.location.href='./debit.php';
		</script>";
		exit();	
	}
?>