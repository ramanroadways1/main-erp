<?php
require_once 'connection.php';

$tno = escapeString($conn,strtoupper($_POST['tno']));

$check = Qry($conn,"SELECT id,name,mo1,pan,up1 as rc_front,rc_rear,up4 as pan_copy,up6 as dec1 FROM mk_truck WHERE tno='$tno'");
if(!$check){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check)==0)
{
	echo "<script>
		alert('Invalid vehicle number entered !');
		$('#Prev_req').show();
		$('#ChangeDiv').hide();
		$('#Fetch_Owner_Btn').attr('disabled',false);
		$('#button_oc_change').attr('disabled',true);
		$('#oc_tno').attr('readonly',false);
		$('#owner_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();	
}

$row = fetchArray($check);

$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
$url =substr($url,0,-26);

	$rc_copy1 = "$url/truck_rc/".substr($row['rc_front'], strpos($row['rc_front'], "truck_rc/") + 9);    
	$rc_copy2 = "$url/truck_rc/".substr($row['rc_rear'], strpos($row['rc_rear'], "truck_rc/") + 9);    
	$pan_copy = "$url/truck_pan/".substr($row['pan_copy'], strpos($row['pan_copy'], "truck_pan/") + 10);    
	$dec_copy = "$url/truck_dec/".substr($row['dec1'], strpos($row['dec1'], "truck_dec/") + 10);    
	
$rc_front_path = pathinfo($rc_copy1, PATHINFO_EXTENSION);
$rc_rear_path = pathinfo($rc_copy2, PATHINFO_EXTENSION);
$pan_path = pathinfo($pan_copy, PATHINFO_EXTENSION);
$dec_path = pathinfo($dec_copy, PATHINFO_EXTENSION);
											
echo "<script>
		// alert('$url');
		$('#Prev_req').hide();
		$('#ChangeDiv').show();
		$('#Fetch_Owner_Btn').attr('disabled',true);
		$('#oc_tno').attr('readonly',true);
		
		$('#owner_id').val('".$row['id']."');
		$('#oc_name').val('".$row['name']."');
		$('#oc_pan_no').val('".$row['pan']."');
		$('#oc_mobile_no').val('".$row['mo1']."');
		$('#old_rc').val('".$rc_copy1."');
		$('#old_rc_rear').val('".$rc_copy2."');
		$('#old_pan').val('".$pan_copy."');
		$('#old_dec').val('".$dec_copy."');
		
		$('#old_rc_path').val('".$rc_front_path."');
		$('#old_rc_rear_path').val('".$rc_rear_path."');
		$('#old_pan_path').val('".$pan_path."');
		$('#old_dec_path').val('".$dec_path."');
		
		$('#button_oc_change').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();

?>