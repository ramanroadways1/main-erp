<?php 
require_once './connection.php';

$output ='';
$date = date("Y-m-d");

$get_pods = Qry($conn,"SELECT f1.id,f1.vou_id,f1.vou_no,f1.lr_date,f1.create_date,f1.lrno,b.name as broker_name,o.name as owner_name,o.tno 
FROM _pending_lr_list AS f1 
LEFT OUTER JOIN mk_broker AS b ON b.id=f1.bid 
LEFT OUTER JOIN mk_truck AS o ON o.id=f1.oid 
WHERE DATEDIFF('$date',f1.lr_date)>45 AND f1.branch='$branch' AND bid not in(SELECT party_id from _by_pass_pod_lock WHERE is_active='1') 
AND oid not in(SELECT party_id from _by_pass_pod_lock WHERE is_active='1')");

if(!$get_pods){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_pods) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '<table border="1">    
     <tr>  
		<th>#</th>  				
        <th>Vou_no</th>  
        <th>Vehicle_no</th>  
        <th>Owner_name</th>  
        <th>Broker_name</th>  
        <th>LR_date</th>  
        <th>LR_created_on</th>  
        <th>LR_number</th>  
    </tr>';
$sn=1;
  while($row = fetchArray($get_pods))
  {
   $output .= '
    <tr> 
		<td>'.$sn.'</td>  
		<td>'.$row["vou_no"].'</td>  
		<td>'.$row["tno"].'</td>  
		<td>'.$row["owner_name"].'</td>  
		<td>'.$row["broker_name"].'</td>  
		<td>'.$row["lr_date"].'</td>  
		<td>'.$row["create_date"].'</td>  
		<td>'.$row["lrno"].'</td>  
	</tr>';
$sn++;	
  }
  
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Late_pod_Branch: '.$branch.'.xls');
  echo $output;

closeConnection($conn);
?>