<form id="OwnerMobileForm" action="#" method="POST">
<div id="ModalUpdateMobile" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Vehicle Owner Mobile No.
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" id="tno_mobile_update" class="form-control" required="required">
			</div> 
			
			<div class="form-group col-md-6">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input id="update_owner_mobile" type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>Vehicle Owner Name <font color="red"><sup>*</sup></font></label>
				<input id="update_mobile_owner_name" readonly type="text" class="form-control" required="required">
			</div>
			<input type="hidden" name="type" value="owner">
			<input type="hidden" name="id" id="owner_mobile_id" value="">
			
			<div class="form-group col-md-12" id="MobileAlertOwner" style="display:none;color:red">Branch can update mobile only once !!</div>
			
		</div>
      </div>
	  <div id="result_OwnerMobileForm"></div>
      <div class="modal-footer">
        <button type="submit" disabled="disabled" id="updateOwnerMobileBtn" class="btn btn-primary">Update</button>
        <button type="button" id="" onclick="$('#OwnerMobileForm')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#OwnerMobileForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#updateOwnerMobileBtn").attr("disabled", true);
	$.ajax({
        	url: "./save_update_mobile.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_OwnerMobileForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>