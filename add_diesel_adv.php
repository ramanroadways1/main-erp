<?php
require_once("connection.php");

$timestamp = date("Y-m-d H:i:s"); 

$frno = escapeString($conn,strtoupper($_POST['frno_diesel']));
$vou_type = escapeString($conn,strtoupper($_POST['vou_type']));

if(empty($frno))
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Data : Vou Id.');
		window.location.href='./';
	</script>";
	exit();
}

if(empty($vou_type))
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Data : Vou Type.');
		window.location.href='./';
	</script>";
	exit();
}

$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='MARKET'");
if(!$get_diesel_rate_max){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
				
$row_dsl_max_rate = fetchArray($get_diesel_rate_max);

$dsl_max_rate=$row_dsl_max_rate['rate'];
$dsl_max_amount=$row_dsl_max_rate['amount'];
$dsl_max_qty=$row_dsl_max_rate['qty'];

$amount = escapeString($conn,$_POST['amount']);
$rilpre = escapeString($conn,$_POST['rilpre']);

$pump_card = escapeString($conn,strtoupper($_POST['pump_card']));

if($pump_card=='CARD')
{
	$card=escapeString($conn,strtoupper($_POST['card_no']));
	$type='CARD';
	$rate=0;
	$qty=0;
	$card_id=escapeString($conn,strtoupper($_POST['cardno2']));
	$phy_card_no=escapeString($conn,strtoupper($_POST['phy_card_no']));
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$trans_date = date("Y-m-d");
	$mobile_no="";	
}
else if($pump_card=='OTP')
{
	$card_id="0";
	$card=escapeString($conn,strtoupper($_POST['mobile_no']));
	$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
	$type="OTP";
	$rate=0;
	$qty=0;
	$phy_card_no=$mobile_no;
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$trans_date = date("Y-m-d");
}
else
{
	$mobile_no="";	
	$card_id="0";		
	$card=escapeString($conn,strtoupper($_POST['pump_name']));	
	$card=strtok($card,'-');
	$phy_card_no=$card;
	$fuel_company=escapeString($conn,strtoupper($_POST['pump_company']));
	$type='PUMP';
	$rate=$_POST['rate'];
	$qty=$_POST['qty'];
	$trans_date = escapeString($conn,strtoupper($_POST['trans_date']));
}

$date=date("Y-m-d");

if($rate!=0 AND $rate>$dsl_max_rate)
{
	echo "<script type='text/javascript'>
		alert('Error : Rate is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($qty!=0 AND $qty>$dsl_max_qty)
{
	echo "<script type='text/javascript'>
		alert('Error : Qty is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($amount!=0 AND $amount>$dsl_max_amount)
{
	echo "<script type='text/javascript'>
		alert('Error : Amount is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$delete = Qry($conn,"DELETE FROM diesel_sample WHERE date(timestamp)!='$date'");
if(!$delete){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($type=='PUMP')
{
	$done_one="1";
	$done_time=$timestamp;
}
else
{
	$done_one="0";
	$done_time="";
}

if($rilpre=="1")
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Stock not found !');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$amount)
	{
		echo "<script>
			alert('Card not in stock. Available Balance is : $row_ril_stock[balance].');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

$insert=Qry($conn,"INSERT INTO diesel_sample (frno,type,qty,rate,amount,card_pump,card_no,phy_card,card_id,ril_card,mobile_no,fuel_comp,
dsl_nrr,branch,date,done,done_time,timestamp,stockid) VALUES ('$frno','$vou_type','$qty','$rate','$amount','$type','$card','$phy_card_no',
'$card_id','$rilpre','$mobile_no','$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date',
'$done_one','$done_time','$timestamp','$ril_stockid')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	

echo "<script>
		alert('Diesel Entry Success.');
		LoadDiesel();
		$('#InsertDiesel')[0].reset();
		$('#insert_diesel_button').attr('disabled', false);
		document.getElementById('close_diesel_button').click();
	</script>";
	exit();
?>