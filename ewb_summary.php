<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<?php
include '_header3.php';
include './disable_right_click_for_index.php';
?>

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Eway-bill summary:</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>LR_No</th>
					<th>Vehicle_No</th>
					<th>OwnerMobile</th>
					<th>DriverMobile</th>
					<th>Crossing</th>
					<th>Crossing Veh</th>
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Ewb_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Ewb_Expiry_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Location & Party</th>
					<th>Ewb_No</th>
					<th>Narration</th>
					<th>#Status#</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<div id="edjsakld"></div>

<script type="text/javascript">
function FetchData(){
$("#loadicon").show();
var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		// "excel"
		// ,"colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=load_table.gif height=20> </center>"
        },
		"order": [[9, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{
        targets: 13, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<input type="hidden" value="'+full[1]+'" id="lrno_ip_'+full[0]+'"><button type="button" class="btn-xs btn-primary" onclick="UpdateStatus('+full[0]+','+full[11]+')" id="status_btn_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update</button>';
        }
      },
	], 
        "serverSide": true,
        "ajax": "get_ewb_summary.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchData();
</script>

<script type="text/javascript">
function UpdateStatus(id,ewbno)
{
	var lrno = $('#lrno_ip_'+id).val();
	$('#ewb_id').val(id);
	$('#ewb_no_html').html(ewbno+" , LR Number: "+lrno);
	$('#modal_btn')[0].click();
}
</script>

<?php include("./modal_update_ewb_status.php"); ?>

<div id="function_result"></div>