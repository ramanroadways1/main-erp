<form action="" id="LRForm" method="POST" autocomplete="off">

<div style="text-transform" class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
            <div class="modal-body">

<script type="text/javascript">
function CheckConsignor()
{
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
	}
	else
	{
		$('#consignor').attr('readonly',true);
	}
}

function CheckOpenLR()
{
	$('#lrno').val('');
	
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
	}
	else
	{
		var array = '<?php echo $open_lr_party_id; ?>';
		if(array.indexOf($('#consignor_id').val())!== -1)
		{
			$("#lrno").attr("oninput","this.value=this.value.replace(/[^a-zA-Z0-9]/,'');CheckConsignor()");
		}
		else
		{
			var lrno_next = $('#last_lrno_auto').val();
			$("#lrno").attr("oninput","this.value=this.value.replace(/[^0-9]/,'');CheckConsignor()");
			$("#lrno").val(lrno_next);
		}
		
		if($('#consignor_id').val()=='56' || $('#consignor_id').val()=='675')
		{
			$('#dest_zone_div').show();
			$('#dest_zone').attr('required',true);
		}
		else
		{
			$('#dest_zone_div').hide();
			$('#dest_zone').val('');
			$('#dest_zone').attr('required',false);
		}
	}
}

function checkLr()
{
	var lrno = $('#lrno').val();
	var billing_type = $('#billing_type').val();
	
	if(billing_type=='')
	{
		alert('select billing type first !');
	}
	else
	{
	if(!lrno.match(/^[0-9a-zA-Z]+$/))
	{
		alert('Plese check LR Number !');
		$('#lrno').val('');
	}
	else
	{
		$('#lrchk_button').attr("disabled",true);
		$('#lrno').attr("readonly",true);
		$('#LRallInputs *').prop('disabled', true);
		$('#LRallInputs').hide();
		$('#bilty_id').val('');
		
		if($('#consignor_id').val()=='' || $('#consignor').val()=='')
		{
			$('#consignor').focus();
			$("#consignor").attr("onblur","CheckOpenLR();$('#lrno').val('')");
			$('#lrno').val("");
			$('#lrchk_button').attr("disabled",false);
			$('#lrno').attr("readonly",false);
		}
		else
		{
			$('#consignor').attr('readonly',true);
			$("#consignor").attr("onblur","");
			$("#loadicon").show();
			jQuery.ajax({
				url: "./checklr_entry.php",
				data: 'lrno=' + lrno + '&con1_id=' + $('#consignor_id').val() + '&consignor=' + $('#consignor').val() + '&billing_type=' + billing_type,
				type: "POST",
				success: function(data) {
					$("#lr-status").html(data);
				},	
					error: function() {}
			});
		}  
	}  
	}  
}
</script>

<?php
if($branch=='KORBA')
{
	$fetch_balco=Qry($conn,"SELECT id,name,gst FROM consignor WHERE id='10'");
	if(!$fetch_balco){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($fetch_balco)>0)
	{
		$row_party = fetchArray($fetch_balco);
		
		$party_id=$row_party['id'];
		$party_name=$row_party['name'];
		$party_gst=$row_party['gst'];
		$party_name_readonly="readonly";
	}
	else
	{
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Consignor not found for korba.","./");
		exit();
	}
}
else
{
	$party_name_readonly="";
	$party_name="";
	$party_gst="";
	$party_id="";
}	
?>
	<div class="row">
	
	<div class="form-group col-md-3">
		 <label>Consignor <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'');$('#lrno').val('')" id="consignor" 
		value="<?php echo $party_name; ?>" onblur="CheckOpenLR();" name="consignor" class="form-control" <?php echo $party_name_readonly; ?> required />
	</div>
	
	<input type="hidden" name="consignor_id" id="consignor_id" value="<?php echo $party_id; ?>" required>
	
	<div class="form-group col-md-2">
		<label>Consignor GST No </label>
		<input id="con1_gst" type="text" name="con1_gst" class="form-control" value="<?php echo $party_gst; ?>" readonly />
	</div>
	
	<div class="form-group col-md-2">
		<label>Billing Type <font color="red">*</font></label>
       <select name="billing_type" id="billing_type" class="form-control" required="required">
			<option value="">--Select--</option>
			<option id="bill_2" value="NORMAL">Normal</option>
			<option id="bill_1" value="BILL_TO_SHIP_TO">Bill to Ship to</option>
		</select>
    </div>
	
	<div class="form-group col-md-3">
		<div class="row">
			<div class="col-md-8">
				<label>LR No. <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^0-9]/,'');CheckConsignor()" type="text" name="lrno" id="lrno" class="form-control" required />
			</div>
			<div class="col-md-4">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="lrchk_button" onclick="checkLr()" class="btn btn-sm btn-primary"><i style="font-size:14px;" class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Check</button>
			</div>		
		</div>		
    </div>

	<input type="hidden" id="bilty_id" name="bilty_id" />		

<div id="LRallInputs">
	
	<div id="man_company_div" style="display:none" class="form-group col-md-2">
		<label>Company <font color="red">*</font></label>
       <select name="company_man" id="company_man" class="form-control" required="required">
			<option value="">Select Company</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
    </div>
	
	<div id="auto_company_div" class="form-group col-md-2">
         <label>Company <font color="red">*</font></label>
         <input type="text" id="company_auto" name="company_auto" class="form-control" readonly required />
	</div>
	
	<div id="lr-status"></div>

<script type="text/javascript">			
function VehicleType(elem)
{
	$("#tno_market").val('');
	$("#tno_own").val('');
	
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
		$('#lr_type').val("");
	}
	else
	{
		if($('#lrno').val()=='')
		{
			$('#lrno').focus();
			$('#lr_type').val("");
		}
		else
		{
			if(elem=="OWN")
			{
				$("#div_market").hide();
				$("#tno_market").attr("required",false);
				
				$("#div_own").show();	
				$("#tno_own").attr("required",true);
			}
			else
			{
				$("#div_market").show();
				$("#tno_market").attr("required",true);
				
				$("#div_own").hide();	
				$("#tno_own").attr("required",false);
			}
		}
	}
}
</script>		

	<div class="form-group col-md-2">
		<label>Vehicle Type <font color="red">*</font></label>
		<select onchange="VehicleType(this.value)" name="lr_type" id="lr_type" class="form-control" required="required">
			<option value="">Select Vehicle Type</option>
			<option value="MARKET">MARKET Vehicle</option>
			<option value="OWN">OWN Vehicle</option>
		</select>
	</div>
							
	<div class="form-group col-md-2" id="div_market" style="display:none1">
		<label>Market TruckNo <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_market" id="tno_market" type="text" class="form-control" required />
    </div>
	
	<input type="hidden" id="wheeler_market_truck" name="wheeler_market">
	<input type="hidden" id="wheeler_own_truck" name="wheeler_own_truck">
	
	<div class="form-group col-md-2" id="div_own" style="display:none">
		<label>Own TruckNo <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_own" id="tno_own" type="text" class="form-control" required />
	</div>
	
	<div class="form-group col-md-2">
		<label>LR Date <font color="red">*</font></label>
		<input id="lr_date" name="date" type="date" class="form-control" min="<?php echo $min_lr_date; ?>" 
		max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-3">
		<label>From Station <font color="red">*</font></label>
		<input <?php if($branch=='KORBA'){echo "readonly"; } ?> type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" name="from" id="from" value="<?php if($branch=='KORBA'){echo "KORBA"; } ?>" class="form-control" required />
    </div>
	
	<input type="hidden" id="from_id" value="<?php if($branch=='KORBA'){echo "1326"; } ?>" name="from_id">
	<input type="hidden" id="to_id" name="to_id">
	
	<div class="form-group col-md-3">
		<label>To Station <font color="red">*</font></label>
		<input name="to" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" type="text" id="to" class="form-control" required />
	</div>
	
	<div style="display:none" id="dest_zone_div" class="form-group col-md-3">
		<label>Destination Zone <font color="red">*</font></label>
		<input name="dest_zone" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" type="text" id="dest_zone" class="form-control" />
	</div>

	<input type="hidden" id="dest_zone_id" name="dest_zone_id">
	<input type="hidden" id="bill_to_party_id" name="bill_to_party_id">
	
	<div id="bill_to_party_div">
	
	<div class="form-group col-md-3">
		<label>Bill To <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'')" id="bill_to_party" name="bill_to_party" class="form-control" required />
	</div>
	
	<div class="form-group col-md-3">
		<label>Bill To GST <font color="red">*</font></label>
		<input type="text" readonly id="bill_to_gst" name="bill_to_gst" class="form-control" required />
	</div>
	
	</div>
	
	<div class="form-group col-md-3">
		<label>Consignee<span id="ship_to">/Ship To</span><font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'')" id="consignee" name="consignee" class="form-control" required />
	</div>

<script>
function FetchEwayBill(eway_no){
	$('#EwayBillFlag').val('');
	
	var consignor_id = $('#consignor_id').val();
	var item_id = $('#item_id').val();
	
	if(consignor_id=='')
	{
		$('#eway_bill_no').val('');
		$('#consignor').focus;
	}	
	else
	{	
		if(item_id=="")
		{
			alert('Enter item first !')
			$('#eway_bill_no').val('');
		}
		else
		{
			$('#loadicon').show();
			jQuery.ajax({
				url: "./_API/get_eway_bill.php",
				data: 'ewb_no=' + eway_no + '&consignor_id=' + consignor_id + '&item_id=' + item_id,
				type: "POST",
				success: function(data) {
					$("#eway_result").html(data);
					$('#loadicon').hide();
				},	
				error: function() {}
			});
		}
	}
}

function CheckItemCode(elem)
{
	if(elem=='SALT')
	{
		$('#EwayBillFlag').val('2');
		$('#item1').attr('readonly',true);
		$('#eway_bill_no').attr('readonly',true);
		$('#eway_bill_no').attr('onblur','');
	}
}
</script>

<div id="eway_result"></div>
		
	<div class="form-group col-md-3">
		<label>Consignee GST No </label>
		<input id="con2_gst" type="text" name="con2_gst" class="form-control" readonly />
	</div>
	
	<div class="form-group col-md-3">
		<label>Material/Item <font color="red">*</font></label>
		<input onblur="CheckItemCode(this.value)" id="item1" name="item" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');" placeholder="Product/Item" class="form-control" required />
    </div>
	
	<input type="hidden" id="consignee_id" name="consignee_id">
	<input type="hidden" id="consignee_pincode" name="consignee_pincode">
	
	<!-- -->
		<input type="hidden" id="EwayBillFlag" name="EwayBillFlag">
		<input type="hidden" id="EwayBill_con1_gst" name="EwayBill_con1_gst">
		<input type="hidden" id="EwayBill_con1" name="EwayBill_con1">
		<input type="hidden" id="EwayBill_con2_gst" name="EwayBill_con2_gst">
		<input type="hidden" id="EwayBill_con2" name="EwayBill_con2">
		<input type="hidden" id="EwayBill_tno" name="EwayBill_tno">
	<!-- -->
	
<div class="form-group col-md-12">
	
	<div class="row">
	
	<div class="form-group col-md-3">
		<label>DO No. <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" name="do_no" id="do_no" class="form-control" required />
	</div>
	
    <div class="form-group col-md-3">
		<label>Invoice No. <font color="red">*</font></label>
		<input name="invno" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" type="text" id="invno" class="form-control" required />
    </div>
   
	<div class="form-group col-md-3">
		<label>Shipment/BE<?php if($branch=='KORBA'){echo "/LC"; } ?> No. <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9]/,'')" style="text-transform:uppercase" name="shipno" id="shipno" class="form-control" required />
     </div>
	
	<div class="form-group col-md-3">
		<label>PO No. <font color="red">*</font></label>
		<input name="po_no" oninput="this.value=this.value.replace(/[^a-z A-Z-0-9,]/,'')" type="text" class="form-control" required />
    </div>
   
	</div>	
</div>	
	
<div class="form-group col-md-12">
	
	<div class="row">
	
	<div class="form-group col-md-3">
		<label>Act.Wt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" name="act_wt" type="number" id="act_wt" step="any" class="form-control" required />
    </div> 

	<div class="form-group col-md-3">
		<label>GrossWt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" name="gross_wt" type="number" id="gross_wt" step="any" class="form-control" required />
    </div> 	
	
	<div class="form-group col-md-3">
       <label>Chrg.Wt (in MT) <font color="red">*</font></label>
	   <input onchange="ResetAll();" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" oninput="ResetAll();;" onblur="ResetAll();" min="0.010" name="chrg_wt" type="number" id="chrg_wt" class="form-control" step="any" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>E-way Bill Number <font color="red"><sup>*</sup></font></label>
		<input id="eway_bill_no" onblur="FetchEwayBill(this.value)" type="text" maxlength="12" name="eway_bill_no" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" />
	</div>
	
    </div>
</div>
	
	<?php
	if($branch=='KORBA' || $branch=='RAIPUR')
	{
	?>	
<div class="form-group col-md-12">
	
	<div class="row">	
	
	<div class="form-group col-md-2">
		<label>Bank </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,]/,'')" name="bank" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>Sold To PARTY </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.-,]/,'')" name="sold_to_pay" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>LR Name </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.-,]/,'')" name="lr_name" class="form-control" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Consignee Address </label>
		<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-.,/]/,'')" name="con2_addr" class="form-control"></textarea>
	</div>
	
	</div>
</div>
	<?php
	}
	else
	{
		echo "<input type='hidden' name='bank' value='' />";
		echo "<input type='hidden' name='sold_to_pay' value='' />";
		echo "<input type='hidden' name='lr_name' value='' />";
		echo "<input type='hidden' name='con2_addr' value='' />";
	}
	?>
	
	
	<input type="hidden" name="item_id" id="item_id">
	
	<div class="form-group col-md-12">
		<span style="font-size:13px;font-weight:bold;color:red">----- Optional fields ------</span>
	</div>

	<div class="form-group col-md-3">
		<label>No of Articles </label>
		<input placeholder="No of Bags Etc." name="articles" type="text" oninput="this.value=this.value.replace(/[^a-z- A-Z,0-9]/,'')" class="form-control" />
    </div>

	<div class="form-group col-md-3">
		<label>Goods Value </label>
		<input placeholder="Goods Value" min="0" step="any" name="goods_value" type="number" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Billing Rate <span id="rate_req"><font color="red">*</font></span></label>
		<input onchange="sum1();" onkeyup="sum1();" oninput="sum1();" onblur="sum1();" value="0" min="0" step="any" name="b_rate" type="number" id="b_rate" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Billing Amount <span id="rate_req"><font color="red">*</font></span></label>
		<input onblur="sum2();" onchange="sum2();" oninput="sum2();" onkeyup="sum2();" min="0" step="any" name="b_amt" type="number" id="b_amt" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Truck Type</label>
		<select name="t_type" class="form-control">
			<option value="">--Select Option--</option>
			<?php
			$get_t_type = Qry($conn,"SELECT title FROM truck_type WHERE hide=0");
			if(numRows($get_t_type)>0)
			{
				while($row_t_type = fetchArray($get_t_type))
				{
					echo "<option value='$row_t_type[title]'>$row_t_type[title]</option>";	
				}
			}
			?>
		</select>
	</div>
	
	<?php
	if($branch=='KORBA')
	{
	?>	
		<div class="form-group col-md-2">
		   <label>Plant </label>
		   <select name="plant_name" class="form-control">
				<option value="">--select--</option>
				<option value="1101">1101</option>
				<option value="1102">1102</option>
			</select>
		</div>	
	<?php
	}
	else
	{
		echo "<input type='hidden' name='plant_name' value=''>";
	}
	?>
	
	<?php
	if($branch=='KORBA' || $branch=='RAIPUR')
	{
	?>	
		<div class="form-group col-md-2">
		   <label>Freight Type </label>
		   <select name="freight_type" class="form-control">
				<option value="">NULL</option>
				<option value="PREPAID">PREPAID</option>
				<option value="TOPAY">TOPAY</option>
			</select>
		</div>	
	<?php
	}
	else
	{
		echo "<input type='hidden' name='freight_type' value=''>";
	}
	?>
	
	<div class="form-group col-md-<?php if($branch=='KORBA') { echo "2"; } else { echo "3"; } ?>">
		<label>HSN Code </label>
		<input name="hsn_code" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" />
    </div>
	
	<div class="form-group col-md-3">
		<label>Description of Goods </label>
		<textarea name="goods_desc" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')" class="form-control"></textarea>
    </div>
	
	<?php	
	if($branch=='PUNE')
	{
	?>
	<div class="form-group col-md-3">
		<label>Route Type: <font color="red">*</font></label>
			<select style="border:1px solid #000" name="route_type" required="required" class="form-control">	
				<option value="">Select an option</option>
				<option value="0">By Road</option>
				<option value="1">Express</option>
				<option value="2">Not BOSCH</option>
			</select>
	</div>
	<?php
	}
	else
	{
		echo "<input name='route_type' value='0' type='hidden' required />";
	}
	?>
	
</div>	
	
	<div class="form-group col-md-12">	
		<button type="submit" disabled="disabled" id="lr_sub" name="lr_sub" style="margin-left:10px;" class="btn btn-primary pull-right">ADD LR</button>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
	</div>	

	</div>	
   </div>
  </div>
 </div>
</div>
</form>      

<script type="text/javascript">
function sum1() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_amt").val(Math.round(Number($("#chrg_wt").val()) * Number($("#b_rate").val())).toFixed(2));
	}
}

function sum2() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_rate").val(Math.round(Number($("#b_amt").val()) / Number($("#chrg_wt").val())).toFixed(2));
	}	
}

function ResetAll() 
{
	$("#b_amt").val('');
	$("#b_rate").val('');
}
 </script>  

<script>	
// function itemFunc(item)
// {
	// if(item.value=='OTHER')
	// {
		// $("#item_div_korba").show();
		// $("#item1").val('');
	// }
	// else
	// {
		// $("#item_div_korba").hide();
		// $("#item1").val(item.value);
	// }
// }
</script>	

<?php
/*
if($branch=='KORBA')
{
?>
	<div class="form-group col-md-3">
		<label>Material/Item <font color="red">*</font></label>
		<select style="border:1px solid #000" id="items" onchange="itemFunc(this);" required="required" class="form-control">	
			<option value="">Select an option</option>
			<option value="INGOT">INGOT</option>
			<option value="WIRE ROD">WIRE ROD</option>
			<option value="CRP">CRP</option>
			<option value="HRP">HRP</option>
			<option value="ROLLED PRODUCT">ROLLED PRODUCT</option>
			<option value="OTHER">ANY OTHER</option>
		</select>
    </div>
	
	<div class="form-group col-md-3" style="display:none" id="item_div_korba">
		<label>Material/Item <font color="red">*</font></label>
		<input id="item1" name="item" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');" placeholder="Product/Item" class="form-control" required />
    </div>
<?php
}
else
{
	*/
?> 