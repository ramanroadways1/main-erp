<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form2").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./lr_date_exceed_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_function1").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-5 col-md-offset-4">

<form autocomplete="off" id="Form2">	

<div class="row">
	
<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Request : LR exceed 10 days
		<br />
		<br />
		क्रासिंग LR  की अवधि बढ़ने के लिए अनुरोध करे
		</h4></center>
		<br />
	</div>
	
	<div class="form-group col-md-12">
		<label>LR Number <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="lrno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-12">
		<label>Narration <font color="red"><sup>*</sup></font></label>
		<textarea style="font-size:12px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-/,.]/,'')" required="required" id="narration" name="narration" class="form-control"></textarea>
    </div>

	<div class="form-group col-md-12">
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" name="submit" value="Submit Request" />
	</div>
	
</form>	
	<div id="result_function1"></div>
</div>

</div>

</div>
</div>
</body>
</html>