<form id="CreditHOForm" action="#" method="POST">
<div id="CrHOModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Credit Head-Office
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input type="number" oninput="$('#confirm_sel').val('')" placeholder="Enter Amount" name="amount" min="1" id="amount_ho" 
				class="form-control" required />	
			</div>
			
			<div class="form-group col-md-6">
				<label>Company <font color="red">*</font></label>
				<select class="form-control" onchange="$('#confirm_sel').val('')" id="company_ho" name="company" required="required">
					<option value="">--Select Company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Confirm By <font color="red">*</font></label>
				<select class="form-control" id="confirm_sel" onchange="" name="confirm" required="required">
					<option value="">--Select Company--</option>
					<?php
						$fetch_confirm=Qry($conn,"SELECT name,username FROM ho_confirm order by id asc");
						if(!$fetch_confirm){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
							Redirect("Error while processing Request","./");
							exit();
						}
			
						if(numRows($fetch_confirm)>0)
						{
							while($row_confirm=fetchArray($fetch_confirm))
							{
								echo "<option value='$row_confirm[username]'>$row_confirm[name]</option>";
							}
						}
						?>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="confirm_button" onclick="Confirm($('#confirm_sel').val())" class="btn btn-danger">Send OTP</button>
			</div>
			
			<div id="result_otp" class="form-group col-md-6">
				<label>OTP <font color="red">*</font></label>
				<input type="number" name="otp" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-6">
				<label>Narration <font color="red">*</font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'');$('#confirm_sel').val('')" name="narration" class="form-control" required="required"></textarea>	
			</div>
			
		</div>
      </div>
	  
		<input type="hidden" id="company2" name="company2">
		<input type="hidden" id="confirm_by_db" name="confirm_by_db">
	
	  <div id="result_cr_ho_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="cr_ho_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

	
<script>
function Confirm(confirm_by)
{
	var amount = $("#amount_ho").val();
	var company = $("#company_ho").val();
	
	if(amount=='' || company=='')
	{
		alert('Amount and Company Required First !');
		$("#amount_ho").val('');
		$("#company_ho").val('');
		$("#confirm_sel").val('');
	}
	else
	{
		$("#amount_ho").attr('readonly',true);
		$("#company2").val(company);
		$("#company_ho").attr('disabled',true);
			
		if(confirm_by=='')
		{
			alert('Select Confirm by First !');
			$("#confirm_sel").focus();
		}
		else
		{
			$('#confirm_button').attr('disabled',true);
			$("#loadicon").show();
			$("#cr_ho_button").attr('disabled',true);
			$.ajax({
			url: "_otp_credit_ho.php",
			method: "post",
			data:'name=' + confirm_by + '&company=' + company + '&amount=' + amount + '&type=' + 'credit',
			success: function(data){
				$("#result_cr_ho_modal").html(data);
			}})
		}		
	}
}
</script>
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#CreditHOForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#cr_ho_button").attr("disabled", true);
	$.ajax({
        	url: "./save_credit_head_office.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_cr_ho_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>