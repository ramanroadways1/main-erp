<?php 
require_once './connection.php';

$lr_type=escapeString($conn,($_POST['lr_type']));
$lrno = escapeString($conn,$_POST['lrno']);
$amount=escapeString($conn,($_POST['amount']));
$narration=escapeString($conn,($_POST['narration']));
$company=escapeString($conn,($_POST['company']));
$tno=escapeString($conn,($_POST['tno_1']));
$from_loc=escapeString($conn,($_POST['from_loc']));
$to_loc=escapeString($conn,($_POST['to_loc']));
$date = date("Y-m-d"); 
$nrr = "LR: ".$lrno."-> ".$narration;
$timestamp = date("Y-m-d H:i:s");
	
if($amount<=0 || $amount=='')
{
	echo "<script>
		alert('Error : Invalid amount !');
		$('#loadicon').hide();
		$('#cr_claim_cr_button').attr('disabled',false);
	</script>";
	exit();	
}

if($lr_type=="")
{
	echo "<script>
		alert('LR Type is not valid !');
		$('#loadicon').hide();
		$('#cr_claim_cr_button').attr('disabled',false);
	</script>";
	exit();	
}	

if($lrno=="")
{
	echo "<script>
		alert('LR number is not valid !');
		$('#loadicon').hide();
		$('#cr_claim_cr_button').attr('disabled',false);
	</script>";
	exit();	
}

if($lr_type=='MARKET')
{
	$check_bilty_bal=Qry($conn,"SELECT balance FROM dairy.bilty_balance WHERE bilty_no='$lrno'");

	if(!$check_bilty_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./credit.php");
		exit();
	}
	
	if(numRows($check_bilty_bal)==0)
	{
		echo "<script>
			alert('Market bilty not valid !');
			$('#loadicon').hide();
			$('#cr_claim_cr_button').attr('disabled',false);
		</script>";
		exit();	
	}
	
	$row = fetchArray($check_bilty_bal);
	$new_balance = $row['balance']-$amount;
}
else
{
	$check_bilty_bal=Qry($conn,"SELECT balance FROM claim_book_trans WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");

	if(!$check_bilty_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./credit.php");
		exit();
	}
	
	if(numRows($check_bilty_bal)==0)
	{
		echo "<script>
			alert('Market bilty not valid !');
			$('#loadicon').hide();
			$('#cr_claim_cr_button').attr('disabled',false);
		</script>";
		exit();	
	}
	
	$row = fetchArray($check_bilty_bal);
	$new_balance = $row['balance']-$amount;
}

$check_bal=Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");

if(!$check_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./credit.php");
	exit();
}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash = $row_bal['balance'];
	$rr_cash = $row_bal['balance2'];
	$email = $row_bal['email'];
	
if($company=='RRPL') 
{	
	$newbal = $amount + $rrpl_cash;
	$credit_col = 'credit';		
	$balance_col = 'balance';		
}
else if($company=='RAMAN_ROADWAYS')
{
	$newbal=$amount + $rr_cash;
	$credit_col = 'credit2';		
	$balance_col = 'balance2';		
}
else
{
	errorLog("Company not found. $company.",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Something went wrong.. Company : $company');
		$('#loadicon').hide();
		$('#cr_claim_cr_button').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$query = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");

if(!$query){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$cashbook_entry = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$credit_col`,`$balance_col`,timestamp) VALUES 
('$branch','$_SESSION[user_code]','$date','$company','Credit_Claim','$nrr','$amount','$newbal','$timestamp')");

if(!$cashbook_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$cash_id = getInsertID($conn);
	
$insert_credit = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,tno,bilty_type,from_stn,to_stn,branch,branch_user,company,
bilty_no,amount,narr,date,timestamp) VALUES ('CASH','$cash_id','CLAIM','$tno','$lr_type','$from_loc','$to_loc','$branch',
'$_SESSION[user_code]','$company','$lrno','$amount','$narration','$date','$timestamp')");		
	
if(!$insert_credit){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

if($lr_type=='MARKET')
{
	$insert_balance = Qry($conn,"INSERT INTO dairy.bilty_book(bilty_no,branch,branch_user,date,debit,balance,narration,timestamp) VALUES 
	('$lrno','$branch','$_SESSION[user_code]','$date','$amount','$new_balance','$narration','$timestamp')");		
		
	if(!$insert_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$udpate_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd+'$amount',balance='$new_balance' WHERE bilty_no='$lrno'");		
		
	if(!$udpate_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}
else
{
	$insert_balance = Qry($conn,"INSERT INTO claim_book_trans(lrno,branch,branch_user,date,debit,balance,narration,timestamp) VALUES 
	('$lrno','$branch','$_SESSION[user_code]','$date','$amount','$new_balance','$narration','$timestamp')");		
		
	if(!$insert_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Claim Credited Successfully !!');
		window.location.href='./credit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit.php");
	exit();
}
?>