<form id="RcForm" action="#" method="POST">
<div id="RcModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update RC
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-7">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="rc_veh_no" name="veh_no" class="form-control" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Rc Copy (front side) <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="rc_front" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Rc Copy (rear side) <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="rc_rear" class="form-control" required="required">
			</div>
		</div>
      </div>
	  <input type="hidden" name="id" id="rc_form_id">
	  <div id="result_rc_form"></div>
      <div class="modal-footer">
        <button type="submit" id="rc_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" onclick="$('#RcForm')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#RcForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#rc_button").attr("disabled", true);
	$.ajax({
        	url: "./save_rc_update.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_rc_form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>