<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$frno1 = escapeString($conn,strtoupper($_POST['fm_no']));

$frno = explode("_",$frno1)[0];
$lr_date = explode("_",$frno1)[1];
$create_date_fm = explode("_",$frno1)[2];
$create_date_fm2 = date("d-m-Y",strtotime($create_date_fm));
$late_pod = escapeString($conn,($_POST['late_pod']));
$add_unloading = escapeString($conn,($_POST['add_unloading']));
$add_detention = escapeString($conn,($_POST['add_detention']));
$narration = escapeString($conn,($_POST['narration']));

if($late_pod=="2")
{
	$deduction_amount = escapeString($conn,($_POST['late_pod_amt']));
}
else
{
	$deduction_amount = 0;
}

$get_record = Qry($conn,"SELECT id FROM rcv_pod_free WHERE frno='$frno'");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./unload_detention.php");
	exit(); 
}

if(numRows($get_record)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO rcv_pod_free(lrno,frno,lr_date,charges,deduct_late_pod,add_unloading,add_detention,narration,branch,branch_user,
timestamp) VALUES ('$lrno','$frno','$lr_date','$deduction_amount','$late_pod','$add_unloading','$add_detention','$narration','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./unload_detention.php");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./unload_detention.php';
	</script>";
	exit();
?>