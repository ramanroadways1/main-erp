<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
$min2="-2 days";
$min = date("Y-m-d", strtotime("$min2"));
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<form style="font-size:14px" autocomplete="off" id="DieselVouForm">	

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	
<div class="row">
	
<div class="form-group col-md-2"></div>
	
<div class="form-group col-md-8">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Fuel - Voucher</h4></center>
		<br />
	</div>

	<div class="form-group col-md-6">
		<label>Select Vehicle <font color="red">*</font></label>
		<select data-size="6" name="veh_no" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
			<option value="">--Select--</option>
			<?php
$get_veh_list = Qry($conn,"SELECT reg_no,branch,owner_name FROM asset_vehicle WHERE branch IN('$branch','CGROAD') AND active='1' ORDER by reg_no ASC");
if(!$get_veh_list){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

while($row_veh = fetchArray($get_veh_list))
{
	echo "<option value='$row_veh[reg_no]'>$row_veh[reg_no]- $row_veh[branch] ($row_veh[owner_name])</option>";
}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-6">
		<label>Cash/Card/Pump/Otp <font color="red">*</font></label>
		<select onchange="DieselType(this.value)" name="diesel_type" class="form-control" required>
			<option value="">--Select--</option>
			<option value="CASH">CASH (debit from cashbook)</option>
			<option value="CARD">CARD</option>
			<option value="OTP">OTP</option>
			<option value="PUMP">PUMP</option>
		</select>
	</div>
	
	<script>
	function DieselType(elem)
	{
		$('#card_div').hide();
		$('#mobile_div').hide();
		$('#pump_div').hide();
		$('#card_company_div').hide();
		$('#cash_company_div').hide();
		$('#qty_div').hide();
		$('#rate_div').hide();
		$('#bill_no_div').hide();
		// $('#sign_div_1').hide();
		$('#pump_slip_div').hide();
		$('#narration_div').hide();
		$('#cash_debit_company_div').hide();
		$('#card_no').attr('required',false);
		$('#mobile_no').attr('required',false);
		$('#pump_list').attr('required',false);
		$('#card_company').attr('required',false);
		$('#cash_company').attr('required',false);
		$('#bill_no').attr('required',false);
		$('#pump_slip').attr('required',false);
		$('#narration').attr('required',false);
		$('#qty').attr('required',false);
		$('#rate').attr('required',false);
		$('#cash_debit_company').attr('required',false);
		$('#dsl_amount').attr('oninput','');
		$('#qty').val('');
		$('#rate').val('');
		$('#dsl_amount').val('');
		$('#pump_list').val('');
		$('#card_no').val('');
		
		if(elem=='CARD')
		{
			$('#card_div').show();
			$('#card_company_div').show();
			$('#card_no').attr('required',true);
			$('#card_company').attr('required',true);
		}
		else if(elem=='PUMP')
		{
			$('#pump_list').val('');
			$('#loadicon').show();
			$.ajax({
			url: "_load_pump_list_diesel_vou.php",
			method: "post",
			data:'ok=' + 'ok',
			success: function(data){
				$("#pump_list").html(data);
			}})
		}
		else if(elem=='OTP')
		{
			$('#card_company_div').show();
			$('#card_company').attr('required',true);
			$('#mobile_div').show();
			$('#mobile_no').attr('required',true);
		}
		else if(elem=='CASH')
		{
			// $('#sign_div_1').show();
			$('#cash_company_div').show();
			$('#bill_no_div').show();
			$('#pump_slip_div').show();
			$('#narration_div').show();
			$('#qty_div').show();
			$('#rate_div').show();
			$('#cash_debit_company_div').show();
			$('#qty').attr('required',true);
			$('#rate').attr('required',true);
			$('#dsl_amount').attr('oninput','AmountCall()');
			$('#cash_company').attr('required',true);
			$('#bill_no').attr('required',true);
			$('#pump_slip').attr('required',true);
			$('#narration').attr('required',true);
			$('#cash_debit_company').attr('required',true);
		}
		else
		{
			$('#card_no').attr('required',true);
			$('#mobile_no').attr('required',true);
			$('#pump_list').attr('required',true);
		}
		
		$('#pump_list').selectpicker('refresh');
		$('#card_no').selectpicker('refresh');
	}
	
	function FuelCompany(elem)
	{
		if(elem!='')
		{
			$('#card_no').val('');
			$('#loadicon').show();
			$.ajax({
			url: "_load_card_list_diesel_vou.php",
			method: "post",
			data:'company=' + elem,
			success: function(data){
				$("#card_no").html(data);
			}})
		}
		else
		{
			$("#card_no").html('<option value="">--Select--</option>');
			$('#card_no').selectpicker('refresh');
		}
	}
	</script>
	
	<div class="form-group col-md-6">
		<label>Transaction Date <font color="red">*</font></label>
		<input name="date" type="date" class="form-control" min="<?php echo $min; ?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div id="card_company_div" style="display:none" class="form-group col-md-6">
		<label>Fuel Company <font color="red">*</font></label>
		<select onchange="FuelCompany(this.value)" id="card_company" name="card_company" class="form-control" required>
			<option value="">--Select--</option>
			<?php
			$get_fuel_comp_11 = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1' ORDER by name ASC");
			if(!$get_fuel_comp_11){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}

			while($r_f_c = fetchArray($get_fuel_comp_11))
			{
				echo "<option value='$r_f_c[name]'>$r_f_c[name]</option>";
			}
			?>
		</select>
	</div>
	
	<div id="cash_company_div" style="display:none" class="form-group col-md-6">
		<label>Fuel Company <font color="red">*</font></label>
		<select id="cash_company" name="cash_company" class="form-control" required>
			<option value="">--Select--</option>
			<?php
			$get_fuel_comp_22 = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1' ORDER by name ASC");
			if(!$get_fuel_comp_22){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}

			while($r_f_c2 = fetchArray($get_fuel_comp_22))
			{
				echo "<option value='$r_f_c2[name]'>$r_f_c2[name]</option>";
			}
			?>
		</select>
	</div>
	
	<div style="display:none" id="card_div" class="form-group col-md-6">
		<label>Card number <font color="red">*</font></label>
		<select data-size="6" onchange="if($('#card_company').val()=='') { $('#card_company').focus();$('#card_no').val(''); }" id="card_no" name="card_no" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
			<option value="">--Select--</option>
		</select>
	</div>
	
	<div style="display:none" id="mobile_div" class="form-group col-md-6">
		<label>Mobile number <font color="red">*</font></label>
		<input name="mobile_no" id="mobile_no" maxlength="10" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required />
    </div>
	
	<div style="display:none" id="pump_div" class="form-group col-md-6">
		<label>Select Pump <font color="red">*</font></label>
		<select data-size="6" id="pump_list" name="pump_list" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
			<option value="">--Select--</option>
		</select>
	</div>
	
	<div class="form-group col-md-3" id="qty_div" style="display:none">
		<label>Qty <font color="red">*</font></label>
		<input step="any" name="qty" id="qty" type="number" class="form-control" min=".20" required />
    </div>
	
	<div class="form-group col-md-3" id="rate_div" style="display:none">
		<label>Rate <font color="red">*</font></label>
		<input step="any" name="rate" id="rate" type="number" max="120" class="form-control" min="40" required />
    </div>
	
	<div class="form-group col-md-6">
		<label>Amount <font color="red">*</font></label>
		<input oninput="AmountCall();" name="amount" id="dsl_amount" type="number" class="form-control" min="1" max="15000" required />
    </div>
	
	<div style="display:none" id="bill_no_div" class="form-group col-md-6">
		<label>Bill number <font color="red">*</font></label>
		<input name="bill_no" id="bill_no" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" class="form-control" required />
    </div>
	
	<div class="form-group col-md-6" id="pump_slip_div" style="display:none">
		<label>Upload Slip/Bill <font color="red"><sup>*</sup></font></label>
		<input type="file" accept="image/*" id="pump_slip" name="pump_slip" class="form-control" required="required">
	</div>
	
	<div id="cash_debit_company_div" style="display:none" class="form-group col-md-6">
		<label>Debit From Company <font color="red">*</font></label>
		<select id="cash_debit_company" name="cash_debit_company" class="form-control" required>
			<option value="">--Select--</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div style="display:none" id="narration_div" class="form-group col-md-6">
		<label>Narration <font color="red">*</font></label>
		<textarea name="narration" id="narration" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,-./]/,'')" class="form-control" required></textarea>
    </div>
	
	<div class="col-md-12" style="display:none1" id="sign_div_1">
	<div class="row">
		<div class="form-group col-md-6">
			<label>Cashier Signature <font color="red"><sup>*</sup></font></label>
			<div id="newsign">
				<div class="m-signature-pad--body">
					<canvas class="signature" style="width:100%;height:130px;max-width:100%;border:1.5px #CCC solid;background-color:#FFF"></canvas>
				</div>
				<div class="m-signature-pad--footer" style="padding-top:10px;">
					<button type="button" class="btn btn-sm btn-default" data-action="clear">Clear sign</button>
					<button type="button" class="btn btn-sm btn-default" data-action="save">Save sign</button>
				</div>
			</div>
		</div>

		<div class="form-group col-md-6">
			<label> Signature Preview</label>
			<br>
			 <script src="js/signature_pad.js"></script>
			 <script src="js/app_exp_vou.js"></script>
			<img id="myImg" name="myImg" style="width:100%;height:130px;border:1.5px #CCC solid" />
			<input type="hidden" id="sign" name="signature" />
		</div>
	</div>
	</div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="Save Voucher" />
	</div>
	
	<div id="vou_result"></div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>

<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselVouForm").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./diesel_vou_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#vou_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(function() {
$("#qty,#rate").on("keydown keyup blur change input", CalcDiesel);
function CalcDiesel() {
	if($("#qty").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#qty").val());
	}

	if($("#rate").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#rate").val());
	}
	
	$("#dsl_amount").val(Math.round(qty * rate).toFixed(2));
}});
				 
function AmountCall() 
{
	if($("#qty").val()=='' && $("#rate").val()==''){
		$("#qty").focus()
		$("#dsl_amount").val('');
	}
	else
	{
		if($("#rate").val()=='' || $("#rate").val()<60)
		{
			$("#rate").val((Number($("#dsl_amount").val()) / Number($("#qty").val())).toFixed(2));
		}
		else
		{
			$("#qty").val((Number($("#dsl_amount").val()) / Number($("#rate").val())).toFixed(2));
		}
	}	
}
</script>

<?php
include("./_footer.php");
?>
