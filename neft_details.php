<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">
	
<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">	

<form autocomplete="off" action="_fetch_neft_details.php" method="POST">	

<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="col-md-12">	
		<br />
		<br />
		<center><h4 style="color:#FFF">Rtgs/Neft Summary</h4></center>
		<br />
		<br />
	</div>

<script type="text/javascript">
$(function() {
		$("#truck_no").autocomplete({
		source: './autofill/get_market_vehicle.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $('#owner_id').val(ui.item.oid);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			$("#owner_id").val('');
			}}, 
		focus: function (event, ui){
			return false;}
		});});
		
  $(function() {
		$("#broker_name").autocomplete({
		source: './autofill/get_broker.php',
		select: function (event, ui) { 
               $('#broker_name').val(ui.item.value);   
               $('#broker_id').val(ui.item.id);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#broker_name").val('');
			$("#broker_name").focus();
			$("#broker_id").val('');
			}}, 
		focus: function (event, ui){
			return false;}
		});});		
		
function MyOpt(elem)
{
	if(elem=='BROKER')
	{
		$('#broker_div').show();
		$('#broker_name').attr('required',true);
		$('#owner_div').hide();
		$('#truck_no').attr('required',false);
	}
	else if(elem=='TRUCK')
	{
		$('#broker_div').hide();
		$('#broker_name').attr('required',false);
		$('#owner_div').show();
		$('#truck_no').attr('required',true);
	}
	else
	{
		$('#broker_div').hide();
		$('#broker_name').attr('required',false);
		$('#owner_div').hide();
		$('#truck_no').attr('required',false);
	}
}		
</script>
		
	<div class="form-group col-md-12">
		<label>Broker/Vehicle No Wise <font color="red">*</font></label>
		<select class="form-control" onchange="MyOpt(this.value)" name="type1" required="required">
			<option value="">Select option</option>
			<option value="BROKER">Broker Name</option>
			<option value="TRUCK">Vehicle No.</option>
		</select>
	</div>
	
	<div class="form-group col-md-12" id="broker_div" style="display:none">
		<label>Broker Name <font color="red">*</font></label>
		<input name="broker_name" oninput="this.value=this.value.replace(/[^a-z.- A-Z0-9]/,'')" id="broker_name" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-12" id="owner_div" style="display:none">
		<label>Vehicle Number <font color="red">*</font></label>
		<input name="truck_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="truck_no" type="text" class="form-control" required />
    </div>
	
	<input type="hidden" id="broker_id" name="broker_id">
	<input type="hidden" id="owner_id" name="owner_id">
	
	<div class="form-group col-md-12">
		<label>From Date <font color="red">*</font></label>
		<input name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">
		<label>To Date <font color="red">*</font></label>
		<input name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="View Rtgs/Neft" />
	</div>
</div>

</div>
</div>
</div>
</form>

</body>
</html>