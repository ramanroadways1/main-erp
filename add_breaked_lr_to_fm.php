<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$actual_wt = escapeString($conn,strtoupper($_POST['actual_wt']));
$charge_wt = escapeString($conn,strtoupper($_POST['charge_wt']));
$break_id = escapeString($conn,strtoupper($_POST['breaking_id']));

$from_loc = escapeString($conn,strtoupper($_POST['from']));
$to_loc = escapeString($conn,strtoupper($_POST['to']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));
$from_id = escapeString($conn,strtoupper($_POST['from_id']));
$page_name1 = escapeString($conn,($_POST['page_name1']));

if($page_name1=='smemo_own.php')
{
	$vou_no = $_SESSION['voucher_olr'];
	$veh_no = $_SESSION['voucher_olr_vehicle'];
}
else
{
	$vou_no = $_SESSION['freight_memo'];
	$veh_no = $_SESSION['freight_memo_vehicle'];
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$check_for_duplicate = Qry($conn,"SELECT id FROM lr_pre WHERE frno='$vou_no' AND lrno='$lrno' AND brk_id='$break_id'");
if(!$check_for_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_for_duplicate)>0){
	echo "<script>
		alert('You have already added this LR.');
		$('#break_lr_add_btn').attr('disabled',false);
	</script>";
	HideLoadicon();
	exit();
}

$check_lr = Qry($conn,"SELECT id,company,date,truck_no,consignor,consignee,con1_id,con2_id,date(timestamp) as date_created 
FROM lr_sample WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found.');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$row_lr = fetchArray($check_lr);

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_lr['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

if($diff_days>60)
{
	echo "<script>
		alert('Error : LR exceed 60 days.');
		window.location.href='./$page_name1';
	</script>";
	HideLoadicon();
	exit();
}

if($row_lr['truck_no']!=$veh_no)
{
	$lr_type = "1";
}
else
{
	$lr_type = "0";
}

$get_company = Qry($conn,"SELECT company FROM lr_pre WHERE frno='$vou_no'");

if(!$get_company){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_company)==0){
	$company = $row_lr['company'];
}
else
{
	$row_company = fetchArray($get_company);
	$company = $row_company['company'];
}

if($company=='')
{
	echo "<script>
		alert('Company not found.');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$insert_break_lr = Qry($conn,"INSERT INTO lr_pre(frno,company,branch,date,create_date,truck_no,lrno,lr_type,lr_id,fstation,tstation,
consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,crossing,brk_id,timestamp) VALUES ('$vou_no','$company','$branch',
'$row_lr[date]','$date','$veh_no','$lrno','$lr_type','$row_lr[id]','$from_loc','$to_loc',
'$row_lr[consignor]','$row_lr[consignee]','$from_id','$to_id','$row_lr[con1_id]','$row_lr[con2_id]','$actual_wt',
'$charge_wt','NO','$break_id','$timestamp')");

if(!$insert_break_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

// $update_breaking_status = Qry($conn,"UPDATE lr_break SET crossing='NO' WHERE id='$break_id'");
// if(!$update_breaking_status){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

echo "<script>
		alert('LR Number : $lrno. Successfully added !');
		window.location.href='./$page_name1';
	</script>";
exit();
?>