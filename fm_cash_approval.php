<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CashApprovalForm").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./fm_cash_approval_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_function1").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-5 col-md-offset-4">

<form autocomplete="off" id="CashApprovalForm">	

<div class="row">
	
<script>	
function GetFm(lrno)
{
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
				url: "get_fm_number.php",
				data: 'lrno=' + lrno,
				type: "POST",
				success: function(data) {
				$("#fm_no").html(data);
			},
			error: function() {}
		});
	}
}
</script>

<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Request : Cash approval in FM
		<br />
		<br />
		फ्रेट मेमो में नकद के लिए अनुरोध करे 
		</h4></center>
		<br />
	</div>
	
	<div class="form-group col-md-6">
		<label>LR Number <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="lrno" onblur="GetFm(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-6">
		<label>Select FM Number</label> 
		<select style="font-size:12px;height:32px;" id="fm_no" name="fm_no" required="required" class="form-control">
			<option style="font-size:12px" value="">--select fm--</option>
		</select>
    </div>
	
	<div class="form-group col-md-6">
		<label>Advance / Balance</label> 
		<select style="font-size:12px;height:32px;" id="adv_bal" name="adv_bal" required="required" class="form-control">
			<option style="font-size:12px" value="">--select type--</option>
			<option style="font-size:12px" value="1">Advance Cash</option>
			<option style="font-size:12px" value="2">Balance Cash</option>
		</select>
    </div>
	
	<div class="form-group col-md-6">
		<label>Cash Amount <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="amount" name="amount" type="number" min="0" class="form-control" required />
    </div>
	
	<div class="form-group col-md-12">
		<label>Narration <font color="red"><sup>*</sup></font></label>
		<textarea style="font-size:12px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-/,.]/,'')" required="required" id="narration" name="narration" class="form-control"></textarea>
    </div>

	<div class="form-group col-md-12">
		<input id="button_sub" disabled type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" name="submit" value="Submit Request" />
	</div>
	
</form>	
	<div id="result_function1"></div>
</div>

</div>

</div>
</div>
</body>
</html>