<div id="loadicon_ewb_val" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>

<button id="EwbValModalBtn" data-toggle="modal" data-target="#EwbValExtendModal" style="display:none"></button>

<form style="font-size:12px;color:#000" id="EwbValExtendForm" action="#" method="POST">
<div id="EwbValExtendModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div style="font-size:14px;" class="modal-header bg-primary">
		Extend eway-bill validity
      </div>
      <div class="modal-body">
        <div class="row">
		
<script>		
function EwbValExtSearchBy(elem)
{
	$('#modal_ewb_ewb_no').val('');
	$('#modal_ewb_lrno').val('');
		
	if(elem=='LR')
	{
		$('#modal_ext_val_ewb_div').hide();
		$('#modal_ext_val_lr_div').show();
		$('#modal_ewb_ewb_no').attr('required',false);
		$('#modal_ewb_lrno').attr('required',true);
	}
	else if(elem=='EWB')
	{
		$('#modal_ext_val_ewb_div').show();
		$('#modal_ext_val_lr_div').hide();
		$('#modal_ewb_ewb_no').attr('required',true);
		$('#modal_ewb_lrno').attr('required',false);
	}
	else
	{
		$('#modal_ext_val_ewb_div').hide();
		$('#modal_ext_val_lr_div').hide();
		$('#modal_ewb_ewb_no').attr('required',true);
		$('#modal_ewb_lrno').attr('required',true);
	}
}

function FetchEwbLRdetails()
{
	var search_by = $('#modal_ewb_val_search_by').val();
	
	if(search_by=='')
	{
		$('#modal_ewb_val_search_by').focus();
	}
	else
	{
		if(search_by=='EWB'){
			var vou_no = $('#modal_ewb_ewb_no').val();
		}
		else if(search_by=='LR'){
			var vou_no = $('#modal_ewb_lrno').val();
		} 
		else {
			window.location.href='./';
		}
		
		if(vou_no=='')
		{
			if(search_by=='LR'){
				$('#modal_ewb_lrno').focus();
			}
			else{
				$('#modal_ewb_ewb_no').focus();
			}
		}
		else
		{
			$('#fetch_btn_ewb_ext_modal').attr('disabled',true);
			$("#loadicon_ewb_val").show();
			jQuery.ajax({
			url: "./fetch_lr_ewb_data.php",
			data: 'search_by=' + search_by + '&vou_no=' + vou_no,
			type: "POST",
			success: function(data) {
				$("#result_Ewb_Extend_Form").html(data);
			},
			error: function() {}
			});
		}
	}
}
</script>		
		
			<div class="form-group col-md-4">
				<label style="color:#000">Search by <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12.5px;border:.5px solid #ddd" onchange="EwbValExtSearchBy(this.value)" name="search_by" id="modal_ewb_val_search_by" class="form-control" required="required">
					<option class="ewb_search_by_sel" style="font-size:12.5px" value="">--select--</option>
					<option id="ewb_search_by_LR" class="ewb_search_by_sel" style="font-size:12.5px" value="LR">LR number</option>
					<option id="ewb_search_by_EWB" class="ewb_search_by_sel" style="font-size:12.5px" value="EWB">Ewb number</option>
				</select>
			</div>
			
			<div class="form-group col-md-4" id="modal_ext_val_ewb_div" style="display:none">
				<label style="color:#000">Ewb number <font color="red"><sup>*</sup></font></label>
				<input maxlength="12" style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="ewb_no" id="modal_ewb_ewb_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4" id="modal_ext_val_lr_div" style="display:none1">
				<label style="color:#000">LR number <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" id="modal_ewb_lrno" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="fetch_btn_ewb_ext_modal" onclick="FetchEwbLRdetails()" class="btn btn-primary btn-sm">Fetch details</button>
			</div>
			
			<div class="form-group col-md-12">
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">From Location </label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" readonly id="modal_ewb_from" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">To Location <sup style="color:maroon;font-size:11px" id="ewb_ext_modal_dest_pincode"></sup></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" readonly id="modal_ewb_to" class="form-control">
			</div>
			 
			<div class="form-group col-md-4">
				<label style="color:#000">Vehicle number </label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" readonly id="modal_ewb_truck_no" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">Extension reason <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12.5px;border:.5px solid #ddd" name="reason" class="form-control" required="required">
					<option style="font-size:12.5px" value="">--select--</option>
					<option disabled style="font-size:12.5px" value="1">Natural Calamity</option>
					<option disabled style="font-size:12.5px" value="2">Law & Order</option>
					<option disabled style="font-size:12.5px" value="4">Transhipment</option>
					<option disabled style="font-size:12.5px" value="5">Accident</option>
					<option style="font-size:12.5px" value="99">Others</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">EWB number </label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" readonly id="modal_ewb_ewb_no_ip" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">Extension Remark <font color="red"><sup>*</sup></font></label>
				<textarea style="font-size:12.5px;border:.5px solid #ddd" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" name="remark" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group bg-primary col-md-12" style="color:maroon;padding:5px;font-size:14px">
				** Current place details ** 
			</div>
			
			<div class="form-group col-md-3">
				<label style="color:#000">Current place <font color="red"><sup>*</sup></font></label>
				<input placeholder="current location" style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="current_place" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3">
				<label style="color:#000">Pincode <font color="red"><sup>*</sup></font></label>
				<input id="modal_ewb_ext_curr_loc_pincode" placeholder="current pincode" maxlength="6" style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^0-9,]/,'')" name="pincode" class="form-control" required="required">
			</div>

<script>		
function CalcDistancePincode()
{
	if($('#modal_ewb_truck_no').val()=='')
	{
		$('#result_Ewb_Extend_Form').html('<span style=/"color:red;font-size:13px/">Enter LR number or eway-bill number first !</span>');
	}
	else
	{
		var pincode = $('#modal_ewb_ext_curr_loc_pincode').val();
		
			$('#ext_val_ewb_btn_save').attr('disabled',true);
			$('#ewb_extd_verify_btn').attr('disabled',true);
			$("#loadicon_ewb_val").show(); 
			jQuery.ajax({
			url: "./fetch_distance_bw_pincode.php",
			data: 'pincode=' + pincode,
			type: "POST",
			success: function(data) {
				$("#result_Ewb_Extend_Form").html(data);
			},
			error: function() {}
			});
	}
}
</script>		
		
			<div class="form-group col-md-3">
				<label style="color:#000">State <font color="red"><sup>*</sup></font></label>
				<select style="font-size:12.5px;border:.5px solid #ddd" id="modal_ewb_ext_state" name="state" class="form-control" required="required">
					<option style="font-size:12.5px" value="">--select--</option>
					<?php
					$qry_get_current_states = Qry($conn,"SELECT name,code FROM state_codes ORDER BY name ASC");
					
					if(numRows($qry_get_current_states)>0)
					{
						while($row_curr_states = fetchArray($qry_get_current_states))
						{
							echo "<option style='font-size:12.5px' value='$row_curr_states[code]'>$row_curr_states[name]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-3">
				<label style="color:#000">Remaining distance <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" name="distance" id="modal_ewb_ext_distance_remaining" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">Address line-1 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_1" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">Address line-2 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_2" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label style="color:#000">Address line-3 <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px;border:.5px solid #ddd" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" name="addr_3" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<button disabled type="button" id="ewb_extd_verify_btn" onclick="CalcDistancePincode()" class="btn btn-sm btn-primary">Save details</button>
			</div>
			
			<div class="form-group col-md-8" id="result_Ewb_Extend_Form"></div>
			
		</div>
      </div>
	 
      <div class="modal-footer">
<?php
$fromTime_ewb_ext1 = DateTime::createFromFormat('H:i a', "18:00 pm");
$toTime_ewb_ext1 = DateTime::createFromFormat('H:i a', "23:40 pm");

if(DateTime::createFromFormat('H:i a', date("H:i a")) > $fromTime_ewb_ext1 && DateTime::createFromFormat('H:i a', date("H:i a")) < $toTime_ewb_ext1)		
{
?>
<button type="submit" disabled id="ext_val_ewb_btn_save" class="btn btn-sm btn-primary">Confirm Extension</button>
<?php
}
else
{
	echo "<center><span style='color:red;font-size:13px'>Extension Timing is: 06:00 PM to 11:30 PM !</span></center>";
}
?>
<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#EwbValExtendForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon_ewb_val").show();
	$("#ext_val_ewb_btn_save").attr("disabled", true);
	$.ajax({
        	url: "./save_ewb_extend_validity.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Ewb_Extend_Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>