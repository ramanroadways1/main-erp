<?php
require("./connection.php");

$pincode = escapeString($conn,($_POST['pincode']));
$google_addr = escapeString($conn,($_POST['google_addr']));
$google_lat = escapeString($conn,($_POST['google_lat']));
$google_lng = escapeString($conn,($_POST['google_lng']));
$to_lat_long = escapeString($conn,($_POST['to_lat_long']));
$to_id = escapeString($conn,($_POST['to_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));

if($to_id=='')
{
	echo "<script>alert('Location not found !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

if($con2_id=='')
{
	echo "<script>alert('Consignee not found !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

if($to_lat_long=='')
{
	echo "<script>alert('Location POI not found !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

if($google_lat=='')
{
	echo "<script>alert('Google location not found !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

if(strlen($pincode)!=6)
{
	echo "<script>alert('Invalid pincode !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

 
$origin = $to_lat_long;
$destination = $google_lat.",".$google_lng;
$api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=".$google_api_key."");
$data = json_decode($api);
			
$api_status = $data->rows[0]->elements[0]->status;
	
if($api_status=='NOT_FOUND')
{
	echo "<script>alert('Distance not found !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

if($api_status!='OK')
{
	echo "<script>alert('API Error: $api_status !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}
			
$dest_addr = $data->destination_addresses[0];
$origin_addr = $data->origin_addresses[0];
$distance_cal = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
$travel_time = $data->rows[0]->elements[0]->duration->text;
$travel_time_value = $data->rows[0]->elements[0]->duration->value;
$travel_hrs = gmdate("H", $travel_time_value);
$travel_minutes = gmdate("i", $travel_time_value);
$travel_seconds = gmdate("s", $travel_time_value);

$get_max_distance = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='LOCATION_MAX_DISTANCE_TO_POI' AND is_active='1'");

if(!$get_max_distance)
{
	echo "<script>alert('Error while processing request !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_max_distance)==0)
{
	$max_distance = "2";
}
else
{
	$row_max_d = fetchArray($get_max_distance);
	$max_distance = $row_max_d['func_value'];
}

if($distance_cal>$max_distance)
{
	echo "<script>alert('Invalid unloading point. Distance between location and unloading point is: $distance_cal KMs !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

$_SESSION['unloading_point_type1'] = "GOOGLE";
$_SESSION['unloading_point_distance_km1'] = $distance_cal;
$_SESSION['unloading_point_google_pincode'] = $pincode;
$_SESSION['unloading_point_google_addr'] = $google_addr;
$_SESSION['unloading_point_google_lat'] = $google_lat;
$_SESSION['unloading_point_google_lng'] = $google_lng;
$_SESSION['unloading_point_lat_lng'] = $to_lat_long;
$_SESSION['unloading_point_add_con1_id'] = $con2_id;
$_SESSION['unloading_point_add_from_id'] = $to_id;

$end_point = $google_lat.",".$google_lng; 
?>
<script type="text/javascript">
    function myfunction(){
        var map; 
        var start = new google.maps.LatLng(<?php echo $to_lat_long; ?>);
        var end = new google.maps.LatLng(<?php echo $end_point; ?>);
        var option ={
            zoom : 15,
            center : start,
			disableDefaultUI: true,
        };
        map = new google.maps.Map(document.getElementById('map_div'),option);
        var display = new google.maps.DirectionsRenderer();
        var services = new google.maps.DirectionsService();
        display.setMap(map);
            var request ={
                origin : start,
                destination:end,
                travelMode: 'DRIVING'
            };
            services.route(request,function(result,status){
                if(status =='OK'){
                    display.setDirections(result);
					 var directionsData = result.routes[0].legs[0]; // Get data about the mapped route
					if (!directionsData) {
					  alert('Request failed');
						$('#loadicon').fadeOut('slow');
						$('#button_sub').attr('disabled',true);
					}
					else {
					  var distance1 = directionsData.distance.text;
					  var distance_no = distance1.replace(/km/g,'');  // get only numeric value
					  var distance_no = distance_no.replace(/ /g,'');  // get only numeric value
						
					  var duration1 =  directionsData.duration.text;
					  $('#loadicon').fadeOut('slow');
					 $('#button_sub').attr('disabled',false);
					 $('#search_loading_point').attr('readonly',true);
					  $('#validate_btn_id').attr('disabled',true);
					  
				}
                }
				else{
					alert('Error: '+status);
					$('#loadicon').fadeOut('slow');
					$('#button_sub').attr('disabled',true);
				}
            });
}
myfunction();
</script>
 