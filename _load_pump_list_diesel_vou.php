<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$get_pump_list = Qry($conn,"SELECT name,code,comp FROM dairy.diesel_pump_own WHERE branch='$branch' AND active='1' ORDER by name ASC");
if(!$get_pump_list){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<option value=''>--select--</option>";

while($row = fetchArray($get_pump_list))
{
	echo "<option value='$row[code]_$row[comp]'>$row[name] - $row[comp]($row[code])</option>";
}
 

	echo "<script type='text/javascript'> 
		$('#pump_div').show();
		$('#qty_div').show();
		$('#rate_div').show();
		$('#bill_no_div').show();
		$('#pump_list').attr('required',true);
		$('#bill_no').attr('required',false);
		$('#qty').attr('required',true);
		$('#rate').attr('required',true);
		$('#dsl_amount').attr('oninput','AmountCall()');
		$('#pump_list').selectpicker('refresh');
		$('#loadicon').hide();
	</script>";
?>