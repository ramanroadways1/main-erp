<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$date_16year_ago = date('Y-m-d', strtotime('-15 years', strtotime(date("Y-m-d"))));
$min_date_join = date('Y-m-d', strtotime('-10 days', strtotime(date("Y-m-d"))));
$date_60_year_before = date('Y-m-d', strtotime('-45 years', strtotime($date_16year_ago)));
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
label{
	font-size:12.5px;
}
</style> 

<html>

<?php 
	include("./_header.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./employee_view.php"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary">
	<span class="glyphicon glyphicon-chevron-left"></span> Go Back</button></a>
<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-danger">Dashboard</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<form id="EmpForm" action="" method="post" autocomplete="off">

<div class="container-fluid"> 
    <div class="row">
		
<div class="form-group col-md-10 col-md-offset-1">
	
	<div class="row">	
		<div class="form-group col-md-4">
		   <h3><span class="fa fa-user-plus"></span> &nbsp; ADD NEW EMPLOYEE </h3> 
		</div>
	</div>
	
	<div class="form-group col-md-12 bg-primary" style="padding:4px;">Personal Information</div>

<div class="row">	
	
	<div class="form-group col-md-3">
		<label>Employee Photo <font color="red"><sup>*</sup></font></label>
		<input class="form-control" type="file" accept="image/*" name="emp_photo" required="required">
   </div>
      
   <div class="form-group col-md-3">
		<label>Full Name <font color="red"><sup>*</sup></font></label>
        <input type="text" id="fullname" name="fullname" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>

   <div class="form-group col-md-3">
		<label>Branch <font color="red"><sup>*</sup></font></label>
		<input type="text" class="form-control" style="cursor:pointer;" name="branch" readonly value="<?php echo $branch; ?>" required="required">
	</div>
    
   <div class="form-group col-md-3">
        <label>Date of birth <font color="red"><sup>*</sup></font></label> 
        <input max="<?php echo $date_16year_ago; ?>" min="<?php echo $date_60_year_before; ?>" type="date" class="form-control" id="birthdate" name="birthdate" required="required" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"> 
	</div>
     
   <div class="form-group col-md-3">
        <label>Age <font color="red"><sup>*</sup></font></label>
        <input type="number" id="age" name="age" class="form-control" required="required" readonly="readonly">
    </div>  

	<div class="form-group col-md-3">
         <label>Date of Joining <font color="red"><sup>*</sup></font></label>
         <input max="<?php echo $date; ?>" min="<?php echo $min_date_join; ?>" type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" name="joindate" required="required"> 
    </div>
	
   <div class="form-group col-md-3">
		<label>Father's Name <font color="red"><sup>*</sup></font></label>
       <input type="text" name="fathername" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
 
   <div class="form-group col-md-3">
       <label>Father's Occupation <font color="red"><sup>*</sup></font></label>
       <select name="fatherocc" onchange="Occupation(this.value)" class="form-control" style="cursor: pointer;" class="form-control" required="required">	
				<option value="">--Select option--</option>
				<option value="Farmer">Farmer</option>
				<option value="ShopOwner">Shop Owner</option>
				<option value="Businessman">Businessman</option>
				<option value="Govt">Govt. Employee</option>
				<option value="Pvt">Pvt. Sector Employee</option>
				<option value="Others">Others</option>
		</select>	
	</div>

<script>
function Occupation(elem)
{
	if(elem=='Others')
	{
		$('#occp_other').show();
		$('#fatherocc_other').attr('required',true);
	}
	else
	{
		$('#occp_other').hide();
		$('#fatherocc_other').attr('required',false);
	}
}
</script>

	<div class="form-group col-md-3" style="display:none" id="occp_other">
       <label>Write Father's Occupation <font color="red"><sup>*</sup></font></label>
       <input type="text" name="fatherocc_other" id="fatherocc_other" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
   
   <div class="form-group col-md-3">
       <label>Mother's Name <font color="red"><sup>*</sup></font></label>
       <input type="text" name="mothername" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>

<script>
function OccupationMother(elem)
{
	if(elem=='Others')
	{
		$('#occp_other_mother').show();
		$('#motherocc_other').attr('required',true);
	}
	else
	{
		$('#occp_other_mother').hide();
		$('#motherocc_other').attr('required',false);
	}
}
</script>

   <div class="form-group col-md-3">
       <label>Mother's Occupation <font color="red"><sup>*</sup></font></label>
       <select name="motherocc" onchange="OccupationMother(this.value)" class="form-control" style="cursor: pointer;" class="form-control" required="required">	
				<option value="">--Select option--</option>
				<option value="House-Wife">House Wife</option>
				<option value="Govt">Govt. Employee</option>
				<option value="Pvt">Pvt. Sector Employee</option>
				<option value="Others">Others</option>
		</select>	
	</div>  

	<div class="form-group col-md-3" style="display:none" id="occp_other_mother">
       <label>Write Mother's Occupation <font color="red"><sup>*</sup></font></label>
       <input type="text" name="motherocc_other" id="motherocc_other" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>	

   <div class="form-group col-md-3">
       <label>Aadhar Number <font color="red"><sup>*</sup></font></label>
       <input type="text" name="aadharno" class="form-control" maxlength="12" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required">
   </div> 

   <div class="form-group col-md-3">
       <label>Aadhar Card's Copy - Front <font color="red"><sup>*</sup></font></label>
       <input class="form-control" type="file" accept="image/*" name="aadhar_copy_front" required="required">
   </div>
   
   <div class="form-group col-md-3">
       <label>Aadhar Card's Copy - Rear<font color="red"><sup>*</sup></font></label>
       <input class="form-control" type="file" accept="image/*" name="aadhar_copy_rear" required="required">
   </div>

	<div class="form-group col-md-3">
        <label>PAN Number </label>
        <input type="text" maxlength="10" name="pan_no" id="pan_no" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateNewPan()">
    </div>

   <div class="form-group col-md-3">
       <label>PAN Card's Copy </label>
       <input class="form-control" type="file" accept="image/*" id="pan_copy" name="pan_copy">
   </div>

<!-- <div class="col-md-3">
  <div class="form-group">
    <label>SALARY</label>
    <select class="form-control" style="cursor: pointer;" id="selectsalary" required="required">
    <option value=""> --- select --- </option>
    <option value="LATER"> LATER </option>
    <option value="NOW"> NOW </option>
    </select> 
  </div>
</div>

      <div class="col-md-3">
        <div class="form-group">
          <label>AMOUNT</label>
          <input type="text" name="salary" id="salary" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')"  disabled="disabled">
        </div>
      </div> -->

    <div class="form-group col-md-3">
         <label>Pincode <font color="red"><sup>*</sup></font></label>
         <input type="text" name="pincode" maxlength="6" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required">
    </div>


     <div class="form-group col-md-3">
          <label>Nearest Police Station <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="police_station" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" >
      </div>

	 <div class="form-group col-md-3">
          <label>Qualification <font color="red"><sup>*</sup></font></label>
		  <select name="qualification" class="form-control" style="cursor: pointer;" class="form-control" required="required">	
				<option value="">--Select option--</option>
				<option value="NO">No Qualification</option>
				<option value="Under_10">Under 10th</option>
				<option value="10">10th</option>
				<option value="12">12th</option>
				<option value="Graduate">Graduate</option>
		  </select>		  
	 </div>

    <div class="form-group col-md-3">
          <label>Work Experience <font color="red"><sup>*</sup></font></label>
		  <select name="experience"  style="cursor: pointer;" class="form-control" required="required">	
				<option value="">--Select option--</option>
				<option value="NO">No Experience</option>
				<option value="1">1 Year</option>
				<option value="2">2 Years</option>
				<option value="2-5">2-5 Years</option>
				<option value="5+">More than 5Yrs</option>
		</select>
    </div>

	<div class="form-group col-md-3">
        <label>Blood Group <font color="red"><sup>*</sup></font></label>
		<select class="form-control" style="cursor: pointer;" name="bloodgroup" required="required">
			<option value=""> --- select --- </option>
			<option value="NO">Don't Know</option>
			<option value="O+"> O+ </option>
			<option value="O−"> O− </option>
			<option value="A−"> A− </option>
			<option value="A+"> A+ </option>
			<option value="B−"> B− </option>
			<option value="B+"> B+ </option>
			<option value="AB−"> AB− </option>
			<option value="AB+"> AB+ </option>
		</select> 
    </div>

<script>
function SendOTP()
{
	$('#button_add').attr('disabled',true);
	$('#send_otp_button').hide();
	$('#fullname').attr('readonly',true);
	$('#mobileno').attr('readonly',true);
	
	var mobile = $('#mobileno').val();
	var name = $('#fullname').val();
	
	if(mobile!='' && name!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./_send_otp_verify_mobile_add_employee.php",
			data: 'mobile=' + mobile + '&name=' + name,
			type: "POST",
			success: function(data){
				$("#result_emp").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		alert('Enter Mobile Number and Your Name First !');
	
		$('#fullname').attr('readonly',false);
		$('#mobileno').attr('readonly',false);
		$('#send_otp_button').show();
	}
}
</script>

    <div class="form-group col-md-3">
         <label>Mobile No. <sup><a id="send_otp_button" onclick="SendOTP()" style="font-family:Verdana;cursor:pointer;font-size:11">Send OTP</a> <font color="red">*</font></sup></label>
         <input type="text" name="mobileno" id="mobileno" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required" maxlength="10">
    </div>
	
	<div class="form-group col-md-3">
         <label>OTP <sup><font color="red">*</font></sup></label>
         <input type="text" name="otp" id="otp" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required" maxlength="6">
    </div>

     <div class="form-group col-md-3">
          <label>Alternate Mobile No. </label>
          <input type="text" name="alternateno" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')"  maxlength="10">
     </div>

   <div class="form-group col-md-3">
        <label>Email Id </label>
        <input type="email" name="emailid" class="form-control" oninput="this.value=this.value.replace(/[^a-z-A-Z0-9-_@#.]/,'')">
   </div>

	<div class="form-group col-md-3">
		<label>Emergency Mobile No. <font color="red"><sup>*</sup></font></label>
         <input type="text" name="emergencymo" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required" maxlength="10">
    </div>

	<div class="form-group col-md-3">
          <label>Languages Known <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="language" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z,]/,'')">
    </div>

	<div class="form-group col-md-3">
		<label>Any Major Diseases ? <font color="red"><sup>*</sup></font></label>
        <input type="text" name="dieases" required="required" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')">
    </div>
	
	<div class="form-group col-md-3">
        <label>Permanent Address <font color="red"><sup>*</sup></font></label>
        <textarea name="residence" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" required="required"></textarea>
   </div>

   <div class="form-group col-md-3">
        <label>Current Address <font color="red"><sup>*</sup></font></label>
        <textarea name="currentaddr" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" required="required"></textarea>
    </div>

</div>

	<div class="form-group col-md-12 bg-primary" style="padding:4px;">Maritial Information</div>
	
<div class="row">
	
<script>
function MaritialStatus(elem)
{
  if(elem=='MARRIED')
  {
      $("#wife_name").prop('readonly',false);  
      $("#wife_occ").prop('readonly',false);  
      $('#no_of_child').attr("disabled", false); 

      $("#wife_name").prop('required',true);  
      $("#wife_occ").prop('required',true);  
      $('#no_of_child').attr("required", true);  

  }
  else
  {
      $("#wife_name").prop('readonly',true);  
      $("#wife_occ").prop('readonly',true);  
      $('#no_of_child').attr("disabled", true);  
 
      $("#wife_name").prop('required',false);  
      $("#wife_occ").prop('required',false);  
      $('#no_of_child').attr("required", false);  

	}  
}  
</script>	

    <div class="form-group col-md-3">
          <label>Maritial Staus <font color="red"><sup>*</sup></font></label>
			<select id="maritial" class="form-control" onchange="MaritialStatus(this.value)" style="cursor: pointer;" name="maritial" required="required">
				<option value=""> --- select --- </option>
				<option value="MARRIED"> MARRIED </option>
				<option value="UNMARRIED"> UNMARRIED </option> 
			</select> 
     </div>
     
	<div class="form-group col-md-3">
          <label>Wife's Name <font color="red"><sup>*</sup></font></label>
          <input type="text" id="wife_name" required="required" name="wifename" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" readonly="readonly">
     </div>
     
	<div class="form-group col-md-3">
          <label>Wife's Occupation <font color="red"><sup>*</sup></font></label>
          <input type="text" id="wife_occ" name="wifeocc" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z]/,'')"  readonly="readonly">
    </div>
    
	<div class="form-group col-md-3">
		<label>No of Childrens <font color="red"><sup>*</sup></font></label>
		<select class="form-control" id="no_of_child" required="required" style="cursor: pointer;" name="child" disabled="">
		  <option value="">-- select --</option>
		  <option value="0"> 0 </option>
		  <option value="1"> 1 </option>
		  <option value="2"> 2 </option>
		  <option value="3"> 3 </option>
		  <option value="4"> 4 </option>
		  <option value="5"> 5 </option>
		  <option value="6"> 6 </option>
		  <option value="7"> 7 </option>
		  <option value="8"> 8 </option> 
		</select> 
    </div>

</div>

<script>
function Rule_BankAc()
{
	$("#acc_fullname").val('');
	  $("#acc_number").val('');
	  $("#acc_ifsc").val('');
	  $("#acc_bank").val('');
	  
	if($('#bank_checkbox').is(":checked")){
       
	   $("#acc_fullname").attr('readonly',false);
	   $("#acc_number").attr('readonly',false);
	   $("#acc_ifsc").attr('readonly',false);
	   $("#acc_bank").attr('readonly',false);
	   
	   $("#acc_fullname").attr('required',true);
	   $("#acc_number").attr('required',true);
	   $("#acc_ifsc").attr('required',true);
	   $("#acc_bank").attr('required',true);
    }
	else{
       $("#acc_fullname").attr('readonly',true);
	   $("#acc_number").attr('readonly',true);
	   $("#acc_ifsc").attr('readonly',true);
	   $("#acc_bank").attr('readonly',true);
	   
	   $("#acc_fullname").attr('required',false);
	   $("#acc_number").attr('required',false);
	   $("#acc_ifsc").attr('required',false);
	   $("#acc_bank").attr('required',false);
    }
}
</script>

	<div class="form-group col-md-12 bg-primary" style="padding:4px;">Bank A/c Information &nbsp; 
	<input id="bank_checkbox" checked="checked" name="bank_checkbox" type="checkbox" onchange="Rule_BankAc()" /></div>
	
<div class="row">

	<div class="form-group col-md-4">
          <label>A/c Holder (as per bank) <font color="red"><sup>*</sup></font></label>
          <input type="text" id="acc_fullname" name="acc_fullname" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
     </div>
     
     <div class="form-group col-md-3">
          <label>Account Number <font color="red"><sup>*</sup></font></label>
          <input type="text" id="acc_number" name="acc_number" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" required="required">
    </div>

    <div class="form-group col-md-2">
          <label>IFSC Code <font color="red"><sup>*</sup></font></label>
          <input type="text" name="acc_ifsc" maxlength="11" id="acc_ifsc" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" required="required">
    </div>

    <div class="form-group col-md-3">
          <label>Bank Name <font color="red"><sup>*</sup></font></label>
          <input id="acc_bank" type="text" name="acc_bank" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
   
</div>

	<div class="form-group col-md-12 bg-primary" style="padding:4px;">Reference/Guarantor Information</div>
	
<div class="row">

    <div class="form-group col-md-4">
          <label>Reference Person's Name <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="gaurantor_name" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')">
    </div>

	<div class="form-group col-md-4">
          <label>Mobile Number <font color="red"><sup>*</sup></font></label> 
          <input type="text" required="required" name="gaurantor_mobile" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="10">
	</div>

   <div class="form-group col-md-4">
        <label>Address <font color="red"><sup>*</sup></font></label>
        <textarea name="gaurantor_address" required="required" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,-0-9]/,'')"></textarea>
    </div>

</div>

	<div class="form-group col-md-12 bg-primary" style="padding:4px;">Relative in company ?</div>
	
<div class="row">

<!--       <div class="col-md-4">
        <div class="form-group">
          <label> RELATIVE IN COMPANY (NAME) </label>
          <input type="text" name="relative_name" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')" >
        </div>
      </div>
 -->
    <div class="form-group col-md-4">
		<label>Any Relative in Company ? <font color="red"><sup>*</sup></font></label>
		<select class="form-control" style="cursor: pointer;" name="relative_name" id="relativeid"  required="required" onchange="FetchRelative(this.value)">
			<option value="">--Select Employee--</option>
			<option value="NO" style="color: red;">NO RELATIVE</option>
			<?php 
			$get_emp_name = Qry($conn,"SELECT name,branch,code,mobile_no,currentaddr FROM emp_attendance WHERE status='3' ORDER BY name ASC");
			if(numRows($get_emp_name)>0)
			{
				while($row_emp=fetchArray($get_emp_name))
				{
					echo "<option value=".$row_emp['code'].">".$row_emp['name']." (".$row_emp['code'].") - ".$row_emp['branch']."</option>";
				}
			}
			?>  
		</select>
    </div>
   
<script>   
function FetchRelative(code)
{
	// alert(code);
	if(code!='NO' && code!='')
	{
		$("#loadicon").show();
		$.ajax({
		url: "fetch_relative.php",
		method: "post",
		data:'code=' + code,
		success: function(data){
			$("#result_emp").html(data);	
		}})
	}
}
</script>   
     
	<div class="form-group col-md-3">
		<label>Relative's Mobile No. <font color="red"><sup>*</sup></font></label> 
        <input type="text" id="relative_mobile" name="relative_mobile" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" readonly="readonly">
	</div>

    
	<div class="form-group col-md-5">
         <label>Relative's Address <font color="red"><sup>*</sup></font></label>
         <textarea type="text" id="relative_address" name="relative_address" class="form-control" readonly="readonly"></textarea>
    </div>
	
</div>

<div class="row">
	<br />
	<br />
	<br />
	<div class="col-md-12" style="text-align: right;"> 
		<button type="submit" disabled name="submit" id="button_add" class="btn btn-lg btn-success">ADD NEW EMPLOYEE</span></button>
	</div> 
	
</div> 

 </div>
</div>
</div>
</form>
  <!-- End of Main Content -->

<script type="text/javascript">
$(document).ready(function (e) {
	$("#EmpForm").on('submit',(function(e) {
		$('#button_add').attr('disabled',true);
		$("#loadicon").show();
		e.preventDefault();
		$.ajax({
        	url: "./save_emp_add_new.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_emp").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});


$('#birthdate').change(function() {
dob = new Date($('#birthdate').val());
var today = new Date();
var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
$('#age').val(age);
});
</script>

<script>
function ValidateNewPan() { 
  var Obj = document.getElementById("pan_no");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
      document.getElementById("pan_no").setAttribute("style","background-color:red;color:#fff;");
	document.getElementById("button_add").setAttribute("disabled",true);
    Obj.focus();
                return false;
            }
          else
            {
              document.getElementById("pan_no").setAttribute("style","background-color:green;color:#fff;");
			  document.getElementById("button_add").removeAttribute("disabled");
          }
        }
		else
		{
			document.getElementById("button_add").removeAttribute("disabled");
		}
  } 			
</script>

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("acc_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("acc_ifsc").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("button_add").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("acc_ifsc").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("button_add").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("acc_ifsc").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("button_add").removeAttribute("disabled");
		}
 } 			
</script>

<div id="result_emp"></div>  