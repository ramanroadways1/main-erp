<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$narration = escapeString($conn,($_POST['narration']));

$chk_record = Qry($conn,"SELECT date,branch FROM lr_sample WHERE lrno='$lrno'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./lr_date_exceed.php");
	exit(); 
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('Invalid LR entered !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$row = fetchArray($chk_record);

$lr_date = $row['date'];
$lr_branch = $row['branch'];

$insert_req = Qry($conn,"INSERT INTO allow_lr_exceed_validity(lrno,lr_date,lr_branch,narration,branch,branch_user,is_pending,timestamp) VALUES 
('$lrno','$lr_date','$lr_branch','$narration','$branch','$branch_sub_user','0','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./lr_date_exceed.php';
	</script>";
	exit();
?>