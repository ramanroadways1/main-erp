<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
  font-size:12px !important;
}

label{
	font-size:12px !important;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<body style="font-family: 'Open Sans', sans-serif !important">

<div class="container-fluid"> 
	
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
		</div>			
	</div>			
	
	<div class="row">	
	<div class="col-md-12">
		<div class="form-group col-md-1 col-md-offset-8 col-xs-6">
			<a href="#"><button data-toggle="modal" data-target="#view_assetReq" style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-danger">
			<span class="fa fa-exclamation-triangle"></span> Pending REQ. (<span id="pending_Staus"></span>)</button></a>
		</div>

		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button data-toggle="modal" data-target="#AssetReqModal" style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success">
			<span class="fa fa-plus-circle"></span> New Asset REQ.</button></a>
		</div>		
		
		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success" data-toggle="modal" data-target="#AssetAddnewReq">
			<span class="fa fa-plus-circle"></span> Add New Asset</button></a>
		</div>
		
		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success" data-toggle="modal" data-target="#AddOldAsset">
			<span class="fa fa-plus-circle"></span> Add Old Asset</button></a>
		</div>
	</div>
</div>

<div class="form-group col-md-12">

	<div class="row">	
		<div class="form-group col-md-4">
			<h4><span class="fa fa-mobile"></span> &nbsp; Assets : <font color="blue"><?php echo $branch; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Asset_Code</th>
					<th>Asset_Category</th>
					<th>Manufacturer & Model</th>
					<th>General/specific</th>
					<th>Asset_Holder</th>
					<th>Lost/Scrap</th>
				</tr>	
				
				<tr>
					<td colspan="7">--</td>
				</tr>	
<?php
// $GetAssets = Qry($conn,"SELECT a.id,a.req_code,a.typeof,a.holder,c.title as cat_name,a.asset_company,a.asset_model,e.name as holder_name1 
// FROM asset_main AS a 
// LEFT OUTER JOIN asset_category AS c ON c.id=a.category 
// LEFT OUTER JOIN emp_attendance AS e ON e.code=a.holder 
// WHERE a.branch='$branch' AND a.lost_scrap='0'");

// if(!$GetAssets){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// if(numRows($GetAssets)>0)
// {
	// $sn=1;
	
	// while($row = fetchArray($GetAssets))
	// {
		// if($row['holder']==$branch || $row['holder']=='' || $row['holder']=='NULL'){
			// $holder_name = $branch;
		// }
		// else{
			// $holder_name = $row['holder_name1'];
		// }
		
	// echo "<tr>
			// <td>$sn</td>
			// <td>$row[req_code]</td>
			// <td>$row[cat_name]</td>
			// <td>$row[asset_company]<br>$row[asset_model]</td>
			// <td>$row[typeof]</td>
			// <td>$holder_name</td>
			// <td>
			// <div class='form-inline'>
			// <select class='form-control' style='width:120px;font-size:12px;height:30px;' id='lost_scrap$row[id]'>
				// <option value=''>--option--</option>
				// <option value='LOST'>Lost/Theft</option>
				// <option value='SCRAP'>Scrap</option>
			// </select>
// <button type='button' id='lost_scrap_button$row[id]' onclick='LostScrap($row[id])' 
// class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></a></div>
			// </td>
	// </tr>";
	// <td>$approve_text</td>	
	// $sn++;	
	// }
// }
// else
// {
	// echo "<tr><td colspan='14'>No records found.</td></tr>";
// }
			?>			
			</table>
		</div>
		
	</div>
</div>
</div>

<script>
function LostScrap(id)
{
	var option_value = $('#lost_scrap'+id).val();
	
	if(option_value!='')
	{
		if(confirm("Do you really want to mark lost/scrap this item ?") == true)
		{
			$('#lost_scrap_button'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_asset_lost_scrap.php",
			data: 'option_value=' + option_value + '&id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}
	}
	else
	{
		alert('Select Option First !');
	}
}
</script>

<div id="func_result"></div>

<?php
include("./asset_add_new.php");
include("./asset_old_assset.php");
?>

<form id="FormAssetReq" action="#" method="POST">
<div id="AssetReqModal" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Generate New Asset Request : ( नया एसेट खरीदने के लिए उपयोग करे। )
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-7">
				<label>Asset Catagory <font color="red"><sup>*</sup></font></label>
				<select class="form-control" required="required" name="asset_cat">
					<option value="">--Select option--</option>
					<?php
					$GetCat = Qry($conn,"SELECT id,title FROM asset_category ORDER BY title ASC");
					if(numRows($GetCat)>0)
					{
						while($rowCat = fetchArray($GetCat))
						{
							echo "<option value='$rowCat[id]_$rowCat[title]'>$rowCat[title]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Manufacturer Name <font color="red"><sup>*</sup></font></label>
				<textarea name="maker" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Model Name <font color="red"><sup>*</sup></font></label>
				<textarea name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" class="form-control" required="required"></textarea>
			</div>
		
	
<!--		
			<div class="form-group col-md-6">
				<label>Payment Mode <font color="red"><sup>*</sup></font></label>
				<select class="form-control" required="required" name="payment_mode">
					<option value="">--Select option--</option>
					<option value="CASH">Cash</option>
					<option value="CHEQUE">Cheque</option>
					<option value="NEFT">Neft/Rtgs</option>
				</select>
			</div>
	
		<div class="form-group col-md-12" id="rtgs_div" style="display:none">
			<div class="row">
				<div class="form-group col-md-6">
					<label>A/c Holder <font color="red"><sup>*</sup></font></label>
					<input type="text" name="ac_holder" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>A/c Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="ac_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Bank Name <font color="red"><sup>*</sup></font></label>
					<input type="text" name="bank_name" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>IFSC Code <font color="red"><sup>*</sup></font></label>
					<input type="text" maxlength="11" name="ifsc" id="ifsc2" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>PAN No. <font color="red"><sup>*</sup></font></label>
					<input type="text" maxlength="10" name="pan_no" id="pan_no2" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN()" class="form-control" required="required">
				</div>
			</div>
		</div>
-->		
			<div class="form-group col-md-12">
				<label>Narration <font color="red"><sup>*</sup></font></label>
				<textarea name="narration" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" class="form-control" required="required"></textarea>
			</div>
			
		</div>
      </div>
	  <div id="ReqFormResult"></div>
      <div class="modal-footer">
        <button type="submit" id="req_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormAssetReq").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#req_button").attr("disabled", true);
		$.ajax({
        	url: "./save_asset_request.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#ReqFormResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no2");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no2").setAttribute("style","background-color:red;color:#FFF");
				$("#req_button").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no2").setAttribute("style","background-color:green;color:#FFF");
				$("#req_button").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no2").setAttribute("style","background-color:white;");
			$("#req_button").attr("disabled", false);
		}
  }
</script>

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc2");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ifsc2").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("req_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ifsc2").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("req_button").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ifsc2").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("req_button").removeAttribute("disabled");
		}
 } 			
</script>

<div id="view_assetReq" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
			Pending Asset Requests : <?php echo $branch; ?>
      </div>
	  
  <div class="modal-body">
	  <div class="row">
			<div class="col-md-12 table-striped">
<?php
$GetAssetsReq = Qry($conn,"SELECT a.id,a.req_code,a.date,a.maker,a.model,a.approval,a.ho_approval,c.title as cat_name 
FROM asset_request AS a 
LEFT OUTER JOIN asset_category AS c ON c.id=a.category 
WHERE a.asset_added='0' AND a.branch='$branch'");

if(!$GetAssetsReq){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_pending_asset = numRows($GetAssetsReq);

echo "<script>
	$('#pending_Staus').html($total_pending_asset);
</script>";

if($total_pending_asset>0)
{
	echo "
		<table class='table table-bordered' style='color:#000;font-size:11.5px;'>
			<tr>
				<th>#</th>
				<th>Token_No</th>
				<th>Category_Name</th>
				<th>Maker & Model</th>
				<th>Status</th>
			</tr>";

	$sn_1=1;
				
	while($row_pending_asset = fetchArray($GetAssetsReq))
	{
		echo "<tr>
			<td>$sn_1</td>
			<td>$row_pending_asset[req_code]</td>
			<td>$row_pending_asset[cat_name]</td>
			<td>$row_pending_asset[maker]<br>$row_pending_asset[model]</td>
			<td>";
			if($row_pending_asset['approval']==0){
				echo "<b><font color='red'>Manager Approval Pending.</b></font>";
			}
			else if($row_pending_asset['ho_approval']==0){
				echo "<b><font color='red'>HO Approval Pending.</b></font>";
			}
			else{
				echo "<b><font color='green'>Ready to add.</b></font>";
			}
		echo "</td>
		</tr>";
		$sn_1++;
	}	
	echo "</table>";	
}
else
{
	echo "<center><b><font color='red'>No result found.</b></font></center>";
}
		?>
		</div>
      </div>
  </div>
  
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" style="color:#FFF;font-weight:bold;" class="btn btn-sm btn-danger">Close</button>
      </div>
    </div>

  </div>
</div>