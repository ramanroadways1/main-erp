<form id="CreditClaimCreditForm" action="#" method="POST">
<div id="CrClaimModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Credit Claim
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-4">
				<label>LR Type <font color="red">*</font></label>
				<select onchange="LRTypeClaim(this.value)" class="form-control" id="claim_lr_type" name="lr_type" required="required">
					<option class="claim_lr_type_sel" value="">--select lr type--</option>
					<option class="claim_lr_type_sel" id="claim_lr_type_MARKET" value="MARKET">Market LR</option>
					<option class="claim_lr_type_sel" id="claim_lr_type_OWN" value="OWN">Own LR</option>
				</select>
			</div>
			
			<script>
			function LRTypeClaim(elem)
			{
				$('#claim_lrno').val('');
				
				if(elem=='MARKET'){
					$('#lr_type_html').html('Market');
				}
				else{
					$('#lr_type_html').html('Own');
				}
			}
			
			function CheckClailLR()
			{
				var claim_lrno = $('#claim_lrno').val();
				var lr_type = $('#claim_lr_type').val();
				
				if(claim_lrno=='')
				{
					alert('Enter LR number first !!');
				}
				else
				{
					if(lr_type=='')
					{
						alert('Select LR Type first !!');
					}
					else
					{
						$("#loadicon").show();	
						 jQuery.ajax({
							 url: "./get_data_claim_lr_wise.php",
							data: 'lrno=' + claim_lrno + '&lr_type=' + lr_type,
							 type: "POST",
							 success: function(data) {
								$("#result_cr_claim_modal2").html(data);
							},
							 error: function() {}
						});
					}
				}
			}
			</script>
			
			<div class="form-group col-md-7 form-inline">
				<label><span id="lr_type_html">Own</span> LR number <font color="red">*</font></label>
				<input class="form-control" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');if($('#claim_lr_type').val()==''){ $('#claim_lrno').val('');$('#claim_lr_type').focus(); }" type="text" name="lrno" id="claim_lrno" required />
				<button onclick="CheckClailLR()" id="claim_check_btn" type="button" class="btn btn-sm btn-success">
				Check LR <span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button>
			</div>
			
			<div style="display:none" id="claim_lr_desc" class="form-group col-md-12">
				<span style="font-size:13px;color:#000" class="bg-primary form-control" id="claim_locations_tno">>> Trip: Aburoad to Jalore & Vehicle no: GJ18AX7902</span>
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input class="form-control" min="1" type="number" name="amount" required />
			</div>
			
			<input type="hidden" name="tno_1" id="tno_claim1" />
			<input type="hidden" name="from_loc" id="from_loc_claim1" />
			<input type="hidden" name="to_loc" id="to_loc_claim1" />
			
			<div class="form-group col-md-6">
				<label>Company <font color="red">*</font></label>
				<select class="form-control" name="company" required="required">
					<option value="">--select company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration <font color="red">*</font></label>
				<textarea name="narration" class="form-control" required></textarea>
			</div>
			
		</div>
      </div>
	  
	  <div id="result_cr_claim_modal2"></div>
	  
      <div class="modal-footer">
        <button type="submit" disabled id="cr_claim_cr_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#CreditClaimCreditForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#cr_claim_cr_button").attr("disabled", true);
	$.ajax({
        	url: "./save_cr_claim_cash.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_cr_claim_modal2").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>