<?php
session_start();
require_once("../_connect.php");

$aadhar_no = strtoupper($_POST['aadhar_no']);

if(strlen($aadhar_no)!=12)
{
	echo "<script>
		alert('Check Aadhar - number !');
		$('#market_driver_aadhar_validate_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$data = array(
"id_number"=>"$aadhar_no",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$aadhar_api_url/api/v1/aadhaar-v2/generate-otp",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$aadhar_api_token.''
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response, true);

if($result['status_code']!='200')
{
	$error_msg = htmlspecialchars($result['message']);
	$error_code = $result['status_code'];
	
	echo "<script>
		alert('Error : '+\"$error_code: $error_msg !\");
		$('#loadicon').fadeOut('slow');
		$('#market_driver_aadhar_validate_btn').attr('disabled',false);
	</script>";
	exit();
}

$client_id = $result['data']['client_id'];				 // id string
$otp_sent = $result['data']['otp_sent'];	 			// 1 or 0
$if_number = $result['data']['if_number']; 				// 1 or 0
$valid_aadhaar = $result['data']['valid_aadhaar']; 		// 1 or 0
$msg = $result['message']; 								// OTP Sent.
$message_code = $result['message_code']; 					// success
$is_success = $result['success'];					 // 1 or  0

if($msg=='OTP Sent.')
{
	$_SESSION['driver_add_aadhar_client_id'] = $client_id;
	$_SESSION['driver_add_aadhar_no'] = $aadhar_no;
	
	echo '<script>
		alert("OTP Sent on Aadhar registered mobile number !\nआधार के साथ पंजीकृत नंबर पर OTP भेजा गया .");
		$("#market_driver_aadhar_validate_btn").attr("disabled",true);
		$(".aadhar_no_div").hide();
		
		$(".aadhar_otp_div").show();
		$("#market_driver_aadhar_validate_otp_btn").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
else
{
	echo "<script>
		alert('Error while sending OTP : $msg !');
		$('#market_driver_aadhar_validate_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>