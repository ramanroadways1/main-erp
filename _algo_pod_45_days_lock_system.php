<?php
	
	$my_date1 = date("Y-m-d");	
	
	$check_45_days_pod = Qry($conn,"SELECT lr_45_days,remind_on FROM _pending_lr WHERE branch='$_SESSION[user]'");
	
	if(!$check_45_days_pod){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}

	if(numRows($check_45_days_pod)>0)
	{
		$row_45_days_pod = fetchArray($check_45_days_pod);
			
		if($row_45_days_pod['lr_45_days']>0 AND ($row_45_days_pod['remind_on']==0 || strtotime($row_45_days_pod['remind_on'])<strtotime($my_date1)))
		{
				$current_time = date("h:i a");
				$sunrise = "7:00 am";
				$sunset = "10:00 pm";
				
				$current_time = DateTime::createFromFormat('h:i a', $current_time);
				$time_from = DateTime::createFromFormat('h:i a', $sunrise);
				$time_to = DateTime::createFromFormat('h:i a', $sunset);
				
			if ($current_time > $time_from && $current_time < $time_to)
			{
				echo "<script>
					alert('Clear your 45 days older LRs first.');
					window.location.href='https://rrpl.online/b5aY6EZzK52NA8F/clear_45_days_lrs.php';
				</script>";
				exit();
			}
		}
	}
	///////////////// /POD 45 ALGO ENDS ///////////////////////
	
?>	