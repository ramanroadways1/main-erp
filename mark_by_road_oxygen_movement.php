<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$lrno = escapeString($conn,$_POST['lrno']);
$id = escapeString($conn,$_POST['id']);
$pincode = escapeString($conn,$_POST['pincode']);

echo "<script>$('#mark_button_$id').attr('disabled',true);$('#voucher_btn_$id').attr('disabled',true);</script>";

if(strlen($pincode)!=6)
{
	echo "<script>
		alert('Check pincode !');
		$('#loadicon').fadeOut('slow');
		$('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$check_record = Qry($conn,"SELECT o.frno,o.vou_status,o.lrno,o.trip_id,o.from_id,l.tstation,l.to_id 
FROM oxygen_olr AS o 
LEFT JOIN lr_sample AS l ON l.id = o.lr_id 
WHERE o.id='$id'");

if(!$check_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_record)==0)
{
	echo "<script>
		alert('No record found !');
		$('#loadicon').fadeOut('slow');
		$('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$row = fetchArray($check_record);

$vou_no = $row['frno'];
$from_id = $row['from_id'];
$to_id = $row['to_id'];
$to_loc = $row['tstation'];

if($row['lrno']!=$lrno)
{
	echo "<script>
		alert('Error : LR number not verified !');
		$('#loadicon').fadeOut('slow');
		$('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['vou_status']=='1')
{
	echo "<script>
		alert('Error : Voucher is not active !');
		$('#loadicon').fadeOut('slow');
		$('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['trip_id']!=0)
{
	$get_trip = Qry($conn,"SELECT tno FROM dairy.trip WHERE id='$row[trip_id]'");

	if(!$get_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_trip)==0)
	{
		echo "<script>
			alert('Error : Trip not found !');
			$('#loadicon').fadeOut('slow');
			$('#mark_button_$id').attr('disabled',false);
		</script>";
		exit();
	}
	
	$row_trip = fetchArray($get_trip);
	
	$get_nxt_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id>'$row[trip_id]' AND tno='$row_trip[tno]'");

	if(!$get_nxt_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_nxt_trip)>0)
	{
		echo "<script>
			alert('Error : Next Trip Created !');
			$('#loadicon').fadeOut('slow');
			$('#mark_button_$id').attr('disabled',false);
		</script>";
		exit();
	}
}

$get_last_record = Qry($conn,"SELECT count FROM oxygen_olr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");

if(!$get_last_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row2 = fetchArray($get_last_record);

if($row2['count']!='1')
{
	echo "<script>
		alert('Error : Second voucher created already !');
		$('#loadicon').fadeOut('slow');
		// $('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row2['count']>=3)
{
	echo "<script>
		alert('Error : Voucher closed already !');
		$('#loadicon').fadeOut('slow');
		// $('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}
	
StartCommit($conn);
$flag = true;

$mark_by_road = Qry($conn,"UPDATE oxygen_olr SET close_user='$_SESSION[user_code]',close_timestamp='$timestamp',vou_status='1',count='3',
running_trip='1' WHERE lrno='$lrno'");

if(!$mark_by_road){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($row['trip_id']!=0)
{
	$get_kms = Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc_id='$from_id' AND to_loc_id='$to_id'");
	
	if(!$get_kms){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_kms)>0)
	{
		$row_km = fetchArray($get_kms);
		
		$update_trip = Qry($conn,"UPDATE dairy.trip SET to_station='$to_loc',to_id='$to_id',km='$row_km[km]',pincode='$pincode' WHERE id='$row[trip_id]'");
	
		if(!$update_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_trip = Qry($conn,"UPDATE dairy.trip SET to_station='$to_loc',to_id='$to_id',pincode='$pincode' WHERE id='$row[trip_id]'");
	
		if(!$update_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$update_olr = Qry($conn,"UPDATE freight_form_lr SET tstation='$to_loc',to_id='$to_id' WHERE frno='$vou_no'");
	
if(!$update_olr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('OK : Success.');
		window.location.href='./oxygen_movement.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#mark_button_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>