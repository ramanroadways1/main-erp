<?php
require_once("./connection.php");

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$asset_type = escapeString($conn,strtoupper($_POST['asset_type']));
$id = escapeString($conn,strtoupper($_POST['id']));
$emp_code = escapeString($conn,strtoupper($_POST['emp_code']));
$vehicle_no = escapeString($conn,strtoupper($_POST['vehicle_no']));

$chk_ho_approval = Qry($conn,"SELECT ho_approval FROM asset_vehicle WHERE id='$id'");

if(!$chk_ho_approval){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		window.location.href='update_asset_type.php';
	</script>";
	exit();
}

if(numRows($chk_ho_approval)==0){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Asset not found !');
		window.location.href='update_asset_type.php';
	</script>";
	exit();
}

$row_asset = fetchArray($chk_ho_approval);

if($row_asset['ho_approval']==0)
{
	echo "<script>
		alert('HO Approval pending !');
		window.location.href='update_asset_type.php';
	</script>";
	exit();
}

if($asset_type=='SPECIFIC')
{
	$emp_code=$emp_code;
}
else
{
	$emp_code="";
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE asset_vehicle SET vehicle_holder='$emp_code',asset_type='$asset_type',active='1' WHERE 
id='$id' AND ho_approval='1'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$vehicle_no','Asset_type_update_branch','General_Specific','Updated to $asset_type.','$branch','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
			alert('Asset Type updated succesfully !');
			window.location.href='update_asset_type.php';
		</script>";
	
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./update_asset_type.php");
	exit();
}	
?>