<?php
require_once './connection.php';
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../../favicon.png" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
<link href="./google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="./data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

<?php
include("./_loadicon.php");
include("./disable_right_click_for_index.php");
?>

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Oxygen Movemenet Summary :</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>Vou_No</th>
					<th>Vehicle_No</th>
					<th>LR_Number</th>
					<th>From</th>
					<th>To</th>
					<th>Branch</th>
					<th>User</th>
					<th>Vou_Date</th>
					<th>Vou_Count</th>
					<th>LR_Destination</th>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>#</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function FetchLRs(){
$("#loadicon").show();
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=./load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	{ 
    "targets": 13, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	
	{ 
    "targets": 15, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	
	{ 
    "targets": 16, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 {
        targets: 11, // edit column order
        data: null,
        mRender: function(data, type, full){
			if(data[13]=="0"){				
				return '<input type="hidden" id="lr_dest_'+full[0]+'" value="'+full[10]+'"><input type="hidden" id="vou_count_'+full[0]+'" value="'+full[9]+'"><input type="hidden" id="lrno_input_'+full[0]+'" value="'+full[3]+'"><input type="hidden" id="from_loc_'+full[0]+'" value="'+full[5]+'"><input type="hidden" id="vou_no_'+full[0]+'" value="'+full[1]+'"><input type="hidden" id="veh_no_'+full[0]+'" value="'+full[2]+'"><button type="button" class="btn-xs btn-primary" onclick="CreateVoucher('+full[0]+')" id="voucher_btn_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create Vou.</button>';
			}	else{
				return "<span style='color:red'>TRIP Closed</span>";
			}
        }
      },
      {
		targets: 12, // edit column order
        data: null,
        mRender: function(data, type, full){
			if(data[13]=="0" && data[9]=="1"){	
				return '<input type="text" placeholder="Dest. Pincode" style="font-size:11px;width:100%;height:20px !important" oninput="this.value=this.value.replace(/[^0-9]/,\'\')" id="dest_pincode_'+full[0]+'" class="form-control" maxlength="6"><button type="button" class="btn-xs btn-danger" onclick="MarkByRoadBtn('+full[0]+')" id="mark_button_'+full[0]+'"><i class="fa fa-road" aria-hidden="true"></i> Mark By-Road Direct</button>';
			}	else{
				return "<span style='color:red'>TRIP Closed</span>";
			}
        }
      },
	  {
		targets: 14, // edit column order
        data: null,
        mRender: function(data, type, full){
         if(data[13]=="0"){		

				if(data[15]==data[16]){
					return '<input type="hidden" id="lr_dest_cross_'+full[0]+'" value="'+full[10]+'"><input type="hidden" id="vou_count_cross_'+full[0]+'" '+
					'value="'+full[9]+'"><input type="hidden" id="lrno_input_cross_'+full[0]+'" value="'+full[3]+'"><input type="hidden"'+
					'id="from_loc_cross_'+full[0]+'" value="'+full[5]+'"><input type="hidden" id="vou_no_cross_'+full[0]+'" value="'+full[1]+'">'+
					'<input type="hidden" id="veh_no_cross_'+full[0]+'" value="'+full[2]+'"><button type="button" class="btn-xs btn-primary" '+
					'onclick="CrossingVoucher('+full[0]+')" id="voucher_btn_cross_'+full[0]+'"><i class="fa fa-truck" aria-hidden="true"></i>'+
					' Crossing Vou.</button><br><br><button type="button" class="btn-xs btn-danger" '+
					'onclick="CloseVoucher('+full[0]+')" id="voucher_btn_close_'+full[0]+'"><i class="fa fa-window-close-o" aria-hidden="true"></i>'+
					' Close LR</button>';
				}
				else
				{
					return '<input type="hidden" id="lr_dest_cross_'+full[0]+'" value="'+full[10]+'"><input type="hidden" id="vou_count_cross_'+full[0]+'" '+
					'value="'+full[9]+'"><input type="hidden" id="lrno_input_cross_'+full[0]+'" value="'+full[3]+'"><input type="hidden"'+
					'id="from_loc_cross_'+full[0]+'" value="'+full[5]+'"><input type="hidden" id="vou_no_cross_'+full[0]+'" value="'+full[1]+'">'+
					'<input type="hidden" id="veh_no_cross_'+full[0]+'" value="'+full[2]+'"><button type="button" class="btn-xs btn-primary" '+
					'onclick="CrossingVoucher('+full[0]+')" id="voucher_btn_cross_'+full[0]+'"><i class="fa fa-truck" aria-hidden="true"></i>'+
					' Crossing Vou.</button>';
				}
			}	else{
				return "<span style='color:red'>TRIP Closed</span>";
			}
        }
      },
	    // {
		// targets: 15, // edit column order
        // data: null,
        // mRender: function(data, type, full){
         // if(data[13]=="0"){		

				// if(data[15]==data[16]){
					// return '<input type="hidden" id="lr_dest_cross_'+full[0]+'" value="'+full[10]+'"><input type="hidden" id="vou_count_cross_'+full[0]+'" '+
					// 'value="'+full[9]+'"><input type="hidden" id="lrno_input_cross_'+full[0]+'" value="'+full[3]+'"><input type="hidden"'+
					// 'id="from_loc_cross_'+full[0]+'" value="'+full[5]+'"><input type="hidden" id="vou_no_cross_'+full[0]+'" value="'+full[1]+'">'+
					// '<input type="hidden" id="veh_no_cross_'+full[0]+'" value="'+full[2]+'"><button type="button" class="btn-xs btn-primary" '+
					// 'onclick="CrossingVoucher('+full[0]+')" id="voucher_btn_cross_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>'+
					// ' Crossing Vou.</button><br><br><button type="button" class="btn-xs btn-danger" '+
					// 'onclick="CloseVoucher('+full[0]+')" id="voucher_btn_close_'+full[0]+'"><i class="fa fa-window-close-o" aria-hidden="true"></i>'+
					// ' Close LR</button>';
				// }
				// else
				// {
					// return '<input type="hidden" id="lr_dest_cross_'+full[0]+'" value="'+full[10]+'"><input type="hidden" id="vou_count_cross_'+full[0]+'" '+
					// 'value="'+full[9]+'"><input type="hidden" id="lrno_input_cross_'+full[0]+'" value="'+full[3]+'"><input type="hidden"'+
					// 'id="from_loc_cross_'+full[0]+'" value="'+full[5]+'"><input type="hidden" id="vou_no_cross_'+full[0]+'" value="'+full[1]+'">'+
					// '<input type="hidden" id="veh_no_cross_'+full[0]+'" value="'+full[2]+'"><button type="button" class="btn-xs btn-primary" '+
					// 'onclick="CrossingVoucher('+full[0]+')" id="voucher_btn_cross_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>'+
					// ' Crossing Vou.</button>';
				// }
			// }	else{
				// return "<span style='color:red'>TRIP Closed</span>";
			// }
        // }
      // }
		], 
        "serverSide": true,
        "ajax": "_load_oxygen_movement.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
function CreateVoucher(id)
{
	var lrno = $('#lrno_input_'+id).val();
	var from_loc = $('#from_loc_'+id).val();
	var veh_no = $('#veh_no_'+id).val();
	var vou_no = $('#vou_no_'+id).val();
	var lr_dest = $('#lr_dest_'+id).val();
	var vou_count = Number($('#vou_count_'+id).val())+1;
	
	$('#table_id').val(id);
	$('#modal_oxygent_count').html(vou_count);
	$('#modal_oxygent_trip_lrno_html').html(lrno);
	$('#modal_oxygent_trip_lrno').val(lrno);
	$('#modal_oxn_from_loc').val(from_loc);
	$('#modal_oxn_lr_loc').val(lr_dest);
	$('#modal_oxn_tno').val(veh_no);
	$('#modal_oxn_vou_no').val(vou_no);
	$('#open_oxygen_trip_modal')[0].click();
}

function CrossingVoucher(id)
{
	var lrno = $('#lrno_input_cross_'+id).val();
	var from_loc = $('#from_loc_cross_'+id).val();
	var veh_no = $('#veh_no_cross_'+id).val();
	var vou_no = $('#vou_no_cross_'+id).val();
	var lr_dest = $('#lr_dest_cross_'+id).val();
	var vou_count = Number($('#vou_count_cross_'+id).val())+1;
	
	$('#table_id_crossing').val(id);
	$('#modal_cross_oxygent_count').html(vou_count);
	$('#modal_cross_oxygent_trip_lrno_html').html(lrno);
	$('#modal_cross_oxygent_trip_lrno').val(lrno);
	$('#modal_cross_oxn_from_loc').val(from_loc);
	$('#modal_cross_oxn_lr_loc').val(lr_dest);
	$('#modal_cross_oxn_tno').val(veh_no);
	$('#modal_cross_oxn_vou_no').val(vou_no);
	$('#open_cross_oxygen_trip_modal')[0].click();
}

$(function() {
		$("#dest_location_oxygen_trip").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#dest_location_oxygen_trip').val(ui.item.value);   
           $('#dest_location_oxygen_trip_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#dest_location_oxygen_trip").val('');
			$("#dest_location_oxygen_trip").focus();
			$("#dest_location_oxygen_trip_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#dest_location_oxygen_cross_trip").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#dest_location_oxygen_cross_trip').val(ui.item.value);   
           $('#dest_location_oxygen_cross_trip_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#dest_location_oxygen_cross_trip").val('');
			$("#dest_location_oxygen_cross_trip").focus();
			$("#dest_location_oxygen_cross_trip_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#cross_modal_tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		select: function (event, ui) { 
           $('#cross_modal_tno').val(ui.item.value);   
           $('#cross_modal_wheeler').val(ui.item.wheeler);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#cross_modal_tno").val('');
			$("#cross_modal_tno").focus();
			$("#cross_modal_wheeler").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

function CloseVoucher(id)
{
	var lrno = $('#lrno_input_'+id).val();
	$('#voucher_btn_close_'+id).attr('disabled',true);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "./lr_close_oxygen_movement.php",
	data: 'id=' + id + '&lrno=' + lrno,
	type: "POST",
	success: function(data){
		$("#func_results").html(data);
	},
	error: function() {}
	});
}

function MarkByRoadBtn(id)
{
	var lrno = $('#lrno_input_'+id).val();
	var pincode = $('#dest_pincode_'+id).val();
	$('#mark_button_'+id).attr('disabled',true);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "./mark_by_road_oxygen_movement.php",
	data: 'id=' + id + '&lrno=' + lrno + '&pincode=' + pincode,
	type: "POST",
	success: function(data){
		$("#func_results").html(data);
	},
	error: function() {}
	});
}

FetchLRs();
</script>

<?php
include("modal_create_vou_oxygen_trip.php");
include("modal_create_crossin_vou_oxygen_trip.php");
?>

<div id="func_results"></div>