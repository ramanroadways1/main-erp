    var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = document.getElementById("textCanvas2"),
    signaturePad;

function resizeCanvas() {
var ratio =  Math.max(window.devicePixelRatio || 1, 1);
canvas.width = canvas.offsetWidth * ratio;
canvas.height = canvas.offsetHeight * ratio;
canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad2 = new SignaturePad(canvas);

clearButton.addEventListener("click", function (event) {
    signaturePad2.clear();
});

saveButton.addEventListener("click", function (event) {
    if (signaturePad2.isEmpty()) {
        alert("Please provide signature first.");
    } else {


    document.getElementById("myImg2").src = signaturePad2.toDataURL();
    var canvas2 = document.createElement('canvas');
    var context2 = canvas2.getContext('2d');
    var img = document.getElementById('myImg2');

    context2.drawImage(img, 0, 0 );
    var theData = canvas2.toDataURL();
    document.getElementById('sign2').value = theData;
    }
});
