// Add Record
function addRecord() {
    // get values
var fids = $("#fids").val();
var bid = $("#bid").val();
var oid = $("#oid").val();
var did = $("#search-box").val();
var lrno = $("#lrno").val();
var trkno = $("#search-box2").val();
var wt = $("#wt").val();
var wt12 = $("#wt12").val();
var rate = $("#rate").val();
var actual = $("#actual").val();
var load = $("#load").val();
var total = $("#total").val();
var advance = $("#advance").val();
var balance = $("#balance").val();
var newother = $("#newother").val();
var newtds = $("#newtds").val();
var shipno = $("#shipno").val();
var inno = $("#inno").val();

    // Add record
    $.post("ajax/addRecord.php", {
 fids: fids,
 bid: bid,
 oid: oid,
 did: did,
 lrno: lrno,
 trkno: trkno,
 wt: wt,
 wt12: wt12,
 rate: rate,
 actual: actual,
 load: load,
 total: total,
 advance: advance,
 balance: balance,
 newother : newother,
 newtds : newtds,
 shipno : shipno,
 inno : inno

    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readRecords();

        // clear fields from the popup
 $("#bid").val("");
 $("#oid").val("");
 $("#search-box").val("");
 $("#lrno").val("");
 $("#search-box2").val("");
 $("#wt").val("");
 $("#wt12").val("");
 $("#rate").val("");
 $("#actual").val("");
 $("#load").val("");
 $("#total").val("");
 $("#advance").val("");
 $("#balance").val("");
 $("#newother").val("");
 $("#newtds").val("");
 $("#shipno").val("");
 $("#inno").val("");

    });
}

// READ records
function readRecords() {

var fids = $("#fids").val();
$.ajax({
    data: 'fids=' + fids,
    url: 'ajax/readRecords.php',
    method: 'POST', // or GET
    success: function(readRecords) {
        $(".records_content").html(readRecords);
    }
});

}


function DeleteUser(id) {
    var conf = confirm("Are you sure, do you really want to delete User?");
    if (conf == true) {
        $.post("ajax/deleteUser.php", {
                id: id
            },
            function (data, status) {
                // reload Users by using readRecords();
                readRecords();
            }
        );
    }
}

function GetUserDetails(id) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_user_id").val(id);
    $.post("ajax/readUserDetails.php", {
            id: id
        },
        function (data, status) {
            // PARSE json data
            var user = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_lrno").val(user.lrno);
            $("#update_bid").val(user.fstation);
            $("#update_oid").val(user.tstation);
            $("#update_did").val(user.consignor);
            $("#update_trkno").val(user.consignee);
            $("#update_wt").val(user.weight);
            $("#update_rate").val(user.ratepmt);
            $("#update_actual").val(user.actualf);
            $("#update_load").val(user.loadd);
            $("#update_total").val(user.totalf);
            $("#update_advance").val(user.advamt);
            $("#update_balance").val(user.balamt);
            $("#update_newtds").val(user.newtds);
            $("#update_newother").val(user.newother);
            $("#update_do_no").val(user.do_no);
            $("#update_shipno").val(user.shipno);
            $("#update_inno").val(user.invno);
        }
    );
    // Open modal popup
    $("#update_user_modal").modal("show");
}

function UpdateUserDetails() {
    // get values

var bid = $("#update_bid").val();
var oid = $("#update_oid").val();
var did = $("#update_did").val();
var lrno = $("#update_lrno").val();
var trkno = $("#update_trkno").val();
var wt = $("#update_wt").val();
var rate = $("#update_rate").val();
var actual = $("#update_actual").val();
var load = $("#update_load").val();
var total = $("#update_total").val();
var advance = $("#update_advance").val();
var balance = $("#update_balance").val();
var newtds = $("#update_newtds").val();
var newother = $("#update_newother").val();
var do_no = $("#update_do_no").val();
var shipno = $("#update_shipno").val();
var inno = $("#update_inno").val();


    // get hidden field value
    var id = $("#hidden_user_id").val();
// Update the details by requesting to the server using ajax
    $.post("ajax/updateUserDetails.php", {

 id: id,
 bid: bid,
 oid: oid,
 did: did,
 lrno: lrno,
 trkno: trkno,
 wt: wt,
 rate: rate,
 actual: actual,
 load: load,
 total: total,
 advance: advance,
 balance: balance,
 newtds : newtds,
 newother : newother,
 shipno : shipno,
 inno : inno 
 do_no : do_no 

        },
        function (data, status) {
            // hide modal popup            

            $("#update_user_modal").modal("hide");
            // reload Users by using readRecords();
            readRecords();
        }
    );
}

$(document).ready(function () {
    // READ recods on page load
    readRecords(); // calling function
readRecords2();

});