    var wrapper = document.getElementById("signature-pad2"),
    clearButton = wrapper.querySelector("[data-action=clear2]"),
    saveButton = wrapper.querySelector("[data-action=save2]"),
    canvas = document.getElementById("textCanvas"),
    signaturePad;


function resizeCanvas() {
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {


    document.getElementById("myImg").src = signaturePad.toDataURL();
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var img = document.getElementById('myImg');

    context.drawImage(img, 0, 0 );
    var theData = canvas.toDataURL();
    document.getElementById('sign').value = theData;

    }
});
