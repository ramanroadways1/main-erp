<?php
require_once './connection.php';

$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$name = trim(escapeString($conn,strtoupper($_POST['name'])));

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile Number !');
		$('#button_add').attr('disabled',true);
		$('#mobileno').attr('readonly',false);
		$('#send_otp_button').show();
		$('#loadicon').hide();
	</script>";
	exit();
}

$Check_Mobile = Qry($conn,"SELECT name FROM emp_attendance WHERE mobile_no='$mobile'");

if(!$Check_Mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
}

if(numRows($Check_Mobile)>0)
{
	$row_Check = fetchArray($Check_Mobile);
	
	echo "<script>
		alert('Duplicate Mobile Number. Mobile Number Registered with $row_Check[name] !');
		$('#button_add').attr('disabled',true);
		$('#mobileno').attr('readonly',false);
		$('#send_otp_button').show();
		$('#loadicon').hide();
	</script>";
	exit();
}
	
$date = date('Y-m-d');
$otp = rand(100000,999999);
$message="Hello $name, Enter OTP : $otp to verify your mobile number.";

$msg_template="Hello $name,\nEnter OTP: $otp to verify your mobile number.\nRamanRoadways.";
$msg_wa = SendWAMsg($conn,$mobile,$msg_template);

if($msg_wa!='1')
{
	echo "<script>
		alert('OTP sent via text msg to : $mobile.');
	</script>";	
	MsgToVerifyMobile($mobile,$name,$otp);
}

$_SESSION['session_otp'] = $otp;

		echo "<script>
			alert('OTP has been sent to : $mobile.');
			$('#button_add').attr('disabled',false);
			$('#send_otp_button').hide();
			$('#mobileno').attr('readonly',true);
			$('#loadicon').hide();
		</script>";	

?>