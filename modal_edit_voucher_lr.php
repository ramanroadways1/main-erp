<button id="vou_lr_edit_modal_btn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#EditVouLRModal" style="display:none"></button>

<form id="VouLRForm" action="#" method="POST">
<div id="EditVouLRModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Edit Locations
      </div>
      <div class="modal-body">
        <div class="row">
			
			<div class="form-group col-md-6">
				<label>From Station <font color="red"><sup>*</sup></font></label>
				<input type="text" id="from_loc_edit_lr" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="from" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>To Station <font color="red"><sup>*</sup></font></label>
				<input type="text" id="to_loc_edit_lr" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="to" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="lr_id" id="lr_id_edit_lr">
			
			<input type="hidden" name="from_id" id="from_loc_edit_lr_id">
			<input type="hidden" name="to_id" id="to_loc_edit_lr_id">
			
		</div>
      </div>
	  <div id="result_VouLRForm"></div>
      <div class="modal-footer">
        <button type="submit" id="edit_lr_button_vou" disabled class="btn btn-primary">Update Location</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#VouLRForms").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#edit_lr_button_vou").attr("disabled", true);
		$.ajax({
        	url: "./smemo_own_lr_loc_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_VouLRForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

$(function() {
		$("#from_loc_edit_lr").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#from_loc_edit_lr').val(ui.item.value);   
           $('#from_loc_edit_lr_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#from_loc_edit_lr").val('');
			$("#from_loc_edit_lr").focus();
			$("#from_loc_edit_lr_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		
$(function() {
		$("#to_loc_edit_lr").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#to_loc_edit_lr').val(ui.item.value);   
           $('#to_loc_edit_lr_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#to_loc_edit_lr").val('');
			$("#to_loc_edit_lr").focus();
			$("#to_loc_edit_lr_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});		
</script>