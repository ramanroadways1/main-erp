<?php
require_once 'connection.php';

$company =  escapeString($conn,strtoupper($_REQUEST['company'])); 
$from_date =  escapeString($conn,strtoupper($_REQUEST['from_date']));
$to_date =  escapeString($conn,strtoupper($_REQUEST['to_date']));

if($company=='RRPL'){
	$sql ="SELECT c.id,DATE_FORMAT(c.date,'%d-%m-%y') as date,DATE_FORMAT(c.vou_date,'%d-%m-%y') as vou_date,c.vou_no,c.vou_type,
c.desct,c.debit as dr,c.credit as cr,c.balance as balance,e.name FROM cashbook AS c 
LEFT OUTER JOIN emp_attendance AS e ON e.code=c.user_code WHERE 
c.date BETWEEN '$from_date' AND '$to_date' AND c.comp='RRPL' AND c.user='$branch' ORDER BY c.id";
}else{
	$sql ="SELECT c.id,DATE_FORMAT(c.date,'%d-%m-%y') as date,DATE_FORMAT(c.vou_date,'%d-%m-%y') as vou_date,c.vou_no,c.vou_type,
c.desct,c.debit2 as dr,c.credit2 as cr,c.balance2 as balance,e.name FROM cashbook AS c 
LEFT OUTER JOIN emp_attendance AS e ON e.code=c.user_code WHERE 
c.date BETWEEN '$from_date' AND '$to_date' AND c.comp='RAMAN_ROADWAYS' AND c.user='$branch' ORDER BY c.id";
}

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
	array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'name', 'dt' => 1),
    array( 'db' => 'date', 'dt' => 2),
    array( 'db' => 'vou_date', 'dt' => 3), 
    array( 'db' => 'vou_no', 'dt' => 4), 
    array( 'db' => 'vou_type', 'dt' => 5),  
    array( 'db' => 'desct', 'dt' => 6),
    array( 'db' => 'cr', 'dt' => 7),
    array( 'db' => 'dr', 'dt' => 8), 
    array( 'db' => 'balance', 'dt' => 9),
);
 
 $sql_details = array(
   'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require( 'scripts/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);