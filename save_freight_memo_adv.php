<?php 
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$fids = escapeString($conn,strtoupper($_POST['fmemoid']));

$oid = escapeString($conn,strtoupper($_POST['owner_id']));
$bid = escapeString($conn,strtoupper($_POST['broker_id']));
$company = escapeString($conn,strtoupper($_POST['company_name']));
$vou_date = escapeString($conn,strtoupper($_POST['vou_date']));
$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));

$db_af = escapeString($conn,strtoupper($_POST['db_af']));
$db_weight = escapeString($conn,strtoupper($_POST['db_weight']));
$db_lrno = escapeString($conn,strtoupper($_POST['db_lrno']));
$db_lrno2 = strtoupper($_POST['db_lrno2']);
$db_lr_count = escapeString($conn,strtoupper($_POST['db_lr_count']));

$loading = escapeString($conn,strtoupper($_POST['load']));
$dsl_inc = 0;
$gps_charges = escapeString($conn,strtoupper($_POST['gps_charges']));
$other = escapeString($conn,strtoupper($_POST['other']));
$tds = escapeString($conn,strtoupper($_POST['tds']));
$claim_amount = escapeString($conn,strtoupper($_POST['claim_amount']));

$advance_to = escapeString($conn,strtoupper($_POST['ptob']));
$cash_adv = escapeString($conn,strtoupper($_POST['casmt']));
$gps_deposit_at_adv = escapeString($conn,($_POST['gps_deposit_at_adv']));

$cheque_payment = escapeString($conn,strtoupper($_POST['chq_sel']));
$diesel_payment = escapeString($conn,strtoupper($_POST['dsl_sel']));
$rtgs_payment = escapeString($conn,strtoupper($_POST['rtgs_sel']));

$narration = escapeString($conn,strtoupper(trim($_POST['narration'])));	

$total_freight = escapeString($conn,strtoupper($_POST['total_freight']));	
$total_advance = escapeString($conn,strtoupper($_POST['total_adv_amount']));	
$balance_left = escapeString($conn,strtoupper($_POST['balance_left']));	

$from_loc_db1 = escapeString($conn,strtoupper($_POST['from_loc_db1']));	
$to_loc_db1 = escapeString($conn,strtoupper($_POST['to_loc_db1']));	

if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $narration) AND $narration!='')
{
	echo "<script>
		alert('Error : Check Narration !');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>";
	exit();
}
		
if($company!='RRPL' AND $company!='RAMAN_ROADWAYS')
{
	echo "<script>
			alert('Company not found.');
			window.location.href='./';
		</script>";
	exit();	
}

if($total_freight<0)
{
	echo "<script>
			alert('Please Check Freight and Advance Amount..');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
	exit();
}

// gps charges verify start

$get_fm_id = Qry($conn,"SELECT id,date FROM freight_form WHERE frno='$fids'");

if(!$get_fm_id){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_fm_id = fetchArray($get_fm_id);

$fm_id = $row_fm_id['id'];
$fm_date = $row_fm_id['date'];

$datediff = round((strtotime(date("Y-m-d"))-strtotime($fm_date)) / (60 * 60 * 24));

$chk_adv_allow = Qry($conn,"SELECT is_allowed FROM advance_unlock WHERE frno='$fids' AND is_allowed='1'");

if(!$chk_adv_allow){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows_adv_allowed = fetchArray($chk_adv_allow);

if($numrows_adv_allowed>0)
{
	$row_adv_unlock = fetchArray($numrows_adv_allowed);
	$adv_unlock = $row_adv_unlock['is_allowed'];
}
else
{
	$adv_unlock = "0";
}

if($datediff>=15 AND $adv_unlock=="0")
{
	echo "<script>
		alert('Warning : Advance blocked as FM is 15 days older !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_gps_data = Qry($conn,"SELECT id,device_imei,fix_attach,active_vou,is_repeated,pending_charges,validity_end,
current_validity FROM gps_device_inventory WHERE active_on='$truck_no'");

if(!$get_gps_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$update_pending_charges="NO";
$update_gps_validity="NO";
$is_repeated = "NO";
$set_fm_no = "NO";

if(numRows($get_gps_data)==0)
{
	$gps_rent_charge = 0;
}
else
{	
	if(numRows($get_gps_data)>1)
	{
		echo "<script>
			alert('Multiple GPS devices active on this vehicle !!');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_Chk_Gps = fetchArray($get_gps_data);
	
	if($row_Chk_Gps['is_repeated']=="1")
	{
		$is_repeated = "YES";
	}
	
	$gps_record_id = $row_Chk_Gps['id'];
	$gps_device_no = $row_Chk_Gps['device_imei'];
	$gps_fix_attach = $row_Chk_Gps['fix_attach'];
	
	if($row_Chk_Gps['active_vou']=="")
	{
		$set_fm_no = "YES";
	}
	
	if($row_Chk_Gps['pending_charges']=="1")
	{
		if($row_Chk_Gps['fix_attach']=="Fix"){
			$gps_rent_charge = 1880;
		}
		else{
			$gps_rent_charge = 380;
		}
		
		$update_pending_charges="YES";
	}
	else
	{
			if($row_Chk_Gps['current_validity']==0 || $row_Chk_Gps['current_validity']==""){
				$gps_validity_date = $row_Chk_Gps['validity_end'];
			}
			else{
				$gps_validity_date = $row_Chk_Gps['current_validity'];
			}
			
			if($gps_validity_date==0 || $gps_validity_date=="")
			{
				echo "<script>
					alert('Warning : GPS validity date is not valid.');
					window.close();	
				</script>";
				exit();
			}
			
			if(strtotime(date("Y-m-d"))>strtotime($gps_validity_date))
			{
				if($row_Chk_Gps['fix_attach']=="Fix")
				{
					$gps_rent_charge = 380;
				}
				else
				{
					$gps_day = round((strtotime(date("Y-m-d")) - strtotime($gps_validity_date)) / (60 * 60 * 24));	
						$count1 = floor($gps_day/30)+1;

						if($gps_day % 30 == 0){
							$count1=$count1-1;
						}
												
						$gps_rent_charge = $count1 * 380;
												
						if($gps_rent_charge>1880){
							$gps_rent_charge=1880;
						}
				}
				
				$gps_validity_date_new = date('Y-m-d', strtotime('+30 day', strtotime(date("Y-m-d"))));;
				$update_gps_validity="YES";	
			}
			else
			{
				$gps_rent_charge = 0;
			}
	}
}

if(sprintf("%.2f",$gps_rent_charge)!=sprintf("%.2f",$gps_charges))
{ 
	errorLog("GPS charges not verified. Truck_no: $truck_no. ByCode: $gps_rent_charge , byForm:$gps_charges ",$conn,$page_name,__LINE__);
	echo "<script>
			alert('GPS charges not verified.');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
	exit();
}

$final_freight_amount = $db_af + $loading + $dsl_inc;

$verify_amount = (($db_af + $loading + $dsl_inc) - ($gps_charges + $gps_deposit_at_adv + $other + $tds + $claim_amount));

if($total_freight!=$verify_amount)
{
	echo "<script>
			alert('Please Check Freight and Advance Amount..');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
	exit();
}

if($tds>0 AND $db_af<=0)
{
	echo "<script>
			alert('Freight amount must be greater than 0 when TDS deduction is mandatory.');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
	exit();
}

if($advance_to=='NULL' && $total_advance>0)
{
	echo "<script>
		alert('Please Check Advance Amount.');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>";
	exit();
}
else if($advance_to!='NULL' && $total_advance<=0)
{
	echo "<script>
		alert('Please Check Advance Amount.');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>";
	exit();
}

if($total_advance<0)
{
	echo "<script>
		alert('Total Advance is less than Zero. Please Check Advance Amount.');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>";
	exit();
}
	
if($balance_left<0)
{
	echo "<script type='text/javascript'>
		alert('Balance amount is less than Zero.');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>"; 
	exit();
}

if($balance_left!=($total_freight-$total_advance))
{
	echo "<script type='text/javascript'>
		alert('Please check advance and balance amount.');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>"; 
	exit();
}
	
if($advance_to=='NULL' || $advance_to=='' AND ($cheque_payment=='1' || $diesel_payment=='1' || $rtgs_payment=='1'))
{
	echo "<script>
			alert('Invalid payment mode selected !');
			window.location.href='./';
		</script>";
	exit();
}

$get_broker_name = Qry($conn,"SELECT name,pan FROM mk_broker WHERE id='$bid'");
	
if(!$get_broker_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($get_broker_name)==0){
	errorLog("Broker not found. Bid: $bid.",$conn,$page_name,__LINE__);
	Redirect("Broker not found..","./");
	exit();
}
		
$row_broker_name = fetchArray($get_broker_name);
$broker_name = $row_broker_name['name'];

$broker_pan_no = $row_broker_name['pan'];

$get_owner_name = Qry($conn,"SELECT name,mo1,mo2,pan,up6 as dec1 FROM mk_truck WHERE id='$oid'");
	
if(!$get_owner_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($get_owner_name)==0){
	errorLog("owner not found. Oid: $oid.",$conn,$page_name,__LINE__);
	Redirect("owner not found..","./");
	exit();
}
		
$row_owner_name = fetchArray($get_owner_name);
$owner_name = $row_owner_name['name'];
$owner_mo1 = $row_owner_name['mo1'];
$owner_mo2 = $row_owner_name['mo2'];
$dec_db = $row_owner_name['dec1'];

$owner_pan_no = $row_owner_name['pan'];

$pan_latter = substr($owner_pan_no,3,1);
$pan_latter_broker = substr($broker_pan_no,3,1);
			
if($dec_db=='')
{
	$tds_deduct=1;
				
	if($owner_pan_no=='AAACC7939H' || $broker_pan_no=='AAACC7939H')
	{
		$get_tds_chartered = Qry($conn,"SELECT freight FROM _party_freight_db WHERE party_pan='AAACC7939H'");
					
		if(!$get_tds_chartered){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
					
		$row_tds_chartered = fetchArray($get_tds_chartered);
					
		$tds_chartered = $row_tds_chartered['freight'] + $db_af;
					
		if($tds_chartered>=2500000)
		{
			$tds_amount_owner = sprintf("%0.2f",ceil($db_af*2/100));
			$tds_amount_broker = sprintf("%0.2f",ceil($db_af*2/100));
		}
		else
		{
			$tds_amount_owner = sprintf("%0.2f",ceil($db_af*0.10/100));
			$tds_amount_broker = sprintf("%0.2f",ceil($db_af*0.10/100));
		}
	}
	else
	{
		$get_tds_owner = Qry($conn,"SELECT tds_value FROM tds_value WHERE pan_letter='$pan_latter'");
					
		if(!$get_tds_owner){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($get_tds_owner)>0)
		{
			$row_tds_value_owner = fetchArray($get_tds_owner);
			$tds_amount_owner = sprintf("%0.2f",ceil(($db_af*$row_tds_value_owner['tds_value'])/100));
		}
		else
		{
			$tds_amount_owner=0;
		}
		
		$get_tds_broker = Qry($conn,"SELECT tds_value FROM tds_value WHERE pan_letter='$pan_latter_broker'");
		
		if(!$get_tds_broker){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($get_tds_broker)>0) 
		{
			$row_tds_value_broker = fetchArray($get_tds_broker);
			$tds_amount_broker = sprintf("%0.2f",ceil(($db_af*$row_tds_value_broker['tds_value'])/100));
		}
		else
		{
			$tds_amount_broker=0;
		}
	}
	
	// if($advance_to=='NULL')
	// {
		// $tds_deduct=0;
	// }
}
else
{
	$tds_deduct=0;
	$tds_amount_broker=0;
	$tds_amount_owner=0;
}

if($advance_to=='OWNER')
{
	if($tds_deduct=='1' AND $tds_amount_owner==0)
	{
		echo "<script>
			alert('Owner\'s PAN Card is Invalid. Please update Broker\'s PAN Card !');
			window.close();
		</script>";
		exit();
	}
	
	if($tds != $tds_amount_owner)
	{
		echo "<script>
			alert('TDS amount not verified ! Advance : Owner');
			window.close();
		</script>";
		exit();
	}
	
	$get_party_details = Qry($conn,"SELECT name,pan,mo1,bank,acname,accno,ifsc FROM mk_truck WHERE id='$oid'");
	
	if(!$get_party_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($get_party_details)==0){
		errorLog("Account details not found. Oid: $oid and Bid: $bid.",$conn,$page_name,__LINE__);
		Redirect("Account details not found. Owner","./");
		exit();
	}
		
	$row_party = fetchArray($get_party_details);
	
	$adv_party = $row_party['name'];
	$adv_party_pan = $row_party['pan'];
	$adv_party_mobile = $row_party['mo1'];
	
	$advance_to_msg = "OWNER";
}
else if($advance_to=='BROKER')
{
	if($tds_deduct=='1' AND $tds_amount_broker==0)
	{
		echo "<script>
			alert('Broker\'s PAN Card is Invalid. Please update Broker\'s PAN Card !');
			window.close();
		</script>";
		exit();
	}
	
	if($tds != $tds_amount_broker)
	{
		echo "<script>
			alert('TDS amount not verified ! Advance : Broker');
			window.close();
		</script>";
		exit();
	}
	
	$get_party_details = Qry($conn,"SELECT name,pan,mo1,bank,acname,accno,ifsc FROM mk_broker WHERE id='$bid'");
	
	if(!$get_party_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($get_party_details)==0){
		errorLog("Account details not found. Oid: $oid and Bid: $bid.",$conn,$page_name,__LINE__);
		Redirect("Account details not found. Broker","./");
		exit();
	}
		
	$row_party = fetchArray($get_party_details);
	
	$adv_party = $row_party['name'];
	$adv_party_pan = $row_party['pan'];
	$adv_party_mobile = $row_party['mo1'];
	
	$advance_to_msg = "BROKER";
}
else if($advance_to=='NULL')
{
	$get_owner = Qry($conn,"SELECT name,pan,mo1 FROM mk_truck WHERE id='$oid'");
	
	if(!$get_owner){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_owner)==0){
		errorLog("Account details not found. Oid: $oid.",$conn,$page_name,__LINE__);
		Redirect("Account details not found. Owner(N)","./");
		exit();
	}
	
	$row_owner = fetchArray($get_owner);
	
	$get_broker = Qry($conn,"SELECT name,pan,mo1 FROM mk_broker WHERE id='$bid'");
	
	if(!$get_broker){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($get_broker)==0){
		errorLog("Account details not found. Bid: $bid.",$conn,$page_name,__LINE__);
		Redirect("Account details not found. Broker(N)","./");
		exit();
	}
		
	$row_broker = fetchArray($get_broker);
		
	if($tds_deduct=='1' AND $tds_amount_owner==0 AND $tds_amount_broker==0 AND $db_af>0)
	{
		echo "<script>
			alert('At least one PAN Card required for TDS deduction. $tds_deduct');
			window.close();
		</script>";
		exit();
	}
	
	if(strlen($row_owner['pan'])!=10)
	{
		if($tds_amount_broker>0 AND $tds != $tds_amount_broker)
		{
			echo "<script>
				alert('TDS amount not verified ! Advance : NULL(Broker)');
				window.close();
			</script>";
			exit();
		}
	}
	else
	{
		if($tds_amount_owner>0 AND $tds != $tds_amount_owner)
		{
			echo "<script>
				alert('TDS amount not verified ! Advance : NULL(Owner)');
				window.close();
			</script>";
			exit();
		}
	}
	
	if(strlen($row_owner['pan'])!=10)
	{
		if(strlen($row_broker['pan'])!=10)
		{
			Redirect("Broker PAN Card is not valid.","./");
			exit();
		}
		else
		{
			$adv_party = $row_broker['name'];
			$adv_party_pan = $row_broker['pan'];
			$adv_party_mobile = $row_broker['mo1'];
			$advance_to_msg = "BROKER";
		}
	}
	else
	{
		$adv_party = $row_owner['name'];
		$adv_party_pan = $row_owner['pan'];
		$adv_party_mobile = $row_owner['mo1'];
		$advance_to_msg = "OWNER";
	}
}
else
{
	Redirect("Invalid option selected.","./");
	exit();
}

$qry_pre_fetch = Qry($conn,"SELECT GROUP_CONCAT(QUOTE(diesel_id) SEPARATOR ',') as diesel_id,tno FROM _pending_diesel WHERE 
lrno IN($db_lrno2) AND branch='$branch' GROUP BY fno");

if(!$qry_pre_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry_pre_fetch)==0)
{
	$advance_diesel=0;
}
else if(numRows($qry_pre_fetch)>1)
{
	echo "<script>
		alert('Warning : Advance Diesel Requested Multiple Time. You Can not Proceed !');
		window.location.href='./';	
	</script>";
	exit();
}
else if(numRows($qry_pre_fetch)==1)
{
$row_request = fetchArray($qry_pre_fetch);

$diesel_Ids = $row_request['diesel_id'];

$get_diesel_details = Qry($conn,"SELECT branch,fno,tno,com,disamt,cash,tno,dsl_nrr,done,no_lr,crossing FROM diesel_fm WHERE id 
IN($diesel_Ids) AND branch='$branch'");	

if(!$get_diesel_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

	$advance_diesel=0;
	$no_lr_set=array();
	
	while($row_pre_req=fetchArray($get_diesel_details))
	{
		$sys_tno=$row_pre_req['tno'];
		$advance_diesel=$advance_diesel+$row_pre_req['disamt'];
		$crossing=$row_pre_req['crossing'];
		$done_diesel=$row_pre_req['done'];
		$no_lr_set[]=$row_pre_req['no_lr'];
	}
	
	if(in_array("1",$no_lr_set))
	{
		echo "<script>
			alert('Error: LR No pending against Diesel Recharge.');
			window.location.href='./';	
		</script>";
		exit();
	}
	
	// if($crossing==1)
	// {
		if($sys_tno!=$truck_no)
		{
			echo "<script>
					alert('Warning : Vehicle Number not matching with requested diesel.');
					window.location.href='./';	
				</script>";
			exit();
		}	
	// }
	
	if($advance_diesel>0 && $done_diesel==0)
	{
		echo "<script>
				alert('Warning : Requested diesel not marked as done yet. Contact Diesel Department.');
				window.location.href='./';	
			</script>";
		exit();
	}
}

if($cheque_payment=='1')
{
	$cheque_amount = escapeString($conn,strtoupper($_POST['chqamt']));
	$cheque_no = escapeString($conn,strtoupper($_POST['chqno']));
	
	if($cheque_amount<=0){
		echo "<script>
			alert('Cheque Amount is not valid !');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	$cheque_amount = 0;
	$cheque_no = "";
}

	$fetch_diesel = Qry($conn,"SELECT SUM(amount) as diesel_amount,SUM(qty) as dieselQty FROM diesel_sample WHERE frno='$fids' AND 
	type='ADV'");

	if(!$fetch_diesel){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}	

	if(numRows($fetch_diesel)>0)
	{
		$row_diesel = fetchArray($fetch_diesel);
		$diesel_entry = $row_diesel['diesel_amount'];
		$diesel_Qty = $row_diesel['dieselQty'];
	}
	else
	{
		$diesel_entry = 0;
		$diesel_Qty = 0;
	}
	
$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$fids' AND ril_card='1' AND type='ADV'");
	
if(!$chk_ril_stock){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_ril_stock)>0)
{
	while($row_ril = fetchArray($chk_ril_stock))
	{
		$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
		if(!$get_balance){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_balance = fetchArray($get_balance);
		
		if($row_balance['balance']<$row_ril['amount'])
		{
			echo "<script>
				alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();	
		}
	}
}	

$total_diesel = $advance_diesel+$diesel_entry;

if($diesel_payment=='1')
{
	$diesel_fm_amount = escapeString($conn,$_POST['disamt']);
	
	if($diesel_fm_amount<=0)
	{
		echo "<script>
			alert('Check Diesel Amount !!');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
	
	if($diesel_fm_amount!=$total_diesel)
	{
		echo "<script>
			alert('Diesel Amount not verified. Adv Requested Diesel is : $advance_diesel and Adding now is : $diesel_entry. Please Check !');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	$diesel_fm_amount=0;
	
	if($total_diesel>0)
	{
		echo "<script>
			alert('Diesel Advance is mandatory !');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
}

if($rtgs_payment=='1')
{
	$rtgs_amount = escapeString($conn,strtoupper($_POST['rtgs']));
	
	if($rtgs_amount<=0){
		echo "<script>
			alert('Check RTGS Amount !');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
	
	if($row_party['accno']!='')
	{
		$ac_holder = escapeString($conn,strtoupper($row_party['acname']));
		$ac_no = escapeString($conn,strtoupper($row_party['accno']));
		$bank_name = escapeString($conn,strtoupper($row_party['bank']));
		$ifsc_code = escapeString($conn,strtoupper($row_party['ifsc']));
		
		$update_ac = "NO";
	}
	else
	{
		$ac_holder = trim(escapeString($conn,strtoupper($_POST['holder'])));
		$ac_no =  trim(escapeString($conn,strtoupper($_POST['acno'])));
		$bank_name =  trim(escapeString($conn,strtoupper($_POST['bank'])));
		$ifsc_code =  trim(escapeString($conn,strtoupper($_POST['ifsc'])));
		
		if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $ac_holder))
		{
			echo "<script>
				alert('Error : Check Ac holder name !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ac_no))
		{
			echo "<script>
				alert('Error : Check Ac Number !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-z A-Z\.]*$/', $bank_name))
		{
			echo "<script>
				alert('Error : Check Bank Name !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ifsc_code))
		{
			echo "<script>
				alert('Error : Check IFSC Code !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}

		$update_ac = "YES";
	}
	
	if($ac_no=='')
	{
		// echo "<script type='text/javascript'>
			// alert('Account details not found. Empty Ac Number. $update_ac');
			// $('#loadicon').hide();
		// </script>"; 
		// exit(); 
		Redirect("EMPTY Account details.","./");
		exit();
	}
	
	if($update_ac=='YES')
	{
		if($advance_to=='OWNER')
		{
			$update_ac_details = Qry($conn,"UPDATE mk_truck SET bank='$bank_name',acname='$ac_holder',accno='$ac_no',ifsc='$ifsc_code' 
			WHERE id='$oid'");
			$ac_for=$oid;
		}
		else
		{
			$update_ac_details = Qry($conn,"UPDATE mk_broker SET bank='$bank_name',acname='$ac_holder',accno='$ac_no',ifsc='$ifsc_code' 
			WHERE id='$bid'");
			$ac_for=$bid;
		}
		
		if(!$update_ac_details){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$update_ac_add_log = Qry($conn,"INSERT INTO ac_update(o_b,ac_for,at_the_time_of,acname,acno,bank,ifsc,branch,
		branch_user,timestamp) VALUES ('$advance_to','$ac_for','ADVANCE','$ac_holder','$ac_no','$bank_name','$ifsc_code',
		'$branch','$branch_sub_user','$timestamp')");
		
		if(!$update_ac_add_log){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
	}
}
else
{
	$rtgs_amount = 0;
}

if($total_advance!=($cash_adv+$cheque_amount+$diesel_fm_amount+$rtgs_amount))
{
	echo "<script>
			alert('Please Check Advance Amount..');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
	exit();
}


// echo $diesel_Ids;
// echo "<script>
		// $('#loadicon').hide();
	// </script>"; 
	// exit();
	
$get_cash_balance = Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");

if(!$get_cash_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_cash_balance)==0){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Branch not found.","./");
	exit();
}
		
$row_cash_bal = fetchArray($get_cash_balance);
		
$email = $row_cash_bal['email'];
$rrpl_cash = $row_cash_bal['balance'];
$rr_cash = $row_cash_bal['balance2'];

if($company=='RRPL')
{
	$debit_col='debit';		
	$balance_col='balance';		
}
else if($company=="RAMAN_ROADWAYS")
{
	$debit_col='debit2';		
	$balance_col='balance2';			
}
else
{
	echo "<script type='text/javascript'>
			alert('Unable to fetch company.');
			window.location.href='./';
		</script>"; 
	exit(); 
}

if($cash_adv>0)
{
	if($company=='RRPL' && $rrpl_cash>=$cash_adv)
	{
		$new_cash = $rrpl_cash - $cash_adv;
	}
	else if($company=="RAMAN_ROADWAYS" && $rr_cash>=$cash_adv)
	{
		$new_cash = $rr_cash - $cash_adv;
	}
	else
	{
		echo "<script>
			alert('UNSUFFICIENT BALANCE in $company.');
			$('#loadicon').hide();
			$('#btn_submit').attr('disabled', false);
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;
$rtgs_flag = false;	

if($cash_adv>0)
{
	$update_main_cash = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$cash_adv' WHERE username='$branch'");
	
	if(!$update_main_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Balance not updated. Branch: $branch. VouNo: $fids. Cash Amount: $cash_adv.",$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,lrno,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$branch','$date','$vou_date','$company','$fids','Freight_Memo','Advance Cash','$db_lrno','$cash_adv','$new_cash','$timestamp')");
		
	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Cash not inserted. Branch: $branch. VouNo: $fids. Cash Amount: $cash_adv.",$conn,$page_name,__LINE__);
	}
}

if($cheque_amount>0)
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,lrno,chq_no,`$debit_col`,timestamp) 
	VALUES ('$branch','$fids','$date','$vou_date','$company','Freight_Memo','Advance Cheque','$db_lrno','$cheque_no','$cheque_amount',
	'$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Passbook entry not inserted. Branch: $branch. VouNo: $fids. Chq Amount: $cheque_amount.",$conn,$page_name,__LINE__);
	}

	$insert_cheque = Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,pay_type,vou_date,lrno,amount,cheq_no,date,branch,timestamp) 
	VALUES ('$fids','Freight_Memo','ADV','$vou_date','$db_lrno','$cheque_amount','$cheque_no','$date','$branch','$timestamp')");
		
	if(!$insert_cheque){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Cheque entry not inserted. Branch: $branch. VouNo: $fids. Chq Amount: $cheque_amount.",$conn,$page_name,__LINE__);
	}
}
	
if($rtgs_amount>0)
{	
	$insert_passbook_rtgs = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,lrno,`$debit_col`,timestamp) 
	VALUES ('$branch','$fids','$date','$vou_date','$company','Freight_Memo','Advance RTGS/NEFT','$db_lrno','$rtgs_amount',
	'$timestamp')");
	
	if(!$insert_passbook_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Passbook entry not inserted. Branch: $branch. VouNo: $fids. Rtgs Amount: $rtgs_amount.",$conn,$page_name,__LINE__);
	}	
	
	$updateTodayRtgs = Qry($conn,"UPDATE today_data SET neft=neft+'1',neft_amount=neft_amount+'$rtgs_amount' WHERE branch='$branch'");
	
	if(!$updateTodayRtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$rtgs_adv_set="0";
}
else
{
	$rtgs_adv_set="1";
}

if($diesel_entry>0)
{
	$insert_new_diesel = Qry($conn,"INSERT INTO diesel_fm(branch,fno,com,qty,rate,disamt,tno,lrno,type,dsl_by,dcard,veh_no,dcom,dsl_nrr,
	pay_date,fm_date,crossing,done,done_time,ril_card,dsl_mobileno,timestamp,stockid) SELECT branch,frno,'$company',qty,rate,amount,'$truck_no',
	'$db_lrno','ADVANCE',card_pump,card_no,phy_card,fuel_comp,dsl_nrr,date,'$vou_date','0',done,done_time,ril_card,mobile_no,
	'$timestamp',stockid FROM diesel_sample WHERE frno='$fids' AND type='ADV' AND branch='$branch'");
	
	if(!$insert_new_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Diesel Record Copy Failed. Branch: $branch. VouNo: $fids. Diesel Amount: $diesel_entry.",$conn,$page_name,__LINE__);
	}
	
	$get_ril_amount = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$fids' AND ril_card='1' AND type='ADV'");
	
	if(!$get_ril_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_ril_amount)>0)
	{
		while($row_ril2 = fetchArray($get_ril_amount))
		{
			$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
			cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
			
			if(!$update_ril_stock){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}

	$delete_diesel = Qry($conn,"DELETE FROM diesel_sample WHERE frno='$fids' AND type='ADV' AND branch='$branch'");
	
	if(!$delete_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_diesel_info = Qry($conn,"SELECT id,branch,disamt,tno,dsl_by,dcard,veh_no,dcom,timestamp,dsl_mobileno FROM diesel_fm 
	WHERE timestamp='$timestamp' AND fno='$fids' AND type='ADVANCE' AND dsl_by='CARD'");
	
	if(!$get_diesel_info){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_diesel_info)>0)
	{
		while($row_log=fetchArray($get_diesel_info))
		{
			if($row_log['dsl_by']=='CARD')
			{
				$card_live_bal = Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_log[dcard]'");
				if(!$card_live_bal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$row_card_bal=fetchArray($card_live_bal); 
				$live_bal = $row_card_bal['balance']; 

				$qry_log = Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,dslcomp,dsltype,
				cardno,vehno,cardbal,amount,mobileno,reqid) VALUES ('$row_log[branch]','SUCCESS','New Request Added','$row_log[timestamp]',
				'MARKET','$row_log[tno]','$row_log[dcom]','CARD','$row_log[dcard]','$row_log[veh_no]','$live_bal','$row_log[disamt]',
				'$row_log[dsl_mobileno]','$row_log[id]')");
				
				if(!$qry_log){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
	
	$updateTodayDiesel = Qry($conn,"UPDATE today_data SET diesel_qty_market=ROUND(diesel_qty_market+'$diesel_Qty',2),
	diesel_amount_market=diesel_amount_market+'$diesel_entry' WHERE branch='$branch'");
	
	if(!$updateTodayDiesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($advance_diesel>0)
{
	$insert_requested_diesel = Qry($conn,"UPDATE diesel_fm SET token_no=fno,fno='$fids',pre_req='1',pre_and_fm='1' WHERE 
	id IN(SELECT diesel_id FROM _pending_diesel WHERE lrno IN($db_lrno2) AND branch='$branch')");
	
	if(!$insert_requested_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Requested Diesel Record Copy Failed. Branch: $branch. VouNo: $fids. Diesel Amount: $advance_diesel.",$conn,$page_name,__LINE__);
	}
	
	$delete_reqested_diesel = Qry($conn,"DELETE FROM _pending_diesel WHERE lrno IN($db_lrno2) AND branch='$branch'");
	
	if(!$delete_reqested_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$gps_charge_insert = $gps_charges + $gps_deposit_at_adv;

$update_freight_memo = Qry($conn,"UPDATE freight_form SET actualf='$db_af',newtds='$loading',dsl_inc='$dsl_inc',gps='$gps_charge_insert',
adv_claim='$claim_amount',newother='$other',tds='$tds',totalf='$total_freight',totaladv='$total_advance',adv_branch_user='$branch_sub_user',
ptob='$advance_to',adv_date='$date',pto_adv_name='$adv_party',adv_pan='$adv_party_pan',cashadv='$cash_adv',chqadv='$cheque_amount',
chqno='$cheque_no',disadv='$diesel_fm_amount',rtgsneftamt='$rtgs_amount',rtgs_adv='$rtgs_adv_set',baladv='$balance_left',
narre='$narration' WHERE frno='$fids'");
	
if(!$update_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($claim_amount>0)
{
	$get_claim = Qry($conn,"SELECT claim_vou_no,vou_no,lrno,amount FROM claim_cache WHERE vou_no='$fids' AND adv_bal='ADV'");
	if(!$get_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_claim)==0)
	{
		$flag = false;
		errorLog("Claim not found. Vou: $fids.",$conn,$page_name,__LINE__);
	}
	
	while($row_claim = fetchArray($get_claim))
	{
		$get_claim_balance = Qry($conn,"SELECT id,pod_id,lrno,balance FROM claim_book_trans WHERE vou_no='$row_claim[claim_vou_no]' 
		AND lrno='$row_claim[lrno]' ORDER BY id DESC LIMIT 1");
		
		if(!$get_claim_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$row_claim_bal = fetchArray($get_claim_balance);
		$new_claim_bal = $row_claim_bal['balance']-$row_claim['amount'];
		$pod_id = $row_claim_bal['pod_id'];
		
		$insert_claim = Qry($conn,"INSERT INTO claim_book_trans(pod_id,lrno,vehicle_type,vou_no,trans_id,branch,branch_user,
		date,debit,balance,timestamp) VALUES ('$pod_id','$row_claim[lrno]','MARKET','$row_claim[claim_vou_no]','ADVANCE','$branch',
		'$branch_sub_user','$date','$row_claim[amount]','$new_claim_bal','$timestamp')");
		
		if(!$insert_claim){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$copy_claim = Qry($conn,"INSERT INTO claim_book(pod_id,vou_no,lrno,debit,adv_bal,branch,timestamp) SELECT '$pod_id',vou_no,lrno,
	amount,adv_bal,'$branch','$timestamp' FROM claim_cache WHERE vou_no='$fids' AND adv_bal='ADV'");
	
	if(!$copy_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_claim_cache = Qry($conn,"DELETE FROM claim_cache WHERE vou_no='$fids' AND adv_bal='ADV'");
	if(!$delete_claim_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($total_advance>0)
{
	$updateTodayAdv = Qry($conn,"UPDATE today_data SET fm_adv=fm_adv+'1',fm_adv_amount=fm_adv_amount+'$total_advance' WHERE branch='$branch'");
	
	if(!$updateTodayAdv){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($db_af>0)
{
	$updatePOD_Count = Qry($conn,"UPDATE _pending_lr SET lr_pod_pending=lr_pod_pending+'$db_lr_count' WHERE branch='$branch'");
	if(!$updatePOD_Count){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_pod_pending = Qry($conn,"INSERT INTO _pending_lr_list(vou_id,vou_no,bid,oid,branch,lr_date,create_date,lrno,timestamp) 
	SELECT id,frno,'$bid','$oid',branch,date,create_date,lrno,'$timestamp' FROM freight_form_lr WHERE frno='$fids'");
	
	if(!$insert_pod_pending){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$updateBalPending = Qry($conn,"UPDATE _pending_lr SET bal_pending=bal_pending+1 WHERE branch='$branch'");
if(!$updateBalPending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($rtgs_amount>0)
{
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	
$chk_for_crn = Qry($conn,"SELECT id FROM rtgs_fm WHERE crn='$crnnew'");	
if(!$chk_for_crn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($chk_for_crn)>0)
{
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
}	
	
$chk_for_rtgs = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$fids' AND type='ADVANCE'");	
if(!$chk_for_rtgs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_for_rtgs)==0)
{
$insert_rtgs=Qry($conn,"INSERT INTO rtgs_fm (fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,lrno,tno,type,
mobile,email,crn,timestamp) VALUES ('$fids','$branch','$company','$db_af','$rtgs_amount','$ac_holder','$ac_no','$bank_name','$ifsc_code',
'$adv_party_pan','$date','$vou_date','$db_lrno','$truck_no','ADVANCE','$adv_party_mobile','$email','$crnnew','$timestamp')");
	
	if(!$insert_rtgs){
		$flag = false;
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

}
	
// insert gps charges	

if($gps_rent_charge>0)
{
	$insert_gps_charges = Qry($conn,"INSERT INTO gps_amount_deducted (device_imei,vehicle_no,vou_no,adv_bal,
	gps_charges,deduct_type,branch,timestamp) VALUES ('$gps_device_no','$truck_no','$fids','1','$gps_rent_charge',
	'$gps_fix_attach','$branch','$timestamp')");
		
	if(!$insert_gps_charges){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($update_pending_charges=='YES')
	{
		$mark_gps_installed=Qry($conn,"UPDATE gps_device_inventory SET pending_charges='0' WHERE id='$gps_record_id'");
		
		if(!$mark_gps_installed){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$device_use_type = "New";
	}
	else
	{
		$device_use_type = "Re-use";
	}
	
	$insert_gps_log = Qry($conn,"INSERT INTO gps_device_log (vou_no,adv_bal,use_type,imei_no,tno,fix_attach,branch,timestamp) 
	VALUES ('$fids','ADVANCE','$device_use_type','$gps_device_no','$truck_no','$gps_fix_attach','$branch','$timestamp')");
			
	if(!$insert_gps_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($update_gps_validity=='YES')
	{
		$update_new_validity = Qry($conn,"UPDATE gps_device_inventory SET current_validity='$gps_validity_date_new' 
		WHERE id='$gps_record_id'");
		
		if(!$update_new_validity){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($set_fm_no=="YES" || $is_repeated=='YES')
{
	$get_gps_data2 = Qry($conn,"SELECT fix_attach,active_on,pending_charges,validity_start,validity_end,current_validity 
	FROM gps_device_inventory WHERE id='$gps_record_id'");
		
	if(!$get_gps_data2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_gps_data2 = fetchArray($get_gps_data2);
	
	if($row_gps_data2['pending_charges']=='1'){
		if($row_gps_data2['fix_attach']=='Fix'){
			$pending_amount_gps = 1880;
		}
		else{
			$pending_amount_gps = 380;
		}
	}
	else{
		$pending_amount_gps = 0;
	}
}

if($set_fm_no=="YES")
{
	$insert_gps_data = Qry($conn,"INSERT INTO gps_charges_pending(fix_attach,device_id,amount_deduct,pending_charges,tno,vou_no,
	val_start,val_end,curr_val,timestamp) VALUES ('$row_gps_data2[fix_attach]','$gps_record_id','$pending_amount_gps',
	'$row_gps_data2[pending_charges]','$row_gps_data2[active_on]','$fids','$row_gps_data2[validity_start]',
	'$row_gps_data2[validity_end]','$row_gps_data2[current_validity]','$timestamp')");
		
	if(!$insert_gps_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_vou_no = Qry($conn,"UPDATE gps_device_inventory SET active_vou='$fids',lr_mapping_timestamp='$timestamp' 
	WHERE id='$gps_record_id'");
		
	if(!$update_vou_no){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_gps_id = Qry($conn,"UPDATE freight_form SET gps_id='$gps_record_id' WHERE id='$fm_id'");
		
	if(!$update_gps_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($is_repeated=='YES')
{
	$insert_gps_data = Qry($conn,"INSERT INTO gps_charges_pending(fix_attach,device_id,amount_deduct,pending_charges,tno,vou_no,
	val_start,val_end,curr_val,timestamp) VALUES ('$row_gps_data2[fix_attach]','$gps_record_id','$pending_amount_gps',
	'$row_gps_data2[pending_charges]','$row_gps_data2[active_on]','$fids','$row_gps_data2[validity_start]',
	'$row_gps_data2[validity_end]','$row_gps_data2[current_validity]','$timestamp')");
		
	if(!$insert_gps_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_repeat_status = Qry($conn,"UPDATE gps_device_inventory SET active_vou='$fids',lr_mapping_timestamp='$timestamp',
	is_repeated='0' WHERE id='$gps_record_id'");
		
	if(!$update_repeat_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_gps_id2 = Qry($conn,"UPDATE freight_form SET gps_id='$gps_record_id' WHERE id='$fm_id'");
		
	if(!$update_gps_id2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_repeat_status2 = Qry($conn,"UPDATE gps_device_marked_repeat SET is_active='1' WHERE tno='$truck_no' AND 
	is_active='0'");
		
	if(!$update_repeat_status2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

// insert gps charges ends	

$get_insert_frt_party = Qry($conn,"SELECT id FROM _party_freight_db WHERE (party_pan='$owner_pan_no' || party_pan='$broker_pan_no')");
		
if(!$get_insert_frt_party){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($get_insert_frt_party)>0)
{
	$row_insert_frt = fetchArray($get_insert_frt_party);
	
	$update_freight_amt = Qry($conn,"UPDATE _party_freight_db SET freight=freight+'$final_freight_amount',
	loading_detention=loading_detention+'$loading' WHERE id='$row_insert_frt[id]'");
		
	if(!$update_freight_amt){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
echo "
<form action='smemo2.php' id='myFormadv' method='POST'>
	<input type='hidden' value='FM' name='key'>		
	<input type='hidden' value='$fids' name='idmemo'>		
</form>";

$check_owner_msg = Qry($conn,"SELECT id FROM `_adv_msg_to_owner` WHERE branch='$branch' AND is_active='1'");
if(!$check_owner_msg){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
	if(numRows($check_owner_msg)>0)
	{
		if(strlen($owner_mo1)==10)
		{
			$adv_msg_mobile = $owner_mo1;
		}
		else if(strlen($owner_mo2)==10)
		{
			$adv_msg_mobile = $owner_mo2;
		}
	
		// if(strlen($adv_msg_mobile)==10)
		// { 
		MsgFreightMemoAdv($conn,$oid,$bid,$fids,$advance_to_msg,$adv_msg_mobile,$total_advance,$cash_adv,$diesel_fm_amount,$rtgs_amount,$broker_name,$truck_no,$from_loc_db1,$to_loc_db1,$db_af,$branch,$branch_sub_user,$advance_to,$timestamp);	
		// }
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Freight Advance Submitted..');
		document.getElementById('myFormadv').submit();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#btn_submit').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#btn_submit').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}	
?>