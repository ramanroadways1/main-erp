<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$company = escapeString($conn,$_POST['company']);
$ac_no = escapeString($conn,$_POST['ac_no_deposit']);
$bank_name = escapeString($conn,$_POST['bank_name']);
$amount = escapeString($conn,$_POST['amount']);
$deposit_date = escapeString($conn,$_POST['deposit_date']);
$narration = $bank_name."-".$ac_no.", Date:$deposit_date /".escapeString($conn,$_POST['narration']);

if($amount<=0)
{
	echo "<script>
		alert('Error : Invalid amount !');
		$('#loadicon').hide();
		$('#dr_bank_deposit_button').attr('disabled',false);
	</script>";
	exit();	
}

if($ac_no=="")
{
	echo "<script>
		alert('Account nummber: $ac_no is not valid !');
		$('#dr_bank_ac_list').val('');
		$('#dr_bank_name').val('');
		$('#dr_bank_ac_no').val('');
		$('#loadicon').hide();
		$('#dr_bank_deposit_button').attr('disabled',false);
	</script>";
	exit();	
}

$sourcePath = $_FILES['deposit_slip']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

if(!in_array($_FILES['deposit_slip']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only image/pdf upload allowed.');
		$('#dr_bank_deposit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fix_name = mt_rand().date('dmYHis');

$targetPath="bank_deposit_slip/".$fix_name.".".pathinfo($_FILES['deposit_slip']['name'],PATHINFO_EXTENSION);

ImageUpload(700,700,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	echo "<script>
		alert('Error while upload deposit slip.');
		$('#dr_bank_deposit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

	
$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	
if(!$fetch_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	echo "<script>
		alert('Error !!');
		$('#dr_bank_deposit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
	
$row_bal = fetchArray($fetch_bal);
$rrpl_cash = $row_bal['balance'];
$rr_cash = $row_bal['balance2'];
		
if($company=='RRPL' AND $rrpl_cash>=$amount) 
{
	$dr_column="debit";
	$balance_column="balance";
	$new_balance = $rrpl_cash-$amount;
}
else if($company=='RAMAN_ROADWAYS' AND $rr_cash>=$amount) 
{
	$dr_column="debit2";
	$balance_column="balance2";
	$new_balance = $rr_cash-$amount;
}
else
{
	echo "<script>
		alert('Insufficient fund in company: $company !!');
		$('#dr_bank_deposit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_balance = Qry($conn,"update user set `$balance_column`='$new_balance' where username='$branch'");
if(!$update_balance){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Balance not updated. branch: $branch. Company: $company.",$conn,$page_name,__LINE__);	
}
	
$insert_debit = Qry($conn,"INSERT INTO cashbook(user,user_code,date,vou_date,comp,vou_type,desct,`$dr_column`,`$balance_column`,timestamp) VALUES 
	('$branch','$branch_sub_user','$date','$deposit_date','$company','DEBIT-Bank_Deposit','$narration','$amount','$new_balance','$timestamp')");
			
		if(!$insert_debit){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$debit_cash_id = getInsertID($conn);
		
	
	$insert_debit_table = Qry($conn,"INSERT INTO debit(debit_by,cash_id,section,branch,branch_user,company,amount,deposit_slip,ac_no,narr,date,timestamp) VALUES 
	('CASH','$debit_cash_id','Bank_Deposit','$branch','$_SESSION[user_code]','$company','$amount','$targetPath','$ac_no','$narration','$date','$timestamp')");
			
		if(!$insert_debit_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		
	if($flag)
		{
			MySQLCommit($conn);
			closeConnection($conn);
			echo "<script> 	
				alert('Bank deposit submitted !');
				window.location.href='./debit.php';
			</script>";
			exit();
		}
		else
		{
			unlink($targetPath);
			MySQLRollBack($conn);
			closeConnection($conn);
			Redirect("Error While Processing Request.","./debit.php");
			exit();
		}
		
		
?>