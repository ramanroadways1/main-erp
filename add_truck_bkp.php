<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$tno1 = escapeString($conn,strtoupper($_POST['tno_1']));
$tno2 = escapeString($conn,strtoupper($_POST['tno_2']));
$tno3 = escapeString($conn,strtoupper($_POST['tno_3']));

if(strlen($tno1)!=2)
{
	echo "<script>
		alert('Error: Please check vehicle number.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($tno2)!=2)
{
	echo "<script>
		alert('Error: Please check vehicle number.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$tno = $tno1.$tno2.$tno3;

$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$mobile2 = escapeString($conn,strtoupper($_POST['mobile2']));
$pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));
$wheeler = escapeString($conn,strtoupper($_POST['wheeler']));

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile2!='' AND strlen($mobile2)!=10)
{
	echo "<script>
		alert('Invalid Alternate Mobile No.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan_no))
{
	echo "<script>
			alert('Invalid PAN No.');
			$('#add_owner_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($mobile2,'0')===0 AND $mobile2!='')
{
	echo "<script>
		alert('Error: Check mobile number-2.');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_truck_no = Qry($conn,"SELECT tno FROM mk_truck WHERE tno='$tno'");
if(!$chk_truck_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_truck_no)>0)
{
	echo "<script type='text/javascript'>
		alert('Vehicle Number : $tno already registered.'); 
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$sourcePath_Pan = $_FILES['pan_copy']['tmp_name'];
$sourcePath_Rc_Front = $_FILES['rc_front']['tmp_name'];
$sourcePath_Rc_Rear = $_FILES['rc_rear']['tmp_name'];
$sourcePath_Dec = $_FILES['dec_copy']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['pan_copy']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. PAN Copy');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!in_array($_FILES['rc_front']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. RC Front Copy');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!in_array($_FILES['rc_rear']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. RC Rear Copy');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($sourcePath_Dec!='')
{
	if(!in_array($_FILES['dec_copy']['type'], $valid_types)){
		echo "<script>
			alert('Error : Only Image and PDF Upload Allowed. Declaration Copy');
			$('#add_owner_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	$dec_time=$timestamp;
}
else
{
	$dec_time="";
}

$fix_name = mt_rand().date('dmYHis');

$targetPath_Pan="truck_pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_front="truck_rc/Front_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_rear="truck_rc/Rear_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);

if($sourcePath_Dec!='')
{	
	$targetPath_Dec="truck_dec/".mt_rand().date('dmYHis').".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);
}
else
{
	$targetPath_Dec="";
}

// ImageUpload(1000,1000,$sourcePath_Pan);

if(!move_uploaded_file($sourcePath_Pan,$targetPath_Pan)){
	Redirect("Unable to Upload PAN Card Copy.","./");
	exit();
}

$chk_ocr = Qry($conn,"SELECT active FROM _ocr WHERE branch='$branch'");

$row_ocr = fetchArray($chk_ocr);
	
if($row_ocr['active']=="1")
{
	$fileName = $targetPath_Pan;
	$documentNumber = $pan_no;
	include '../Google_Cloud_Vision/pan_validation.php'; 
	
	if(isset($_SESSION['error_log_dl']))
		{
			$_SESSION['error_log_dl'] = mysqli_real_escape_string($conn,$_SESSION['error_log_dl']);
		}
		
		if(isset($_SESSION['error_log_pan']))
		{
			$_SESSION['error_log_pan'] = mysqli_real_escape_string($conn,$_SESSION['error_log_pan']);
		}
		
	if(strlen($response_photo)==10)
	{
		// if()substr($response_photo, -1, 1);
		if($response_photo!=$pan_no)
		{
			mysqli_set_charset($conn, 'utf8');
			
			$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('OWNER_PAN','$pan_no','$response_photo','PAN_NOT_MATCHING','".$_SESSION["error_log_pan"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
				
			unlink($targetPath_Pan);
			// errorLog("OCR PAN: PAN VERIFICATION FAILED. PAN:$pan_no. OCR:$response_photo  Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
			echo "<script type='text/javascript'> 
				alert('PAN number verification failed ! Please check PAN number.');
				$('#loadicon').hide();
				$('#add_owner_button').attr('disabled',false);
			</script>";
			exit();
		}
	}
	else if($response_photo == 'NOT_FOUND')
	{
		mysqli_set_charset($conn, 'utf8');
		
		$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('OWNER_PAN','$pan_no','$response_photo','NOT_FOUND','".$_SESSION["error_log_pan"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
		unlink($targetPath_Pan);
		// errorLog("OCR PAN: DOC NO. NOT_FOUND. PAN: $pan_no. Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'> 
			alert('Error while scanning the document. Please upload a valid and scannable document !');
			$('#loadicon').hide();
			$('#add_owner_button').attr('disabled',false);
		</script>";
		exit();
	}
	else if($response_photo == 'Invalid_Response')
	{
		mysqli_set_charset($conn, 'utf8');
		
		$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('OWNER_PAN','$pan_no','$response_photo','Invalid_Response','".$_SESSION["error_log_pan"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
		unlink($targetPath_Pan);
		// errorLog("OCR PAN: Invalid response. PAN: $pan_no. Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'> 
			alert('Error while scanning the document. Please upload a valid and scannable document !');
			$('#loadicon').hide();
			$('#add_owner_button').attr('disabled',false);
		</script>";
		exit();
	}
}

ImageUpload(1000,1000,$sourcePath_Rc_Front);

if(!move_uploaded_file($sourcePath_Rc_Front,$targetPath_Rc_front)){
	unlink($targetPath_Pan);
	Redirect("Unable to Upload RC Front Copy.","./");
	exit();
}

ImageUpload(1000,1000,$sourcePath_Rc_Rear);

if(!move_uploaded_file($sourcePath_Rc_Rear,$targetPath_Rc_rear)){
	unlink($targetPath_Pan);
	unlink($targetPath_Rc_front);
	Redirect("Unable to Upload RC Rear Copy.","./");
	exit();
}

if($sourcePath_Dec!='')
{
	ImageUpload(1000,1000,$sourcePath_Dec);
	
	if(!move_uploaded_file($sourcePath_Dec,$targetPath_Dec)){
		unlink($targetPath_Pan);
		unlink($targetPath_Rc_front);
		unlink($targetPath_Rc_rear);
		Redirect("Unable to Upload Declaration Copy.","./");
		exit();
	}
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO mk_truck(tno,wheeler,name,mo1,mo2,pan,up1,rc_rear,up4,full,up6,dec_upload_time,branch,branch_user,
timestamp) VALUES ('$tno','$wheeler','$name','$mobile','$mobile2','$pan_no','$targetPath_Rc_front','$targetPath_Rc_rear',
'$targetPath_Pan','$addr','$targetPath_Dec','$dec_time','$branch','$branch_sub_user','$timestamp')");
	
$oid = mysqli_insert_id($conn);

if(!$insert)
{
	unlink($targetPath_Pan);
	unlink($targetPath_Rc_front);
	unlink($targetPath_Rc_rear);
	unlink($targetPath_Dec);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($pan_no=="AAKCS4923M" || $pan_no=="AAMCS2305R")
{
	$insert2 = Qry($conn,"INSERT INTO _by_pass_pod_lock(party_id,pan,is_active,owner_broker,timestamp) VALUES ('$oid','$pan_no','1',
	'OWNER','$timestamp')");

	if(!$insert2)
	{
		unlink($targetPath_Pan);
		unlink($targetPath_Rc_front);
		unlink($targetPath_Rc_rear);
		unlink($targetPath_Dec);
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Vehicle : $tno. Added Successfully.');
		$('#loadicon').hide();
		$('#add_owner_button').attr('disabled',false);
		$('#close_add_owner').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Error...');
		$('#loadicon').hide();
		$('#add_owner_button').attr('disabled',false);
	</script>";
	exit();
}
?>