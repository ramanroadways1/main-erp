<?php
require_once("./connection.php");

$status =array();

$m=$_POST['month'];
$y = $_POST['year'];

$number = cal_days_in_month(CAL_GREGORIAN, $m, $y); // 31

$date1 =$y."-0".$m."-01";
$date2 =$y."-0".$m."-".$number;
?>
<!doctype html>
<html lang="en">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<link href="<?php echo $baseUrl1 ?>google_font.css" rel="stylesheet">

<script>
$(document).ready(function() {
    $('#example').DataTable( {
		"scrollY": 500,
        "scrollX": true,
		"bProcessing": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            // 'pdfHtml5'
        ]
    } );
} );
</script>

<a href="./attendance_sheet.php">
	<button style="margin-top:10px;margin-left:10px" class="btn btn-primary">
	<span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>

<br />
<br />

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
<div class="row">
<div class="form-group col-md-12 table-responsive">
<table id="example" class="display table table-striped" style="width:100%;font-size:12.5px;">
    <thead>
	
	<tr>
		<th>Month/Year</th>	
		<th>Name</th>	
		<th>Branch</th>
		<th>Total P</th>
		<th>Total A</th>
		<th>Total HD</th>
		<?php for ($i=1; $i <= $number ; $i++) { 
			echo "<th>".$i."</th>";
		} ?>
	
	</tr>
	</thead>
        <tbody>
<?php
$sql = Qry($conn, "SELECT name,code,branch FROM emp_attendance WHERE branch='$branch' AND status='3' ORDER BY id ASC");	

if(!$sql){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

while($row =fetchArray($sql)){
	$code = $row['code'];

$sql1 = Qry($conn, "SELECT status FROM emp_attendance_save WHERE date BETWEEN '$date1' AND '$date2' AND code='$code'");

if(!$sql1){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

while ($row2=fetchArray($sql1)) {
	$data = $row2['status'];
	array_push($status, $data);
}
$count = count($status);
$p=array();
$a = array();
$hd=array();

for ($i=0; $i <$count ; $i++) { 
	$sign =$status[$i];
	if($sign == 'A'){
		array_push($a, 'A');
	}
	elseif ($sign == 'P') {
		array_push($p, 'P');
		# code...
	}
	elseif ($sign == 'HD') {
		array_push($hd, 'HD');
		# code...
	}
}
$countp = count($p);
$counta = count($a);
$counthd = count($hd);
?>
<tr>
	<td><?php echo $m."/".$y; ?></td>
	<td><?php echo $row['name']; ?></td>
	<td><?php echo $row['branch']; ?></td>
	<td><?php echo $countp; ?></td>
	<td><?php echo $counta; ?></td>
	<td><?php echo $counthd; ?></td>
	
	<?php for ($i=0; $i <$number ; $i++) { 
		if($i<=$count){
			echo "<td>".@$status[$i]."</td>";
		}
		else{
			echo "<td></td>";
		}
		} ?>
</tr>
<?php
$status = array();
}
?>
 </tbody>
</table>
</div>
</div>
</div>
</body>
</html>