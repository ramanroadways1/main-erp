<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$company = escapeString($conn,($_POST['company']));

$get_card_list = Qry($conn,"SELECT cardno,vehicle,id,rilpre FROM diesel_api.dsl_cards WHERE active='1' and company='$company' ORDER by id ASC");
if(!$get_card_list){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<option value=''>--select--</option>";

while($row = fetchArray($get_card_list))
{
	echo "<option value='$row[cardno]_$row[vehicle]_$row[id]_$row[rilpre]'>$row[cardno] - ($row[vehicle])</option>";
}
 

	echo "<script type='text/javascript'> 
		$('#card_div').show();
		$('#card_no').attr('required',true);
		$('#card_no').selectpicker('refresh');
		$('#loadicon').hide();
	</script>";
?>