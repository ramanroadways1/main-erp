<?php
require_once("connection.php");

$date = date("Y-m-d");

$veh_type = escapeString($conn,$_POST['veh_type']);

if($veh_type=='OWN')
{
	$id_own_veh = escapeString($conn,$_POST['id']);
	$tno_own_db = escapeString($conn,strtoupper($_POST['tno']));
	
	if($id_own_veh=='' || $tno_own_db=='')
	{
		echo "<script>
			Swal.fire({
			icon: 'error',
			title: 'Warning',
			text: 'Please enter own vehicle number !'
			})
		$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$verify_own_vehicle = Qry($conn,"SELECT o.tno,o.wheeler,d.mobile as driver_mobile 
	FROM dairy.own_truck AS o 
	LEFT OUTER JOIN dairy.driver AS d ON d.code=o.driver_code 
	WHERE o.id='$id_own_veh'");
	
	if(!$verify_own_vehicle){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($verify_own_vehicle)==0)
	{
		echo "<script>
			alert('Vehicle not found !!');
			window.location.href='_fetch_lr_entry.php';
		</script>";
		exit();
	}
	
	$row_verify_vehicle = fetchArray($verify_own_vehicle);
	
	if($row_verify_vehicle['tno']!=$tno_own_db)
	{
		echo "<script>
			alert('Vehicle not verified !!');
			window.location.href='_fetch_lr_entry.php';
		</script>";
		exit();
	}
	
	$_SESSION['verify_vehicle_type'] = "OWN";
	$_SESSION['verify_vehicle'] = $row_verify_vehicle['tno'];
	$_SESSION['verify_vehicle_wheeler'] = $row_verify_vehicle['wheeler'];
	$_SESSION['verify_driver_mobile_no'] = $row_verify_vehicle['driver_mobile'];
	$_SESSION['verify_vehicle_id'] = $id_own_veh;
	$_SESSION['verify_driver'] = "OWN_TRUCK";
	$_SESSION['verify_driver_id'] = "NA";
	
	echo "<script>
		$('.veh_sel_dropdown').attr('disabled',true);
		$('#VEH_OWN').attr('disabled',false);
	</script>";
}
else if($veh_type=='MARKET')
{
	if(!isset($_SESSION['verify_vehicle_id']))
	{
		echo "<script>
			Swal.fire({
			icon: 'error',
			title: 'Warning',
			text: 'Vehicle not verified'
			})
		$('#loadicon').hide();
		</script>";
		exit();
	}

	if(!isset($_SESSION['verify_driver_id']))
	{
		echo "<script>
			Swal.fire({
			icon: 'error',
			title: 'Warning',
			text: 'Driver not verified'
			})
		$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$_SESSION['verify_vehicle_type'] = "MARKET";
}
else
{
	echo "<script>
			window.location.href='_fetch_lr_entry.php';
		</script>";
		exit();
}
?>

<div id='load_lr_modal_create_lr'></div>
	
<script>
		jQuery.ajax({
		url: './_lr_modal.php',
		data: 'ok=' + 'OK' + '&myVar=' + 'OK',
		type: 'POST',
		success: function(data) {
			$('#load_lr_modal_create_lr').html(data);
		},
		error: function() {}
	});
</script>