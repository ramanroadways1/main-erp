<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" target="_blank" action="excel_download_file.php" method="POST">	
	
<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Excel - Downloads</h4></center>
		<br />
	</div>

	<div class="form-group col-md-12">
		<label>Select File <font color="red">*</font></label>
		<select onchange="Option(this.value)" name="file" class="form-control" required="required">
			<option value="">Select File</option>
			<option value="FM_ADV">Freight Memo Advance (by adv. date)</option>
			<option value="FM_BAL">Freight Memo Balance (by bal. date)</option>
			<option value="LR_FM">Lr Entry - With Adv (by lr date)</option>
			<option value="LR">Lr Entry</option>
			<option value="LR_T">Lr Entry With Transshipment Flag</option>
			<option value="EXP">Expense Voucher</option>
			<option value="TDV">Truck Voucher</option>
			<option value="OWN">Own Truck Form</option>
			<option value="MB">Market Bilty</option>
			<option value="CASH">Cash Book</option>
			<option value="PASS">Pass Book</option>
			<option value="DSL">Diesel Book</option>
			<option value="RTGS">RTGS Sheet</option>
			<option value="CR">Your Credits</option>
			<option value="DR">Your Debits</option>
		</select>
	</div>
	
<script>	
function Option(elem)
{
	if(elem=='CASH' || elem=='PASS')
	{
		$('#company_div').show();
		$('#company').attr('required',true);
	}
	else
	{
		$('#company_div').hide();
		$('#company').attr('required',false);
	}
}
</script>	
	
	<div class="form-group col-md-12" style="display:none" id="company_div">
		<label>Select Company <font color="red">*</font> <sup style="color:#FFF">(for cashbook only)</sup></label>
		<select class="form-control" name="company" id="company" required="required">
			<option value="">SELECT COMPANY</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div class="form-group col-md-12">
		<label>From Date <font color="red">*</font></label>
		<input name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">
		<label>To Date <font color="red">*</font></label>
		<input name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="Download" />
	</div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>