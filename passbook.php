<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">
	
<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">	

<form autocomplete="off" action="_fetch_passbook.php" method="POST">	

<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="col-md-12">	
		<br />
		<br />
		<center><h4 style="color:#FFF">Passbook Summary</h4></center>
		<br />
		<br />
	</div>

	<div class="form-group col-md-12">
		<label>Select Company <font color="red">*</font></label>
		<select class="form-control" name="company" required="required">
			<option value="">SELECT COMPANY</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div class="form-group col-md-12">
		<label>From Date <font color="red">*</font></label>
		<input id="lr_date" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">
		<label>To Date <font color="red">*</font></label>
		<input id="lr_date" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="View Passbook" />
	</div>
</div>

</div>
</div>
</div>
</form>

<div id="addbal_result"></div>
</body>
</html>