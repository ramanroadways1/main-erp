<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$min = date("Y-m-d", strtotime("-2 days"));
?>
<html>
<head>
<meta charset="utf-8"><title>RRPL</title> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>	

<script>
        function convert_number(number) {
            if ((number < 0) || (number > 999999999)) {
                return "Number is out of range";
            }
            var Gn = Math.floor(number / 10000000); /* Crore */
            number -= Gn * 10000000;
            var kn = Math.floor(number / 100000); /* lakhs */
            number -= kn * 100000;
            var Hn = Math.floor(number / 1000); /* thousand */
            number -= Hn * 1000;
            var Dn = Math.floor(number / 100); /* Tens (deca) */
            number = number % 100; /* Ones */
            var tn = Math.floor(number / 10);
            var one = Math.floor(number % 10);
            var res = "";

            if (Gn > 0) {
                res += (convert_number(Gn) + " Crore");
            }
            if (kn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " Lakhs");
            }
            if (Hn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " Thousand");
            }

            if (Dn) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " hundred");
            }


            var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

            if (tn > 0 || one > 0) {
                if (!(res == "")) {
                    res += " and ";
                }
                if (tn < 2) {
                    res += ones[tn * 10 + one];
                } else {

                    res += tens[tn];
                    if (one > 0) {
                        res += ("-" + ones[one]);
                    }
                }
            }

            if (res == "") {
                res = "zero";
            }
            return res;
        }

        function toWords() {
			document.getElementById("amountw").value = convert_number(document.getElementById("amount").value);
        }
    </script>
 
<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}

.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 
	
<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_own_vehicle_for_diesel.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

</head>

<a href="../"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<form action="./ftruck2.php" method="post" autocomplete="off">    
	
<div class="container-fluid">
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
	
	<div class="form-group col-md-12">
		<center><h4>Truck - Voucher</h4></center>
		<br />
	</div>	
	
	<div class="form-group col-md-4">
		<label>Vou Date <font color="red">*</font></label>
        <input value="" name="vou_date" type="date" class="form-control" min="<?php echo $min; ?>" max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Vehicle No. <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" id="truck_no" type="text" name="tno" required />
	</div>
	
	<div class="form-group col-md-4">
		<label>Payment Mode <font color="red">*</font></label>
			<select class="form-control" id="payment_mode" onchange="payBy(this.value);" name="payment_mode" required="required">
			<option value="">Mode of payment</option>
			<option value="CASH">CASH</option>
			<option value="CHEQUE">CHEQUE</option>
			<option value="NEFT">RTGS/NEFT</option>
		</select>
	</div>
	
	<div class="form-group col-md-4">
		<label>Driver Name <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" type="text" name="driver_name" required />
	</div>
	
	<div class="form-group col-md-4">
		<label>Amount <font color="red">*</font></label>
		<input class="form-control" min="10" id="amount" type="number" oninput="toWords();CheckCash(this.value);" name="amount" required />
	</div>
	
	<div class="form-group col-md-4">
		<label>LR No. <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-z, A-Z0-9]/,'')" name="lrno" class="form-control" min="0" placeholder="Fill EMPTY if No LR" type="text" required />
	</div>
	
	<div class="form-group col-md-6">
		<label>Amount (in words) <font color="red">*</font></label>
		<input class="form-control" type="text" name="amount_word" id="amountw" required readonly />
	</div>
	
	<script>
	function CheckCash(cash)
	{
		var mode = $('#payment_mode').val();
		
		if(mode=='')
		{
			$('#payment_mode').focus();
			$('#amount').val('');
			$('#amountw').val('');
		}
		else
		{
			if(mode=='CASH' && cash>10000)
			{
				alert('Max Cash Allowed is 10,000 !');
				$('#amount').val('');
				$('#amountw').val('');				
			}
		}
	}
	</script>

	
	<div class="form-group col-md-8" id="chq_div" style="display:none">
		<div class="row">
			<div class="form-group col-md-6">
				<label>Cheque No. <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9]/,'')" class="form-control" type="text" name="cheque_no" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Bank Name <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" class="form-control" name="bank" />
			</div>
		</div>
	</div>
	
	<div class="form-group col-md-12" id="neft_div" style="display:none;">
		<div class="row">
			<div class="form-group col-md-4">
				<label>PAN No. </label>
				<input pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN()" 
				type="text" id="pan_no" onblur="ValidatePAN();" class="form-control" minlength="10" maxlength="10" name="pan" />
			</div>
		
			<div class="form-group col-md-4">
				<label>A/c Holder <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="ac_holder" class="form-control" type="text" 
				name="acname" />
			</div>
		
			<div class="form-group col-md-4">
				<label>A/c No <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" id="ac_no" type="text" minlength="6" 
				class="form-control" name="acno" />
			</div>
	
			<div class="form-group col-md-4">
				<label>Bank Name <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="ac_bank" type="text" 
				class="form-control" minlength="3" name="bank_name" />
			</div>
	
			<div class="form-group col-md-4">
				<label>IFSC Code <font color="red">*</font> </label>
				<input pattern="[a-zA-Z]{4}[0]{1}[a-zA-Z0-9]{6}" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" type="text" 
				id="ac_ifsc" class="form-control" minlength="11" maxlength="11" name="ifsc" />
			</div>
		</div>	
	</div>
     
	<div class="form-group col-md-12">
		
		<label style="font-size:13px">Amount Description <font color="red">*</font></label>
			
		<div class="col-md-12" style="border:2px solid brown;border-style:dotted;border-radius:10px;padding:10px;">
					
			<div class="form-group col-md-4">
				<label>SALARY</label>
				<input type="number" class="form-control" min="0" id="salary" name="salary" value="0" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>TRIP EXP.</label>
				<input type="number" class="form-control" min="0" id="trip" name="trip" value="0" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>INSURANCE</label>
				<input type="number" class="form-control" min="0" id="ins" name="ins" value="0" required />	
			</div>
			
			<div class="form-group col-md-4">
				<label>RTO</label>
				<input type="number" class="form-control" min="0" id="rto" name="rto" value="0" required />
			</div>
		  
			<div class="form-group col-md-4">
				<label>CASH DIESEL</label>
				<input type="number" class="form-control" id="dsl" min="0" name="dsl" value="0" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>OTHER</label>
				<input type="number" class="form-control" oninput="OtherAmount(this.value)" id="other" min="0" name="other" value="0" required />
			</div>
			
		</div>
		
	</div>

<script type="text/javascript">
function OtherAmount(amount)
{     
	if($('#other').val() > 0)
	{
		$("#other_div").show();
		$("#other1").prop('required','required');
	}
	else
	{
		$("#other_div").hide();
		$("#other1").prop('required',null);
	}
}
</script>
	
	<div class="form-group col-md-4" id="other_div" style="display:none">	
		<label>Other Amount Narr. <font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9]/,'')" class="form-control" placeholder="Other Amount's Narration.." type="text" name="other1" id="other1"></textarea> 
	</div>

	<div class="form-group col-md-4">
		<label>Total Amount <font color="red">*</font></label>
		<input class="form-control" placeholder="Total" type="text" name="total" id="total" readonly /> 
	</div>

	<div class="form-group col-md-4">
		<label>Narration (from,to statoin etc..)<font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'')" type="text" class="form-control" name="narration" required="required"></textarea>
	</div>
	
	<div class="form-group col-md-6">
		<label>Cashier Signature <font color="red"><sup>*</sup></font></label>
		<div id="newsign">
			<div class="m-signature-pad--body">
				<canvas class="signature" style="width:100%;height:180px;max-width:100%;border:1.5px #CCC solid;background-color:white;"></canvas>
			</div>
			<div class="m-signature-pad--footer" style="padding-top:10px;">
				<button type="button" class="btn btn-primary" data-action="clear">Clear</button>
				<button type="button" class="btn btn-primary" data-action="save">Save</button>
			</div>
		</div>
	</div>

	<div class="form-group col-md-6">
		<label> Signature Preview</label>
		<br>
		 <script src="js/signature_pad.js"></script>
		 <script src="js/app_exp_vou.js"></script>
		<img id="myImg" name="myImg" style="width:40%;" />
		<input type="hidden" id="sign" name="signature" />
	</div>
	
	<div class="form-group col-md-12">
		<div class="row">
			<div class="form-group col-md-4" style="color:red"><span id="sign_result">Please upload signature first.</span></div>
			<div class="form-group col-md-4" style="color:red"><span id="amount_result">Amount not matching.</span></div>
			
			<div class="form-group col-md-4">
				<button type="submit" style="display:none" id="truck_vou_save" class="btn pull-right btn-md btn-danger">
				<span class="glyphicon glyphicon-chevron-right"></span> NEXT Step</button>
			</div>
		</div>
	</div>
	
</div>
</div>
</div>
</body>
</html> 
</form> 

<script type="text/javascript">
$(function(){
$("#salary, #trip, #ins, #rto, #dsl, #other, #total").on("keydown keyup blur input", sum);
function sum(){
$("#total").val(Number($("#salary").val()) + Number($("#trip").val()) + Number($("#ins").val()) + Number($("#rto").val()) + Number($("#dsl").val()) + Number($("#other").val()));

if(Number($("#total").val())!=Number($("#amount").val()))
{
	$('#amount_result').html('Amount not matching.');
	$('#truck_vou_save').hide();
}
else
{
	$('#amount_result').html('');
	$('#truck_vou_save').show();
}

}
});
</script>
	
<script type="text/javascript">
setInterval(function(){ 
 var sign = $("#sign").val();
 if(sign == '')
 {
	$("#truck_vou_save").hide(); 
	$("#sign_result").html('Please upload signature first.'); 
 }
 else
 {
	 $("#truck_vou_save").show(); 
	 $("#sign_result").hide(); 
 }
	 
}, 500);
</script>
	
<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ac_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ac_ifsc").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("truck_vou_save").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ac_ifsc").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("truck_vou_save").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ac_ifsc").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("truck_vou_save").removeAttribute("disabled");
		}
 } 			
</script>

<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no").setAttribute("style","background-color:red;color:#FFF");
				$("#truck_vou_save").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no").setAttribute("style","background-color:green;color:#FFF");
				$("#truck_vou_save").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no").setAttribute("style","background-color:white;");
			$("#truck_vou_save").attr("disabled", false);
		}
  }
</script>	

<script>	
function payBy(elem){
	
	$('#amount').val("");
	$('#amountw').val("");
	
	if(elem=="CHEQUE")
	{
		$('#chq_div').show();
		$("#chq_div :input").attr('required',true);
		
		$('#neft_div').hide();
		$("#neft_div :input").attr('required',true);
		$("#pan_no").attr('required',false);
	}
	else if(elem == "NEFT")
	{
		$('#chq_div').hide();
		$("#chq_div :input").attr('required',false);
		
		$('#neft_div').show();
		$("#neft_div :input").attr('required',true);
		$("#pan_no").attr('required',false);
	}
	else
	{
		$('#chq_div').hide();
		$("#chq_div :input").attr('required',false);
		
		$('#neft_div').hide();
		$("#neft_div :input").attr('required',false);
		$("#pan_no").attr('required',false);
	}
}
</script>