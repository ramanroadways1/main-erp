<?php 
require_once './connection.php';

$name=escapeString($conn,strtoupper($_POST['party_name_to_add']));
$gst=escapeString($conn,strtoupper($_POST['gst_no']));
$mobile=escapeString($conn,strtoupper($_POST['mobile']));
$addr=escapeString($conn,strtoupper($_POST['addr']));
$pincode=escapeString($conn,strtoupper($_POST['pin_code']));
$state_name=escapeString($conn,strtoupper($_POST['state_name']));
$legal_name=escapeString($conn,strtoupper($_POST['legal_name']));
$trade_name=escapeString($conn,strtoupper($_POST['trade_name']));
		
$chkGST = Qry($conn,"SELECT name FROM consignor WHERE gst='$gst' AND gst!=''");
if(!$chkGST){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGST)>0)
{
	$rowGST = fetchArray($chkGST);
	echo "<script>
		alert('GST Number: $gst already registered with Party $rowGST[name].');
		$('#buttonPartyADD_consignor').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}


// $chkName = Qry($conn,"SELECT gst FROM consignor WHERE name='$name' AND name!=''");
// if(!$chkName){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// if(numRows($chkName)>0)
// {
	// $rowName = fetchArray($chkGST);
	// echo "<script>
		// alert('Party : $name already registered with GST Number $rowName[gst].');
		// $('#buttonPartyADD_consignor').attr('disabled',false);
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

$insertParty = Qry($conn,"INSERT INTO consignor (name,legal_name,trade_name,state,gst,mobile,pincode,addr,branch,timestamp) VALUES 
('$name','$legal_name','$trade_name','$state_name','$gst','$mobile','$pincode','$addr','$branch','".date("Y-m-d H:i:s")."')");

if(!$insertParty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
$insert_id = getInsertID($conn);
	
$updateCode = Qry($conn,"UPDATE consignor SET code='$insert_id' WHERE id='$insert_id'");
if(!$updateCode){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
	
	echo "<script>
		alert('Consignor : $name registered successfully.');
		$('#loadicon').hide();
		$('#buttonPartyADD_consignor').attr('disabled',false);
		$('#Con1Form')[0].reset();
	</script>";
	
?>