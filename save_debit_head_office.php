<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

	$amount = strtoupper(escapeString($conn,$_POST['amount']));
	$company = strtoupper(escapeString($conn,$_POST['company2']));
	$confirm = strtoupper(escapeString($conn,$_POST['confirm_by_db']));
	$otp = strtoupper(escapeString($conn,$_POST['otp']));
	$narration = strtoupper(escapeString($conn,$_POST['narration']));
	
	$narration = "Confirmed By- ".$confirm.", ".$narration;

if($confirm==""){
	Redirect("Confirmation not found.","./");
	exit();
}
	
$qry_fetch=Qry($conn,"SELECT mobile,name FROM ho_confirm WHERE username='$confirm'");

if(!$qry_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
				
if(numRows($qry_fetch)>0)
{
	$row_fetch = fetchArray($qry_fetch);
	$mobile=$row_fetch['mobile'];
}
else
{
	echo "<script>
		alert('No approval authority found.');
		window.location.href='./debit.php';
	</script>";
	exit();
}
	
	if($company=='')
	{
		echo "<script>
			alert('Unable to fetch Company.');
			window.location.href='./debit.php';
		</script>";
		exit();	
	}
	
// For Verify otp

// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// window.location.href='./debit.php';
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#dr_ho_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#dr_ho_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }
// }

VerifyOTP($otp,"dr_ho_button");

	// if($otp=='')
	// {
		// echo "<script>
				// alert('Error : Please Enter OTP.');
				// $('#dr_ho_button').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
		// exit();	
	// }
	// else
	// {
		// if($otp!=$_SESSION['session_otp'])
		// {
			// echo "<script>
				// alert('Error : Invalid OTP entered.');
				// $('#dr_ho_button').attr('disabled',false);
				// $('#loadicon').hide();
			// </script>";
			// exit();	
		// }
	// }
	
	$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	
	if(!$fetch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
     $row_bal = fetchArray($fetch_bal);
     $rrpl_cash = $row_bal['balance'];
     $rr_cash = $row_bal['balance2'];
		
	if($company=='RRPL' AND $rrpl_cash>=$amount) 
	{
		$debit_col="debit";
		$balance_col="balance";
		$newbal = $rrpl_cash - $amount;
	}	
	else if($company=='RAMAN_ROADWAYS' AND $rr_cash>=$amount)
	{
		$debit_col="debit2";
		$balance_col="balance2";
		$newbal =  $rr_cash - $amount;
	}
	else
	{
		echo "<script>
			alert('Insufficient Balance in company : $company !');
			window.location.href='./debit.php';
		</script>";
		exit();	
	}

	
StartCommit($conn);
$flag = true;
	
	$update_balance = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook = Qry($conn,"INSERT INTO cashbook(user_code,vou_type,`$debit_col`,desct,user,`$balance_col`,comp,date,timestamp) VALUES 
		('$_SESSION[user_code]','DEBIT-HO','$amount','$narration','$branch','$newbal','$company','$date','$timestamp')");
		
	if(!$insert_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$cash_id = getInsertID($conn);
	
	$insert_debit = Qry($conn,"INSERT INTO debit(debit_by,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
	('CASH','HO','$branch','$_SESSION[user_code]','$company','$amount','$narration','$date','$timestamp')");		
	
	if(!$insert_debit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('HO Debited Successfully !!');
		window.location.href='./debit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./debit.php");
	exit();
}
?>