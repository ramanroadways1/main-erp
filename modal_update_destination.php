<button style="display:none" type="button" id="open_destination_modal_lr" data-toggle="modal" data-target="#DestChangModal"></button>

<form style="font-size:13px" id="UpdateDestinataionLRCache" action="#" method="POST">
<div id="DestChangModal" style="background:#eee" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update destination. LR : <span id="dest_cache_lrno"></span>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-8">
				<label>Select Destination Location <font color="red"><sup>*</sup></font></label>
				<input id="dest_cache_location" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="dest_loc" 
				class="form-control" required="required">
			</div>
		</div>
      </div>
	  <input type="hidden" name="dest_loc_id" id="dest_cache_loc_id"> 
	  <input type="hidden" name="lrno" id="dest_cache_lrno2">
	  <input type="hidden" name="table_id" id="checkboxIdDestUpdate">
	  <input type="hidden" name="page_name1" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
	  
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" onclick="UnCheckDestCache($('#checkboxIdDestUpdate').val())" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<div id="result_form_DestUpdateModal"></div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateDestinataionLRCache").on('submit',(function(e) {
		$("#loadicon").show();
		e.preventDefault();
		$.ajax({
        	url: "./update_destination_loc_cache.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_form_DestUpdateModal").html(data);
				// $("#loadicon").hide();
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>