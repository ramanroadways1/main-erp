<?php 
require_once 'connection.php';

$tno = escapeString($conn,strtoupper($_POST['tno'])); 
$trip_no = escapeString($conn,strtoupper($_POST['trip_no'])); 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($tno))
{
	echo "<script>
			alert('Vehicle number not found !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

if(empty($trip_no))
{
	echo "<script>
			alert('Trip number not found !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

$chk_request = Qry($conn,"SELECT id FROM dairy.hisab_approval WHERE tno='$tno'");

if(!$chk_request){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_request)>0)
{
	echo "<script>
			alert('Duplicate request found !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

$chk_trip_no = Qry($conn,"SELECT t.trip_no,o.superv_id,u.title 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.tno 
LEFT OUTER JOIN dairy.user AS u ON u.id = o.superv_id
WHERE t.tno='$tno'");

if(!$chk_trip_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_trip_no) == 0)
{
	echo "<script>
			alert('Running trip not found : $tno !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

$row_trip_no = fetchArray($chk_trip_no);

if($row_trip_no['trip_no'] != $trip_no)
{
	echo "<script>
			alert('Trip number not verified !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

if(empty($row_trip_no['superv_id']))
{
	echo "<script>
			alert('Supervisor not found !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}
	
StartCommit($conn);
$flag = true;

$insert_req = Qry($conn,"INSERT INTO dairy.hisab_approval(tno,trip_no,supervisor,supervisor_name,branch,branch_user,timestamp) VALUES('$tno',
'$trip_no','$row_trip_no[superv_id]','$row_trip_no[title]','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Request Submitted Successfully');
		window.location.href='./hisab_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Error While Processing Request !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}	
?>