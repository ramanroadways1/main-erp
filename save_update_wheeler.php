<?php
require_once 'connection.php';

$tno = escapeString($conn,strtoupper($_POST['tno']));
$wheeler = escapeString($conn,strtoupper($_POST['wheeler']));
$timestamp = date("Y-m-d H:i:s");

$get_wheeler = Qry($conn,"SELECT wheeler FROM mk_truck WHERE tno='$tno'");
if(!$get_wheeler){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row = fetchArray($get_wheeler);

if($row['wheeler']==$wheeler)
{
	echo "<script type='text/javascript'>
			alert('Nothing to update !'); 
			$('#WheelerForm').trigger('reset');
			$('#updateWheelerBtn').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	closeConnection($conn);
	exit();
}

StartCommit($conn);
$flag = true;

$updateWheeler = Qry($conn,"UPDATE mk_truck set wheeler='$wheeler' WHERE tno='$tno'");

if(!$updateWheeler){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$tno','MARKET_TRUCK','WHEELER_UPDATE','$row[wheeler] to $wheeler.','$branch','$branch_sub_user','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
			alert('Wheeler Updated Successfully !'); 
			$('#WheelerForm').trigger('reset');
			$('#updateWheelerBtn').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>