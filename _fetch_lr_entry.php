<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>
 
<?php
include '_header3.php';
include './disable_right_click_for_index.php';

// if($_SESSION['user_code']!='032')
// {
	// echo "<script>
		// alert('LR Entry is under maintenance till 01:00 PM !');
		// window.location.href='./';
	// </script>";
	// exit();
// }
?>

<style>
label{
	text-transform:none;
}

@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<script>
$(function() {
		$("#from_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_edit').val(ui.item.value);   
            $('#from_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_edit').val("");   
			$('#from_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_edit').val(ui.item.value);   
            $('#to_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_edit').val("");   
			$('#to_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#dest_zone_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#dest_zone_edit').val(ui.item.value);   
            $('#dest_zone_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#dest_zone_edit').val("");   
			$('#dest_zone_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {  
      $("#consignee_edit").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee_edit').val(ui.item.value);   
               $('#consignee_id_edit').val(ui.item.id);     
               $('#con2_gst_edit').val(ui.item.gst);     
               $('#consignee_pincode').val(ui.item.pincode);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee_edit").val('');
				$("#consignee_id_edit").val('');
				$("#con2_gst_edit").val('');
				$("#consignee_pincode").val('');
                alert('Consignee does not exist !'); 
              } 
              },
			}); 
});

$(function() {  
      $("#item1_edit").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/lr_items.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#item1_edit').val(ui.item.value);   
               $('#item_id_edit').val(ui.item.id);
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#item1_edit").val('');
				$("#item_id_edit").val('');
                alert('Item does not exist !'); 
              } 
              },
			}); 
}); 
</script>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">LR/Dispatch Summary :</span></h5>
		</div>
		<div class="col-md-4">
			<button id="lr_create_btn" type="button" class="btn btn-sm btn-default pull-right" onclick="LoadModal()">
			<span class="glyphicon glyphicon-pencil"></span> Create NEW LR</button>
		</div>	
	</div>	
</div>

<?php
echo "<div id='lr_error_text' style='display:none' class='form-group col-md-12'>
	<h5 style='color:red'>Some of your LRs are older than 10 Days and not Created any Voucher against them. So You Can't Create NEW LR. Contact Head-Office or Clear them first.
	<br>
	<br>
	आपके कुछ LRs 10 दिनों से पुराने हैं और उनके सामने कोई वाउचर नहीं बनाया गया है। तो आप नया LR नहीं बना सकते। हेड-ऑफिस से संपर्क करें या पहले वाउचर बनाएं ।
	</h5>
</div>";
?>

	<input type="hidden" id="last_lrno_auto">
		
		<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>LRid</th>
					<th>LR_No</th>
					<th>Vehicle_Type</th>
					<th>Truck_No</th>
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>From <span class="glyphicon glyphicon-filter"></span></th>
					<th>To <span class="glyphicon glyphicon-filter"></span></th>
					<th>Consignor & Consignee</th>
					<th>DoNo/InvNo/ShipNo</th>
					<th>Awt <span class="glyphicon glyphicon-filter"></span></th>
					<th>Cwt <span class="glyphicon glyphicon-filter"></span></th>
					<th>Item</th>
					<th>Edit</th>
					<th>Delete</th>
					<th>e-LR</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<div id="edjsakld"></div>

<script type="text/javascript">
function FetchLRs(){
$("#loadicon").show();
jQuery.ajax({
		url: "get_lr_pending.php",
		data: 'branch=' + '<?php $branch; ?>',
		type: "POST",
		success: function(data) {
		$("#edjsakld").html(data);
	},
	error: function() {}
});

// alert('hiee');
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 {
        targets: 12, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-primary" onclick="EditButton('+full[0]+')" id="edit_button_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>';
        }
      },
      {
		targets: 13, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-danger" onclick="DeleteButton('+full[0]+')" id="delete_button_'+full[0]+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>';
        }
      },
	  {
		targets: 14, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<input type="hidden" id="lrno_input_'+full[0]+'" value="'+full[1]+'"><button type="button" class="btn-xs btn-warning" onclick="PrintLR('+full[0]+')" id="print_button_'+full[0]+'"><i class="fa fa-print" aria-hidden="true"></i> Print</button>';
        }
      }
		], 
        "serverSide": true,
        "ajax": "_fetch_lr_server.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchLRs();
</script>

<script type="text/javascript">
function EditButton(id)
{
	$('#edit_button_'+id).attr('disabled',true);
	$('#lr_edit_id').val(id);
	$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_lr_details_for_edit.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#function_result").html(data);
	},
	error: function() {}
	});
}

function DeleteButton(id)
{
	if(confirm("Do you really want to delete this LR ?") == true) {
		$('#delete_button_'+id).attr('disabled',true);
		// alert(id);
		$("#loadicon").show();
			jQuery.ajax({
			url: "delete_lr.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#function_result").html(data);
			},
			error: function() {}
		});
	}
}

function PrintLR(id)
{
	var lrno = $('#lrno_input_'+id).val();
	$('#lrno_print').val(lrno);
	$('#Form_eLR').submit();
}
</script>

<form action="e_lr2.php"method="POST" id="Form_eLR" target="_blank">
	<input type="hidden" value="LR" name="by" />
	<input type="hidden" id="lrno_print" name="lrno" />
</form>

<?php include("./modal_edit_lr.php"); ?>

<div id="function_result"></div>
<div id="modal_load_data"></div>
<div id="modal_load_data_lr"></div>


<script>
function ResetVehicle()
{
	if(confirm("Are you sure ??")==true)
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "reset_vehicle_data.php",
			data: 'id=' + 'OK',
			type: "POST",
			success: function(data) {
				$("#function_result").html(data);
		},
		error: function() {}
		});
	}
}

function LoadModal()
{
	$('#lr_create_btn').attr('disabled',true);
	$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_data_create_lr.php",
		data: 'id=' + 'OK',
		type: "POST",
		success: function(data) {
			$("#function_result").html(data);
	},
	error: function() {}
	});
}
</script>

<?php
if(isset($_SESSION['verify_vehicle']) AND isset($_SESSION['verify_driver']))
{
	echo "<script>
		$('#lr_create_btn')[0].click();
	</script>";
}
?>