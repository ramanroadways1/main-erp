<?php 
require_once 'connection.php';

$lrno = escapeString($conn,strtoupper($_POST['lrno'])); 

if(is_numeric($lrno)!=1)
{
	echo "<script type='text/javascript'>
			alert('Invalid LR numbe entered !');
			window.location.href='./lr_cancel.php';
		</script>";	
	exit();
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$chk_lr = Qry($conn,"SELECT id FROM lr_check WHERE lrno='$lrno'");
if(!$chk_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_lr)>0)
{
	echo "<script type='text/javascript'>
			alert('LR entry already exists. Please delete LR first and try to cancel LR again !');
			window.location.href='./lr_cancel.php';
		</script>";	
	exit();
}
	
$chk_bank = Qry($conn,"SELECT id,branch,company,stock_left FROM lr_bank WHERE $lrno>=from_range AND $lrno<=to_range");

if(!$chk_bank){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	$numRows_bilty = numRows($chk_bank);

	if($numRows_bilty==0)
	{
		echo "<script type='text/javascript'>
			alert('LR stock not updated. Please contact head-office.');
			window.location.href='./lr_cancel.php';
		</script>";	
		exit();
	}

	$row_stock=fetchArray($chk_bank);

	$stock_left = $row_stock['stock_left'];
	$bilty_id_db = $row_stock['id'];
	$bilty_branch_db = $row_stock['branch'];
	$company = $row_stock['company'];

	if($numRows_bilty>1)
	{
		echo "<script type='text/javascript'>
			alert('Error: Duplicate LR Series found. Bilty Id $bilty_id_db. Please contact head-office.');
			window.location.href='./lr_cancel.php';
		</script>";
		exit();
	}

	if($stock_left!='' AND $stock_left<=0)
	{
		echo "<script type='text/javascript'>
			alert('Bilty Book not in stock. Please contact head-office.');
			window.location.href='./lr_cancel.php';
		</script>";
		exit();
	}
		
	if($bilty_branch_db!=$branch)
	{
		echo "<script type='text/javascript'>
			alert('Not your LR. LR Book belongs to $bilty_branch_db Branch !');
			window.location.href='./lr_cancel.php';
		</script>";
		exit();
	}
	

StartCommit($conn);
$flag = true;

$query_lr = Qry($conn,"INSERT INTO lr_sample(company,branch,lrno,date,crossing,cancel,timestamp) VALUES('$company','$branch','$lrno',
'$date','NO','1','$timestamp')");
if(!$query_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$query_lr2 = Qry($conn,"INSERT INTO lr_check(branch,lrno,bilty_id,timestamp) VALUES('$branch','$lrno','$bilty_id_db','$timestamp')");
if(!$query_lr2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_stock=Qry($conn,"UPDATE lr_bank SET stock_left=stock_left-1 WHERE id='$bilty_id_db'");
	
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp,branch_user) VALUES 
('$lrno','','LR_CANCEL','','$branch','BRANCH','$timestamp','$branch_sub_user')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
	<script>
		alert('LR Cancelled Successfully');
		window.location.href='./lr_cancel.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>