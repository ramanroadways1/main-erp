<form id="PucForm" action="#" method="POST">
<div id="PucModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update PUC
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="puc_veh_no" name="veh_no" class="form-control" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC Copy <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="puc_copy" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC Start Date <font color="red">*</font></label>
				<input onchange="ChkPucDate(this.value)" id="puc_start_date" name="start_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC Expiry Date <font color="red">*</font></label>
				<input onchange="if($('#puc_start_date').val()==''){alert('Select Inspection date first !');$(this).val('');}" id="puc_end_date" name="end_date" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
	
			<script>
			function ChkPucDate(from_date)
			{
				var nextDay = new Date(from_date);
				nextDay.setDate(nextDay.getDate() + 180);
				$('#puc_end_date').attr("min",nextDay.toISOString().slice(0,10));
			}
			</script>
			
		</div>
      </div>
	  <input type="hidden" name="id" id="puc_form_id">
	  <div id="result_puc_form"></div>
      <div class="modal-footer">
        <button type="submit" id="puc_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" onclick="$('#PucForm')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#PucForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#puc_button").attr("disabled", true);
	$.ajax({
        	url: "./save_puc_update.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_puc_form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>