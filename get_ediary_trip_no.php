<?php
require("./connection.php");

$tno = escapeString($conn,strtoupper($_POST['tno']));

$get_trip_no = Qry($conn,"SELECT (SELECT trip_no FROM dairy.trip WHERE tno='$tno' ORDER BY id ASC LIMIT 1) as 'trip_no',
(SELECT from_station FROM dairy.trip WHERE tno='$tno' ORDER BY id ASC LIMIT 1) as 'from1',
(SELECT to_station FROM dairy.trip WHERE tno='$tno' ORDER BY id DESC LIMIT 1) as 'to1'");

if(!$get_trip_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_trip_no)==0)
{
	echo "<script>$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

$row = fetchArray($get_trip_no);

$get_driver_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code=(SELECT driver_code FROM dairy.own_truck WHERE tno='$tno')");

$row2 = fetchArray($get_driver_name);

echo "<script>$('#trip_no').val('$row[trip_no]');$('#driver_name').val('$row2[name]');$('#trip_station').val('$row[from1] to $row[to1]');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";

?>