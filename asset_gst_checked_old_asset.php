<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$gst_no = trim(escapeString($conn,strtoupper($_POST['gst_no'])));
$gst_selection = escapeString($conn,($_POST['gst_selection']));

if(strlen($gst_no)!=15)
{
	echo "<script>
		alert('Invalid GST number !');
		$('#gst_no_1').attr('readonly',false);
		$('#gst_validate_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($gst_selection!='YES' AND $gst_selection!='NO')
{
	echo "<script>
		alert('Select invoice type first !');
		$('#gst_no_1').attr('readonly',false);
		$('#gst_validate_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

	if(substr($gst_no, 0, 2)=="24"){
		echo "<script>$('#gst_type_old_asset').val('CGST+SGST');</script>";
	}
	else{
		echo "<script>$('#gst_type_old_asset').val('IGST');</script>";
	}

echo "<script>
		$('.gst_sel_opt').attr('disabled',true);
		$('#gst_sel_opt_$gst_selection').attr('disabled',false);
		$('#amount_old_assset').attr('readonly',true);
		$('#gst_value_old_asset').attr('readonly',false);
		$('#gst_value_old_asset').val('');
		$('#gst_amount_old_asset').val('0');
		$('#discount_1').val('0');
		$('#total_amount_old_asset').val($('#amount_old_assset').val());
		$('#loadicon').fadeOut('slow');
		$('#gst_validate_btn').attr('disabled',true);
		$('#gst_validate_btn').html('Validated');
		$('#gst_validate_btn').attr('class','btn btn-sm btn-success');
		$('#btn_old_asset_add').attr('disabled',false);
</script>";

?>