<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	font-size:12px !important;
}

input[type='text'],input[type='number'],input[type='file'],input[type='date'],input[type='date'],input[type='file'],select,textarea{
	font-size:12px !important;
}
select>option,option{
	font-size:12px !important;
}
</style>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid"> 
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
		</div>			
	</div>		
	
<div class="row">	
	<div class="col-md-12">
	
		<div class="col-md-8"></div>
		
		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button data-toggle="modal" data-target="#view_assetReq" style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-danger">
			<span class="fa fa-exclamation-triangle"></span> Pending REQ. (<span id="pending_Staus"></span>)</button></a>
		</div>

		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button data-toggle="modal" data-target="#AssetReqModal" style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success">
			<span class="fa fa-plus-circle"></span> New Veh. REQ.</button></a>
		</div>		
		
		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success" data-toggle="modal" data-target="#VehAddnewModal">
			<span class="fa fa-plus-circle"></span> Add New Veh.</button></a>
		</div>
		
		<div class="form-group col-md-1 col-xs-6">
			<a href="#"><button style="margin-top:10px;margin-right:10px;font-family: 'Open Sans', sans-serif !important" 
			class="btn-xs btn btn-success" data-toggle="modal" data-target="#VehAddnewModal2">
			<span class="fa fa-plus-circle"></span> Add Old Veh.</button></a>
		</div>
	</div>
</div>

<?php
$chk_pending_type = Qry($conn,"SELECT id FROM asset_vehicle WHERE asset_type='' AND branch='$branch'");
if(!$chk_pending_type){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
?>
<div class="form-group col-md-12">

	<div class="row">	
		<div class="form-group col-md-4">
			<h4><span class="fa fa-truck"></span> &nbsp; All Vehicles : <font color="blue"><?php echo $branch; ?></font> <?php
if(numRows($chk_pending_type)>0)
{
?>
&nbsp; &nbsp; <a href="./update_asset_type.php" class="btn-xs btn btn-danger"><span class="fa fa-exclamation-triangle"></span> Update Asset Type</a>
<?php
}
?></h4> 
		</div>
		
		
		<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Reg.No</th>
					<th>Vehicle_Class</th>
					<th>Vehicle_Holder</th>
					<th>General/specific</th>
					<th>Owner_Name</th>
					<th>RC</th>
					<th>Insurance</th>
					<th>PUC</th>
					<th>View_Full</th>
					<th>Lost/Scrap</th>
				</tr>	
				
<?php
$view_vehicle = Qry($conn,"SELECT v.id,v.vehicle_holder,v.asset_type,v.token_no,v.reg_no,v.owner_name,v.veh_class,v.rc,v.insurance,v.puc,active,
v.ho_approval,d.ins_end,d.puc_end,e.name as vehicle_holder1 
FROM asset_vehicle AS v 
LEFT OUTER JOIN asset_vehicle_docs AS d ON d.veh_id=v.id 
LEFT OUTER JOIN emp_attendance AS e ON e.code=v.vehicle_holder 
WHERE v.branch='$branch' AND v.active='1' AND v.ho_approval='1'");

if(!$view_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_vehicle)>0)
{
	$sn=1;
	
	while($row = fetchArray($view_vehicle))
	{
		if($row['reg_no']==""){
			$vehicle_no = $row['token_no'];
		}
		else{
			$vehicle_no = $row['reg_no'];
		}
		
		if($row['vehicle_holder']==$branch || $row['vehicle_holder']==''){
			$vehicle_holder = $branch;
		}
		else{
			$vehicle_holder = $row['vehicle_holder1'];
		}
		
		if($row['ho_approval']=="0"){
			$delete_button_disabled="disabled";
			$approve_text="<font color='green'>Approved</font>";
		}
		else{
			$delete_button_disabled="";
			$approve_text="<font color='red'>Pending</font>";
		}
		
		// if($row['active']=="0"){
			// $active_text="<font color='red'>In-Active</font>";
		// }
		// else{
			// $active_text="<font color='green'>Active</font>";
		// }
		
	echo "<tr>
			<td>$sn</td>
			<td>$vehicle_no</td>
			<td>$row[veh_class]</td>
			<td>$vehicle_holder</td>
			<td>$row[asset_type]</td>
			<td>$row[owner_name]</td>
			<td>";
			
		if($row['rc']==0){
			echo "<button type='button' onclick=RcUpdate('$row[id]','$vehicle_no') class='btn btn-xs btn-default'>Update</button>";
		}
		else{
			echo "<font color='green'><b>Updated</b></font>";
		}
		echo "</td>
		<td>";
		
		if($row['insurance']==0){
			echo "<button type='button' onclick=InsUpdate('$row[id]','$vehicle_no') class='btn btn-xs btn-default'>Update</button>";
		}
		else{
			echo "<font color='green'><b>Updated</b></font>";
			
			if(strtotime($row['ins_end'])<=strtotime(date("Y-m-d"))){
				echo "&nbsp; <button type='button' onclick=InsRenew('$row[id]','$vehicle_no') class='btn btn-xs btn-danger'>Renew</button";
			}
		}
		echo "</td>
		<td>";
		
		if($row['puc']==0){
			echo "<button type='button' onclick=PucUpdate('$row[id]','$vehicle_no') class='btn btn-xs btn-default'>Update</button>";
		}
		else{
			echo "<font color='green'><b>Updated</b></font>";
			
			if(strtotime($row['puc_end'])<=strtotime(date("Y-m-d"))){
				echo "&nbsp; <button type='button' onclick=PucRenew('$row[id]','$vehicle_no') class='btn btn-xs btn-danger'>Renew</button";
			}
		}
		echo "</td>
			
			<td>
				<a style='color:#000' href='asset_vehicle_view_full.php?id=$row[id]' class='btn btn-sm btn-default'>
				<span class='glyphicon glyphicon-list-alt'></span></a>
			</td>
			<td>
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='lost_scrap$row[id]'>
				<option value=''>--option--</option>
				<option value='LOST'>Lost/Theft</option>
				<option value='SCRAP'>Scrap</option>
			</select>
<button type='button' id='lost_scrap_button$row[id]' onclick='LostScrap($row[id])' 
class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></a></div>
			</td>
	</tr>";
	//<td>$approve_text</td>	
	$sn++;	
	}
}
else
{
	echo "<tr><td colspan='14'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
</div>
</div>

<script>
function RcUpdate(id,tno)
{
	$('#rc_form_id').val(id);
	$('#rc_veh_no').val(tno);
	jQuery.noConflict();
	$('#RcModal').modal(); 
}

function InsUpdate(id,tno)
{
	$('#ins_form_id').val(id);
	$('#ins_veh_no').val(tno);
	jQuery.noConflict();
	$('#InsModal').modal(); 
}

function PucUpdate(id,tno)
{
	$('#puc_form_id').val(id);
	$('#puc_veh_no').val(tno);
	jQuery.noConflict();
	$('#PucModal').modal(); 
}

function InsRenew(id,tno)
{
	$('#ins_ren_form_id').val(id);
	$('#ins_ren_veh_no').val(tno);
	jQuery.noConflict();
	$('#InsRenewalModal').modal(); 
}

function PucRenew(id,tno)
{
	$('#puc_ren_form_id').val(id);
	$('#puc_ren_veh_no').val(tno);
	jQuery.noConflict();
	$('#PUCRenewalModal').modal(); 
}

function LostScrap(id)
{
	var option_value = $('#lost_scrap'+id).val();
	
	if(option_value!='')
	{
		if(confirm("Do you really want to mark lost/scrap this vehicle ?") == true)
		{
			$('#lost_scrap_button'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_asset_vehicle_lost_scrap.php",
			data: 'option_value=' + option_value + '&id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}
	}
	else
	{
		alert('Select Option First !');
	}
}
</script>

<div id="func_result"></div>

<?php
include("./modal_asset_veh_rc_update.php");
include("./modal_asset_veh_insurance_update.php");
include("./modal_asset_veh_insurance_renewal.php");
include("./modal_asset_veh_puc_update.php");
include("./modal_asset_veh_puc_renewal.php");
include("./modal_asset_veh_add_new.php");
include("./modal_asset_veh_add_new_purchased.php");
?>

<form id="FormAssetReq" action="#" method="POST">
<div id="AssetReqModal" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Generate New Vehicle Request : ( नया Vehicle खरीदने के लिए उपयोग करे। )
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12">
				<label>Vehicle Type <font color="red"><sup>*</sup></font></label>
				<select class="form-control" required="required" name="veh_type">
					<option value="">--Select option--</option>
					<option value="VEH_TW">Two Wheeler</option>
					<option value="VEH_FW">Four Wheeler</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Maker Name <font color="red"><sup>*</sup></font></label>
				<textarea type="text" name="maker" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Model <font color="red"><sup>*</sup></font></label>
				<textarea type="text" name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" class="form-control" required="required"></textarea>
			</div>
			
<script>	
// function PaymentMode(elem)
// {
	// if(elem=="NEFT")
	// {
		// $('#rtgs_div').show();
		// $("#rtgs_div :input").prop('required',true);
	// }
	// else
	// {
		// $('#rtgs_div').hide();
		// $("#rtgs_div :input").prop('required',false);
	// }
// }
</script>	
		
		<div class="form-group col-md-12">
			<label>Narration <font color="red"><sup>*</sup></font></label>
			<textarea name="narration" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" class="form-control" required="required"></textarea>
		</div>
			
		</div>
      </div>
	  <div id="ReqFormResult"></div>
      <div class="modal-footer">
        <button type="submit" id="req_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormAssetReq").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#req_button").attr("disabled", true);
		$.ajax({
        	url: "./save_asset_vehicle_request.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#ReqFormResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no2");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no2").setAttribute("style","background-color:red;color:#FFF");
				$("#req_button").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no2").setAttribute("style","background-color:green;color:#FFF");
				$("#req_button").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no2").setAttribute("style","background-color:white;");
			$("#req_button").attr("disabled", false);
		}
  }
</script>

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc2");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ifsc2").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("req_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ifsc2").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("req_button").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ifsc2").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("req_button").removeAttribute("disabled");
		}
 } 			
</script>

<div id="VehAddnewModalSelection" class="modal fade" style="" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add New Vehicle
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12">&nbsp;</div>
			
			<div class="form-group col-md-6">
				<button type="button" onclick="jQuery.noConflict();$('#VehAddnewModalSelection').modal('toggle');" data-toggle="modal" data-target="#VehAddnewModal" style="border-radius:%;padding:20px" class="btn btn-md btn-danger">New Purchased Vehicle</button>
			</div>
			
			<div class="form-group col-md-6">
				<button type="button" onclick="jQuery.noConflict();$('#VehAddnewModalSelection').modal('toggle');" data-toggle="modal" data-target="#VehAddnewModal2" style="border-radius:%;padding:20px" class="pull-right btn btn-md btn-danger">Add Old Vehicle</button>
			</div>
			
			<div class="form-group col-md-12">&nbsp;</div>
			
		</div>
      </div>
	<!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>

<div id="view_assetReq" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
			Pending Asset Requests : <?php echo $branch; ?>
      </div>
	  
  <div class="modal-body">
	  <div class="row">
			<div class="col-md-12 table-striped">
<?php
$GetAssetsReq = Qry($conn,"SELECT a.id,a.req_code,a.req_date,a.veh_type,a.maker_name,a.model_name,a.approval,a.ho_approval
FROM asset_vehicle_req AS a 
WHERE a.asset_added='0' AND a.branch='$branch'");

if(!$GetAssetsReq){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_pending_asset = numRows($GetAssetsReq);

echo "<script>
	$('#pending_Staus').html($total_pending_asset);
</script>";

if($total_pending_asset>0)
{
	echo "
		<table class='table table-bordered' style='color:#000;font-size:11.5px;'>
			<tr>
				<th>#</th>
				<th>Token_No</th>
				<th>Req.Date</th>
				<th>Veh_Type</th>
				<th>Maker & Model</th>
				<th>Status</th>
			</tr>";

	$sn_1=1;
				
	while($row_pending_asset = fetchArray($GetAssetsReq))
	{
		echo "<tr>
			<td>$sn_1</td>
			<td>$row_pending_asset[req_code]</td>
			<td>$row_pending_asset[req_date]</td>
			<td>$row_pending_asset[veh_type]</td>
			<td>$row_pending_asset[maker_name]<br>$row_pending_asset[model_name]</td>
			<td>";
			if($row_pending_asset['approval']==0){
				echo "<b><font color='red'>Manager Approval Pending.</b></font>";
			}
			else if($row_pending_asset['ho_approval']==0){
				echo "<b><font color='red'>HO Approval Pending.</b></font>";
			}
			else{
				echo "<b><font color='green'>Ready to add.</b></font>";
			}
		echo "</td>
		</tr>";
		$sn_1++;
	}	
	echo "</table>";	
}
else
{
	echo "<center><b><font color='red'>No result found.</b></font></center>";
}
		?>
		</div>
      </div>
  </div>
  
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" style="color:#FFF;font-weight:bold;" class="btn btn-sm btn-danger">Close</button>
      </div>
    </div>

  </div>
</div>