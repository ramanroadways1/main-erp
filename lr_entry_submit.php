<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$consignor_id=escapeString($conn,strtoupper($_POST['consignor_id']));

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

// echo "<script>
		// window.location.href='./_fetch_lr_entry.php';
	// </script>";
	// exit();

if(strpos($lrno,'0')===0 AND $consignor_id!='848')
{
	echo "<script>
		alert('Error: Hey you have entered invalid LR number.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($branch=='PUNE')
{
	$pune_plant = escapeString($conn,strtoupper($_POST['pune_plant']));
	
	if($pune_plant!='AT' AND $pune_plant!='AS' AND $pune_plant!='OTHER')
	{
		echo "<script>
			alert('Error: Plant not found..');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}
}
else
{
	$pune_plant="";
}

$get_con1 = Qry($conn,"SELECT name,gst FROM consignor WHERE id='$consignor_id' AND hide!='1'");
if(!$get_con1){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_con1)==0)
{
	 echo "<script>
		alert('Error : Consignor not found !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_con1 = fetchArray($get_con1);

$consignor = $row_con1['name'];	
$con1_gst = $row_con1['gst'];

$bilty_id=escapeString($conn,strtoupper($_POST['bilty_id']));
$bilty_belongs_to=escapeString($conn,($_POST['bilty_belongs_to']));

// if($branch=='KORBA')
// {
	// $lr_type=escapeString($conn,strtoupper($_POST['lr_type']));
// }
// else
// {
	$lr_type = $_SESSION['verify_vehicle_type'];
// }
// 

$consignee_id=escapeString($conn,strtoupper($_POST['consignee_id']));

$get_con2 = Qry($conn,"SELECT name,gst FROM consignee WHERE id='$consignee_id' AND hide!='1'");
if(!$get_con2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_con2)==0)
{
	 echo "<script>
		alert('Error : Consignee not found !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_con2 = fetchArray($get_con2);
$consignee = $row_con2['name'];	
$con2_gst = $row_con2['gst'];

$from_id=escapeString($conn,strtoupper($_POST['from_id']));

$get_from = Qry($conn,"SELECT name FROM station WHERE id='$from_id'");
if(!$get_from){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_from)==0)
{
	 echo "<script>
		alert('Error : Location not found !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_from = fetchArray($get_from);
$from = $row_from['name'];	

$to_id=escapeString($conn,strtoupper($_POST['to_id']));
$billing_station_id=escapeString($conn,strtoupper($_POST['billing_station_id']));

$get_to = Qry($conn,"SELECT name FROM station WHERE id='$to_id'");
if(!$get_to){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_to)==0)
{
	 echo "<script>
		alert('Error : Location not found !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_to = fetchArray($get_to);
$to = $row_to['name'];	

$get_bill_station = Qry($conn,"SELECT name FROM station WHERE id='$billing_station_id'");
if(!$get_bill_station){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_bill_station)==0)
{
	 echo "<script>
		alert('Error : Billing Location not found !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_bill_station = fetchArray($get_bill_station);
$billing_station = $row_bill_station['name'];


$lr_date=escapeString($conn,strtoupper($_POST['date']));
$bank=escapeString($conn,strtoupper(trim($_POST['bank'])));
$sold_to=escapeString($conn,strtoupper(trim($_POST['sold_to_pay'])));
$lr_name=escapeString($conn,strtoupper(trim($_POST['lr_name'])));
$con2_addr=escapeString($conn,strtoupper(trim($_POST['con2_addr'])));
$dest_zone=escapeString($conn,strtoupper($_POST['dest_zone']));
$dest_zone_id=escapeString($conn,strtoupper($_POST['dest_zone_id']));
$do_no=escapeString($conn,strtoupper(trim($_POST['do_no'])));
$invno=escapeString($conn,strtoupper(trim($_POST['invno'])));
$shipno=escapeString($conn,strtoupper(trim($_POST['shipno'])));
$po_no=escapeString($conn,strtoupper(trim($_POST['po_no'])));
$actual_wt=escapeString($conn,strtoupper($_POST['act_wt']));
$charge_wt=escapeString($conn,strtoupper($_POST['chrg_wt']));
$route_type=escapeString($conn,strtoupper($_POST['route_type']));
$item=escapeString($conn,strtoupper($_POST['item']));
$item_id=escapeString($conn,strtoupper($_POST['item_id']));
$articles=escapeString($conn,strtoupper(trim($_POST['articles'])));
$goods_value=escapeString($conn,strtoupper($_POST['goods_value']));
$b_rate=escapeString($conn,strtoupper($_POST['b_rate']));
$b_amt=escapeString($conn,strtoupper($_POST['b_amt']));
$t_type=escapeString($conn,strtoupper($_POST['t_type']));
$freight_type=escapeString($conn,strtoupper($_POST['freight_type']));
$goods_desc=escapeString($conn,strtoupper(trim($_POST['goods_desc'])));
$gross_wt=escapeString($conn,strtoupper($_POST['gross_wt']));
$consignee_pincode=escapeString($conn,strtoupper($_POST['consignee_pincode']));
$hsn_code=escapeString($conn,strtoupper(trim($_POST['hsn_code'])));
$billing_type=escapeString($conn,strtoupper($_POST['billing_type']));
$bill_to_gst=escapeString($conn,strtoupper($_POST['bill_to_gst']));
$item_name=escapeString($conn,strtoupper($_POST['item_name']));
$EwayBill_bypass=escapeString($conn,strtoupper($_POST['EwayBill_bypass']));
$plant=escapeString($conn,strtoupper($_POST['plant_name']));
$con1_unit_name=escapeString($conn,($_POST['unit_name_vizag']));
$bilty_type=escapeString($conn,($_POST['bilty_type']));

$loading_point_id=escapeString($conn,($_POST['loading_point_id']));
$unloading_point_id=escapeString($conn,($_POST['unloading_point_id']));

//dates & extended or not
$EwayBill_reg_date=escapeString($conn,($_POST['EwayBill_reg_date']));
$EwayBill_exp_date=escapeString($conn,($_POST['EwayBill_exp_date']));
$EwayBill_extended=escapeString($conn,($_POST['EwayBill_extended']));
//dates & extended or not

if($consignor_id=='56')
{
	$sub_consignor = escapeString($conn,($_POST['consignor_sub']));
	$sub_consignor_id = escapeString($conn,($_POST['consignor_sub_id']));
	$sub_consignor_gst = escapeString($conn,($_POST['consignor_sub_gst']));
}
else
{
	$sub_consignor = "";
	$sub_consignor_id = "";
	$sub_consignor_gst = "";
}

if($billing_type=='BILL_TO_SHIP_TO')
{
	$bill_to=escapeString($conn,strtoupper($_POST['bill_to_party']));
	$bill_to_id=escapeString($conn,strtoupper($_POST['bill_to_party_id']));
	
	// $get_gst_bill_party = Qry($conn,"SELECT gst FROM consignee WHERE id=$bill_to_id'");
	// if(!$get_gst_bill_party){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./");
		// exit();
	// }
	
	$bill_gst = $bill_to_gst;
	$gst_alert = "Bill Party";
}
else
{
	$bill_to = "";
	$bill_to_id = "";
	$gst_alert = "Consignee";
	$bill_gst = $con2_gst;
}

$trip_type_pune = "";
$round_trip_lrno = "";

if($branch=='PUNE' AND $from_id=='210' AND $to_id=='951')
{
	$trip_type_pune = $_POST['trip_type'];

	if($trip_type_pune=='Round')
	{
		if(empty($_POST['round_trip_lrno']))
		{
			echo "<script>
				alert('Error : Round Trip LR not found !');
				$('#loadicon').hide();
				$('#lr_sub').attr('disabled', false);
			</script>";
			exit();
		}
		
		$round_trip_lrno = escapeString($conn,strtoupper($_POST['round_trip_lrno']));;
	}
	else if($trip_type_pune=='One_Way')	
	{
		
	}
	else
	{
		echo "<script>
			alert('Error : Trip type not found !');
			$('#loadicon').hide();
			$('#lr_sub').attr('disabled', false);
		</script>";
		exit();
	}
}

// if(!preg_match('~[^0-9,. A-Z-a-z\\/]~', $con2_addr) AND $con2_addr!='')
// {
// if(preg_match("/'/u", $con2_addr))
// {
	// echo "<script>
		// alert('Error : Check Consignee Address. Single quote found !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }


// if(strpos("'", $con2_addr) > 0 OR strpos('"', $con2_addr) > 0){
  // echo "<script>
		// alert('Error : Check Consignee Address !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

	// echo "<script>
		// alert('Error : Check Consignee Address !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $bank) AND $bank!='')
// {
	// echo "<script>
		// alert('Error : Check Bank Name !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// if(!preg_match('/^[a-z,& A-Z-0-9\.]*$/', $sold_to) AND $sold_to!='')
// {
	// echo "<script>
		// alert('Error : Check Sold to party !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// if(!preg_match('/^[a-z,& A-Z-0-9\.]*$/', $lr_name) AND $lr_name!='')
// {
	// echo "<script>
		// alert('Error : Check LR Name !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

if(!preg_match('/^[a-z,A-Z-0-9\.]*$/', $do_no))
{
	echo "<script>
		alert('Error : Check DO Number !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(!preg_match('/^[a-z,A-Z-0-9\.]*$/', $invno))
{
	echo "<script>
		alert('Error : Check Invoice Number !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $shipno))
{
	echo "<script>
		alert('Error : Check Shipment Number !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $po_no))
{
	echo "<script>
		alert('Error : Check PO Number !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $hsn_code))
{
	echo "<script>
		alert('Error : Check HSN Code !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

// if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $goods_desc))
// {
	// echo "<script>
		// alert('Error : Check Input Goods Description !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $articles))
// {
	// echo "<script>
		// alert('Error : Check Input No. of articles !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// Eway Bill Validations 

$EwayBillFlag=escapeString($conn,strtoupper($_POST['EwayBillFlag']));
$EwayBillFlag_desc=escapeString($conn,($_POST['EwayBillFlag_desc']));
$EwayBill_con1_gst=escapeString($conn,strtoupper($_POST['EwayBill_con1_gst']));
$EwayBill_con1=escapeString($conn,strtoupper($_POST['EwayBill_con1']));
$EwayBill_con2_gst=escapeString($conn,strtoupper($_POST['EwayBill_con2_gst']));
$EwayBill_con2=escapeString($conn,strtoupper($_POST['EwayBill_con2']));
$EwayBill_tno=escapeString($conn,strtoupper($_POST['EwayBill_tno']));
$by_pass_narr=escapeString($conn,($_POST['by_pass_narr']));
$by_pass_id=escapeString($conn,($_POST['by_pass_id']));

$ewb_no_db=escapeString($conn,strtoupper($_POST['ewb_no_db']));
$ewayBillDate=escapeString($conn,strtoupper($_POST['ewayBillDate']));
$ewb_docNo=escapeString($conn,strtoupper($_POST['ewb_docNo']));
$ewb_docDate=escapeString($conn,strtoupper($_POST['ewb_docDate']));
$ewb_fromPincode=escapeString($conn,strtoupper($_POST['ewb_fromPincode']));
$ewb_totInvValue=escapeString($conn,strtoupper($_POST['ewb_totInvValue']));
$ewb_totalValue=escapeString($conn,strtoupper($_POST['ewb_totalValue']));
$ewb_toPincode=escapeString($conn,strtoupper($_POST['ewb_toPincode']));
$invoice_value=escapeString($conn,strtoupper($_POST['invoice_value']));

// Eway Bill Validations

if(empty($item_id)){
	echo "<script>
		alert('Item not found !!');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(empty($from_id) || empty($to_id)){
	echo "<script>
		alert('Locations not found !!');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($lr_type=='MARKET')
{
	// $tno=escapeString($conn,strtoupper($_POST['tno_market']));
	// $wheeler=escapeString($conn,strtoupper($_POST['wheeler_market']));
	$update_col_name = "market_lr";
}
else
{
	// $tno=escapeString($conn,strtoupper($_POST['tno_own']));
	// $wheeler=escapeString($conn,strtoupper($_POST['wheeler_own_truck']));
	$update_col_name = "own_lr";
}

$owner_mobile_no="";	
$driver_mobile_no="";	
	
// if($branch=='KORBA')
// {
	// if($lr_type=='MARKET')
	// {
		// $tno=escapeString($conn,strtoupper($_POST['tno_market']));
		// $wheeler=escapeString($conn,strtoupper($_POST['wheeler_market']));
		
		// $get_owner_mob_no = Qry($conn,"SELECT mo1 FROM mk_truck WHERE tno='$tno'");
		// if(!$get_owner_mob_no){
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// Redirect("Error while processing Request","./");
			// exit();
		// }
		
		// $row_owner_mob = fetchArray($get_owner_mob_no);
		// $owner_mobile_no = $row_owner_mob['mo1'];
		
		// $get_driver_mobile_from_mis = Qry($conn,"SELECT driver_mobile FROM mis WHERE truck_no='$tno' ORDER BY id DESC LIMIT 1");
		// if(!$get_driver_mobile_from_mis){
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			// Redirect("Error while processing Request","./");
			// exit();
		// }
		
		// if(numRows($get_driver_mobile_from_mis)>0){
			// $row_mis_driver_mobile = fetchArray($get_driver_mobile_from_mis);
			// $driver_mobile_no = $row_mis_driver_mobile['driver_mobile'];
		// }
		// else{
			// $driver_mobile_no = "";
		// }
	// }
	// else
	// {
		// $tno=escapeString($conn,strtoupper($_POST['tno_own']));
		// $wheeler=escapeString($conn,strtoupper($_POST['wheeler_own_truck']));
		
		// $driver_mobile_no = $_SESSION['verify_driver_mobile_no']; // own truck active driver mobile get from session
	// }
// }
// else
// {
	$tno = $_SESSION['verify_vehicle'];
	$wheeler = $_SESSION['verify_vehicle_wheeler'];
	
	if($lr_type=='MARKET') // if market vehicle 
	{
		$owner_mobile_no = $_SESSION['verify_vehicle_mobile_no']; // market veh. owner mobile get from session
	}
	
	$driver_mobile_no = $_SESSION['verify_driver_mobile_no']; // own truck active driver mobile get from session
// }

if(count(explode(' ', $tno)) > 1)
{
	echo "<script>
		alert('Error: Invalid vehicle number.');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if(empty($tno)){
	echo "<script>
		alert('Vehicle Number not found !!');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if(empty($wheeler)){
	echo "<script>
		alert('Wheeler not found !!');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if($EwayBillFlag!="1" AND $EwayBillFlag!="2"){
	echo "<script>
		alert('Error : eway-bill details not found !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if($lr_type=='MARKET')
{
	$chk_market_vehicle = Qry($conn,"SELECT blocked FROM mk_truck WHERE tno='$tno'");

	if(!$chk_market_vehicle){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_market_vehicle)==0)
	{
		echo "<script>
			alert('Error : Vehicle not found !');
			$('#loadicon').hide();
			$('#lr_sub').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_mkt = fetchArray($chk_market_vehicle);
	
	if($row_mkt['blocked']=="1")
	{
		echo "<script>
			alert('Error : Vehicle blocked !');
			$('#loadicon').hide();
			$('#lr_sub').attr('disabled', false);
		</script>";
		exit();
	}
}

if($EwayBillFlag=="1")
{
	if($consignor_id=='56')
	{
		if($EwayBill_con1_gst!=$sub_consignor_gst AND (substr($EwayBill_con1_gst,0,3)!='URP'))
		{
			echo "<script>
				alert('Error : Consignor\'s GST not matching with e-way Bill !');
				$('#loadicon').hide();
				$('#lr_sub').attr('disabled', false);
			</script>";
			exit();
		}
	}
	else
	{
		if($EwayBill_con1_gst!=$con1_gst AND (substr($EwayBill_con1_gst,0,3)!='URP'))
		{
			echo "<script>
				alert('Error : Consignor\'s GST not matching with e-way Bill !');
				$('#loadicon').hide();
				$('#lr_sub').attr('disabled', false);
			</script>";
			exit();
		}
	}
	
	if($consignor_id=='848' AND $bill_gst=="" AND $EwayBill_con2_gst=='24AAAAK0203G1ZH')
	{
		 // by pass consignee gst check for krishak bharti
	}
	else
	{
		if($EwayBill_con2_gst!=$bill_gst AND (substr($EwayBill_con2_gst,0,3)!='URP'))
		{
			echo "<script>
				alert('Error : $gst_alert\'s GST: $bill_gst not matching with e-way Bill GST: $EwayBill_con2_gst !');
				$('#loadicon').hide();
				$('#lr_sub').attr('disabled', false);
			</script>";
			exit();
		}
	}

	if($EwayBill_tno!=$tno)
	{
		echo "<script>
			alert('Error : Vehicle Number: $tno not matching with e-way Bill: $EwayBill_tno !');
			$('#loadicon').hide();
			$('#lr_sub').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	if($EwayBill_bypass!="1")
	{
		if($item_id!='101')
		{
			// $sourcePath = $_FILES['ewb_copy']['tmp_name'];
			// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

			// if(!in_array($_FILES['ewb_copy']['type'], $valid_types))
			// {
				// echo "<script>
					// alert('Error : Only Image and PDF Upload Allowed: eway bill.');
					// $('#lr_sub').attr('disabled',false);
					// $('#loadicon').hide();
				// </script>";
				// exit();
			// }
			
			// if($sourcePath=='')
			// {
				// echo "<script>
					// alert('Error : eway bill copy not found.');
					// $('#lr_sub').attr('disabled',false);
					// $('#loadicon').hide();
				// </script>";
				// exit();
			// }
		}
	}
}

$this_round_trip_lr="NO";

if($branch=='PUNE')
{
	$chk_for_round_trip = Qry($conn,"SELECT id,vehicle_no,is_cleared FROM lr_round_trip WHERE round_trip_lrno='$lrno'");

	if(!$chk_for_round_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_for_round_trip)>0)
	{
		$this_round_trip_lr="YES";
		$row_round_trip = fetchArray($chk_for_round_trip);
		$round_trip_id = $row_round_trip['id'];
		
		if($row_round_trip['vehicle_no']!=$tno)
		{
			echo "<script>
				alert('Error : Round trip vehicle number not matching !');
				$('#loadicon').hide();
				$('#lr_sub').attr('disabled', false);
			</script>";
			exit();
		}
	}
}


if($bilty_id=='OTHERLR')
{
	$company=escapeString($conn,$_POST['company_man']); 
}
else if($bilty_id=='ZERO' || $bilty_id=='')
{
	echo "<script>
		alert('Please Try Again !!');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}
else
{
	$company=escapeString($conn,$_POST['company_auto']);	
}

$check_open_lr = Qry($conn,"SELECT party FROM open_lr WHERE party_id='$consignor_id'");
if(!$check_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_open_lr)==0)
{
	if($bilty_id=='OTHERLR' || $bilty_id=='ZERO' || $bilty_id=='')
	{
		echo "<script>
			alert('Bilty Id not found !');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}
	
	// if($consignor_id=='848') // for krishak bharti LR starts with 00
	// {
		// $lrno_for_stock = ltrim($lrno, "0"); 
		
		// $chk_bank = Qry($conn,"SELECT id,branch,company,stock_left,lr_belongs_to FROM lr_bank WHERE $lrno_for_stock>=from_range 
		// AND $lrno_for_stock<=to_range");
	// }
	// else
	// {
	$chk_bank = Qry($conn,"SELECT id,branch,company,stock_left,lr_belongs_to FROM lr_bank WHERE $lrno>=from_range 
	AND $lrno<=to_range");
	// }
	
	if(!$chk_bank){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$numRows_bilty = numRows($chk_bank);

	if($numRows_bilty==0)
	{
		echo "<script type='text/javascript'>
			alert('LR stock not updated. Please contact head-office.');
			window.location.href='./_fetch_lr_entry.php';
		</script>";	
		exit();
	}

	$row_stock=fetchArray($chk_bank);

	if($row_stock['lr_belongs_to']!=$bilty_belongs_to)
	{
		echo "<script type='text/javascript'>
			alert('LR book not verified.');
			window.location.href='./_fetch_lr_entry.php';
		</script>";	
		exit();
	}
	
	if($bilty_belongs_to=="HINDALCO")
	{
		$lrno = "RRPL".$lrno;
	}
	
	$stock_left = $row_stock['stock_left'];
	$bilty_id_db = $row_stock['id'];
	$bilty_branch_db = $row_stock['branch'];

	if($bilty_id!=$bilty_id_db)
	{
		errorLog("LR book not verified. Dbid: $bilty_id_db. and client input is: $bilty_id",$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Error: LR book not verified.');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}
	
	if($numRows_bilty>1)
	{
		echo "<script type='text/javascript'>
			alert('Error: Duplicate LR Series found. Bilty Id $bilty_id_db. Please contact head-office.');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}

	if($stock_left!='' AND $stock_left<=0)
	{
		echo "<script type='text/javascript'>
			alert('Bilty Book not in stock. Please contact head-office.');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}
		
	if($bilty_branch_db!=$branch)
	{
		echo "<script type='text/javascript'>
			alert('Not your LR. LR Book belongs to $bilty_branch_db Branch !');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}
}
		
if(empty($company)){
	echo "<script>
		alert('Company not found !!');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}	

if($consignor_id=='128')
{
	$lrno = "M".$lrno;
}

if($consignor_id=='848')
{
	$lrno = "AOR".$lrno;
}

// -- check ewb no 

// $check_lr = Qry($conn,"SELECT id FROM lr_check WHERE lrno='$ewb_no_db'");
// if(!$check_lr){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// if(numRows($check_lr)>0)
// {
	// echo "<script>
		// alert('Duplicate LR Found !!');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled', false);
	// </script>";
	// exit();
// }

// -- check ewb no 
	
$check_lr = Qry($conn,"SELECT id FROM lr_check WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr)>0)
{
	echo "<script>
		alert('Duplicate LR Found !!');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled', false);
	</script>";
	exit();
}

if($EwayBillFlag=="1")
{
	$ewb_copy = "";
}
else 
{
	if($EwayBill_bypass=="1")
	{
		$ewb_copy = "";
	}
	else
	{
		if($item_id=='101')
		{
			$ewb_copy = "";
		}
		else
		{
			$ewb_copy = "";
			// $fix_name = mt_rand().date('dmYHis');
			// $targetPath="ewb_copy/".$fix_name.".".pathinfo($_FILES['ewb_copy']['name'],PATHINFO_EXTENSION);
			// $ewb_copy = $targetPath;
			
			// ImageUpload(600,800,$sourcePath);
			
			// if(!move_uploaded_file($sourcePath,$targetPath))
			// {
				// echo "<script>
					// alert('Error while uploading eway bill copy.');
					// $('#lr_sub').attr('disabled',false);
					// $('#loadicon').hide();
				// </script>";
				// exit();
			// }
		}
	}
} 

StartCommit($conn);
$flag = true;

// $ChkForDo = Qry($conn,"SELECT id FROM gps_install WHERE unistall='0' AND VehicleNumber='$tno'");

// if(!$ChkForDo){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }


if(numRows($check_open_lr)==0)
{
	$update_stock=Qry($conn,"UPDATE lr_bank SET stock_left=stock_left-1 WHERE id='$bilty_id_db'");
	
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$bilty_id_db="";
}

$insert_lr_check = Qry($conn,"INSERT INTO lr_check(branch,lrno,bilty_id,plant,route_type,timestamp) VALUES 
('$branch','$lrno','$bilty_id_db','$pune_plant','$route_type','$timestamp')");
	
if(!$insert_lr_check){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_id_lr1 = getInsertID($conn);
		
if(isset($_SESSION['verify_driver_id']))
{
	$dl_id = $_SESSION['verify_driver_id'];
}
else
{
	$dl_id="";
}

// if(($lrno>=2361252 AND $lrno<=2361300) || ($lrno>=2361601 AND $lrno<=2361700) || ($lrno>=2360051 AND $lrno<=2360200) || ($lrno>=2360251 AND $lrno<=2360300))
// {
	// $oxygen_lr = "1";
// }
// else
// {
	$oxygen_lr = "0";
// }

if($_POST['shipment_party_sel']=='SAME')
{
	$shipment_party="";
	$shipment_party_id="";
	$shipment_party_gst="";
}
else
{
	$shipment_party=escapeString($conn,strtoupper($_POST['shipment_party']));
	$shipment_party_id=escapeString($conn,strtoupper($_POST['shipment_party_id']));
	$shipment_party_gst=escapeString($conn,strtoupper($_POST['shipment_party_gst']));
}

$insert_lr = Qry($conn,"INSERT INTO lr_sample(dl_id,oxygen_lr,ewb_no,company,branch,branch_user,lrno,loading_point_id,unloading_point_id,bilty_type,lr_type,wheeler,date,create_date,truck_no,fstation,
tstation,billing_station,billing_station_id,dest_zone,consignor,sub_consignor,bill_to,bill_to_id,consignee,shipment_party,shipment_party_id,
con1_unit_name,con2_addr,from_id,to_id,con1_id,sub_con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,
weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,invoice_value,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,
sub_con1_gst,con2_gst,shipment_party_gst,hsn_code,timestamp,trip_type,round_trip_lrno) VALUES ('$dl_id','$oxygen_lr','$ewb_no_db','$company','$branch',
'$branch_sub_user','$lrno','$loading_point_id','$unloading_point_id','$bilty_type','$lr_type','$wheeler','$lr_date','$date','$tno','$from','$to','$billing_station','$billing_station_id',
'$dest_zone','$consignor','$sub_consignor','$bill_to','$bill_to_id','$consignee','$shipment_party','$shipment_party_id','$con1_unit_name',
'$con2_addr','$from_id','$to_id','$consignor_id','$sub_consignor_id','$consignee_id','$dest_zone_id','$po_no','$bank','$freight_type',
'$sold_to','$lr_name','$actual_wt','$gross_wt','$charge_wt','$charge_wt','$b_rate','$b_amt','$t_type','$do_no','$shipno','$invno',
'$invoice_value','$item','$item_id','$item_name','$plant','$articles','$goods_desc','$goods_value','$con1_gst','$sub_consignor_gst','$con2_gst',
'$shipment_party_gst','$hsn_code','$timestamp','$trip_type_pune','$round_trip_lrno')");

if(!$insert_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$lr_id = getInsertID($conn);

$insert_lr_cache = Qry($conn,"INSERT INTO lr_sample_pending(dl_id,oxygen_lr,lr_id,company,branch,branch_user,lrno,loading_point_id,unloading_point_id,bilty_type,lr_type,wheeler,date,create_date,
truck_no,fstation,tstation,billing_station,billing_station_id,dest_zone,consignor,sub_consignor,bill_to,bill_to_id,consignee,shipment_party,
shipment_party_id,con2_addr,from_id,to_id,con1_id,sub_con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,
weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,invoice_value,item,item_id,item_name,plant,articles,goods_desc,goods_value,
con1_gst,sub_con1_gst,con2_gst,shipment_party_gst,hsn_code,timestamp) VALUES ('$dl_id','$oxygen_lr','$lr_id','$company','$branch',
'$branch_sub_user','$lrno','$loading_point_id','$unloading_point_id','$bilty_type','$lr_type','$wheeler','$lr_date','$date','$tno','$from','$to','$billing_station','$billing_station_id','$dest_zone',
'$consignor','$sub_consignor','$bill_to','$bill_to_id','$consignee','$shipment_party','$shipment_party_id','$con2_addr','$from_id','$to_id',
'$consignor_id','$sub_consignor_id','$consignee_id','$dest_zone_id','$po_no','$bank','$freight_type','$sold_to','$lr_name','$actual_wt',
'$gross_wt','$charge_wt','$charge_wt','$b_rate','$b_amt','$t_type','$do_no','$shipno','$invno','$invoice_value','$item','$item_id','$item_name',
'$plant','$articles','$goods_desc','$goods_value','$con1_gst','$sub_consignor_gst','$con2_gst','$shipment_party_gst','$hsn_code','$timestamp')");

if(!$insert_lr_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$lr_id2 = getInsertID($conn);

if($round_trip_lrno!='')
{
	$insert_round_trip_lr = Qry($conn,"INSERT INTO lr_round_trip(lr_id,round_trip_lrno,vehicle_no,timestamp) VALUES 
	('$lr_id','$round_trip_lrno','$tno','$timestamp')");

	if(!$insert_round_trip_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_lr_count = Qry($conn,"UPDATE _pending_lr SET `$update_col_name`=`$update_col_name`+1 WHERE branch='$branch'");

if(!$update_lr_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
 
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to update LR count. Branch : $branch. LRNo: $lrno.",$conn,$page_name,__LINE__);
}  

$insert_ewb_data = Qry($conn,"INSERT INTO _eway_bill_lr_wise(lrno,lr_id,branch,ewb_no,ewb_flag_desc,by_pass,ewb_copy,ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,
totalValue,toPincode,timestamp) VALUES ('$lrno','$lr_id','$branch','$ewb_no_db','$EwayBillFlag_desc','$EwayBill_bypass','$ewb_copy','$ewayBillDate','$EwayBill_reg_date','$EwayBill_exp_date','$EwayBill_extended','$ewb_docNo','$ewb_docDate','$ewb_fromPincode',
'$ewb_totInvValue','$ewb_totalValue','$ewb_toPincode','$timestamp')");

if(!$insert_ewb_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$ewb_id = getInsertID($conn);

// if($ewb_no_db!="")
// {
	$insert_ewb_data2 = Qry($conn,"INSERT INTO _eway_bill_validity(owner_mobile,driver_mobile,lrno,truck_no,lr_date,from_loc,to_loc,
	consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,ewb_id,ewb_date,ewb_expiry,ewb_extended,timestamp) VALUES 
	('$owner_mobile_no','$driver_mobile_no','$lrno','$tno','$lr_date','$from','$to','$consignor','$consignee','$lr_id','$branch',
	'$ewb_no_db','$EwayBillFlag_desc','$ewb_id','$EwayBill_reg_date','$EwayBill_exp_date','$EwayBill_extended','$timestamp')");

	if(!$insert_ewb_data2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
// }

if($EwayBill_bypass=="1")
{
	$insert_ewb_by_pass = Qry($conn,"INSERT INTO _eway_bill_by_pass(lrno,by_pass_id,lr_id,ewb_no,branch,narration,timestamp) VALUES 
	('$lrno','$by_pass_id','$lr_id','$ewb_no_db','$branch','$by_pass_narr','$timestamp')");

	if(!$insert_ewb_by_pass){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($this_round_trip_lr=='YES')
{
	$update_round_clear = Qry($conn,"UPDATE lr_round_trip SET is_cleared='1',clear_timestamp='$timestamp' WHERE id='$round_trip_id'");

	if(!$update_round_clear){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($lr_type=='MARKET')
{
	$insert_placement_req_db = Qry($conn,"INSERT INTO vehicle_placement_req_db(lr_branch,lr_branch_user,lr_id,lr_timestamp,tno,dl_no,
	driver_mobile,branch,branch_user,timestamp) SELECT '$branch','$branch_sub_user','$lr_id','$timestamp',tno,dl_no,driver_mobile,
	branch,branch_user,timestamp FROM vehicle_placement_req WHERE id='$_SESSION[vehicle_placement_req_id]'");
		
	if(!$insert_placement_req_db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$delete_placement_req = Qry($conn,"DELETE FROM vehicle_placement_req WHERE id='$_SESSION[vehicle_placement_req_id]'");
		
	if(!$delete_placement_req){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);

if($lr_type!='MARKET')
{
		unset($_SESSION['verify_vehicle_type']);
		unset($_SESSION['verify_vehicle']);
		unset($_SESSION['verify_vehicle_id']);
		unset($_SESSION['verify_vehicle_wheeler']);
		unset($_SESSION['verify_driver']);
		unset($_SESSION['verify_driver_id']);
		unset($_SESSION['verify_vehicle_mobile_no']);
		unset($_SESSION['verify_driver_mobile_no']);
		unset($_SESSION['vehicle_placement_req_id']);
}
	
	// if($branch!='KORBA')
	// {
		// unset($_SESSION['verify_vehicle_type']);
		// unset($_SESSION['verify_vehicle']);
		// unset($_SESSION['verify_vehicle_id']);
		// unset($_SESSION['verify_vehicle_wheeler']);
		// unset($_SESSION['verify_driver']);
		// unset($_SESSION['verify_driver_id']);
		// unset($_SESSION['verify_vehicle_mobile_no']);
		// unset($_SESSION['verify_driver_mobile_no']);
		// unset($_SESSION['vehicle_placement_req_id']);
	// }

	// if($branch=='HEAD')
	// {
		// echo "<script>
			// LocanixPush('$lr_id2','$lrno');
		// </script>";
	// }
	// else
	// {
		echo "<script>
			alert('LR : $lrno. Created Successfully !');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
	// }
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>