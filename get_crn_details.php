<?php
require("./connection.php");

$crn = escapeString($conn,strtoupper($_POST['crn']));

$get_rtgs = Qry($conn,"SELECT id,acname,amount,approval FROM rtgs_fm WHERE crn='$crn' AND branch='$branch'");

if(!$get_rtgs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_rtgs)==0)
{
	echo "<script>
		alert('No record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',true);
	</script>";
	exit();
}
else
{
	$row = fetchArray($get_rtgs);
	
	if($row['approval']=='1')
	{
		echo "<script>
			alert('Payment already approved !');
			$('#loadicon').fadeOut('slow');
			$('#rtgs_id').val('');
			$('#button_sub').attr('disabled',true);
		</script>";
		exit();
	}
	
	echo "<script>	
		$('#party_name').val('$row[acname]');
		$('#amount').val('$row[amount]');
		$('#rtgs_id').val('$row[id]');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}
?>