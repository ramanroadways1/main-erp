<?php
require_once("connection.php");

unset($_SESSION['verify_vehicle_type']);
unset($_SESSION['verify_vehicle']);
unset($_SESSION['verify_vehicle_id']);
unset($_SESSION['verify_vehicle_wheeler']);
unset($_SESSION['verify_driver']);
unset($_SESSION['verify_driver_id']);

echo "<script>
	window.location.href='_fetch_lr_entry.php';
</script>";
?>