<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['emp_id']));
$emp_code = escapeString($conn,($_POST['emp_code']));

if(empty($id) || empty($emp_code))
{
	echo "<script>
		alert('Error: data not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

$chk_emp = Qry($conn,"SELECT code,image FROM emp_attendance WHERE id='$id'");

if(!$chk_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./employee_view.php");
	exit();
}

if(numRows($chk_emp)==0)
{
	echo "<script type='text/javascript'>
		alert('Error: employee not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();	
}

$row_emp = fetchArray($chk_emp);

$ext_path = $row_emp['image'];

if($row_emp['code'] != $emp_code)
{
	echo "<script type='text/javascript'>
		alert('Error: employee not verfied !');
		window.location.href='./employee_view_full.php?id=$id';
	</script>";
	exit();	
}

$sourcePath = $_FILES['file']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['file']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image Upload Allowed !');
		$('#btn_upload').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}


$fix_name = mt_rand().date('dmYHis');

$targetPath = "employee_data/photo/".mt_rand().date('dmYHis').".".pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);

ImageUpload(400,400,$sourcePath);

StartCommit($conn);
$flag = true;

$update_dp = Qry($conn,"UPDATE emp_attendance SET image='$targetPath' WHERE id='$id'");
	
if(!$update_dp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES ('$emp_code','Employee_edit',
'Photo_update','Old path: $ext_path to $targetPath','$branch','$branch_sub_user','$timestamp')");
	
if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($sourcePath,$targetPath))
{
	$flag = false;
	errorLog("File upload failed. Id: $id, Code: $emp_code.",$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 
		alert('Profile updated successfully.');
		window.location.href='./employee_view_full.php?id=$id';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script> 
		alert('Error while processing request !');
		$('#btn_upload').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}	
?>