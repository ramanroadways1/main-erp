<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

	$amount = strtoupper(escapeString($conn,$_POST['amount']));
	$company_to = strtoupper(escapeString($conn,$_POST['company_cr_to']));
	$branch_to = strtoupper(escapeString($conn,$_POST['branch_cr_to']));
	$company_from = strtoupper(escapeString($conn,$_POST['debit_company']));
	$otp = strtoupper(escapeString($conn,$_POST['otp']));
	$narration = strtoupper(escapeString($conn,$_POST['narration']));
	
$narration_from = "DEBIT TO BRANCH => ".$branch_to.": ".$narration;
$narration_to = "CREDIT FROM BRANCH => ".$branch.": ".$narration;
		
if($company_to=="" || $branch_to=="" || $company_from=="")
{
	echo "<script>
		alert('Something went wrong. Please try again..');
		window.location.href='./debit.php';
	</script>";
	exit();
}
	 
$qry_fetch=Qry($conn,"SELECT m.emp_code,e.mobile_no FROM manager AS m 
LEFT OUTER JOIN emp_attendance AS e ON e.code=m.emp_code 
WHERE m.branch='$branch_to'");

if(!$qry_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry_fetch)==0)
{
	echo "<script>
		alert('Manager not found.');
		window.location.href='./debit.php';
	</script>";
	exit();
}

$row_fetch = fetchArray($qry_fetch);

if($row_fetch['emp_code']=='')
{
	echo "<script>
		alert('Manager not found.');
		window.location.href='./debit.php';
	</script>";
	exit();
}

$mobile = $row_fetch['mobile_no'];

if(strlen($mobile)!='10')
{
	echo "<script>
		alert('Manager\'s mobile number not updated of $branch_to.');
		window.location.href='./debit.php';
	</script>";
	exit();
}	

// For Verify otp

// $url="https://www.smsschool.in/api/verifyRequestOTP.php";
// $postData = array(
    // 'authkey' => "242484ARRP024r65bc082c9",
    // 'mobile' => "$mobile",
    // 'otp' => "$otp"
// );

// $ch = curl_init();
// curl_setopt_array($ch, array(
// CURLOPT_URL => $url,
// CURLOPT_RETURNTRANSFER => true,
// CURLOPT_POST => true,
// CURLOPT_POSTFIELDS => $postData
// ));

// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// $output = curl_exec($ch);

// if(curl_errno($ch))
// {
	// echo "<script>
		// alert('API Failure !');
		// window.location.href='./debit.php';
	// </script>";
	// exit();	
// }

// $result = json_decode($output, true);
// curl_close($ch);

// if($result['type']=='error')
// {
	// echo "<script>
			// alert('Error : $result[message].');
			// $('#dr_b_to_b_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
	// exit();	
// }
// else
// {
	// if($result['message']!='otp_verified')
	// {
		// echo "<script>
			// alert('Error : $result[message].');
			// $('#dr_b_to_b_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
		// exit();	
	// }
// }

if($otp!=$_SESSION['session_otp'])
{
	echo "<script>
			alert('Error : Invalid OTP entered.');
			$('#dr_b_to_b_button').attr('disabled',false);
			$('#loadicon').hide();
	</script>";
	exit();	
}
	
StartCommit($conn);
$flag = true;
	
	$fetch_from_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	if(!$fetch_from_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
    $row_from_bal = fetchArray($fetch_from_bal);
    $rrpl_cash_from = $row_from_bal['balance'];
    $rr_cash_from = $row_from_bal['balance2'];
	
	$fetch_to_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch_to'");
	if(!$fetch_to_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
    $row_to_bal = fetchArray($fetch_to_bal);
    $rrpl_cash_to = $row_to_bal['balance'];
    $rr_cash_to = $row_to_bal['balance2'];
		
	if($company_from=='RRPL' AND $rrpl_cash_from>=$amount) 
	{
		$newbal_from =  $rrpl_cash_from-$amount;
		$debit_col_from="debit";
		$balance_col_from="balance";
	}	
	else if($company_from=='RAMAN_ROADWAYS' AND $rr_cash_from>=$amount)
	{
		$newbal_from =  $rr_cash_from-$amount;
		$debit_col_from="debit2";
		$balance_col_from="balance2";
	}
	else
	{
		$flag = false;
		echo "<script>
			alert('Insufficient Balance in $company_from !');
		</script>";
	}
	
	if($company_to=='RRPL') 
	{
		$newbal_to =  $amount + $rrpl_cash_to;
		$credit_col_to="credit";
		$balance_col_to="balance";
	}	
	else
	{
		$newbal_to =  $amount + $rr_cash_to;
		$credit_col_to="credit2";
		$balance_col_to="balance2";
	}
	
	$update_from_branch = Qry($conn,"UPDATE user SET `$balance_col_from` = '$newbal_from' WHERE username='$branch'");
	if(!$update_from_branch){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook_from = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$debit_col_from`,`$balance_col_from`,timestamp) 
	VALUES ('$branch','$_SESSION[user_code]','$date','$company_from','DEBIT-BRANCH','$narration_from','$amount','$newbal_from','$timestamp')");
		
	if(!$insert_cashbook_from){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$debit_cash_id = getInsertID($conn);
	
	$update_to_branch = Qry($conn,"UPDATE user SET `$balance_col_to` = '$newbal_to' WHERE username='$branch_to'");
	if(!$update_to_branch){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook_to = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$credit_col_to`,`$balance_col_to`,timestamp) 
	VALUES ('$branch_to','$_SESSION[user_code]','$date','$company_to','CREDIT-BRANCH','$narration_to','$amount','$newbal_to','$timestamp')");
		
	if(!$insert_cashbook_to){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$credit_cash_id = getInsertID($conn);
	
	$qry_debit = Qry($conn,"INSERT INTO debit(debit_by,cash_id,section,to_branch,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
		('CASH','$debit_cash_id','DEBIT-BRANCH','$branch_to','$branch','$_SESSION[user_code]','$company_from','$amount','$narration_from','$date','$timestamp')");	

	if(!$qry_debit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}	
	
	$debit_tab_id = getInsertID($conn);
	
	$qry_credit = Qry($conn,"INSERT INTO credit(credit_by,cash_id,debit_table_id,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
		('CASH','$credit_cash_id','$debit_tab_id','CREDIT-BRANCH','$branch_to','$_SESSION[user_code]','$company_to','$amount','$narration_to','$date','$timestamp')");	
		
	if(!$qry_credit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	// echo "Amount : ".$amount.", New Bal To: ".$newbal_to.", New Bal From: ".$newbal_from;
	echo "<script> 	
		alert('Branch Debited Successfully !!');
		window.location.href='./debit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./debit.php");
	exit();
}
?>