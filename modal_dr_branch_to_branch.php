<form id="DrBranchtoBranchForm" action="#" method="POST">
<div id="DrB2BModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Branch to Branch Transfer
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Credit To Branch (+) <font color="red">*</font></label>
				<select class="form-control" id="to_branch" name="to_branch" required="required">
				<option value="">Select Option</option>
				<?php
				$fetchBranch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username!='$branch' AND username 
				NOT IN('DUMMY','HEAD') ORDER BY username ASC");
				
				if(!$fetchBranch){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
					Redirect("Error while processing Request","./");
					exit();
				}
				
				if(numRows($fetchBranch)>0)
				{
					while($rowBranch = fetchArray($fetchBranch))
					{
						echo "<option value='$rowBranch[username]'>$rowBranch[username]</option>";
					}
				}
				?>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Credit to Company (+) <font color="red">*</font></label>
				<select class="form-control" id="to_company" name="to_company" required="required">
					<option value="">--Select Company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input type="number" name="amount" min="1" id="amount_transfer" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-6">
				<label>Debit From Branch (-) <font color="red">*</font></label>
				<input type="text" value="<?php echo $branch; ?>" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label>Debit From Company (-) <font color="red">*</font></label>
				<select class="form-control" id="from_company" name="from_company" required="required">
					<option value="">--Select Company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>&nbsp;</label>
				<br />
				<button type="button" onclick="SendOTPtoManager()" id="send_otp_to_manager" class="btn btn-danger">Send OTP</button>
			</div>
			
			<div class="form-group col-md-4">
				<label>OTP <font color="red">*</font></label>
				<input type="number" name="otp" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-8">
				<label>Narration <font color="red">*</font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'');" name="narration" class="form-control" required="required"></textarea>	
			</div>
			
		</div>
      </div>
	  
		<input type="hidden" id="company_cr_to" name="company_cr_to">
		<input type="hidden" id="branch_cr_to" name="branch_cr_to">
		<input type="hidden" id="debit_company" name="debit_company">
	
	  <div id="result_b_to_b_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="dr_b_to_b_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

	
<script>
function SendOTPtoManager()
{
	var amount = $("#amount_transfer").val();
	var to_branch = $("#to_branch").val();
	var to_company = $("#to_company").val();
	var from_company = $("#from_company").val();
	
	if(amount=='' || to_branch=='' || to_company=='' || from_company=='')
	{
		alert('All above fields are required first !');
		$("#amount_transfer").val('');
		$("#to_branch").val('');
		$("#to_company").val('');
		$("#from_company").val('');
	}
	else
	{
		$("#amount_transfer").attr('readonly',true);
		$("#company_cr_to").val(to_company);
		$("#branch_cr_to").val(to_branch);
		$("#debit_company").val(from_company);
		$("#to_branch").attr('disabled',true);
		$("#to_company").attr('disabled',true);
		$("#from_company").attr('disabled',true);
			
		$('#send_otp_to_manager').attr('disabled',true);
		$("#loadicon").show();
		$("#dr_b_to_b_button").attr('disabled',true);
		$.ajax({
		url: "_otp_to_manager.php",
		method: "post",
		data:'to_branch=' + to_branch + '&to_company=' + to_company + '&amount=' + amount,
		success: function(data){
			$("#result_b_to_b_modal").html(data);
		}})
	}
}
</script>
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#DrBranchtoBranchForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#dr_b_to_b_button").attr("disabled", true);
	$.ajax({
        	url: "./save_dr_branch_to_branch.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_b_to_b_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>