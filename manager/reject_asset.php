<?php 
require_once '../connection.php';

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($type=='asset')
{
	$vou_type="ASSET_REQ";
	
	$get_data = Qry($conn,"SELECT c.title,a.req_code,a.date,a.branch,e.name,a.maker,a.model,a.narration,a.approval FROM asset_request AS a 
	LEFT OUTER JOIN asset_category AS c ON c.id=a.category 
	LEFT OUTER JOIN emp_attendance AS e ON e.code=a.branch_user 
	WHERE a.id='$id'");
	
	if(!$get_data){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error while processing request.","../");
		exit();
	}

	$row = fetchArray($get_data);

	if($row['approval']=="1")
	{
		echo "<script>
			alert('Request already approved !');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}

	$branch_user = $row['name'];
	$req_code = $row['req_code'];

	$data = "Req.Code: $req_code, Branch: $row[branch]($branch_user), Category: $row[title], Maker: $row[maker], Model: $row[model], Date: $row[date], Narration: $row[narration]";	
	$table="asset_request";
}
else
{
	$vou_type="ASSET_VEH_REQ";
	
	$get_data = Qry($conn,"SELECT a.req_code,a.req_date,a.branch,e.name,a.maker_name,a.model_name,a.narration,a.approval 
	FROM asset_vehicle_req AS a 
	LEFT OUTER JOIN emp_attendance AS e ON e.code=a.branch_user 
	WHERE a.id='$id'");
	
	if(!$get_data){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		Redirect("Error while processing request.","../");
		exit();
	}

	$row = fetchArray($get_data);

	if($row['approval']=="1")
	{
		echo "<script>
			alert('Request already approved !');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}

	$branch_user = $row['name'];
	$req_code = $row['req_code'];

	$data = "Req.Code: $req_code, Branch: $row[branch]($branch_user), Maker: $row[maker_name], Model: $row[model_name], Date: $row[req_date], Narration: $row[narration]";	
	$table="asset_vehicle_req";
}

StartCommit($conn);
$flag = true;

$qry = Qry($conn,"DELETE FROM `$table` WHERE id='$id'");

if(!$qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$req_code','$vou_type','REQ_DEL','$data','$branch','$branch_sub_user','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}


if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Rejected Successfully !');
		$('#RejectAsset$id').html('Rejected');
		$('#ApproveAsset$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	echo "<script>
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>