<?php 
require_once '../connection.php';

$branch = escapeString($conn,strtoupper($_SESSION['user']));

if(isset($_SESSION['manager_login']))
{
	echo "<script>
		window.location.href='./index.php';
	</script>";
}
?>
<html>

<?php
include("../_header.php");
echo '<link href="../css/styles.css" rel="stylesheet">';
include("../_loadicon.php");
include("../disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#managerForm").on('submit',(function(e) {
	$("#loadicon").show();
		e.preventDefault();
		$.ajax({
        	url: "./login_validate.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result").html(data);
				$("#loadicon").hide();
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function ShowPassFunc(){
	$('#password1').attr('type','text');
	$('#ShowBtn').hide();
	$('#HideBtn').show();
}
function HidePassFunc(){
	$('#password1').attr('type','password');
	$('#HideBtn').hide();
	$('#ShowBtn').show();
}
</script>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include '../sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" id="managerForm" method="POST">	
	
<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Manager Log-in</h4></center>
	</div>

	<div class="form-group col-md-12">
		<label>Mobile Number <font color="red">*</font></label>
		<input type="text" class="form-control" id="username1" name="username" 
		oninput="this.value=this.value.replace(/[^0-9]/,'');" maxlength="10" required="required" />
	</div>
	
	<div class="form-group col-md-12">
		<label>Password <font color="red">*</font> 
		&nbsp; &nbsp; <span id="ShowBtn" onclick="ShowPassFunc()" class="btn btn-xs btn-default" style="">show Password</span>
		<span id="HideBtn" style="display:none" onclick="HidePassFunc()" class="btn btn-xs btn-default">hide Password</span>
		</label>
		<input type="password" id="password1" class="form-control" name="password" 
		oninput="this.value=this.value.replace(/[^a-zA-Z.-@#0-9]/,'');" required="required" />
	</div>
	
	<div class="form-group col-md-12">
		<label><a id="password_link" style="color:#FFF" href="#" onclick="ResetPassword()">Forgot Password</a></label>		
	</div>
	
		<input type="hidden" id="loc_error" name="loc_error" />
		<input type="hidden" id="loc_lat" name="loc_lat" />
		<input type="hidden" id="loc_long" name="loc_long" />

	<div class="form-group col-md-12">	
		<button id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit">Log in</button>
	</div>
	
	<div class="form-group col-md-12" id="result">
	</div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>

<script type="text/javascript">
function ResetPassword()
{
	var username = $('#username1').val();
	
	if(username=='')
	{
		alert('Enter username first !');
	}
	else
	{
		$("#loadicon").show();
		$('#password_link').attr('disabled',true);
			jQuery.ajax({
				url: "./reset_password.php",
				data: 'username=' + username,
				type: "POST",
				success: function(data) {
					$("#result").html(data);
				},
			error: function() {}
		});
	}	
}
</script>
<script>
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};
	function getLocation() {
		  if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition, showError,options);
		  } else { 
			$('#loc_lat').val("");
			$('#loc_long').val("");
			$('#loc_error').val("Geolocation is not supported by this browser.");
		  }
		}

		function showPosition(position) {
			$('#loc_lat').val(position.coords.latitude);
			$('#loc_long').val(position.coords.longitude);
			$('#loc_error').val("");
		}

		function showError(error) {
		$('#loc_lat').val("");
		$('#loc_long').val("");
		  switch(error.code) {
			case error.PERMISSION_DENIED:
			  $('#loc_error').val("User denied the request for Geolocation.");
			  break;
			case error.POSITION_UNAVAILABLE:
			  $('#loc_error').val("Location information is unavailable.");
			  break;
			case error.TIMEOUT:
			  $('#loc_error').val("The request to get user location timed out.");
			  break;
			case error.UNKNOWN_ERROR:
			  $('#loc_error').val("An unknown error occurred.");
			  break;
		  }
		}
getLocation();
</script>