<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT code,branch,name,status,mobile_no FROM emp_attendance WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_approval.php");
	exit();
}

$row_data = fetchArray($get_data);

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['status']!="0")
{	
	echo "<script>
		alert('Employee not newly added !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$emp_code = $row_data['code'];
$emp_name = $row_data['name'];

$get_record = Qry($conn,"SELECT * FROM emp_attendance WHERE id='$id'");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$log_record = array();

while($row_record = fetchArray($get_record))
{
    foreach($row_record as $key => $value)
    {
		$log_record[]=$key."->".$value;
    }
}

$log_record = implode(', ',$log_record); 

StartCommit($conn);
$flag = true;

$delete_emp = Qry($conn,"DELETE FROM emp_attendance WHERE id='$id'");

if(!$delete_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$emp_code($emp_name)','Employee_add','Employee_deleted','$log_record','$branch','$branch_sub_user','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $emp_name. Deleted Successfully !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_approval.php");
	exit();
}

?>