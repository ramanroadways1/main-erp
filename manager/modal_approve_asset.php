<button type="button" style="display:none" id="BtnAssetApproveModal" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ModalAssetApprove"></button>

<script type="text/javascript">
	$(function() {
		$("#modal_party").autocomplete({
		source: '../autofill/get_asset_party.php',
		select: function (event, ui) { 
               $('#modal_party').val(ui.item.value);   
               $('#modal_party_id').val(ui.item.id);     
               $('#modal_party_pan').val(ui.item.pan_no);     
               $('#modal_party_gst').val(ui.item.gst_no);     
               $('#modal_ac_details').val(ui.item.ac_details);     
               $("#buttonApprove1").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Party does not exists.');
			$("#modal_party").val('');
			$("#modal_party").focus();
			$("#modal_party_id").val('');
			$("#modal_party_pan").val('');
			$("#modal_party_gst").val('');
			$("#modal_ac_details").val('');
			$("#buttonApprove1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<form id="Form1" action="#" method="POST">
<div id="ModalAssetApprove" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Approve Asset : <span id="Modal_AssetCode" style="color:red"></span>
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-4">
				<label>Asset Catagory <font color="red"><sup>*</sup></font></label>
				<input type="text" id="modal_catagory" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Request Date <font color="red"><sup>*</sup></font></label>
				<input type="text" id="modal_date" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Username <font color="red"><sup>*</sup></font></label>
				<input type="text" id="modal_username" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Maker Name <font color="red"><sup>*</sup></font></label>
				<textarea type="text" id="modal_maker" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="maker" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Model <font color="red"><sup>*</sup></font></label>
				<textarea type="text" id="modal_model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="model" class="form-control" required="required"></textarea>
			</div>
			
			<input type="hidden" name="req_id" id="asset_id_tabs">
			
			<!--
			<div class="form-group col-md-4">
				<label>Have Party details ? <font color="red"><sup>*</sup></font></label>
				<select name="party_details" onchange="PartyDetails(this.value)" class="form-control" required="required">
					<option value="">--select option--</option>
					<option value="YES">YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			-->	
<script>		
function PartyDetails(elem)
{
	$('#modal_party').val('');
	$('#modal_party_id').val('');
	$('#modal_party_pan').val('');
	$('#modal_party_gst').val('');
	$('#modal_ac_details').val('');
		
	if(elem=='NO')
	{
		$('#modal_party').attr('readonly',true);
		$("#buttonApprove1").attr('disabled',false);
	}
	else
	{
		$('#modal_party').attr('readonly',false);
		$("#buttonApprove1").attr('disabled',true);
	}
}
</script>	

<script>
			function CheckPaymentMode()
			{
				var payment_mode = $('#modal_payment_mode').val();
				
				if(payment_mode=='')
				{
					$('#asset_amount').val('');
					$('#modal_payment_mode').focus();
				}
				else
				{
					var amount = $('#asset_amount').val();
	
					if(amount==''){
						var amount = 0;
					}else{
						var amount = Number($('#asset_amount').val());
					}
					
					var gst_per = $('#gst_value').val();
					
					if(gst_per==''){
						var gst_per = 0;
					}else{
						var gst_per = Number($('#gst_value').val());
					}
					
					var gst_amount = $('#gst_amount').val();
					
					if(gst_amount==''){
						var gst_amount = 0;
					}else{
						var gst_amount = Number($('#gst_amount').val());
					}
					
					var gst_amount = Number(amount*gst_per/100);
					$('#gst_amount').val(gst_amount.toFixed(2))
					
					var discount = $('#discount1').val();
					
					if(discount==''){
						var discount = 0;
					}else{
						var discount = Number($('#discount1').val());
					}
					
					$('#total_amount').val(Number(amount+gst_amount-discount).toFixed(2));
				}
			}
			
			function PaymentBy(elem)
			{
				if(elem=='CASH')
				{
					$('#cash_warning').show();
					$('#cheque_div').hide();
					$('#cheque_no').attr('required',false);
					$('#asset_amount').attr('max','10000');
				}
				else
				{
					$('#cash_warning').hide();
					$('#asset_amount').attr('max','');
					
					if(elem=='CHEQUE')
					{
						$('#cheque_div').show();
						$('#cheque_no').attr('required',true);
					}
					else
					{
						$('#cheque_div').hide();
						$('#cheque_no').attr('required',false);
					}
				}
			}
			</script>
			
			<div class="form-group col-md-4">
				<label>Payment Mode <font color="red"><sup>*</sup></font></label>
				<select name="payment_mode" id="modal_payment_mode" onchange="PaymentBy(this.value);$('#asset_amount').val('')" class="form-control" required="required">
					<option value="">--select option--</option>
					<option value="CASH">CASH</option>
					<option value="CHEQUE">CHEQUE</option>
					<option value="NEFT">Neft/Rtgs</option>
				</select>
			</div>
			
			<div class="form-group col-md-8">
				<label>Select Party <font color="red"><sup>* </font> &nbsp; <a href="#" onclick="$('#mainModalHide2').click()" data-toggle="modal" data-target="#ModalAssetParty" style="color:blue;font-size:12px">Add Party</a>
				</sup></label>
				<input type="text" id="modal_party" oninput="this.value=this.value.replace(/[^A-Z a-z0-9-]/,'')" name="party" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="party_id" id="modal_party_id">
			
			<div class="form-group col-md-3">
				<label>Party's PAN No <font color="red"><sup>*</sup></font></label>
				<input type="text" id="modal_party_pan" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3">
				<label>Party's GST No <font color="red"><sup>*</sup></font></label>
				<input type="text" id="modal_party_gst" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Party's A/c details <font color="red"><sup>*</sup></font></label>
				<textarea type="text" style="font-size:10px !important" id="modal_ac_details" readonly class="form-control" required="required"></textarea>
			</div>

<script>		
function GstInvoiceFunc(elem){
	
	// $('#asset_amount').attr('readonly',true);
		
	var discount = $('#discount1').val();
					
	if(discount==''){
		var discount = 0;
	}else{
		var discount = Number($('#discount1').val());
	}
					
	if(elem=='YES')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./asset_gst_checked.php",
			data: 'party_id=' + $('#modal_party_id').val() + '&tab_name=' + 'asset_request',
			type: "POST",
			success: function(data) {
				$("#result_Form1").html(data);
			},
		error: function() {}
		});
	}
	else
	{
		$('#gst_type').val('');
		$('#gst_value').val('0');
		$('#gst_value').attr('readonly',true);
		$('#gst_amount').val('0');
		$('#total_amount').val(Number($('#asset_amount').val() - discount));
	}
}
</script>		

			<div class="form-group col-md-3">
				<label>GST Invoice ? <font color="red"><sup>*</sup></font></label>
				<select name="gst_invoice" id="gst_selection" onchange="GstInvoiceFunc(this.value)" class="form-control" required="required">
					<option value="">--select option--</option>
					<option value="YES">YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			
			<div class="form-group col-md-3">
				<label>Amount <font color="red"><sup>* (w/o GST)</sup></font></label>
				<input oninput="CheckPaymentMode()" type="number" max="10000" min="1" id="asset_amount" name="asset_amount" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3">
				<label>GST Type <font color="red"><sup>*</sup></font></label>
				<input type="text" id="gst_type" name="gst_type" readonly class="form-control" required="required">
			</div>
				
			<div class="form-group col-md-3">
				<label>GST Value <font color="red"><sup>* in (%)</sup></font></label>
				<input oninput="GetGstAmount(this.value)" type="number" name="gst_value" id="gst_value" min="1" max="30" class="form-control" required="required">
			</div>

<script>			
function GetGstAmount(gst_per){
	var amount = Number($('#asset_amount').val());
	var gst_amount = Number(amount*gst_per/100);
	$('#gst_amount').val(gst_amount.toFixed(2))
	var discount = $('#discount1').val();
	
	if(discount==''){
		var discount = 0;
	}else{
		var discount = Number($('#discount1').val());
	}
	
	$('#total_amount').val(Number(amount+gst_amount-discount).toFixed(2));
}

function DiscountFunc()
{
	var amount = $('#asset_amount').val();
	
	if(amount==''){
		var amount = 0;
	}else{
		var amount = Number($('#asset_amount').val());
	}
	
	var gst_amount = $('#gst_amount').val();
	
	if(gst_amount==''){
		var gst_amount = 0;
	}else{
		var gst_amount = Number($('#gst_amount').val());
	}
	
	var discount = $('#discount1').val();
	
	if(discount==''){
		var discount = 0;
	}else{
		var discount = Number($('#discount1').val());
	}
	
	$('#total_amount').val(Number(amount+gst_amount-discount).toFixed(2));
}
</script>			
			
				<div class="form-group col-md-3">
					<label>GST Amount <font color="red"><sup>*</sup></font></label>
					<input step="any" type="number" id="gst_amount" name="gst_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Discount(-) <font color="red"><sup>*</sup></font></label>
					<input step="any" oninput="DiscountFunc(this.value)" type="number" id="discount1" name="discount" min="0" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Total Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="total_amount" name="total_amount" min="1" readonly class="form-control" required="required">
				</div>
			
			<div class="form-group col-md-4" id="cheque_div" style="display:none">
				<label>Cheque Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="cheque_no" oninput="this.value=this.value.replace(/[^A-Za-z0-9-]/,'')" name="cheque_no" 
				class="form-control" required="required">
			</div>
			
			<div style="color:red;display:none" id="cash_warning" class="form-group col-md-12">
				You have chosen the cash option. Cash will automatically be debited from the branch <span class="branchDebit"></span>
				<br>
				आपने कॅश का  ऑप्शन चुना है. ब्रांच <span class="branchDebit"></span> से नकद स्वतः ही डेबिट हो जायेगा.
			</div>
			
		</div>
      </div>
	  <div id="result_Form1"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="buttonApprove1" class="btn btn-sm btn-primary">Approve</button>
        <button type="button" id="mainModal" onclick="ResetFuncOne()" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
		<button type="button" id="mainModalHide2" style="display:none" data-dismiss="modal"></button>
	  </div>
    </div>

  </div>
</div>
</form>

<script>	
function ResetFuncOne()
{
	var id1 = $('#asset_id_tabs').val();
	$('#Form1')[0].reset();
	$('#ApproveAsset'+id1).attr('disabled',false);
}
</script>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#buttonApprove1").attr("disabled", true);
	$.ajax({
        	url: "./save_assset_approval.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Form1").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>