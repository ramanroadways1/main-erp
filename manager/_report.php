<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");

$report_name = escapeString($conn,$_REQUEST['report_name']);
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_report(report_name)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "_load_report.php",
			data: 'report_name=' + report_name,
			type: "POST",
			success: function(data) {
			$("#result_div").html(data);
			   $('#example').DataTable({ 
			   // "scrollY": 500,
				"scrollX": true,
				"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
				"bProcessing": true,
				"sPaginationType":"full_numbers",
				"dom": "lBfrtip",
				buttons: [
					'copyHtml5',
					'excelHtml5',
					// 'csvHtml5',
					// 'pdfHtml5'
				],
                 "destroy": true, //use for reinitialize datatable
				
            });
		},
		error: function() {}
	});
}
load_report('<?php echo $report_name; ?>');
</script>