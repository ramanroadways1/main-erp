<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT t.code,t.oldbranch,t.newbranch,t.approval_from,t.approval_to,e.name,e.status,e.mobile_no 
FROM emp_transfer as t 
LEFT OUTER JOIN emp_attendance AS e ON e.code=t.code 
WHERE t.id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_approval.php");
	exit();
}


$row_data = fetchArray($get_data);

if($row_data['newbranch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['approval_from']!="1")
{
	echo "<script>
		alert('Employee not approved from $row_data[oldbranch] Branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['approval_to']=="1")
{
	echo "<script>
		alert('Employee already approved !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['status']!="2")
{	
	echo "<script>
		alert('Employee not marked as transferred !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$approve = Qry($conn,"UPDATE emp_transfer SET approval_to='1',approval_to_timestamp='$timestamp' WHERE id='$id'");

if(!$approve){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$new_pass = GenPassword(8); 
// MsgSendNewPasswordTransfer($row_data['mobile_no'],$new_pass,$row_data['name'],$branch,$row_data['code']);

$msg_template="Hello, $row_data[name]($row_data[code]).\nYou are Transferred to $branch Branch.\nYour new password is: $new_pass.\nRamanRoadways.";
SendWAMsg($conn,$row_data['mobile_no'],$msg_template);

$update_emp = Qry($conn,"UPDATE emp_attendance SET password='".md5($new_pass)."',last_pass='$timestamp',active_login='1',branch='$branch',
status='3',branchtransfer='' WHERE code='$row_data[code]'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_Today_attd_from_branch = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$row_data[oldbranch]' 
AND (p+a+hd)=0");

if(!$chk_Today_attd_from_branch){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_Today_attd_from_branch)>0)
{
	$row_attd_from_branch = fetchArray($chk_Today_attd_from_branch);
	
	$update_today_data_from_branch = Qry($conn,"UPDATE emp_attd_check SET total=total-1 WHERE id='$row_attd_from_branch[id]'");
	if(!$update_today_data_from_branch){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
	if(!$chk_Today_attd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($chk_Today_attd)>0)
	{
		$row_attd = fetchArray($chk_Today_attd);
		
		$update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total+1 WHERE id='$row_attd[id]'");
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $row_data[name]. Successfully Transferred in.');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_approval.php");
	exit();
}
?>