<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

if(!isset($_SESSION['manager_login']))
{	
	echo "<script>
		window.location.href='./login.php';
	</script>";
	exit();
}

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_Approvals(){
	$('#loadicon').show();
    $.ajax({
    url: "load_employee_approvals.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

load_Approvals();
</script>

<script>
function TransferIn(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#TransferIn'+id).attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_transfer_in.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function TransferOut(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#TransferOut'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_transfer_out.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function Terminate(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#Terminate'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_terminate.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function NewEmpApprove(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#NewEmpApprove'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_new.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function NewEmpReject(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#NewEmpReject'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./reject_emp_new.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function TerminateReject(id)
{
	if(confirm("are you sure ??")==true)
	{
		$('#TerminateReject'+id).attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
			url: "./reject_emp_termiated.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>

<div id="func_result"></div>
