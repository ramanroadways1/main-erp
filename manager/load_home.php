<?php
require_once '../connection.php';

$date = date("Y-m-d"); 

$total_emp = Qry($conn,"SELECT id FROM emp_attendance WHERE branch='$branch' AND status='3'");
$total_emp = numRows($total_emp);

$total_vehicle = Qry($conn,"SELECT id FROM asset_vehicle WHERE branch='$branch' AND active='1'");
$total_vehicle = numRows($total_vehicle);

$total_asset = Qry($conn,"SELECT id FROM asset_main WHERE branch='$branch' AND active='1'");
$total_asset = numRows($total_asset);

$today_data = Qry($conn,"SELECT * FROM today_data WHERE date='$date' AND branch='$branch'");
if(numRows($today_data)>0)
{
	$row = fetchArray($today_data);	
	$truck_vou = $row['truck_vou'];
	$truck_vou_amount = $row['truck_vou_amount'];
	$exp_vou = $row['exp_vou'];
	$exp_vou_amount = $row['exp_vou_amount'];
	$fm_adv = $row['fm_adv'];
	$fm_adv_amount = $row['fm_adv_amount'];
	$fm_bal = $row['fm_bal'];
	$fm_bal_amount = $row['fm_bal_amount'];
	$diesel_qty_market = $row['diesel_qty_market'];
	$diesel_qty_own = $row['diesel_qty_own'];
	$diesel_amount_market = $row['diesel_amount_market'];
	$diesel_amount_own = $row['diesel_amount_own'];
	$rtgs = $row['neft'];
	$rtgs_amount = $row['neft_amount'];
}
else
{
	$truck_vou = 0;
	$truck_vou_amount = 0;
	$exp_vou = 0;
	$exp_vou_amount = 0;
	$fm_adv = 0;
	$fm_adv_amount = 0;
	$fm_bal = 0;
	$fm_bal_amount = 0;
	$diesel_qty_market = 0;
	$diesel_qty_own = 0;
	$diesel_amount_market = 0;
	$diesel_amount_own = 0;
	$rtgs = 0;
	$rtgs_amount = 0;
}

echo "<script>
	$('#TotalEmpSpan').html('$total_emp');
	$('#TotalAssetSpan').html('$total_asset');
	$('#TotalVehSpan').html('$total_vehicle');
	$('#TotalRtgsSpan').html('$rtgs_amount ($rtgs)');
	$('#TotalDieselSpan').html('$diesel_amount_market ($diesel_qty_market Ltr)');
	$('#TotalExpSpan').html('$exp_vou_amount ($exp_vou)');
	$('#TotalTdvSpan').html('$truck_vou_amount ($truck_vou)');
	$('#TotalFmAdvSpan').html('$fm_adv_amount ($fm_adv)');
	$('#TotalFmBalSpan').html('$fm_bal_amount ($fm_bal)');
	$('#TotalDieselSpanOwn').html('$diesel_amount_own ($diesel_qty_own Ltr)');
	$('#loadicon').fadeOut();
</script>";
?>