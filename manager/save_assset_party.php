<?php
require_once '../connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$gst_no=escapeString($conn,strtoupper($_POST['GstNo']));
$party_name=escapeString($conn,strtoupper($_POST['party_name']));
$party_legal_name=escapeString($conn,strtoupper($_POST['party_legal_name']));
$party_trade_name=escapeString($conn,strtoupper($_POST['party_trade_name']));
$pan_no=escapeString($conn,strtoupper($_POST['pan_no']));
$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
$ac_holder=escapeString($conn,strtoupper($_POST['ac_holder']));
$ac_no=escapeString($conn,strtoupper($_POST['ac_no']));
$bank_name=escapeString($conn,strtoupper($_POST['bank_name']));
$ifsc_code=escapeString($conn,strtoupper($_POST['ifsc_code']));
$address=escapeString($conn,strtoupper($_POST['address']));

if(strlen($mobile_no)!=10)
{
	echo "<script>
		alert('Invalid Mobile Number : $mobile_no.');
		$('#loadicon').fadeOut('slow');
		$('#buttonPartyADD').attr('disabled', false);
	</script>";
	exit();
}

$chkGst = Qry($conn,"SELECT party_name FROM asset_party WHERE gst_no='$gst_no'");

if(!$chkGst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGst)>0)
{
	$row_party = fetchArray($chkGst);
	
	echo "<script>
		alert('Duplicate GST Number : $gst_no ! GST already registered with party: $row_party[party_name].');
		$('#loadicon').fadeOut('slow');
		$('#buttonPartyADD').attr('disabled', false);
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO asset_party(party_type,party_name,legal_name,trade_name,mobile,address,pan_no,gst_no,ac_holder,ac_no,bank_name,ifsc_code,branch,
branch_user,timestamp) VALUES ('0','$party_name','$party_legal_name','$party_trade_name','$mobile_no','$address','$pan_no','$gst_no','$ac_holder','$ac_no','$bank_name','$ifsc_code',
'$branch','$branch_sub_user','$timestamp')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Party Added Successfully.');
	$('#modal_gstno').attr('readonly', false);
	$('#ValidateBtn').attr('disabled', false);
	$('#partyAddCloseBtn').click();
	$('#loadicon').fadeOut('slow');
	$('#buttonPartyADD').attr('disabled', false);
	// $('#BtnAssetApproveModal').click();
</script>";
?>