<?php 
require_once '../connection.php';

$qry = Qry($conn,"SELECT id,fno,com,acname,acno,ifsc,bank_name,totalamt,amount,type,pay_date,pan,tno,colset,approval,crn FROM 
rtgs_fm WHERE approval!='1' AND branch='$branch' AND type IN('ADVANCE','BALANCE')");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}
			
if(numRows($qry)==0)	
{		
	echo "<br><font color='red' size='3'><center>No pending freight memo approval found.</center></font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}	
		
	echo "<table class='table table-bordered table-striped' style='font-size:12px'>
		<tr>		
			<th>#</th>		
			<th>Vou_No</th>		
			<th>Company</th>		
			<th>Freight</th>
			<th>Amount</th>		
			<th>Vehicle_No</th>		
			<th>Date</th>		
			<th>Ref_No</th>		
			<th>A/c Details</th>		
			<th>Adv/Bal</th>		
			<th>Approve</th>		
			<th>Reset</th>		
		</tr>";
		
$num = 1;	

	while($row=fetchArray($qry))
	{		
		echo "<tr>
				<td>$num</td>
				<td>
					<form method='post' action='../smemo2.php' target='_blank'>	
						<input type='hidden' name='key' value='FM' />
						<input type='hidden' name='idmemo' value='$row[fno]' />
						<input type='submit' value='$row[fno]'/>
					</form>
				</td>
				<td>$row[com]</td>				
				<td>$row[totalamt]</td>				
				<td>$row[amount]</td>				
				<td>$row[tno]</td>				
				<td>".date('d-m-y',strtotime($row['pay_date']))."</td>				
				<td>$row[crn]</td>
				<td style='font-size:13px'>
					<b>NAME : </b>$row[acname]<br />
					<b>A/C No : </b>$row[acno]<br />
					<b>BANK : </b>$row[bank_name]<br />
					<b>IFSC : </b>$row[ifsc]
				</td>
				<td>$row[type]</td>			
		<td>
			<button type='button' id='approve_button$row[id]' onclick='approve_rtgs($row[id])' class='btn btn-sm btn-primary'>Approve</button>
		</td>";
		
		if($row['type']=='ADVANCE')
		{
		echo "<td>
			<button type='button' id='reset_button_$row[id]' onclick=ResetFM('$row[id]','ADVANCE') class='btn btn-sm btn-danger'>Advance Reset</button>
		</td>";
		}
		else
		{
		echo "<td>
			<button type='button' id='reset_button_$row[id]' onclick=ResetFM('$row[id]','BALANCE') class='btn btn-sm btn-danger'>Balance Reset</button>
		</td>";
		}
			
		echo "</tr>";
	$num++;
	}
	echo "</table>";
echo "<script>
	$('#loadicon').hide();
</script>";	
?>