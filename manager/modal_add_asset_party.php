<button type="button" style="display:none" id="PartyModalBtn" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ModalAssetParty"></button>

<script>
function ValidateGst()
{
	var gst = $('#modal_gstno').val();
	
	if(gst=='')
	{
		alert('Enter GST Number !');
	}
	else
	{
		$("#modal_gstno").attr("readonly",true);
		$("#ValidateBtn").attr("disabled",true);
		$("#buttonPartyADD").attr("disabled",true);
			
		var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
	   
	   if(gstinformat.test(gst))
	   {
			 $("#loadicon").show();
				jQuery.ajax({
				url: "./verify_gst.php",
				data: 'gst=' + gst,
				type: "POST",
				success: function(data) {
				$("#party_name_selectbox").html(data);
				},
			error: function() {}
			});
	   }
	   else
	   {
			alert('Please Enter Valid GSTIN Number');
			$("#modal_gstno").attr("readonly",false);
			$("#ValidateBtn").attr("disabled",false);
			$("#buttonPartyADD").attr("disabled",true);
		}
	}
}
</script>

<form id="FormParty" action="#" method="POST">
<div id="ModalAssetParty" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add new Party
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label>GST Number <font color="red"><sup>*</sup></font></label>
						<input name="GstNo" type="text" maxlength="15" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" id="modal_gstno" class="form-control" required="required">
					</div>
					
					<div class="form-group col-md-4">
						<label>&nbsp;</label>
						<br>
						<button type="button" id="ValidateBtn" onclick="ValidateGst()" class="btn btn-danger btn-sm">Validate GST</button>
					</div>
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label>Legal Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="new_party_legal_name" readonly class="form-control" name="party_legal_name" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Trade Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="new_party_trade_name" readonly class="form-control" name="party_trade_name" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Select Party Name <font color="red"><sup>*</sup></font></label>
				<select id="party_name_selectbox" name="party_name" class="form-control" required="required">
					<option value="">--select party name--</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN No <font color="red"><sup>*</sup></font></label>
				<input type="text" id="new_party_pan_no" readonly name="pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Mobile No <font color="red"><sup>*</sup></font></label>
				<input type="text" name="mobile_no" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>A/c Holder <font color="red"><sup>*</sup></font></label>
				<input type="text" name="ac_holder" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>A/c No <font color="red"><sup>*</sup></font></label>
				<input type="text" name="ac_no" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Bank Name <font color="red"><sup>*</sup></font></label>
				<input type="text" name="bank_name" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>IFSC Code <font color="red"><sup>*</sup></font></label>
				<input type="text" id="ifsc_code1" name="ifsc_code" maxlength="11" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ValidateIFSC()" class="form-control" required="required">
			</div>
		
			<div class="form-group col-md-8">
				<label>Party's Address <font color="red"><sup>*</sup></font></label>
				<textarea type="text" name="address" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,-/]/,'')" class="form-control" required="required"></textarea>
			</div>
			
		</div>
      </div>
	  <div id="result_FormParty"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="buttonPartyADD" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" id="partyAddCloseBtn" onclick="$('#FormParty')[0].reset();$('#BtnAssetApproveModal').click();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormParty").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#buttonPartyADD").attr("disabled", true);
	$.ajax({
        	url: "./save_assset_party.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_FormParty").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc_code1");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ifsc_code1").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("buttonPartyADD").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ifsc_code1").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("buttonPartyADD").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ifsc_code1").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("buttonPartyADD").removeAttribute("disabled");
		}
 } 			
</script>