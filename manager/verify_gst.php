<?php
require_once '../connection.php';

$gst = escapeString($conn,strtoupper($_POST['gst']));

$chkGst = Qry($conn,"SELECT legal_name FROM asset_party WHERE gst_no='$gst'");
if(!$chkGst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGst)>0)
{
	echo "<script>
		alert('Duplicate GST Number : $gst.');
		$('#new_party_legal_name').val('');
		$('#new_party_trade_name').val('');
		$('#new_party_pan_no').val('');
		$('#modal_gstno').attr('readonly',false);
		$('#ValidateBtn').attr('disabled',false);
		$('#buttonPartyADD').attr('disabled',true);
		
		$('#party_name_selectbox').html('<option value=\"\">--select party name--</option>');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
			
	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => ''.$url_verify_gst_common_api.'/commonapi/v1.1/search?aspid='.$tax_pro_asp_id.'&password='.$tax_pro_asp_password.'&Action=TP&Gstin='.$gst.'',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 900,
		   CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
	));

		$result = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if($err)
		{
			echo "<script>
				alert('Unable to Get GST Details from Server.');
					$('#new_party_legal_name').val('');
					$('#new_party_trade_name').val('');
					$('#new_party_pan_no').val('');
					$('#modal_gstno').attr('readonly',false);
					$('#ValidateBtn').attr('disabled',false);
					$('#buttonPartyADD').attr('disabled',true);
					
					$('#party_name_selectbox').html('<option value=\"\">--select party name--</option>');
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
		else
		{
			$response = json_decode($result, true);
			
			if(@$response['error'])
			{
				$errMsg = $response['error']['message'];
				echo "<script>
					alert('Error : $errMsg.');
					$('#new_party_legal_name').val('');
					$('#new_party_trade_name').val('');
					$('#new_party_pan_no').val('');
					$('#modal_gstno').attr('readonly',false);
					$('#ValidateBtn').attr('disabled',false);
					$('#buttonPartyADD').attr('disabled',true);
					
					$('#party_name_selectbox').html('<option value=\"\">--select party name--</option>');
					$('#loadicon').fadeOut('slow');
				</script>";
				exit();
			}
			else
			{
				$LegalName = $response['lgnm'];
				$trade_name = $response['tradeNam'];
				$Pan_No = substr($gst,2,-3);
				
				if($LegalName=='' AND $trade_name=='')
				{
					echo "<script>
						alert('Party not found !');
						$('#ValidateBtn').attr('disabled',false);
						$('#buttonPartyADD').attr('disabled',true);
						
						$('#party_name_selectbox').html('<option value=\"\">--select party name--</option>');
						$('#loadicon').fadeOut('slow');
					</script>";
					exit();
				}
				
				// if($trade_name=='')
				// {
					// $trade_name = $LegalName;
				// }
				
				// if($LegalName=='')
				// {
					// $LegalName = $trade_name;
				// }
			}
			
			echo "<script>
				$('#party_name_selectbox').html('<option value=\"\">--select party name--</option><option value=\"$trade_name\">$trade_name</option><option value=\"$LegalName\">$LegalName</option>');
				
				$('#new_party_legal_name').val('$trade_name');
				$('#new_party_trade_name').val('$LegalName');
				$('#new_party_pan_no').val('$Pan_No');
				$('#modal_gstno').attr('readonly',true);
				$('#ValidateBtn').attr('disabled',true);
				$('#buttonPartyADD').attr('disabled',false);
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}	
?>