<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT code,branch,name,status,mobile_no FROM emp_attendance WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_approval.php");
	exit();
}


$row_data = fetchArray($get_data);

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['status']!="0")
{	
	echo "<script>
		alert('Employee not newly added !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$new_pass = GenPassword(8);

// MsgSendNewPasswordNewEmployee($row_data['mobile_no'],$new_pass,$row_data['name'],$branch,$row_data['code']);

$msg_template="Hello, $row_data[name]($row_data[code]).\nWelcome to RamanRoadways.\nYou are now Employee of $branch Branch.\nYour Password is: $new_pass.";
SendWAMsg($conn,$row_data['mobile_no'],$msg_template);

$update_emp = Qry($conn,"UPDATE emp_attendance SET password='".md5($new_pass)."',last_pass='$timestamp',active_login='1',
status='3' WHERE id='$id'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
	if(!$chk_Today_attd){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($chk_Today_attd)>0)
	{
		$row_attd = fetchArray($chk_Today_attd);
		
		$update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total+1 WHERE id='$row_attd[id]'");
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $row_data[name]. Successfully Added to Your Branch.');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_approval.php");
	exit();
}
?>