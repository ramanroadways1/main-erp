<?php
require_once '../connection.php';
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><i class="fa fa-mobile" aria-hidden="true"></i> &nbsp; Asset Requests :</h4> 
		</div>
		
	<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr>
					<th>#</th>
					<th>Token_No</th>
					<th>Catagory</th>
					<th>Date</th>
					<th>Username</th>
					<th>Maker</th>
					<th>Model</th>
					<th>Narration</th>
					<th>Request status</th>
					<th>Approve</th>
					<th>Reject</th>
				</tr>	
<?php
$getAsset = Qry($conn,"SELECT a.id,a.req_code,a.date,a.branch,a.branch_user,a.maker,a.model,a.narration,a.approval,a.ho_approval,
c.title,u.name 
FROM asset_request AS a 
LEFT OUTER JOIN asset_category as c ON c.id=a.category
LEFT OUTER JOIN emp_attendance AS u ON u.code=a.branch_user 
WHERE a.asset_added!='1' AND a.branch='$branch'");

if(!$getAsset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getAsset)>0)
{
	$sn1=1;
	while($row = fetchArray($getAsset))
	{
		if($row['branch']==$row['branch_user']){
			$branch_user = $row['branch'];
		}
		else{
			$branch_user = $row['name'];
		}
		
		if($row['approval']=="0")
		{
			$approval_status = "<font color='red'>Manager approval pending !</font>";
		}
		else if($row['ho_approval']=="0")
		{
			$approval_status = "<font color='red'>Head-Office approval pending !</font>";
		}
		else
		{
			$approval_status = "<font color='green'>Ready to add !</font>";
		}
		
		$ReqDate = convertDate("d-m-y",$row["date"]);
		
		echo "<tr>
			<td>$sn1</td>
			<td>$row[req_code]</td>
			<td>$row[title]</td>
			<td>$ReqDate</td>
			<td>$branch_user</td>
			<td>$row[maker]</td>
			<td>$row[model]</td>
			<td>$row[narration]</td>
			<td>$approval_status</td>";
			
			if($row['approval']=="0")
			{
			echo "
			<td>
				<input type='hidden' id='BranchUser1$row[id]' value='$branch_user'>
				<input type='hidden' id='MakerName1$row[id]' value='$row[maker]'>
				<input type='hidden' id='ModelName1$row[id]' value='$row[model]'>
				<input type='hidden' id='AssetCat1$row[id]' value='$row[title]'>
				<input type='hidden' id='ReqDate$row[id]' value='$ReqDate'>
				<button type='button' id='ApproveAsset$row[id]' onclick='ApproveAsset($row[id])' 
				class='btn btn-sm btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Approve</button>
			</td>
			<td>
			<button type='button' id='RejectAsset$row[id]' onclick='RejectAsset($row[id])' 
				class='btn btn-sm btn-danger'><span class='glyphicon glyphicon-thumbs-down'></span> Reject</button>
			</td>";
			}
			else
			{
				echo "
				<td></td>
				<td></td>";
			}
			
		echo "</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='12'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	