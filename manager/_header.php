<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="../google_font.css" rel="stylesheet">
<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../css/styles.css" rel="stylesheet">

<?php 
include("../_loadicon.php");
include("../disable_right_click.php");
	
$menu_page_name = basename($_SERVER['PHP_SELF']);
$menu_page_name2=$_SERVER['QUERY_STRING'];;
?>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
.form-control
{
	border:1px solid #000;
	text-transform:uppercase;
}

#active{
	background-color:green;
}

#active>#DropDownLink{
	color:#FFF;
}

.dropdown-menu>li>a{
	padding:6px;
}

#DropDownLink{
	color:#000;
}

#DropDownLink:hover{
	background-color:DodgerBlue;
	color:#FFF;
}
.ui-autocomplete { z-index:2147483647; } 

ul>li:hover{
	background-color:#333;
}
 </style> 

</head>

<body style="background-color:#FFF;color:#000;font-family: 'Open Sans', sans-serif !important">

<nav class="navbar navbar-inverse navbar-static-top" style="margin-top:-51px;background-color:MediumSeaGreen;font-size:13px;">
  <div class="container-fluid">
   <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
       </button>
 </div>
   
<div id="navbar" class="navbar-collapse collapse">   
    <ul class="nav navbar-nav"><!--;-->
      <li id="<?php if($menu_page_name=="index.php") {echo "active";} ?>"><a style="color:#FFF" href="./">Home</a></li>
      <li class="dropdown" id="<?php if($menu_page_name=="approval.php" AND $menu_page_name2!="type=DIESEL") {echo "active";} ?>"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:#FFF" href="#">RTGS Approval <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li id="<?php if($menu_page_name2=="type=FM") {echo "active";} ?>"><a id="DropDownLink" href="./approval.php?type=FM">Freight Memo</a></li>
          <li id="<?php if($menu_page_name2=="type=TRUCK_VOU") {echo "active";} ?>"><a id="DropDownLink" href="./approval.php?type=TRUCK_VOU">Truck Voucher</a></li>
          <li id="<?php if($menu_page_name2=="type=EXP_VOU") {echo "active";} ?>"><a id="DropDownLink" href="./approval.php?type=EXP_VOU">Expense Voucher</a></li>
        </ul>
      </li>
      <li id="<?php if($menu_page_name2=="type=DIESEL") {echo "active";} ?>"><a style="color:#FFF" href="./approval.php?type=DIESEL">Diesel Approval</a></li>
      <li id="<?php if($menu_page_name=="employee_approval.php") {echo "active";} ?>"><a style="color:#FFF" href="./employee_approval.php">Employee Approval</a></li>
      <li id="<?php if($menu_page_name=="asset_approval.php") {echo "active";} ?>"><a style="color:#FFF" href="./asset_approval.php">Asset Approval</a></li>
      <li id="<?php if($menu_page_name=="vehicle_approval.php") {echo "active";} ?>"><a style="color:#FFF" href="./vehicle_approval.php">Vehicle Approval</a></li>
	
<!--	
	  <li id="<?php if($menu_page_name=="") {echo "active";} ?>"><a style="color:#FFF" href="./consumerPump/">Consumer Pump</a></li>
	  <li id="<?php if($menu_page_name=="") {echo "active";} ?>"><a style="color:#FFF" href="./fixPump/">Fix Pump/Card (RIL)</a></li>
-->
   </ul>
	<ul class="nav navbar-nav navbar-right">
	   <a href="../"><button style="margin:10px;" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
       <a href="./logout.php"><button style="margin:10px;" class="btn btn-sm btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</button></a>
    </ul>
</div>		
</div>
</nav>