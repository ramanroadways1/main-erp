<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT t.code,t.oldbranch,t.newbranch,t.approval_from,t.approval_to,e.name,e.status,e.mobile_no 
FROM emp_transfer as t 
LEFT OUTER JOIN emp_attendance AS e ON e.code=t.code 
WHERE t.id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_approval.php");
	exit();
}


$row_data = fetchArray($get_data);

if($row_data['oldbranch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['approval_from']=="1")
{
	echo "<script>
		alert('Employee already approved !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['approval_to']=="1")
{
	echo "<script>
		alert('Employee already approved !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['status']!="2")
{	
	echo "<script>
		alert('Employee not marked as transferred !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$approve = Qry($conn,"UPDATE emp_transfer SET approval_from='1',approval_from_timestamp='$timestamp' WHERE id='$id'");

if(!$approve){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// SendNewPassword($row_data['mobile_no'],"12345",$row_data['name'],$row_data['newbranch'],$row_data['code']);

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $row_data[name]. Successfully Transferred Out.');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_approval.php");
	exit();
}
?>