<?php 
require_once '../connection.php';

$id = escapeString($conn,$_POST['id']);

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$approve_qry = Qry($conn,"UPDATE diesel_fm SET approval='1',timestamp_approve='$timestamp' WHERE id='$id'");
if(!$approve_qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

	echo "<script>
		$('#approve_diesel_button$id').html('Approved');
		$('#loadicon').hide();
	</script>";
?>