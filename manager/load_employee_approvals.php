<?php
require_once '../connection.php';
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><span class="glyphicon glyphicon-save"></span> &nbsp; Employee Transfer in :</h4> 
		</div>
		
	<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:13px;">
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>Current Branch</th>
					<th>Timestamp</th>
					<th>Username</th>
					<th>Approve</th>
				</tr>	
<?php
$view_emp_in = Qry($conn,"SELECT t.id,t.oldbranch,t.timestamp,t.approval_from,e.name,e.code,e.join_date,e.father_name,e.mobile_no,
e.acc_pan,u.name as user_name FROM emp_transfer AS t 
LEFT OUTER JOIN emp_attendance as e ON e.code=t.code
LEFT OUTER JOIN emp_attendance AS u ON u.code=t.user_code 
WHERE t.newbranch='$branch' AND t.approval_to!='1'");

if(!$view_emp_in){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_emp_in)>0)
{
	$sn1=1;
	while($row_in = fetchArray($view_emp_in))
	{
		if($row_in['join_date']==0){
			$join_date_in = "NULL";
		}
		else
		{
			$join_date_in = convertDate("d-m-y",$row_in["join_date"]);
		}
		echo "<tr>
			<td>$sn1</td>
			<td>$row_in[name]</td>
			<td>$row_in[code]</td>
			<td>$join_date_in</td>
			<td>$row_in[father_name]</td>
			<td>$row_in[mobile_no]</td>
			<td>$row_in[oldbranch]</td>
			<td>".convertDate("d-m-y H:i A",$row_in["timestamp"])."</td>
			<td>$row_in[user_name]</td>
			<td>";
		if($row_in['approval_from']=="0"){
			echo "<font color='red'>$row_in[oldbranch] Approvel pending.</font>";
		}else{
			echo "<button type='button' id='TransferIn$row_in[id]' onclick='TransferIn($row_in[id])' 
		class='btn btn-success'><span class='glyphicon glyphicon-thumbs-up'></span></a>";
		}
		echo "</td>
		</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='10'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<div class="row"><div class="form-group col-md-12"><hr></div></div>
<!-- ------------------------------------------------Transfer Out Start------------------------------------->	


<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><span class="glyphicon glyphicon-share-alt"></span> &nbsp; Employee Transfer Out :</h4> 
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:13px;">
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>Transfer To</th>
					<th>Timestamp</th>
					<th>Username</th>
					<th>Approve</th>
				</tr>	
<?php
$view_transfer_out = Qry($conn,"SELECT t.id,t.newbranch,t.timestamp,e.name,e.code,e.join_date,e.father_name,e.mobile_no,
e.acc_pan,u.name as user_name FROM emp_transfer AS t 
LEFT OUTER JOIN emp_attendance as e ON e.code=t.code
LEFT OUTER JOIN emp_attendance AS u ON u.code=t.user_code 
WHERE t.oldbranch='$branch' AND t.approval_from!='1'");

if(!$view_transfer_out){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_transfer_out)>0)
{
	$sn2=1;
	while($row_t_out = fetchArray($view_transfer_out))
	{
		if($row_t_out['join_date']==0){
			$join_date_out = "NULL";
		}
		else
		{
			$join_date_out = convertDate("d-m-y",$row_t_out["join_date"]);
		}
		
		echo "<tr>
			<td>$sn2</td>
			<td>$row_t_out[name]</td>
			<td>$row_t_out[code]</td>
			<td>$join_date_out</td>
			<td>$row_t_out[father_name]</td>
			<td>$row_t_out[mobile_no]</td>
			<td>$row_t_out[newbranch]</td>
			<td>".convertDate("d-m-y H:i A",$row_t_out["timestamp"])."</td>
			<td>$row_t_out[user_name]</td>
			<td><button type='button' id='TransferOut$row_t_out[id]' onclick='TransferOut($row_t_out[id])' 
		class='btn btn-success'><span class='glyphicon glyphicon-thumbs-up'></span></a></td>
		</tr>";
		
	$sn2++;	
	}
}
else
{
	echo "<tr><td colspan='10'>No records found.</td></tr>";
}
			?>		
			</table>
		</div>
		
	</div>
	
	
<div class="row"><div class="form-group col-md-12"><hr></div></div>	

<!-- ------------------------------------------------ Termination Approval ------------------------------------->		


<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:Red"><span class="glyphicon glyphicon-ban-circle"></span> &nbsp; Employee Terminated :</h4> 
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:13px;">
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>Timestamp</th>
					<th>Added_By</th>
					<th>Approve</th>
					<th>Reject</th>
				</tr>	
<?php
$view_terminated = Qry($conn,"SELECT t.id,t.code,t.branch,t.timestamp,e.name,e.join_date,e.father_name,e.mobile_no,u.name 
as user_name FROM emp_terminated as t
LEFT OUTER JOIN emp_attendance AS e ON e.code=t.code 
LEFT OUTER JOIN emp_attendance AS u ON u.code=t.user_code 
WHERE t.approval='0' AND t.branch='$branch'");

if(!$view_terminated){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_terminated)>0)
{
	$sn3=1;
	while($row_t = fetchArray($view_terminated))
	{
		if($row_t['join_date']==0){
			$join_date_t = "NULL";
		}
		else{
			$join_date_t = convertDate("d-m-y",$row_t["join_date"]);
		}
		
		echo "<tr>
			<td>$sn3</td>
			<td>$row_t[name]</td>
			<td>$row_t[code]</td>
			<td>$join_date_t</td>
			<td>$row_t[father_name]</td>
			<td>$row_t[mobile_no]</td>
			<td>".convertDate("d-m-y H:i A",$row_t["timestamp"])."</td>
			<td>$row_t[user_name]</td>
			<td><button type='button' id='Terminate$row_t[id]' onclick='Terminate($row_t[id])' 
		class='btn btn-success'><span class='glyphicon glyphicon-thumbs-up'></span></a></td>
		
		<td><button type='button' id='TerminateReject$row_t[id]' onclick='TerminateReject($row_t[id])' 
		class='btn btn-danger'><span class='glyphicon glyphicon-thumbs-down'></span></a></td>
		</tr>";
		
	$sn3++;	
	}
}
else
{
	echo "<tr><td colspan='10'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>	

<div class="row"><div class="form-group col-md-12"><hr></div></div>	

<!-- ------------------------------------------------ New Employee Approval ------------------------------------->		


<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:green"><span class="glyphicon glyphicon-ban-circle"></span> &nbsp; New Employee Approval :</h4> 
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:13px;">
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>Timestamp</th>
					<th>Username</th>
					<th>Approve</th>
					<th>Reject</th>
				</tr>	
<?php
$view_new_emp = Qry($conn,"SELECT e.id,e.name,e.code,e.join_date,e.father_name,e.mobile_no,e.timestamp,u.name as added_by 
FROM emp_attendance AS e 
LEFT OUTER JOIN emp_attendance AS u ON u.code=e.user_code
WHERE e.status='0' AND e.branch='$branch'");

if(!$view_new_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_new_emp)>0)
{
	$sn4=1;
	while($row_new = fetchArray($view_new_emp))
	{
		if($row_new['join_date']==0){
			$join_date_t = "NULL";
		}
		else{
			$join_date_t = convertDate("d-m-y",$row_new["join_date"]);
		}
		
		echo "<tr>
			<td>$sn4</td>
			<td>$row_new[name]</td>
			<td>$row_new[code]</td>
			<td>$join_date_t</td>
			<td>$row_new[father_name]</td>
			<td>$row_new[mobile_no]</td>
			<td>".convertDate("d-m-y H:i A",$row_new["timestamp"])."</td>
			<td>$row_new[added_by]</td>
			<td><button type='button' id='NewEmpApprove$row_new[id]' onclick='NewEmpApprove($row_new[id])' 
		class='btn btn-success'><span class='glyphicon glyphicon-thumbs-up'></span></a></td>
		<td><button type='button' id='NewEmpReject$row_new[id]' onclick='NewEmpReject($row_new[id])' 
		class='btn btn-danger'><span class='glyphicon glyphicon-thumbs-down'></span></a></td>
		</tr>";
		
	$sn4++;	
	}
}
else
{
	echo "<tr><td colspan='10'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>	
	
<script>
	$('#loadicon').hide();
</script>