<?php 
require_once '../connection.php';

$type=escapeString($conn,strtoupper($_REQUEST['type']));
$date = date("Y-m-d"); 

if(!isset($_SESSION['manager_login']))
{	
	echo "<script>
		window.location.href='./login.php';
	</script>";
	exit();
}

if($type!='FM' AND $type!='TRUCK_VOU' AND $type!='EXP_VOU' AND $type!='DIESEL')
{	
	echo "<script>
		window.location.href='./index.php';
	</script>";
	exit();
}

if($type=="FM"){
	$text = "Freight Memo";
}
else if($type=='TRUCK_VOU'){
	$text = "Truck Voucher";
}
else if($type=='EXP_VOU'){
	$text = "Expense Voucher";
}
else if($type=='DIESEL'){
	$text = "Diesel";
}
else{
	echo "<script>
		window.location.href='./index.php';
	</script>";
	exit();
}

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-2">
		<h5 style="color:#000"><?php echo $text." Approval"; ?></h5>
	</div>
</div>
	
<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>   


<script type="text/javascript">
function load_rtgs(){
	$('#loadicon').show();
    $.ajax({
    url: "load_rtgs.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

function load_diesel(){
	$('#loadicon').show();
    $.ajax({
    url: "load_diesel.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

function load_exp_vou(){
	$('#loadicon').show();
    $.ajax({
    url: "load_exp.php",
    cache: false,
    success: function(data){
      $("#result_div").html(data);
	} 
  });
}

function load_truck_vou(){
	$('#loadicon').show();
    $.ajax({
    url: "load_truck.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}
</script>
		
<script type="text/javascript">
function approve_rtgs(id){
	$('#approve_button'+id).attr('disabled',true);
	$('#loadicon').show();
	$.ajax({
		url: "approve_rtgs.php",
		method: "post",
		data:'id=' + id,
		success: function(data){
		$("#function_result").html(data);
	}})
}

function approve_diesel(id){
	$('#approve_diesel_button'+id).attr('disabled',true);
	$('#loadicon').show();
   $.ajax({
		url: "approve_diesel.php",
		method: "post",
		data:'id=' + id,
		success: function(data){
		$("#function_result").html(data);
	}})
}
</script>

<div id="function_result"></div>

<?php
if($type=="FM"){
	$FuncName = "load_rtgs()";
}
else if($type=='TRUCK_VOU'){
	$FuncName = "load_truck_vou()";
}
else if($type=='EXP_VOU'){
	$FuncName = "load_exp_vou()";
}
else if($type=='DIESEL'){
	$FuncName = "load_diesel()";
}
else{
	echo "<script>
		window.location.href='./index.php';
	</script>";
	exit();
}
?>
<script>
<?php echo $FuncName; ?>

function ResetFM(id,type1)
{
	if(confirm("Are you sure ?")==true)
	{
		$('#reset_button_'+id).attr('disabled',true);
		$('#loadicon').show(); 
		$.ajax({
			url: "reset_adv_bal.php",
			method: "post",
			data:'id=' + id + '&type=' + type1,
			success: function(data){
			$("#function_result").html(data);
		}})
	}
}
</script>
