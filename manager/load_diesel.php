<?php 
require_once '../connection.php';

$qry = Qry($conn,"SELECT id,fno,com,disamt,tno,lrno,type,dsl_by,dsl_nrr,pay_date FROM diesel_fm WHERE approval!='1' AND disamt>0 AND 
branch='$branch'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

if(numRows($qry)==0)	
{		
	echo "<br><font color='red' size='3'><center>No pending diesel approval found.</center></font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

	echo "<table class='table table-bordered table-striped' style='font-size:12px'>
		<tr>		
			<th>#</th>		
			<th>VouId</th>		
			<th>Company</th>		
			<th>Adv/Bal</th>		
			<th>Amount</th>		
			<th>TruckNo</th>		
			<th>LR_No</th>		
			<th>Card/Pump</th>		
			<th>Narration</th>		
			<th>Date</th>		
			<th>Approval</th>		
		</tr>";
	$num = 1;	
	
	while($row = fetchArray($qry))		
	{
		echo "<tr>
				<td>$num</td>
				<td>
				<form method='post' action='../smemo2.php' target='_blank'>	
					<input type='hidden' name='key' value='FM' />
					<input type='hidden' name='idmemo' value='$row[fno]' />
					<input type='submit' value='$row[fno]'/>
				</form>
				</td>
				<td>$row[com]</td>
				<td>$row[type]</td>
				<td>$row[disamt]</td>
				<td>$row[tno]</td>
				<td>$row[lrno]</td>
				<td>$row[dsl_by]</td>
				<td>$row[dsl_nrr]</td>
				<td>".date('d-m-y',strtotime($row['pay_date']))."</td>
				<td>
	<button type='button' id='approve_diesel_button$row[id]' onclick='approve_diesel($row[id])' class='btn btn-sm btn-primary'>Approve</button>
				</td>
			</tr>";
			$num++;
	}
echo "</table>";

echo "<script>
	$('#loadicon').hide();
</script>";
?>