<?php 
require_once '../connection.php';

$qry = Qry($conn,"SELECT id,fno,com,tno,acname,acno,ifsc,bank_name,totalamt,amount,type,pay_date,pan,crn FROM rtgs_fm 
WHERE approval!='1' AND branch='$branch' AND type='TRUCK_ADVANCE'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}
			
if(numRows($qry)==0)	
{		
	echo "<br><font color='red' size='3'><center>No pending truck voucher approval found.</center></font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

	
echo "<table class='table table-bordered table-striped' style='font-size:12px'>
		<tr>		
			<th>#</th>		
			<th>Vou_No</th>		
			<th>Company</th>		
			<th>Vehicle_No</th>		
			<th>Amount</th>		
			<th>A/c Details</th>		
			<th>PAN No.</th>		
			<th>Date</th>		
			<th>Approval</th>		
		</tr>";
	$num = 1;	

while($row = fetchArray($qry))		
{
		echo "<tr>
			<td>$num</td>
			<td>$row[fno]<br><br> <b>Ref_No: $row[crn]</td>
			<td>$row[com]</td>
			<td>$row[tno]</td>
			<td>$row[amount]</td>
			<td>
				<b>NAME : </b>$row[acname]<br />
				<b>A/C No : </b>$row[acno]<br />
				<b>BANK : </b>$row[bank_name]<br />
				<b>IFSC : </b>$row[ifsc]
			</td>
			<td>$row[pan]</td>
			<td>".date('d-m-y',strtotime($row['pay_date']))."</td>
			<td>						
		<button type='button' id='approve_button$row[id]' onclick='approve_rtgs($row[id])' class='btn btn-sm btn-primary'>Approve</button>		
			</td>
		</tr>";
	$num++;
}

echo "</table>";
echo "<script>
	$('#loadicon').hide();
</script>";		
?>