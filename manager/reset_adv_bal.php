<?php 
require_once '../connection.php';

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$qry = Qry($conn,"SELECT id,fno,com,amount,type,approval FROM rtgs_fm WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

if(numRows($qry) == 0)
{
	echo "<script>
		alert('No record found !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$row = fetchArray($qry);

if($row['type'] != $type)
{
	echo "<script>
		alert('Something went wrong !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($row['approval']=="1")
{
	echo "<script>
		alert('Payment already approved !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$frno = $row['fno'];

$get_fm = Qry($conn,"SELECT id,company,branch_user,adv_branch_user,baladv,unloadd,detention,gps_rent,gps_device_charge,bal_tds,otherfr,paidto,
adv_date,bal_date,cashadv,chqadv,disadv,rtgsneftamt,paycash,paycheq,paydsl,newrtgsamt,gps_id,branch,branch_bal,claim,late_pod,totalbal,
bal_branch_user,ptob,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv FROM freight_form WHERE frno='$frno'");

if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

if(numRows($get_fm) == 0)
{
	echo "<script>
		alert('Freight-Memo record found !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

if($row_fm['gps_rent']>0)
{
	echo "<script>
		alert('GPS Rent deducted. Can\'t reset balance !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($row_fm['gps_device_charge']>0)
{
	echo "<script>
		alert('GPS Device charges deducted. Can\'t reset balance !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($row_fm['gps_id']!=0)
{
	echo "<script>
		alert('Unable to Reset. Freight-Memo linked with GPS !');
		$('#reset_button_$id').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$company = $row_fm['company'];
$frno_id = $row_fm['id'];

$update_log_adv = "Branch : $row_fm[branch]($row_fm[branch_user]), Company: $company, O/B: $row_fm[ptob], Loading(+): $row_fm[newtds], 
Diesel_Inc(+): $row_fm[dsl_inc], GPS(-): $row_fm[gps], Adv_Claim(-): $row_fm[adv_claim], Others(-): $row_fm[newother], 
TDS(-): $row_fm[tds], Total_Freight: $row_fm[totalf], Total_Adv: $row_fm[totaladv], Adv_date: $row_fm[adv_date], 
Adv_User: $row_fm[adv_branch_user], Cash_adv: $row_fm[cashadv], Cheq_adv: $row_fm[chqadv], Diesel_adv: $row_fm[disadv], 
Rtgs_adv: $row_fm[rtgsneftamt].";

$update_log = "Branch : $row_fm[branch]($row_fm[branch_user]), AdvUser: $row_fm[adv_branch_user], Company: $company, O/B: $row_fm[paidto], 
BalanceAmount: $row_fm[baladv], Unloading: $row_fm[unloadd], Detention: $row_fm[detention], GPS_Rent: $row_fm[gps_rent], 
GPS_Device_Charge: $row_fm[gps_device_charge], TDS: $row_fm[bal_tds], Others: $row_fm[otherfr], Claim: $row_fm[claim], 
Late_POD: $row_fm[late_pod], Total_Bal: $row_fm[totalbal], Bal_date: $row_fm[bal_date], Cash: $row_fm[paycash], Cheq: $row_fm[paycheq], 
Diesel: $row_fm[paydsl], Rtgs: $row_fm[newrtgsamt], Bal_Branch: $row_fm[branch_bal]($row_fm[bal_branch_user]).";

if($row['type']=='BALANCE')
{
	$total_balance = $row_fm['paycash']+$row_fm['paycheq']+$row_fm['paydsl']+$row_fm['newrtgsamt']; 
	
	$fm_branch = $row_fm['branch_bal'];
	
	if($row_fm['paydsl']>0)
	{
		$chk_diesel = Qry($conn,"SELECT id FROM diesel_fm WHERE fno='$frno' AND type='BALANCE' AND IF(dsl_by='PUMP',colset_d,done)='1'");
		
		if(!$chk_diesel){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_diesel)>0)
		{
			echo "<script>
				alert('Diesel request done by fuel-department !');
				$('#reset_button_$id').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();	
		}
		
		$chk_diesel_sum = Qry($conn,"SELECT SUM(qty) as total_qty FROM diesel_fm WHERE fno='$frno' AND type='BALANCE'");
		
		if(!$chk_diesel_sum){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		$row_diesel_qty = fetchArray($chk_diesel_sum);
		$diesel_Qty = $row_diesel_qty['total_qty'];
	}
	else
	{
		$diesel_Qty = 0;
	}
	
	StartCommit($conn);
	$flag = true;
	
	if($row_fm['newrtgsamt']>0)
	{
		if($row_fm['bal_date'] == $date)
		{	
			$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$row_fm[newrtgsamt]' WHERE 
			branch='$fm_branch' AND date='$date'");
			
			if(!$update_today_data_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$neft_delete=Qry($conn,"DELETE FROM rtgs_fm WHERE id='$id' AND colset_d!='1'");
		
		if(!$neft_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("UNABLE TO DELETE RTGS PAYMENT WHEN RTGS AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
				
		$passbook_delete=Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Balance NEFT/RTGS Amount'");
		
		if(!$passbook_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row_fm['paydsl']>0)
	{
		if($row_fm['bal_date']==$date)
		{	
			$update_today_data_dsl=Qry($conn,"UPDATE today_data SET diesel_qty_market=diesel_qty_market-'$diesel_Qty',
			diesel_amount_market=diesel_amount_market-'$row_fm[paydsl]' WHERE branch='$fm_branch' AND date='$date'");	
			
			if(!$update_today_data_dsl){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$diesel_delete=Qry($conn,"DELETE FROM diesel_fm WHERE fno='$frno' AND type='BALANCE' AND 
		IF(dsl_by='PUMP',approval,done)!='1'");
		
		if(!$diesel_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("UNABLE TO DELETE Diesel PAYMENT WHEN Diesel AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
	}

	if($row_fm['paycheq']>0)
	{
		$cheque_delete = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Balance Cheque Amount'");
		if(!$cheque_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$cheque_delete2=Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno' AND pay_type='BAL'");
		if(!$cheque_delete2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	
	if($row_fm['paycash']>0)
	{
		$get_cash_id = Qry($conn,"SELECT id,debit,debit2 FROM cashbook WHERE vou_no='$frno' AND desct='Balance Cash Amount'");
		
		if(!$get_cash_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_cash_id)==0)
		{
			$flag = false;
			errorLog("UNABLE TO FETCH CASH ENTRY WHILE CASH AMOUNT IS GREATER THAN 0. Vou_No : $frno.",$conn,$page_name,__LINE__);
		}
				
		$row_cash = fetchArray($get_cash_id);
				
		$cash_id = $row_cash['id'];
		
		$delete_cash_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($company=='RRPL'){
			$balance_col="balance";
		}
		else{
			$balance_col="balance2";
		}
		
		$update_balance = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$row_fm[paycash]' WHERE username='$fm_branch'");
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook=Qry($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$row_fm[paycash]' WHERE id>$cash_id AND comp='$company' 
		AND user='$fm_branch'");
		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row_fm['bal_date']==$date)
	{
		$update_today_data=Qry($conn,"UPDATE today_data SET fm_bal=fm_bal-1,fm_bal_amount=fm_bal_amount-'$total_balance' 
		WHERE branch='$fm_branch' AND date='$date'");
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	
	$reset_balance = Qry($conn,"UPDATE freight_form SET unloadd=0,detention=0,gps_deposit_return=0,gps_rent=0,gps_device_charge=0,bal_tds=0,
	otherfr=0,claim=0,late_pod=0,totalbal='NA',bal_branch_user='',paidto='',bal_date=0,pto_bal_name='',bal_pan='',paycash=0,paycheq=0,
	paycheqno='',paydsl=0,newrtgsamt=0,rtgs_bal=0,narra='',colset_bal=0,colset_d_bal=0,bal_download='',pod=0,branch_bal='' WHERE frno='$frno'");
	
	if(!$reset_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}	
	
	$insertLog = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,edit_by,timestamp) VALUES 
	('$frno','FM_UPDATE','BALANCE_RESET','$update_log','$branch','$branch_sub_user','BRANCH','$timestamp')");

	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script>
			alert('Freight Memo Balance Reset Success !');
			$('#reset_button_$id').attr('disabled',true);
			$('#reset_button_$id').attr('onclick','');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		echo "<script>
			alert('Error while processing request !');
			$('#reset_button_$id').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}	
}
else
{
	$total_balance = $row_fm['cashadv']+$row_fm['chqadv']+$row_fm['disadv']+$row_fm['rtgsneftamt']; 
	
	$fm_branch = $row_fm['branch'];
	
	$get_lrs = Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno'");

	if(!$get_lrs){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./fm_view.php");
		exit();
	}

	if(numRows($get_lrs)==0)
	{
		echo "<script>
			alert('LR not found !');
			$('#reset_button_$id').attr('disabled',true);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}

	$lrnos = array();
	$lrnos_store = array();
			
	while($row_lrno=fetchArray($get_lrs))
	{
		$lrnos[] = "'".$row_lrno['lrno']."'";
		$lrnos_store[] = $row_lrno['lrno'];
	}
			
	$lrnos = implode(',', $lrnos);
	$lrnos_store = implode(',', $lrnos_store);
	
	if($diesel>0)
	{
		$chk_diesel = Qry($conn,"SELECT id FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE' AND IF(dsl_by='PUMP',colset_d,done)='1'");
		
		if(!$chk_diesel){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_diesel)>0)
		{
			echo "<script>
				alert('Diesel Request done by Fuel-Department !');
				$('#reset_button_$id').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();	
		}
		
		$chk_diesel_sum = Qry($conn,"SELECT SUM(qty) as total_qty FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE'");
		
		if(!$chk_diesel_sum){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		$row_diesel_qty = fetchArray($chk_diesel_sum);
		$diesel_Qty = $row_diesel_qty['total_qty'];
	}
	else
	{
		$diesel_Qty = 0;
	}
	
	$chk_diesel_pre_req = Qry($conn,"SELECT lrno FROM lr_sample WHERE lrno IN($lrnos) AND diesel_req>0");
	
	if(!$chk_diesel_pre_req){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./fm_view.php");
		exit();
	}
	
	if(numRows($chk_diesel_pre_req)==1)
	{
		$chk_diesel_req_entry = Qry($conn,"SELECT id FROM _pending_diesel WHERE lrno IN($lrnos)");
	
		if(!$chk_diesel_req_entry){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_diesel_req_entry)>0)
		{
			echo "<script>
				alert('Pending diesel found !');
				$('#reset_button_$id').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
		
		$requested_diesel = "1";
		
	}
	else if(numRows($chk_diesel_pre_req)>1)
	{
		echo "<script>
				alert('Multiple diesel request found !');
				$('#reset_button_$id').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		$requested_diesel = "0";
	}
	
	if($requested_diesel==1 AND $diesel==0)
	{
		echo "<script>
				alert('Diesel Error !');
				$('#reset_button_$id').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
	
	StartCommit($conn);
	$flag = true;
	
	if($requested_diesel=="1")
	{
		$copy_diesel = Qry($conn,"INSERT INTO _pending_diesel(diesel_id,fno,lrno,tno,branch) SELECT id,token_no,lrno,tno,branch FROM 
		diesel_fm WHERE fno='$frno' AND pre_req='1' AND branch='$fm_branch'");
			
		if(!$copy_diesel){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Diesel request copy failed. LRnos: $lrnos_store.",$conn,$page_name,__LINE__);
		}
		
		$change_diesel_status = Qry($conn,"UPDATE diesel_fm SET fno='',pre_and_fm='0' WHERE fno='$frno' AND pre_req='1' AND
		branch='$fm_branch'");
			
		if(!$change_diesel_status){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("Diesel status modify failed. LRnos: $lrnos_store.",$conn,$page_name,__LINE__);
		}
	}	
	
	if($row_fm['newrtgsamt']>0)
	{
		if($row_fm['adv_date']==$date)
		{	
			$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$row_fm[newrtgsamt]' WHERE 
			branch='$fm_branch' AND date='$date'");
			
			if(!$update_today_data_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$neft_delete=Qry($conn,"DELETE FROM rtgs_fm WHERE id='$id' AND type='ADVANCE' AND colset_d!='1'");
		
		if(!$neft_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("UNABLE TO DELETE RTGS PAYMENT WHEN RTGS AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
				
		$passbook_delete=Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Advance RTGS/NEFT'");
		
		if(!$passbook_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($row_fm['disadv']>0)
	{
		if($row_fm['adv_date']==$date)
		{	
			$update_today_data_dsl = Qry($conn,"UPDATE today_data SET diesel_qty_market=diesel_qty_market-'$diesel_Qty',
			diesel_amount_market=diesel_amount_market-'$row_fm[disadv]' WHERE branch='$fm_branch' AND date='$date'");	
			
			if(!$update_today_data_dsl){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$diesel_delete = Qry($conn,"DELETE FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE' AND IF(dsl_by='PUMP',colset_d,done)!='1'");
		
		if(!$diesel_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0 AND $requested_diesel==0)
		{
			$flag = false;
			errorLog("UNABLE TO DELETE Diesel PAYMENT WHEN Diesel AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
	}
		
	if($row_fm['chqadv']>0)
	{
		$cheque_delete = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Advance Cheque'");
		
		if(!$cheque_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$cheque_delete2=Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno' AND pay_type='ADV'");
		
		if(!$cheque_delete2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($row_fm['cashadv']>0)
	{
		$get_cash_id=Qry($conn,"SELECT id,debit,debit2 FROM cashbook WHERE vou_no='$frno' AND desct='Advance Cash'");
		
		if(!$get_cash_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_cash_id)==0)
		{
			$flag = false;
			errorLog("UNABLE TO FETCH CASH ENTRY WHILE CASH AMOUNT IS GREATER THAN 0. Vou_No : $frno.",$conn,$page_name,__LINE__);
		}
				
		$row_cash = fetchArray($get_cash_id);
				
		$cash_id = $row_cash['id'];
			
		$delete_cash_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
	
		if(!$delete_cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($company=='RRPL'){
			$balance_col="balance";
		}
		else{
			$balance_col="balance2";
		}
		
		$update_balance = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$row_fm[cashadv]' WHERE username='$fm_branch'");
		
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook=Qry($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$row_fm[cashadv]' WHERE id>$cash_id AND comp='$company' 
		AND user='$fm_branch'");
		
		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($row_fm['adv_date']==$date)
	{
		$update_today_data=Qry($conn,"UPDATE today_data SET fm_adv=fm_adv-1,fm_adv_amount=fm_adv_amount-'$total_advance' 
		WHERE branch='$fm_branch' AND date='$date'");
		
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	$adv_reset_fm = Qry($conn,"UPDATE freight_form SET actualf=0,newtds=0,dsl_inc=0,gps=0,adv_claim=0,newother=0,tds=0,totalf=0,
	totaladv=0,adv_branch_user='',ptob='',adv_date=0,pto_adv_name='',adv_pan='',cashadv=0,chqadv=0,chqno='',disadv=0,rtgsneftamt=0,
	rtgs_adv='0',narre='',baladv=0,colset_adv=0,colset_d_adv=0,adv_download='' WHERE frno='$frno'");
		
	if(!$adv_reset_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}		
	
	$insertLog = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,edit_by,timestamp) VALUES 
	('$frno','FM_UPDATE','ADVANCE_RESET','$update_log_adv','$branch','$branch_sub_user','BRANCH','$timestamp')");

	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script>
			alert('Freight Memo Advance Reset Success !');
			$('#reset_button_$id').attr('disabled',true);
			$('#reset_button_$id').attr('onclick','');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		echo "<script>
			alert('Error while processing request !');
			$('#reset_button_$id').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}		
}
?>