<?php
require_once '../connection.php';

$report_name = escapeString($conn,$_POST['report_name']);
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000">View : <?php echo $report_name; ?></h4> 
		</div>
		
	<div class="form-group col-md-12">
			<table id="example" class="display table table-bordered table-striped" style="font-size:12px;width:100%">
				<thead>
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Branch</th>
					<?php
					if($report_name=='Total Employees')
					{
						echo "
						<th>Emp. name</th>
						<th>Emp. code</th>
						<th>Other branch access</th>
						<th>Join date</th>
						<th>Birth date</th>
						<th>Mobile number</th>
						<th>Email id</th>
						<th>Guarantor name</th>
						";
					}
					else if($report_name=='Total Vehicles')
					{
						echo "
						<th>Vehicle holder</th>
						<th>Vehicle no.</th>
						<th>General/specific</th>
						<th>Wheeler</th>
						<th>Reg. date</th>
						<th>Owner name</th>
						<th>Added on</th>
						<th>Approved on</th>
						";
					}
					else
					{
						echo "
						<th>Asset holder</th>
						<th>Asset code</th>
						<th>Category</th>
						<th>Asset company</th>
						<th>Invoice date</th>
						<th>General/specific</th>
						<th>Added on</th>
						";
					}
					?>
				</tr>	
			 </thead>	
			  <tbody>
<?php
if($report_name=='Total Employees')
{
$get_asset = Qry($conn,"SELECT a.id,a.name as emp_name,a.code,a.branch,a.alternate_branch,a.join_date,a.birth_date,a.mobile_no,a.email_id,
a.guarantor_name
FROM emp_attendance AS a 
WHERE a.status='3' AND a.branch='$branch' AND a.code!='032'");
}
else if($report_name=='Total Vehicles')
{
$get_asset = Qry($conn,"SELECT a.id,a.vehicle_holder,a.reg_no,a.branch,a.veh_type,a.asset_type as typeof,a.reg_date,
a.owner_name,date(a.ho_approval_time) as approval_date,a.date as added_date,u.name as vehicle_holder1
FROM asset_vehicle AS a 
LEFT OUTER JOIN emp_attendance AS u ON u.code=a.vehicle_holder 
WHERE a.active='1' AND a.branch='$branch'");
}
else
{
$get_asset = Qry($conn,"SELECT a.id,a.req_code,a.branch,a.asset_company,a.invoice_date,a.typeof,date(a.timestamp) as added_date,
a.branch,a.holder as asset_holder,c.title as asset_category,u.name as asset_holder1
FROM asset_main AS a 
LEFT OUTER JOIN asset_category as c ON c.id=a.category
LEFT OUTER JOIN emp_attendance AS u ON u.code=a.holder 
WHERE a.active='1' AND a.branch='$branch'");
}

if(!$get_asset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_asset)>0)
{
	$sn1=1;
	while($row = fetchArray($get_asset))
	{
		// $ReqDate = convertDate("d-m-y",$row["date"]);
		
		if($report_name=='Total Employees')
		{
			echo "<tr>
			<td>$sn1</td>
			<td>$row[branch]</td>";
			
			echo "
			<td>$row[emp_name]</td>
			<td>$row[code]</td>";
			
			if($row['alternate_branch']==""){
				echo "<td>$row[alternate_branch]</td>";
			}else{
				echo "<td>$row[alternate_branch]</td>";
			}
			
			if($row['join_date']!="" and $row['join_date']!=0){
				echo "<td>".convertDate("d-m-y",$row["join_date"])."</td>";
			}else{
				echo "<td></td>";
			}
			
			if($row['birth_date']!="" and $row['birth_date']!=0){
				echo "<td>".convertDate("d-m-y",$row["birth_date"])."</td>";
			}else{
				echo "<td></td>";
			}
				
			echo "
			<td>$row[mobile_no]</td>
			<td>$row[email_id]</td>
			<td>$row[guarantor_name]</td>
			";
		}
		else if($report_name=='Total Vehicles')
		{
			echo "<tr>
			<td>$sn1</td>
			<td>$row[branch]</td>";
			
			if($row['vehicle_holder']!=$row['branch']){
				echo "<td>$row[vehicle_holder1]</td>";
			}else{
				echo "<td>$row[vehicle_holder]</td>";
			}
			
			echo "
			<td>$row[reg_no]</td>
			<td>$row[typeof]</td>
			<td>$row[veh_type]</td>";
			
			if($row['reg_date']!="" and $row['reg_date']!=0){
				echo "<td>".convertDate("d-m-y",$row["reg_date"])."</td>";
			}else{
				echo "<td></td>";
			}
			
			echo "<td>$row[owner_name]</td>";
			
			if($row['added_date']!="" and $row['added_date']!=0){
				echo "<td>".convertDate("d-m-y",$row["added_date"])."</td>";
			}else{
				echo "<td></td>";
			}
			
			if($row['approval_date']!="" and $row['approval_date']!=0){
				echo "<td>".convertDate("d-m-y",$row["approval_date"])."</td>";
			}else{
				echo "<td></td>";
			}
		}
		else
		{
			// echo "<tr>
			// <td>$sn1</td>
			// <td>$row[branch]</td>";
			
			// if($row['asset_holder']!=$row['branch']){
				// echo "<td>$row[asset_holder1]</td>";
			// }else{
				// echo "<td>$row[asset_holder]</td>";
			// }
			
			// echo "
			// <td>$row[req_code]</td>
			// <td>$row[asset_category]</td>
			// <td>$row[asset_company]</td>";
			
			// if($row['invoice_date']!="" and $row['invoice_date']!=0){
				// echo "<td>".convertDate("d-m-y",$row["invoice_date"])."</td>";
			// }else{
				// echo "<td></td>";
			// }
			
			// echo "
			// <td>$row[typeof]</td>
			// ";
			
			// if($row['added_date']!="" and $row['added_date']!=0){
				// echo "<td>".convertDate("d-m-y",$row["added_date"])."</td>";
			// }else{
				// echo "<td></td>";
			// }
		}
					
		echo "</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='15'>No records found.</td></tr>";
}
			?>	
 </tbody>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	