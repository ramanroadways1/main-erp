<?php 
require_once '../connection.php';

$id = escapeString($conn,$_POST['id']);

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$get_payment_date = Qry($conn,"SELECT crn,pay_date FROM rtgs_fm WHERE id='$id'");

if(!$get_payment_date){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

if(numRows($get_payment_date) == 0)
{
	echo "<script>
		alert('Payment not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_payment = fetchArray($get_payment_date);

$diff_days = round((strtotime($date) - strtotime($row_payment['pay_date'])) / (60 * 60 * 24));

$chk_exemption = Qry($conn,"SELECT id FROM extend_rtgs_approval_validity WHERE crn='$row_payment[crn]' AND approval='1'");

if(!$chk_exemption){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

if($diff_days > 30 AND numRows($chk_exemption)==0)
{
	echo "<script>
		alert('Payment is older than 30 days !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$approve_qry = Qry($conn,"UPDATE rtgs_fm SET approval='1',timestamp_approve='$timestamp' WHERE id='$id'");
if(!$approve_qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	Redirect("Error while processing request.","../");
	exit();
}

	echo "<script>
		$('#approve_button$id').html('Approved');
		$('#loadicon').hide();
	</script>";
?>