<?php
require_once '../connection.php';

$mobile = escapeString($conn,strtoupper($_POST['username']));
$timestamp = date("Y-m-d H:i:s");
$branch = escapeString($conn,($_SESSION['user']));

$check_msg_function = Qry($conn,"SELECT is_active FROM _functions where func_type='WHATSAPP_MSG'");
		
if(!$check_msg_function){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>alert('ERROR : While processing Request !');window.location.href='./';</script>";
	exit();
}
		
if(numRows($check_msg_function)==0)
{
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>alert('ERROR : Something went wrong !');window.location.href='./';</script>";
	exit();
}
	
$row_msg_func = fetchArray($check_msg_function);	
	
$msg_whatsapp_func = $row_msg_func['is_active'];

if($mobile==''){
	echo "<script>
		alert('Employee mobile number is not valid !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$verify_login = Qry($conn,"SELECT name,mobile_no,whatsapp_enabled FROM emp_attendance where code = (SELECT emp_code FROM manager WHERE branch='$branch')");
if(!$verify_login){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('ERROR : While processing Request !');	
		window.location.href='./';
	</script>";
	exit();
}
		
		if(numRows($verify_login)==0)
        {
			echo "<script>
				alert('ERROR : Manager not found !');	
				window.location.href='./';
			</script>";
			exit();
		}
		
		$row_login = fetchArray($verify_login);
		
		// if($row_login['active_login']==0)
		// {
			// echo "<script>
				// alert('WARNING : $row_login[name] You are blocked from login. Contact Your Branch !');	
				// window.location.href='./';
			// </script>";
			// exit();
		// }
		
		// if($row_login['branch']!=$branch)
		// {
			// echo "<script>
				// alert('Error : Employee not belongs to Your Branch : $branch !');	
				// window.location.href='./';
			// </script>";
			// exit();
		// }

// $code = $row_login['code'];

// $chk_manager = Qry($conn,"SELECT id FROM manager where emp_code='$code'");
		
// if(!$chk_manager){
	// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	// echo "<script>
		// alert('ERROR : While processing Request !');	
		// window.location.href='./';
	// </script>";
	// exit();
// }

// if(numRows($chk_manager)==0)
// {
	// echo "<script>
		// alert('Error : $row_login[name] is not manager of Branch $branch !');	
		// window.location.href='./';
	// </script>";
	// exit();
// }

$password = GenPassword(8);
// SendPasswordMsg($row_login['mobile_no'],$password);

if($row_login['whatsapp_enabled']=="0" OR $msg_whatsapp_func=="0")
{
	MsgSendNewPasswordManager($row_login['mobile_no'],$branch,$password);
	
	$updatePass = Qry($conn,"UPDATE manager SET pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
	
	if(!$updatePass){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	echo "<script>
			alert('Password reset success!');
			window.location.href='./';
			$('#loadicon').hide();
		</script>";
		exit();
}
else
{	
	$msg_template="Your new password for manager($branch) login is: $password.\nRamanRoadways.";
	
	$wa_msg_func = SendWAMsg($conn,$row_login['mobile_no'],$msg_template);
	
	if($wa_msg_func=="1")
	{
		$updatePass = Qry($conn,"UPDATE manager SET pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
		
		if(!$updatePass){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}

		echo "<script>
				alert('Password reset success!');
				window.location.href='./';
				$('#loadicon').hide();
			</script>";
		exit();
	}
	else
	{
		echo "<script>
				alert('WhatsApp offline ! Unable to send msg.');
				window.location.href='./';
				$('#loadicon').hide();
			</script>";
		exit();
	}
}
?>