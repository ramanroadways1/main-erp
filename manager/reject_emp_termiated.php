<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT t.code,t.branch,t.approval,t.user_code,t.timestamp,e.name as emp_name,e2.name as termination_by 
FROM emp_terminated AS t 
LEFT OUTER JOIN emp_attendance AS e ON e.code = t.code 
LEFT OUTER JOIN emp_attendance AS e2 ON e2.code = t.code 
WHERE t.id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_approval.php");
	exit();
}

$row_data = fetchArray($get_data);

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

if($row_data['approval']!="0")
{	
	echo "<script>
		alert('Employee termination approved already !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}

$emp_code = $row_data['code'];
$emp_name = $row_data['emp_name'];
$termination_by_emp = $row_data['termination_by'];
$emp_branch = $row_data['branch'];
$by_user = $row_data['user_code'];
$termination_timestamp = $row_data['timestamp'];

$log_data = "Employee_name: $emp_name($emp_code), Branch: $emp_branch, By-user: $termination_by_emp($by_user), Timestamp: $termination_timestamp.";

StartCommit($conn);
$flag = true;

$delete_termination_record = Qry($conn,"DELETE FROM emp_terminated WHERE id='$id'");

if(!$delete_termination_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$active_login = Qry($conn,"UPDATE emp_attendance SET active_login='1',status='3',terminate='0' WHERE code='$emp_code'");

if(!$active_login){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$emp_code($emp_name)','Employee_Terminate','Termination_Cancelled','$log_data','$branch','$branch_sub_user','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Cancelled Successfully !');
		window.location.href='./employee_approval.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_approval.php");
	exit();
}

?>