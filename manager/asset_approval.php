<?php
require_once '../connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

if(!isset($_SESSION['manager_login']))
{	
	echo "<script>
		window.location.href='./login.php';
	</script>";
	exit();
}

include ("./_header.php");
?>
<style>
label{
	font-size:12px !important;
}

input[type='text'],input[type='number'],select,textarea{
	font-size:12px !important;
}
select>option,option{
	font-size:12px !important;
}
</style>

<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_Approvals(){
	$('#loadicon').show();
    $.ajax({
    url: "load_asset_approvals.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

load_Approvals();
</script>

<script>
function ApproveAsset(id)
{
	$('#loadicon').show();
	
	$('#ApproveAsset'+id).attr('disabled',true);
	
	var branch_user = $('#BranchUser1'+id).val();
	var maker = $('#MakerName1'+id).val();
	var model = $('#ModelName1'+id).val();
	var cat = $('#AssetCat1'+id).val();
	var date = $('#ReqDate'+id).val();
	
	$('#modal_catagory').val(cat);
	$('#modal_date').val(date);
	$('#modal_username').val(branch_user);
	$('#modal_maker').val(maker);
	$('#modal_model').val(model);
	$('#asset_id_tabs').val(id);
		
	$('#BtnAssetApproveModal').click(); 
	$('#loadicon').hide();
}

function RejectAsset(id)
{
	if(confirm("Confirm Reject ?")==true)
	{
		$('#RejectAsset'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "./reject_asset.php",
			data: 'id=' + id + '&type=' + 'asset',
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
		error: function() {}
		});
	}
}
</script>

<div id="func_result"></div>

<?php include("./modal_approve_asset.php"); ?>
<?php include("./modal_add_asset_party.php"); ?>