<?php
require_once '../connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$gst_no=escapeString($conn,strtoupper($_POST['GstNo']));
$party_name=escapeString($conn,strtoupper($_POST['party_name']));
$pan_no=escapeString($conn,strtoupper($_POST['pan_no']));
$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
$ac_holder=escapeString($conn,strtoupper($_POST['ac_holder']));
$ac_no=escapeString($conn,strtoupper($_POST['ac_no']));
$bank_name=escapeString($conn,strtoupper($_POST['bank_name']));
$ifsc_code=escapeString($conn,strtoupper($_POST['ifsc_code']));
$address=escapeString($conn,($_POST['address']));

if(strlen($mobile_no)!=10)
{
	echo "<script>
		alert('Invalid Mobile Number : $mobile_no.');
		$('#loadicon').hide();
		$('#buttonPartyADD').attr('disabled', false);
	</script>";
	exit();
}

$chkGst = Qry($conn,"SELECT legal_name FROM asset_party WHERE gst_no='$gst_no'");
if(!$chkGst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGst)>0)
{
	echo "<script>
		alert('Duplicate GST Number : $gst_no.');
		$('#loadicon').hide();
		$('#buttonPartyADD').attr('disabled', false);
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO asset_party(party_type,legal_name,mobile,address,pan_no,gst_no,ac_holder,ac_no,bank_name,ifsc_code,branch,branch_user,
timestamp) VALUES ('1','$party_name','$mobile_no','$address','$pan_no','$gst_no','$ac_holder','$ac_no','$bank_name','$ifsc_code','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Party Added Successfully.');
	$('#modal_gstno').attr('readonly', false);
	$('#ValidateBtn').attr('disabled', false);
	$('#partyAddCloseBtn').click();
	$('#loadicon').hide();
	$('#buttonPartyADD').attr('disabled', false);
	// $('#BtnAssetApproveModal').click();
</script>";
?>