<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$branch_to = escapeString($conn,strtoupper($_POST['branch']));
$id = escapeString($conn,strtoupper($_POST['id']));

if($branch==""){
	echo "<script>
		alert('Branch not found !');
		$('#loadicon').hide();
		$('#transfer_button$id').attr('disabled',false);
	</script>";
	exit();
}

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT name,code,branch,status FROM emp_attendance WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_data = fetchArray($get_data);

$chk_manager = Qry($conn,"SELECT id FROM manager WHERE emp_code='$row_data[code]'");
if(!$chk_manager){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_manager)>0)
{
	echo "<script>
		alert('Seleted Employee is manager of your branch. You Can\'t Transfer !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

if($row_data['status']=="2")
{
	$chk_pending_transfer = Qry($conn,"SELECT newbranch,approval_from,approval_to FROM emp_transfer WHERE code='$row_data[code]' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$chk_pending_transfer){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($chk_pending_transfer)>0)
	{
		$row_chk = fetchArray($chk_pending_transfer);
		
		if($row_chk['approval_from']==0)
		{
			echo "<script>
				alert('Employee Transfer already initiated. Manager Approval pending from Your branch !');
				$('#loadicon').hide();
				$('#transfer_button$id').attr('disabled',false);
			</script>";
			exit();
		}
		else if($row_chk['approval_to']==0)
		{
			echo "<script>
				alert('Employee Transfer already initiated. Manager Approval pending from Branch $row_chk[newbranch] !');
				$('#loadicon').hide();
				$('#transfer_button$id').attr('disabled',false);
			</script>";
			exit();
		}
		else 
		{
			errorLog("Transfer already initiated but record not found in table. Code: $row_data[code]",$conn,$page_name,__LINE__);
			Redirect("Transfer already initiated but record not found.","./");
			exit();
		}
	}
	else
	{
		errorLog("Transfer already initiated but record not found in table. Code: $row_data[code]",$conn,$page_name,__LINE__);
		Redirect("Transfer already initiated but record not found.","./");
		exit();
	}
}
	
if($row_data['status']!="3")
{	
	echo "<script>
		alert('Employee is not active. You can not transfer !');
		$('#loadicon').hide();
		$('#transfer_button$id').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_transfer = Qry($conn,"INSERT INTO emp_transfer(code,oldbranch,newbranch,user_code,dated,timestamp) VALUES 
('$row_data[code]','$row_data[branch]','$branch_to','$branch_sub_user','$date','$timestamp')");

if(!$insert_transfer){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update = Qry($conn,"UPDATE emp_attendance SET active_login='0',status='2',branchtransfer='$branch_to' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
// if(!$chk_Today_attd){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// if(numRows($chk_Today_attd)>0)
// {
	// $row_attd = fetchArray($chk_Today_attd);

	// $update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total-1 WHERE id='$row_attd[id]'");
	// if(!$update_today_data){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }

// }

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Transfer of $row_data[name] to $branch_to. Initiated Successfully. Manager Approval required to confirm transfer..');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_view.php");
	exit();
}
?>