<form id="MgrLoginForm" autocomplete="off" action="#" method="POST">
<div id="MgrLoginModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Manager - Login
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12">
				<label>Username <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" maxlength="10" name="username" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>Password <font color="red"><sup>*</sup></font></label>
				<input type="password" name="password" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="loc_error">
			<input type="hidden" name="loc_lat">
			<input type="hidden" name="loc_long">
			<input type="hidden" name="mgr_flag">
			
			 <div class="form-group col-md-12" style="color:red" id="result_MgrLoginForm"></div>
			  
		</div>
      </div>
	
      <div class="modal-footer">
        <button type="submit" id="login_mgr_btn" class="btn btn-primary">Login</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#MgrLoginForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#login_mgr_btn").attr("disabled", true);
	$.ajax({
        	url: "./manager/login_validate.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data){
				$("#result_MgrLoginForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>
