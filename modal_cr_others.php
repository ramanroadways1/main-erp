<form id="CreditOthersForm" action="#" method="POST">
<div id="CrOthersModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Credit Others
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input type="number" placeholder="Enter Amount" name="amount" min="1" class="form-control" required />	
			</div>

			<div class="form-group col-md-6">
				<label>Company <font color="red">*</font></label>
				<select class="form-control" name="company" required="required">
					<option value="">--Select Company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<label>Narration <font color="red">*</font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'');" id="" name="narration" class="form-control" required="required"></textarea>	
			</div>
			
		</div>
      </div>
	  
	  <div id="result_cr_other_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="cr_others_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#CreditOthersForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#cr_others_button").attr("disabled", true);
	$.ajax({
        	url: "./save_credit_other.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_cr_other_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>