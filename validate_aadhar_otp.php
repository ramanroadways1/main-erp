<?php
session_start();
require_once("../_connect.php");

$otp = $_POST['otp'];
$client_id = $_SESSION['driver_add_aadhar_client_id'];

if(strlen($otp)!=6)
{
	echo "<script>
		alert('Error : Check OTP !');
		$('#loadicon').fadeOut('slow');
		$('#market_driver_aadhar_validate_otp_btn').attr('disabled',false);
	</script>";
	exit();
}

if(empty($client_id))
{
	echo "<script>
		alert('Error : Client id not found ! !');
		$('#loadicon').fadeOut('slow');
		$('#market_driver_aadhar_validate_otp_btn').attr('disabled',false);
	</script>";
	exit();
}

$data = array(
"otp"=>"$otp",
"client_id"=>"$client_id",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$aadhar_api_url/api/v1/aadhaar-v2/submit-otp",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$aadhar_api_token.''
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response, true);

if($result['status_code']!='200')
{
	$error_msg = clean_input($result['message']);
	$error_code = $result['status_code'];
	
	echo "<script>
		alert('Error : '+\"$error_code: $error_msg !\");
		$('#loadicon').fadeOut('slow');
		$('#market_driver_aadhar_validate_otp_btn').attr('disabled',false);
	</script>";
	exit();
}

$full_name = $result['data']['full_name'];
$dob = $result['data']['dob'];
$gender = $result['data']['gender'];  // M or F
$pincode = $result['data']['zip']; 
$care_of = $result['data']['care_of']; 
$image = $result['data']['profile_image']; 

$country = $result['data']['address']['country']; 
$dist = $result['data']['address']['dist']; 
$state = $result['data']['address']['state']; 
$po = $result['data']['address']['po']; 
$loc = $result['data']['address']['loc']; 
$vtc = $result['data']['address']['vtc']; 
$street = $result['data']['address']['street']; 
$house = $result['data']['address']['house']; 
$landmark = $result['data']['address']['landmark']; 
	
$addr_string = "$care_of, $house, $street, $landmark, $po, $vtc, $dist, $state, $country. $pincode";

$_SESSION['driver_aadhar_name'] = $full_name;
$_SESSION['driver_aadhar_img'] = $image;
$_SESSION['driver_aadhar_addr'] = $addr_string;
$_SESSION['driver_aadhar_dob'] = $dob;

echo "<script>
	$('#market_driver_addhar_holder_name').val('$full_name');
</script>";
/*
?>
<div class="form-group col-md-3">
	<img style="width:100%;height:100px" src="data:image/png;base64,<?php echo $image; ?>" />
</div>

<div class="form-group col-md-9">
	<table class="table table-bordered table-striped" style="color:#000;font-size:10px">
		<tr>
			<th>Full Name</th>
			<td><?php echo $full_name; ?></td>
		</tr>	
		<tr>
			<th>DOB</th>
			<td><?php echo $dob; ?></td>
		</tr>
		<tr>
			<th>Address</th>
			<td><?php echo $addr_string; ?></td>
		</tr>
	</table>
</div>
<?php
*/
?>
<script>
	$('#loadicon').fadeOut('slow');
	$('.aadhar_otp_div').hide();
	$('.aadhar_holder_name_div').show();
	$('#market_driver_aadhar_validate_otp_btn').attr('disabled',true);
	$('#add_driver_button').attr('disabled',false);
	$('#add_driver_button').show();
</script>
