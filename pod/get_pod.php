<?php
require_once('../connection.php');

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

if($lrno!='')
{
	$get_branch = Qry($conn,"SELECT DISTINCT frno,truck_no,branch FROM freight_form_lr WHERE lrno='$lrno' AND frno like '___F%'");
	
	if(!$get_branch){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_branch)>0)
	{
		echo "<option value=''>Select Voucher</option>";
		while($row=fetchArray($get_branch))
		{
			echo "<option value='$row[branch]_$row[frno]'>$row[frno] => $row[truck_no]($row[branch])</option>";
		}
		
		echo "<script>
			$('#button1').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	}
	else
	{
		echo "<option value=''>Select Voucher</option>";
		echo "<script>
			$('#button1').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
	}
}
?>