<?php
require_once '../connection.php';
$date2=date("Y-m-d"); 

if(isset($_POST['lrno_set'])){
	$lrno_set = escapeString($conn,$_POST['lrno_set']);
	$frno_set = escapeString($conn,$_POST['frno_set']);
}
else{
	$lrno_set = "";
	$frno_set = "";
}
?>
<html>

<?php
include ("../_header.php");
include ("../_loadicon.php");
include("../disable_right_click.php");
?>

<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}

.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}

::-webkit-scrollbar{
    width: 4px;
    height: 4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style> 
  
<script type="text/javascript">
function getpod(){
	
	if($("#lrno").val()!='')
	{
		$("#loadicon").show();	
		 jQuery.ajax({
			 url: "./get_pod.php",
			data: 'lrno=' + $("#lrno").val(),
			 type: "POST",
			 success: function(data) {
				$("#vou_no").html(data);
			},
			 error: function() {}
		});
	}
}
</script>

<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
  <div class="row">
		
		<div class="form-group col-md-3">
			<a href="../"><button style="margin-top:10px" class="btn btn-sm btn-primary">
			<span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
			<a href="./"><button style="margin-top:10px" class="btn btn-sm btn-danger">
			<span class="glyphicon glyphicon-refresh"></span> Refresh</button></a>
		</div>
		
		<div class="form-group col-md-6">
		 <center>
				<h3 style="letter-spacing:1px">Receive - POD (Proof of delivery)</h3>
		   </center>
		</div>	
</div>	
<br />		

<div class="row">
	
		<div class="form-group col-md-2">
			<label>Enter LR Number <font color="red"><sup>*</sup></font></label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="getpod();" type="text" id="lrno" name="lrno" 
			placeholder="Enter LR No." class="form-control" value="<?php echo $lrno_set; ?>" required />
		</div>	

		<?php
		if($frno_set=='')
		{
			?>
		<div class="form-group col-md-3">
			<label>Select Voucher <font color="red"><sup>*</sup></font></label>
			<select id="vou_no" name="vou_no" value="<?php echo $frno_set; ?>" class="form-control" required="required">
				<option value="">Select Voucher</option>
			</select>
		</div>	
		<?php
		}
		else
		{
		?>
		<div class="form-group col-md-2">
			<label>Voucher Number <font color="red"><sup>*</sup></font></label>
			<input type="text" id="vou_no" name="vou_no" class="form-control" value="<?php echo $frno_set; ?>" required />
		</div>	
		<?php
		}
		?>

	<div class="form-group col-md-2">
		<label>&nbsp;</label>
		<br>
		<button id="button1" disabled onclick="GetFm($('#vou_no').val())" type="button" class="btn btn-sm btn-danger" 
		style="letter-spacing:1px;">Get Details</button>
	</div>					 

</div>	
	
<br /> 

<input type="hidden" id="vou_no_db2">
	
<div id="pod_result" class="row"></div>
<br />
<br />
</div>

<script type="text/javascript">
function GetFm(vou_no){
	
	if(vou_no!='')
	{
		$('#vou_no').attr('disabled',true);
		$('#button1').attr('disabled',true);
		$('#lrno').attr('readonly',true);
		$('#lrno').attr('onblur','');
		
		var branch = vou_no.split("_")[0];
		var vou_no = vou_no.split("_")[1];
		
		$("#loadicon").show();	
			 jQuery.ajax({
				 url: "./get_fm_details_for_pod.php",
				data: 'vou_no=' + vou_no + '&branch=' + branch,
				 type: "POST",
				 success: function(data) {
					$("#pod_result").html(data);
				},
				 error: function() {}
			});
	}
	else
	{
		alert('Select Voucher First !');
		$('#button1').attr('disabled',false);
		$('#lrno').attr('readonly',false);
		$('#lrno').attr('onblur','getpod()');
	}
}

function ClaimSelection(elem,lr_id,pod_id)
{
	var lrno = $('#claim_branch_lrno_'+lr_id).val();
	var vou_no = $('#claim_branch_vou_no_'+lr_id).val();
	
	if(pod_id=='')
	{
		alert('Please receive pod first !!');
		$('#claim_branch_'+pod_id).val('');
	}
	else
	{
		if(elem=='YES')
		{
			$("#loadicon").show();	
				jQuery.ajax({
				url: "./load_claim_modal.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&pod_id=' + pod_id,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
			},
			 error: function() {}
			});
		}
		else if(elem=='NO')
		{
			$("#loadicon").show();	
				jQuery.ajax({
				url: "./update_claim_status.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&pod_id=' + pod_id,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
			},
			 error: function() {}
			});
		}
	}
}
</script>


<div id="func_result"></div>

<?php
if($lrno_set!='')
{
	echo "<script>
		$('#lrno').attr('readonly',true);
		getpod();
		GetFm('$frno_set');
		$('#button1').attr('disabled',true);
	</script>";
}
?>