<?php
require_once '../connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$pod_id = escapeString($conn,($_POST['pod_id']));

$chk_record = Qry($conn,"SELECT claim_branch FROM rcv_pod WHERE id='$pod_id'");
	
if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> 	
		alert('Error !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if(numRows($chk_record)==0)
{
	echo "<script> 	
		alert('Please upload pod first !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}

$row_pod = fetchArray($chk_record);

if($row_pod['claim_branch']!="0")
{
	echo "<script> 	 
		alert('Error : Claim status already updated !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}
	

$update_claim_status = Qry($conn,"UPDATE rcv_pod SET claim_branch='2' WHERE id='$pod_id'");
	
if(!$update_claim_status){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> 	
		alert('Error !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
	echo "<script>
		alert('Claim status updated !');
		$('#claim_branch_$pod_id').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>"; 
	exit();

?>