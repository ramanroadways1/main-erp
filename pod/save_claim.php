<?php
require_once("../connection.php");

$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$pod_id=escapeString($conn,strtoupper($_POST['pod_id']));
$vou_no=escapeString($conn,strtoupper($_POST['vou_no']));

$chk_record=Qry($conn,"SELECT id FROM claim_records WHERE pod_id='$pod_id'");
if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !!');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_record)>0)
{
	echo "<script>
		alert('Duplicate record found.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$chk_record_claim_txns=Qry($conn,"SELECT id FROM claim_book_trans WHERE pod_id='$pod_id' AND main_entry='1'");
if(!$chk_record_claim_txns){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !!');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_record_claim_txns)>0)
{
	echo "<script>
		alert('Claim Txns. --> Duplicate record found.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$timestamp=date("Y-m-d H:i:s");
$date=date("Y-m-d");

if(empty($_POST['claim_amount']))
{
	echo "<script type='text/javascript'>
		alert('No record added.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$total_count=count($_POST['claim_amount']);

if($total_count<=0)
{
	echo "<script type='text/javascript'>
		alert('No record added.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}
	
if($_POST['total_claim_amount_db']!=array_sum($_POST['claim_amount']))
{
	echo "<script type='text/javascript'>
		alert('Claim amount not verified.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$damage_amount = 0;
$shortage_amount = 0;

for($i=0; $i<$total_count;$i++) 
{
	if($_POST['claim_type'][$i]=="Damage")
	{
		$damage_amount = $_POST['claim_amount'][$i];
	}
	
	if($_POST['claim_type'][$i]=="Shortage")
	{
		$shortage_amount = $_POST['claim_amount'][$i];
	}
}

if(($damage_amount+$shortage_amount)!=$_POST['total_claim_amount_db'])
{
	echo "<script type='text/javascript'>
		alert('Total amount and damage+shortage amount not verified.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}

$total_claim = $_POST['total_claim_amount_db'];

StartCommit($conn);
$flag = true;	

$insert_main_record = Qry($conn,"INSERT INTO claim_records(vou_no,lrno,pod_id,amount,damage_amount,shortage_amount,branch,branch_user,
timestamp) VALUES ('$vou_no','$lrno','$pod_id','$total_claim','$damage_amount','$shortage_amount','$branch','$branch_sub_user',
'$timestamp')");

if(!$insert_main_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$claim_id = getInsertID($conn);

$insert_claim_txn = Qry($conn,"INSERT INTO claim_book_trans(pod_id,lrno,vehicle_type,vou_no,branch,branch_user,date,credit,
balance,branch_amount,narration,main_entry,timestamp) VALUES ('$pod_id','$lrno','MARKET','$vou_no','$branch',
'$branch_sub_user','$date','$total_claim','$total_claim','$total_claim','Claim on POD','1','$timestamp')");

if(!$insert_claim_txn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

for($i=0; $i<$total_count;$i++) 
{
    $insert_claim = Qry($conn,"INSERT INTO claim_records_desc(claim_id,pod_id,amount,claim_type,claim_unit,unit_value,narration,
	timestamp) VALUES ('".$claim_id."','$pod_id','".$_POST['claim_amount'][$i]."','".$_POST['claim_type'][$i]."',
	'".$_POST['claim_unit'][$i]."','".$_POST['unit_value'][$i]."','".$_POST['narration'][$i]."','".$timestamp."')");
		  
	if(!$insert_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_claim_status = Qry($conn,"UPDATE rcv_pod SET claim_branch='1' WHERE id='$pod_id'");

if(!$update_claim_status){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Claim Record Successfully Updated !');
			$('#claim_branch_$pod_id').attr('disabled',true);
			$('#loadicon').hide();
			document.getElementById('ClaimAmountForm').reset();
			$('#btn_claim_submit').attr('disabled',false);
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#btn_claim_submit').attr('disabled',false);
	</script>";
	exit();
}
?>