<?php 
require_once('../connection.php');

$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$timestamp = date("Y-m-d H:i:s");

$fetch_lr = Qry($conn,"SELECT f.id as fm_id,f.frno,f.branch as fm_branch,f.lrno,f.fstation,f.tstation,f.truck_no,f.crossing,
f2.newdate,f2.actualf as freight_amount,f2.totaladv as freight_advance,l.branch,l.date,l.consignor,l.con1_id,l.pod_rcv_date,l.id as lr_id,
l.item_id,l.billparty as lr_bill_party_id,l.billingbranch,p.pod_copy,p.claim_branch,p.id as pod_id
FROM freight_form_lr AS f 
LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
LEFT OUTER JOIN freight_form AS f2 ON f2.frno=f.frno 
LEFT OUTER JOIN rcv_pod AS p ON p.frno=f.frno AND p.lr_id=l.id 
WHERE f.frno='$vou_no' GROUP BY f.lrno ORDER BY f.id ASC");

if(!$fetch_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo '<div class="form-group col-md-12 table-responsive" style="overflow-x:auto">';

if(numRows($fetch_lr)==0)
{
	echo "<center><span style='color:red'>Unable to fetch LR details !</span></font>
	</div>
	<script>$('#loadicon').hide();</script>";
	exit();
}
?>
<div class="alert alert-danger" role="alert">
Please upload the POD submitted by your branch, if it is submitted by any other branch then the same branch will upload it. 
The wrong copy will be removed.
<br>
कृपया आपके ब्रांच द्वारा जमा की गयी पहोच ही अपलोड करे अगर पहोच किसी अन्य ब्रांच द्वारा जमा की गयी है फिर वही ब्रांच उसे अपलोड करेंगे. गलत प्रतिलिपि को हटा दिया जायेगा.
</div>

<table class="table table-bordered table-striped" border="0" style="border:0;font-size:12px">								   
<tr style="background:#0AF;color:#FFF;font-size:14px">
	<td colspan="8">Vehicle Number : <span id="veh_no1"></span>, Vou_No : <?php echo $vou_no; ?></td>
</tr>		
	<tr>
		<th>#</th>
		<th>Branch</th>
		<th>LR_No & Date</th>
		<th>Consignor</th>
		<th>Locations</th>
		<th>Pod Rcv_Charges</th>
		<th>POD_Copy</th>
		<th>Claim</th>
	</tr>
<?php 
// $check_record = Qry($conn,"SELECT p.id,p.lrno,p.fm_date,p.consignor_id,p.pod_branch,p.pod_rcv_charges,p.pod_copy,
// c.name as consignor FROM rcv_pod_pending as p 
// LEFT OUTER JOIN consignor as c on c.id=p.consignor_id
// WHERE p.frno='$vou_no'");

// if(!$check_record){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// if(numRows($check_record)>0)
// {
	// $row = fetchArray($fetch_lr);
	// $row2 = fetchArray($check_record);
	// $LR_date = date('d/m/y', strtotime($row2['fm_date']));
	
// if($row2['pod_copy']!="")
// {
	// $pod_count=1;
	// $pod_copy=array();
	// foreach(explode(",",$row2['pod_copy']) as $pod_copy1)
	// {
		// $pod_copy[]="<a href='../$pod_copy1' target='_blank'>Copy: $pod_count</a>";
		// $pod_count++;	
	// }
	// $pod_copy = implode("  ,  ",$pod_copy);
// }
// else
// {
	// $pod_copy="";
// }	
	
	// echo "<tr>
		// <td>1</td>
		// <td>$row2[pod_branch]</td>
		// <td>$row2[lrno]</td>
		// <td>$LR_date</td>
		// <td>$row2[consignor]</td>
		// <td></td>
		// <td>$row2[pod_rcv_charges]</td>
		// <td>$pod_copy</td>
	// </tr>";
	// $veh_no = $row['truck_no'];
// }
// else
// {
$sn=1;
while($row = fetchArray($fetch_lr))
{
$con1_id_DB = $row['con1_id'];
$veh_no = $row['truck_no'];
$fm_branch = $row['fm_branch'];
$LR_date = date('d/m/y', strtotime($row['date']));

if($row['pod_copy']!="")
{
	$pod_count=1;
	$pod_copy=array();
	foreach(explode(",",$row['pod_copy']) as $pod_copy1)
	{
		$pod_copy[]="<a href='../$pod_copy1' target='_blank'>Copy: $pod_count</a>";
		$pod_count++;	
	}
	$pod_copy = implode("  ,  ",$pod_copy);
}
else
{
	$pod_copy="";
}

	echo "<tr>
		<td>$sn</td>
		<td>$row[branch]</td>
		<td>$row[lrno]<br>$LR_date</td>
		<td>$row[consignor]</td>
		<td>From: $row[fstation]<br>To: $row[tstation]</td>
		<td>";
		
		if(($row['fm_branch']=='KORBA' || $row['fm_branch']=='RAIPUR') AND $branch!=$row['fm_branch']){
			$pod_rcv_charges=300;
		}
		else{
			$pod_rcv_charges=0;
		}
		
		echo $pod_rcv_charges;
		echo "</td>
		<td id='pod_copy_td$row[lr_id]'>";
		if($pod_copy=='')
		{
		echo '<script type="text/javascript">
			$(document).ready(function (e) {
				$("#PodFormUpload'.$row["lr_id"].'").on("submit",(function(e) {
				e.preventDefault();
				$("#loadicon").show();
				$("#rcv_button'.$row["lr_id"].'").attr("disabled", true);
				$.ajax({
						url: "./upload_pod.php",
						type: "POST",
						data:  new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function(data){
							$("#func_result").html(data);
						},
						error: function() 
						{
						} 	        
				   });
				}));
			});
		</script>
		<form id="PodFormUpload'.$row["lr_id"].'"
		<div class="form-inline">';
		if($row['crossing']!='YES')
		{
			echo "
			<label>Delivery Date <font color='red'><sup>*</sup></font></label>
			<br>
			<input min='".$row["date"]."' max='".date('Y-m-d', strtotime('+15 days', strtotime($row["date"])))."' class='del_date_Ip form-control' id='del_date$row[lr_id]' required='required' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' type='date' style='width:140px;font-size:9px;height:28px;' name='del_date'>";
		}
		else
		{
			echo "
			<label>Upload Crossing Slip <font color='red'><sup>*</sup></font></label>
			<br>
			<input type='hidden' class='form-control' name='del_date'>";
		}
		
		if($row['con1_id']=='56' || $row['con1_id']=='158')
		{
			echo "<input id='pod_copy_ip$row[lr_id]' required='required' type='file' class='form-control' 
			accept='application/pdf' style='width:140px;font-size:9px;height:28px;' name='upload_file[]'>";
		}
		else
		{
			echo "<input id='pod_copy_ip$row[lr_id]' required='required' type='file' multiple='multiple' class='form-control' 
			accept='image/jpeg,image/gif,image/png,application/pdf,image/x-eps' style='width:140px;font-size:9px;height:28px;' name='upload_file[]'>";
		}
		
		echo "
		<input type='hidden' name='lr_id' value='$row[lr_id]'>			
		<input type='hidden' name='item_id' value='$row[item_id]'>			
		<input type='hidden' name='billingbranch' value='$row[billingbranch]'>			
		<input type='hidden' name='lr_bill_party_id' value='$row[lr_bill_party_id]'>			
		<input type='hidden' name='vou_no' value='$vou_no'>			
		<input type='hidden' name='lrno' value='$row[lrno]'>			
		<input type='hidden' name='truck_no' value='$row[truck_no]'>			
		<input type='hidden' name='lr_branch' value='$row[branch]'>			
		<input type='hidden' name='lr_date' value='$row[date]'>			
		<input type='hidden' name='consignor' value='$row[consignor]'>			
		<input type='hidden' name='con1_id' value='$row[con1_id]'>			
		<input type='hidden' name='pod_rcv_charges' value='$pod_rcv_charges'>			
		<input type='hidden' name='crossing' value='$row[crossing]'>			
		<input type='hidden' name='fm_id' value='$row[fm_id]'>			
		<input type='hidden' name='freight_amount' value='$row[freight_amount]'>			
		<input type='hidden' name='freight_advance' value='$row[freight_advance]'>			
		<input type='hidden' name='newdate' value='$row[newdate]'>			
		<input type='hidden' name='fm_branch' value='$fm_branch'>
		
		<button type='submit' id='rcv_button$row[lr_id]' class='btn btn-primary'>
				<span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span>
		</button>
		</div>
		</form>"; 
		}
		else		
		{
			echo $pod_copy;
		}
		echo "</td>";
		
		// <button type='button' class='btn btn-primary btn-sm' id='claim_branch_btn'>
				// View
			// </button>
			
		if($row['claim_branch']=='' || $row['claim_branch']=="0")
		{
		echo " 
		<input type='hidden' id='claim_branch_lrno_$row[lr_id]' value='$row[lrno]'>
		<input type='hidden' id='claim_branch_vou_no_$row[lr_id]' value='$vou_no'>
		<input type='hidden' id='is_pod_rcvd_$row[lr_id]'>
		
		<td>
		<div class='form-inline'>
			<label>Claim in this LR? <font color='red'><sup>*</sup></font></label>
			<br />
			<select style='width:130px !important;font-size:12px;height:30px;' onchange=ClaimSelection(this.value,'$row[lr_id]','$row[pod_id]') class='form-control' id='claim_branch_$row[pod_id]'>
				<option value=''>--select--</option>
				<option value='YES'>YES</option>
				<option value='NO'>NO</option>
			</select>
		</div>
		</td>";
		}
		else
		{
			if($row['claim_branch']=="1"){
				$claim_branch="YES";
			}else{
				$claim_branch="NO";
			}
			echo "<td>$claim_branch</td>";
		}
		
	echo "</tr>";
$sn++;	
}
echo "</table>";

// if($fm_branch=='KORBA' || $fm_branch=='RAIPUR' || $con1_id_DB=='303')
// {
	// $chk_charges = Qry($conn,"SELECT id FROM gps WHERE frno='$vou_no'");
	// if(!$chk_charges){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./");
		// exit();
	// }
	
	// if(numRows($chk_charges)==0)
	// {
		// $charges_div="1";
	// }
	// else
	// {
		// $charges_div="0";
	// }
// }
// else
// {
	// $charges_div="0";
// }

if($con1_id_DB=='303')
{
	$gps_install_type='Consignor';
	
	$chk_charges = Qry($conn,"SELECT id FROM gps WHERE frno='$vou_no'");
	if(!$chk_charges){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_charges)==0)
	{
		$charges_div="1";
	}
	else
	{
		$charges_div="0";
	}
}
else
{
	$gps_install_type='Inventory';
	
	$chk_gps_installed = Qry($conn,"SELECT fix_attach,is_repeated FROM gps_device_inventory WHERE active_vou='$vou_no'");
	
	if(!$chk_gps_installed){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($chk_gps_installed)>0)
	{
		if(numRows($chk_gps_installed)>1)
		{
			echo "<script>
				alert('Warning : Multiple GPS devices active on this vehicle.');
				window.close();
			</script>";
			exit();
		}
		
		$row_gps_check = fetchArray($chk_gps_installed);

		if($row_gps_check['fix_attach']=='Attach' AND $row_gps_check['is_repeated']=="0")
		{
			$chk_charges = Qry($conn,"SELECT id FROM gps WHERE frno='$vou_no'");
			if(!$chk_charges){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			if(numRows($chk_charges)==0){
				$charges_div="1";
			}
			else{
				$charges_div="0";
			}
		}
		else
		{
			$charges_div="0";
		}
	}
	else
	{
		$charges_div="0";
	}
}

if($charges_div=="1")
{
	
	if($gps_install_type=='Consignor')
	{
	?>
<script>
function GetGPSDevice()
{
	var selection = $('#GPSDeviceSelection').val();
	
	if(selection=='')
	{
		alert('Select option first !');
	}
	else
	{
		$('#GPSDeviceButton').attr('disabled',true);
		$("#loadicon").show();	
			jQuery.ajax({
			url: "./update_device_status_consignor.php",
			data: 'selection=' + selection + '&vou_no=' + '<?php echo $vou_no; ?>',
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function MyFunc1(elem)
{
		if(elem=='0'){
			$('#gps_alert').show();
		}
		else{
			$('#gps_alert').hide();
		}
}
</script>

<div class="form-group col-md-12">
	<div class="row">
	
		<div class="form-inline col-md-8">
			<label>GPS Device Received ?</label>
			<br />
			<select name="gps" style="width:200px" id="GPSDeviceSelection" onchange="MyFunc1(this.value)" class="form-control" required>
				<option value="">--option--</option>
				<option value="1">YES</option>
				<option value="0">NO</option>
			</select>
			<button type="button" id="GPSDeviceButton" onclick="GetGPSDevice()" class="btn btn-primary">
				<span style="font-size:20px;" class="glyphicon glyphicon-share-alt"></span>
			</button>
			
			<div style="color:red;display:none;" id="gps_alert">
				<br />
				<b>Rs : 3000/- will be deducted from balance (GPS Device charge).</b>
			</div>
		
		</div>
	</div>	
</div>
	<?php	
	}	 
	else
	{
	?>
	<script>
	function MyFunc1(elem)
	{
		$('#device_imei').val('');
		
		if(elem=='0')
		{
			$('#gps_alert').show();
			$('#device_imei').hide();
		}
		else
		{
			$('#gps_alert').hide();
			$('#device_imei').show();
		}
	}

	function GetGPSDevice()
	{
		var selection = $('#GPSDeviceSelection').val();
		
		if(selection=='')
		{
			alert('Select option first !');
		}
		else
		{
			var tno = $('#tno_gps_selection').val();
			var imei = $('#device_imei').val();
			
			if(tno=='')
			{
				alert('Error : vehicle number not found !');
			}
			else
			{
				$('#GPSDeviceButton').attr('disabled',true);
				$("#loadicon").show();	
					jQuery.ajax({
					url: "./update_device_status.php",
					data: 'selection=' + selection + '&vou_no=' + '<?php echo $vou_no; ?>' + '&tno=' + '<?php echo $veh_no; ?>' + '&tno2=' + tno + '&imei=' + imei,
					type: "POST",
					success: function(data) {
						$("#func_result").html(data);
					},
					error: function() {}
				});
			}
		}
	}
	</script>
	
	<div class="form-group col-md-12">
	<div class="row">
	
		<div class="form-inline col-md-8">
			<label>GPS Device Received ?</label>
			<br />
			<select name="gps" style="width:200px" id="GPSDeviceSelection" onchange="MyFunc1(this.value)" class="form-control" required>
				<option value="">--option--</option>
				<option value="1">YES</option>
				<option value="0">NO</option>
			</select>
			<input style="text-transform:lowercase;display:none" placeholder="Enter IMEI no." type="number" id="device_imei" class="form-control">
			<input type="hidden" value="<?php echo $veh_no; ?>" id="tno_gps_selection">
			<button type="button" id="GPSDeviceButton" onclick="GetGPSDevice()" class="btn btn-primary">
				<span style="font-size:20px;" class="glyphicon glyphicon-share-alt"></span>
			</button>
			
			<div style="color:red;display:none;" id="gps_alert">
				<br />
				<b>Rs : 1880/- will be deducted from balance (GPS Device charge).</b>
			</div>
		
		</div>
	</div>	
	</div>
	<?php	
	}
}

$GetDeliveryDate = Qry($conn,"SELECT del_date FROM rcv_pod WHERE frno='$vou_no' AND del_date!=0 ORDER BY id DESC LIMIT 1");
if(!$GetDeliveryDate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($GetDeliveryDate)>0)
{
	$rowDelDate = fetchArray($GetDeliveryDate);
	echo "<script>
		$('.del_date_Ip').val('$rowDelDate[del_date]');
	</script>";
}

// echo $rowDelDate['del_date'];
// }	
echo "
</div>
<script>
$('#veh_no1').html('$veh_no');
$('#loadicon').hide();
</script>";
?>