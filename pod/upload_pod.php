<?php
require_once('../connection.php');

$id = escapeString($conn,strtoupper($_POST['lr_id']));
$del_date = escapeString($conn,strtoupper($_POST['del_date']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$lr_branch = escapeString($conn,strtoupper($_POST['lr_branch']));
$lr_date = escapeString($conn,strtoupper($_POST['lr_date']));
$consignor = escapeString($conn,strtoupper($_POST['consignor']));
$con1_id = escapeString($conn,strtoupper($_POST['con1_id']));
$pod_rcv_charges = escapeString($conn,strtoupper($_POST['pod_rcv_charges']));
$crossing = escapeString($conn,strtoupper($_POST['crossing']));
$fm_id = escapeString($conn,strtoupper($_POST['fm_id']));
$fm_branch = escapeString($conn,strtoupper($_POST['fm_branch']));
$freight_amount = escapeString($conn,strtoupper($_POST['freight_amount']));
$freight_advance = escapeString($conn,strtoupper($_POST['freight_advance']));
$item_id = escapeString($conn,strtoupper($_POST['item_id']));
$newdate = escapeString($conn,strtoupper($_POST['newdate']));
$lr_bill_branch = escapeString($conn,($_POST['billingbranch']));
$lr_bill_party_id = escapeString($conn,($_POST['lr_bill_party_id']));

$timestamp = date("Y-m-d H:i:s");

if(empty($item_id))
{ 
	echo "<script> 
		alert('Unable to fetch item !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

// $chk_gps_device = Qry($conn,"SELECT id FROM _gps_record WHERE tno='$truck_no'");
// if(!$chk_gps_device){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// if(numRows($chk_gps_device)>0)
// {
	// echo "<script>
			// alert('Rcv POD function of gps installed vehicles temporary on hold.');
			// window.close();
		// </script>";
	// exit();
// }

if(($fm_branch=='KORBA' || $fm_branch=='RAIPUR') AND $branch!=$fm_branch)
{
	$pod_rcv_charges_chk=300;
}
else
{
	$pod_rcv_charges_chk=0;
}
	
if($pod_rcv_charges_chk!=$pod_rcv_charges)
{
	echo "<script> 
		alert('POD receive charges is not valid !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}
	
$chk_pod = Qry($conn,"SELECT id FROM rcv_pod WHERE frno='$vou_no' AND lrno='$lrno'");

if(!$chk_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_pod)>0)
{
	echo "<script>
		alert('POD already received for LR number : $lrno ($vou_no) !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if($con1_id=='' || $con1_id==0)
{
	echo "<script>
		alert('Consignor not found !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if(count($_FILES['upload_file']['name'])==0) 
{
	echo "<script>
		alert('No attachment found please try again !!');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

// billing ofc and billing party id function ///

$bill_func = GetBillPartyBranch($conn,$branch,$con1_id,$lr_bill_branch,$lr_bill_party_id);

$billing_branch = $bill_func[0];
$billing_party_id = $bill_func[1];
$self_var = $bill_func[2];

// billing ofc and billing party id function ///

if(count($_FILES['upload_file']['name'])>0)
{
	if($con1_id=='56' || $con1_id=='158')
	{
		$valid_types = array("application/pdf");
		$valid_types_text = "application/pdf";
	}
	else
	{
		$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
		$valid_types_text = "image/jpg, image/jpeg, image/bmp, image/gif, image/png, application/pdf";
	}
	
	foreach($_FILES['upload_file']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Uploaded file format is not allowed. Supported formats are : $valid_types_text !');
				$('#loadicon').hide();
				$('#rcv_button$id').attr('disabled', false);
			</script>";
			exit();
		}
    }
}

	for($i=0; $i<count($_FILES['upload_file']['name']);$i++) 
	{
		$fix_Name = date('dmYHis').mt_rand().$i;
		$sourcePath = $_FILES['upload_file']['tmp_name'][$i];
		$shortname = "pod_copy/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		$targetPath = "../pod_copy/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		if($_FILES['upload_file']['type'][$i]!='application/pdf')
		{		
			ImageUpload(1000,1000,$_FILES['upload_file']['tmp_name'][$i]);
		}
		
	   if(move_uploaded_file($sourcePath, $targetPath))
	   {
			$files[] = $shortname;
	   }
	   else
	   {
			echo "<script> 
				alert('Upload Failed !');
				$('#loadicon').hide();
				$('#rcv_button$id').attr('disabled', false);
			</script>";
			exit();		
	   }
	}

	$file_name=implode(',',$files);
	
StartCommit($conn);
$flag = true;	

if($crossing=='YES')
{
	$cross_set = "1";
}
else
{
	$cross_set = "0";
}

$insert_pod = Qry($conn,"INSERT INTO rcv_pod(tno,frno,veh_type,lrno,lr_id,consignor_id,branch,branch_user,fm_date,fm_date2,fm_amount,fm_adv,pod_branch,
pod_copy,crossing,pod_date,del_date,pod_rcv_charges,billing_party,billing_ofc,timestamp,self,exdate,fromstation,tostation) 
VALUES ('$truck_no','$vou_no','MARKET','$lrno','$id','$con1_id','$branch','$branch_sub_user','$lr_date','$newdate','$freight_amount','$freight_advance',
'$fm_branch','$file_name','$cross_set','".date('Y-m-d')."','$del_date','$pod_rcv_charges','$billing_party_id','$billing_branch',
'$timestamp','$self_var','".date('Y-m-d')."','$branch','$billing_branch')");

if(!$insert_pod){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($item_id=='101')
{
	$where_condition = "frno='$vou_no' AND lrno='$lrno'";
	$where_condition2 = "vou_no='$vou_no' AND lrno='$lrno'";
}
else
{
	$where_condition = "id='$fm_id'";
	$where_condition2 = "vou_id='$fm_id'";
}

if($crossing=='YES')
{
	$update_pod_date = Qry($conn,"UPDATE freight_form_lr SET pod_rcv_date='".date('Y-m-d')."' WHERE $where_condition");
}
else
{
	$update_pod_date = Qry($conn,"UPDATE lr_sample SET pod_rcv_date='".date('Y-m-d')."' WHERE id='$id'");
}

if(!$update_pod_date){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='".date('Y-m-d')."' WHERE $where_condition");
if(!$update_pod_date_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_from_pending_pod_list = Qry($conn,"DELETE FROM _pending_lr_list WHERE $where_condition2");
if(!$delete_from_pending_pod_list){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updatePodCount = Qry($conn,"UPDATE _pending_lr SET lr_pod_pending=lr_pod_pending-1 WHERE branch='$fm_branch'");
if(!$updatePodCount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	$fm_var1 = $fm_branch."_".$vou_no;
	
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('POD Copy Received. LR Number : $lrno !');
		$('#rcv_button$id').attr('disabled',true);	
		$('#pod_copy_ip$id').attr('disabled',true);	
		$('#del_date$id').attr('disabled',true);	
		$('#is_pod_rcvd_$id').val('YES');	
		$('.del_date_Ip').val('$del_date');
		$('#pod_copy_td$id').html('POD Received.');	
		$('#button1').attr('disabled',false);
		GetFm('$fm_var1');	
		$('#loadicon').hide();	
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>
