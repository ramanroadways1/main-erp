<?php
require_once("../connection.php");

$lrno = escapeString($conn,($_POST['lrno']));
$vou_no = escapeString($conn,($_POST['vou_no']));
$pod_id = escapeString($conn,($_POST['pod_id']));

$chk_record = Qry($conn,"SELECT claim_branch,lrno,frno as vou_no FROM rcv_pod WHERE id='$pod_id'");
	
if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> 	
		alert('Error !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if(numRows($chk_record)==0)
{
	echo "<script> 	
		alert('Please upload pod first !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}

$row_pod = fetchArray($chk_record);

if($row_pod['claim_branch']!="0")
{
	echo "<script> 	
		alert('Error : Claim status already updated !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}

if($row_pod['lrno']!=$lrno)
{
	echo "<script> 	
		alert('Error : Lr number not verified !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}

if($row_pod['vou_no']!=$vou_no)
{
	echo "<script> 	
		alert('Error : Voucher number not verified !');
		$('#claim_branch_$pod_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>
<style>
#label_modal{font-size:12px;}
</style>

<form id="ClaimAmountForm" action="#" method="POST">

<div id="ClaimAmountModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Claim : <?php echo $vou_no." => ".$lrno; ?>, Total Claim Amount: <span id="total_claim_amount">0</span>
      </div>
      <div class="modal-body">
        <div class="row">
		
<script type="text/javascript">
var room = 1;
function education_fields()
{
	room++;
	var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+room);
	var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="col-md-2"><div class="form-group"> <label id="label_modal">Claim type <font color="red">*</font></label>'+
	'<select style="font-size:13px" class="form-control" name="claim_type[]" required="required"><option value="">--Select Type--</option>'+
	'<option value="Damage">Damage (डैमेज)</option><option value="Shortage">Shortage (शोर्टेज)</option></select></div></div>'+
	'<div class="col-md-2"><div class="form-group"> <label id="label_modal">Claim amount <font color="red">*</font></label>'+
	'<input type="number" min="0" name="claim_amount[]" oninput="FuncCalc()" class="form-control claim_amount_class" required></div></div>'+
	'<div class="col-md-3"><div class="form-group"><label id="label_modal">Unit (किलो/बैग/नंग)<font color="red">*</font></label>'+
	'<select style="font-size:13px" onchange=UnitSel(this.value,"'+room+'") class="form-control" name="claim_unit[]" required="required">'+
	'<option value="">--Select Type--</option><option value="Kg">Kg (किलो)</option><option value="Bag">Bag (बैग)</option>'+
	'<option value="Nos">Nos (नंग)</option></select></div></div>'+
	// '<script>function UnitSel_'+room+'(elem){ if(elem=="Kg"){ $("#unit_html'+room+'").html("(in Kg)"); }'+
	// 'else if(elem=="Bag"){ $("#unit_html'+room+'").html("(No of Bag)"); }else if(elem=="Nos")'+
	// '{ $("#unit_html'+room+'").html("(No of Nos)"); } else{ $("#unit_html'+room+'").html(""); }}
	'<div class="col-md-2"><div class="form-group"><label id="label_modal">Unit value <font color="red">* '+
	'<sup><span id="unit_html'+room+'"></span></sup></font></label>'+
	'<input type="text" style="font-size:12px" placeholder="No. of bag/nos etc.." oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,\'\')"'+ 
	'class="form-control" name="unit_value[]" required /></div></div><div class="col-md-2"><div class="form-group">'+
	'<label id="label_modal">Narration <font color="red">*</font></label><textarea '+
	'oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,\'\')" style="font-size:12px" class="form-control" name="narration[]" required></textarea>'+
	'</div></div><div class="col-md-1"><div class="form-group"><label>&nbsp;</label><br />'+
	'<button class="btn btn-danger btn-sm pull-right" type="button" onclick="remove_education_fields('+ room +');FuncCalc();">'+
	'<span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div></div>';
	// '<div class="col-md-12"><hr></div>'; 
    
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }
</script>

	<div class="col-md-2">
		<div class="form-group">
			<label id="label_modal">Claim type <font color="red">*</font></label>
			<select style="font-size:13px" class="form-control" name="claim_type[]" required="required">
				<option value="">--Select Type--</option>
				<option value="Damage">Damage (डैमेज)</option>
				<option value="Shortage">Shortage (शोर्टेज)</option>
			</select>
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			<label id="label_modal">Claim amount <font color="red">*</font></label>
			<input type="number" min="0" oninput="FuncCalc()" name="claim_amount[]" class="form-control claim_amount_class" required>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="form-group">
			<label id="label_modal">Unit (किलो/बैग/नंग)<font color="red">*</font></label>
			<select style="font-size:13px" onchange="UnitSel(this.value,'0')" class="form-control" name="claim_unit[]" required="required">
				<option value="">--Select Type--</option>
				<option value="Kg">Kg (किलो)</option>
				<option value="Bag">Bag (बैग)</option>
				<option value="Nos">Nos (नंग)</option>
			</select>
		</div>
	</div>
		
<script>		
function UnitSel(elem,id1)
{
	if(elem=='Kg')
	{
		$('#unit_html'+id1).html('(in Kg)');
	}
	else if(elem=='Bag')
	{
		$('#unit_html'+id1).html('(No of Bags)');
	}
	else if(elem=='Nos')
	{
		$('#unit_html'+id1).html('(No of Nos)');
	}
	else
	{
		$('#unit_html'+id1).html('');
	}
}
</script>		
		
<script>
function FuncCalc()
{
	var sum = 0; 
	$(".claim_amount_class").each(function(){
		sum += +$(this).val();
	});
	$("#total_claim_amount").html(sum);
	$("#total_claim_amount_db").val(sum);
}
</script>
		
	<div class="col-md-2">
		<div class="form-group">	
			<label id="label_modal">Unit value/qty <font color="red">* <sup><span id="unit_html0"></span></sup></font></label>
			<input style="font-size:12px" type="text" placeholder="No. of bag/nos etc.." oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" class="form-control" name="unit_value[]" required />
		</div>
	</div>
	
	<div class="col-md-2">
		<div class="form-group">
			<label id="label_modal">Narration <font color="red">*</font></label>
			<textarea style="font-size:12px" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" class="form-control" name="narration[]" required></textarea>
		</div>
	</div>

	<div class="col-md-1">
		<div class="form-group">
			<label>&nbsp;</label>
			<br />
			<button class="btn btn-success  btn-sm pull-right" type="button" id="add_button" onclick="education_fields();FuncCalc();"> 
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			</button>	
		</div>
	</div>
	
	
	<div id="education_fields"></div>
				
		</div>
      </div>
	
	<input type="hidden" name="total_claim_amount_db" id="total_claim_amount_db">
	<input type="hidden" name="lrno" value="<?php echo $lrno; ?>">
	<input type="hidden" name="pod_id" value="<?php echo $pod_id; ?>">
	<input type="hidden" name="vou_no" value="<?php echo $vou_no; ?>">
	
	  <div id="result_modal_claim"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="btn_claim_submit" class="btn btn-primary">Submit</button>
        <button type="button" onclick="$('#claim_branch_<?php echo $pod_id; ?>').val('')" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

<button style="display:none" data-toggle="modal" data-target="#ClaimAmountModal" id="claim_modal_btn1" type="button"></button>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#ClaimAmountForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_claim_submit").attr("disabled", true);
	$.ajax({
        	url: "./save_claim.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_modal_claim").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

$('#claim_modal_btn1')[0].click();
$('#loadicon').hide();
</script>