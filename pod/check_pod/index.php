<?php
require_once '../../connection.php';
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
	<center><img style="margin-top:150px" src="../../load.gif" /></center>
</div>

<style>
  .form-control
  {
	  border:1px solid #000;
	  background:#FFF;
  }
  label{
	  font-size:13px;
  }
</style>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style> 
  
</head>

<script type="text/javascript">
function getpod(type){
	
if(type=='LR')
{
	var lrno = $('#lrno').val();
	
	if(lrno=="")
	{
		alert('Enter LR number first !');
	}
	else
	{
		$("#loadicon").show();	
	   jQuery.ajax({
	   url: "./fetch_pod.php",
		data: 'lrno=' + lrno + '&type=' + type,
	   type: "POST",
	   success: function(data) {
		$("#pod_result").html(data);
	   },
	   error: function() {}
	   });
	}
}
else
{
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	
	if(from_date=="" || to_date=="")
	{
		alert('Select from and to date first !');
	}
	else
	{
		$("#loadicon").show();	
	   jQuery.ajax({
	   url: "./fetch_pod.php",
		data: 'from_date=' + from_date + '&to_date=' + to_date + '&type=' + type,
	   type: "POST",
	   success: function(data) {
		$("#pod_result").html(data);
	   },
	   error: function() {}
	   });
	}
}
}

function getpodExcel()
{
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	
	if(from_date=="" || to_date=="")
	{
		alert('Select from and to date first !');
	}
	else
	{
		$('#from_date1').val(from_date);
		$('#to_date1').val(to_date);
		$('#ExcelForm').submit();
	}
}
</script>

<form action="pod_excel.php" id="ExcelForm" method="POST" target="_blank">
	<input type="hidden" name="from_date" id="from_date1">
	<input type="hidden" name="to_date" id="to_date1">
</form>

<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
  <div class="row">
	<div class="col-md-3">
			<a href="../../"><button style="margin-top:10px" class="btn btn-danger">Dashboard</button></a>										
	</div>
	<div class="col-md-6">
		 <center>
			<h3 style="color:#000;letter-spacing:0px">Check POD Status</h3>
		 </center>
	</div>	
	</div>	

<br /> 
<br />

<div class="row">
	
	<div class="form-group col-md-12">
	
	<div class="row">
	
	<div class="form-group col-md-4 col-md-offset-4" style="">
	
	<div class="col-md-12">
		<h5 style="background:gray;padding:8px;color:#fff">-- LR Number Wise --</h5>
	</div>
	
	<div class="form-group col-md-10">
		<label>LR Number </label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" id="lrno" name="lrno" placeholder="Enter LR No." class="form-control" required />
	</div>	
	
	<div class="form-group col-md-11">
		<button onclick="getpod('LR')" id="button_LR" type="button" class="btn btn-sm btn-success" style="letter-spacing:1px;">View</button>
	</div>	
	
	</div>	
	<!--
	<div class="form-group col-md-4">
	
	<div class="col-md-12">
		<h5 style="background:gray;padding:8px;color:#fff">-- LR Date Wise --</h5>
	</div>
	
	<div class="form-group col-md-6">
		<label>From Date </label>
		<input pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d");  ?>" type="date" id="from_date" name="from_date" class="form-control" required />
	</div>	
	
	<div class="form-group col-md-6">
		<label>To Date </label>
		<input pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d");  ?>" type="date" id="to_date" name="to_date" class="form-control" required />
	</div>	
	
	<!--
	<div class="form-group col-md-4">
		<label>Vehicle Type </label>
		<select class="form-control" id="vehicle_type_date">
			<option value=""></option>
			<option value="MARKET">MARKET Vehicle</option>
			<option value="OWN">OWN Vehicle</option>
		</select>
	</div>	

	<div class="form-group col-md-12">
		<button onclick="getpod('DATE')" id="button_DATE" type="button" class="btn btn-sm btn-success" style="letter-spacing:1px;">View</button>
		<button onclick="getpodExcel('DATE')" type="button" class="btn btn-sm btn-primary" style="letter-spacing:1px;">Excel download</button>
	</div>	
	
	</div>
	
<form action="fetch_pod_excel.php" method="POST">	
	<div class="form-group col-md-4" style="">
	
	<div class="col-md-12">
		<h5 style="background:gray;padding:8px;color:#fff">-- Excel Upload --</h5>
	</div>
	
	<div class="form-group col-md-10">
		<label>Select File </label>
		<input type="file" name="lrno_file" class="form-control" required />
	</div>	
	
	<div class="form-group col-md-11">
		<button type="submit" name="excel_view" class="btn btn-sm btn-success" style="letter-spacing:1px;">View</button>
		<button type="submit" name="excel_download" class="btn btn-sm btn-primary" style="letter-spacing:1px;">Excel download</button>
	</div>	
	
	</div>	
</form>	 -->

	</div>	
	</div>	
</div>	

<div class="row">
	<div class="form-group col-md-12" id="pod_result"></div>
</div>
</div>

</body>
</html>

<script>
function ViewPod(vou_type,vou_no)
{
	$("#loadicon").show();	
	   jQuery.ajax({
	   url: "./fetch_pod_copy.php",
		data: 'vou_type=' + vou_type + '&vou_no=' + vou_no,
	   type: "POST",
	   success: function(data) {
		$("#pod_result2").html(data);
	   },
	   error: function() {}
	   });
}
</script>

<button type="button" id="modal_button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display:none"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">POD Copy : <span id="vou_no_modal"></span></h4>
      </div>
      <div class="modal-body">
		<div class="row" id="pod_result2">
		</div>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>