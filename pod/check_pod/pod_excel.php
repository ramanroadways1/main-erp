<?php 
require_once '../../connection.php';

$output ='';

$from = escapeString($conn,$_POST['from_date']);
$to = escapeString($conn,$_POST['to_date']);

$result = Qry($conn,"SELECT frno,lrno,branch,date,truck_no,fstation,tstation,consignor,consignee,wt12 FROM freight_form_lr WHERE date 
BETWEEN '$from' AND '$to'");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">   
    <tr>  
		<th>Vou No</th>
		<th>LR No</th>
		<th>Branch</th>
		<th>LR Date</th>
		<th>Vehicle No</th>
		<th>From_Loc</th>
		<th>To_Loc</th>
		<th>Consignor</th>
		<th>Consignee</th>
		<th>POD Copy</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
if(strpos($row['frno'],"OLR") !== false){
   $vou_type="OLR";
} else{
   $vou_type="MARKET";
}	  
	  
if($vou_type=='OLR')
{	
	$qry2 = Qry($conn,"SELECT lrno,date as rcv_date,copy as pod_copy,branch as pod_rcv_by FROM dairy.rcv_pod WHERE lrno='$row[frno]'");
	$base_url = "https://rrpl.online/diary/close_trip";
}
else
{
	$qry2 = Qry($conn,"SELECT lrno,pod_date as rcv_date,pod_copy as pod_copy,branch as pod_rcv_by FROM rcv_pod WHERE frno='$row[frno]'");
	$base_url = "https://rrpl.online/b5aY6EZzK52NA8F";
}

$pod_view = array();
while($row2 = fetchArray($qry2))
{
	$pod_copies = explode(',', $row2['pod_copy']);
	foreach($pod_copies as $pod_copies1)
	{		
		$pod_view[]="$base_url/$pod_copies1";
	// <b>LR Number : $row2[lrno]</b> &nbsp; $row2[rcv_date] ($row2[pod_rcv_by])
		// <br>	
		// echo "<a href='$base_url/$pod_copies1' target='_blank'>View Copy</a>";
		// $pod;
		// echo $pod_copies1.'<br>';  
	}
}	
	  
   $output .= '
    <tr> 
			<td>'.$row["frno"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["truck_no"].'</td>
			<td>'.$row["fstation"].'</td>
			<td>'.$row["tstation"].'</td>
			<td>'.$row["consignor"].'</td>
			<td>'.$row["consignee"].'</td>
			<td>'.implode("<br>",$pod_view).'</td>	
		</tr>
   ';
  }
  $output .= '</table>';
  mysqli_close($conn);
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=POD.xls');
  echo $output;

?>