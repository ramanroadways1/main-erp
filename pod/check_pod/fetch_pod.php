<?php
require_once '../../connection.php';

$type = $_POST['type'];

if($type=='LR')
{
	$lrno = escapeString($conn,$_POST['lrno']);
	
	$qry = Qry($conn,"SELECT frno,lrno,branch,date,truck_no,fstation,tstation,consignor,consignee,wt12 FROM freight_form_lr WHERE 
	lrno='$lrno'");
}
else if($type=='DATE')
{
	$from_date = escapeString($conn,$_POST['from_date']);
	$to_date = escapeString($conn,$_POST['to_date']);
	
	$qry = Qry($conn,"SELECT frno,lrno,branch,date,truck_no,fstation,tstation,consignor,consignee,wt12 FROM freight_form_lr WHERE 
	date BETWEEN '$from_date' AND '$to_date'");
}
else
{
	echo "<center><h3 style='color:red'>Invalid Request..</h3></center>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!$qry)
{
	echo "<center><h3 style='color:red'>".mysqli_error($conn)."</h3></center>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($qry)==0)
{
	echo "<center><h3 style='color:red'>No result found..</h3></center>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}
// branch,lr_date,truck_no,fstation,tstation,consignor,consignee,wt12
echo "<table class='table table-bordered table-striped' style='color:#000;font-size:12px;'>
<tr>
	<th>#</th>
	<th>Vou_No</th>
	<th>LR_No</th>
	<th>Branch</th>
	<th>LR_Date</th>
	<th>Vehicle_No</th>
	<th>From_Loc</th>
	<th>To_Loc</th>
	<th>Consignor & Consignee</th>
	<th>View_POD</th>
</tr>";
$i=1;
while($row = fetchArray($qry))
{
	
if(strpos($row['frno'],"OLR") !== false){
   $vou_type="OLR";
} else{
   $vou_type="MARKET";
}
	echo "<tr>
		<td>$i</td>
		<td>$row[frno]</td>
		<td>$row[lrno]</td>
		<td>$row[branch]</td>
		<td>".date("d-m-y",strtotime($row["date"]))."</td>
		<td>$row[truck_no]</td>
		<td>$row[fstation]</td>
		<td>$row[tstation]</td>
		<td>$row[consignor] <br> $row[consignee]</td>
		<td><button type='button' onclick=ViewPod('$vou_type','$row[frno]') class='btn btn-danger btn-xs'>View_POD</button></td>
	</tr>";
$i++;	
}

echo "<table>
<script>
$('#loadicon').hide();
</script>
";
?>