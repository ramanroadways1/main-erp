<?php
require_once '../../connection.php';

$vou_type = escapeString($conn,$_POST['vou_type']);
$vou_no = escapeString($conn,$_POST['vou_no']);

if($vou_type=='OLR')
{	
	$qry = Qry($conn,"SELECT lrno,pod_date as rcv_date,pod_copy as pod_copy,branch as pod_rcv_by FROM rcv_pod WHERE frno='$vou_no'");
	$base_url = "https://rrpl.online/diary/close_trip";
}
else
{
	$qry = Qry($conn,"SELECT lrno,pod_date as rcv_date,pod_copy as pod_copy,branch as pod_rcv_by FROM rcv_pod WHERE frno='$vou_no'");
	$base_url = "https://rrpl.online/b5aY6EZzK52NA8F";
}

if(!$qry)
{
	echo "<center><h5 style='color:red'>".mysqli_error($conn)."</h5></center>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

echo "<script>
		$('#vou_no_modal').html('$vou_no');
	</script>";
	// exit();
	
if(numRows($qry)==0)
{
	echo "<center><h5 style='color:red'>No result found or pod not received.</h5></center>
	<script>
		$('#modal_button').click();
		$('#loadicon').hide();
	</script>";
	exit();
}

while($row = fetchArray($qry))
{
	$pod_copies = explode(',', $row['pod_copy']);
	foreach($pod_copies as $pod_copies1)
	{	
	echo "<div class='form-group col-md-6'>
		<b>LR Number : $row[lrno]</b> &nbsp; ".date("d-m-y",strtotime($row["rcv_date"]))." ($row[pod_rcv_by]) <a href='$base_url/$pod_copies1' target='_blank'>View Full-Screen</a>
		<br>
		<a href='$base_url/$pod_copies1' target='_blank'><img src='$base_url/$pod_copies1' style='width:100%;height:300px' class='img-responsive' /></a>
	</div>";
		// echo $pod_copies1.'<br>';  
	}
}

echo "<script>
		$('#modal_button').click();
		$('#loadicon').hide();
	</script>";
	exit();
?>	