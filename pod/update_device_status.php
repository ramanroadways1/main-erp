<?php
require_once '../connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$selection = escapeString($conn,strtoupper($_POST['selection']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$tno = escapeString($conn,strtoupper($_POST['tno']));
$tno2 = escapeString($conn,strtoupper($_POST['tno2']));
$imei = escapeString($conn,strtoupper($_POST['imei']));

if($vou_no=='')
{
	echo "<script> 	
		alert('Invalid voucher number !');
		window.close();
	</script>";
	exit();
}

if($tno=='')
{
	echo "<script> 	
		alert('Vehicle number not found !');
		window.close();
	</script>";
	exit();
}

if($tno!=$tno2)
{
	echo "<script> 	
		alert('Vehicle number not verified !');
		window.close();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM gps WHERE frno='$vou_no'");
	
	if(!$chk_record){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> 	
			alert('Error !');
			window.close();
		</script>";
		exit();
	}
	
	if(numRows($chk_record)>0)
	{
		echo "<script> 	
			alert('Duplicate record found !');
			$('#GPSDeviceSelection').attr('disabled',true);	
			$('#GPSDeviceButton').attr('disabled',true);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
	

	$chk_device = Qry($conn,"SELECT id,device_imei,fix_attach FROM gps_device_inventory WHERE active_vou='$vou_no'");
	
	if(!$chk_device){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> 	
			alert('Error !');
			window.close();
		</script>";
		exit();
	}
	
	if(numRows($chk_device)==0)
	{
		echo "<script> 	
			alert('GPS device not found !');
			$('#GPSDeviceSelection').attr('disabled',true);	
			$('#GPSDeviceButton').attr('disabled',true);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
	else if(numRows($chk_device)>1)
	{
		echo "<script> 	
			alert('Multiple GPS devices active on this vehicle !');
			$('#GPSDeviceSelection').attr('disabled',true);	
			$('#GPSDeviceButton').attr('disabled',true);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
	
	$row_gps_id = fetchArray($chk_device);
	
	if($row_gps_id['fix_attach']!='Attach')
	{
		echo "<script> 	
			alert('Error : Device type is not type of Attach.');
			window.close();
		</script>";
		exit();
	}
	
	$device_imei_db = $row_gps_id['device_imei'];

if($selection=='1')
{
	if($imei=='')
	{
		echo "<script> 	
			alert('Device\'s IMEI number is required !');
			$('#GPSDeviceButton').attr('disabled',false);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
	
	if($device_imei_db!=$imei)
	{
		echo "<script> 	
			alert('Device number not verified !');
			$('#GPSDeviceButton').attr('disabled',false);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
}
		
StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO gps(frno,gps_device_id,gps,gps_service,timestamp) VALUES ('$vou_no','$row_gps_id[id]',
'$selection','1','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($selection=='0')
{
	// not rcvd 
	
	$update_device_status = Qry($conn,"UPDATE gps_device_inventory SET active_on='',active_vou='',status='2',
	remark='Branch: $branch. Device Lost. Voucher no: $vou_no',was_active_on='$tno',remark_timestamp='$timestamp' WHERE id='$row_gps_id[id]'");
	if(!$update_device_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	// rcvd 
	
	$update_device_status = Qry($conn,"UPDATE gps_device_inventory SET fix_attach='',active_vou='',active_on='',status='0',
	was_active_on='$tno' WHERE id='$row_gps_id[id]'");
	
	if(!$update_device_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_device_status2 = Qry($conn,"INSERT INTO gps_device_rcvd_log(device_id,was_active_on,narration,timestamp) 
	VALUES ('$row_gps_id[id]','$tno','$vou_no','$timestamp')"); 
	
	if(!$update_device_status2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Device status updated successfully !');
		$('#GPSDeviceSelection').attr('disabled',true);	
		$('#GPSDeviceButton').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Error While Processing Request !');
		$('#GPSDeviceSelection').attr('disabled',true);	
		$('#GPSDeviceButton').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>