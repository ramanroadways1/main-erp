<?php
require_once '../connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$selection = escapeString($conn,strtoupper($_POST['selection']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));

if($vou_no=='')
{
	echo "<script> 	
		alert('Invalid voucher number !');
		window.close();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT id FROM gps WHERE frno='$vou_no'");
	
	if(!$chk_record){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script> 	
			alert('Error !');
			window.close(); 
		</script>";
		exit();
	}
	
	if(numRows($chk_record)>0)
	{
		echo "<script> 	
			alert('Duplicate record found !');
			$('#GPSDeviceSelection').attr('disabled',true);	
			$('#GPSDeviceButton').attr('disabled',true);	
			$('#loadicon').hide();	
		</script>";
		exit();
	}
	
$insert = Qry($conn,"INSERT INTO gps(frno,gps,gps_service,timestamp) VALUES ('$vou_no','$selection','2','$timestamp')");
if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	closeConnection($conn);
	echo "<script> 	
		alert('Updated Successfully !');
		$('#GPSDeviceSelection').attr('disabled',true);	
		$('#GPSDeviceButton').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>";
	exit();
?>