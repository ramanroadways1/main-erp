<?php 
require_once '../connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));

if(empty($_POST['signature']))
{
	echo '<script>
			alert("Please Save Signature First !");
			$("#balance_submit").attr("disabled", false);
			$("#loadicon").hide();
		</script>';
	exit();
}
else
{
	$signature = $_POST['signature'];
}

$oid = escapeString($conn,strtoupper($_POST['oid']));
$bid = escapeString($conn,strtoupper($_POST['bid']));

// if($bid=='344')
// {
	// echo '<script>
			// alert("Balance blocked of transporter : SOURABH TRANSPORT CO !");
			// window.location.href="./";
		// </script>';
	// exit();
// }

$company = escapeString($conn,strtoupper($_POST['company']));
$vou_date = escapeString($conn,strtoupper($_POST['vou_date']));
$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));

$db_af = escapeString($conn,strtoupper($_POST['db_af']));
$db_weight = escapeString($conn,strtoupper($_POST['db_weight']));
$db_lrno = escapeString($conn,strtoupper($_POST['db_lrno']));
$pod_date = escapeString($conn,strtoupper($_POST['pod_rcvd_date']));
$rcv_pod_charges_db = escapeString($conn,strtoupper($_POST['rcv_pod_charges_db']));
$gps_deposit_return = escapeString($conn,($_POST['gps_deposit_return']));

if(empty($pod_date) || $pod_date==0 || $pod_date=='')
{
	echo '<script>
			alert("POD Date is not valid !");
			document.getElementById("RefreshForm").submit();
		</script>';
	exit();
}

$balance = escapeString($conn,strtoupper($_POST['balance']));

$chk_empty_lr = Qry($conn,"SELECT active_vou FROM gps_device_inventory WHERE active_on='$truck_no'");

if(!$chk_empty_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_empty_lr)>0)
{
	$row_chk_lr = fetchArray($chk_empty_lr);
	
	if($row_chk_lr['active_vou']=="")
	{
		echo "<script>
				alert('Error : LR not mapped with gps device !');
				$('#balance_submit').attr('disabled', false);
				$('#loadicon').hide();
			</script>";
		exit();
	}
}

$chk_gps_repeat_active = Qry($conn,"SELECT device_imei,fix_attach,is_repeated FROM gps_device_inventory WHERE 
active_vou='$vou_no'");

if(!$chk_gps_repeat_active){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_gps_repeat_active)>0)
{
	$row_repeat_device = fetchArray($chk_gps_repeat_active);
	$device_no_repeat = "******".substr($row_repeat_device['device_imei'],-5);
	
	if($row_repeat_device['is_repeated']=="1")
	{
		echo "<script>
				alert('Warning : GPS device(repeated): $device_no_repeat not received !');
				$('#loadicon').hide();
			</script>";
		exit();
	}
}

$chk_gps_in_transit = Qry($conn,"SELECT id FROM gps_device_in_transit WHERE vou_no='$vou_no' AND status='0'");
if(!$chk_gps_in_transit){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_gps_in_transit)>0)
{
	echo '<script>
			alert("Error : GPS device(in-transit) not received !");
			$("#loadicon").hide();
		</script>';
	exit();
}

$get_fm_bal = Qry($conn,"SELECT branch,baladv,gps_id FROM freight_form WHERE frno='$vou_no'");
if(!$get_fm_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($get_fm_bal)==0){
	errorLog("Freight Memo not found.",$conn,$page_name,__LINE__);
	Redirect("Freight Memo not found.","./");
	exit();
}

$row_fm_bal = fetchArray($get_fm_bal);

if($row_fm_bal['baladv']!=$balance)
{
	echo '<script>
			alert("Balance amount verification failed !");
			document.getElementById("RefreshForm").submit();
		</script>';
	exit();
}
	
$unloading = escapeString($conn,strtoupper($_POST['unloading']));
$detention = escapeString($conn,strtoupper($_POST['detention']));
$other = escapeString($conn,strtoupper($_POST['other']));
$claim = escapeString($conn,strtoupper($_POST['claim']));
$tds = escapeString($conn,strtoupper($_POST['tds']));
$gps = escapeString($conn,strtoupper($_POST['gps']));
$gps_device_charge = escapeString($conn,strtoupper($_POST['gps_device_charge']));
$late_pod = escapeString($conn,strtoupper($_POST['late_charge_pod']));
$total_balance = escapeString($conn,strtoupper($_POST['total_balance']));

$balance_to = escapeString($conn,strtoupper($_POST['paidto']));
$cash = escapeString($conn,strtoupper($_POST['cash_amount']));

$cheque_payment = escapeString($conn,strtoupper($_POST['chq_sel']));
$diesel_payment = escapeString($conn,strtoupper($_POST['dsl_sel']));
$rtgs_payment = escapeString($conn,strtoupper($_POST['rtgs_sel']));

$narration = escapeString($conn,strtoupper(trim($_POST['narration'])));	

$balance_left = escapeString($conn,strtoupper($_POST['balance_left']));	

if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $narration) AND $narration!='')
{
	echo "<script>
		alert('Error : Check Narration !');
		$('#loadicon').hide();
		$('#btn_submit').attr('disabled', false);
	</script>";
	exit();
}

if($company!='RRPL' AND $company!='RAMAN_ROADWAYS')
{
	echo "<script>
			alert('Company not found.');
			document.getElementById('RefreshForm').submit();
		</script>";
	exit();	
}

if($balance_left!=0)
{
	echo "<script>
		alert('Please check Balance Amount..!!');
		$('#loadicon').hide();
		$('#balance_submit').attr('disabled', false);
	</script>";
	exit();	
}

$verify_amount = (($balance+$unloading+$detention+$gps_deposit_return)-($other+$claim+$tds+$gps+$gps_device_charge+$late_pod));

if($total_balance!=$verify_amount)
{
	echo "<script>
		alert('Please check Balance Amount..!!');
		$('#loadicon').hide();
		$('#balance_submit').attr('disabled', false);
	</script>";
	exit();	
}

if($balance_to=='NULL' && $total_balance!=0)
{
	echo "<script>
		alert('Please Check Balance Amount.');
		$('#loadicon').hide();
		$('#balance_submit').attr('disabled', false);
		</script>";
	exit();
}
else if($balance_to!='NULL' && ($total_balance<=0 AND $tds==0))
{
	echo "<script>
		alert('Please Check Balance Amount.');
		$('#loadicon').hide();
		$('#balance_submit').attr('disabled', false);
		</script>";
	exit();
}

if($balance_to=='NULL' || $balance_to=='' AND ($cheque_payment=='1' || $diesel_payment=='1' || $rtgs_payment=='1'))
{
	echo "<script>
			alert('Invalid payment mode selected !');
			document.getElementById('RefreshForm').submit();
		</script>";
	exit();
}

$year_now = date("Y");
$year_prev = $year_now-1;
$year_prev = $year_prev."-04-01";

if((strtotime($vou_date)<strtotime($year_prev)) AND $_POST['rtgs_amount']>0)
{
	$check_rtgs_allow_last_year = Qry($conn,"SELECT id FROM allow_rtgs_last_fy_fm WHERE vou_no='$vou_no'");
	if(!$check_rtgs_allow_last_year){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
			
	if(numRows($check_rtgs_allow_last_year)==0){
		Redirect("This voucher is not belongs to current financial year. You can not pay throught RTGS.","./");
		exit();
	}
}

if($cheque_payment=='1')
{
	$cheque_amount = escapeString($conn,strtoupper($_POST['cheque_amount']));
	$cheque_no = escapeString($conn,strtoupper($_POST['cheque_no']));
	
	if($cheque_amount<=0){
		echo "<script>
			alert('Cheque Amount is not valid !');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	$cheque_amount = 0;
	$cheque_no = "";
}

	$fetch_diesel = Qry($conn,"SELECT SUM(amount) as diesel_amount,SUM(qty) as dieselQty FROM diesel_sample WHERE frno='$vou_no' AND 
	type='BAL'");

	if(!$fetch_diesel){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}	

	if(numRows($fetch_diesel)>0)
	{
		$row_diesel = fetchArray($fetch_diesel);
		$diesel_entry = $row_diesel['diesel_amount'];
		$diesel_Qty = $row_diesel['dieselQty'];
	}
	else
	{
		$diesel_entry = 0;
		$diesel_Qty = 0;
	}
	
$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$vou_no' AND ril_card='1' AND type='BAL'");
	
if(!$chk_ril_stock){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_ril_stock)>0)
{
	while($row_ril = fetchArray($chk_ril_stock))
	{
		$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
		if(!$get_balance){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_balance = fetchArray($get_balance);
		
		if($row_balance['balance']<$row_ril['amount'])
		{
			echo "<script>
				alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
				$('#loadicon').hide();
				$('#balance_submit').attr('disabled', false);
			</script>";
			exit();	
		}
	}
}

if($diesel_payment=='1')
{
	$diesel_fm_amount = escapeString($conn,$_POST['total_diesel']);
	
	if($diesel_fm_amount<=0)
	{
		echo "<script>
			alert('Please Check Diesel Amount !!');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
		exit();
	}
	
	if($diesel_fm_amount != $diesel_entry)
	{
		echo "<script>
			alert('Diesel Amount not verified. Please Check !!');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	if($diesel_entry>0)
	{
		echo "<script>
			alert('Diesel Cache found. You can not select NO option !');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
		exit();
	}
}

$get_owner_info = Qry($conn,"SELECT name,mo1,pan,up6,bank,acname,accno,ifsc FROM mk_truck WHERE id='$oid'");
if(!$get_owner_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_owner_info)==0){
	errorLog("OWNER not found. Oid: $oid.",$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
$row_owner = fetchArray($get_owner_info);
$dec = $row_owner['up6'];
	
$owner_pan_no = $row_owner['pan'];
	
$get_broker_info = Qry($conn,"SELECT name,mo1,pan,bank,acname,accno,ifsc FROM mk_broker WHERE id='$bid'");

if(!$get_broker_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($get_broker_info)==0){
	errorLog("BROKER not found. Bid: $bid.",$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
$row_broker = fetchArray($get_broker_info);

$broker_pan_no = $row_broker['pan'];
	
if($balance_to=='OWNER')
{
	$adv_party = $row_owner['name'];
	$adv_party_pan = $row_owner['pan'];
	$adv_party_mobile = $row_owner['mo1'];
}
else if($balance_to=='BROKER')
{
	$adv_party = $row_broker['name'];
	$adv_party_pan = $row_broker['pan'];
	$adv_party_mobile = $row_broker['mo1'];
}
else if($balance_to=='NULL')
{
	if(strlen($row_owner['pan'])!=10)
	{
		$get_broker = Qry($conn,"SELECT name,pan,mo1 FROM mk_broker WHERE id='$bid'");
	
		if(!$get_broker){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
		}
		
		if(numRows($get_broker)==0){
		errorLog("Account details not found. Bid: $bid.",$conn,$page_name,__LINE__);
		Redirect("Account details not found.","./");
		exit();
		}
		
		$row_broker1 = fetchArray($get_broker);
		
		if(strlen($row_broker1['pan'])!=10)
		{
			echo "<script>
				alert('Company not found.');
				document.getElementById('RefreshForm').submit();
			</script>";
			exit();	
		}
		else
		{
			$adv_party = $row_broker1['name'];
			$adv_party_pan = $row_broker1['pan'];
			$adv_party_mobile = $row_broker1['mo1'];
		}
	}
	else
	{
		$adv_party = $row_owner['name'];
		$adv_party_pan = $row_owner['pan'];
		$adv_party_mobile = $row_owner['mo1'];
	}
}
else
{
	echo "<script>
		alert('Invalid option selected.');
		document.getElementById('RefreshForm').submit();
	</script>";
	exit();	
}

	if($dec=='')
	{
		$tds_deduct=1;
	
		if($balance_to=='OWNER')
		{
			if($row_owner['pan']=='AAACC7939H')
			{
				$get_tds_chartered = Qry($conn,"SELECT freight FROM _party_freight_db WHERE party_pan='AAACC7939H'");
					
				if(!$get_tds_chartered){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}
						
				$row_tds_chartered = fetchArray($get_tds_chartered);
						
				$tds_chartered = $row_tds_chartered['freight'] + $unloading+$detention;
						
				if($tds_chartered>=2500000)
				{
					$tds_amount_db = sprintf("%0.2f",ceil(($unloading+$detention)*2/100));
				}
				else
				{
					$tds_amount_db = sprintf("%0.2f",ceil(($unloading+$detention)*0.10/100));
				}
			}
			else
			{
					$get_tds_owner = Qry($conn,"SELECT tds_value FROM tds_value WHERE pan_letter='".substr($row_owner['pan'],3,1)."'");
					
					if(!$get_tds_owner){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while processing Request","./");
						exit();
					}
					
					if(numRows($get_tds_owner)>0)
					{
						$row_tds_value_owner = fetchArray($get_tds_owner);
						$tds_amount_db=sprintf("%0.2f",ceil(($unloading+$detention)*$row_tds_value_owner['tds_value']/100));
					}
					else
					{
						echo "<script>
							alert('Owner\'s PAN Card is Invalid. Please update Owner\'s PAN Card !');
							document.getElementById('RefreshForm').submit();
						</script>";
						exit();
					}
					
				if(strlen($row_owner['pan'])!=10)
				{
					echo "<script>
						alert('Owner\'s PAN Card is Invalid. Please update Owner\'s PAN Card !');
						document.getElementById('RefreshForm').submit();
					</script>";
					exit();
				}
			}
		}
		else if($balance_to=='BROKER')
		{
			if($row_broker['pan']=='AAACC7939H')
			{
				$get_tds_chartered = Qry($conn,"SELECT freight FROM _party_freight_db WHERE party_pan='AAACC7939H'");
					
				if(!$get_tds_chartered){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}
						
				$row_tds_chartered = fetchArray($get_tds_chartered);
						
				$tds_chartered = $row_tds_chartered['freight'] + $unloading+$detention;
						
				if($tds_chartered>=2500000)
				{
					$tds_amount_db = sprintf("%0.2f",ceil(($unloading+$detention)*2/100));
				}
				else
				{
					$tds_amount_db = sprintf("%0.2f",ceil(($unloading+$detention)*0.10/100));
				}
			}
			else
			{
				$get_tds_broker = Qry($conn,"SELECT tds_value FROM tds_value WHERE pan_letter='".substr($row_broker['pan'],3,1)."'");
					
					if(!$get_tds_broker){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while processing Request","./");
						exit();
					}
					
					if(numRows($get_tds_broker)>0)
					{
						$row_tds_value_broker = fetchArray($get_tds_broker);
						$tds_amount_db=sprintf("%0.2f",ceil(($unloading+$detention)*$row_tds_value_broker['tds_value']/100));
					}
					else
					{
						echo "<script>
							alert('Broker\'s PAN Card is Invalid. Please update Broker\'s PAN Card !');
							document.getElementById('RefreshForm').submit();
						</script>";
						exit();
					}
					
				if(strlen($row_broker['pan'])!=10)
				{
					echo "<script>
						alert('Broker\'s PAN Card is Invalid. Please update Broker\'s PAN Card !');
						document.getElementById('RefreshForm').submit();
					</script>";
					exit();
				}
			}
		}
		else
		{
			$tds_amount_db = 0;
		}
	}
	else
	{
		$tds_deduct=0;
	}
	
if($tds_deduct==1)
{
	if(($unloading+$detention)>0)
	{
		if($tds_amount_db<=0)
		{
			echo "<script>
					alert('TDS amount is not valid !');
					$('#loadicon').hide();
					$('#balance_submit').attr('disabled', false);
				</script>";
			exit();
		}
	}
	else
	{
		$tds_amount_db = 0;
	}
}
else
{
	$tds_amount_db = 0;
}

if($tds != $tds_amount_db)
{
	echo "<script>
			alert('Invalid TDS amount. TDS amount should be : $tds_amount_db.');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
	exit();
}

$net_bal = $balance+$unloading+$detention;
			
$company_fund_qry = Qry($conn,"SELECT charges FROM company_fund WHERE $net_bal>=`amt_from` AND $net_bal<=`amt_to`");

if(!$company_fund_qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($company_fund_qry)>0)
{
	$row_company_fund = fetchArray($company_fund_qry);
	$amount_company_fund = $row_company_fund['charges'];
}
else
{
	$amount_company_fund = 0;
}	

$get_balance_max_val = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='BALANCE_LOCK' AND is_active='1'");

if(!$get_balance_max_val){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit(); 
}

if(numRows($get_balance_max_val)==0)
{
	$max_balance_days = 60;
}
else
{
	if($branch=='PEN'){
		$max_balance_days = 180;
	}else{
		$row_bal_lock_val = fetchArray($get_balance_max_val);
		$max_balance_days = $row_bal_lock_val['func_value'];
	}
}

$datediff_60days = round((strtotime(date("Y-m-d"))-strtotime($vou_date)) / (60 * 60 * 24));	
	
// if($branch=='PEN'){
	// $max_balance_days = 180;
// }else{
	// $max_balance_days = 90;
// }

if($datediff_60days>$max_balance_days)
{	
	$chkForAccess=Qry($conn,"SELECT id FROM extend_bal_pending_validity WHERE frno='$vou_no' AND admin_approval_timestamp IS NOT NULL AND admin_approval_timestamp!=''");
	if(!$chkForAccess){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
				
	if(numRows($chkForAccess)==0)
	{
		if($row_fm_bal['branch']=='PEN' AND (strtotime($vou_date)>=strtotime("2021-01-01")))
		{
			
		}
		else
		{
			echo "<script>
			alert('ERROR : Freight Memo is $max_balance_days days older. You Can not pay balance.');
				document.getElementById('RefreshForm').submit();
			</script>";
			exit();
		}
	}
	else
	{
		// $bal_val_extend_charges="50";
	}
}
else
{
	// $bal_val_extend_charges="0";
}

$fetch_min_lr_date = Qry($conn,"SELECT branch,date FROM freight_form_lr WHERE frno='$vou_no' ORDER BY date ASC LIMIT 1");

if(!$fetch_min_lr_date){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_min_lr_date)==0)
{
	echo "<script>
		alert('Freight Memo not found.');
		window.location.href='../';
	</script>";
	exit();
}

$row_lr_date = fetchArray($fetch_min_lr_date);
$lr_branch = $row_lr_date['branch'];
$min_lr_date = $row_lr_date['date'];

if((strtotime($min_lr_date)>=strtotime("2020-11-01")) AND $db_af>10000 AND $branch!='SIROHIROAD')
{
	// if($vou_no=="DHUF051020202" || $vou_no=="DHUF1910202010" || $vou_no=="DHUF231020202" || $vou_no=="DHUF301020205" || $vou_no=="DHUF3010202010")
	// {
		// $balance_charges="0";
	// }
	// else
	// {
		// $balance_charges="50";
	// }
	
	if($lr_branch=='JHARSUGUDA')
	{
		$balance_charges="100";
	}
	else
	{
		$balance_charges="50";
	}
}
else
{
	$balance_charges="0";
}

$total_other_db = $rcv_pod_charges_db+$amount_company_fund+$balance_charges;

if($other<$total_other_db)
{
	echo "<script>
			alert('Other amount is Invalid. Other amount\'s min value is : $total_other_db.');
			document.getElementById('RefreshForm').submit();
		</script>";
	exit();
}

$qry_gps_rcvd = Qry($conn,"SELECT id,gps,gps_device_id,gps_service FROM gps WHERE frno='$vou_no'");

if(!$qry_gps_rcvd){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($row_fm_bal['gps_id']!="0" AND numRows($qry_gps_rcvd)==0 AND numRows($chk_gps_repeat_active)>0 AND $row_repeat_device['fix_attach']!='Fix')
{
	echo '<script>
			alert("Error : GPS device not received.");
			document.getElementById("RefreshForm").submit();
		</script>';
	exit();
}

if(numRows($qry_gps_rcvd)>0)
{
	$rowGps_Charges = fetchArray($qry_gps_rcvd);
	
	if($rowGps_Charges['gps_service']=="1")
	{
		if($rowGps_Charges['gps']=='0'){
			$gps_charges = 1880;
		}
		else{
			$gps_charges = 0;
		}
	}
	else if($rowGps_Charges['gps_service']=="2")
	{
		if($rowGps_Charges['gps']=='0'){
			$gps_charges = 3000;
		}
		else{
			$gps_charges = 0;
		}
	}
	else
	{
		echo "<script>
			alert('Invalid service type : GPS.');
			window.close();
		</script>";
		exit();
	}
}
else
{
	$gps_charges = 0;
}

if($gps_device_charge!=$gps_charges)
{
	echo "<script>
			alert('GPS Device Charge not verified. Value should be : $gps_charges.');
			document.getElementById('RefreshForm').submit();
		</script>";
	exit();
}

$fetch_open_pod = Qry($conn,"SELECT branch FROM open_pod WHERE branch='$branch'");
if(!$fetch_open_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_open_pod)>0)
{
	$late_pod_charges = 0;
}
else
{
	// if(strtotime($vou_date)>=strtotime('2018-05-19'))
	// {
		// if((strtotime($vou_date)>=strtotime('2020-02-15')) AND (strtotime($vou_date)<=strtotime('2020-06-30')) AND (strtotime(date("Y-m-d"))<=strtotime('2020-08-31')))
		// {
			// $late_pod_charges = 0;
		// }
		// else
		// {
			
		$datediff = strtotime($pod_date) - strtotime($vou_date);
		$diff_value=round($datediff / (60 * 60 * 24));	

		if($diff_value>30)
		{
			$chkForLatePODFree = Qry($conn,"SELECT charges,deduct_late_pod,add_unloading,add_detention FROM rcv_pod_free WHERE frno='$vou_no' AND is_allowed='1'");
			if(!$chkForLatePODFree){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
				
			if(numRows($chkForLatePODFree)>0)
			{
				$row_free_pod = fetchArray($chkForLatePODFree);
				$is_deduct_late_pod = $row_free_pod['deduct_late_pod'];
				
				if($is_deduct_late_pod=="0") // sys default
				{
					if($diff_value>60)
					{
						$late_pod_charges = (($diff_value-60)*100)+1500;
					}
					else
					{
						$late_pod_charges = ($diff_value-30)*50;
					}
				}
				else if($is_deduct_late_pod=="2") // entered value
				{
					$late_pod_charges = $row_free_pod['charges'];
				}
				else // no deduction
				{
					$late_pod_charges = 0;
				}
			} 
			else
			{					
				// $get_late_pod_charges = Qry($conn,"SELECT chrg FROM pod_late_chrg WHERE $diff_value>=`from1` AND $diff_value<=`to1` AND $net_bal>=`amt_frm` AND $net_bal<=`amt_to`");
				// if(!$get_late_pod_charges){
					// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					// Redirect("Error while processing Request","./");
					// exit();
				// }	
				// $row_charges = fetchArray($get_late_pod_charges);
				// $late_pod_charges = $row_charges['chrg'];
				
				if($diff_value>60)
					{
						$late_pod_charges = (($diff_value-60)*100)+1500;
					}
					else
					{
						$late_pod_charges = ($diff_value-30)*50;
					}
			}
		}
		else
		{
			$late_pod_charges = 0;
		}
		// }
	// }
	// else
	// {
		// $late_pod_charges = 0;
	
}

// $check_60_days = Qry($conn,"SELECT id FROM rcv_pod WHERE frno='$vou_no' and DATEDIFF(pod_date,fm_date)>60");
// if(!$check_60_days){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }

// AND (strtotime(date("Y-m-d"))<=strtotime('2020-07-31'))

// if((strtotime($vou_date)>=strtotime('2020-02-15')) AND (strtotime($vou_date)<=strtotime('2020-06-30')) AND (strtotime(date("Y-m-d"))<=strtotime('2020-08-31')))
// {
	// $lock_60_days = "0";
// }
// else
// {
	// $lock_60_days = "1";
// }
		
// if(numRows($check_60_days)>0 AND $lock_60_days=="1")
// {
	
// }

if($late_pod!=$late_pod_charges)
{
	echo "<script>
			alert('Late POD charge is not valid. Value should be : $late_pod_charges.');
			$('#balance_submit').attr('disabled', false);
			$('#loadicon').hide();
			// document.getElementById('RefreshForm').submit();
		</script>";
	exit();
}

// gps charges verify start

$update_pending_charges="NO";
$update_gps_validity="NO";
$mark_balance_done="NO";

if($gps_device_charge>0 AND $rowGps_Charges['gps_service']=="1")
{
	$gps_rent_charge = 0;
	$gps_device_lost = "YES";
	$gps_device_id = $rowGps_Charges['gps_device_id'];
	$gps_action="NO";
}
else
{
	$gps_device_lost = "NO";
	
	$get_gps_data = Qry($conn,"SELECT id,device_imei,fix_attach,pending_charges,validity_start,validity_end,
	current_validity FROM gps_device_inventory WHERE active_vou='$vou_no'");
	
	if(!$get_gps_data){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_gps_data)==0)
	{
		$gps_rent_charge = 0;
		
		$chk_gps_install_records = Qry($conn,"SELECT fix_attach,pending_charges,val_start as validity_start,
		val_end as validity_end,curr_val as current_validity,balance_done,manual FROM gps_charges_pending 
		WHERE vou_no='$vou_no'");

		if(!$chk_gps_install_records){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		} 
		
		if(numRows($chk_gps_install_records)>0)
		{
			
			$mark_balance_done="YES";
				
			$row_gps_record1 = fetchArray($chk_gps_install_records);
			
			if($row_gps_record1['balance_done']=="1")
			{
				echo "<script>
					alert('Error : Balance paid already.');
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			if($row_gps_record1['manual']=="1")
			{
				$gps_rent_charge = 380;
			}
			else
			{			
				if($row_gps_record1['pending_charges']=="1")
				{
					if($row_gps_record1['fix_attach']=="Fix"){
						$gps_rent_charge = 1880;
					}
					else{
						$gps_rent_charge = 380;
					}
				}
				else
				{
					if($row_gps_record1['fix_attach']=="Fix")
					{
						$gps_rent_charge = 0;
					}
					else
					{		
						if($row_gps_record1['current_validity']==0 || $row_gps_record1['current_validity']==""){
							$gps_validity_date = $row_gps_record1['validity_end'];
						}
						else{
							$gps_validity_date = $row_gps_record1['current_validity'];
						}
						
						if($gps_validity_date==0 || $gps_validity_date=="")
						{
							echo "<script>
								alert('Warning : GPS validity date is not valid.');
								window.close();	
							</script>";
							exit();
						}
					
						if(strtotime(date("Y-m-d"))>strtotime($gps_validity_date))
						{
							$gps_day = round((strtotime(date("Y-m-d")) - strtotime($gps_validity_date)) / (60 * 60 * 24));	
								$count1 = floor($gps_day/30)+1;

								if($gps_day % 30 == 0){
									$count1=$count1-1;
								}
														
								$gps_rent_charge = $count1 * 380;
														
								if($gps_rent_charge>1880){
									$gps_rent_charge=1880;
								}
							$gps_validity_date_new = date('Y-m-d', strtotime('+30 day', strtotime(date("Y-m-d"))));;	
						}
						else
						{
							$gps_rent_charge = 0;
						}
					}
				}
			}
		}
	}
	else if(numRows($get_gps_data)>1)
	{
		echo "<script>
			alert('Multiple GPS devices active on this vehicle !!')
			window.close();
		</script>";
		exit();
	}
	else
	{	
		$row_last_record = fetchArray($get_gps_data);
		
		$gps_record_id = $row_last_record['id'];
		$gps_device_no = $row_last_record['device_imei'];
		$gps_fix_attach = $row_last_record['fix_attach'];
	
			if($row_last_record['pending_charges']=="1")
			{
				if($row_last_record['fix_attach']=="Fix"){
					$gps_rent_charge = 1880;
				}
				else{
					$gps_rent_charge = 380;
				}
				
				$update_pending_charges="YES";
			}
			else
			{
				if($row_last_record['fix_attach']=="Fix")
				{
					$gps_rent_charge = 0;
				}
				else
				{		
					if($row_last_record['current_validity']==0 || $row_last_record['current_validity']==""){
						$gps_validity_date = $row_last_record['validity_end'];
					}
					else{
						$gps_validity_date = $row_last_record['current_validity'];
					}
					
					if($gps_validity_date==0 || $gps_validity_date=="")
					{
						echo "<script>
							alert('Warning : GPS validity date is not valid.');
							window.close();	
						</script>";
						exit();
					}
				
					if(strtotime(date("Y-m-d"))>strtotime($gps_validity_date))
					{
						$gps_day = round((strtotime(date("Y-m-d")) - strtotime($gps_validity_date)) / (60 * 60 * 24));	
							$count1 = floor($gps_day/30)+1;

							if($gps_day % 30 == 0){
								$count1=$count1-1;
							}
													
							$gps_rent_charge = $count1 * 380;
													
							if($gps_rent_charge>1880){
								$gps_rent_charge=1880;
							}
							
						$gps_validity_date_new = date('Y-m-d', strtotime('+30 day', strtotime(date("Y-m-d"))));;	
						$update_gps_validity="YES";		
					}
					else
					{
						$gps_rent_charge = 0;
					}
				}
			}
	}
}
	

// gps charges verify ends

if($gps_rent_charge!=$gps)
{
	echo "<script>
		alert('GPS Rent Charge not verified. Value should be : $gps_rent_charge.');
		document.getElementById('RefreshForm').submit();
	</script>";
	exit();
}

if($rtgs_payment=='1')
{
	$rtgs_amount = escapeString($conn,strtoupper($_POST['rtgs_amount']));
	
	if($rtgs_amount<=0)
	{
		echo "<script>
			alert('RTGS Amount is not valid !');
			$('#loadicon').hide();
			$('#balance_submit').attr('disabled', false);
		</script>";
		exit();
	}
	
	if($branch=='PEN')
	{
		$ac_holder = trim(escapeString($conn,strtoupper($_POST['holder'])));
		$ac_no =  trim(escapeString($conn,strtoupper($_POST['acno'])));
		$bank_name =  trim(escapeString($conn,strtoupper($_POST['bank'])));
		$ifsc_code =  trim(escapeString($conn,strtoupper($_POST['ifsc'])));
		
		if($balance_to=='OWNER')
		{
			$ac_holder_db = escapeString($conn,strtoupper($row_owner['acname']));
			$ac_no_db = escapeString($conn,strtoupper($row_owner['accno']));
			$bank_name_db = escapeString($conn,strtoupper($row_owner['bank']));
			$ifsc_code_db = escapeString($conn,strtoupper($row_owner['ifsc']));
		}
		else if($balance_to=='BROKER')
		{
			$ac_holder_db = escapeString($conn,strtoupper($row_broker['acname']));
			$ac_no_db = escapeString($conn,strtoupper($row_broker['accno']));
			$bank_name_db = escapeString($conn,strtoupper($row_broker['bank']));
			$ifsc_code_db = escapeString($conn,strtoupper($row_broker['ifsc']));
		}
		else
		{
			echo "<script>
				alert('Invalid payment mode selected.');
				document.getElementById('RefreshForm').submit();
			</script>";
			exit();
		}
		
		if($ac_holder!=$ac_holder_db || $ac_no!=$ac_no_db || $bank_name!=$bank_name_db || $ifsc_code!=$ifsc_code_db)
		{
			$update_ac = "YES";
		}
		else
		{
			$update_ac = "NO";
		}	
		
		if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $ac_holder))
		{
			echo "<script>
				alert('Error : Check Ac holder name !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ac_no))
		{
			echo "<script>
				alert('Error : Check Ac Number !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-z A-Z\.]*$/', $bank_name))
		{
			echo "<script>
				alert('Error : Check Bank Name !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ifsc_code))
		{
			echo "<script>
				alert('Error : Check IFSC Code !');
				$('#loadicon').hide();
				$('#btn_submit').attr('disabled', false);
			</script>";
			exit();
		}
		
		$open_account="1";
	}
	else
	{
		$open_account="0";
		
		if($balance_to=='OWNER')
		{
			$ac_holder = escapeString($conn,strtoupper($row_owner['acname']));
			$ac_no = escapeString($conn,strtoupper($row_owner['accno']));
			$bank_name = escapeString($conn,strtoupper($row_owner['bank']));
			$ifsc_code = escapeString($conn,strtoupper($row_owner['ifsc']));
		}
		else if($balance_to=='BROKER')
		{
			$ac_holder = escapeString($conn,strtoupper($row_broker['acname']));
			$ac_no = escapeString($conn,strtoupper($row_broker['accno']));
			$bank_name = escapeString($conn,strtoupper($row_broker['bank']));
			$ifsc_code = escapeString($conn,strtoupper($row_broker['ifsc']));
		}
		else
		{
			echo "<script>
				alert('Invalid payment mode selected.');
				document.getElementById('RefreshForm').submit();
			</script>";
			exit();
		}
		
		if($ac_no=='')
		{
			$ac_holder = trim(escapeString($conn,strtoupper($_POST['holder'])));
			$ac_no =  trim(escapeString($conn,strtoupper($_POST['acno'])));
			$bank_name =  trim(escapeString($conn,strtoupper($_POST['bank'])));
			$ifsc_code =  trim(escapeString($conn,strtoupper($_POST['ifsc'])));
		
			$update_ac = "YES";
			
			if(!preg_match('/^[a-z, A-Z-0-9\.]*$/', $ac_holder))
			{
				echo "<script>
					alert('Error : Check Ac holder name !');
					$('#loadicon').hide();
					$('#btn_submit').attr('disabled', false);
				</script>";
				exit();
			}
			
			if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ac_no))
			{
				echo "<script>
					alert('Error : Check Ac Number !');
					$('#loadicon').hide();
					$('#btn_submit').attr('disabled', false);
				</script>";
				exit();
			}
			
			if(!preg_match('/^[a-z A-Z\.]*$/', $bank_name))
			{
				echo "<script>
					alert('Error : Check Bank Name !');
					$('#loadicon').hide();
					$('#btn_submit').attr('disabled', false);
				</script>";
				exit();
			}
			
			if(!preg_match('/^[a-zA-Z0-9\.]*$/', $ifsc_code))
			{
				echo "<script>
					alert('Error : Check IFSC Code !');
					$('#loadicon').hide();
					$('#btn_submit').attr('disabled', false);
				</script>";
				exit();
			}
		}
		else
		{
			$update_ac = "NO";
		}
	}

		if($ac_no=='')
		{
			echo "<script>
				alert('Account details not found.');
				document.getElementById('RefreshForm').submit();
			</script>";
			exit();
		}
	
	
	if($update_ac=='YES')
	{
		if($balance_to=='OWNER')
		{
			$update_ac_details = Qry($conn,"UPDATE mk_truck SET bank='$bank_name',acname='$ac_holder',accno='$ac_no',ifsc='$ifsc_code' 
			WHERE id='$oid'");
			
			$ac_for=$oid;
		}
		else
		{
			$update_ac_details = Qry($conn,"UPDATE mk_broker SET bank='$bank_name',acname='$ac_holder',accno='$ac_no',ifsc='$ifsc_code' 
			WHERE id='$bid'");
			
			$ac_for=$bid;
		}
		
		if(!$update_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
		}
		
		$update_ac_add_log = Qry($conn,"INSERT INTO ac_update(o_b,ac_for,at_the_time_of,acname,acno,bank,ifsc,branch,
		branch_user,timestamp) VALUES ('$balance_to','$ac_for','BALANCE','$ac_holder','$ac_no','$bank_name','$ifsc_code',
		'$branch','$branch_sub_user','$timestamp')");
		
		if(!$update_ac_add_log){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
	}
}
else
{
	$rtgs_amount = 0;
}

if($total_balance!=($cash+$cheque_amount+$diesel_fm_amount+$rtgs_amount))
{
	echo "<script>
		alert('Please check balance amount !');
		document.getElementById('RefreshForm').submit();
	</script>";
	exit();	
}
	
$get_cash_balance = Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");

if(!$get_cash_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_cash_balance)==0){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Branch not found.","./");
	exit();
}
		
$row_cash_bal = fetchArray($get_cash_balance);
		
$email = $row_cash_bal['email'];
$rrpl_cash = $row_cash_bal['balance'];
$rr_cash = $row_cash_bal['balance2'];

if($company=='RRPL')
{
	$debit_col='debit';		
	$balance_col='balance';		
}
else if($company=="RAMAN_ROADWAYS")
{
	$debit_col='debit2';		
	$balance_col='balance2';			
}
else
{
	echo "<script type='text/javascript'>
			alert('Unable to fetch company.');
			document.getElementById('RefreshForm').submit();
		</script>"; 
	exit(); 
}		
	
if($cash>0)
{
	if($company=='RRPL' && $rrpl_cash>=$cash)
	{
		$new_cash = $rrpl_cash - $cash;
	}
	else if($company=="RAMAN_ROADWAYS" && $rr_cash>=$cash)
	{
		$new_cash = $rr_cash - $cash;
	}
	else
	{
		echo "<script>
			alert('UNSUFFICIENT BALANCE in $company.');
			document.getElementById('RefreshForm').submit();
		</script>";
		exit();
	}
}	

StartCommit($conn);
$flag = true;
$rtgs_flag = false;	

if($cash>0)
{
	$update_main_cash = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$cash' WHERE username='$branch'");
	
	if(!$update_main_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Balance not updated. Branch: $branch. VouNo: $vou_no. Cash Amount: $cash.",$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,lrno,`$debit_col`,`$balance_col`,timestamp) 
VALUES ('$branch','$date','$vou_date','$company','$vou_no','Freight_Memo','Balance Cash Amount','$db_lrno','$cash','$new_cash','$timestamp')");
		
	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Cash not inserted. Branch: $branch. VouNo: $vou_no. Cash Amount: $cash.",$conn,$page_name,__LINE__);
	}
}
	
if($cheque_amount>0)
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,lrno,chq_no,`$debit_col`,timestamp) 
	VALUES ('$branch','$vou_no','$date','$vou_date','$company','Freight_Memo','Balance Cheque Amount','$db_lrno','$cheque_no','$cheque_amount',
	'$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Passbook entry not inserted. Branch: $branch. VouNo: $vou_no. Chq Amount: $cheque_amount.",$conn,$page_name,__LINE__);
	}

	$insert_cheque = Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,pay_type,vou_date,lrno,amount,cheq_no,date,branch,timestamp) 
	VALUES ('$vou_no','Freight_Memo','BAL','$vou_date','$db_lrno','$cheque_amount','$cheque_no','$date','$branch','$timestamp')");
		
	if(!$insert_cheque){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Cheque entry not inserted. Branch: $branch. VouNo: $vou_no. Chq Amount: $cheque_amount.",$conn,$page_name,__LINE__);
	}
}	
 
if($rtgs_amount>0)
{	
	$insert_passbook_rtgs = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,lrno,`$debit_col`,timestamp) 
	VALUES ('$branch','$vou_no','$date','$vou_date','$company','Freight_Memo','Balance NEFT/RTGS Amount','$db_lrno','$rtgs_amount',
	'$timestamp')");
	
	if(!$insert_passbook_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Passbook entry not inserted. Branch: $branch. VouNo: $vou_no. Rtgs amount: $rtgs_amount.",$conn,$page_name,__LINE__);
	}	
	
	$updateTodayRtgs = Qry($conn,"UPDATE today_data SET neft=neft+'1',neft_amount=neft_amount+'$rtgs_amount' WHERE branch='$branch'");
	
	if(!$updateTodayRtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$rtgs_bal_set="0";
}
else
{
	$rtgs_bal_set="1";
} 

if($diesel_entry>0)
{
	$insert_new_diesel = Qry($conn,"INSERT INTO diesel_fm(branch,fno,com,qty,rate,disamt,tno,lrno,type,dsl_by,dcard,veh_no,dcom,dsl_nrr,
	pay_date,fm_date,crossing,done,done_time,ril_card,dsl_mobileno,timestamp,stockid) SELECT branch,frno,'$company',qty,rate,amount,'$truck_no',
	'$db_lrno','BALANCE',card_pump,card_no,phy_card,fuel_comp,dsl_nrr,date,'$vou_date','0',done,done_time,ril_card,mobile_no,
	'$timestamp',stockid FROM diesel_sample WHERE frno='$vou_no' AND type='BAL' AND branch='$branch'");
	
	if(!$insert_new_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Diesel Record Copy Failed. Branch: $branch. VouNo: $vou_no. Diesel Amount: $diesel_entry.",$conn,$page_name,__LINE__);
	}
	
	$get_ril_amount = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$vou_no' AND ril_card='1' AND type='BAL'");
	
	if(!$get_ril_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_ril_amount)>0)
	{
		while($row_ril2 = fetchArray($get_ril_amount))
		{
			$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
			cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
			
			if(!$update_ril_stock){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$delete_diesel = Qry($conn,"DELETE FROM diesel_sample WHERE frno='$vou_no' AND type='BAL' AND branch='$branch'");
	
	if(!$delete_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_diesel_info = Qry($conn,"SELECT id,branch,disamt,tno,dsl_by,dcard,veh_no,dcom,timestamp,dsl_mobileno FROM diesel_fm 
	WHERE timestamp='$timestamp' AND fno='$vou_no' AND type='BALANCE' AND dsl_by='CARD'");
	
	if(!$get_diesel_info){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_diesel_info)>0)
	{
		while($row_log=fetchArray($get_diesel_info))
		{
			if($row_log['dsl_by']=='CARD')
			{
				$card_live_bal = Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_log[dcard]'");
				if(!$card_live_bal){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$row_card_bal=fetchArray($card_live_bal); 
				$live_bal = $row_card_bal['balance']; 

				$qry_log = Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,dslcomp,dsltype,
				cardno,vehno,cardbal,amount,mobileno,reqid) VALUES ('$row_log[branch]','SUCCESS','New Request Added','$row_log[timestamp]',
				'MARKET','$row_log[tno]','$row_log[dcom]','CARD','$row_log[dcard]','$row_log[veh_no]','$live_bal','$row_log[disamt]',
				'$row_log[dsl_mobileno]','$row_log[id]')");
				
				if(!$qry_log){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
		}
	}
	
	$updateTodayDiesel = Qry($conn,"UPDATE today_data SET diesel_qty_market=ROUND(diesel_qty_market+'$diesel_Qty',2),
	diesel_amount_market=diesel_amount_market+'$diesel_entry' WHERE branch='$branch'");
	
	if(!$updateTodayDiesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$img = $signature; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
file_put_contents('../sign/fm/bal/'.$vou_no.'.png',$data);

$bal_sign="sign/fm/bal/$vou_no.png";		

$update_freight_memo=Qry($conn,"UPDATE freight_form SET unloadd='$unloading',detention='$detention',gps_deposit_return='$gps_deposit_return',
gps_rent='$gps',gps_device_charge='$gps_device_charge',bal_tds='$tds',otherfr='$other',claim='$claim',late_pod='$late_pod',totalbal='$total_balance',
bal_branch_user='$branch_sub_user',paidto='$balance_to',bal_date='$date',pto_bal_name='$adv_party',bal_pan='$adv_party_pan',
paycash='$cash',paycheq='$cheque_amount',paycheqno='$cheque_no',paydsl='$diesel_entry',newrtgsamt='$rtgs_amount',rtgs_bal='$rtgs_bal_set',
narra='$narration',sign_bal='$bal_sign',pod='1',branch_bal='$branch' WHERE frno='$vou_no'");

if(!$update_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($claim>0)
{
	$get_claim = Qry($conn,"SELECT claim_vou_no,vou_no,lrno,amount FROM claim_cache WHERE vou_no='$vou_no' AND adv_bal='BAL'");
	if(!$get_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_claim)==0)
	{
		$flag = false;
		errorLog("Claim not found. Vou: $vou_no.",$conn,$page_name,__LINE__);
	}
	
	while($row_claim = fetchArray($get_claim))
	{
		$get_claim_balance = Qry($conn,"SELECT id,pod_id,lrno,balance FROM claim_book_trans WHERE vou_no='$row_claim[claim_vou_no]' 
		AND lrno='$row_claim[lrno]' ORDER BY id DESC LIMIT 1");
		
		if(!$get_claim_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$row_claim_bal = fetchArray($get_claim_balance);
		$new_claim_bal = $row_claim_bal['balance']-$row_claim['amount'];
		$pod_id = $row_claim_bal['pod_id'];
		
		$insert_claim = Qry($conn,"INSERT INTO claim_book_trans(pod_id,lrno,vehicle_type,vou_no,trans_id,branch,branch_user,
		date,debit,balance,timestamp) VALUES ('$pod_id','$row_claim[lrno]','MARKET','$row_claim[claim_vou_no]','BALANCE','$branch',
		'$branch_sub_user','$date','$row_claim[amount]','$new_claim_bal','$timestamp')");
		
		if(!$insert_claim){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$copy_claim = Qry($conn,"INSERT INTO claim_book(pod_id,vou_no,lrno,debit,adv_bal,branch,timestamp) SELECT '$pod_id',vou_no,lrno,
	amount,adv_bal,'$branch','$timestamp' FROM claim_cache WHERE vou_no='$vou_no' AND adv_bal='BAL'");
	
	if(!$copy_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_claim_cache = Qry($conn,"DELETE FROM claim_cache WHERE vou_no='$vou_no' AND adv_bal='BAL'");
	if(!$delete_claim_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_table = Qry($conn,"UPDATE rcv_pod_free SET done='1' WHERE frno='$vou_no'");

if(!$update_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($total_balance>0)
{
	$updateTodayAdv = Qry($conn,"UPDATE today_data SET fm_bal=fm_bal+'1',fm_bal_amount=fm_bal_amount+'$total_balance' WHERE branch='$branch'");
	
	if(!$updateTodayAdv){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$updateBalPending = Qry($conn,"UPDATE _pending_lr SET bal_pending=bal_pending-1 WHERE branch='$row_fm_bal[branch]'");
if(!$updateBalPending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($rtgs_amount>0)
{
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	
$chk_for_crn = Qry($conn,"SELECT id FROM rtgs_fm WHERE crn='$crnnew'");	
if(!$chk_for_crn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($chk_for_crn)>0)
{
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
}		

$chk_for_rtgs = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$vou_no' AND type='BALANCE'");	
if(!$chk_for_rtgs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_for_rtgs)==0)
{
$insert_rtgs=Qry($conn,"INSERT INTO rtgs_fm (fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,lrno,tno,type,
mobile,email,crn,timestamp) VALUES ('$vou_no','$branch','$company','$db_af','$rtgs_amount','$ac_holder','$ac_no','$bank_name','$ifsc_code',
'$adv_party_pan','$date','$vou_date','$db_lrno','$truck_no','BALANCE','$adv_party_mobile','$email','$crnnew','$timestamp')");
	
	if(!$insert_rtgs){
		$flag = false;
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("RTGS entry not inserted. Branch: $branch. VouNo: $vou_no. RTGS Amount: $rtgs_amount.",$conn,$page_name,__LINE__);
	}	
}

}

// insert gps charges	

if($gps_rent_charge>0)
{
	$fids = $vou_no;
	
	$insert_gps_charges = Qry($conn,"INSERT INTO gps_amount_deducted (device_imei,vehicle_no,vou_no,adv_bal,gps_charges,
	deduct_type,branch,timestamp) VALUES ('$gps_device_no','$truck_no','$fids','2','$gps_rent_charge','$gps_fix_attach',
	'$branch','$timestamp')");
		
	if(!$insert_gps_charges){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($update_pending_charges=='YES')
	{
		$mark_gps_installed=Qry($conn,"UPDATE gps_device_inventory SET pending_charges='0' WHERE id='$gps_record_id'");
		
		if(!$mark_gps_installed){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$device_use_type = "New";
	}
	else
	{
		$device_use_type = "Re-use";
	}
	
	$insert_gps_log = Qry($conn,"INSERT INTO gps_device_log (vou_no,adv_bal,use_type,imei_no,tno,fix_attach,branch,timestamp) 
	VALUES ('$fids','BALANCE','$device_use_type','$gps_device_no','$truck_no','$gps_fix_attach','$branch','$timestamp')");
			
	if(!$insert_gps_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($update_gps_validity=='YES')
	{
		$update_new_validity = Qry($conn,"UPDATE gps_device_inventory SET current_validity='$gps_validity_date_new' 
		WHERE id='$gps_record_id'");
		
		if(!$update_new_validity){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$get_insert_frt_party = Qry($conn,"SELECT id FROM _party_freight_db WHERE (party_pan='$owner_pan_no' || party_pan='$broker_pan_no')");
		
if(!$get_insert_frt_party){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($get_insert_frt_party)>0)
{
	$row_insert_frt = fetchArray($get_insert_frt_party);
	$total_added_amt = $unloading+$detention;
	
	$update_freight_amt = Qry($conn,"UPDATE _party_freight_db SET freight=freight+'$total_added_amt',
	loading_detention=loading_detention+'$total_added_amt' WHERE id='$row_insert_frt[id]'");
		
	if(!$update_freight_amt){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($mark_balance_done=='YES')
{
	$update_balance_done = Qry($conn,"UPDATE gps_charges_pending SET balance_done='1' WHERE vou_no='$vou_no'");
		
	if(!$update_balance_done){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

// insert gps charges ends	

echo "
<form action='rcv_pod_full.php' id='myFormadv' method='POST'>
	<input type='hidden' value='$vou_no' name='idmemo'>
</form>";

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Freight Balance Submitted..');
		document.getElementById('myFormadv').submit();
	</script>"; 
	exit();
}  
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#balance_submit').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#balance_submit').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
?>