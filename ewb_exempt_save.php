<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$lr_date = escapeString($conn,($_POST['lr_date']));
$narration = escapeString($conn,($_POST['narration']));

$chk_record = Qry($conn,"SELECT id FROM _eway_bill_free WHERE lrno='$lrno'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit(); 
}

if(numRows($chk_record)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO _eway_bill_free(lrno,lr_date,narration,req_branch,req_branch_user,req_pending,timestamp) VALUES 
('$lrno','$lr_date','$narration','$branch','$branch_sub_user','0','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./ewb_exempt.php';
	</script>";
	exit();
?>