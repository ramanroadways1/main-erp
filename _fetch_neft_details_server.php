<?php
require_once 'connection.php';

$type =  escapeString($conn,($_REQUEST['type'])); 
$pan_no =  escapeString($conn,strtoupper($_REQUEST['pan_no'])); 
$from_date =  escapeString($conn,strtoupper($_REQUEST['from_date']));
$to_date =  escapeString($conn,strtoupper($_REQUEST['to_date']));

if($type=='BROKER')
{
	$id_col="bid";
}
else
{
	$id_col="oid";
}

$sql ="SELECT id,fno,fm_date,pay_date,com,amount,acname,acno,ifsc,pan,type,tno,crn FROM rtgs_fm WHERE pay_date BETWEEN '$from_date' AND 
'$to_date' AND pan='$pan_no' ORDER BY id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
   array( 'db' => 'fno', 'dt' => 0),
    array( 'db' => 'fm_date', 'dt' => 1),
    array( 'db' => 'com', 'dt' => 2),
    array( 'db' => 'tno', 'dt' => 3),
    array( 'db' => 'amount', 'dt' => 4),
    array( 'db' => 'acname', 'dt' => 5), 
    array( 'db' => 'acno', 'dt' => 6), 
    array( 'db' => 'ifsc', 'dt' => 7), 
    array( 'db' => 'pan', 'dt' => 8), 
    array( 'db' => 'type', 'dt' => 9), 
    array( 'db' => 'crn', 'dt' => 10), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);