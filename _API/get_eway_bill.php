<?php
require_once("../connection.php");

$timestamp = date("Y-m-d H:i:s");

$ewbNo= escapeString($conn,$_POST['ewb_no']);
$consignor_id= escapeString($conn,$_POST['consignor_id']);
$consignee_id= escapeString($conn,$_POST['consignee_id']);
$lr_date= escapeString($conn,$_POST['lr_date']);

if($branch=='KORBA')
{
	$lr_type= escapeString($conn,$_POST['lr_type']);
	
	if($lr_type=='MARKET'){
		$tno = escapeString($conn,$_POST['tno_market']);
	}
	else{
		$tno =  escapeString($conn,$_POST['tno_own']);
	}
}
else
{
	$tno = $_SESSION['verify_vehicle'];
}

$item_id= escapeString($conn,$_POST['item_id']);
$company= escapeString($conn,$_POST['company']);
$invoice_value= escapeString($conn,$_POST['invoice_value']);
$consignor_sub_gst= escapeString($conn,$_POST['consignor_sub_gst']);

if($lr_date=='')
{
	echo "<script>
		alert('Select LR date first !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($tno=='')
{
	echo "<script>
		alert('Enter vehicle number first !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($consignor_id=='')
{
	echo "<script>
		alert('Select consignor first !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($consignee_id=='')
{
	echo "<script>
		alert('Select consignee first !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($invoice_value=='')
{
	echo "<script>
		alert('Enter invoice value first !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($company!='RRPL' AND $company!='RAMAN_ROADWAYS')
{
	echo "<script>
		alert('Company not found !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($ewbNo)!=12 AND $invoice_value>=50000)
{
	echo "<script>
		alert('Please check eway bill number !');
		$('#EwayBillFlag').val('');
		$('#eway_bill_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// if($consignor_id=='56' || $consignor_id=='675' || $consignor_id=='158')
// {
	// echo "<script>
	// $('#consignor').attr('readonly',true); 
	// $('#consignee').attr('readonly',true); 
	// $('#lr_date').attr('readonly',true); 
	// $('#from').attr('readonly',true); 
	// $('#to').attr('readonly',true); 
	
		// $('#EwayBillFlag').val('2');
		// $('#EwayBillFlag_desc').val('Consignor Exempt');
		// $('#EwayBill_con1_gst').val('');
		// $('#EwayBill_con1').val('');
		// $('#EwayBill_con2_gst').val('');
		// $('#EwayBill_con2').val('');
		// $('#EwayBill_tno').val('');
		
		// $('#EwayBill_ewb_no').val('$ewbNo');
		// $('#EwayBill_ewayBillDate').val('');
		// $('#EwayBill_docNo').val('');
		// $('#EwayBill_docDate').val('');
		// $('#EwayBill_fromPincode').val('');
		// $('#EwayBill_totInvValue').val('');
		// $('#EwayBill_totalValue').val('');
		// $('#EwayBill_toPincode').val('');
		
		// $('#EwayBill_bypass').val('0');
		// $('#by_pass_narr').val('0'); 
		// $('#EwayBill_copy_div').hide();
		// $('#EwayBill_copy_file').attr('required',false);
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
// }

if($item_id=='101') // SALT LRs
{
	echo "<script>
	$('#consignor').attr('readonly',true); 
	$('#consignee').attr('readonly',true); 
	$('#lr_date').attr('readonly',true); 
	$('#from').attr('readonly',true); 
	$('#to').attr('readonly',true); 
	
		$('#EwayBillFlag').val('2'); 
		$('#EwayBillFlag_desc').val('Salt Exempt'); 
		$('#EwayBill_con1_gst').val('');
		$('#EwayBill_con1').val('');
		$('#EwayBill_con2_gst').val('');
		$('#EwayBill_con2').val('');
		$('#EwayBill_tno').val('');
		
		$('#EwayBill_ewb_no').val('$ewbNo');
		$('#EwayBill_ewayBillDate').val('');
		$('#EwayBill_docNo').val('');
		$('#EwayBill_docDate').val('');
		$('#EwayBill_fromPincode').val('');
		$('#EwayBill_totInvValue').val('');
		$('#EwayBill_totalValue').val('');
		$('#EwayBill_toPincode').val('');
		
		$('#EwayBill_bypass').val('0');
		$('#by_pass_narr').val('0');
		$('#EwayBill_copy_div').hide();
		$('#EwayBill_copy_file').attr('required',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// Comment this after server or other issue resolved

$get_con1_gst = Qry($conn,"SELECT gst FROM consignor WHERE id='$consignor_id' AND hide!='1'");

if(!$get_con1_gst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$get_con2_gst = Qry($conn,"SELECT gst FROM consignee WHERE id='$consignee_id' AND hide!='1'");

if(!$get_con2_gst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($get_con1_gst)==0)
{
	echo "<script>
		alert('Error : Consignor not found !');
		$('#eway_bill_no').val('');
		$('#EwayBillFlag').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(numRows($get_con2_gst)==0)
{
	echo "<script>
		alert('Error : Consignee not found !');
		$('#eway_bill_no').val('');
		$('#EwayBillFlag').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$row_con1 = fetchArray($get_con1_gst);
$row_con2 = fetchArray($get_con2_gst);

$con1_gst = $row_con1['gst'];
$con2_gst = $row_con2['gst'];

$check_by_pass=Qry($conn,"SELECT id,narration FROM _by_pass_ewb WHERE is_active='1'");

if(!$check_by_pass){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($check_by_pass)>0)
{
$row_by_pass = fetchArray($check_by_pass);

echo "<script>
	$('#consignor').attr('readonly',true); 
	$('#consignee').attr('readonly',true); 
	$('#lr_date').attr('readonly',true); 
	$('#from').attr('readonly',true); 
	$('#to').attr('readonly',true); 
	
		$('#EwayBillFlag').val('2');
		$('#EwayBillFlag_desc').val('ByPass');
		$('#EwayBill_con1_gst').val('');
		$('#EwayBill_con1').val('');
		$('#EwayBill_con2_gst').val('');
		$('#EwayBill_con2').val('');
		$('#EwayBill_tno').val(''); 
		$('#EwayBill_bypass').val('1');
		$('#by_pass_narr').val('$row_by_pass[narration]');
		$('#by_pass_id').val('$row_by_pass[id]');
		
		$('#EwayBill_ewb_no').val('$ewbNo');
		$('#EwayBill_ewayBillDate').val('');
		$('#EwayBill_docNo').val('');
		$('#EwayBill_docDate').val('');
		$('#EwayBill_fromPincode').val('');
		$('#EwayBill_totInvValue').val('');
		$('#EwayBill_totalValue').val('');
		$('#EwayBill_toPincode').val('');
		
		$('#EwayBill_copy_div').hide();
		$('#EwayBill_copy_file').attr('required',false);
		$('#loadicon').fadeOut('slow');
	</script>";
exit();
}

// Comment this after server resolved

function url_test( $url ) {
	$timeout = 10;
	$ch = curl_init();
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
	$http_respond = curl_exec($ch);
	$http_respond = trim( strip_tags( $http_respond ) );
	$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	if ( ( $http_code == "200" ) || ( $http_code == "302" ) ) {
		return true;
	} else {
		// you can return $http_code here if necessary or wanted
		return false;
	}
	curl_close( $ch );
}

$website = "https://api.taxprogsp.co.in";

// if(!url_test( $website ))
// {
	// echo "<script>
		// alert('Error: e-way bill server down. Please try after some time !!');
		// $('#EwayBillFlag').val('');
		// $('#eway_bill_no').val('');
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
// }

// else
// {
	// echo $website ." functions correctly.";
// }

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");

if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$row_token = fetchArray($get_token);
$authToken = $row_token['token'];

if($company=='RRPL')
{
	$gst_no_company=$rrpl_gst_no;
	$gst_username=$ewb_rrpl_username;
	$gst_password=$ewb_rrpl_password;
}
else
{
	$gst_no_company=$rr_gst_no;
	$gst_username=$ewb_rr_username;
	$gst_password=$ewb_rr_password;
}

include("../../_ewb_functions.php");

$ewb_result = GetEwayBill($conn,$tax_pro_asp_id,$gst_username,$tax_pro_asp_password,$gst_no_company,$gst_password,$company,$ewbNo,$branch,$branch_sub_user,$authToken);

if($ewb_result[0]=="ERROR")
{
	$ewb_error = $ewb_result[1];
		
	echo "<script>
		alert('Error : $ewb_error !');
		$('#eway_bill_no').val('');
		$('#EwayBillFlag').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	$tno_ewb = $ewb_result[1];
	$ewb_id = $ewb_result[2];
	$expiry_date = $ewb_result[3];
	$lr_date_ewb = $ewb_result[4];
	$con1_gst_ewb = $ewb_result[5];
	$con2_gst_ewb = $ewb_result[6];
	$con1_ewb = $ewb_result[7];
	$con2_ewb = $ewb_result[8];
	$from_pincode_ewb = $ewb_result[9];
	$to_pincode_ewb = $ewb_result[10];
	$doc_no_ewb = $ewb_result[11];
	$doc_date_ewb = $ewb_result[12];
	$ewb_date = $ewb_result[13];
	$ewb_date2 = $ewb_result[14];
	$ext_times_ewb = $ewb_result[15];
	$goods_value_ewb = $ewb_result[16];
	$inv_value_ewb = $ewb_result[17];
	$exp_date_timestamp = $ewb_result[18];
}

if(empty($tno_ewb))
{
	echo "<script>
		alert('Error : something went wrong !');
		$('#eway_bill_no').val('');
		$('#EwayBillFlag').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
	
// $check_ewb = Qry($conn,"SELECT tno,ewb_date,date(exp_date) as exp_date,doc_type,doc_no,doc_date,con1_gst,con1_name,from_pincode,con2_gst,con2_name,to_pincode,
// goods_value,invoice_value,extended_times FROM ewb_db WHERE ewb_no='$ewbNo' ORDER BY id DESC LIMIT 1");

// if(!$check_ewb){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","../");
	// exit();
// }

// $row_ewb = fetchArray($check_ewb);
	
// $tno_ewb = $row_ewb['tno'];
// $expiry_date = $row_ewb['exp_date'];
// $lr_date_ewb = $row_ewb['doc_date'];
// $con1_gst_ewb = $row_ewb['con1_gst'];
// $con2_gst_ewb = $row_ewb['con2_gst'];
// $con1_ewb = $row_ewb['con1_name']; 
// $con2_ewb = $row_ewb['con2_name'];
// $from_pincode_ewb = $row_ewb['from_pincode'];
// $to_pincode_ewb = $row_ewb['to_pincode'];
// $doc_no_ewb = $row_ewb['doc_no'];
// $doc_date_ewb = $row_ewb['doc_date'];
// $ewb_date = $row_ewb['ewb_date'];
// $ewb_date2 = $row_ewb['ewb_date'];
// $ext_times_ewb = $row_ewb['extended_times'];
// $goods_value_ewb = $row_ewb['goods_value'];
// $inv_value_ewb = $row_ewb['invoice_value'];
// $exp_date_timestamp = $row_ewb['exp_date'];


if($tno_ewb != $tno)
{
	echo "<script>
		alert('Error : Entered vehicle No: $tno not matching with e-way Bill: $tno_ewb !');
		$('#eway_bill_no').val('');
		$('#EwayBillFlag').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$ewb_date_verify = date("Y-m-d",strtotime($ewb_date2));
$ewb_date_verify_next = date('Y-m-d', strtotime("+1 day", strtotime($ewb_date_verify)));
$ewb_date_verify_prev = date('Y-m-d', strtotime("-1 day", strtotime($ewb_date_verify)));

if($lr_date!=$ewb_date_verify AND $lr_date!=$ewb_date_verify_next AND $lr_date!=$ewb_date_verify_prev)
	{
		echo "<script>
			alert('Error : LR date is not valid ! Ewb date : $ewb_date_verify.');
			$('#eway_bill_no').val('');
			$('#EwayBillFlag').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}


if($consignor_id=='56')
{
	if($con1_gst_ewb!=$consignor_sub_gst AND (substr($con1_gst_ewb,0,3)!='URP'))
	{
		echo "<script>
			alert('Error : Consignor GST: $consignor_sub_gst not matching with e-way Bill GST: $con1_gst_ewb !');
			$('#eway_bill_no').val('');
			$('#EwayBillFlag').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
else
{
	if($con1_gst_ewb!=$con1_gst AND (substr($con1_gst_ewb,0,3)!='URP'))
	{
		echo "<script>
			alert('Error : Consignor GST: $con1_gst not matching with e-way Bill GST: $con1_gst_ewb !');
			$('#eway_bill_no').val('');
			$('#EwayBillFlag').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}

if($consignor_id=='848' AND $con2_gst_ewb=='24AAAAK0203G1ZH')
{
	 // by pass consignee gst check for krishak bharti
}
else
{
	if($con2_gst_ewb!=$con2_gst AND (substr($con2_gst_ewb,0,3)!='URP'))
	{
		echo "<script>
			alert('Error : Consignee GST: $con2_gst not matching with e-way Bill GST: $con2_gst_ewb !');
			$('#eway_bill_no').val('');
			$('#EwayBillFlag').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
	
if(strtotime(date("Y-m-d"))>strtotime($expiry_date))
{
	$chk_allow_expired_ewb = Qry($conn,"SELECT id FROM _allow_lr_ewb_expired WHERE ewb_no='$ewbNo' AND branch='$branch'");
				
	if(!$chk_allow_expired_ewb){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","../");
		exit();
	}
		
	if(numRows($chk_allow_expired_ewb)==0)
	{		
		echo "<script>
			alert('Eway bill: $ewbNo expired on $expiry_date !');
			$('#eway_bill_no').val('');
			$('#EwayBillFlag').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
 
	echo "<script>
	$('#consignor').attr('readonly',true); 
	$('#consignee').attr('readonly',true); 
	$('#lr_date').attr('readonly',true); 
	$('#from').attr('readonly',true); 
	$('#to').attr('readonly',true); 
	
		$('#EwayBillFlag').val('1'); 
		$('#EwayBillFlag_desc').val('Success');
		$('#EwayBill_con1_gst').val('$con1_gst_ewb');
		$('#EwayBill_con1').val('$con1_ewb');
		$('#EwayBill_con2_gst').val('$con2_gst_ewb');
		$('#EwayBill_con2').val('$con2_ewb');
		$('#EwayBill_tno').val('$tno_ewb');
		
		$('#EwayBill_ewb_no').val('$ewbNo');
		$('#EwayBill_ewayBillDate').val('$ewb_date');
		$('#EwayBill_docNo').val('$doc_no_ewb');
		$('#EwayBill_docDate').val('$doc_date_ewb');
		$('#EwayBill_fromPincode').val('$from_pincode_ewb');
		$('#EwayBill_totInvValue').val('$inv_value_ewb');
		$('#EwayBill_totalValue').val('$goods_value_ewb');
		$('#EwayBill_toPincode').val('$to_pincode_ewb');
		
		$('#EwayBill_reg_date').val('$ewb_date2');
		$('#EwayBill_exp_date').val('$exp_date_timestamp');
		$('#EwayBill_extended').val('$ext_times_ewb');
		 
		$('#EwayBill_bypass').val('0');
		$('#by_pass_narr').val('0');
		$('#EwayBill_copy_div').hide();
		$('#EwayBill_copy_file').attr('required',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
?>	