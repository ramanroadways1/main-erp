<?php
require_once("../_connect.php");

$timestamp = date("Y-m-d H:i:s");
$oid = escapeString($conn,$_POST['oid']);
$did = escapeString($conn,$_POST['did']);
 
$get_vehicle = Qry($conn,"SELECT last_verify,tno FROM mk_truck WHERE id='$oid'");
	
if(!$get_vehicle){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($get_vehicle)==0){
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Warning !!!',
	text: 'Vehicle not found..'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

$get_driver = Qry($conn,"SELECT last_verify,pan as lic_no FROM mk_driver WHERE id='$did'");
	
if(!$get_driver){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($get_driver)==0){
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Warning !!!',
	text: 'Driver not found..'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

$row_owner = fetcchArray($get_vehicle);
$row_driver = fetcchArray($get_driver);
	
$tno = $row_owner['tno'];
$tno_last_verify = $row_owner['last_verify'];
$lic_no = $row_owner['lic_no'];
$lic_no_last_verify = $row_owner['last_verify'];

$hourdiff_tno = round((strtotime($timestamp) - strtotime($tno_last_verify))/3600, 1);
$hourdiff_driver = round((strtotime($timestamp) - strtotime($lic_no_last_verify))/3600, 1);

if($tno_last_verify==0 || $hourdiff_tno>36)
{
// echo  '<script>
				// Swal.fire({
				// title: "Success",
				// text: "RC already exist in RRPL System.",
				// icon: "warning"
				// }).then(function() {
					// window.location = "rc_api_print.php?id='.$rc.'";
				// })
				// </script>';
			// exit();
	
 

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://prod.aadhaarapi.com/verify-rc/v3",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>"{\r\n
	  	\"reg_no\": \"$rc\",\r\n
	  	\"consent\": \"Y\",\r\n
	  	\"consent_text\": \"Details fetched for $rc verification on behalf of RRPL. \"\r\n}",
	  CURLOPT_HTTPHEADER => array(
	    "qt_api_key: 74b6283a-c1b0-4199-a7e6-f2bc2f176b83",
	    "qt_agency_id: 80c497b4-bc77-42f5-8e9f-56ec2c4beb0c",
	    "Content-Type: application/json"
	  ),
	));

	$output = curl_exec($curl); 
	curl_close($curl); 


	// $output = '{"id":"10bb2c9a-2732-4d8f-8c03-7c226e9fceb1","env":2,"request_timestamp":"2020-11-03 15:12:45:325 +05:30","response_timestamp":"2020-11-03 15:12:47:642 +05:30","transaction_status":1,"result":{"rc_blacklist_status":"NA","rc_body_type_desc":"TILT_CAB","rc_chasi_no":"MAT548210J8N30112","rc_color":"NP","rc_cubic_cap":"3783.0","rc_eng_no":"497TC41KRY838160","rc_f_name":"LATE ALLA RAKHA","rc_financer":"INDUSIND BANK LTD","rc_fit_upto":"29-Mar-2021","rc_fuel_desc":"DIESEL","rc_gvw":"10400","rc_insurance_comp":"Magma HDI General Insurance Co. Ltd.","rc_insurance_policy_no":"P0020000100/4103/854485","rc_insurance_upto":"14-Mar-2021","rc_maker_desc":"TATA MOTORS LTD","rc_maker_model":"LPT 1010 CR X / 38WB BS IV","rc_manu_month_yr":"11/2018","rc_mobile_no":null,"rc_no_cyl":"4","rc_noc_details":"NA","rc_non_use_from":null,"rc_non_use_status":null,"rc_non_use_to":null,"rc_norms_desc":"BHARAT STAGE IV","rc_np_issued_by":"PAURI RTO","rc_np_no":"NP/UK/12/032020/90021","rc_np_upto":"02-Apr-2021","rc_owner_name":"NISAR AHMAD","rc_owner_sr":"1","rc_permanent_address":"NEAR RASIDYA MSAZID LAKRI PADAW , KOTDWARA,PAURI GARHWAL, Garhwal -246149","rc_permit_issue_dt":"03-Apr-2019","rc_permit_no":"UK/12/NP/MGV/2019/2","rc_permit_type":"National Permit[MEDIUM GOODS VEHICLE]","rc_permit_valid_from":"03-Apr-2019","rc_permit_valid_upto":"02-Apr-2024","rc_present_address":" NEAR RASIDYA MSAZID LAKRI PADAW , KOTDWARA,PAURI GARHWAL, Garhwal -246149","rc_pucc_no":"UP01401850004576","rc_pucc_upto":"17-Jan-2021","rc_registered_at":"KOTDWAR ARTO, Uttarakhand","rc_regn_dt":"30-Mar-2019","rc_regn_no":"UK15CA1998","rc_seat_cap":"2","rc_sleeper_cap":"0","rc_stand_cap":"0","rc_status":"ACTIVE","rc_status_as_on":"03-Nov-2020","rc_tax_upto":"30-Nov-2020","rc_unld_wt":"3955","rc_vch_catg":"MGV","rc_vh_class_desc":"Goods Carrier(MGV)","rc_wheelbase":"3800","state_cd":"UK","stautsMessage":"OK"},"response_msg":"Success","response_code":"101"}';

	// $output = '{ "statusCode": 401, "message": "Access Denied: IP is not white listed : 103.82.80.104" }';

	$response = json_decode($output, true);
	@$row = $response['result'];
	@$rowcode = $response['response_code'];
	   
	if($rowcode=="101")
	{ 
			@$rc_fit_upto = date_create_from_format("d-M-Y",$row['rc_fit_upto']);
			@$rc_insurance_upto = date_create_from_format("d-M-Y",$row['rc_insurance_upto']);
			@$rc_non_use_from = date_create_from_format("d-M-Y",$row['rc_non_use_from']);
			@$rc_non_use_to = date_create_from_format("d-M-Y",$row['rc_non_use_to']);
			@$rc_np_upto = date_create_from_format("d-M-Y",$row['rc_np_upto']);
			@$rc_permit_issue_dt = date_create_from_format("d-M-Y",$row['rc_permit_issue_dt']);
			@$rc_permit_valid_from = date_create_from_format("d-M-Y",$row['rc_permit_valid_from']);
			@$rc_permit_valid_upto = date_create_from_format("d-M-Y",$row['rc_permit_valid_upto']);
			@$rc_pucc_upto = date_create_from_format("d-M-Y",$row['rc_pucc_upto']);
			@$rc_regn_dt = date_create_from_format("d-M-Y",$row['rc_regn_dt']);
			@$rc_status_as_on = date_create_from_format("d-M-Y",$row['rc_status_as_on']);

			if($row['rc_tax_upto']!='LTT'){
				@$rc_tax_upto = date_format(date_create_from_format("d-M-Y",$row['rc_tax_upto']), "Y-m-d");
			} else {
				@$rc_tax_upto = 'LTT';
			}
			

			$insert_fetched_data_rc = Qry($conn,"INSERT INTO rrpl_database.zoop_rc (`request_id`, `request_timestamp`, 
			`rc_blacklist_status`, `rc_body_type_desc`, 
			`rc_chasi_no`, `rc_color`, `rc_cubic_cap`, `rc_eng_no`, `rc_f_name`, `rc_financer`, `rc_fit_upto`, `rc_fuel_desc`, `rc_gvw`, 
			`rc_insurance_comp`, `rc_insurance_policy_no`, `rc_insurance_upto`, `rc_maker_desc`, `rc_maker_model`, `rc_manu_month_yr`,
			`rc_mobile_no`, `rc_no_cyl`, `rc_noc_details`, `rc_non_use_from`, `rc_non_use_status`, `rc_non_use_to`, `rc_norms_desc`, 
			`rc_np_issued_by`, `rc_np_no`, `rc_np_upto`, `rc_owner_name`, `rc_owner_sr`, `rc_permanent_address`, `rc_permit_issue_dt`, 
			`rc_permit_no`, `rc_permit_type`, `rc_permit_valid_from`, `rc_permit_valid_upto`, `rc_present_address`, `rc_pucc_no`, 
			`rc_pucc_upto`, `rc_registered_at`, `rc_regn_dt`, `rc_regn_no`, `rc_seat_cap`, `rc_sleeper_cap`, `rc_stand_cap`, 
			`rc_status`, `rc_status_as_on`, `rc_tax_upto`, `rc_unld_wt`, `rc_vch_catg`, `rc_vh_class_desc`, `rc_wheelbase`, 
			`state_cd`, `branch`, `branch_user`) VALUES ('$response[id]', '$response[request_timestamp]', '$row[rc_blacklist_status]', 
			'$row[rc_body_type_desc]', '$row[rc_chasi_no]', '$row[rc_color]', '$row[rc_cubic_cap]', '$row[rc_eng_no]', '$row[rc_f_name]',
			'$row[rc_financer]', '".date_format($rc_fit_upto,"Y-m-d")."', '$row[rc_fuel_desc]', '$row[rc_gvw]', 
			'$row[rc_insurance_comp]', '$row[rc_insurance_policy_no]', '".date_format($rc_insurance_upto,"Y-m-d")."', 
			'$row[rc_maker_desc]', '$row[rc_maker_model]', '$row[rc_manu_month_yr]', '$row[rc_mobile_no]', '$row[rc_no_cyl]', 
			'$row[rc_noc_details]', '".date_format($rc_non_use_from, "Y-m-d")."', '$row[rc_non_use_status]', 
			'".date_format($rc_non_use_to, "Y-m-d")."', '$row[rc_norms_desc]', '$row[rc_np_issued_by]', '$row[rc_np_no]',
			'".date_format($rc_np_upto,"Y-m-d")."', '$row[rc_owner_name]', '$row[rc_owner_sr]', '$row[rc_permanent_address]',
			'".date_format($rc_permit_issue_dt,"Y-m-d")."', '$row[rc_permit_no]', '$row[rc_permit_type]',
			'".date_format($rc_permit_valid_from,"Y-m-d")."', '".date_format($rc_permit_valid_upto, "Y-m-d")."',
			'$row[rc_present_address]', '$row[rc_pucc_no]', '".date_format($rc_pucc_upto, "Y-m-d")."', '$row[rc_registered_at]',
			'".date_format($rc_regn_dt, "Y-m-d")."', '$row[rc_regn_no]', '$row[rc_seat_cap]', '$row[rc_sleeper_cap]', 
			'$row[rc_stand_cap]', '$row[rc_status]', '".date_format($rc_status_as_on, "Y-m-d")."', '$rc_tax_upto', 
			'$row[rc_unld_wt]', '$row[rc_vch_catg]', '$row[rc_vh_class_desc]', '$row[rc_wheelbase]', '$row[state_cd]', 
			'$branch_user', '$branch_sub_user')");

			if(!$insert_fetched_data_rc){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$udpate_new_timestamp = Qry($conn,"UPDATE mk_truck SET last_verify='$timestamp' WHERE id='$oid'");

			if(!$udpate_new_timestamp){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}

				// echo '<script>
				// Swal.fire({
				// title: "Success",
				// text: "RC Checked Successfully.",
				// icon: "success"
				// }).then(function() {
					// window.location = "rc_api_print.php?id='.$row["rc_regn_no"].'";
				// })
				// </script>';
				// exit();

	$rc_flag="1";	
		
	}
	else
	{
		$content = "Code: ".$response['statusCode']." ".$response['message'];
		$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
		echo "<script>Swal.fire({
				icon: 'error',
				title: 'API Error !!!',
				text: '$content'
			})
		$('#loadicon').hide();	
		</script>";
		exit();
	}
}
else
{
	$rc_flag="1";
}

echo "<script>Swal.fire({
				icon: 'success',
				title: 'success !!!',
				text: 'Verified'
			})
		$('#loadicon').hide();	
		</script>";
		exit();
?>