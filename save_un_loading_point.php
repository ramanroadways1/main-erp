<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$to_id = escapeString($conn,($_POST['to_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));
$own_truck_visited = escapeString($conn,($_POST['own_truck_visited']));
$loading_point = escapeString($conn,($_POST['loading_point']));
$loading_pincode = escapeString($conn,($_POST['loading_pincode']));
$label_name = escapeString($conn,strtoupper($_POST['label_name']));

if($own_truck_visited=='1')
{
	$own_truck_visited_var = "GPS";
	$tno_visited = escapeString($conn,$_POST['owo_tno']);
	$visit_date = escapeString($conn,$_POST['visit_date']);
}
else
{
	$own_truck_visited_var = "GOOGLE";
	$tno_visited="";
	$visit_date="";
}

if(!isset($_SESSION['unloading_point_type1']))
{
	echo "<script>alert('Invalid data parameters !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if($own_truck_visited_var != $_SESSION['unloading_point_type1'])
{
	echo "<script>alert('Invalid data parameters !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if($con2_id != $_SESSION['unloading_point_add_con1_id'])
{
	echo "<script>alert('Consignee not verified !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if($to_id != $_SESSION['unloading_point_add_from_id'])
{
	echo "<script>alert('Location not verified !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if($own_truck_visited_var=='GPS')
{
	$distance= $_SESSION['unloading_point_distance_km1'];
	$lat = $_SESSION['unloading_point_gps_lat'];
	$long = $_SESSION['unloading_point_gps_lng'];
	$pincode = $_SESSION['unloading_point_gps_pincode'];
}
else
{
	$distance = $_SESSION['unloading_point_distance_km1'];
	$pincode = $_SESSION['unloading_point_google_pincode'];
	$lat = $_SESSION['unloading_point_google_lat'];
	$long = $_SESSION['unloading_point_google_lng'];
}

if($lat=='')
{
	echo "<script>alert('Coordinates not found - 1 !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if($long=='')
{
	echo "<script>alert('Coordinates not found -2 !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

$chk_data = Qry($conn,"SELECT id FROM address_book_consignee WHERE to_id='$to_id' AND consignee='$con2_id'");

if(!$chk_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_data)>0)
{
	echo "<script>
			alert('Unloading point details already updated for this selection !');
			$('#loadicon').fadeOut('slow');
			('#button_sub').attr('disabled',false);
		</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO address_book_consignee(label,consignee,to_id,pincode,_lat,_long,google_km,branch,branch_user,record_by,
tno_visited,visit_date,timestamp) VALUES ('$label_name','$con2_id','$to_id','$pincode','$lat','$long','$distance','$branch','$_SESSION[user_code]',
'$own_truck_visited_var','$tno_visited','$visit_date','$timestamp')");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$this_id = getInsertID($conn);

$update_code = Qry($conn,"UPDATE address_book_consignee SET code='$this_id' WHERE id='$this_id'");

if(!$update_code){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(isset($_SESSION['unloading_point_google_addr']))
{
	$addr_loc = $_SESSION['unloading_point_google_addr'];
}
else
{
	$addr_loc = "";
}

$update_party = Qry($conn,"UPDATE consignee SET lat='$lat',lng='$long',location_addr='$addr_loc' WHERE id='$con2_id'");

if(!$update_party){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	unset($_SESSION['unloading_point_distance_km1']);
	unset($_SESSION['unloading_point_gps_lat']);
	unset($_SESSION['unloading_point_gps_lng']);
	unset($_SESSION['unloading_point_gps_pincode']);

	unset($_SESSION['unloading_point_add_con1_id']);
	unset($_SESSION['unloading_point_add_from_id']);
	unset($_SESSION['unloading_point_google_pincode']);
	unset($_SESSION['unloading_point_google_addr']);
	unset($_SESSION['unloading_point_google_lat']);
	unset($_SESSION['unloading_point_google_lng']);
	unset($_SESSION['unloading_point_lat_lng']);
 
	echo "<script>
			alert('OK : Unloading point added successfully !');
			window.location.href='./unloading_point.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertError("Error while processing request !!");
	exit();
}
?>