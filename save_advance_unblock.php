<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$frno1 = escapeString($conn,strtoupper($_POST['fm_no']));

$frno = explode("_",$frno1)[0];
$lr_date = explode("_",$frno1)[1];
$create_date_fm = explode("_",$frno1)[2];
$create_date_fm2 = date("d-m-Y",strtotime($create_date_fm));
$narration = escapeString($conn,($_POST['narration']));

$chk_req = Qry($conn,"SELECT id FROM advance_unlock WHERE frno='$frno'");

if(!$chk_req){ 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
	Redirect("Error while processing Request","./");
	exit();
}
 
if(numRows($chk_req)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO advance_unlock(lrno,frno,lr_date,is_allowed,narration,branch,branch_user,timestamp) VALUES 
('$lrno','$frno','$lr_date','0','$narration','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./unblock_advance.php';
	</script>";
	exit();
?>