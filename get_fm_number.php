<?php
require("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$get_fm_no = Qry($conn,"SELECT DISTINCT frno,truck_no,date,create_date FROM freight_form_lr WHERE lrno='$lrno' AND frno like '___F%'");

if(!$get_fm_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<option value='' style='font-size:12px'>--select fm--</option>";

if(numRows($get_fm_no)==0)
{
	echo "<script>
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',true);
		$('#lrno').attr('readonly',false);
	</script>";
	exit();
}
else
{
	while($row = fetchArray($get_fm_no))
	{
		echo "<option value='$row[frno]_$row[date]_$row[create_date]' style='font-size:12px'>$row[frno] - $row[truck_no]</option>";
	}
	
	echo "<script>
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
		$('#lrno').attr('readonly',true);
	</script>";
	exit();
}
?>