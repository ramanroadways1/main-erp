<?php
require_once("./connection.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['lr_id']));
$from = escapeString($conn,strtoupper($_POST['from']));
$to = escapeString($conn,strtoupper($_POST['to']));
$from_id = escapeString($conn,strtoupper($_POST['from_id']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));

$update = Qry($conn,"UPDATE lr_pre SET fstation='$from',tstation='$to',from_id='$from_id',to_id='$to_id' WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Updated Successfully !');
	window.location.href='./smemo_own.php';
</script>";
exit();
?>