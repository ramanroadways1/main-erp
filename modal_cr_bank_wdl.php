<form id="CreditBankWdlForm2" action="#" method="POST">
<div id="CrBankWdlModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Credit Bank Withdrawal
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Credit in Company (+) <font color="red">*</font></label>
				<select onchange="DebitFromComp(this.value)" class="form-control" name="company" required="required">
					<option class="company_sel_bank" value="">SELECT COMPANY</option>
					<option class="company_sel_bank" id="cr_comp_RRPL" value="RRPL">RRPL</option>
					<option class="company_sel_bank" id="cr_comp_RR" value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<script>
			function DebitFromComp(elem)
			{
				$('#cr_bank_ac_list').val('');
				
				if(elem!='')
				{
					$('#loadicon').show();
					$.ajax({
					url: "_load_ac_list.php",
					method: "post",
					data:'company=' + elem + '&type=' + 'credit',
					success: function(data){
						$("#cr_bank_ac_list").html(data);
					}})
				}
			}
			
			function CheckAcSelected(elem)
			{
				if(elem!='')
				{
					var res = elem.split("_");
					$('#cr_bank_name').val(res[2]);
					$('#cr_bank_ac_no').val(res[1]);
					$('#cr_branch_name').val(res[0]);
				}
				else
				{
					$('#cr_bank_name').val('');
					$('#cr_bank_ac_no').val('');
					$('#cr_branch_name').val('');
				}
			} 
			</script>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Select Account number <font color="red">*</font></label>
				<select onchange="CheckAcSelected(this.value)" id="cr_bank_ac_list" data-size="8" name="tno_1" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
					<option value="">--Select--</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Bank Name <font color="red">*</font></label>
				<input type="text" id="cr_bank_name" name="bank_name" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">A/c Number <font color="red">*</font></label>
				<input type="text" id="cr_bank_ac_no" name="ac_no" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Branch Name <font color="red">*</font></label>
				<input type="text" id="cr_branch_name" name="branch_name" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input class="form-control" min="1" type="number" name="amount" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Cheque No. <font color="red">*</font></label>
				<input class="form-control" oninput="this.value=this.value.replace(/[^0-9,]/,'')" type="text" name="chq_no" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Withdrawal Date <font color="red">*</font></label>
				<input type="text" class="form-control" value="<?php echo date("d-m-Y"); ?>" readonly required />
			</div>
			
		</div>
      </div>
	  
	  <div id="result_cr_bank_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" disabled id="cr_bank_deposit_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#CreditBankWdlForm2").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#cr_bank_deposit_button").attr("disabled", true);
	$.ajax({
        	url: "./save_cr_bank_wdl.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_cr_bank_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>