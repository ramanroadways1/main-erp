<?php
require_once 'connection.php';

$chk_pending_type = Qry($conn,"SELECT id FROM asset_vehicle WHERE asset_type='' AND branch='$branch'");
if(!$chk_pending_type){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_pending_type)==0)
{
	echo "<script>
		window.location.href='asset_vehicle_view.php';
	</script>";
	exit();
}

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./asset_vehicle_view.php"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid"> 
<div class="form-group col-md-12">
<br />	
	<div class="row">	
		<div class="form-group col-md-4">
			<h4><span class="fa fa-truck"></span> &nbsp; Update Asset Type : <font color="blue"><?php echo $branch; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Reg.No</th>
					<th>Vehicle_Class</th>
					<th>Owner_Name</th>
					<th>General/specific</th>
					<th>Update</th>
				</tr>	
				
<?php
$view_vehicle = Qry($conn,"SELECT v.id,v.vehicle_holder,v.asset_type,v.token_no,v.reg_no,v.owner_name,v.veh_class
FROM asset_vehicle AS v 
WHERE v.branch='$branch' AND v.active='1' AND asset_type=''");

if(!$view_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_vehicle)>0)
{
	$sn=1;
	
	while($row = fetchArray($view_vehicle))
	{
		if($row['reg_no']==""){
			$vehicle_no = $row['token_no'];
		}
		else{
			$vehicle_no = $row['reg_no'];
		}
		
		echo "<tr>
			<td>$sn</td>
			<td>$vehicle_no</td>
			<td>$row[veh_class]</td>
			<td>$row[owner_name]</td>
			<td>$row[asset_type]</td>
			<td><button type='button' onclick=TypeUpdate('$row[id]','$vehicle_no') class='btn btn-xs btn-default'>Update</button></td>";
		echo "
	</tr>";
	$sn++;	
	}
}
else
{
	echo "<tr><td colspan='14'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
</div>
</div>

<script>
function TypeUpdate(id,tno)
{
	$('#veh_id').val(id);
	$('#veh_txt').html(tno);
	$('#veh_txt2').val(tno);
	jQuery.noConflict();
	$('#AssetUpdateModal').modal(); 
}
</script>

<div id="func_result"></div>

<form id="FormUpdate" action="#" method="POST">
<div id="AssetUpdateModal" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Asset Type : <span id="veh_txt"></span>
      </div>
	  
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Vehicle number <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="veh_txt2" name="vehicle_no" class="form-control">
			</div>
			
			<div class="form-group col-md-6">
				<label>Asset Type <font color="red"><sup>*</sup></font></label>
				<select class="form-control" onchange="AssetChange(this.value)" required="required" name="asset_type">
					<option value="">--Select option--</option>
					<option value="GENERAL">General (for branch use)</option>
					<option value="SPECIFIC">Specific (personal vehicle)</option>
				</select>
			</div>
			
			<script>
			function AssetChange(type1)
			{
				if(type1=='SPECIFIC')
				{
					$('#emp_div').show();
					$('#emp_code').attr('required',true);
				}
				else
				{
					$('#emp_div').hide();
					$('#emp_code').attr('required',false);
				}
			}
			</script>
				
			<div id="emp_div" style="display:none" class="form-group col-md-6">
				<label>Select employee <font color="red"><sup>*</sup></font></label>
				<select class="form-control" required="required" id="emp_code" name="emp_code">
					<option value="">--Select option--</option>
					<?php
					$get_emp = Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3' ORDER by name ASC");
					
					if(numRows($get_emp)>0)
					{
						while($row_emp = fetchArray($get_emp))
						{
							echo "<option value='$row_emp[code]'>$row_emp[name] ($row_emp[code])</option>";
						}
					}
					?>
				</select>
			</div>
			
		<input type="hidden" id="veh_id" name="id">
			
		</div>
      </div>
	  <div id="ReqFormResult"></div>
      <div class="modal-footer">
        <button type="submit" id="req_button" class="btn btn-sm btn-primary">Update</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormUpdate").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#req_button").attr("disabled", true);
		$.ajax({
        	url: "./update_asset_type_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#ReqFormResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>