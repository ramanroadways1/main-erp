<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$veh_type = escapeString($conn,strtoupper($_POST['veh_type']));
// $payment_mode = escapeString($conn,strtoupper($_POST['payment_mode']));
$maker = trim(escapeString($conn,strtoupper($_POST['maker'])));
$model = trim(escapeString($conn,strtoupper($_POST['model'])));
$narration = trim(escapeString($conn,strtoupper($_POST['narration'])));

$getReqCode = GetVehCode($veh_type,$conn);

if(!$getReqCode || $getReqCode=="0" || $getReqCode==""){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
$reg_code = $getReqCode;

if($veh_type=="VEH_TW"){
	$veh_type="TWO_WHEELER";
}
else{
	$veh_type="FOUR_WHEELER";
}

// if($payment_mode=='NEFT'){
	// $ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	// $ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	// $bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	// $ifsc = trim(escapeString($conn,strtoupper($_POST['ifsc'])));
	// $pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
// }
// else{
	// $ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	// $ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	// $bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	// $ifsc = trim(escapeString($conn,strtoupper($_POST['ifsc'])));
	// $pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
// }

$insert_reg = Qry($conn,"INSERT INTO asset_vehicle_req(req_code,req_date,veh_type,maker_name,model_name,narration,branch,
branch_user,timestamp) VALUES ('$reg_code','$date','$veh_type','$maker','$model','$narration','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert_reg){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
			alert('Request Generated Successfully. Manager Approval Required !');
			window.location.href='asset_vehicle_view.php';
		</script>";
	closeConnection($conn);
	exit();
	
?>