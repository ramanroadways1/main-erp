<?php
require_once 'connection.php';

// if($branch!=='CGROAD')
// {
// echo "<script> 
		// alert('Temporary on hold.');
		// window.location.href='./';
	// </script>";
	// exit();
// }
	
$ipAddr = get_client_ip();
$branch = escapeString($conn,strtoupper($_SESSION['user']));
$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");

$chk_diesel_lr = Qry($conn,"SELECT id FROM diesel_lr_pending WHERE branch='$branch' AND HOUR(TIMEDIFF(NOW(), lr_expiry))>=24");

if(!$chk_diesel_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

// if(numRows($chk_diesel_lr)>0)
// {
	// echo "<script>
		// window.location.href='./pre_entry/';
	// </script>";
	// exit();
// }
?>
<!doctype html>
<html lang="en">
<head>
<title>RRPL: FREIGHT MEMO</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<?php 
	include("./_loadicon.php");
	// include("./disable_right_click.php");
?>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}

.ui-autocomplete { z-index:2147483647; }

.form-control{
	text-transform:uppercase;
}
</style> 

<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $('#owner_id').val(ui.item.oid);     
               $('#owner_name').val(ui.item.owner_name);     
               $('#owner_mobile').val(ui.item.owner_mobile);     
               $('#owner_pan').val(ui.item.owner_pan);     
               $('#truck_wheeler').val(ui.item.wheeler);       
				$("#get_lr_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			$("#owner_id").val('');
			$("#owner_name").val('');
			$("#owner_mobile").val('');
			$("#owner_pan").val('');
			$("#truck_wheeler").val('');
			$("#get_lr_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

</head>

<?php include("./modal_crossing_lr_add.php"); ?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php 	
if(isset($_SESSION['freight_memo'])){
	$fm_id = $_SESSION['freight_memo'];
}
else{
	$fm_id = "NA";
}
?>

<div id="get_lr_result"></div>

<script>
function ClearCache()
{
	var retVal = confirm("Do You Really Want to Clear Cache ?");
   if(retVal == true)
   {
	$("#loadicon").show();
		jQuery.ajax({
			url: "./clear_fm_cache.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
			error: function() {}
	});
   }
}

function LoadLrs(vou_no)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./get_lr_cache.php",
			data: 'vou_id=' + vou_no,
			type: "POST",
			success: function(data){
				$("#get_lr_cache").html(data);
			},
			error: function() {}
	});
}
</script>

<br />

<div class="container-fluid">
<div class="row">
	<div class="form-group col-md-4">
		<a href="./" style="color:#FFF;" class="btn btn-sm btn-primary">
			<span class="glyphicon glyphicon-chevron-left"></span> Dashboard
		</a>
	</div>	

	<div class="form-group col-md-4">
		<center><h4>Freight - Memo</h4></center>
	</div>	
</div>

	<div class="row">
<script>
function GetLR(tno){
	if(tno==''){
		alert('Enter Vehicle Number First !!');
	}
	else{
		$('#truck_no').attr('readonly',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_lr_for_fm.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
	
		<div class="form-group col-md-3">
			<label>Truck No <font color="red">*</font></label>
			<input value="<?php if(isset($_SESSION['freight_memo_vehicle'])) { echo $_SESSION['freight_memo_vehicle']; } ?>" <?php if(isset($_SESSION['freight_memo'])){ echo "readonly"; } ?> type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="truck_no" id="truck_no" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br>
			<button onclick="GetLR($('#truck_no').val())" id="get_lr_button" type="button" class="btn btn-sm btn-primary">Get LRs</button>
		</div>
		
	</div>
	
	<div class="row" id="get_lr_cache">
	</div>
</div>	

<?php include("./modal_add_driver.php"); ?>
<?php include("./modal_add_broker.php"); ?>

<script type="text/javascript"> 
function calcFix(){
	$("#rate").val((Number($("#actual").val()) / Number($("#weight_g").val())).toFixed(2));	
	
	var stn = $("#last_location").val().split('_');
	var stn = stn[0];
	
	if(stn=='HALDIA' || stn=='KOLKATA' || stn=='DHULAGARH (HOWRAH)')
	{
		if($("#actual").val()>225000)
		{
			alert('Freight is out of value. Please check it.');
			$("#rate").val('');
			$("#actual").val('');
		}
	}
	else
	{
		if($("#actual").val()>170000)
		{
			alert('Freight is out of value. Please check it.');
			$("#rate").val('');
			$("#actual").val('');
		}
	}
}

function calcRate(){
	
	$("#actual").val(Math.round(Number($("#rate").val()) * Number($("#weight_g").val())));
	
	var stn = $("#last_location").val().split('_');
	var stn = stn[0];
	
	if(stn=='HALDIA' || stn=='KOLKATA' || stn=='DHULAGARH (HOWRAH)')
	{
		if($("#actual").val()>225000)
		{
			alert('Freight is out of value. Please check it.');
			$("#rate").val('');
			$("#actual").val('');
		}
	}
	else
	{ 
		if($("#actual").val()>170000)
		{
			alert('Freight is out of value. Please check it.');
			$("#rate").val('');
			$("#actual").val('');
		}
	}
}
</script> 				

<script type="text/javascript">			
function Known(elem){
	
	var chrg1=$("#weight_g").val();
	
	if(chrg1=='')
	{
		alert('Please enter charge weight..');
		$("#known").val('');
	}
	if(elem == "FIX")
	{
		$("#rate").val("");
		$("#actual").val("");
		$("#rate").attr("readonly","readonly");
		$("#actual").attr("readonly",false);
		$("#actual").attr("onkeyup","calcFix();");
		$("#rate").attr("onkeyup",false);
	}
	else if(elem == "RATE")
	{
		$("#rate").val("");
		$("#actual").val("");
		$("#actual").attr("readonly","readonly");
		$("#rate").attr("readonly",false);
		$("#rate").attr("onkeyup","calcRate();");
		$("#actual").attr("onkeyup",false);
	}
	else if(elem == "ZERO")
	{
		$("#rate").val("0");
		$("#actual").val("0");
		$("#actual").attr("readonly","readonly");
		$("#rate").attr("readonly","readonly");
		$("#actual").attr("onkeyup",false);
		$("#rate").attr("onkeyup",false);
	}
	else
	{
		$("#rate").val("");
		$("#actual").val("");
		$("#actual").attr("readonly","readonly");
		$("#rate").attr("readonly","readonly");
		$("#actual").attr("onkeyup",false);
		$("#rate").attr("onkeyup",false);
	}
}
</script>

<script>
function MyFunc1()
{
	$("#known").val('');
	
	if($("#start_location").val()=='' || $("#last_location").val()=='')
	{
		$("#start_location").val('');
		$("#last_location").val('');
		$("#start_location").focus();
		$("#weight_g").val('');
	}
}

function ResetInput()
{
	$("#known").val('');
	$("#weight_g").val('');
	$("#rate").val('');
	$("#actual").val('');
}
</script>
	
</body>
</html>

<?php include("./_footer.php"); ?>

<script>
LoadLrs('<?php echo $fm_id; ?>');

function DeleteLrFromFm(id){
   var retVal = confirm("Do you really want to delete this LR ?");
   if(retVal == true)
   {
	 $("#loadicon").show();
		jQuery.ajax({
        url: "./delete_lr_from_cache.php",
        data: 'id=' + id + '&vou_no=' + '<?php echo $fm_id; ?>' + '&page_name=' + 'smemo.php',
        type: "POST",
        success: function(data){
            $("#get_lr_result").html(data);
        },
        error: function() {}
    });
   }
}
</script>

<div id="FreightMemoSuccessModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Freight Memo Created Successfully : <span id="freight_memo_id_val"></span>
      </div>
      <div class="modal-body">
	  <br />
        <div class="row">
			
			<div class="form-group col-md-4">
				<a style="text-decoration:none" href="./"><button class="btn btn-warning btn-block"><b>Dashboard</b></button></a>
			</div>
			
			<div class="form-group col-md-4">
				<a style="text-decoration:none" href="./smemo.php"><button class="btn btn-primary btn-block"><b>New Freight Memo</b></button></a>
			</div>
			
			<form action="smemo2.php" method="POST">
				<div class="form-group col-md-4">
					<button class="btn btn-danger btn-block"><b>Give Advance</b></button>
				</div>
				<input type="hidden" name="idmemo" id="freight_memo_id_val2" />
				<input type="hidden" name="key" value="FM" />
			</form>
		</div>
      </div>
	</div>
</div>
</div>

<button type="button" id="button_modal2" style="display:none" data-toggle="modal" data-target="#FreightMemoSuccessModal"></button>

<?php
include("./modal_update_crossing_loc.php");
include("./modal_add_breaking_lr.php");
?>