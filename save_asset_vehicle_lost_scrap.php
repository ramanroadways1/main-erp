<?php
require_once("./connection.php");

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$reason = escapeString($conn,strtoupper($_POST['option_value']));
$id = escapeString($conn,strtoupper($_POST['id']));

$chk_for_item = Qry($conn,"SELECT branch,active FROM asset_vehicle WHERE id='$id'");

if(!$chk_for_item){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_for_item)==0){
	echo "<script>
		alert('Vehicle not found.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$row_veh = fetchArray($chk_for_item);

if($row_veh['branch']!=$branch){
	echo "<script>
		alert('Vehicle not belongs to your branch.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['active']!="1"){
	echo "<script>
		alert('Vehicle is in-active.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$chk_for_entry = Qry($conn,"SELECT approval,ho_approval,timestamp FROM asset_vehicle_lost_scrap WHERE veh_id='$id'");

if(!$chk_for_entry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_for_entry)>0){
	
$row_entry = fetchArray($chk_for_entry);

if($row_entry['approval']!="1"){
	$approval_text = "Manager";
}else{
	$approval_text = "Head-Office";
}

	echo "<script>
		alert('You already generate a lost/scrap request on ".date("d-m-Y h:i A",strtotime($row_entry["timestamp"]))." ! Wait for $approval_text Approval.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO asset_vehicle_lost_scrap(veh_id,reason,branch,branch_user,timestamp) VALUES ('$id','$reason','$branch',
'$branch_sub_user','$timestamp')");

if(!$chk_for_item){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
			alert('Lost/Scrap Request Submitted Successfully. Wait for manager approval !');
			window.location.href='asset_vehicle_view.php';
		</script>";
	closeConnection($conn);
	exit();
?>