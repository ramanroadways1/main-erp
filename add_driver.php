<?php
require_once './connection.php'; 

$branch = escapeString($conn,strtoupper($_SESSION['user']));

$add_driver_filepath = $_SESSION['add_driver_filepath'];

$check_ocr_active = Qry($conn,"SELECT id FROM _ocr_active WHERE branch='$branch' AND active='1'");
		
if(!$check_ocr_active){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo '<script>
		alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

if(numRows($check_ocr_active) >0 )
{	
	$aadhar_no = $_SESSION['driver_add_aadhar_no'];
	$aadhar_name = strtoupper($_SESSION['driver_aadhar_name']);
	$aadhar_img = $_SESSION['driver_aadhar_img'];
	$aadhar_dob = $_SESSION['driver_aadhar_dob'];
	$aadhar_addr = $_SESSION['driver_aadhar_addr'];
	
	$lic_no = $_SESSION['add_driver_lic_no'];
	$driver_name = strtoupper($_SESSION['add_driver_name']);
	$permanent_addr = $_SESSION['add_driver_permanent_addr'];
	$temp_addr = $_SESSION['add_driver_temp_addr'];
	$father_husband_name = $_SESSION['add_driver_father_husband'];
	$dob = $_SESSION['add_driver_dob'];
	$expiry_date = $_SESSION['add_driver_expiry_date'];
	$issue_date = $_SESSION['add_driver_issue_date'];
	$blood_group = $_SESSION['add_driver_blood_group'];
	$vehicle_class = $_SESSION['add_driver_vehicle_classes'];
	$dl_img = $_SESSION['add_driver_image'];
}
else
{
	$driver_name = escapeString($conn,strtoupper($_POST['driver_name']));
	$lic_no = escapeString($conn,strtoupper($_POST['lic_no']));
	$aadhar_no = "";
	$aadhar_name = $driver_name;
	$aadhar_img = "";
	$aadhar_dob = "";
	$aadhar_addr = "";
	$permanent_addr = "";
	$temp_addr = "";
	$father_husband_name = "";
	$dob = "";
	$expiry_date = "";
	$issue_date = "";
	$blood_group = "";
	$vehicle_class = "";
	$dl_img = "";
}

$chk_driver = Qry($conn,"SELECT name FROM mk_driver WHERE pan='$lic_no'");
		
if(!$chk_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo '<script>
		alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

if(numRows($chk_driver) > 0)
{
	$row_dup = fetchArray($chk_driver);
	
	echo "<script>
		alert('Duplicate license found. License: $lic_no already registered with driver : $row_dup[name] !');
		$('#verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$mobile = escapeString($conn,strtoupper($_POST['mobile1']));
$mobile2 = escapeString($conn,strtoupper($_POST['mobile2']));

$compare = compareStrings($aadhar_name, $driver_name);

if($compare<50)
{
	echo "<script type='text/javascript'> 
		alert('Error: Driver name not matching with Aadhar name !');
		$('#loadicon').hide();
		$('#add_driver_button').attr('disabled',false);
	</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($mobile2,'0')===0 AND $mobile2!='')
{
	echo "<script>
		alert('Error: Check mobile number-2.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$lic_path = "driver_pan/".$add_driver_filepath;

if(!copy("driver_pan_temp/".$add_driver_filepath,$lic_path)){
	
    echo "<script>
		alert('Error while uploading document : Driving License !');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO mk_driver(name,mo1,mo2,pan,full,up5,branch,branch_user,aadhar_no,aadhar_name,aadhar_img,dl_img,
aadhar_dob,aadhar_addr,permanent_addr,temp_addr,father_husband_name,dob,expiry_date,issue_date,blood_group,vehicle_class,timestamp) VALUES 
('$driver_name','$mobile','$mobile2','$lic_no','$permanent_addr','$lic_path','$branch','$branch_sub_user','$aadhar_no','$aadhar_name','$aadhar_img',
'$dl_img','$aadhar_dob','$aadhar_addr','$permanent_addr','$temp_addr','$father_husband_name','$dob','$expiry_date','$issue_date','$blood_group',
'$vehicle_class','$timestamp')");
	
if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	unset($_SESSION['add_driver_filepath']);
	unset($_SESSION['driver_add_aadhar_no']);
	unset($_SESSION['driver_aadhar_name']);
	unset($_SESSION['driver_aadhar_img']);
	unset($_SESSION['driver_aadhar_dob']);
	unset($_SESSION['driver_aadhar_addr']);
	unset($_SESSION['add_driver_lic_no']);
	unset($_SESSION['add_driver_name']);
	unset($_SESSION['add_driver_permanent_addr']);
	unset($_SESSION['add_driver_temp_addr']);
	unset($_SESSION['add_driver_father_husband']);
	unset($_SESSION['add_driver_dob']);
	unset($_SESSION['add_driver_expiry_date']);
	unset($_SESSION['add_driver_issue_date']);
	unset($_SESSION['add_driver_blood_group']);
	unset($_SESSION['add_driver_vehicle_classes']);
	unset($_SESSION['add_driver_image']);
	
	echo "<script type='text/javascript'> 
		alert('Driver : $driver_name. Added Successfully..');
		window.location.href='./all_functions.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Error while processing request !');
		$('#loadicon').hide();
		$('#add_driver_button').attr('disabled',false);
	</script>";
	exit();
}

?>