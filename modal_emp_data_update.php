<form id="UpdateEmpDataForm" action="#" method="POST">
<div id="UpdateEmpModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Employee Data : <span id="emp_name_html"></span>
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-9">
				<label>Employee Name <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="emp_name_update" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="mobile_emp_update" name="mobile" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3">
				<label>&nbsp;</label>
				<br>
				<button type="button" id="send_otp_button" onclick="SendOTPUpdate()" class="btn btn-danger">Send OTP !</button>
			</div>
			
			<div class="form-group col-md-12" id="otp_div" style="display:none">
				<label>Enter OTP <font color="red"><sup>*</sup></font></label>
				<input type="text" id="otp_update" name="otp" maxlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="emp_code" id="emp_code_update">
			
		</div>
      </div>
	  <div id="FormResult1"></div>
      <div class="modal-footer">
        <button type="submit" id="update_button" disabled class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-default" onclick="ResetModalUpdate()" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateEmpDataForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#update_button").attr("disabled", true);
		$.ajax({
        	url: "./emp_data_update.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#FormResult1").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function SendOTPUpdate()
{
	$('#send_otp_button').attr('disabled',true);
	
	var mobile = $('#mobile_emp_update').val();
	var name = $('#emp_name_update').val();
	
	if(mobile!='')
	{
		// alert(mobile);
		$('#mobile_emp_update').attr('readonly',true);
		$('#update_button').attr('disabled',true);
		
		$("#loadicon").show();
		jQuery.ajax({
			url: "./_send_otp_verify_mobile.php",
			data: 'mobile=' + mobile + '&name=' + name,
			type: "POST",
			success: function(data){
				$("#FormResult1").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		alert('Enter Mobile Number First !');
		$('#mobile_emp_update').focus();
		$('#mobile_emp_update').attr('readonly',false);
		$('#update_button').attr('disabled',true);
		$('#send_otp_button').attr('disabled',false);
	}
}

function ResetModalUpdate()
{
	document.getElementById("UpdateEmpDataForm").reset();
	$('#update_button').attr('disabled',true);
	$('#mobile_emp_update').attr('readonly',false);
	$('#send_otp_button').attr('disabled',false);
	$('#otp_div').hide();
}
</script>