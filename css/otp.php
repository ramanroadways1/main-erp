<?php
require_once '../connection.php';

$branch=mysqli_real_escape_string($conn,strtoupper($_SESSION['user']));

$branch_to=$_POST['branch_to'];
$company_to=$_POST['company_to'];
$amount=$_POST['amount'];

$qry_fetch=mysqli_query($conn,"SELECT mobile FROM branch_manager WHERE branch='$branch_to'");
if(mysqli_num_rows($qry_fetch)>0)
{
	$row_fetch=mysqli_fetch_array($qry_fetch);
	$mobile=$row_fetch['mobile'];
}
else
{
	echo "<script>
			alert('Branch not found contact head-office.');
			$('#submit_branch').attr('disabled',true);
		</script>
		";
		exit();
}

if(strlen($mobile)!='10')
{
	echo "<script>
			alert('Please enter 10 digits Mobile No.');
			$('#submit_branch').attr('disabled',true);
		</script>
		";
		exit();
}

$today=date('Y-m-d');

$otp=rand(100000,999999);

$mobileNumber=$mobile;

$qry=mysqli_query($conn,"INSERT INTO reg_otp (branch,otp,type,try,date) VALUES ('$branch','$otp','B2B','3','$today')");

$message="Branch $branch wants to Credit Rs. $amount/- to your $company_to. Allow with share OTP $otp.";
$message = urlencode($message);
$senderId="RRPLHO";
$route=2;
$authKey="242484AIn6HT1iiUiU5bca2d9c";

   $postData = array(
					'authkey' => $authKey,
					'mobiles' => $mobileNumber,
					'message' => $message,
					'sender' => $senderId,
					'route' => $route
				);

				//API URL
				$url="http://www.smsschool.in/api/sendhttp.php";

				// init the resource
				$ch = curl_init();
				curl_setopt_array($ch, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $postData
					//,CURLOPT_FOLLOWLOCATION => true
				));


				//Ignore SSL certificate verification
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


				//get response
				$output = curl_exec($ch);
				
//Print error if any
    if(curl_errno($ch))
    {
        echo 'error:' . curl_error($ch);
		echo '<script>
$("#submit_branch").attr("disabled",true);
</script>';
		exit();
    }
curl_close($ch);

?>

<style>
label{font-size:12px;font-family:Verdana;}
#otp{border:1px solid #000;}
</style>

<div class="form-group col-md-12">
<label>Enter OTP <font color="red">*</font></label>
<input id="otp" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" name="otp" class="form-control" required="required" />
</div>

<div class="form-group col-md-12">
<span style="color:#FFF;font-size:12px;font-weight:bold"><center>OTP has been sent to <?php echo $branch_to; ?> Branch. <br> Mobile No. : <?php echo $mobile; ?>. <br />(wait for 50 seconds to share OTP..!)</center></span>
</div>

<script>
$("#submit_branch").attr('disabled',false);
</script>