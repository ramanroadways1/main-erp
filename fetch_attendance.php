<?php
require_once 'connection.php';

$date_attendance = escapeString($conn,$_POST['date']);

$total_no = Qry($conn,"SELECT total,p,a,hd FROM emp_attd_check WHERE date='$date_attendance' AND branch='$branch'");
if(!$total_no){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_fetch=fetchArray($total_no);
				
$total_strength=$row_fetch['total'];
$present=$row_fetch['p'];
$absent=$row_fetch['a'];
$half_day=$row_fetch['hd'];
?>

<a style="display:none" id="view_attd_modal_button" data-toggle="modal" data-target="#view_attd"></a>

<div id="view_attd" class="modal fade" role="dialog" style="background:#299C9B" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
			Attendance Summary : <?php echo date("d-m-y",strtotime($date_attendance)); ?>
      </div>
	  
  <div class="modal-body">
	  <div class="row">
			<div class="col-md-12 table-striped">
<?php
$fetchAttd = Qry($conn,"SELECT r.name,s.code,s.status,s.remarks 
FROM emp_attendance as r 
LEFT OUTER JOIN emp_attendance_save AS s ON s.code=r.code 
WHERE (r.status IN('3','2') OR (r.status='-1' AND r.terminate='0')) AND s.date='$date_attendance' AND s.branch='$branch'");

if(!$fetchAttd){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($fetchAttd)>0)
{

echo "
<div class='row'>
	<div class='col-md-12' style='color:#000'>
	Total Employee : $total_strength, Total Present : $present, Total Absent : $absent, Total Half day : $half_day.
	</div>
</div>
<br />
<table class='table table-bordered' style='color:#000;font-size:11.5px;'>
	<tr>
		<th>Id</th>
		<th>Employee Name</th>
		<th>Emp. Code</th>
		<th>Action</th>
		<th>Remarks</th>
	</tr>";

$srno=1;
				
	while($row_emp=fetchArray($fetchAttd))
	{
					if($row_emp['status']=='P')
					{
						$status= "<td style='background:#52EB4A'><font color='#000'>Present</font></td>";
					}
					else if($row_emp['status']=='A')
					{
						$status= "<td style='background:#F47676'><font color='#FFF'>Absent</font></td>";
					}
					else if($row_emp['status']=='HD')
					{
						$status= "<td style='background:#5DDBE0'><font color='#000'>Half day</font></td>";
					}
					else
					{
						$status= "<td style='background:#F47676'><font color='#FFF'>ERROR</font></td>";
					}
		
		echo "<tr>
			<td>$srno</td>
			<td>$row_emp[name]</td>
			<td>$row_emp[code]</td>
			$status
			<td>$row_emp[remarks]</td>
		</tr>";
		$srno++;
	}	
	echo "</table>";	
}
else
{
	echo "<center><b><font color='red'>No result found.</b></font></center>";
}
		?>
		</div>
      </div>
  </div>
  
    <div class="modal-footer">
        <button type="button" onclick="$('#attd_viewDate').val('')" data-dismiss="modal" style="color:#FFF;font-weight:bold;" class="btn btn-danger">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
	document.getElementById('view_attd_modal_button').click();
</script>