<?php
require_once('connection.php');

$lrno = escapeString($conn,strtoupper($_POST['lrno'])); 
		
$act_wt = escapeString($conn,strtoupper($_POST['act_wt'])); 
$chrg_wt = escapeString($conn,strtoupper($_POST['chrg_wt']));
		
$total1 = escapeString($conn,strtoupper($_POST['total_1'])); 
$total2 = escapeString($conn,strtoupper($_POST['total_2'])); 
$left1 = escapeString($conn,strtoupper($_POST['left_1'])); 
$left2 = escapeString($conn,strtoupper($_POST['left_2'])); 
$used1 = escapeString($conn,strtoupper($_POST['used_1'])); 
$used2 = escapeString($conn,strtoupper($_POST['used_2'])); 
		
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
		
$qry=Qry($conn,"SELECT wt12,weight,break FROM lr_sample WHERE lrno='$lrno' AND cancel!='1' AND crossing!='NO'");
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script type='text/javascript'>
		alert('LR not found');
		window.location.href='./lr_break.php';
	</script>";		
	exit();
}

	$row=fetchArray($qry);
	
	$breaking=$row['break'];
	
	$lr_actual=$row['wt12'];
	$lr_charge=$row['weight'];
	
$sum=Qry($conn,"SELECT IFNULL(SUM(act_wt),0) as total_act,IFNULL(SUM(chrg_wt),0) as total_chrg FROM lr_break WHERE lrno='$lrno' 
AND branch='$branch'");

if(!$sum){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row2=fetchArray($sum);

$break_actual=$row2['total_act'];
$break_charge=$row2['total_chrg'];

if($break_actual>=$lr_actual)
{
	echo "<script>
		alert('No weight left for LR breaking.');
		window.location.href='./lr_break.php';
	</script>";
	exit();
}

$left_actual=$lr_actual-$break_actual;
$left_charge=$lr_charge-$break_charge;

if($act_wt>$left_actual)
{
	echo "<script>
		alert('Actual weight is geater than actual weight left.');
		window.location.href='./lr_break.php';
	</script>";
	exit();
}

if($chrg_wt>$left_charge)
{
	echo "<script>
		alert('Charge weight is geater than charge weight left.');
		window.location.href='./lr_break.php';
	</script>";
	exit();
}

$insert=Qry($conn,"INSERT INTO lr_break (branch,branch_user,lrno,act_wt,chrg_wt,date) VALUES 
('$branch','$branch_sub_user','$lrno','$act_wt','$chrg_wt','$date')");
	
if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$update_lr_table = Qry($conn,"UPDATE lr_sample SET break=break+1 WHERE lrno='$lrno'");
if(!$update_lr_table){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
	echo "<script>
		alert('LR Breaking success.');
		window.location.href='./lr_break.php';
	</script>";
	exit();

?>