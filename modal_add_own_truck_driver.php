<style>
#label_od{
	font-size:12px;
}

input{
	font-size:12px !important;
}

.ui-widget {
  font-family: Verdana;
  font-size: 12px;
}
</style>

<?php
$get_pending_req = Qry($conn,"SELECT id FROM dairy.driver_temp WHERE branch='$branch'");
$pending_req = numRows($get_pending_req);
?>
<script type="text/javascript">
	$(function() {
		$("#otd_tno").autocomplete({
		source: '../diary/autofill/own_tno.php',
		select: function (event, ui) { 
               $('#otd_tno').val(ui.item.value);   
               $('#otd_supervisor').val(ui.item.supervisor);   
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#otd_tno").val('');
			$("#otd_supervisor").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script>

<form style="font-size:12px" id="DriverFormOwnTruck" action="#" method="POST">
<div id="AddOwnTruckDriverModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD NEW DRIVER (Own - Truck)
		
		<span class="pull-right">
			Pending Req. : 	<button type="button" onclick="ViewPendingReqDriver()" class="btn btn-danger btn-xs" id="pending_req_html"><?php echo $pending_req; ?></button>
		</span>
      </div>
	  
<script>	  
function ViewPendingReqDriver()
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "./fetch_pending_driver_req.php",
		data: 'ok=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#PendingReqDiv").html(data);
		},
		error: function() {}
		});
}
</script>	 

<script>	  
function GetLicenseInfo()
{
	var lic_no = $('#lic_no_1').val();
	
	$("#loadicon").show();
		jQuery.ajax({
		url: "./get_license_info.php",
		data: 'lic_no=' + lic_no,
		type: "POST",
		success: function(data) {
			$("#driver_lic_result").html(data);
		},
		error: function() {}
		});
}
</script> 
	  
      <div class="modal-body" style="overflow:auto">
       
	   <div id="PendingReqDiv"></div>
	   
	   <div class="row" id="FormDivDriver">
		
			<div class="form-group col-md-4">
				<label id="label_od">License No. <font color="red"><sup>*</sup></font></label>
				<input type="text" placeholder="RJ04*************" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="lic_no_1" name="lic_no_1" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label id="label_od">&nbsp;</label>
				<br />
				<button type="button" id="get_lic_details_btn" onclick="GetLicenseInfo()" class="btn btn-sm btn-success">Fetch details</button>
			</div>
			
			<div id="driver_lic_result" class="form-group col-md-12">
			</div>
			
			<div class="form-group col-md-12">
				<label id="label_od">Driver Full Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="d_full_name1" readonly oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label id="label_od">Mobile No. <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			<!--
			<div class="form-group col-md-4">
				<label id="label_od">Guarantor <font color="red"><sup>* (active driver)</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" name="guarantor" class="form-control" required="required">
			</div>
			
			
			<div class="form-group col-md-4">
				<label id="label_od">Reference By <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" name="ref_person" class="form-control" required="required">
			</div>
			-->
			<div class="form-group col-md-4"> 
				<label id="label_od">Aadhar Number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="12" name="aadhar_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4"> 
				<label id="label_od">Driver Photo <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12px;" type="file" accept="image/*" name="driver_photo" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3"> 
				<label id="label_od">License Copy <font color="red"><sup>* (front)</sup></font></label>
				<input style="font-size:12px;" type="file" accept="image/*" name="lic_copy_front" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3"> 
				<label id="label_od">License Copy <font color="red"><sup>* (rear)</sup></font></label>
				<input style="font-size:12px;" type="file" accept="image/*" name="lic_copy_rear" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3"> 
				<label id="label_od">Aadhar Copy <font color="red"><sup>* (front)</sup></font></label>
				<input style="font-size:12px;" type="file" accept="image/*" name="aadhar_front" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-3"> 
				<label id="label_od">Aadhar Copy <font color="red"><sup>* (rear)</sup></font></label>
				<input style="font-size:12px;" type="file" accept="image/*" name="aadhar_rear" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12"> 
				<label id="label_od" style="color:maroon">Update Account details ? <input type="checkbox" name="ac_details" id="ac_details_chkbox" /></label>
			</div>

<script>
$(document).on('click','#ac_details_chkbox',function(){
var ckbox = $('#ac_details_chkbox');

		$('#otd_ac_holder').val('');
		$('#otd_ac_no').val('');
		$('#otd_bank_name').val('');
		$('#otd_ifsc').val('');
		
	if (ckbox.is(':checked'))
	{
		// $('.driver_ac_details_div').show();
		$('#otd_ac_holder').attr('required',true);
		$('#otd_ac_no').attr('required',true);
		$('#otd_bank_name').attr('required',true);
		$('#otd_ifsc').attr('required',true);
		
		$('#otd_ac_holder').attr('readonly',false);
		$('#otd_ac_no').attr('readonly',false);
		$('#otd_bank_name').attr('readonly',false);
		$('#otd_ifsc').attr('readonly',false);
	}
	else
	{
		// $('.driver_ac_details_div').hide();
		$('#otd_ac_holder').attr('required',false);
		$('#otd_ac_no').attr('required',false);
		$('#otd_bank_name').attr('required',false);
		$('#otd_ifsc').attr('required',false);
		
		$('#otd_ac_holder').attr('readonly',true);
		$('#otd_ac_no').attr('readonly',true);
		$('#otd_bank_name').attr('readonly',true);
		$('#otd_ifsc').attr('readonly',true);
	}
});
</script>
			
			<div style="display:none1" class="driver_ac_details_div form-group col-md-4"> 
				<label id="label_od">A/c holder <font color="red"><sup>*</sup></font></label>
				<input readonly type="text" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" name="ac_holder" id="otd_ac_holder" class="form-control">
			</div>
			
			<div style="display:none1" class="driver_ac_details_div form-group col-md-4"> 
				<label id="label_od">A/c number <font color="red"><sup>*</sup></font></label>
				<input readonly type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="ac_no" id="otd_ac_no" class="form-control">
			</div>
			
			<div style="display:none1" class="driver_ac_details_div form-group col-md-4"> 
				<label id="label_od">Bank name <font color="red"><sup>*</sup></font></label>
				<input readonly type="text" oninput="this.value=this.value.replace(/[^A-Z a-z]/,'')" name="bank_name" id="otd_bank_name" class="form-control">
			</div>
			
			<div style="display:none1" class="driver_ac_details_div form-group col-md-4"> 
				<label id="label_od">IFSC code <font color="red"><sup>*</sup></font></label>
				<input readonly maxlength="11" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ValidateIFSCDriver()" name="ifsc" id="otd_ifsc" class="form-control">
			</div>
			
			<div class="form-group col-md-4"> 
				<label id="label_od">Vehicle number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'');" name="tno" id="otd_tno" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label id="label_od">Driver's Address <font color="red"><sup>*</sup></font></label>
				<textarea readonly oninput="this.value=this.value.replace(/[^a-z A-Z0-9-.,/@#]/,'')" id="driver_addr1" name="addr" class="form-control" required="required"></textarea>
			</div>
			<input type="hidden" id="otd_supervisor" name="supervisor">
		</div>
      </div>
	  <div id="result_otd_driverForm"></div>
      <div class="modal-footer">
        <button type="submit" disabled style="display:none" id="add_driver_button_own_truck" class="btn btn-sm btn-primary">Submit Request</button>
        <button type="button" id="close_add_driver_own_truck" onclick="$('#DriverFormOwnTruck')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
function ValidateIFSCDriver() {
  var Obj = document.getElementById("otd_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("otd_ifsc").setAttribute("style","background-color:red;color:#fff;");
				// document.getElementById("add_driver_button_own_truck").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("otd_ifsc").setAttribute("style","background-color:green;color:#fff;");
				// document.getElementById("add_driver_button_own_truck").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("otd_ifsc").setAttribute("style","background-color:white;color:#fff");
			// document.getElementById("add_driver_button_own_truck").removeAttribute("disabled");
		}
 } 			
</script>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#DriverFormOwnTruck").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#add_driver_button_own_truck").attr("disabled", true);
	$.ajax({
        	url: "./add_driver_own_truck.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_otd_driverForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>