<?php
require_once("./connection.php");

$plant_name = escapeString($conn,strtoupper($_POST['plant_name']));

if($plant_name=='OTHER')
{
	echo "<script>
		$('.pune_plant_val').attr('disabled',true);
		$('#pune_plant_OTHER').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else if($plant_name=='AT' || $plant_name=='AS')
{
	$chk_plant = Qry($conn,"SELECT lrno+1 as lrno FROM lr_check WHERE branch='PUNE' AND plant='$plant_name' ORDER BY id DESC 
	LIMIT 1");

	if(!$chk_plant){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_plant)==0)
	{
		echo "<script>
			alert('Previous LRs not found belongs to plant : $plant_name.')
			$('#pune_plant').val('');
			$('#pune_plant').focus();
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_lr = fetchArray($chk_plant);
	
	echo "<script>
		$('.pune_plant_val').attr('disabled',true);
		$('#pune_plant_$plant_name').attr('disabled',false);
		$('#lrno').val('$row_lr[lrno]');
		$('#lrno').attr('readonly',true);
		CheckConsignor();
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	echo "<script>
		alert('Invalid plant selected !!');
		window.location.href='./';
	</script>";
	exit();
}
?>