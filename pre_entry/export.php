<?php
require_once("../connection.php");

$branch = escapeString($conn,strtoupper($_SESSION['user']));

$from = escapeString($conn,$_POST['from_date']);
$to = escapeString($conn,$_POST['to_date']);

$output='';

$qry = Qry($conn,"SELECT fno,tno,lrno,com,SUM(cash) as cash,SUM(disamt) as diesel,GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as dsl_nrr,
pay_date,done FROM diesel_fm WHERE pay_date BETWEEN '$from' AND '$to' AND branch='$branch' GROUP BY fno");
if(!$qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('No record found.');
		window.location.href='./';
	</script>";
	exit();
}

$output .= '
   <table border="1">  
       <tr>  
        <th>Token No.</th>
		<th>Date</th>
		<th>LR NO</th>
		<th>Truck No</th>
		<th>Company</th>
		<th>Cash</th>
		<th>Diesel</th>
		<th>Diesel Details</th>
		<th>Status</th>
	</tr>';
	
  while($row = fetchArray($qry))
  {
	 if($row['done']==1)
	 {
		$done="<font color='black'>Done</font>";
	 }	
	 else
	 {
		 $done="<font color='red'>Pending</font>";
	 }

   $output .= '
				<tr> 
							<td>'.$row["fno"].'</td> 
							<td>'.$row["pay_date"].'</td> 
							<td>'.$row["lrno"].'</td> 
							<td>'.$row["tno"].'</td> 
							<td>'.$row["com"].'</td>  
							<td>'.$row["cash"].'</td>  
							<td>'.$row["diesel"].'</td>  
							<td>'.$row["dsl_nrr"].'</td>  
							<td>'.$done.'</td>  
				</tr>
   ';
  }
  $output .= '</table>';
  closeConnection($conn);
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans.xls');
  echo $output;
  exit();
?>