<?php
require_once('../connection.php');

// echo "<script>
		// alert('Diesel Temporary on hold till 1:40 PM.');
		// window.location.href='./';
	// </script>";
	// exit();
	
$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$id=escapeString($conn,strtoupper($_POST['diesel_id']));

if(empty($id)){
	echo "<script>
			alert('Unable to fetch Diesel.');
			window.location.href='./';
		</script>";
	exit();
}

$get_diesel_data = Qry($conn,"SELECT tno,lrno,com as company,branch,crossing,no_lr FROM diesel_fm WHERE id IN($id)");

if(!$get_diesel_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_diesel_data)==0)
{
	echo "<script>
			alert('Diesel Record not found.');
			window.location.href='./';
		</script>";
	exit();
}

// if($branch=='CGROAD')
// {
	// echo numRows($get_diesel_data).' '.$id;
	// echo "<script>
			// $('#loadicon').hide();
			// $('#lr_update_button').attr('disabled',false);
		// </script>";
	// exit();
// }
	
$row_diesel = fetchArray($get_diesel_data);

if($row_diesel['no_lr']==0 || $row_diesel['lrno']!='EMPTY')
{
	echo "LR Number already mapped to this Diesel Request.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

$get_lr_data = Qry($conn,"SELECT id,lr_id,company,branch,truck_no,crossing,cancel,break,diesel_req FROM lr_sample_pending WHERE 
lrno='$lrno'");

if(!$get_lr_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lr_data)==0)
{
	echo "Invalid LR number entered.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

$row_lr = fetchArray($get_lr_data);

$lr_id_main = $row_lr['lr_id'];
$lr_id = $row_lr['id'];

if($row_lr['cancel']=="1")
{
	echo "LR Marked as Cancel.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['branch']!=$row_diesel['branch'])
{
	echo "LR Belongs to $row_lr[branch] Branch.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['break']!=0)
{
	echo "This is breaking LR You Can not add this LR.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['crossing']!="")
{
	echo "Voucher Created against LR. You can not add this LR.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['diesel_req']>0)
{
	echo "LR Number already Mapped with another Diesel Recharge.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['company']!=$row_diesel['company'])
{
	echo "Company not matching.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['truck_no']!=$row_diesel['tno'])
{
	echo "Vehicle Number not matching.";
	echo "<script>
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$delete_record = Qry($conn,"DELETE FROM diesel_lr_pending WHERE diesel_id IN($id)");

if(!$delete_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr = Qry($conn,"UPDATE lr_sample_pending SET diesel_req=diesel_req+1 WHERE id='$lr_id'");

if(!$update_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_main = Qry($conn,"UPDATE lr_sample SET diesel_req=diesel_req+1 WHERE id='$lr_id_main'");

if(!$update_lr_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_diesel_lr = Qry($conn,"UPDATE diesel_fm SET lrno='$lrno',no_lr='0' WHERE id IN($id)");

if(!$update_diesel_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_to_pending_diesel = Qry($conn,"UPDATE _pending_diesel SET lrno='$lrno' WHERE diesel_id IN($id)");

if(!$update_lr_to_pending_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('LR Mapped Successfully.');
		window.location.href='./';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>