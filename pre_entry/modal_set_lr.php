<form id="UpdateLRForm" action="#" method="POST">
<div id="UpdateLRModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		LR Number Against Diesel Recharge
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Enter LR No.<font color="red"><sup>*</sup></font></label>
				<input type="text" id="lrno_diesel" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			<input type="hidden" name="diesel_id" id="diesel_id">
		</div>
		
		<div class="row">
			<div class="form-group col-md-12" id="result_UpdateLRForm" style="color:red">
			</div>
		</div>
		
      </div>
	  <div class="modal-footer">
        <button type="submit" id="lr_update_button" class="btn btn-primary">Update LR</button>
        <button type="button" class="btn btn-default" onclick="document.getElementById('UpdateLRForm').reset()" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateLRForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#lr_update_button").attr("disabled", true);
		$.ajax({
        	url: "./lr_update.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_UpdateLRForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>