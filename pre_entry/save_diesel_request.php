<?php
require_once '../connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

// echo "<script>
		// alert('Diesel Temporary on hold till 1:40 PM.');
		// window.location.href='./';
	// </script>";
	// exit();
	
if(!isset($_SESSION['diesel_req'])){
	echo "<script>
		alert('Something went wrong with session.');
		window.location.href='./';
	</script>";
	exit();
}

$token_no = escapeString($conn,strtoupper($_SESSION['diesel_req']));
$tno=escapeString($conn,strtoupper($_POST['tno']));
$lrno=escapeString($conn,strtoupper($_POST['lrno']));

$chk_diesel = Qry($conn,"SELECT id FROM diesel_sample WHERE frno='$token_no'");
if(!$chk_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_diesel)==0)
{
	echo "<script>
		alert('Diesel not added yet !');
		$('#req_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$getTotal = Qry($conn,"SELECT ROUND(SUM(qty),2) AS total_qty,ROUND(SUM(amount),2) AS total_amount FROM diesel_sample WHERE frno='$token_no'");
if(!$getTotal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$rowTotal = fetchArray($getTotal);

$totalQty = $rowTotal['total_qty'];
$totalAmount = $rowTotal['total_amount'];

$check_truck_diesel = Qry($conn,"SELECT date(timestamp) as recharge_date,branch FROM diesel_lr_pending WHERE tno='$tno'");
if(!$check_truck_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_truck_diesel)>0)
{
	$row_truck=fetchArray($check_truck_diesel);
	
	echo "<script>
		alert('Diesel already requested of this vehicle on $row_truck[recharge_date] by branch $row_truck[branch] and LR not mapped yet.');
		window.location.href='./';
	</script>";
	exit();	
}

if($lrno!='EMPTY')
{
	$chk_lr = Qry($conn,"SELECT id,lr_id,company,branch,lr_type,truck_no,crossing,break,cancel,diesel_req FROM lr_sample_pending WHERE 
	lrno='$lrno'");
	
	if(!$chk_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_lr)==0)
	{
		echo "<script>
			alert('Invalid LR Number entered !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	$row_chk_lr = fetchArray($chk_lr);
	
	$lr_id_main = $row_chk_lr['lr_id'];
	$lr_id = $row_chk_lr['id'];
	
	if($row_chk_lr['branch']!=$branch)
	{
		echo "<script>
			alert('LR Number: $lrno belong to $row_chk_lr[branch] Branch !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['truck_no']!=$tno)
	{
		echo "<script>
			alert('Vehicle Number not matching with LR\'s Vehicle Number !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['lr_type']!='MARKET')
	{
		echo "<script>
			alert('LR Number : $lrno is not market vehicle LR !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['cancel']!=0)
	{
		echo "<script>
			alert('LR Number : $lrno Marked as Canceled !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['diesel_req']!=0)
	{
		echo "<script>
			alert('LR Number : $lrno Mapped with another diesel request !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['crossing']!="")
	{
		echo "<script>
			alert('LR Number : $lrno has been closed !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	if($row_chk_lr['break']!=0)
	{
		echo "<script>
			alert('Your can\'t request diesel from breaking LRs !');
			$('#req_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
	
	$company = escapeString($conn,strtoupper($row_chk_lr['company']));
}
else
{
	$company = escapeString($conn,strtoupper($_POST['company']));
}

$select_diesel = Qry($conn,"SELECT frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,ril_card,mobile_no,fuel_comp,
dsl_nrr,branch,date,done,done_time,stockid FROM diesel_sample WHERE frno='$token_no'");
	
if(!$select_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($select_diesel)==0)
{
	echo "<script>
		alert('Diesel not added yet !');
		window.location.href='./';
	</script>";
	exit();	
}

$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$token_no' AND ril_card='1'");
	
if(!$chk_ril_stock){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_ril_stock)>0)
{
	while($row_ril = fetchArray($chk_ril_stock))
	{
		$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
		if(!$get_balance){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_balance = fetchArray($get_balance);
		
		if($row_balance['balance']<$row_ril['amount'])
		{
			echo "<script>
				alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
				window.location.href='./';
			</script>";
			exit();	
		}
	}
}
	
StartCommit($conn);
$flag = true;

if($lrno=='EMPTY')
{
	while($row_copy = fetchArray($select_diesel))
	{
		$insert_main = Qry($conn,"INSERT INTO diesel_fm(branch,branch_user,fno,token_no,com,qty,rate,disamt,tno,lrno,type,
		dsl_by,dcard,veh_no,dcom,dsl_nrr,pay_date,done,done_time,no_lr,approval,pre_req,timestamp,timestamp_approve,
		ril_card,dsl_mobileno,stockid) VALUES ('$branch','$branch_sub_user','$token_no','$token_no','$company','$row_copy[qty]','$row_copy[rate]',
		'$row_copy[amount]','$tno','$lrno','ADVANCE','$row_copy[card_pump]','$row_copy[card_no]','$row_copy[phy_card]',
		'$row_copy[fuel_comp]','$row_copy[dsl_nrr]','$date','$row_copy[done]','$row_copy[done_time]','1','1','1','$timestamp',
		'$timestamp','$row_copy[ril_card]','$row_copy[mobile_no]','$row_copy[stockid]')"); 
		
		if(!$insert_main){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_id = getInsertID($conn);
		
		$insert_no_lr = Qry($conn,"INSERT INTO diesel_lr_pending(diesel_id,tno,company,branch,timestamp,lr_expiry) VALUES 
		('$insert_id','$tno','$company','$branch','$timestamp','$timestamp')");
		if(!$insert_no_lr){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_pending_diesel = Qry($conn,"INSERT INTO _pending_diesel(diesel_id,fno,lrno,tno,branch) VALUES ('$insert_id','$token_no',
		'$lrno','$tno','$branch')");
		
		if(!$insert_pending_diesel){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($row_copy['card_pump']=='CARD')
		{
			$card_live_bal = Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_copy[card_no]'");
			if(!$card_live_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
					
			$row_card_bal=fetchArray($card_live_bal); 
			$live_bal = $row_card_bal['balance']; 

			$qry_log = Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,dslcomp,dsltype,
			cardno,vehno,cardbal,amount,mobileno,reqid) VALUES ('$branch','SUCCESS','New Request Added','$timestamp',
			'MARKET','$tno','$row_copy[fuel_comp]','CARD','$row_copy[card_no]','$row_copy[card_no]','$live_bal','$row_copy[amount]',
			'$row_copy[mobile_no]','$insert_id')");
					
			if(!$qry_log){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}
else
{
	while($row_copy = fetchArray($select_diesel))
	{
		$insert_main = Qry($conn,"INSERT INTO diesel_fm(branch,branch_user,fno,token_no,com,qty,rate,disamt,tno,lrno,type,dsl_by,
		dcard,veh_no,dcom,dsl_nrr,pay_date,done,done_time,no_lr,approval,pre_req,timestamp,timestamp_approve,ril_card,
		dsl_mobileno,stockid) VALUES ('$branch','$branch_sub_user','$token_no','$token_no','$company','$row_copy[qty]',
		'$row_copy[rate]',
		'$row_copy[amount]','$tno','$lrno','ADVANCE','$row_copy[card_pump]','$row_copy[card_no]','$row_copy[phy_card]',
		'$row_copy[fuel_comp]','$row_copy[dsl_nrr]','$date','$row_copy[done]','$row_copy[done_time]','0','1','1','$timestamp',
		'$timestamp','$row_copy[ril_card]','$row_copy[mobile_no]','$row_copy[stockid]')"); 
		
		if(!$insert_main){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_id = getInsertID($conn);
		
		$insert_pending_diesel = Qry($conn,"INSERT INTO _pending_diesel(diesel_id,fno,lrno,tno,branch) VALUES ('$insert_id','$token_no',
		'$lrno','$tno','$branch')");
		
		if(!$insert_pending_diesel){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($row_copy['card_pump']=='CARD')
		{
			$card_live_bal = Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_copy[card_no]'");
			if(!$card_live_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
					
			$row_card_bal=fetchArray($card_live_bal); 
			$live_bal = $row_card_bal['balance']; 

			$qry_log = Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,dslcomp,dsltype,
			cardno,vehno,cardbal,amount,mobileno,reqid) VALUES ('$branch','SUCCESS','New Request Added','$timestamp',
			'MARKET','$tno','$row_copy[fuel_comp]','CARD','$row_copy[card_no]','$row_copy[card_no]','$live_bal','$row_copy[amount]',
			'$row_copy[mobile_no]','$insert_id')");
					
			if(!$qry_log){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

$chk_ril_stock2 = Qry($conn,"SELECT phy_card,amount FROM diesel_sample WHERE frno='$token_no' AND ril_card='1'");
	
if(!$chk_ril_stock2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_ril_stock2)>0)
{
	while($row_ril2 = fetchArray($chk_ril_stock2))
	{
		$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
		cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
		
		if(!$update_ril_stock){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
	
	$dlt_cache = Qry($conn,"DELETE FROM diesel_sample WHERE frno='$token_no'");
	if(!$dlt_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
if($lrno!='EMPTY')
{	
	$update_lr=Qry($conn,"UPDATE lr_sample SET diesel_req=diesel_req+1 WHERE id='$lr_id_main'");
	if(!$update_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_lr_main = Qry($conn,"UPDATE lr_sample_pending SET diesel_req=diesel_req+1 WHERE id='$lr_id'");
	if(!$update_lr_main){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$updateTodayData = Qry($conn,"UPDATE today_data SET diesel_qty_market=ROUND(diesel_qty_market+'$totalQty',2),
diesel_amount_market=diesel_amount_market+'$totalAmount' WHERE branch='$branch'");

if(!$updateTodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	unset($_SESSION['diesel_req']);
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Request submitted successfully.');
		window.location.href='./';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>