<?php
require_once '../connection.php';

require('../_algo_pod_45_days_lock_system.php');

// echo "<script>
		// alert('Diesel Temporary on hold till 1:40 PM.');
		// window.location.href='./';
	// </script>";
	// exit();
	
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(!isset($_SESSION['diesel_req']))
{
	$_SESSION['diesel_req']=$branch.mt_rand();
}

$diesel_req = $_SESSION['diesel_req'];

// $chk_no_lr = Qry($conn,"SELECT timestamp FROM diesel_fm WHERE no_lr=1 AND branch='$branch'");

$chk_diesel_lr = Qry($conn,"SELECT GROUP_CONCAT(diesel_id SEPARATOR ',') as diesel_id FROM diesel_lr_pending WHERE branch='$branch' 
AND HOUR(TIMEDIFF(NOW(), lr_expiry))>=24 GROUP BY tno");

if(!$chk_diesel_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($chk_diesel_lr)>0)
{
	$row_diesel = fetchArray($chk_diesel_lr);

	if($row_diesel['diesel_id']=='')
	{
		$diesel_ids = "0";
		$lock_diesel="0";
	}
	else
	{
		$diesel_ids = $row_diesel['diesel_id'];
		$lock_diesel="1";
	}
}
else
{
	$diesel_ids = "0";
	$lock_diesel="0";
}
?>
<!DOCTYPE html>
<html lang="en">

<?php
	include("../_header.php");
?>

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="../load.gif" /><br><b>Please wait ...</b></center>
</div>

<?php 
	include("../_loadicon.php");
	include("../disable_right_click.php");
?>

<style>
 .ui-autocomplete { z-index:2147483647; }
 label{
	 font-size:13px;
 }
</style>

<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>

<?php
include ("./modal_set_lr.php");
?>
<div id="function_result"></div>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
<?php
if($lock_diesel=="1")
{
	// <div class='form-group col-md-8 col-md-offset-2 table-responsive'>
			// <center><h5 style='color:red'>आपके द्वारा करवाए गए डीजल रिचार्ज के आगे एलआर  नंबर दर्ज करे !</h4></center>
		// <br />
	echo "<div class='row'>
		
		<div style='background-color:#eee;padding:5px;' class='form-group col-md-12'>
			<a href='../'><button class='btn-sm btn btn-primary pull-left'><span class='glyphicon glyphicon-chevron-left'></span> Dashboard</button></a>
			<center><h4>It's time to clear requested diesel.</h4></center>
		</div>
	
	<div class='form-group col-md-10 col-md-offset-1 table-responsive'>	
		<br />
		<table class='table table-bordered table-striped' style='font-size:12px'>
				<tr>
					<th>#</th>
					<th>Token_No</th>
					<th>Truck_No</th>
					<th>Company</th>
					<th>Amount</th>
					<th>Diesel_Details</th>
					<th>Date</th>
					<th>Enter_LR</th>
				</tr>";
		
$fetch_pending_lr_diesel = Qry($conn,"SELECT GROUP_CONCAT(id SEPARATOR ',') as diesel_ids,fno,com,SUM(disamt) as amount,tno,
	GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr,pay_date FROM diesel_fm WHERE id IN($diesel_ids) AND branch='$branch' 
	GROUP by tno");	
	
	$sn=1;	
	
	while($row_fetch_diesel = fetchArray($fetch_pending_lr_diesel))
	{
		echo "
		<tr>
			<td>$sn</td>
			<td>$row_fetch_diesel[fno]</td>
			<td>$row_fetch_diesel[tno]</td>
			<td>$row_fetch_diesel[com]</td>
			<td>$row_fetch_diesel[amount]</td>
			<td>$row_fetch_diesel[diesel_narr]</td>
			<td>".convertDate("d-m-y",$row_fetch_diesel["pay_date"])."</td>
			<td><button class='btn btn-sm btn-danger' onclick=($('#diesel_id').val('$row_fetch_diesel[diesel_ids]')) type='button' 
			data-toggle='modal' data-target='#UpdateLRModal'><i style='font-size:14px;' class='fa fa-arrow-circle-o-right' aria-hidden='true'></i></button></td>
		</tr>";
		$sn++;
	}
			
echo "</table>
	</div>
</div>
<script>
$(window).load(function(){ 
$('#window_loadicon').fadeOut();
});
</script>";
exit();
}
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselReqForm").on('submit',(function(e) {
$("#loadicon").show();
$("#req_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_diesel_request.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#function_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
	$(function() {
		$("#truck_no").autocomplete({
		source: '../autofill/get_market_vehicle.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $("#req_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			$("#req_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script>
function CheckLRNO(elem)
{
	if(elem=='NO')
	{
		$('#lrno_request').val('EMPTY');
		$('#lrno_request').attr('readonly',true);
		$('#company_div').show();
		$('#company_name').attr('required',true);
	}
	else
	{
		$('#lrno_request').val('');
		$('#lrno_request').attr('readonly',false);
		$('#company_div').hide();
		$('#company_name').attr('required',false);
	}
}
</script>

<br />

<div class="container-fluid">
	
	<div class="row">
		<div class="form-group col-md-6">
			<label>&nbsp;</label>
			<a href="../"><button class="btn btn-sm btn-primary pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
		</div>
		
		<div class="form-group col-md-6">
			<form action="export.php" method="POST">
			<div class="row">
				<div class="form-group col-md-4">
					<label>From date <font color="red"><sup>*</sup></font></label>
					<input name="from_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
				</div>
				<div class="form-group col-md-4">
					<label>To date <font color="red"><sup>*</sup></font></label>
					<input name="to_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
				</div>
					
				<div class="form-group col-md-4">
					<label>&nbsp;</label>
	<button type="submit" class="btn btn-block btn-success"><span class="glyphicon glyphicon-save"></span> &nbsp; Download Report</button>
				</div>
			</div>
			</form>	
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12"></div>
	</div>
		
	<div class="row">

	<div class="form-group col-md-6">
	
	<form id="DieselReqForm" method="POST">	
	<div class="row">
		<div class="form-group col-md-4">
			<label>Do you have LR No. ? <sup><font color="red">*</font></sup></label> 
			<select name="lr_sel" id="lr_sel" onchange="CheckLRNO(this.value)" required="required" class="form-control">
				<option value="">Select an option</option>
				<option value="YES">YES</option>
				<option value="NO">NO</option>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label>Vehicle No <sup><font color="red">*</font></sup></label>
			<input type="text" name="tno" id="truck_no" class="form-control" required />
		</div>
		
		<input type="hidden" id="crossing" name="crossing" />
		
		<div class="form-group col-md-4">
			<label>LR Number <sup><font color="red">*</font></sup></label>
			<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" id="lrno_request" class="form-control" required />
		</div>
		
		<div class="form-group col-md-4" style="display:none" id="company_div">
			<label>Company <sup><font color="red">*</font></sup></label>
			<select name="company" onchange="if($('#lr_sel').val()==''){ $('#lr_sel').focus(); $(this).val(''); }" class="form-control" id="company_name" required="required">
				<option value="">-- select --</option>
				<option value="RRPL">RRPL</option>
				<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
			</select>
		</div>
		
		<div class="form-group col-md-12">
			<div class="bg-primary form-group col-md-12">Diesel Summary &nbsp; <button  data-toggle="modal" data-target="#diesel_add_modal" style="color:#000" type="button"> Add Diesel</button>
			</div>
			<div class="row"><div id="result_adv_diesel" class="form-group col-md-12"></div></div>
		</div> 
	</div>
	
	<div class="row">
		<div class="form-group col-md-12">
			<button type="submit" id="req_button" style="color:#000" class="btn btn-md btn-warning">Submit Request</button>
		</div>
	</div>
	</form>
	
	</div>

<div class="form-group col-md-6">
	
<div class="row">
	<br />
	<div style="padding:4px;" class="form-group bg-primary col-md-12">
	 Diesel LR Pending:
	</div>	
	<div class="form-group col-md-12 table-responsive" style="font-size:12px;overflow:auto">
	<div class="row">
	<?php
	$lr_pending_diesel = Qry($conn,"SELECT GROUP_CONCAT(id SEPARATOR ',') as diesel_ids,fno,com,SUM(disamt) as amount,tno,
	GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr,pay_date FROM diesel_fm 
	WHERE id IN(SELECT diesel_id FROM diesel_lr_pending WHERE branch='$branch') AND branch='$branch' GROUP by tno");

if(!$lr_pending_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	if(numRows($lr_pending_diesel)>0)
	{
		$sn2=1;
		//<center><h5 style='color:red'>आपके द्वारा करवाए गए डीजल रिचार्ज के आगे एलआर  नंबर दर्ज करे !</h5></center>
	echo "
		<table class='table table-bordered'>
			<tr>
				<th>Id</th>
				<th>Token No</th>
				<th>TruckNo</th>
				<th>Diesel Details</th>
				<th>Date</th>
				<th>#LR</th>
			</tr>";
			
	while($row_lr_pendg=fetchArray($lr_pending_diesel))
		{
			echo "<tr>
					<td>$sn2</td>
					<td>$row_lr_pendg[fno]</td>
					<td>$row_lr_pendg[tno]</td>
					<td>$row_lr_pendg[diesel_narr]</td>
					<td>".convertDate("d/m",$row_lr_pendg["pay_date"])."</td>
					<td>
<button class='btn btn-sm btn-danger' onclick=($('#diesel_id').val('$row_lr_pendg[diesel_ids]')) type='button' data-toggle='modal' 
data-target='#UpdateLRModal'><i style='font-size:14px;' class='fa fa-arrow-circle-o-right' aria-hidden='true'></i></button>
					</td>
				</tr>";
			$sn2++;
		}	
		echo "</table>";	
	}
	else
	{
		echo "<center><b>No records found !</b></center>";
		
	}
	?>
		</div>
		</div>
	</div>
	
<?php
	// $qry_requested = Qry($conn,"SELECT lrno,tno,com,SUM(cash) as cash,SUM(disamt) as diesel,GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as dsl_nrr,
	// pay_date FROM diesel_fm WHERE done!=1 AND branch='$branch' GROUP by fno");
	// if(numRows($qry_requested)>0)
	// {
		// $sr=1;
		
		// echo "<table class='table table-bordered' style='font-family:Verdana;font-size:12px;'>
			// <tr>
				// <th>Id</th>
				// <th>LRNo</th>
				// <th>TruckNo</th>
				// <th>Date</th>
				// <th>Cash</th>
				// <th>Diesel</th>
				// <th>Diesel Details</th>
			// </tr>	
			// ";
		// while($row_old=fetchArray($qry_old))
		// {
			// echo "<tr>
					// <td>$sr</td>
					// <td>$row_old[lrno]</td>
					// <td>$row_old[tno]</td>
					// <td>".date('d-m-y',strtotime($row_old['pay_date']))."</td>
					// <td>$row_old[cash]</td>
					// <td>$row_old[diesel]</td>
					// <td>$row_old[dsl_nrr]</td>
			// </tr>";
		// $sr++;
		// }	
	// echo "</table>";	
	// }
	// else
	// {
		// echo "<center><b>No records found.</b></center>";
		
	// }
	?>

	</div>	
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function LoadDiesel()
{
	$('#loadicon').show();
	$.ajax({
		url: "fetch_diesel.php",
		method: "post",
		data:'vou_no=' + '<?php echo $diesel_req; ?>', 
		success: function(data){
		$("#result_adv_diesel").html(data);
	}})
}

function DeleteDiesel(id)
{
	$("#loadicon").show();
	$.ajax({
		url: "delete_diesel_adv.php",
		method: "post",
		data:'id=' + id, 
		success: function(data){
			$("#function_result").html(data);
			$("#loadicon").hide();
	}})
}
</script>

<script>
$(window).load(function(){ 
$('#window_loadicon').fadeOut();
});
LoadDiesel();
</script>

<?php include ("./modal_diesel_adv.php"); ?>