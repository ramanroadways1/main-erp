<style>
::-webkit-scrollbar{
    width: 5px;
    height: 5px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

body{overflow-x:hidden;}
.nav>li>a{font-size:12.5px; font-family: 'Open Sans', sans-serif !important;}
.navbar-header{font-family: 'Open Sans', sans-serif !important;}
.navbar-inverse{background-color:#299C9B;}

.navbar-default .navbar-nav>li>a{
color:#000;
}

.navbar-default .navbar-nav>li{
border-bottom:1px solid #ccc;
}

.navbar-default .navbar-nav>li>a:hover{
background-color:#000;
color:#fff;
}

#cd{
	color:black;
	padding:8px;
	font-size:12px;
	font-family:'Open Sans', sans-serif !important;
	transition: color 0.3s linear;
   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
	
}

#cd:hover{
	background-color:#07F;
	color:#FFF;
}

#newid>li>a:hover{
	background:#299C9B;
	color:#fff;
	
}
#newid>li>a{
 transition: color 0.3s linear;
 color:#333;
 
-webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
}

#newid>#active{
	background:lightblue;
}
</style>

<?php 
$pname = basename($_SERVER['PHP_SELF']);

$get_ewb_count = Qry($conn,"SELECT id FROM _eway_bill_validity WHERE ewb_expiry> DATE_SUB(NOW(), INTERVAL 3 DAY) AND ewb_expiry!=0 AND 
branch_timestamp IS NULL AND branch='$branch'");

if(!$get_ewb_count){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$ewb_count = "(0)";

if(numRows($get_ewb_count)>0)
{
	$ewb_count = "(".numRows($get_ewb_count).")";
}
?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
			
<a class="navbar-brand" href="<?php echo $baseUrl1 ?>" style="letter-spacing:0px;font-family: 'Open Sans', sans-serif !important">
<!--<span style="color:#FFF" class="glyphicon glyphicon-home"></span> &nbsp; UMMEED (RRPL)-->
<img style="margin-top:-13px;margin-left:-15px;width:160px;height:48px;" src="<?php echo $baseUrl1 ?>logo_final.jpg" /> 
</a>
            <ul class="user-menu">
				<li class="dropdown pull-right" style="font-size:13px;letter-spacing:0px;font-family: 'Open Sans', sans-serif !important">
					<a href="#" id="user_link" class="dropdown-toggle" data-toggle="dropdown">
						<span class="fa fa-user-circle-o" style="font-size:17px"></span> &nbsp;<?php echo strtoupper($branch)." - ".strtolower($row_get_emp_name_connect['name']); ?> &nbsp;<span class="fa fa-angle-down"></span></a> 
					<ul class="dropdown-menu" role="menu">
						<li><a style="font-size:13px;" href="<?php echo $baseUrl1 ?>change_password.php"><span class="fa fa-user-secret"></span> Change Password </a></li>
						<li><a style="font-size:13px;" href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span> Log out </a></li>
					</ul>
				</li>
			</ul>
        </div>
</nav>

<div id="sidebar-collapse" class="sidebar form-group col-md-2 col-sm-3">

   <ul id="newid" class="nav menu" style="font-weight:; letter-spacing:0.5px">
			<li id="<?php if($pname=='index.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>"><span class="fa fa-home" style="font-size:16px;"></span>  Dashboard</a></li>
			
<?php
if($_SESSION['role_login_type']==2 || $_SESSION['role_login_type']==6)
{
	if($_SESSION['user']=='PANIPAT')
	{
	?>
<li id="<?php if($pname=='') {echo "active";} ?>">
	<a href="../diary/"><span class="fa fa-truck" style="font-size:14px;"></span>&nbsp; e-Dairy</a></li>
				
<li id="<?php if($pname=='') {echo "active";} ?>">
<a href="../pod_portal_branch/"><span class="glyphicon glyphicon-duplicate" style="font-size:14px;"></span>&nbsp; Send/Receive PODs</a></li>
	 
	<?php	
	}
else
{
?>			 
			<li id="<?php if($pname=='smemo.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>smemo.php"><span class="fa fa-file-text" style="font-size:14px;"></span>&nbsp; Freight Memo</a></li>
			
			<li id="<?php if($pname=='smemo_own.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>smemo_own.php"><span class="fa fa-truck" style="font-size:14px;"></span>&nbsp; Own Truck Form</a></li>
				
	<!--<li id="<?php if($pname=='oxygen_movement.php') {echo "active";} ?>">
		<a href="<?php echo $baseUrl1 ?>oxygen_movement.php"><span class="fa fa-shield" style="font-size:14px;"></span>&nbsp; Oxygen Movement</a></li>
	-->
	
	<?php
	/*
	<li class="dropdown" id="">
			<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-shield"></span>&nbsp; Oxygen Movement <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
				<li><a id="cd" href="<?php echo $baseUrl1 ?>oxygen_movement.php">Trip Pending</a></li>
				<li><a id="cd" href="#" data-toggle="modal" data-target="#OxyVehModal">Oxygen Vehicles</a></li>
				<li><a id="cd" href="<?php echo $baseUrl1 ?>trip_history.php">Trip History</a></li>
			</ul>
	</li>
	<?php
	*/
	?>
	<li class="dropdown" id="<?php if($pname=='vehicle_placed_by_branch.php' || $pname=='vehicle_placement_req.php' || $pname=='_fetch_lr_entry.php' || $pname=='lr_cancel.php' || $pname=='lr_break.php' || $pname=="address_book.php") {echo "active";} ?>">
			<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-pencil-square-o"></span>&nbsp; LR section <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
				<li class="<?php if($pname=='vehicle_placement_req.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>vehicle_placement_req.php">Vehicle Placement Req.</a></li>
				<li class="<?php if($pname=='vehicle_placed_by_branch.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>vehicle_placed_by_branch.php">Placed Vehicle Summary</a></li>
				<li class="<?php if($pname=='_fetch_lr_entry.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>_fetch_lr_entry.php">LR Entry</a></li>
				<li class="<?php if($pname=='lr_cancel.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>lr_cancel.php">Cancel LR</a></li>
				 	<?php
					$brk_branch_fetch=Qry($conn,"SELECT branch FROM break_branch WHERE branch='$_SESSION[user]'");
					if(!$brk_branch_fetch){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
					
					if(numRows($brk_branch_fetch)>0)
					{
					?>	
						<li><a id="cd" href="<?php echo $baseUrl1 ?>lr_break.php">LR Breaking</a></li>
					<?php 
					}
					
					if($_SESSION['user']=='PUNE')
					{
					?>
						<li><a id="cd" href="<?php echo $baseUrl1 ?>round_trip_lrs.php">Round Trips</a></li>
					<?php					
					}
					?>
				</ul> 
	</li>
	
<?php
// if($_SESSION['user_code']=='032')
// {
?>
	<li id="<?php if($pname=='ewb_summary.php') {echo "active";} ?>">
	<a href="<?php echo $baseUrl1 ?>ewb_summary.php"><span class="fa fa-exclamation-triangle" style="font-size:14px;"></span>&nbsp; Update Ewb Status <font color="red"><sup>
	<?php echo $ewb_count; ?></font></sup></a></li> 
<?php
// }
?>

<li class="dropdown" id="<?php if($pname=='loading_point.php' || $pname=='unloading_point') {echo "active";} ?>">
	<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-map-marker"></span>&nbsp; Load/Unload Points <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>loading_point.php">Loading Points</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>unloading_point.php">Unloading Points</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>my_loading_point.php">MY loading Points</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>my_unloading_point.php">MY unloading Points</a></li>
	</ul>
</li>

	<li id="<?php if($pname=='ewb_validity_extend.php') {echo "active";} ?>">
	<a data-toggle="modal" data-target="#EwbValExtendModal" href="#"><span class="fa fa-history" style="font-size:14px;">
	</span>&nbsp; Extend Ewb Validity</a></li> 
<?php
// }
?>
			
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-tachometer"></span>&nbsp; Diesel <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
					<li><a id="cd" href="<?php echo $baseUrl1 ?>pre_entry/">Market Truck Diesel Req.</a></li>
					<li><a id="cd" href="<?php echo $baseUrl1 ?>pre_entry_own/">Own Truck Diesel Req.</a></li>
					<li><a id="cd" href="<?php echo $baseUrl1 ?>_diesel_tank/">Diesel Tank</a></li>
				</ul>
			</li>
			
			
<li class="dropdown" id="<?php if($pname=='fexp.php' || $pname=='ftruck.php' || $pname=='diesel_vou.php') {echo "active";} ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-inr"></span>&nbsp; Vouchers <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
							<li><a id="cd" href="<?php echo $baseUrl1 ?>fexp.php">Expense Voucher</a></li>
							<li><a id="cd" href="<?php echo $baseUrl1 ?>ftruck.php">Truck Voucher</a></li>
							<li><a id="cd" href="<?php echo $baseUrl1 ?>diesel_vou.php">Fuel Voucher</a></li>
						</ul>
</li>		

<li class="dropdown" id="<?php if($pname=='neft_details.php' || $pname=='attendance_sheet.php') {echo "active";} ?>">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-book"></span>&nbsp; Reports <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>neft_details.php">NEFT by Broker/Owner</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>attendance_sheet.php">Attendance Sheet</a></li>
	</ul>
</li>
			
<li class="dropdown" id="<?php if($pname=='hisab_approval.php' || $pname=='fm_cash_approval.php' || $pname=='block_fm_balance.php' || $pname=='ewb_exempt.php' || $pname=='unblock_advance.php' || $pname=='unlock_rtgs_validity.php' || $pname=='unblock_balance.php' || $pname=='waive_late_pod.php' || $pname=='unload_detention.php' || $pname=='lr_date_exceed.php') {echo "active";} ?>">
<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-envelope-o"></span>&nbsp; Request / Approval <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>hisab_approval.php">e-Diary Hisab Approval</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>unblock_advance.php">Advance Unlock</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>unblock_balance.php">Open Balance Validity</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>unload_detention.php">Unload./Deten./LatePOD</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>lr_date_exceed.php">LR Date-Exceed</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>ewb_exempt.php">Exempt Eway-Bill</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>block_fm_balance.php">Block FM Balance</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>fm_cash_approval.php">FM Cash Approval</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>unlock_rtgs_validity.php">Unlock Rtgs > 30D</a></li>
	</ul>
</li>
			
<!--
<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-search"></span>View <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
							<li><a id="cd" href="exp_vou.php">Expense Voucher</a></li>
							<li><a id="cd" href="truck_vou.php">Truck Voucher</a></li>
							<li><a id="cd" href="viewm_fm_pending.php">FM Adv Pending</a></li>
							<li><a id="cd" href="pod_by_you.php">POD Received</a></li>
							<li><a id="cd" href="balance_by_you.php">Balance Paid</a></li>
							<li><a id="cd" href="balance_pending.php">Balance Pending</a></li>
							<li><a id="cd" href="pod_rcvd_bal_pending.php">POD Rcvd & Bal Pending</a></li>
							<li><a id="cd" href="view_bilty.php">Market Bilty</a></li>
							<li><a id="cd" href="view_own_form.php">Own Truck Form</a></li>
							<li><a id="cd" href="view_neft.php">RTGS/NEFT</a></li>
							<li><a id="cd" href="view_cancel_lr.php">LR Canceled</a></li>
						</ul>
</li>		-->
<?php
/*
if($_SESSION['user_code']=='152' || $_SESSION['user_code']=='032')
{
?>
<li id="<?php if($pname=='upload_veh_data.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>upload_veh_data.php"><i class="fa fa-map-marker"></i>&nbsp; Update Live Loc.</a></li>
<?php
}
*/
?>

<li id="<?php if($pname=='credit.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>credit.php"><i class="fa fa-plus-circle"></i>&nbsp; Credit (+)</a></li>
<li id="<?php if($pname=='debit.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>debit.php"><i class="fa fa-minus-circle"></i>&nbsp; Debit (-)</a></li>

<li class="dropdown" id="<?php if($pname=='asset_vehicle_view.php' || $pname=='assets_view.php') {echo "active";} ?>">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart"></i>&nbsp; Assets Management <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>asset_vehicle_view.php">Vehicles</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>assets_view.php">Other Assets</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>pending_asset_req.php">Pending Requests</a></li>
	</ul>
</li>

	<li id="<?php if($pname=='employee_view.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>employee_view.php"><i class="fa fa-users"></i>&nbsp; Employee Management</a></li>

			<li class="dropdown" id="<?php if($pname=='cashbook.php' || $pname=='passbook.php' || $pname=='cheq_book.php') {echo "active";} ?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-hdd"></span>Books <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
							<li><a id="cd" href="<?php echo $baseUrl1 ?>cashbook.php">Cash Book</a></li>
							<li><a id="cd" href="<?php echo $baseUrl1 ?>passbook.php">Pass Book </a></li>
							<li><a id="cd" href="<?php echo $baseUrl1 ?>cheq_book.php">Cheque Book </a></li>
						</ul>
			</li>
		
	<!--
<li><a href="excel"><span class="glyphicon glyphicon-download-alt"></span><span style="text-transform:uppercase">E</span>xcel <span style="text-transform:uppercase">D</span>ownloads </a></li>	-->

<li id="<?php if($pname=='login.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>manager/"><span class="fa fa-user-secret" style="font-size:15px;"></span>&nbsp; Manager Approval</a></li>
<li id="<?php if($pname=='excel_download.php.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>excel_download.php"><span class="fa fa-file-excel-o" style="font-size:15px;"></span>&nbsp; Excel Downloads</a></li>
<li id="<?php if($pname=='all_functions.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>all_functions.php"><span class="fa fa-database"></span>&nbsp; Functions</a></li>

<li id="<?php if($pname=='') {echo "active";} ?>">
				<a href="../diary/"><span class="fa fa-truck" style="font-size:14px;"></span>&nbsp; e-Dairy</a></li>
<li><a href="_help/"><span class="fa fa-search"></span>&nbsp; Help / Find</a></li>

<!--<li><a href="owner_change_req.php"><span class="fa fa-files-o"></span>&nbsp; Owner Change</a></li>-->
<li id="<?php if($pname=='e_lr.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>e_lr.php"><span class="glyphicon glyphicon-print"></span>e-LR Format</a></li>

<li class="dropdown" id="">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-duplicate"></span>Receive POD <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>pod/check_pod/">Check POD Status</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>pod/">Receive POD</a></li>
		<li><a id="cd" href="../pod_portal_branch/">Send/Receive PODs </a></li>
		<?php
							if($_SESSION['user']=='THANE')
							{
								echo '<li><a id="cd" href="../finetech_bill_gen/">Finetech Bill Gen.</a></li>';
							}
							?>
	</ul>
</li>	
<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
<?php
}
}
else if($_SESSION['role_login_type']==1)
{
?>
	<li class="dropdown" id="<?php if($pname=='_fetch_lr_entry.php' || $pname=='lr_cancel.php' || $pname=='lr_break.php') {echo "active";} ?>">
			<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-pencil-square-o"></span>&nbsp; LR section <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
				<li id="<?php if($pname=='_fetch_lr_entry.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>_fetch_lr_entry.php">LR Entry</a></li>
				<li id="<?php if($pname=='lr_cancel.php') {echo "active";} ?>"><a id="cd" href="<?php echo $baseUrl1 ?>lr_cancel.php">Cancel LR</a></li>
					<?php
					$brk_branch_fetch=Qry($conn,"SELECT branch FROM break_branch WHERE branch='$_SESSION[user]'");
					if(!$brk_branch_fetch){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
					
					if(numRows($brk_branch_fetch)>0)
					{
					?>	
						<li><a id="cd" href="<?php echo $baseUrl1 ?>lr_break.php">LR Breaking</a></li>
					<?php 
					}
					?>
				</ul>
	</li>
		

<li id="<?php if($pname=='e_lr.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>e_lr.php"><span class="glyphicon glyphicon-print"></span>e-LR Format</a></li>
<li id="<?php if($pname=='all_functions.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>all_functions.php"><span class="fa fa-database"></span>&nbsp; Functions</a></li>
<li id="<?php if($pname=='') {echo "active";} ?>">
<li><a href="_help/"><span class="fa fa-search"></span>&nbsp; Help / Find</a></li>

<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
	<?php
}
else if($_SESSION['role_login_type']==3)
{
?>
<li id="<?php if($pname=='all_functions.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>all_functions.php"><span class="fa fa-database"></span>&nbsp; Functions</a></li>
<li id="<?php if($pname=='') {echo "active";} ?>">
<li><a href="_help/"><span class="fa fa-search"></span>&nbsp; Help / Find</a></li>

<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
	<?php
}
else if($_SESSION['role_login_type']==4)
{
?>
<li id="<?php if($pname=='') {echo "active";} ?>">
	<a href="../diary/"><span class="fa fa-truck" style="font-size:14px;"></span>&nbsp; e-Dairy</a></li>

<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
	<?php
}
else if($_SESSION['role_login_type']==5)
{
?>
<li class="dropdown" id="">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-duplicate"></span>Receive POD <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
		<li><a id="cd" href="<?php echo $baseUrl1 ?>pod/check_pod/">Check POD Status</a></li>
		<li><a id="cd" href="<?php echo $baseUrl1 ?>pod/">Receive POD</a></li>
		<li><a id="cd" href="../pod_portal_branch/">Send/Receive PODs </a></li>
		<?php
							if($_SESSION['user']=='THANE')
							{
								echo '<li><a id="cd" href="../finetech_bill_gen/">Finetech Bill Gen.</a></li>';
							}
							?>
	</ul>
</li>	
<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
	<?php
}
?>

		</ul>
</div>

<?php include("modal_extend_ewb_validity.php"); ?>
<?php include("modal_oxygen_vehicles.php"); ?>