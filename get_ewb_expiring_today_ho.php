<?php
require_once 'connection.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT e.id,e.lrno,e.ewb_no,l.truck_no,DATE_FORMAT(e.ewb_date,'%d-%m-%y') as ewb_date,DATE_FORMAT(l.date,'%d-%m-%y') as lr_date,
l.fstation,l.tstation,CONCAT(l.consignor,'<br>',l.consignee) as party,e.del_date,
IF(e.del_date=0,branch_narration,CONCAT('Delivered- ',DATE_FORMAT(e.ewb_date,'%d-%m-%y'))) as branch_narration,
DATE_FORMAT(e.branch_timestamp,'%d-%m-%y %H:%i') as updated_at
FROM _eway_bill_validity AS e 
LEFT JOIN lr_sample AS l ON l.id=e.lr_id 
WHERE e.ho_check='0' AND e.branch_timestamp IS NOT NULL ORDER BY e.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'lrno', 'dt' => 1),
    array( 'db' => 'truck_no', 'dt' => 2),
    array( 'db' => 'lr_date', 'dt' => 3), 
    array( 'db' => 'ewb_date', 'dt' => 4), 
    array( 'db' => 'fstation', 'dt' => 5),  
    array( 'db' => 'tstation', 'dt' => 6),
    array( 'db' => 'party', 'dt' => 7), 
    array( 'db' => 'ewb_no', 'dt' => 8), 
    array( 'db' => 'branch_narration', 'dt' => 9), 
    array( 'db' => 'updated_at', 'dt' => 10), 
    array( 'db' => 'del_date', 'dt' => 11), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);