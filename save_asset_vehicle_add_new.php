<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$reg_no = trim(escapeString($conn,strtoupper($_POST['reg_no'])));
$reg_date = escapeString($conn,strtoupper($_POST['reg_date']));
$rc_holder = escapeString($conn,strtoupper($_POST['rc_holder']));
$veh_type = escapeString($conn,strtoupper($_POST['veh_type']));
$fuel_type = escapeString($conn,strtoupper($_POST['fuel_type']));
$veh_class = trim(escapeString($conn,strtoupper($_POST['veh_class'])));
$maker_name = trim(escapeString($conn,strtoupper($_POST['maker_name'])));
$model = trim(escapeString($conn,strtoupper($_POST['model'])));
$engine_no = escapeString($conn,strtoupper($_POST['engine_no']));
$chasis_no = escapeString($conn,strtoupper($_POST['chasis_no']));
$veh_for = escapeString($conn,strtoupper($_POST['veh_for']));

if($veh_for=='SPECIFIC'){
	$employee = trim(escapeString($conn,strtoupper($_POST['employee'])));
}
else{
	$employee = $branch;
}

$chk_veh = Qry($conn,"SELECT id,branch FROM asset_vehicle WHERE reg_no='$reg_no'");
if(!$chk_veh){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_veh)>0){
	$row_chk_veh = fetchArray($chk_veh);
	echo "<script>
		alert('Duplicate Registration Number : $reg_no. Vehicle already added to branch: $row_chk_veh[branch] !!');
		$('#button_addNewVeh2').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO asset_vehicle(vehicle_holder,reg_no,branch,branch_user,asset_type,veh_type,reg_date,owner_name,veh_class,
chasis_no,engine_no,fuel_type,maker,model,add_type,ho_approval,ho_approval_time,date,timestamp) VALUES ('$employee','$reg_no','$branch','$branch_sub_user','$veh_for','$veh_type',
'$reg_date','$rc_holder','$veh_class','$chasis_no','$engine_no','$fuel_type','$maker_name','$model','2','1','$timestamp','$date','$timestamp')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$insertId = getInsertID($conn);

$insert2 = Qry($conn,"INSERT INTO asset_vehicle_docs(veh_id,timestamp) VALUES ('$insertId','$timestamp')");

if(!$insert2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	
	echo "<script>
		alert('Vehicle: $reg_no added successfully !');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();		   
?>