<?php
require_once("connection.php");

$tno_id = escapeString($conn,strtoupper($_POST['tno_id']));
$tno = escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

if(count(explode(' ', $tno)) > 1)
{
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: 'Invalid vehicle number !!'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

$get_vehicle = Qry($conn,"SELECT tno,mo1,last_verify,wheeler FROM mk_truck WHERE id='$tno_id'");
	
if(!$get_vehicle){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($get_vehicle)==0){
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Warning !!!',
	text: 'Vehicle not found..'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

$row_owner = fetchArray($get_vehicle);
	
if($row_owner['tno']!=$tno)
{
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Warning !!!',
	text: 'Vehicle not verified..'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}
	
$oid = $tno_id;
$wheeler = $row_owner['wheeler'];
$tno_last_verify = $row_owner['last_verify'];

if(empty($wheeler))
{
	echo "<script>Swal.fire({
		icon: 'error',
		title: 'Warning !!!',
		text: 'Wheeler not found..'
		})
		$('#loadicon').hide();
	</script>";
	exit();
}

// for hold api start 

$_SESSION['verify_vehicle_type'] = "MARKET";
$_SESSION['verify_vehicle'] = $tno;
$_SESSION['verify_vehicle_id'] = $tno_id;
$_SESSION['verify_vehicle_wheeler'] = $wheeler;
$_SESSION['verify_vehicle_mobile_no'] = $row_owner['mo1'];

echo "<script>
	$('.veh_sel_dropdown').attr('disabled',true);
	$('#VEH_MARKET').attr('disabled',false);
</script>";

if($branch=='PEN')
{
	$_SESSION['verify_driver'] = "PEN_DRIVER";
	$_SESSION['verify_driver_id'] = "NA";
	
	echo "<script>
		$('#d_lic_no').attr('readonly',true);
		$('#verify_dl_div').show();
		$('#verify_btn_div_dl').hide();
		$('#is_dl_verified').val('1')
	</script>";
}

echo "
<script>
	// $('#close_verify_modal')[0].click();
	// $('#function_result').html('');
	// $('#button_print_modal')[0].click(); 
	// $('#button_verify_1')[0].click();
	$('#veh_no_verify_market').attr('readonly',true);
	$('#verify_rc_div').show();
	$('#verify_btn_div_rc').hide();
	$('#is_rc_verified').val('1')
	$('#loadicon').hide();
</script>";

// for hold api ends 

/*

$hourdiff_tno = round((strtotime($timestamp) - strtotime($tno_last_verify))/3600, 1);

$is_api_error = false;

if($tno_last_verify==0 || $hourdiff_tno>36)
{
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://prod.aadhaarapi.com/verify-rc/v3",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>"{\r\n
	  	\"reg_no\": \"$tno\",\r\n
	  	\"consent\": \"Y\",\r\n
	  	\"consent_text\": \"Details fetched for $tno verification on behalf of RRPL. \"\r\n}",
	  CURLOPT_HTTPHEADER => array(
	    "qt_api_key: 74b6283a-c1b0-4199-a7e6-f2bc2f176b83",
	    "qt_agency_id: 80c497b4-bc77-42f5-8e9f-56ec2c4beb0c",
	    "Content-Type: application/json"
	  ),
	));

	$output = curl_exec($curl); 
	curl_close($curl); 

	$response = json_decode($output, true);
	@$req_id = $response['id'];
	@$row = $response['result'];
	@$rowcode = $response['response_code'];
	   
	if($rowcode=="101")
	{ 
			@$rc_fit_upto = date_create_from_format("d-M-Y",$row['rc_fit_upto']);
			@$rc_insurance_upto = date_create_from_format("d-M-Y",$row['rc_insurance_upto']);
			@$rc_non_use_from = date_create_from_format("d-M-Y",$row['rc_non_use_from']);
			@$rc_non_use_to = date_create_from_format("d-M-Y",$row['rc_non_use_to']);
			@$rc_np_upto = date_create_from_format("d-M-Y",$row['rc_np_upto']);
			@$rc_permit_issue_dt = date_create_from_format("d-M-Y",$row['rc_permit_issue_dt']);
			@$rc_permit_valid_from = date_create_from_format("d-M-Y",$row['rc_permit_valid_from']);
			@$rc_permit_valid_upto = date_create_from_format("d-M-Y",$row['rc_permit_valid_upto']);
			@$rc_pucc_upto = date_create_from_format("d-M-Y",$row['rc_pucc_upto']);
			@$rc_regn_dt = date_create_from_format("d-M-Y",$row['rc_regn_dt']);
			@$rc_status_as_on = date_create_from_format("d-M-Y",$row['rc_status_as_on']);

			if($row['rc_tax_upto']!='LTT'){
				@$rc_tax_upto = date_format(date_create_from_format("d-M-Y",$row['rc_tax_upto']), "Y-m-d");
			} else {
				@$rc_tax_upto = 'LTT';
			}
			
			function RemoveSpecialChar($str) { 
				$res = str_replace( array( '\'', '"',',' , ';', '<', '>' ), ' ', $str); 
				return $res; 
			} 
	
			$maker_name = escapeString($conn,$row['rc_maker_desc']);
			$maker_model_name = escapeString($conn,$row['rc_maker_model']);
			$maker_month_year = escapeString($conn,$row['rc_manu_month_yr']);
			$body_type= escapeString($conn,$row['rc_body_type_desc']);
			$rc_f_name= escapeString($conn,$row['rc_f_name']);
			$chasi_no= escapeString($conn,$row['rc_chasi_no']);
			$eng_no= escapeString($conn,$row['rc_eng_no']);
			$financer= escapeString($conn,$row['rc_financer']);
			$insurance_comp= escapeString($conn,$row['rc_insurance_comp']);
			$ins_policy_no= escapeString($conn,$row['rc_insurance_policy_no']);
			$noc_details= escapeString($conn,$row['rc_noc_details']);
			$rc_norms_desc= escapeString($conn,$row['rc_norms_desc']);
			$rc_np_issued_by= escapeString($conn,$row['rc_np_issued_by']);
			$rc_np_no= escapeString($conn,$row['rc_np_no']);
			$rc_owner_name= escapeString($conn,$row['rc_owner_name']);
			$rc_permanent_address=  $row['rc_permanent_address'];
			$rc_blacklist_status=  $row['rc_blacklist_status'];
			$rc_color=  $row['rc_color'];
			$rc_cubic_cap=  $row['rc_cubic_cap'];
			
			$rc_permanent_address=  escapeString($conn,RemoveSpecialChar($rc_permanent_address));
			
			$rc_permit_no= escapeString($conn,$row['rc_permit_no']);
			$rc_permit_type= escapeString($conn,$row['rc_permit_type']);
			$rc_fuel_desc= escapeString($conn,$row['rc_fuel_desc']);
			$rc_gvw= escapeString($conn,$row['rc_gvw']);
			$rc_non_use_status= escapeString($conn,$row['rc_non_use_status']);
			$rc_pucc_no= escapeString($conn,$row['rc_pucc_no']);
			$rc_registered_at= escapeString($conn,$row['rc_registered_at']);
			
			$rc_present_address=  $row['rc_present_address'];
			$rc_present_address=  escapeString($conn,RemoveSpecialChar($rc_present_address));
			
			echo $rc_permanent_address."<br>".$rc_present_address;
			echo "INSERT INTO zoop_rc (json_data,`request_id`, `request_timestamp`, 
			`rc_blacklist_status`, `rc_body_type_desc`, 
			`rc_chasi_no`, `rc_color`, `rc_cubic_cap`, `rc_eng_no`, `rc_f_name`, `rc_financer`, `rc_fit_upto`, 
			`rc_fuel_desc`, `rc_gvw`, 
			`rc_insurance_comp`, `rc_insurance_policy_no`, `rc_insurance_upto`, `rc_maker_desc`, `rc_maker_model`, 
			`rc_manu_month_yr`,
			`rc_mobile_no`, `rc_no_cyl`, `rc_noc_details`, `rc_non_use_from`, `rc_non_use_status`, 
			`rc_non_use_to`, `rc_norms_desc`, 
			`rc_np_issued_by`, `rc_np_no`, `rc_np_upto`, `rc_owner_name`, `rc_owner_sr`, `rc_permanent_address`, 
			`rc_permit_issue_dt`, 
			`rc_permit_no`, `rc_permit_type`, `rc_permit_valid_from`, `rc_permit_valid_upto`, `rc_present_address`, 
			`rc_pucc_no`, 
			`rc_pucc_upto`, `rc_registered_at`, `rc_regn_dt`, `rc_regn_no`, `rc_seat_cap`, `rc_sleeper_cap`, 
			`rc_stand_cap`, 
			`rc_status`, `rc_status_as_on`, `rc_tax_upto`, `rc_unld_wt`, `rc_vch_catg`, `rc_vh_class_desc`, 
			`rc_wheelbase`, 
			`state_cd`, `branch`, `branch_user`) VALUES ('$output','$response[id]', '$response[request_timestamp]', 
			'$rc_blacklist_status', 
			'$body_type', '$chasi_no', '$rc_color', '$rc_cubic_cap', '$eng_no', '$rc_f_name',
			'$financer', '".@date_format($rc_fit_upto,"Y-m-d")."', '$rc_fuel_desc', '$rc_gvw', 
			'$insurance_comp', '$ins_policy_no', '".@date_format($rc_insurance_upto,"Y-m-d")."', 
			'$maker_name', '$maker_model_name', '$maker_month_year', '$row[rc_mobile_no]', '$row[rc_no_cyl]', 
			'$noc_details', '".@date_format($rc_non_use_from, "Y-m-d")."', '$rc_non_use_status', 
			'".@date_format($rc_non_use_to, "Y-m-d")."', '$rc_norms_desc', '$rc_np_issued_by', '$rc_np_no',
			'".@date_format($rc_np_upto,"Y-m-d")."', '$rc_owner_name', '$row[rc_owner_sr]', '',
			'".@date_format($rc_permit_issue_dt,"Y-m-d")."', '$rc_permit_no', '$rc_permit_type',
			'".@date_format($rc_permit_valid_from,"Y-m-d")."', '".@date_format($rc_permit_valid_upto, "Y-m-d")."',
			'', '$rc_pucc_no', '".@date_format($rc_pucc_upto, "Y-m-d")."', '$rc_registered_at',
			'".@date_format($rc_regn_dt, "Y-m-d")."', '$row[rc_regn_no]', '$row[rc_seat_cap]', '$row[rc_sleeper_cap]', 
			'$row[rc_stand_cap]', '$row[rc_status]', '".@date_format($rc_status_as_on, "Y-m-d")."', '$rc_tax_upto', 
			'$row[rc_unld_wt]', '$row[rc_vch_catg]', '$row[rc_vh_class_desc]', '$row[rc_wheelbase]', '$row[state_cd]', 
			'$branch', '$branch_sub_user')";
		
		$output2 = RemoveSpecialChar($output);
		
		$insert_fetched_data_rc = Qry($conn,"INSERT INTO zoop_rc (json_data,request_id,request_timestamp,
			rc_blacklist_status,rc_body_type_desc,rc_chasi_no,rc_color,rc_cubic_cap,rc_eng_no,rc_f_name,rc_financer,
			rc_fit_upto,rc_fuel_desc,rc_gvw,rc_insurance_comp,rc_insurance_policy_no,rc_insurance_upto,
			rc_maker_desc,rc_maker_model,rc_manu_month_yr,rc_mobile_no,rc_no_cyl,rc_noc_details,rc_non_use_from,
			rc_non_use_status,rc_non_use_to,rc_norms_desc,rc_np_issued_by,rc_np_no,rc_np_upto,rc_owner_name,
			rc_owner_sr,rc_permanent_address,rc_permit_issue_dt,rc_permit_no,rc_permit_type,rc_permit_valid_from,
			rc_permit_valid_upto,rc_present_address,rc_pucc_no,rc_pucc_upto,rc_registered_at,rc_regn_dt,rc_regn_no,
			rc_seat_cap,rc_sleeper_cap,rc_stand_cap,rc_status,rc_status_as_on,rc_tax_upto,rc_unld_wt,rc_vch_catg,
			rc_vh_class_desc,rc_wheelbase,state_cd,branch,branch_user) VALUES ('$output2','$response[id]', '$response[request_timestamp]', 
			'$rc_blacklist_status', 
			'$body_type', '$chasi_no', '$rc_color', '$rc_cubic_cap', '$eng_no', '$rc_f_name',
			'$financer', '".@date_format($rc_fit_upto,"Y-m-d")."', '$rc_fuel_desc', '$rc_gvw', 
			'$insurance_comp', '$ins_policy_no', '".@date_format($rc_insurance_upto,"Y-m-d")."', 
			'$maker_name', '$maker_model_name', '$maker_month_year', '$row[rc_mobile_no]', '$row[rc_no_cyl]', 
			'$noc_details', '".@date_format($rc_non_use_from, "Y-m-d")."', '$rc_non_use_status', 
			'".@date_format($rc_non_use_to, "Y-m-d")."', '$rc_norms_desc', '$rc_np_issued_by', '$rc_np_no',
			'".@date_format($rc_np_upto,"Y-m-d")."', '$rc_owner_name', '$row[rc_owner_sr]', '$rc_permanent_address',
			'".@date_format($rc_permit_issue_dt,"Y-m-d")."', '$rc_permit_no', '$rc_permit_type',
			'".@date_format($rc_permit_valid_from,"Y-m-d")."', '".@date_format($rc_permit_valid_upto, "Y-m-d")."',
			'$rc_present_address', '$rc_pucc_no', '".@date_format($rc_pucc_upto, "Y-m-d")."', '$rc_registered_at',
			'".@date_format($rc_regn_dt, "Y-m-d")."', '$row[rc_regn_no]', '$row[rc_seat_cap]', '$row[rc_sleeper_cap]', 
			'$row[rc_stand_cap]', '$row[rc_status]', '".@date_format($rc_status_as_on, "Y-m-d")."', '$rc_tax_upto', 
			'$row[rc_unld_wt]', '$row[rc_vch_catg]', '$row[rc_vh_class_desc]', '$row[rc_wheelbase]', '$row[state_cd]', 
			'$branch', '$branch_sub_user')");

			if(!$insert_fetched_data_rc){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$udpate_new_timestamp = Qry($conn,"UPDATE mk_truck SET last_verify='$timestamp' WHERE id='$oid'");

			if(!$udpate_new_timestamp){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$insert_err2 = Qry($conn,"INSERT INTO zoop_error_log(req_id,api_name,doc_no,branch,branch_user,timestamp) 
			VALUES ('$req_id','RC','$tno','$branch','$branch_sub_user','$timestamp')");
		
			if(!$insert_err2){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
	}
	else
	{
		$is_api_error = true;
		$api_err_code = $response['response_code'];
		$api_err_msg = $response['response_msg'];
		
		if($api_err_code=="110")
		{
			$insert_err = Qry($conn,"INSERT INTO zoop_error_log(is_error,req_id,api_name,doc_no,branch,branch_user,error_code,error_desc,timestamp) 
			VALUES ('1','$req_id','RC','$tno','$branch','$branch_sub_user','$api_err_code','$api_err_msg','$timestamp')");
		
			if(!$insert_err){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$_SESSION['verify_vehicle_type'] = "MARKET";
			$_SESSION['verify_vehicle'] = $tno;
			$_SESSION['verify_vehicle_id'] = $tno_id;
			$_SESSION['verify_vehicle_wheeler'] = $wheeler;

			echo "<script>
				$('.veh_sel_dropdown').attr('disabled',true);
				$('#VEH_MARKET').attr('disabled',false);
			</script>";

			if($branch=='PEN')
			{
				$_SESSION['verify_driver'] = "PEN_DRIVER";
				$_SESSION['verify_driver_id'] = "NA";
				
				echo "<script>
					$('#d_lic_no').attr('readonly',true);
					$('#verify_dl_div').show();
					$('#verify_btn_div_dl').hide();
					$('#is_dl_verified').val('1')
				</script>";
			}

			echo "<script>
					// $('#close_verify_modal')[0].click();
					// $('#function_result').html('');
					// $('#button_print_modal_dl')[0].click();
					// $('#button_verify_1')[0].click();
					$('#veh_no_verify_market').attr('readonly',true);
					$('#verify_rc_div').show();
					$('#verify_btn_div_rc').hide();
					$('#is_rc_verified').val('1')
					$('#loadicon').hide();
				</script>";
				exit();
		}
		else
		{
			$insert_err = Qry($conn,"INSERT INTO zoop_error_log(is_error,req_id,api_name,doc_no,branch,branch_user,error_code,error_desc,timestamp) 
			VALUES ('1','$req_id','RC','$tno','$branch','$branch_sub_user','$api_err_code','$api_err_msg','$timestamp')");
		
			if(!$insert_err){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$_SESSION['verify_vehicle_type'] = "MARKET";
			$_SESSION['verify_vehicle'] = $tno;
			$_SESSION['verify_vehicle_id'] = $tno_id;
			$_SESSION['verify_vehicle_wheeler'] = $wheeler;

			echo "<script>
				$('.veh_sel_dropdown').attr('disabled',true);
				$('#VEH_MARKET').attr('disabled',false);
			</script>";

			if($branch=='PEN')
			{
				$_SESSION['verify_driver'] = "PEN_DRIVER";
				$_SESSION['verify_driver_id'] = "NA";
				
				echo "<script>
					$('#d_lic_no').attr('readonly',true);
					$('#verify_dl_div').show();
					$('#verify_btn_div_dl').hide();
					$('#is_dl_verified').val('1')
				</script>";
			}

			echo "<script>
					// $('#close_verify_modal')[0].click();
					// $('#function_result').html('');
					// $('#button_print_modal_dl')[0].click();
					// $('#button_verify_1')[0].click();
					$('#veh_no_verify_market').attr('readonly',true);
					$('#verify_rc_div').show();
					$('#verify_btn_div_rc').hide();
					$('#is_rc_verified').val('1')
					$('#loadicon').hide();
				</script>";
				exit();

			// $content = "Code: ".$response['response_code']." ".$response['response_msg'];
			// $content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
			// echo "<script>Swal.fire({
					// icon: 'error',
					// title: 'API Error !!!',
					// text: '$content'
				// })
			// $('#loadicon').hide();	
			// </script>";
			// exit();
		}
	}
}

$get_data1  = Qry($conn,"select * from zoop_rc where rc_regn_no='$tno' order by id desc limit 1");

if(!$get_data1){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}
	
$row = fetchArray($get_data1);

		if($row['rc_blacklist_status']!='NA')
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'RC Error !!!',
				text: 'Rc blacklisted !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$diff_fitness = (strtotime($row['rc_fit_upto'])- strtotime($date))/24/3600; 
		$diff_insurance = (strtotime($row['rc_insurance_upto'])- strtotime($date))/24/3600; 
		$diff_permit = (strtotime($row['rc_permit_valid_upto'])- strtotime($date))/24/3600; 
		$diff_permit_np = (strtotime($row['rc_np_upto'])- strtotime($date))/24/3600; // national permit
		$diff_puc = (strtotime($row['rc_pucc_upto'])- strtotime($date))/24/3600; 
		
		// if($row['rc_np_upto']=='' AND $row['rc_permit_valid_upto']=='')
		// {
			// echo "<script>Swal.fire({
				// icon: 'error',
				// title: 'Permit Error !!!',
				// text: 'Permit not found !!'
				// })
				// $('#loadicon').hide();
			// </script>";
			// exit();
		// }

		// if($row['rc_pucc_upto']=='' || $row['rc_pucc_upto']==0)
		// {
			// echo "<script>Swal.fire({
				// icon: 'error',
				// title: 'PUC Error !!!',
				// text: 'PUC not found !!'
				// })
				// $('#loadicon').hide();
			// </script>";
			// exit();
		// }		
		
		// if($row['rc_fit_upto']=='' || $row['rc_fit_upto']==0)
		// {
			// echo "<script>Swal.fire({
				// icon: 'error',
				// title: 'Fitness Error !!!',
				// text: 'Fitness not found !!'
				// })
				// $('#loadicon').hide();
			// </script>";
			// exit();
		// }
		
		// if($row['rc_insurance_upto']=='' || $row['rc_insurance_upto']==0)
		// {
			// echo "<script>Swal.fire({
				// icon: 'error',
				// title: 'Insurance Error !!!',
				// text: 'Insurance not found !!'
				// })
				// $('#loadicon').hide();
			// </script>";
			// exit();
		// }
		
		
	if(strtotime("Y-m-d")>strtotime("2021-03-31"))
	{
		if($diff_fitness<=5)
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'Fitness Error !!!',
				text: 'Fitness already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($diff_insurance<=5)
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'Insurance Error !!!',
				text: 'Insurance already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		
		if($diff_permit_np<=5 AND $row['rc_np_upto']!='' AND $row['rc_np_upto']!=0)
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'Permit Error !!!',
				text: 'Permit(national) already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($diff_permit<=5 AND $row['rc_permit_valid_upto']!='' AND $row['rc_permit_valid_upto']!=0)
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'Permit Error !!!',
				text: 'Permit already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($diff_puc<=5 AND $row['rc_pucc_upto']!='' AND $row['rc_pucc_upto']!=0 AND $row['rc_pucc_upto']!='NA')
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'PUC Error !!!',
				text: 'PUC already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
		
$_SESSION['verify_vehicle_type'] = "MARKET";
$_SESSION['verify_vehicle'] = $tno;
$_SESSION['verify_vehicle_id'] = $tno_id;
$_SESSION['verify_vehicle_wheeler'] = $wheeler;

echo "<script>
	$('.veh_sel_dropdown').attr('disabled',true);
	$('#VEH_MARKET').attr('disabled',false);
</script>";

if($branch=='PEN')
{
	$_SESSION['verify_driver'] = "PEN_DRIVER";
	$_SESSION['verify_driver_id'] = "NA";
	
	echo "<script>
		$('#d_lic_no').attr('readonly',true);
		$('#verify_dl_div').show();
		$('#verify_btn_div_dl').hide();
		$('#is_dl_verified').val('1')
	</script>";
}

// echo '<script>
				// Swal.fire({
				// title: "Success",
				// text: "RC Checked Successfully.",
				// icon: "success"
				// }).then(function() {
					// window.location = "rc_api_print.php?id='.$row["rc_regn_no"].'";
				// })
				// </script>';
				// exit();
?>
<style>
table td[class*=col-], table th[class*=col-]{
	font-weight: bold !important;
	color: black;
	line-height: 20px;
}
table td[class*=col-] label, table th[class*=col-] label{ 
	font-weight: normal !important;
	color: black;
}

</style>

<style type="text/css">
@media print
{

body {
   zoom:75%;
 }	
body * { visibility: hidden; }
.container-fluid * { visibility: visible; }
.container-fluid { position: absolute; top: 0; left: 0; }
 

.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
  float: left;
}
.col-md-12 {
  width: 100%;
}
.col-md-11 {
  width: 91.66666666666666%;
}
.col-md-10 {
  width: 83.33333333333334%;
}
.col-md-9 {
  width: 75%;
}
.col-md-8 {
  width: 66.66666666666666%;
}
.col-md-7 {
  width: 58.333333333333336%;
}
.col-md-6 {
  width: 50%;
}
.col-md-5 {
  width: 41.66666666666667%;
}
.col-md-4 {
  width: 33.33333333333333%;
 }
 .col-md-3 {
   width: 25%;
 }
 .col-md-2 {
   width: 16.666666666666664%;
 }
 .col-md-1 {
  width: 8.333333333333332%;
 }

 	td {
  	 font-weight: bold !important;
  	 font-size: 14px !important;
  	 	line-height: 25px !important;

 	}
 	label {
  	 font-weight: normal !important;
  	 font-size: 12px !important;
	}	

}

</style>
<script type="text/javascript">
	function callprint(){
		var css = '@page { size: landscape; }',
		head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');

		style.type = 'text/css';
		style.media = 'print';

		if (style.styleSheet){
		style.styleSheet.cssText = css;
		} else {
		style.appendChild(document.createTextNode(css));
		}

		head.appendChild(style);

		window.print();
	}
</script>

<div style="text-transform" class="modal fade" id="RcPrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
            <div class="modal-body" style="max-height: calc(100vh - 70px);overflow: auto;">
         <!--   <div class="modal-body" style="max-height: calc(100vh - 70px);overflow: auto;">-->

	<div class="row">
	
	<div class="form-group col-md-4">
		<button type="button" style="margin:10px" onclick="$('#button_verify_1')[0].click();$('#veh_no_verify_market').attr('readonly',true);$('#verify_rc_div').show();$('#verify_btn_div_rc').hide();$('#is_rc_verified').val('1')" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>		
	
	<div class="form-group col-md-4">
		<center><span style="font-weight: bold; font-size: 18px;">Registration No: <?php echo $tno; ?> </span> </center>
	</div>
	
	 <div class="form-group col-md-4" style="font-size:12px;">
		<span class="pull-right"></span>
	</div>

</div>

<div class="row">
<style type="text/css">

@media all {    
    .watermark {
        display: inline;
        position: fixed !important;
        opacity: 0.1;
        font-size: 5em;
        font-weight: bold;
        width: 100%;
        text-align: center;
        z-index: 1000;
        top: 300px;
        right:150px;
        transform:rotate(330deg);
    -webkit-transform:rotate(330deg);
    }
}
</style>
<?php


?>
	<div class="watermark">RAMAN ROADWAYS PVT LTD</div>

<div class="col-md-12 table-responsive" style='overflow-x:auto'>
<table class="alldata" border="0" style="width:100%;font-size:12.5px">
<tr>
<td class="col-md-6" colspan="2"><label>RC Last Updated at </label> <?php echo $row['request_timestamp']; ?></td>
</tr>
<tr>
<td class="col-md-6"><label>Vehicle Blacklist status: </label> <?php echo $row['rc_blacklist_status']; ?> </td>
<td class="col-md-6"><label>Body Type of the Vehicle: </label> <?php echo $row['rc_body_type_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Chassis Number of the Vehicle: </label> <?php echo $row['rc_chasi_no']; ?> </td>
<td class="col-md-6"><label>Registered Color of the Vehicle: </label> <?php echo $row['rc_color']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Registered Color of the Vehicle: </label> <?php echo $row['rc_cubic_cap']; ?> </td>
<td class="col-md-6"><label>Engine Number of the vehicle: </label> <?php echo $row['rc_eng_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Father's Name of Registered Owner of the vehicle: </label> <?php echo $row['rc_f_name']; ?> </td>
<td class="col-md-6"><label>Name of Vehicle Financier: </label> <?php echo $row['rc_financer']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Date of Validity of Vehicle Fitness certificate: </label> <?php if($row['rc_fit_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_fit_upto'])); ?> </td>
<td class="col-md-6"><label>Vehicle Fuel Type: </label> <?php echo $row['rc_fuel_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Gross Weight of the Vehicle: </label> <?php echo $row['rc_gvw']; ?> </td>
<td class="col-md-6"><label>Insurer Name of the Vehicle: </label> <?php echo $row['rc_insurance_comp']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Insurance Policy Number of the Vehicle: </label> <?php echo $row['rc_insurance_policy_no']; ?> </td>
<td class="col-md-6"><label>Date of validity of RC Insurance: </label> <?php if($row['rc_insurance_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_insurance_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Name of Vehicle Manufacturer: </label> <?php echo $row['rc_maker_desc']; ?> </td>
<td class="col-md-6"><label>Vehicle Model and Make: </label> <?php echo $row['rc_maker_model']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Month & Year of Vehicle Manufacture: </label> <?php echo $row['rc_manu_month_yr']; ?> </td>
<td class="col-md-6"><label>Registered owner Mobile Number: </label> <?php echo $row['rc_mobile_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Number of Cylinders: </label> <?php echo $row['rc_no_cyl']; ?> </td>
<td class="col-md-6"><label>No objection certificate in case of vehicle transfer from one state to another: </label> <?php echo $row['rc_noc_details']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>When NOC was issued: </label> <?php if($row['rc_non_use_from']!=NULL) echo date('d-M-Y', strtotime($row['rc_non_use_from'])); ?> </td>
<td class="col-md-6"><label>No objection certificate status: </label> <?php echo $row['rc_non_use_status']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Expiry date of NOC: </label> <?php if($row['rc_non_use_to']!=NULL) echo date('d-M-Y', strtotime($row['rc_non_use_to'])); ?> </td>
<td class="col-md-6"><label>Vehicle Pollution Norms Description: </label> <?php echo $row['rc_norms_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>National permit issued by RTO: </label> <?php echo $row['rc_np_issued_by']; ?> </td>
<td class="col-md-6"><label>National permit issued number: </label> <?php echo $row['rc_np_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Validity of national permit: </label> <?php if($row['rc_np_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_np_upto'])); ?> </td>
<td class="col-md-6"><label>Registered Name of Owner: </label> <?php echo $row['rc_owner_name']; ?> </td>
</tr><tr>
	<td class="col-md-6" colspan="2"><label>Registered Permanent Address of the Vehicle Owner: </label> <?php echo $row['rc_permanent_address']; ?> </td>
</tr>
<tr>
	<td class="col-md-6" colspan="2"><label>Registered Present Address of the Vehicle Owner: </label> <?php echo $row['rc_present_address']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label>Owner serial no.: </label> <?php echo $row['rc_owner_sr']; ?> </td>
<td class="col-md-6"><label>Permit issue date: </label><?php if($row['rc_permit_issue_dt']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_issue_dt'])); ?></td>
</tr><tr>
<td class="col-md-6"><label>Permit number: </label> <?php echo $row['rc_permit_no']; ?> </td>
<td class="col-md-6"><label>Type of Permit issued for the vehicle: </label> <?php echo $row['rc_permit_type']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Date of issue of Permit of the vehicle: </label> <?php if($row['rc_permit_valid_from']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_valid_from'])); ?> </td>
<td class="col-md-6"><label>Date of validity of Permit of the vehicle upto: </label> <?php if($row['rc_permit_valid_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_permit_valid_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>PUC Registration Number of the vehicle: </label> <?php echo $row['rc_pucc_no']; ?> </td>
<td class="col-md-6"><label>Expiry date of PUC certificate of the vehicle: </label> <?php if($row['rc_pucc_upto']!=NULL) echo date('d-M-Y', strtotime($row['rc_pucc_upto'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Location of RTO where the vehicle was registered: </label> <?php echo $row['rc_registered_at']; ?> </td>
<td class="col-md-6"><label>Date of Registration of the Vehicle: </label> <?php if($row['rc_regn_dt']!=NULL) echo date('d-M-Y', strtotime($row['rc_regn_dt'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Registration number of the Vehicle: </label> <?php echo $row['rc_regn_no']; ?> </td>
<td class="col-md-6"><label>Vehicle Passenger Seating Capacity: </label> <?php echo $row['rc_seat_cap']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Maximum Sleeper Capacity: </label> <?php echo $row['rc_sleeper_cap']; ?> </td>
<td class="col-md-6"><label>Capacity of Standing Passengers in the Vehicle: </label> <?php echo $row['rc_stand_cap']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Active/NOC issued/De registered: </label> <?php echo $row['rc_status']; ?> </td>
<td class="col-md-6"><label>Date of RC Status Verification: </label> <?php if($row['rc_status_as_on']!=NULL) echo date('d-M-Y', strtotime($row['rc_status_as_on'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Duration till the Tax on the Vehicle has been paid (Life time / One time): </label> <?php if($row['rc_tax_upto']!='LTT') { echo date('d-M-Y', strtotime($row['rc_tax_upto'])); } else { echo 'LTT'; } ?>
 </td>
<td class="col-md-6"><label>Unladden Weight of the Vehicle: </label> <?php echo $row['rc_unld_wt']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Vehicle category: </label> <?php echo $row['rc_vch_catg']; ?> </td>
<td class="col-md-6"><label>Description of Vehicle Class: </label> <?php echo $row['rc_vh_class_desc']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Wheelbase in mm of the vehicle: </label> <?php echo $row['rc_wheelbase']; ?> </td>
<td class="col-md-6"><label>State code: </label> <?php echo $row['state_cd']; ?> </td>
</tr>

</table>
</div>
</div>



	</div>	
   </div>
  </div>
 </div>
 
 <button id="button_print_modal" type="button" data-toggle="modal" data-target="#RcPrintModal" style="display:none"></button>
 
<script>
	$('#close_verify_modal')[0].click();
	// $('#function_result').html('');
	$('#button_print_modal')[0].click();
	$('#loadicon').hide();
</script>
