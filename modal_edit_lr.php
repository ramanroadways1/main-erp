<button style="display:none" id="edit_modal_button" data-toggle="modal" data-target="#EditLRModal"></button>

<style>
input[type="text"],input[type="date"],input[type="number"],select,label,textarea {
   font-size:12px !important;
}
</style>

<form action="" id="EditLRForm" method="POST" autocomplete="off" style="font-size:12px !important">

<div style="background:lightblue" class="modal fade" id="EditLRModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
            <div class="modal-body">
	
<div class="row">
	
<!--
	<div class="form-group col-md-3">
		<label>Vehicle Type <font color="red">*</font></label>
		<input type="text" readonly name="lr_type" id="lr_type_edit" class="form-control" required="required">
	</div>
							
	<div class="form-group col-md-3" id="div_market_edit" style="display:none">
		<label>Market Truck No <font color="red">*</font></label>
		<input readonly oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_market" id="tno_market_edit" type="text" class="form-control" required />
    </div>
	
	<input type="hidden" name="wheeler_market" id="wheeler_market_truck_edit">
	<input type="hidden" name="wheeler_own" id="wheeler_own_truck_edit">
	
	<div class="form-group col-md-3" id="div_own_edit" style="display:none">
		<label>Own Truck No <font color="red">*</font></label>
		<input readonly oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_own" id="tno_own_edit" type="text" class="form-control" required />
	</div>
	
	<div class="form-group col-md-3">
		<label>LR Date <font color="red">*</font></label>
		<input readonly id="lr_date_edit" name="date" type="date" class="form-control" min="<?php// echo $min_lr_date; ?>" 
		max="<?php// echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="form-group col-md-3">
		<label>From Station <font color="red">*</font></label>
		<input readonly <?php //if($branch=='KORBA'){echo "readonly"; } ?> type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" name="from" id="from_edit" value="<?php //if($branch=='KORBA'){echo "KORBA"; } ?>" class="form-control" required />
    </div>
	
	<input type="hidden" id="from_id_edit" value="<?php// if($branch=='KORBA'){echo "1326"; } ?>" name="from_id">
	<input type="hidden" id="to_id_edit" name="to_id">
	
	<div class="form-group col-md-3">
		<label>To Station <font color="red">*</font></label>
		<input readonly name="to" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" type="text" id="to_edit" class="form-control" required />
	</div>
	
	<div style="display:none" id="dest_zone_div_edit" class="form-group col-md-3">
		<label>Destination Zone <font color="red">*</font></label>
		<input name="dest_zone" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'')" type="text" id="dest_zone_edit" class="form-control" required />
	</div>

	<input type="hidden" id="dest_zone_id_edit" name="dest_zone_id">
	
	<div class="form-group col-md-3">
		<label>Consignee <font color="red">*</font></label>
		<input readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z.-]/,'')" id="consignee_edit" name="consignee" class="form-control" required />
	</div>

	<div class="form-group col-md-3">
		<label>Consignee GST No </label>
		<input id="con2_gst_edit" type="text" name="con2_gst" class="form-control" readonly />
	</div>
	
	<input type="hidden" id="consignee_id_edit" name="consignee_id">
	
-->	
	<div class="form-group col-md-3">
		<label>LR No. <font color="red">*</font></label>
		<input type="text" name="lrno" id="lrno_edit" readonly class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>DO No. <font color="red">*</font></label>
		<input type="text" name="do_no" id="do_no_edit" class="form-control" required />
	</div>
	
    <div class="form-group col-md-3">
		<label>Invoice No. <font color="red">*</font></label>
		<input name="invno" type="text" id="invno_edit" class="form-control" required />
    </div>
   
	<div class="form-group col-md-3">
		<label>Shipment/BE/LC No. <font color="red">*</font></label>
		<input type="text" name="shipno" id="shipno_edit" class="form-control" required />
     </div>
	
	<div class="form-group col-md-3">
		<label>PO No. <font color="red">*</font></label>
		<input name="po_no" id="po_no_edit" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-2">
		<label>Act.Wt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" name="act_wt" type="number" id="act_wt_edit" step="any" class="form-control" required />
    </div>  
	
	<div class="form-group col-md-2">
		<label>GrossWt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" name="gross_wt" type="number" id="gross_wt_edit" step="any" class="form-control" required />
    </div>  
	
	<div class="form-group col-md-2">
       <label>Chrg.Wt (in MT) <font color="red">*</font></label>
	   <input onchange="ResetAll_edit();" max="<?php if($branch=='MAKRANA'){echo '85';}else{echo '50';} ?>" oninput="ResetAll_edit();;" onblur="ResetAll();" min="0.010" name="chrg_wt" type="number" id="chrg_wt_edit" class="form-control" step="any" required />
    </div>
	
<!--
	<div class="form-group col-md-3">
		<label>Material/Item <font color="red">*</font></label>
		<input id="item1_edit" name="item" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');" placeholder="Product/Item" class="form-control" required />
    </div>
	
	<input type="hidden" name="item_id" id="item_id_edit">
-->
	
	<div class="form-group col-md-12">
		<span style="font-size:13px;font-weight:bold;color:red">----- Optional fields ------</span>
	</div>
	
	<div class="form-group col-md-3">
		<label>Bank Name </label>
		<input type="text" id="bank_edit" name="bank" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>Sold To PARTY </label>
		<input type="text" id="sold_to_pay_edit" name="sold_to_party" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>LR Name </label>
		<input type="text" id="lr_name_edit" name="lr_name" class="form-control" />
	</div>

	<div class="form-group col-md-3">
		<label>No of Articles </label>
		<input placeholder="No of Bags Etc." name="articles" id="articles_edit" type="text" class="form-control" />
    </div>

	<div class="form-group col-md-2">
		<label>Billing Rate <span id="rate_req"><font color="red">*</font></span></label>
		<input onchange="sum1_edit();" onkeyup="sum1_edit();" oninput="sum1_edit();" onblur="sum1_edit();" value="0" min="0" step="any" name="b_rate" type="number" id="b_rate_edit" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Billing Amount <span id="rate_req"><font color="red">*</font></span></label>
		<input onblur="sum2_edit();" onchange="sum2_edit();" oninput="sum2_edit();" onkeyup="sum2_edit();" min="0" step="any" name="b_amt" type="number" id="b_amt_edit" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Truck Type</label>
		<select name="t_type" id="t_type_edit" class="form-control">
			<option value="">--Select Option--</option>
			<?php
			$get_t_type = Qry($conn,"SELECT title FROM truck_type WHERE hide=0");
			if(numRows($get_t_type)>0)
			{
				while($row_t_type = fetchArray($get_t_type))
				{
					echo "<option value='$row_t_type[title]'>$row_t_type[title]</option>";	
				}
			}
			?>
		</select>
	</div>
	
<?php
/*
if($branch=='KORBA' || $branch=='RAIPUR')
{
?>	
	<div class="form-group col-md-2">
       <label>Freight Type </label>
       <select name="freight_type" id="freight_type_edit" class="form-control">
			<option value="">NULL</option>
			<option value="PREPAID">PREPAID</option>
			<option value="TOPAY">TOPAY</option>
		</select>
	</div>	
<?php
}
else
{
	echo "<input type='hidden' name='freight_type' id='freight_type_edit' value=''>";
}
*/
echo "<input type='hidden' name='freight_type' id='freight_type_edit' value=''>";
?>

	<div class="form-group col-md-3">
		<label>HSN Code </label>
		<input name="hsn_code" id="hsn_code_edit" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" />
    </div>
	
	<div class="form-group col-md-3">
		<label>Description of Goods </label>
		<textarea name="goods_desc" id="goods_desc_edit" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')" class="form-control"></textarea>
    </div>
	
	<div class="form-group col-md-3">
		<label>Consignee Address </label>
		<textarea id="con2_addr_edit" name="con2_addr" class="form-control"></textarea>
	</div>
	
	<input type="hidden" name="id" id="lr_edit_id">
	
	<div class="form-group col-md-12">	
		<button type="submit" id="lr_update_edit" disabled name="lr_sub" style="margin-left:10px;" class="btn btn-sm btn-success pull-right">Update LR</button>
		<button type="button" class="btn btn-default bt-sm pull-right" data-dismiss="modal">Cancel</button>
	</div>	

	</div>	
   </div>
  </div>
 </div>
</div>
</form>  

				<script type="text/javascript">
                     function sum1_edit() 
					 {
						if($("#chrg_wt_edit").val()=='')
						{
							$("#chrg_wt_edit").focus();
							$("#b_rate_edit").val('');
							$("#b_amt_edit").val('');
						}	
						else
						{
							$("#b_amt_edit").val(Math.round(Number($("#chrg_wt_edit").val()) * Number($("#b_rate_edit").val())).toFixed(2));
						}
					 }
					 
					 function sum2_edit() 
					 {
						if($("#chrg_wt_edit").val()=='')
						{
							$("#chrg_wt_edit").focus();
							$("#b_rate_edit").val('');
							$("#b_amt_edit").val('');
						}	
						else
						{
							$("#b_rate_edit").val(Math.round(Number($("#b_amt_edit").val()) / Number($("#chrg_wt_edit").val())).toFixed(2));
						}	
					 }
					 
					 function ResetAll_edit() 
					 {
						$("#b_amt_edit").val('');
						$("#b_rate_edit").val('');
                     }
                </script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditLRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_update_edit").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./_edit_lr_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_edit_lr").html(data);
	},
	error: function() 
	{} });}));});
</script>       

<div id="result_edit_lr"></div>