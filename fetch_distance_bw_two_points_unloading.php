<?php
require_once("./connection.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$loading_loc_lat_long = escapeString($conn,($_POST['loading_point']));
$to_lat_long = escapeString($conn,($_POST['to_lat_long']));
$to_id = escapeString($conn,($_POST['to_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));

if($to_id=='')
{
	echo "<script>
		alert('Location not found !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con2_id=='')
{
	echo "<script>
		alert('Consignee not found !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($to_lat_long=='')
{
	echo "<script>
		alert('Location POI not found !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($loading_loc_lat_long=='')
{
	echo "<script>
		alert('Unloading point not found !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$pincode = explode(",",$loading_loc_lat_long)[2];

$origin = $to_lat_long; 
$destination = explode(",",$loading_loc_lat_long)[0].",".explode(",",$loading_loc_lat_long)[1];
// echo $origin."<br>";
// echo $destination."<br>";
$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=".$google_api_key."";

// echo $origin."<br>";
// echo $destination."<br>";

$api = file_get_contents($url);
$data = json_decode($api);
			
$api_status = $data->rows[0]->elements[0]->status;
	
if($api_status=='NOT_FOUND')
{
	echo "<script>
		alert('Distance not found !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($api_status!='OK')
{
	echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
	echo "<script>
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
			
$dest_addr = $data->destination_addresses[0];
$origin_addr = $data->origin_addresses[0];
$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
$travel_time = $data->rows[0]->elements[0]->duration->text;
$travel_time_value = $data->rows[0]->elements[0]->duration->value;
$travel_hrs = gmdate("H", $travel_time_value);
$travel_minutes = gmdate("i", $travel_time_value);
$travel_seconds = gmdate("s", $travel_time_value);

$get_max_distance = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='LOCATION_MAX_DISTANCE_TO_POI' AND is_active='1'");

if(!$get_max_distance)
{
	echo "<script>alert('Error while processing request !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_max_distance)==0)
{
	$max_distance = "2";
}
else
{
	$row_max_d = fetchArray($get_max_distance);
	$max_distance = $row_max_d['func_value'];
}

if($distance>$max_distance)
{
	echo "<script>
		alert('Invalid unloading point. Distance between location and unloading point is: $distance KMs !');
		$('#loading_point').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	if($pincode=='NA')
	{
		$get_pincode = getZipcode(explode(",",$loading_loc_lat_long)[0].",".explode(",",$loading_loc_lat_long)[1]);

		if(strlen($get_pincode)!=6){
			
			echo "<script>
				alert('Unable to find pincode. Contact system-admin !');
				$('#loading_point').val('');
				$('#button_sub').attr('disabled',true);
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		} else {
			$pincode = $get_pincode;
		}
	}
	
	$_SESSION['unloading_point_type1'] = "GPS";
	$_SESSION['unloading_point_distance_km1'] = $distance;
	$_SESSION['unloading_point_gps_lat'] = explode(",",$loading_loc_lat_long)[0];
	$_SESSION['unloading_point_gps_lng'] = explode(",",$loading_loc_lat_long)[1];
	$_SESSION['unloading_point_gps_pincode'] = $pincode;
	$_SESSION['unloading_point_gps_lat_lng'] = $loading_loc_lat_long;
	$_SESSION['unloading_point_add_con1_id'] = $con2_id;
	$_SESSION['unloading_point_add_from_id'] = $to_id;

	echo "<script>
		$('#button_sub').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>