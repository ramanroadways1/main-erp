<script>
  $(function() {  
      $("#card_no").autocomplete({
			// appendTo: $("#card_no").next()
			source: function(request, response) { 
			if(document.getElementById('fuel_company').value!=''){
                 $.ajax({
                  url: '<?php echo $url_of_diesel_card; ?>',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card_no").val('');
				$("#cardno2").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
              }

              },
              select: function (event, ui) { 
               $('#card_no').val(ui.item.value);   
               $('#cardno2').val(ui.item.dbid);     
			   $('#phy_card_no').val(ui.item.phy_card_no);
			   $('#rilpre').val(ui.item.rilpre);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#cardno2").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
</script> 

<?php
$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='OWN'");
if(!$get_diesel_rate_max){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
				
if(numRows($get_diesel_rate_max)>0)
{
	$row_dsl_max_rate = fetchArray($get_diesel_rate_max);
	$dsl_max_rate=$row_dsl_max_rate['rate'];
	$dsl_max_amount1=$row_dsl_max_rate['amount'];
	$dsl_max_qty1=$row_dsl_max_rate['qty'];
}
else
{
	$dsl_max_rate=95;
	$dsl_max_amount1=38000;
	$dsl_max_qty1=400;
}
?>

<div id="result_modal"></div>

<div class="modal fade" style="background:#AAA" id="diesel_add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
	<form id="InsertDiesel" autocomplete="off"> 
		<div class="modal-body">
		
			<div class="row">
				
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Pump/Card. <font color="red"><sup>*</sup></font></label>
					   <select onchange="CardPump(this.value)" id="type1" name="pump_card" class="form-control" required>
							<option value="">Select option</option>
							<option value="CARD">CARD</option>
							<option value="OTP">OTP</option>
							<option value="PUMP">PUMP</option>
						</select>
                  </div>
               </div>
			  
				<div style="display:none" id="pump_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Pump Name. <font color="red"><sup>*</sup></font></label>
					   <select onchange="SetPumpCode(this.value)" id="pump_name" name="pump_name" class="form-control" required>
						<option value="">Select option</option>
						<?php
						$q_pump=Qry($conn,"SELECT name,code,comp,consumer_pump FROM dairy.diesel_pump_own WHERE code!='' AND branch='$branch' AND 
						active='1' ORDER BY name ASC");
						
						if(!$q_pump){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while processing Request","./");
							exit();
						}
						
						if(numRows($q_pump)>0)
						{
							while($row_p=fetchArray($q_pump))
							{
								// if($row_p['consumer_pump']=="1")
								// {
									// echo "<option disabled='disabled' value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";	
								// }
								// else
								// {
									echo "<option value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";		
								// }
								
							}
						}
						?>
					</select>
                  </div>
               </div>
			   
			   	<div style="display:none" id="qty_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">QTY <font color="red"><sup>*</sup></font></label>
					   <input type="number" min="0" max="<?php echo $dsl_max_qty1; ?>" name="qty" step="any" required id="qty" class="form-control" />
                  </div>
               </div>
			   
			   <div style="display:none" id="rate_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Rate <font color="red"><sup>*</sup></font></label>
					   <input type="number" max="<?php echo $dsl_max_rate; ?>" min="0" name="rate" step="any" required id="rate" class="form-control" />
                  </div>
               </div>
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Amount <font color="red"><sup>*</sup></font></label>
					   <input min="70" max="<?php echo $dsl_max_amount1; ?>" type="number" oninput="AmountCall();" oninput="" name="amount" id="dsl_amt2" class="form-control" required />
                  </div>
               </div>
			   
			 <script>
			function SetPumpCode(code)
			{
				$("#qty").val('')
				$("#rate").val('')
				$("#dsl_amt2").val('')
	
				var parts = code.split('-', 2);
				$('#pump_company').val(parts[1]);
				
				$("#loadicon").show();
				$.ajax({
					url: "get_consumer_pump_data.php",
					method: "post",
					data:'code=' + parts[0], 
					success: function(data){
					$("#result_modal").html(data);
				}})
			}
			</script>
			
			<input type="hidden" name="pump_company" id="pump_company">
			
			 <div id="fuel_div" style="display:none" class="col-md-6">
                 <div class="form-group">
                    <label class="control-label mb-1">Fuel Company. <font color="red"><sup>*</sup></font></label>
					<select id="fuel_company" name="fuel_company" onchange="$('#card_no').val('');$('#phy_card_no').val('')" class="form-control" required>
							<option value="">Select option</option>
							<?php
			$get_fuel_comp_22 = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1' ORDER by name ASC");
			if(!$get_fuel_comp_22){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}

			while($r_f_c2 = fetchArray($get_fuel_comp_22))
			{
				echo "<option value='$r_f_c2[name]'>$r_f_c2[name]</option>";
			}
			?>
					</select>
                  </div>
             </div>
			   
			   <script>
			   function ChkPumpInput()
			   {
				  if($('#fuel_company').val()=='')
				  {
					  alert('Select Fuel Company First');
					  $('#card_no').val('');
					  $('#cardno2').val('');
					  $('#mobile_no').val('');
					  $('#phy_card_no').val('');
				  }					  
			   }
			   </script>
			   
			  <div style="display:none" id="card_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Card No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="card_no" oninput="ChkPumpInput();this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" id="card_no" class="form-control" required />
					   <input type="hidden" id="cardno2" name="cardno2">
				 </div>
              </div>
			   
			  <div style="display:none" id="mobile_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Mobile No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" maxlength="10" minlength="10" oninput="ChkPumpInput();this.value=this.value.replace(/[^0-9]/,'')" name="mobile_no" id="mobile_no" class="form-control" required />
				  </div>
               </div>
			   
			    <div style="display:none" id="phy_card_div" style="display:none" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Virtual Id. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="phy_card_no" id="phy_card_no" class="form-control" readonly required />
				  </div>
               </div>
			   
			   <div style="display:none" id="trans_date_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Transaction Date <font color="red"><sup>*</sup></font></label>
					   <input min="<?php echo date('Y-m-d', strtotime('-7 days')) ?>" max="<?php echo date('Y-m-d'); ?>" type="date" name="trans_date" id="trans_date_diesel" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required />
				  </div>
               </div>
			   
			   <input type="hidden" name="frno_diesel" value="<?php echo $diesel_req; ?>">
			   <input type="hidden" name="vou_type" value="ADV">
			   <input type="hidden" name="rilpre" id="rilpre">
			   
			</div>
		</div>
	
	<div class="modal-footer">
		<button type="button" id="close_diesel_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="insert_diesel_button" class="btn btn-primary">Add Diesel</button>
	</div>
		</form>
		</div>	
     </div>
 </div>	
 
<script type="text/javascript">
$(function() {
$("#qty,#rate").on("keydown keyup blur change input", CalcDiesel);
function CalcDiesel() {
	if($("#qty").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#qty").val());
	}

	if($("#rate").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#rate").val());
	}
	
	$("#dsl_amt2").val(Math.round(qty * rate).toFixed(2));
}});
				 
function AmountCall() 
{
	if($("#qty").val()=='' && $("#rate").val()==''){
		$("#qty").focus()
		$("#dsl_amt2").val('');
	}
	else
	{
		if($("#rate").val()=='' || $("#rate").val()<60)
		{
			$("#rate").val((Number($("#dsl_amt2").val()) / Number($("#qty").val())).toFixed(2));
		}
		else
		{
			$("#qty").val((Number($("#dsl_amt2").val()) / Number($("#rate").val())).toFixed(2));
		}
	}	
}
</script>
				
<script type="text/javascript">
$(document).ready(function (e) {
$("#InsertDiesel").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#insert_diesel_button").attr("disabled", true);
	$.ajax({
	url: 'add_diesel_adv.php',
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_modal").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
function CardPump(card)
{
	$("#qty").val('')
	$("#rate").val('')
	$("#dsl_amt2").val('')
	
	$('#fuel_company').val('');
	$('#card_no').val("");
	$('#cardno2').val("");
	$('#mobile_no').val("");
	$('#phy_card_no').val("");
	
	if(card=='')
	{
		$('#pump_div').hide();	
		$('#card_div').hide();	
		$('#mobile_div').hide();
		$('#fuel_div').hide();	
		$('#qty_div').hide();	
		$('#rate_div').hide();	
		$('#phy_card_div').hide();	
		$('#trans_date_div').hide();	
		
		$('#card_no').attr('required',true);	
		$('#mobile_no').attr('required',true);	
		$('#pump_name').attr('required',true);	
		$('#fuel_company').attr('required',true);	
		$('#pump_company').attr('required',true);
		$('#rate').attr('required',true);	
		$('#qty').attr('required',true);	
		$('#phy_card_no').attr('required',true);
		$('#trans_date_diesel').attr('required',true);
		
		$('#dsl_amt2').attr('oninput','');
	}
	else if(card=='CARD' || card=='OTP')
	{
		if(card=='CARD')
		{
			$('#mobile_div').hide();
			$('#mobile_no').attr("required",false);

			$('#card_div').show();
			$('#card_no').attr("required",true);
			$('#card_no2').attr("required",true);

			$('#phy_card_div').show();	
			$('#phy_card_no').attr('required',true);
		}
		else
		{
			$('#mobile_div').show();
			$('#mobile_no').attr("required",true);

			$('#card_div').hide();
			$('#card_no').attr("required",false);
			$('#card_no2').attr("required",false);

			$('#phy_card_div').hide();	
			$('#phy_card_no').attr('required',false);
		}
		
		$('#fuel_div').show();	
		$('#fuel_company').attr('required',true);
		
		$('#pump_div').hide();	
		$('#qty_div').hide();	
		$('#rate_div').hide();	
		$('#trans_date_div').hide();	
		
		$('#pump_name').attr('required',false);	
		$('#pump_company').attr('required',false);	
		$('#rate').attr('required',false);	
		$('#qty').attr('required',false);	
		$('#trans_date_diesel').attr('required',false);		
		
		$('#dsl_amt2').attr('oninput','');
	}
	else if(card=='PUMP')
	{
		$('#pump_div').show();	
		$('#qty_div').show();	
		$('#rate_div').show();	
		$('#trans_date_div').show();	
		
		$('#card_div').hide();	
		$('#fuel_div').hide();	
		$('#mobile_div').hide();
		$('#phy_card_div').hide();	
		
		$('#rate').attr('required',true);	
		$('#qty').attr('required',true);	
		$('#pump_name').attr('required',true);	
		$('#pump_company').attr('required',true); 
		$('#trans_date_diesel').attr('required',true);
		
		$('#card_no').attr('required',false);	
		$('#card_no2').attr('required',false);	
		$('#fuel_company').attr('required',false);	
		$('#mobile_no').attr('required',false);	
		$('#phy_card_no').attr('required',false);
		
		$('#dsl_amt2').attr('oninput','AmountCall()');
	}
}
</script>
