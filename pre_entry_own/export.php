<?php
require_once("../connection.php");

$branch=escapeString($conn,strtoupper($_SESSION['user']));

$from=escapeString($conn,$_POST['from_date']);
$to=escapeString($conn,$_POST['to_date']);

$output='';

$qry=Qry($conn,"SELECT unq_id,tno,lrno,rate,qty,amount,date,narration FROM dairy.diesel WHERE date BETWEEN '$from' AND '$to' 
AND branch='$branch'");

$output .= '
   <table border="1">  
       <tr>  
        <th>Token No.</th>
		<th>Date</th>
		<th>LR No</th>
		<th>TruckNo</th>
		<th>QTY</th>
		<th>Rate</th>
		<th>Amount</th>
		<th>Diesel Details</th>
	</tr>
  ';
  while($row = fetchArray($qry))
  {
	 $output .= '
				<tr> 
							<td>'.$row["unq_id"].'</td> 
							<td>'.$row["date"].'</td> 
							<td>'.$row["lrno"].'</td> 
							<td>'.$row["tno"].'</td> 
							<td>'.$row["qty"].'</td>  
							<td>'.$row["rate"].'</td>  
							<td>'.$row["amount"].'</td>  
							<td>'.$row["narration"].'</td>  
				</tr>
   ';
  }
  
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_Own.xls');
  echo $output;
  exit();
?>