<?php
require_once '../connection.php';

// if($branch!='SIROHIROAD')
// {
// Redirect("Diesel on hold.","./");
// exit();
// }

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(!isset($_SESSION['diesel_req_own'])){
	echo "<script>
		alert('Something went wrong with session.');
		window.location.href='./';
	</script>";
	exit();
}

$get_last_id = Qry($conn,"SELECT id,unq_id FROM dairy.diesel ORDER BY id DESC LIMIT 1");
if(!$get_last_id){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
}

$get_last_id2 = Qry($conn,"SELECT id,unq_id FROM dairy.diesel_entry ORDER BY id DESC LIMIT 1");
if(!$get_last_id2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
}

if(numRows($get_last_id2)>0)
{
	$row_diesel_1 = fetchArray($get_last_id);
	$row_diesel_2 = fetchArray($get_last_id2);

	if($row_diesel_1['id']!=$row_diesel_2['id'] || $row_diesel_1['unq_id']!=$row_diesel_2['unq_id'])
	{
		SendTransSms("9024281599","OwnTruck Diesel Error : diesel id: $row_diesel_1[id]($row_diesel_1[unq_id]) and diesel_entry id is: $row_diesel_2[id]($row_diesel_2[unq_id]). RAMAN_ROADWAYS.");
		Redirect("Diesel Error Contact Diesel Department.","./");
		exit();
	}
}

$token_no = escapeString($conn,strtoupper($_SESSION['diesel_req_own']));
$tno=escapeString($conn,strtoupper($_POST['tno']));
$lrno=escapeString($conn,strtoupper($_POST['lrno']));

$chk_diesel = Qry($conn,"SELECT id FROM dairy.diesel_sample_own WHERE frno='$token_no'");
if(!$chk_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_diesel)==0)
{
	echo "<script>
		alert('Diesel not added yet !');
		$('#req_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$getTotal = Qry($conn,"SELECT ROUND(SUM(qty),2) AS total_qty,ROUND(SUM(amount),2) AS total_amount FROM dairy.diesel_sample_own 
WHERE frno='$token_no'");
if(!$getTotal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$rowTotal = fetchArray($getTotal);

$totalQty = $rowTotal['total_qty'];
$totalAmount = $rowTotal['total_amount'];

$check_truck=Qry($conn,"SELECT lrno FROM dairy.diesel WHERE date='$date' AND branch='$branch' AND tno='$tno'");
if(!$check_truck){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_truck)>0)
{
	echo "<script>
		alert('You already requested diesel for this vehicle today.');
		window.location.href='./';
	</script>";
	exit();	
}

$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM dairy.diesel_sample_own WHERE frno='$token_no' AND ril_card='1'");
	
if(!$chk_ril_stock){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_ril_stock)>0)
{
	while($row_ril = fetchArray($chk_ril_stock))
	{
		$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
		if(!$get_balance){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_balance = fetchArray($get_balance);
		
		if($row_balance['balance']<$row_ril['amount'])
		{
			echo "<script>
				alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
				window.location.href='./';
			</script>";
			exit();	
		}
	}
}

StartCommit($conn);
$flag = true;

$sql = Qry($conn,"INSERT INTO dairy.diesel(unq_id,tno,lrno,rate,qty,amount,date,narration,branch,branch_user,timestamp,stockid,ril_card) 
SELECT frno,'$tno','$lrno',rate,qty,amount,date,dsl_nrr,branch,'$branch_sub_user',timestamp,IF(purchase_id='',stockid,purchase_id),ril_card 
FROM dairy.diesel_sample_own WHERE frno='$token_no'");

if(!$sql){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$sql2 = Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,narration,branch,
date,timestamp,dsl_mobileno) SELECT frno,bill_no,'$tno',amount,card_pump,card_no,phy_card,fuel_comp,dsl_nrr,branch,date,
timestamp,dsl_mobileno FROM dairy.diesel_sample_own WHERE frno='$token_no'");

if(!$sql2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
 
// $insert1=Qry($conn,"INSERT INTO dairy.diesel(unq_id,tno,lrno,rate,qty,amount,date,narration,branch,branch_user,timestamp) SELECT 
// frno,'$tno','$lrno',rate,qty,amount,date,dsl_nrr,branch,'$branch_sub_user',timestamp FROM dairy.diesel_sample_own WHERE 
// frno='$token_no' AND branch='$branch'");

// if(!$insert1){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }


// $insert2=Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,narration,branch,
// date,dsl_mobileno,timestamp) SELECT frno,bill_no,'$tno',amount,card_pump,card_no,phy_card,fuel_comp,dsl_nrr,branch,date,
// dsl_mobileno,timestamp FROM dairy.diesel_sample_own WHERE frno='$token_no' AND branch='$branch'");

// if(!$insert2){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }
	
$sel_data = Qry($conn,"SELECT id,branch,tno,diesel,card_pump,card,veh_no,dsl_company,dsl_mobileno,timestamp FROM 
dairy.diesel_entry WHERE unq_id='$token_no' AND card_pump='CARD'");
		
if(!$sel_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($sel_data)>0)
{
	while($row_log=fetchArray($sel_data))
	{
			$card_live_bal=Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_log[card]'");
			if(!$card_live_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}

			$row_card_bal=fetchArray($card_live_bal); 
			$live_bal = $row_card_bal['balance']; 

			$qry_log=Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,
			dslcomp,dsltype,cardno,vehno,cardbal,amount,mobileno,reqid) VALUES 
			('$row_log[branch]','SUCCESS','New Request Added','$row_log[timestamp]','OWN','$tno','$row_log[dsl_company]',
			'CARD','$row_log[card]','$row_log[veh_no]','$live_bal','$row_log[diesel]','$row_log[dsl_mobileno]','$row_log[id]')");
			
			if(!$qry_log){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
	}
}

$update_stock = Qry($conn,"SELECT qty,tank_id FROM dairy.diesel_sample_own WHERE frno='$token_no' AND consumer_pump='1'");
if(!$update_stock){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($update_stock)>0)
{
	while($rowStock = fetchArray($update_stock))
	{
		$updateStock1 = Qry($conn,"UPDATE dairy.diesel_pump_stock SET balance=balance-'$rowStock[qty]' WHERE id='$rowStock[tank_id]'");
		if(!$updateStock1){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}	

$chk_ril_stock2 = Qry($conn,"SELECT phy_card,amount FROM dairy.diesel_sample_own WHERE frno='$token_no' AND ril_card='1'");
	
if(!$chk_ril_stock2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_ril_stock2)>0)
{
	while($row_ril2 = fetchArray($chk_ril_stock2))
	{
		$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
		cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
		
		if(!$update_ril_stock){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$dlt_old=Qry($conn,"DELETE FROM dairy.diesel_sample_own WHERE frno='$token_no'");	

if(!$dlt_old){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$updateTodayData = Qry($conn,"UPDATE today_data SET diesel_qty_own=diesel_qty_own+'$totalQty',
diesel_amount_own=diesel_amount_own+'$totalAmount' WHERE branch='$branch'");

if(!$updateTodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	unset($_SESSION['diesel_req_own']);
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Request submitted successfully.');
		window.location.href='./';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>