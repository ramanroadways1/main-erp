<?php
require_once("../connection.php");

$timestamp = date("Y-m-d H:i:s"); 

$code=escapeString($conn,strtoupper($_POST['code']));

$getConsumerPump = Qry($conn,"SELECT consumer_pump FROM dairy.diesel_pump_own WHERE code='$code'");
if(!$getConsumerPump){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$rowComp = fetchArray($getConsumerPump);

if($rowComp['consumer_pump']=="1")
{
	echo "<script>
		$('#rate').attr('readonly',true);
		$('#dsl_amt2').attr('oninput','');
		$('#dsl_amt2').attr('readonly',true);
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		$('#rate').attr('readonly',false);
		$('#dsl_amt2').attr('oninput','AmountCall()');
		$('#dsl_amt2').attr('readonly',false);
		$('#loadicon').hide();
	</script>";
}
?>