<?php
require_once("../connection.php");

// if($branch!='CGROAD')
// {
// Redirect("Diesel on hold.","./");
// exit();
// }
$timestamp = date("Y-m-d H:i:s"); 

$frno = escapeString($conn,strtoupper($_POST['frno_diesel']));
$vou_type = escapeString($conn,strtoupper($_POST['vou_type']));

if(empty($frno))
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Data : Vou Id.');
		window.location.href='./';
	</script>";
	exit();
}

if(empty($vou_type))
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Data : Vou Type.');
		window.location.href='./';
	</script>";
	exit();
}

$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='OWN'");
if(!$get_diesel_rate_max){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
				
$row_dsl_max_rate = fetchArray($get_diesel_rate_max);

$dsl_max_rate=$row_dsl_max_rate['rate'];
$dsl_max_amount=$row_dsl_max_rate['amount'];
$dsl_max_qty=$row_dsl_max_rate['qty'];

$amount = escapeString($conn,$_POST['amount']);

$pump_card = escapeString($conn,strtoupper($_POST['pump_card']));
$rilpre = escapeString($conn,strtoupper($_POST['rilpre']));

if($_POST['rate']!=0 AND $_POST['rate']>$dsl_max_rate)
{
	echo "<script type='text/javascript'>
		alert('Error : Rate is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($_POST['qty']!=0 AND $_POST['qty']>$dsl_max_qty)
{
	echo "<script type='text/javascript'>
		alert('Error : Qty is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($amount!=0 AND $amount>$dsl_max_amount)
{
	echo "<script type='text/javascript'>
		alert('Error : Amount is out of Limit.');
		$('#insert_diesel_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($pump_card=='CARD')
{
	$card=escapeString($conn,strtoupper($_POST['card_no']));
	$type='CARD';
	$rate=0;
	$qty=0;
	$card_id=escapeString($conn,strtoupper($_POST['cardno2']));
	$phy_card_no=escapeString($conn,strtoupper($_POST['phy_card_no']));
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$trans_date = date("Y-m-d");
	$mobile_no="";
	$consumer_pump = "0";
}
else if($pump_card=='OTP')
{
	$card_id="0";
	$card=escapeString($conn,strtoupper($_POST['mobile_no']));
	$mobile_no=escapeString($conn,strtoupper($_POST['mobile_no']));
	$type="OTP";
	$rate=0;
	$qty=0;
	$phy_card_no=$mobile_no;
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company']));
	$trans_date = date("Y-m-d");
	$consumer_pump = "0";
}
else
{
	$consumer_pump = "0";
	$mobile_no="";	
	$card_id="0";		
	$card=escapeString($conn,strtoupper($_POST['pump_name']));	
	$card=strtok($card,'-');
	$phy_card_no=$card;
	$fuel_company=escapeString($conn,strtoupper($_POST['pump_company']));
	$type='PUMP';
	$rate=$_POST['rate'];
	$qty=$_POST['qty'];
	$trans_date = escapeString($conn,strtoupper($_POST['trans_date']));
	
	// Consumer Pump Code

if($qty<=0)
{
	echo "<script type='text/javascript'>
			alert('Error : Invalid Qty.');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
	</script>";
	exit();
}

$check_consumer = Qry($conn,"SELECT id FROM dairy.diesel_pump_stock WHERE pumpcode='$card'");
if(!$check_consumer){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_consumer)>0)
{
	$check_stock = Qry($conn,"SELECT SUM(balance) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$card'");
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_balance = fetchArray($check_stock);
	
	if($row_balance['balance']<$qty)
	{
		echo "<script type='text/javascript'>
				alert('Error : Diesel not in Stock.');
				$('#insert_diesel_button').attr('disabled', false);
				$('#loadicon').hide();
		</script>";
		exit();
	}
}

$get_qty = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$card' AND balance>0");
if(!$get_qty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numRowsQty = numRows($get_qty);

// if($numRowsQty==0)
// {
	// echo "<script type='text/javascript'>
			// alert('Error : Diesel not in stock.');
			// $('#insert_diesel_button').attr('disabled', false);
			// $('#loadicon').hide();
		// </script>";
	// exit();
// }
// else
if($numRowsQty==1)
{
	$row_Qty = fetchArray($get_qty);
	
	if($row_Qty['balance']<$qty)
	{
		echo "<script type='text/javascript'>
			alert('Consumer Pump not in Stock. Code: 001');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$remainQty = sprintf("%.2f",$row_Qty['balance']-$qty);
	$StockId = $row_Qty['id'];
	$PurchaseId = $row_Qty['purchaseid'];
	
	$rate = $row_Qty['rate'];
	$amount = round($qty*$rate);
	$consumer_pump = "1";
}
else if($numRowsQty==2)
{
	$consumer_pump = "1";
	
	$get_first_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$card' AND 
	balance>0 ORDER BY id ASC LIMIT 1");
	
	if(!$get_first_pump){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$get_second_pump = Qry($conn,"SELECT id,purchaseid,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$card' AND 
	balance>0 ORDER BY id DESC LIMIT 1");
	if(!$get_second_pump){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$rowFirstPump = fetchArray($get_first_pump);
	$rowSecondPump = fetchArray($get_second_pump);
	
	if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
	{
		echo "<script type='text/javascript'>
			alert('Consumer Pump not in Stock. Code:002.');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($rowFirstPump['balance']>=$qty)
	{
		$split_entry="0";
		$rate = $rowFirstPump['rate'];
		$amount = round($qty*$rate);
		$remainQty = sprintf("%.2f",$rowFirstPump['balance']-$qty);
		$StockId = $rowFirstPump['id'];
		$PurchaseId = $rowFirstPump['purchaseid'];
	}
	else
	{
		$split_entry="1";
		
		$rate = $rowFirstPump['rate'];
		$rate2 = $rowSecondPump['rate'];
		$qty1 = $rowFirstPump['balance'];
		$qty2 = sprintf("%.2f",$qty-$qty1);
		$amount = round($rate * $qty1);
		$amount2 = round($rate2 * $qty2);
		$StockId = $rowFirstPump['id'];
		$StockId2 = $rowSecondPump['id'];
		$remainQty = 0;
		$remainQty2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
		$PurchaseId = $rowFirstPump['purchaseid'];
		$PurchaseId2 = $rowSecondPump['purchaseid'];
	}
}
else if($numRowsQty>2)
{
	echo "<script>
		alert('Multiple Records found of fuel station.');
		window.location.href='./';
	</script>";
	exit();
}
	// Consumer Pump Code Ends
}

if($rilpre=="1")
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Stock not found !');
			window.location.href='./';
		</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$amount)
	{
		echo "<script>
			alert('Card not in stock. Available Balance is : $row_ril_stock[balance].');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#insert_diesel_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

$date=date("Y-m-d");

$delete = Qry($conn,"DELETE FROM dairy.diesel_sample_own WHERE date!='$date'");
if(!$delete){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($type=='PUMP')
{
	$done_one="1";
	$done_time=$timestamp;
}
else
{
	$done_one="0";
	$done_time="";
}

if($consumer_pump=="1")
{
	if($numRowsQty==1)
	{
		$insert = Qry($conn,"INSERT INTO dairy.diesel_sample_own (frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,
		ril_card,fuel_comp,dsl_nrr,branch,date,dsl_mobileno,consumer_pump,tank_id,purchase_id,timestamp,stockid) VALUES ('$frno','$qty','$rate','$amount',
		'$type','$card','$phy_card_no','$card_id','$rilpre','$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date',
		'$mobile_no','1','$StockId','$PurchaseId','$timestamp','$ril_stockid')");
		
		if(!$insert){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}	
	}
	else
	{
		if($split_entry==0)
		{

		$insert=Qry($conn,"INSERT INTO dairy.diesel_sample_own (frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,
		ril_card,fuel_comp,dsl_nrr,branch,date,dsl_mobileno,consumer_pump,tank_id,purchase_id,timestamp,stockid) VALUES ('$frno','$qty','$rate','$amount',
		'$type','$card','$phy_card_no','$card_id','$rilpre','$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date',
		'$mobile_no','1','$StockId','$PurchaseId','$timestamp','$ril_stockid')");	
		
		if(!$insert){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}	
	
		}
		else
		{
			
		$insert=Qry($conn,"INSERT INTO dairy.diesel_sample_own (frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,
		ril_card,fuel_comp,dsl_nrr,branch,date,dsl_mobileno,consumer_pump,tank_id,purchase_id,timestamp,stockid) VALUES ('$frno','$qty1','$rate','$amount',
		'$type','$card','$phy_card_no','$card_id','$rilpre','$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date',
		'$mobile_no','1','$StockId','$PurchaseId','$timestamp','$ril_stockid')");
		
		if(!$insert){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}	

		$insert2=Qry($conn,"INSERT INTO dairy.diesel_sample_own (frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,
		ril_card,fuel_comp,dsl_nrr,branch,date,dsl_mobileno,consumer_pump,tank_id,purchase_id,timestamp,stockid) VALUES ('$frno','$qty2','$rate2',
		'$amount2','$type','$card','$phy_card_no','$card_id','$rilpre','$fuel_company','$fuel_company-$phy_card_no Rs: $amount2/-','$branch',
		'$trans_date','$mobile_no','1','$StockId2','$PurchaseId2','$timestamp','$ril_stockid')");	

		if(!$insert2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}			
			
		}
	}
}
else
{
	$insert=Qry($conn,"INSERT INTO dairy.diesel_sample_own (frno,qty,rate,amount,card_pump,card_no,phy_card,card_id,ril_card,fuel_comp,
	dsl_nrr,branch,date,dsl_mobileno,timestamp,stockid) VALUES ('$frno','$qty','$rate','$amount','$type','$card','$phy_card_no','$card_id','$rilpre',
	'$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date','$mobile_no','$timestamp','$ril_stockid')");

	if(!$insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}	
}	

echo "<script>
		alert('Diesel Entry Success.');
		LoadDiesel();
		$('#InsertDiesel')[0].reset();
		$('#insert_diesel_button').attr('disabled', false);
		document.getElementById('close_diesel_button').click();
	</script>";
	exit();
?>