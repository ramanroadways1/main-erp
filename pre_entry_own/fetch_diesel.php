<?php
require_once("../connection.php");

$timestamp = date("Y-m-d H:i:s"); 

$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));

if($vou_no=='')
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Vou Id.');
		window.location.href='../';
	</script>";
	exit();
}

$get_data=Qry($conn,"SELECT id,qty,rate,amount,card_pump,card_no,fuel_comp,consumer_pump FROM dairy.diesel_sample_own WHERE 
frno='$vou_no'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)>0)
{
	echo "<table class='table table-bordered' style='width:100%;font-size:12px;'>
			<tr>
				<th>Id</th>
				<th>Card/Pump</th>
				<th>Qty</th>
				<th>Rate</th>
				<th>Amount</th>
				<th>Card_No</th>
				<th>FuelComp</th>
				<th>Delete</th>
			</tr>";
	$i=1;	
	$qty = 0;
	$amount = 0;
	while($row_diesel=fetchArray($get_data))
	{
		// if($row_diesel['consumer_pump']==1)
		// {
			// $rate1 = "0";
			// $amount1 = "0";
		// }
		// else
		// {
			// $rate1 = $row_diesel['rate'];
			// $amount1 = $row_diesel['amount'];
		// }
		
		$qty = $qty+$row_diesel['qty'];
		$amount = $amount+$row_diesel['amount'];
		
		echo "<tr>
				  <td>$i</td>
				  <td>$row_diesel[card_pump]</td>
				  <td>$row_diesel[qty]</td>
				  <td>$row_diesel[rate]</td>
				  <td>$row_diesel[amount]</td>
				  <td>$row_diesel[card_no]</td>
				  <td>$row_diesel[fuel_comp]</td>
				  <td><button type='button' class='btn btn-xs btn-danger' onclick=DeleteDiesel('$row_diesel[id]')>Delete</button></td>
			</tr>";
	$i++;
	}	
	
	echo "<tr>
			<td colspan='2'><b>Total : </b></td>
			<td><b>$qty</b></td>
			<td></td>
			<td><b>$amount</b></td>
			<td colspan='3'></td>
		</tr>";

	echo "</table>
	<script>
		$('#req_button').attr('disabled',false);
	</script>";
}
else
{
	echo "Diesel not added yet !
	<script>
		$('#req_button').attr('disabled',true);
	</script>";
}

echo "<script>$('#loadicon').hide();</script>";
?>