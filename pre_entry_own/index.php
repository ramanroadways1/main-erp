<?php
require_once '../connection.php';

require('../_algo_pod_45_days_lock_system.php');

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(!isset($_SESSION['diesel_req_own']))
{
	$_SESSION['diesel_req_own']=$branch.mt_rand();
}

$diesel_req=$_SESSION['diesel_req_own'];
// echo "OK"
?>
<!DOCTYPE html>
<html lang="en">

<?php
	include("../_header.php");
?>

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="../load.gif" /><br><b>Please wait ...</b></center>
</div>

<?php 
	include("../_loadicon.php");
	include("../disable_right_click.php");
?>

<style>
 .ui-autocomplete { z-index:2147483647; }
</style>

<style>
.form-control
{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>

<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: '../autofill/get_own_vehicle_for_diesel.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
              $("#req_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#req_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselForm").on('submit',(function(e) {
$("#loadicon").show();
$("#req_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_diesel_request.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#function_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div id="function_result"></div>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<br />

<div class="container-fluid">

	<div class="row">
		<div class="form-group col-md-6">
			<label>&nbsp;</label>
			<a href="../"><button class="btn btn-sm btn-primary pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
		</div>
		
		<div class="form-group col-md-6">
			<form action="export.php" method="POST">
			<div class="row">
				<div class="form-group col-md-4">
					<label>From date <font color="red"><sup>*</sup></font></label>
					<input name="from_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
				</div>
				<div class="form-group col-md-4">
					<label>To date <font color="red"><sup>*</sup></font></label>
					<input name="to_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
				</div>
					
				<div class="form-group col-md-4">
					<label>&nbsp;</label>
	<button type="submit" class="btn btn-block btn-success"><span class="glyphicon glyphicon-save"></span> &nbsp; Download Report</button>
				</div>
			</div>
			</form>	
		</div>
	</div>

<form action="" id="DieselForm" autocomplete="off">

<div class="row">

	<div class="form-group col-md-5">
	
		<div class="form-group col-md-6">
			<label>Truck No : <sup><font color="red">*</font></sup></label>
			<input type="text" name="tno" id="truck_no" class="form-control" required />
		</div>
		
		<div class="form-group col-md-6">
			<label>LR No : </label>
			<input type="text" name="lrno" id="lrno" class="form-control" />
		</div>
		
		<div class="form-group col-md-12">
			<div class="bg-primary form-group col-md-12">Diesel Summary &nbsp; <button  data-toggle="modal" data-target="#diesel_add_modal" style="color:#000" type="button"> Add Diesel</button>
			</div>
			<div class="row"><div id="result_adv_diesel" class="form-group col-md-12"></div></div>
		</div> 
			
		<div class="form-group col-md-12">
			<div class="row">
				<div class="form-group col-md-4">
					<button type="submit" id="req_button" style="color:#000" class="btn btn-md btn-warning">Submit Request</button>
				</div>
			</div>
		</div>
		
	</div>
	
</form>
	
<div class="form-group col-md-7">
<br />
	<div style="padding:4px;" class="form-group bg-primary col-md-12">
	 Requested Diesel Summary :
	</div>	
<?php
$qry_old=Qry($conn,"SELECT tno,lrno,SUM(amount) as amount,date,GROUP_CONCAT(narration SEPARATOR ', ') as narration FROM 
dairy.diesel WHERE done!=1 AND trip_id='' AND branch='$branch' GROUP BY tno");
	
if(!$qry_old){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	if(numRows($qry_old)>0)
	{
		$sr=1;
		
		echo "<table class='table table-bordered' style='font-size:12px;'>
			<tr>
				<th>Id</th>
				<th>LR No</th>
				<th>Truck No</th>
				<th>Date</th>
				<th>Amount</th>
				<th>Narration</th>
			</tr>	
			";
		while($row_old=fetchArray($qry_old))
		{
			echo "<tr>
					<td>$sr</td>
					<td>$row_old[lrno]</td>
					<td>$row_old[tno]</td>
					<td>".convertDate("d-m-y",$row_old["date"])."</td>
					<td>$row_old[amount]</td>
					<td>$row_old[narration]</td>
				</tr>";
			$sr++;
		}	
		echo "</table>";	
	}
	else
	{
		echo "<center><b>No records found.</b></center>";
		
	}
	?>
		</div>
	</div>
</div>

<script type="text/javascript">
function LoadDiesel()
{
	$('#loadicon').show();
	$.ajax({
		url: "fetch_diesel.php",
		method: "post",
		data:'vou_no=' + '<?php echo $diesel_req; ?>', 
		success: function(data){
		$("#result_adv_diesel").html(data);
	}})
}

function DeleteDiesel(id)
{
	$("#loadicon").show();
	$.ajax({
		url: "delete_diesel_adv.php",
		method: "post",
		data:'id=' + id, 
		success: function(data){
			$("#function_result").html(data);
			$("#loadicon").hide();
	}})
}
</script>

</body>
</html>

<script>
$(window).load(function(){ 
$('#window_loadicon').fadeOut();
});
LoadDiesel();
</script>

<?php include ("./modal_diesel_adv.php"); ?>