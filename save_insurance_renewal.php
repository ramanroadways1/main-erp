<?php
require_once 'connection.php';

$timestamp = date("Y-m-d H:i:s");

$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$id = escapeString($conn,strtoupper($_POST['id']));
$start_date = escapeString($conn,strtoupper($_POST['start_date']));
$end_date = escapeString($conn,strtoupper($_POST['end_date']));

$days = round((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24));
if($days<=360)
{
	echo "<script>
		alert('Error : Please Check Insurance Dates.');
		$('#ins_ren_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$verify = Qry($conn,"SELECT token_no,reg_no,branch,insurance FROM asset_vehicle WHERE id='$id'");
if(!$verify){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_ext_copy = Qry($conn,"SELECT ins_copy,ins_start,ins_end,ins_time FROM asset_vehicle_docs WHERE veh_id='$id'");

if(!$get_ext_copy){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_ext_copy = fetchArray($get_ext_copy);

$old_copy = $row_ext_copy['ins_copy'];
$old_start = $row_ext_copy['ins_start'];
$old_end = $row_ext_copy['ins_end'];
$old_update_time = $row_ext_copy['ins_time'];

if(numRows($verify)==0)
{
	echo "<script>
		alert('Vehicle not found.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$row_veh = fetchArray($verify);

if($row_veh['reg_no']==''){
	$veh_no_db = $row_veh['token_no'];
}
else{
	$veh_no_db = $row_veh['reg_no'];
}

if($veh_no_db!=$veh_no)
{
	echo "<script>
		alert('Vehicle not verified.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['branch']!=$branch)
{
	echo "<script>
		alert('Vehicle not belongs to your branch.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	if(!in_array($_FILES['ins_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed. Insurance Copy');
			$('#ins_ren_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
$sourcePath = $_FILES['ins_copy']['tmp_name'];

	$targetPath = "asset_upload/veh_insurance/".date('dmYHis').mt_rand().".".pathinfo($_FILES['ins_copy']['name'],PATHINFO_EXTENSION);
	
	ImageUpload(1000,700,$sourcePath);
	
	if(!move_uploaded_file($sourcePath, $targetPath)) 
	{
		echo "<script>
			alert('Error : Unable to upload Insurance Copy.');
			$('#ins_ren_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
StartCommit($conn);
$flag = true;

$update2 = Qry($conn,"UPDATE asset_vehicle_docs SET ins_copy='$targetPath',ins_start='$start_date',ins_end='$end_date',ins_time='$timestamp' 
WHERE veh_id='$id'");

if(!$update2){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updateLog = Qry($conn,"INSERT INTO asset_vehicle_docs_renewal(veh_id,doc_type,old_copy,old_start_date,old_end_date,old_upload_time,
start_date,end_date,branch,branch_user,timestamp) VALUES ('$id','INSURANCE','$old_copy','$old_start','$old_end','$old_update_time',
'$start_date','$end_date','$branch','$branch_sub_user','$timestamp')");

if(!$updateLog){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
			alert('Insurance Updated Successfully !');
			window.location.href='asset_vehicle_view.php';
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>