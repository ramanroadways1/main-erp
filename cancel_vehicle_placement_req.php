<?php 
require_once 'connection.php';

$id = escapeString($conn,strtoupper($_POST['id'])); 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	echo "<script>
		alert('ID not found !');
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}

$insert_cancelled_req = Qry($conn,"INSERT INTO vehicle_placement_req_db(tno,dl_no,driver_mobile,branch,branch_user,cancelled_req,cancel_username,
timestamp) SELECT tno,dl_no,driver_mobile,branch,branch_user,'1','$branch_sub_user',timestamp FROM vehicle_placement_req WHERE id='$id'");

if(!$insert_cancelled_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(AffectedRows($conn) == 0)
{
	echo "<script>
		alert('Something went wrong !');
		window.location.href='./vehicle_placed_by_branch.php';
	</script>";	
	exit();
}	

$dlt_req = Qry($conn,"DELETE FROM vehicle_placement_req WHERE id='$id'");

if(!$dlt_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Request Cancelled !');
	window.location.href='./vehicle_placed_by_branch.php';
</script>";	
exit();
?>