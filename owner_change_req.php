<?php
require_once 'connection.php';
if($branch!='CGROAD')
{
	exit();
}
exit();
$date = date("Y-m-d"); 
$timestamp=date("Y-m-d H:i:s");
?>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="help/tphead.js" type="text/javascript"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
</head>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:0.8; cursor: wait">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<script>
<!--
//Disable right mouse click Script
var message="Function Disabled!";
///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
// --> 
</script>

<script type="text/javascript">
     $(function() {
    $( "#truck_no" ).autocomplete({
      source: './autofill/tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			alert('Truck No does not exists.');
			$("#truck_no").val("");
			$("#truck_no").focus();
		}
    }, 
    focus: function (event, ui){
        return false;
    }
    });
  });
</script>

<style>
.form-control{text-transform:uppercase;}
</style>

<div id="result_con"></div>

<script>
function FetchOwner(tno)
{ 
	if(tno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_owner.php",
		data: 'tno=' + tno,
		type: "POST",
		success: function(data) {
		$("#result_con").html(data);
		$("#loadicon").hide();
		},
		error: function() {}
		});
	}
	else
	{
		$('#button1').attr('disabled',true);
		$('#div1').hide();
		$('#div2').hide();
	}		
}		
</script>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button1").attr("disabled",true);
	$("#button1").val("Please wait...");
	$.ajax({
	url: "./owner_change_req_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_con").html(data);
		$("#button1").val("Request for update");
	},
error: function() 
	{}
	});
}));
});
</script>	

<a href="./" class="btn btn-danger" style="margin:10px;">Go back</a>

<body style="font-family:Verdana" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container">
	<form method="POST" id="Form1">
	
	<div class="row">
		<div class="form-group col-md-4">	
			<label>Truck No</label>
			<input type="text" onblur="FetchOwner(this.value)" id="truck_no" name="tno" class="form-control" required>
		</div>	
		
		<div class="form-group col-md-8">
			<label>&nbsp;</label>
			<h5 style="margin-top:3px;color:red">If Uploaded Documents are not clear. Your request will be rejected.</h5>
		</div>	
	</div>
	
	<input type="hidden" id="o_id" name="o_id">
	
	<div class="row" id="div1" style="display:none1">
		<div class="col-md-12">
			<h4 style="padding:5px;color:#FFF;background-color:gray">Current Details :</h4>
		</div>		
		<div class="form-group col-md-4">	
			<label>Owner Name</label>
			<input readonly type="text" name="owner_current" id="owner_current" class="form-control" required>
		</div>

		<div class="form-group col-md-4">	
			<label>Mobile Number</label>
			<input readonly type="text" name="mobile_current" id="mobile_current" class="form-control" required>
		</div>

		<div class="form-group col-md-4">	
			<label>PAN No</label>
			<input readonly type="text" name="pan_current" id="pan_current" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3" id="wheeler_man" style="display:none">	
			<label>Wheeler <font color="red">*</font></label>
			<select name="wheeler" required id="wheeler" class="form-control" />
				<option value="">--Select--</option>
				<option value="4">4</option>
				<option value="6">6</option>
				<option value="10">10</option>
				<option value="12">12</option>
				<option value="14">14</option>
				<option value="16">16</option>
				<option value="18">18</option>
				<option value="22">22</option>
			</select>
		</div>
		
		<div class="form-group col-md-3" id="wheeler_auto">	
			<label>Wheeler <font color="red">*</font></label>
			<input class="form-control" type="text" name="wheeler_auto_box" id="wheeler_auto_box" readonly />
		</div>
		
		<div class="form-group col-md-3">	
			<label>Address</label>
			<textarea name="addr_current" id="addr_current" readonly class="form-control"></textarea>
		</div>

		<div class="form-group col-md-2">	
			<a target="_blank" id="rc_f_current"><button id="bRc" type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-eye-open"></span> RC Front </button></a>
		</div>
		
		<input type="hidden" name="rc_f_current2" id="rc_f_current2">
		<input type="hidden" name="rc_r_current2" id="rc_r_current2">
		<input type="hidden" name="pan_c_current2" id="pan_c_current2">
		
		<input type="hidden" name="wheeler_set" id="wheeler_set">
		
		<div class="form-group col-md-2">	
			<a target="_blank" id="rc_r_current"><button id="bRcR" type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-eye-open"></span> RC Rear </button></a>
		</div>
		
		<div class="form-group col-md-2">	
			<a target="_blank" id="pan_c_current"><button id="bPan" type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-eye-open"></span> PAN Card </button></a>
		</div>
	
	</div>
	
	<div class="row" id="div2" style="display:none1">
	
		<div class="col-md-12">
			<h4 style="padding:5px;color:#FFF" class="bg-primary">New details to be updated :</h4>
		</div>		
		<div class="form-group col-md-4">	
			<label>Owner Name <font color="red">*</font></label>
			<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="owner_name" class="form-control" required>
		</div>

		<div class="form-group col-md-4">	
			<label>Mobile Number <font color="red">*</font></label>
			<input minlength="10" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" name="owner_mobile" class="form-control" required>
		</div>

		<div class="form-group col-md-4">	
			<label>PAN No <font color="red">*</font></label>
			<input minlength="10" maxlength="10" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePan(this.value)" type="text" id="owner_pan" name="owner_pan" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">	
			<label>Address <font color="red">*</font></label>
			<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" name="owner_add" required class="form-control"></textarea>
		</div>
		
		<div class="form-group col-md-3">	
			<label>RC Copy (front) <font color="red">*</font></label>
			<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="rc_copy_f" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">	
			<label>RC Copy (rear) <font color="red">*</font></label>
			<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="rc_copy_r" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">	
			<label>PAN Card Copy <font color="red">*</font></label>
			<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="pan_copy" class="form-control" required>
		</div>
		
		<div class="form-group col-md-12">
		</div>
		
		<div class="form-group col-md-12">
			<input id="button1" disabled type="submit" class="btn btn-lg btn-success" value="Request for update" />
		</div>	
		
	</div>
	
	</form>
	
	<div class="row">
		<div class="col-md-12">
			<b>Pending Requests :<br><br> </b>
		</div>
		<div class="col-md-12 table-responsive">
			<table class="table table-bordered" style="font-size:13px;">
				<tr>
					<th>Id</th>
					<th>TruckNo</th>
					<th>Name</th>
					<th>Mobile</th>
					<th>PAN</th>
					<th>Address</th>
					<th>DateTime</th>
				</tr>
				
				<?php
				$qry=mysqli_query($conn,"SELECT tno,new_name,new_mobile,new_pan,new_addr,timestamp FROM owner_change_req WHERE done=0 AND branch='$branch'");
				if(mysqli_num_rows($qry)==0)
				{
					echo "<tr>
						<td colspan='7'>NO RESULT FOUND..</td>
					</tr>";
				}
				else
				{
					$sn=1;
					while($row=mysqli_fetch_array($qry))
					{
						echo "<tr>
							<td>$sn</td>
							<td>$row[tno]</td>
							<td>$row[new_name]</td>
							<td>$row[new_mobile]</td>
							<td>$row[new_pan]</td>
							<td>$row[new_addr]</td>
							<td>$row[timestamp]</td>
						</tr>";
					
					$sn++;
					}
				}
				?>
			</table>
		</div>
	</div>
	
</div>