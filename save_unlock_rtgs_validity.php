<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$crn_no = escapeString($conn,strtoupper($_POST['crn_no']));
$rtgs_id = escapeString($conn,strtoupper($_POST['rtgs_id']));
$narration = escapeString($conn,($_POST['narration']));

$chk_duplicate = Qry($conn,"SELECT id FROM extend_rtgs_approval_validity WHERE crn='$crn_no'");

if(!$chk_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit(); 
}

if(numRows($chk_duplicate) > 0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$verify_record = Qry($conn,"SELECT acname,amount,crn FROM rtgs_fm WHERE id='$rtgs_id'");

if(!$verify_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit(); 
}

if(numRows($verify_record) == 0)
{
	echo "<script>
		alert('RTGS entry not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$row_rtgs = fetchArray($verify_record);

if($row_rtgs['crn']!=$crn_no)
{
	echo "<script>
		alert('RTGS not verified !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO extend_rtgs_approval_validity(rtgs_id,crn,narration,branch,branch_user,timestamp) VALUES 
('$rtgs_id','$crn_no','$narration','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./unlock_rtgs_validity.php';
	</script>";
	exit();
?>