<?php
require_once './connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));

// echo "<script>
			// alert('On hold');
			// $('#broker_verify_button').attr('disabled',false);
			// $('#add_broker_button').attr('disabled',true);
			// $('#add_broker_button').hide();
			// $('#loadicon').fadeOut('slow');
		// </script>";
	// exit();

if(strlen($pan_no) != 10)
{
	echo "<script>
			alert('Check PAN number !');
			$('#broker_verify_button').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}
	
if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan_no))
{
	echo "<script>
			alert('Invalid PAN nummber !');
			$('#broker_verify_button').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

$chk_pan = Qry($conn,"SELECT name FROM mk_broker WHERE pan='$pan_no' AND pan!='NA' AND pan!=''");

if(!$chk_pan){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo '<script>
		alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
	
if(numRows($chk_pan)>0)
{
	$row_name = fetchArray($chk_pan);
	
	echo "<script type='text/javascript'>
		alert('PAN No : $pan_no already registered with Broker: $row_name[name] !'); 
		$('#broker_verify_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();	
}

$sourcePath = $_FILES['pan_copy']['tmp_name'];

// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['pan_copy']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image Upload Allowed.');
		$('#broker_verify_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$ocr_pan = VerifyOCRPan($aadhar_api_url,$_FILES['pan_copy']['tmp_name'],$aadhar_api_token);

if($ocr_pan[0]=="ERROR")
{
	$error_code = $ocr_pan[1];
	$error_msg = $ocr_pan[2];
	$api_response = escapeString($conn,($ocr_pan[3]));
	
	$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
	('OCR_PAN','1','$api_response','$branch','$branch_sub_user','$timestamp')");
		
	if(!$insert_ocr_api_call){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	
	echo '<script>
		alert("OCR Error_Code '.$error_code.' : '.$error_msg.'");
		$("#broker_verify_button").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
else
{
	$name_ocr = $ocr_pan[1];
	$name_ocr_confd = $ocr_pan[2];
	
	$pan_no_ocr = $ocr_pan[3];
	$pan_no_ocr_confd = $ocr_pan[4];
	
	$father_name_ocr = $ocr_pan[5];
	$father_name_ocr_confd = $ocr_pan[6];
	
	$dob_ocr = $ocr_pan[7];
	$dob_ocr_confd = $ocr_pan[8];
	
	$api_response = escapeString($conn,($ocr_pan[9]));
	
	$chk_ocr_record = Qry($conn,"SELECT id FROM _data_ocr_pan WHERE pan_no='$pan_no_ocr'");
	
	if(strlen($pan_no_ocr)!=10)
	{
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,res_narration,branch,branch_user,timestamp) VALUES 
		('OCR_PAN','1','$api_response','Pan length is invalid.','$branch','$branch_sub_user','$timestamp')");
			
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	
		echo '<script>
			alert("Pan number: '.$pan_no_ocr.' is not valid !");
			$("#broker_verify_button").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	
	if(numRows($chk_ocr_record)==0)
	{
		$insert_ocr = Qry($conn,"INSERT INTO _data_ocr_pan(pan_no,pan_no_confd,name_ocr,name_ocr_confd,father_name,father_name_confd,dob_ocr,
		dob_ocr_confd,branch,branch_user,timestamp) VALUES ('$pan_no_ocr','$pan_no_ocr_confd','$name_ocr','$name_ocr_confd','$father_name_ocr',
		'$father_name_ocr_confd','$dob_ocr','$dob_ocr_confd','$branch','$branch_sub_user','$timestamp')");
		
		if(!$insert_ocr){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	}
	
	$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
	('OCR_PAN','$api_response','$branch','$branch_sub_user','$timestamp')");
			
	if(!$insert_ocr_api_call){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
}

if($pan_no_ocr != $pan_no)
{
	$insert_ocr_doc_no_mismatch = Qry($conn,"INSERT INTO _data_ocr_doc_no_mismatch(doc_type,api_res,user_input,branch,branch_user,timestamp) VALUES 
	('OCR_PAN','$pan_no_ocr','$pan_no','$branch','$branch_sub_user','$timestamp')");
			
	if(!$insert_ocr_doc_no_mismatch){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	
	// alert("Invalid PAN number ! Pan number should be '.$pan_no_ocr.' as per attached document.");
	
	echo '<script>
		alert("PAN number not matching with document !");
		$("#broker_verify_button").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

$verify_pan = VerifyPAN($pan_no,$aadhar_api_url,$aadhar_api_token);

if($verify_pan[0]=="ERROR")
{
	$error_code = $verify_pan[1];
	$error_msg = $verify_pan[2];
	$api_response = escapeString($conn,($verify_pan[3]));
	
	$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
	('VERIFY_PAN','1','$api_response','$branch','$branch_sub_user','$timestamp')");
		
	if(!$insert_ocr_api_call){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	
	echo '<script>
		alert("Verification Error_Code '.$error_code.' : '.$error_msg.'");
		$("#broker_verify_button").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
else
{
	$full_name_api = $verify_pan[1];
	$name_length = strlen($full_name_api);
	$api_response = escapeString($conn,$verify_pan[3]);
	
	$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
	('VERIFY_PAN','$api_response','$branch','$branch_sub_user','$timestamp')");
			
	if(!$insert_ocr_api_call){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#broker_verify_button").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
}

$fix_name = mt_rand().date('dmYHis');

$targetPath = "broker_pan_temp/".$fix_name.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath1 = $fix_name.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);

// ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	echo '<script>
		alert("Error while uploading document !");
		$("#broker_verify_button").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

$_SESSION['add_broker_filepath'] = $targetPath1;	
$_SESSION['add_broker_pan_no'] = $pan_no;
$_SESSION['add_broker_pan_holder'] = $full_name_api;
	
	echo "<script type='text/javascript'> 
		$('#pan_no').attr('readonly',true);
		$('#pan_copy_input').attr('disabled',true);
		$('#broker_verify_button').attr('disabled',true);
		$('#broker_verify_button').hide();
		
		$('#add_broker_button').attr('disabled',false);
		$('#add_broker_button').show();
		
		$('#pan_no_hidden').val('$pan_no');
		$('#pan_holder_name').val('$full_name_api');
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();

?>