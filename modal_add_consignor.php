<form id="Con1Form" action="#" method="POST">
<div id="ModalConsignor" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD New Consignor
      </div>
      <div class="modal-body">
        <div class="row">
		
				<div class="form-group col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label>Consignor's GST Number <font color="red"><sup>*</sup></font></label>
						<input type="text" maxlength="15" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" name="gst_no" id="gst_no_consignor" class="form-control" required="required">
					</div>
					<div class="form-group col-md-4">
						<label>&nbsp;</label>
						<br>
						<button type="button" id="ValidateBtn_consignor" onclick="ValidateGstCon1()" class="btn btn-danger btn-sm">Validate GST</button>
					</div>
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label>Consignor's Legal Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="party_name_legal_consignor" readonly oninput="this.value=this.value.replace(/[^A-Z a-z-]/,'')" name="legal_name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Consignor's Trade Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="party_name_consignor" readonly oninput="this.value=this.value.replace(/[^A-Z a-z-]/,'')" name="trade_name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Select Party Name <font color="red"><sup>*</sup></font></label>
				<select class="form-control gst_parties_sel" required="required" name="party_name_to_add">
					<option value="">--select--</option>
					<option style="display:none" id="party_trade_consignor" value=""></option>
					<option style="display:none" id="party_legal_consignor" value=""></option>
				</select>				
			</div> 
			
			<div class="form-group col-md-6">
				<label>Consignor's PAN No. <font color="red"><sup>*</sup></font></label>
				<input type="text" id="party_pan_consignor" maxlength="10" readonly oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" name="pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Consignor's Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>PINCODE <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="pin_code" class="form-control" required="required">
			</div>
			
			<input type="hidden" id="state_name_consignor" name="state_name">
			
			<div class="form-group col-md-4">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:]/,'')" name="addr" class="form-control" required="required"></textarea>
			</div>
		
		</div>
      </div>
	  <div id="result_Con1Form"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="buttonPartyADD_consignor" class="btn btn-primary">Submit</button>
        <button type="button" id="close_add_con1" onclick="$('#Con1Form')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#Con1Form").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#buttonPartyADD_consignor").attr("disabled", true);
	$.ajax({
        	url: "./save_consignor.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Con1Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script>
function ValidateGstCon1()
{
	var gst = $('#gst_no_consignor').val();
	
	if(gst=='')
	{
		alert('Enter GST Number !');
	}
	else
	{
		$("#gst_no_consignor").attr("readonly",true);
		$("#ValidateBtn_consignor").attr("disabled",true);
		$("#buttonPartyADD_consignor").attr("disabled",true);
			
		var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
		var gstinformat2 = new RegExp('^([0-9]{2})([a-zA-Z]{4}[0-9]{5}[a-zA-Z]{1})([1-9a-zA-Z]{1}[dD]{1}[0-9a-zA-Z]{1})$');
	   
	   if(gstinformat.test(gst))
	   {
			 $("#loadicon").show();
				jQuery.ajax({
				url: "./verify_gst.php",
				data: 'gst=' + gst + '&type=' + 'consignor',
				type: "POST",
				success: function(data) {
				$("#result_Con1Form").html(data);
				},
			error: function() {}
			});
	   }
	   else if(gstinformat2.test(gst))
	   {
			 $("#loadicon").show();
				jQuery.ajax({
				url: "./verify_gst.php",
				data: 'gst=' + gst + '&type=' + 'consignor',
				type: "POST",
				success: function(data) {
				$("#result_Con1Form").html(data);
				},
			error: function() {}
			});
	   }
	   else
	   {
			alert('Please Enter Valid GSTIN Number');
			$("#gst_no_consignor").attr("readonly",false);
			$("#ValidateBtn_consignor").attr("disabled",false);
			$("#buttonPartyADD_consignor").attr("disabled",true);
		}
	}
}
</script>