<?php
require_once("connection.php");

$min_lr_date="-1 day";	

if($branch=='ABUROAD')
{
	$min_lr_date="-10 day";	
}

// $min_lr_date="-2 day";	

$date = date("Y-m-d");

$min_lr_date = date("Y-m-d", strtotime("$min_lr_date"));

$check_open_lr = Qry($conn,"SELECT party_id FROM open_lr");
if(!$check_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_open_lr)>0)
{
	$array_party=array();
	
	while($row_open_lr = fetchArray($check_open_lr))
	{
		$array_party[] = '"'.$row_open_lr['party_id'].'"';
	}
	
	$open_lr_party_id = implode(",",$array_party);
}
else
{
	$open_lr_party_id = '"0"';
}

if($branch=='DHULE')
{
	$fetch_last_lr = Qry($conn,"SELECT lrno FROM lr_check WHERE branch='DHULE' ORDER BY id DESC LIMIT 1");
	
	if(!$fetch_last_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
						
	$row_last_lr = fetchArray($fetch_last_lr);
	$last_lrno=++$row_last_lr['lrno'];
}
else
{
	$last_lrno="";
}
?>

<style type="text/css">
.ui-autocomplete { z-index:2147483647; font-size:12px !important;}

label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}
input[type='date'] { font-size: 12px !important;}
input[type='text'] { font-size: 12px !important;}
input[type='number'] { font-size: 12px !important;}
select { font-size: 12px !important; }
textarea { font-size: 12px !important; }
</style>

<script>
$(function() {  
      $("#item1").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/lr_items.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#item1').val(ui.item.value);   
               $('#item_id').val(ui.item.id);
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#item1").val('');
				$("#item_id").val('');
                alert('Item does not exist !'); 
              } 
              },
			}); 
}); 

$(function() {  
      $("#consignor").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignor').val(ui.item.value);   
               $('#consignor_id').val(ui.item.id);     
               $('#con1_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignor").val('');
				$("#consignor_id").val('');
				$("#con1_gst").val('');
                alert('Consignor does not exist !'); 
              } 
              },
			}); 
});  

$(function() {  
      $("#consignor_sub").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignor_sub').val(ui.item.value);   
               $('#consignor_sub_id').val(ui.item.id);     
               $('#consignor_sub_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignor_sub").val('');
				$("#consignor_sub_id").val('');
				$("#consignor_sub_gst").val('');
                alert('Consignor does not exist !'); 
              } 
              },
			}); 
});  

$(function() {  
      $("#consignee").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee').val(ui.item.value);   
               $('#consignee_id').val(ui.item.id);     
               $('#con2_gst').val(ui.item.gst);     
               $('#consignee_pincode').val(ui.item.pincode);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee").val('');
				$("#consignee_id").val('');
				$("#con2_gst").val('');
				$("#consignee_pincode").val('');
                alert('Consignee does not exist !'); 
              } 
              },
			}); 
});

$(function() {  
      $("#shipment_party").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#shipment_party').val(ui.item.value);   
               $('#shipment_party_id').val(ui.item.id);     
               $('#shipment_party_gst').val(ui.item.gst);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#shipment_party").val('');
				$("#shipment_party_id").val('');
				$("#shipment_party_gst").val('');
				alert('Shipment party does not exist !'); 
              } 
              },
			}); 
});

$(function() {  
      $("#bill_to_party").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#bill_to_party').val(ui.item.value);   
               $('#bill_to_party_id').val(ui.item.id);   
				$('#bill_to_gst').val(ui.item.gst);			   
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#bill_to_party").val('');
				$("#bill_to_party_id").val('');
				$("#bill_to_gst").val('');
				
				alert('Party does not exist !'); 
              } 
              },
			}); 
});

$(function() {
		$("#from").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from').val("");   
			$('#from_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to').val("");   
			$('#to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#billing_station").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#billing_station').val(ui.item.value);   
            $('#billing_station_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#billing_station').val("");   
			$('#billing_station_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#dest_zone").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#dest_zone').val(ui.item.value);   
            $('#dest_zone_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#dest_zone').val("");   
			$('#dest_zone_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#LRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./lr_entry_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_lr").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
function FetchEwayBill(eway_no){
	$('#EwayBillFlag').val('');
	$('#EwayBillFlag_desc').val('');
	
	var consignor_id = $('#consignor_id').val();
	var consignee_id = $('#consignee_id').val();
	var consignor_sub_gst = $('#consignor_sub_gst').val();
	var consignor_sub_id = $('#consignor_sub_id').val();
	var lr_date = $('#lr_date').val();
	var lr_type = $('#lr_type').val();
	var tno_market = $('#tno_market').val();
	var tno_own = $('#tno_own').val();
	var item_id = $('#item_id').val();
	var invoice_value = $('#invoice_value_ewb').val();
	
	if(invoice_value=='')
	{
		$('#eway_bill_no').val('');
		$('#invoice_value_ewb').focus();
	}
	else
	{
		var invoice_value = Number($('#invoice_value_ewb').val());
		$('#invoice_value_ewb').attr('readonly',true);
		
	if(consignor_id=='' || consignee_id=='')
	{
		if(consignee_id=='')
		{
			$('#eway_bill_no').val('');
			$('#consignee').focus();
		}
		else
		{
			$('#eway_bill_no').val('');
			$('#consignor').focus();
		}
	}	
	else
	{
		if(consignee_id=='56' && consignor_sub_id=='')
		{
			$('#eway_bill_no').val('');
			$('#consignor_sub').focus();
		}
		else
		{	
			var bilty_id = $('#bilty_id').val();
			
			if(bilty_id=='' || bilty_id=='ZERO')
			{
				alert('Something went wrong !')
				$('#eway_bill_no').val('');
			}
			else
			{
				if(bilty_id=='OTHERLR'){
					var company = $('#company_man').val();
				}
				else{
					var company = $('#company_auto').val();
				}
				
				if(company=='')
				{
					alert('Company not found !')
					$('#eway_bill_no').val('');
				}
				else
				{
					if(item_id=="")
					{
						alert('Enter item first !')
						$('#eway_bill_no').val('');
					}
					else
					{
						if(bilty_id=='OTHERLR'){
							$('.company_man_sel').attr('disabled',true);
							$('#company_man_'+company).attr('disabled',false);
						}
						
						if(company=='RRPL' || company=='RAMAN_ROADWAYS')
						{
							$('#loadicon').show();
							jQuery.ajax({
								url: "./_API/get_eway_bill.php", 
								data: 'ewb_no=' + eway_no + '&consignor_id=' + consignor_id + '&item_id=' + item_id + '&company=' + company + '&invoice_value=' + invoice_value + '&consignee_id=' + consignee_id + '&lr_date=' + lr_date + '&lr_type=' + lr_type + '&tno_market=' + tno_market + '&tno_own=' + tno_own + '&consignor_sub_gst=' + consignor_sub_gst,
								type: "POST",
								success: function(data) {
									$("#eway_result2").html(data);
									// $('#loadicon').hide(); 
								},	
								error: function() {} 
							});
						}
						else
						{
							alert('Error !')
							$('#eway_bill_no').val('');
						}
					}
				}
			}
		}
	}
	}
}

function InvoiceValueFunc()
{
	$('#eway_bill_no').val('');
	
	var item_name = $('#item1').val();
	var inv_value = Number($('#invoice_value_ewb').val());
	var from_id = $('#from_id').val();
	var to_id = $('#to_id').val();
	var consignor_id = $('#consignor_id').val();
	var consignee_id = $('#consignee_id').val();
		
	if(item_name=='')
	{
		$('#item1').focus();
		$('#invoice_value_ewb').val('');
	}
	else
	{
		if(from_id=='')
		{
			$('#from').focus();
			$('#item1').val('');
			$('#invoice_value_ewb').val('');
		}
		else if(to_id=='')
		{
			$('#to').focus();
			$('#item1').val('');
			$('#invoice_value_ewb').val('');
		}
		else
		{
			if(consignor_id=='' || consignee_id=='')
			{
				alert('Enter consignor and consignee first !');
				$('#item1').val('');
				$('#invoice_value_ewb').val('');
			}
			else
			{
				if(from_id=='210' && to_id=='951')
				{
					$('#from').attr('readonly',true);
					$('#to').attr('readonly',true);
					
					$('#trip_type_div').show();
					$('#round_trip_lrno').attr('required',true);
					$('#trip_type_pune').attr('required',true);
				}
				else
				{
					$('#trip_type_div').hide();
					$('#round_trip_lrno').attr('required',false);
					$('#trip_type_pune').attr('required',false);
				}
				
				if(item_name=='SALT')
				{
					$('#EwayBillFlag').val('2');
					$('#EwayBillFlag_desc').val('Salt Exempt');
					$('#item1').attr('readonly',true);
					$('#invoice_value_ewb').attr('readonly',true);
					$('#eway_bill_no').attr('readonly',true);
					$('#eway_bill_no').attr('onblur','');
					
					$('#EwayBill_copy_file').attr('required',false);
					$('#EwayBill_copy_div').hide();
				}
				else
				{
					var branch = '<?php echo $branch; ?>';
					
					// alert(branch);
					// alert(from_id);
					// alert(to_id);
					// alert(consignor_id);
					// alert(consignee_id);
					if(branch=='MUNDRA' && from_id=='5' && to_id=='1029' && consignor_id=='4' && consignee_id=='860')
					{
						$('#from').attr('readonly',true);
						$('#to').attr('readonly',true);
						$('#consignor').attr('readonly',true);
						$('#consignee').attr('readonly',true);
					
						$('#EwayBillFlag').val('2');
						$('#EwayBillFlag_desc').val('Wolkem Export Exempt');
						$('#invoice_value_ewb').attr('readonly',true);
						$('#item1').attr('readonly',true);
						$('#eway_bill_no').attr('readonly',true);
						$('#eway_bill_no').attr('onblur','');
						
						$('#EwayBill_copy_file').attr('required',false);
						$('#EwayBill_copy_div').hide();
					}
					else
					{
						// alert(inv_value);
						// alert($('#EwayBillFlag_desc').val());
						
						if($('#EwayBillFlag_desc').val()=='Admin Exempt')
						{
							$('#item1').attr('readonly',true);
						}
						else
						{
							if(inv_value<50000)
							{
								$('#EwayBillFlag').val('2');
								$('#EwayBillFlag_desc').val('Invoice Value Exempt');
								$('#item1').attr('readonly',true);
								$('#eway_bill_no').attr('required',false);
							}
							else
							{
								$('#EwayBillFlag').val('');
								$('#EwayBillFlag_desc').val('');
								$('#item1').attr('readonly',true);
								$('#eway_bill_no').attr('required',true);
							}
						}
						
						$('#invoice_value_ewb').attr('readonly',false);
					}
				}
			}
		}
	}
}
		
function LocanixPush(id,lrno)
{
	jQuery.ajax({
		url: "lr_entry_push_locanix.php",
		data: 'id=' + id + '&lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#function_result_locanix").html(data);
	},
	error: function() {}
	});
}
</script>

<div id="function_result_locanix"></div>
<div id="result_lr"></div>

<form action="" id="LRForm" method="POST" autocomplete="off">

<div style="overflow:auto" class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
            <div class="modal-body" style="max-height: calc(100vh - 70px);overflow: auto;">
			<!-- style="max-height: calc(100vh - 70px);overflow: auto;"-->

<script type="text/javascript">
function CheckConsignor()
{
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
	}
	else if($('#consignee_id').val()=='' || $('#consignee').val()=='')
	{
		$('#consignee').focus();
		$('#lrno').val("");
	}
	else
	{
		$('#consignor').attr('readonly',true);
		$('#consignee').attr('readonly',true);
	}
	
	<?php
	if($branch=='PUNE')
	{
	?>
		if($('#pune_plant').val()=='')
		{
			alert('Select plant first !!');
			$('#lrno').val('');
			$('#pune_plant').focus();
		}
	<?php
	}
	?>
}

function CheckOpenLR()
{
	$('#lrno').val('');
	
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
	}
	else
	{
		var array = '<?php echo $open_lr_party_id; ?>';
		if(array.indexOf($('#consignor_id').val())!== -1)
		{
			$("#lrno").attr("oninput","this.value=this.value.replace(/[^a-zA-Z0-9_]/,'');CheckConsignor()");
		}
		else
		{
			var lrno_next = '<?php echo $last_lrno; ?>';
			$("#lrno").attr("oninput","this.value=this.value.replace(/[^0-9]/,'');CheckConsignor()");
			$("#lrno").val(lrno_next);
		}
		
		if($('#consignor_id').val()=='56' || $('#consignor_id').val()=='675')
		{
			$('#dest_zone_div').show();
			$('#dest_zone').attr('required',true);
		}
		else
		{
			$('#dest_zone_div').hide();
			$('#dest_zone').val('');
			$('#dest_zone_id').val('');
			$('#dest_zone').attr('required',false);
		}
		
		if($('#consignor_id').val()=='56')
		{
			$('.qwik_party').show();
			$('#consignor_sub').val('');
			$('#consignor_sub_id').val('');
			$('#consignor_sub_gst').val('');
			
			$('#consignor_sub').attr('required', true);
			$('#consignor_sub_id').attr('required', true);
			$('#consignor_sub_gst').attr('required', true);
		}
		else
		{
			$('.qwik_party').hide();
			$('#consignor_sub').val('');
			$('#consignor_sub_id').val('');
			$('#consignor_sub_gst').val('');
			
			$('#consignor_sub').attr('required', false);
			$('#consignor_sub_id').attr('required', false);
			$('#consignor_sub_gst').attr('required', false);
		}
		
			var consignor_party = $('#consignor').val();
			var consignor_id = $('#consignor_id').val();
			var con1_gst = $('#con1_gst').val();
			
			$('#consignor_sub').val(consignor_party);
			$('#consignor_sub_id').val(consignor_id);
			$('#consignor_sub_gst').val(con1_gst);
	}
}

function checkLr()
{
	var lrno = $('#lrno').val();
	var billing_type = $('#billing_type').val();
	
	if(billing_type=='')
	{
		alert('select billing type first !');
	}
	else
	{
		if(!lrno.match(/^[0-9a-zA-Z_]+$/))
		{
			alert('Plese check LR Number !');
			$('#lrno').val('');
		}
		else
		{
			$('#lrchk_button').attr("disabled",true);
			$('#lrno').attr("readonly",true);
			
			// $('#LRallInputs *').prop('disabled', true);
			// $('#LRallInputs').hide();
			
			$('#bilty_id').val('');
			$('#bilty_belongs_to').val('');
			
			if($('#consignor_id').val()=='' || $('#consignor').val()=='')
			{
				$('#consignor').focus();
				$("#consignor").attr("onblur","CheckOpenLR();$('#lrno').val('')");
				$('#lrno').val("");
				$('#lrchk_button').attr("disabled",false);
				$('#lrno').attr("readonly",false);
			}
			else if($('#consignee_id').val()=='' || $('#consignee').val()=='')
			{
				$('#consignee').focus();
				$('#lrno').val("");
				$('#lrchk_button').attr("disabled",false);
				$('#lrno').attr("readonly",false);
			}
			else if($('#consignor_id').val()=='56' && $('#consignor_sub_id').val()=='')
			{
				$('#consignor_sub').focus();
				$('#lrno').val("");
				$('#lrchk_button').attr("disabled",false);
				$('#lrno').attr("readonly",false);
			}
			else if($('#shipment_party_sel').val()=='')
			{
				$('#shipment_party_sel').focus();
				$('#lrno').val("");
				$('#lrchk_button').attr("disabled",false);
				$('#lrno').attr("readonly",false);
			}
			else
			{
				$('#consignor').attr('readonly',true);
				$('#consignee').attr('readonly',true);
				$('#consignor_sub').attr('readonly',true);
				$("#consignor").attr("onblur","");
				$("#loadicon").show();
				jQuery.ajax({
					url: "./checklr_entry.php",
					data: 'lrno=' + lrno + '&con1_id=' + $('#consignor_id').val() + '&consignor=' + $('#consignor').val() + '&con2_id=' + $('#consignee_id').val() + '&consignee=' + $('#consignee').val() + '&consignor_sub_id=' + $('#consignor_sub_id').val() + '&billing_type=' + billing_type + '&shipment_party_sel=' + $('#shipment_party_sel').val() + '&shipment_party_id=' + $('#shipment_party_id').val(),
					type: "POST",
					success: function(data) {
						$("#location_unloading_point_div").html(data);
					},	
						error: function() {}
				});
			}  
		}  
	}  
}
</script>

<?php
// if($branch=='KORBA')
// {
	// $fetch_balco=Qry($conn,"SELECT id,name,gst FROM consignor WHERE id='10'");
	
	// if(!$fetch_balco){
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./");
		// exit();
	// }
	
	// if(numRows($fetch_balco)>0)
	// {
		// $row_party = fetchArray($fetch_balco);
		
		// $party_id=$row_party['id'];
		// $party_name=$row_party['name'];
		// $party_gst=$row_party['gst'];
		// $party_name_readonly="readonly";
	// }
	// else
	// {
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// Redirect("Consignor not found for korba.","./");
		// exit();
	// }
// }
// else
// {
	$party_name_readonly="";
	$party_name="";
	$party_gst="";
	$party_id="";
// }
?>

<div class="row">
	
	<div class="form-group col-md-4">
		 <label>Consignor <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'');$('#lrno').val('')" id="consignor" 
		value="<?php echo $party_name; ?>" onblur="CheckOpenLR(this.value);" name="consignor" class="form-control" <?php echo $party_name_readonly; ?> required />
	</div>
	
	<input type="hidden" name="consignor_id" id="consignor_id" value="<?php echo $party_id; ?>" required>
	
	<div class="form-group col-md-2">
		<label>Consignor GST No </label>
		<input id="con1_gst" type="text" name="con1_gst" class="form-control" value="<?php echo $party_gst; ?>" readonly />
	</div>
	
	<div class="form-group col-md-4">
		<label>Consignee <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" id="consignee" name="consignee" class="form-control" required />
	</div>
	
	<div class="form-group col-md-2">
		<label>Consignee GST No </label>
		<input id="con2_gst" type="text" name="con2_gst" class="form-control" readonly />
	</div>
	
	<input type="hidden" name="billing_type" id="billing_type" value="NORMAL">
	
	<!--
	<div class="form-group col-md-2">
		<label>Bill To <font color="red">*</font></label>
       <select name="billing_type" id="billing_type" class="form-control" required="required">
			<option value="">--Select--</option>
			<option id="bill_2" value="NORMAL">Normal Billing - नोर्मल बिलिंग</option>
			<option id="bill_1" value="BILL_TO_SHIP_TO">Bill to Ship to - डिलीवरी पार्टी के नाम बिल</option>
		</select>
    </div>
	-->
	<?php
	if($branch=='PUNE')
	{
	?>
	<div class="form-group col-md-2">
		<label>Select Plant <font color="red">*</font></label>
       <select onchange="CheckBoschLR()" name="pune_plant" id="pune_plant" class="form-control" required="required">
			<option class="pune_plant_val" value="">--Select--</option>
			<option class="pune_plant_val" id="pune_plant_AT" value="AT">AT</option>
			<option class="pune_plant_val" id="pune_plant_AS" value="AS">AS</option>
			<option class="pune_plant_val" id="pune_plant_OTHER" value="OTHER">OTHER</option>
		</select>
    </div>
	
	<script>
	function CheckBoschLR() 
	{
		var plant_name = $('#pune_plant').val();
		
		if(plant_name!='')
		{
			$("#loadicon").show();
				jQuery.ajax({
					url: "./check_lr_plant_for_pune_bosch.php",
					data: 'plant_name=' + plant_name,
					type: "POST",
					success: function(data) {
						$("#lr-status").html(data);
					},	
						error: function() {}
				});
		}
	}
	</script>
	<?php
	}
	else
	{
		
		echo '<input type="hidden" value="" name="pune_plant" />';
	}
	?>
	
	<div style="display:none" class="qwik_party form-group col-md-3">
		 <label>Actual Consignor <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'');" id="consignor_sub" name="consignor_sub" 
		class="form-control" required />
	</div>
	
	<input type="hidden" name="consignor_sub_id" id="consignor_sub_id" required>
	
	<div style="display:none" class="qwik_party form-group col-md-2">
		<label>Actual Consignor GST </label>
		<input id="consignor_sub_gst" type="text" name="consignor_sub_gst" class="form-control" readonly />
	</div>
	
	<div class="form-group col-md-2">
		<label>Delivery/Shipment Party <font color="red">*</font></label>
		<select onchange="ShipmentPartySel(this.value)" required="required" id="shipment_party_sel" name="shipment_party_sel" class="form-control">
			<option class="del_party_sel_opt" value="">--select an option--</option>
			<option class="del_party_sel_opt" id="del_party_SAME" value="SAME">Same as consignee - कनसायनी को डिलीवरी</option>
			<option class="del_party_sel_opt" id="del_party_DIFF" value="DIFF">Delivery party is different - डिलीवरी पार्टी अलग है</option> 
		</select>
	</div>	
	
	<script>
	function ShipmentPartySel(elem)
	{
		$('#shipment_party').val('');
		$('#shipment_party_id').val('');
		$('#shipment_party_gst').val('');
		
		if(elem=='DIFF')
		{
			$('.diff_shipment_party').show();
			$('#shipment_party').attr('required',true);
		}
		else
		{
			$('.diff_shipment_party').hide();
			$('#shipment_party').attr('required',false);
		}
	}
	</script>
	
	<div style="display:none" class="diff_shipment_party form-group col-md-3">
		<label>Delivery Party <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" id="shipment_party" name="shipment_party" class="form-control" />
	</div>

	<input type="hidden" name="shipment_party_id" id="shipment_party_id">

	<div style="display:none" class="diff_shipment_party form-group col-md-2">
		<label>DeliveryParty GST </label>
		<input id="shipment_party_gst" type="text" readonly name="shipment_party_gst" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<div class="row">
			<div class="col-md-8">
				<label>LR No. <font color="red">*</font></label>
				<input value="<?php echo $last_lrno; ?>" oninput="this.value=this.value.replace(/[^0-9]/,'');CheckConsignor()" type="text" name="lrno" id="lrno" class="form-control" required />
			</div>
			<div class="col-md-4">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="lrchk_button" onclick="checkLr()" class="btn btn-sm btn-primary"><i style="font-size:14px;" class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Check</button>
			</div>		
		</div>		
    </div>

	<input type="hidden" id="bilty_id" name="bilty_id" />		
	<input type="hidden" id="bilty_belongs_to" name="bilty_belongs_to" />	
	
<div id="LRallInputs">
	
	<div id="man_company_div" style="display:none" class="form-group col-md-2">
		<label>Company <font color="red">*</font></label>
       <select name="company_man" id="company_man" class="form-control" required="required">
			<option value="">Select Company</option>
			<option class="company_man_sel" id="company_man_RRPL" value="RRPL">RRPL</option>
			<option class="company_man_sel" id="company_man_RAMAN_ROADWAYS" value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
    </div>
	
	<div id="auto_company_div" class="form-group col-md-2">
         <label>Company <font color="red">*</font></label>
         <input type="text" id="company_auto" name="company_auto" class="form-control" readonly required />
	</div>
	
	<div id="lr-status"></div>

<script type="text/javascript">			
function VehicleType2(elem)
{
	$("#tno_market").val('');
	$("#tno_own").val('');
	
	if($('#consignor_id').val()=='' || $('#consignor').val()=='')
	{
		$('#consignor').focus();
		$('#lrno').val("");
		$('#lr_type').val("");
	}
	else if($('#consignee_id').val()=='' || $('#consignee').val()=='')
	{
		$('#consignee').focus();
		$('#lrno').val("");
		$('#lr_type').val("");
	}
	else
	{
		if($('#lrno').val()=='')
		{
			$('#lrno').focus();
			$('#lr_type').val("");
		}
		else
		{
			if(elem=="OWN")
			{
				$("#div_market").hide();
				$("#tno_market").attr("required",false);
				
				$("#div_own").show();	
				$("#tno_own").attr("required",true);
			}
			else
			{
				$("#div_market").show();
				$("#tno_market").attr("required",true);
				
				$("#div_own").hide();	
				$("#tno_own").attr("required",false);
			}
		}
	}
}
</script>	

	<?php
	/*
	if($branch=='KORBA')
	{
	?>
	
	<script>
	$(function() {
		$("#tno_market").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_market').val(ui.item.value);   
            $('#wheeler_market_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_market').val("");   
			$('#wheeler_market_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#tno_own").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_own').val(ui.item.value);   
            $('#wheeler_own_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_own').val("");   
			$('#wheeler_own_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});	
	</script>
	
	<div class="form-group col-md-2">
		<label>Vehicle Type <font color="red">*</font></label>
		<select onchange="VehicleType2(this.value)" name="lr_type" id="lr_type" class="form-control" required="required">
			<option value="">Select Vehicle Type</option>
			<option value="MARKET">MARKET Vehicle</option>
			<option value="OWN">OWN Vehicle</option>
		</select>
	</div>
							
	<div class="form-group col-md-2" id="div_market" style="display:none1">
		<label>Market TruckNo <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_market" id="tno_market" type="text" class="form-control" required />
    </div>
	
	<input type="hidden" id="wheeler_market_truck" name="wheeler_market">
	<input type="hidden" id="wheeler_own_truck" name="wheeler_own_truck">
	
	<div class="form-group col-md-2" id="div_own" style="display:none">
		<label>Own TruckNo <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="tno_own" id="tno_own" type="text" class="form-control" required />
	</div>
	
	<input type="hidden" id="validation_type" value="NOT_AUTO">
	<?php
	}
	else
	{
	*/
	?>	
	<div class="form-group col-md-2">
		<label>Vehicle Type <font color="red">*</font></label>
		<input value="<?php echo $_SESSION['verify_vehicle_type']; ?>" type="text" readonly name="lr_type" id="lr_type" class="form-control" required="required">
	</div>
							
	<div class="form-group col-md-2" id="div_market" style="display:none1">
		<label><?php if($_SESSION['verify_vehicle_type']=='MARKET'){ echo "Market"; } else { echo "Own"; } ?> Vehicle No. <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" 
		value="<?php echo $_SESSION['verify_vehicle']; ?>" readonly="readonly" name="vehicle_no" id="vehicle_no" type="text" 
		class="form-control" required />
    </div>
	
	<input type="hidden" id="validation_type" value="AUTO">
	<input type="hidden" id="wheeler_vehicle" value="<?php echo $_SESSION['verify_vehicle_wheeler']; ?>" name="wheeler_vehicle">
	<?php
	// }
	?>
	<div class="form-group col-md-2">
		<label>LR Date <font color="red">*</font></label>
		<input id="lr_date" name="date" type="date" class="form-control" min="<?php echo $min_lr_date; ?>" 
		max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
<script>	
function LoadingPointFunc(elem)
{
	var parts = elem.split('_', 3);
	var loading_point_id = parts[0];
	var from_id  = parts[1];
	var from_loc  = parts[2];
	
	$('#loading_point_id').val(loading_point_id);
	$('#from_id').val(from_id);
	$('#from').val(from_loc);
}

function UnloadingPointFunc(elem)
{
	var parts = elem.split('_', 3);
	var unloading_point_id = parts[0];
	var to_id  = parts[1];
	var to_loc  = parts[2];
	
	$('#unloading_point_id').val(unloading_point_id);
	$('#to_id').val(to_id);
	$('#to').val(to_loc);
}
</script>	
	
	<div id="location_unloading_point_div"></div>
	
	<div class="form-group col-md-3"> 
		<label>From Station <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z-.]/,'')" readonly name="from" id="from" class="form-control" required />
    </div>
	
	<input type="hidden" id="from_id" name="from_id">
	<input type="hidden" id="loading_point_id" name="loading_point_id"> 
	<input type="hidden" id="unloading_point_id" name="unloading_point_id"> 
	<input type="hidden" id="to_id" name="to_id"> 
	<input type="hidden" id="billing_station_id" name="billing_station_id"> 
	
	<div class="form-group col-md-3">
		<label>To Station/Destination <font color="red">*</font></label>
		<input name="to" oninput="this.value=this.value.replace(/[^a-z A-Z-.]/,'')" readonly type="text" id="to" class="form-control" required />
	</div>
	
	<div class="form-group col-md-3">
		<label>Billing Rate Station <font color="red">* <sup>जहा का भाड़ा मिलेगा</sup></font></label>
		<input name="billing_station" oninput="this.value=this.value.replace(/[^a-z A-Z-.]/,'')" type="text" id="billing_station" class="form-control" required />
	</div>
	
	<div style="display:none" id="dest_zone_div" class="form-group col-md-3">
		<label>Transportation Zone <font color="red">*</font></label>
		<input name="dest_zone" oninput="this.value=this.value.replace(/[^a-z A-Z-.]/,'')" type="text" id="dest_zone" class="form-control" />
	</div>

	<input type="hidden" id="dest_zone_id" name="dest_zone_id">
	<input type="hidden" id="bill_to_party_id" name="bill_to_party_id">
	
	<div id="bill_to_party_div">
	
	<div class="form-group col-md-3">
		<label>Bill To <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" id="bill_to_party" name="bill_to_party" class="form-control" required />
	</div>
	
	<div class="form-group col-md-3">
		<label>Bill To GST <font color="red">*</font></label>
		<input type="text" readonly id="bill_to_gst" name="bill_to_gst" class="form-control" required />
	</div>
	
	</div>
	
	<div id="eway_result2"></div>
	
	<div class="form-group col-md-3">
		<label>Item Group <font color="red">*</font></label>
		<input id="item1" name="item" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');" placeholder="Item Group" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>Total Invoice Value <font color="red">*</font></label>
		<input onblur="InvoiceValueFunc(this.value)" min="0" id="invoice_value_ewb" name="invoice_value" type="number" step="any" placeholder="Invoice value" class="form-control" required />
    </div>
	
	<div class="form-group col-md-2">
		<label>Bilty Type <font color="red">*</font></label>
		<select onchange="BiltyTypeLR(this.value)" name="bilty_type" id="bilty_type" class="form-control" required="required">
			<option value="">--select an option--</option>
			<option value="TO_PAY">TO-PAY</option>
			<option value="TBB">To Be Billed</option>
		</select>
	</div>
	
	<input type="hidden" id="consignee_id" name="consignee_id">
	<input type="hidden" id="consignee_pincode" name="consignee_pincode">
	
	<!-- -->
		<input type="hidden" id="EwayBillFlag" name="EwayBillFlag">
		<input type="hidden" id="EwayBillFlag_desc" name="EwayBillFlag_desc">
		
		<input type="hidden" id="EwayBill_con1_gst" name="EwayBill_con1_gst">
		<input type="hidden" id="EwayBill_con1" name="EwayBill_con1">
		<input type="hidden" id="EwayBill_con2_gst" name="EwayBill_con2_gst">
		<input type="hidden" id="EwayBill_con2" name="EwayBill_con2">
		<input type="hidden" id="EwayBill_tno" name="EwayBill_tno">
		
		<!-- dates & extended or not -->
		<input type="hidden" id="EwayBill_reg_date" name="EwayBill_reg_date">
		<input type="hidden" id="EwayBill_exp_date" name="EwayBill_exp_date">
		<input type="hidden" id="EwayBill_extended" name="EwayBill_extended">
		<!-- dates & extended or not -->
		
		<input type="hidden" id="EwayBill_ewb_no" name="ewb_no_db">
		<input type="hidden" id="EwayBill_ewayBillDate" name="ewayBillDate">
		<input type="hidden" id="EwayBill_docNo" name="ewb_docNo"> 
		<input type="hidden" id="EwayBill_docDate" name="ewb_docDate">
		<input type="hidden" id="EwayBill_fromPincode" name="ewb_fromPincode">
		<input type="hidden" id="EwayBill_totInvValue" name="ewb_totInvValue">
		<input type="hidden" id="EwayBill_totalValue" name="ewb_totalValue">
		<input type="hidden" id="EwayBill_toPincode" name="ewb_toPincode"> 
		<input type="hidden" id="EwayBill_bypass" name="EwayBill_bypass">
		<input type="hidden" id="by_pass_narr" name="by_pass_narr">
		<input type="hidden" id="by_pass_id" name="by_pass_id">
	<!-- -->
	
<div class="form-group col-md-12">
	
	<div class="row">
	
	<div class="form-group col-md-3">
		<label>DO No. <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" name="do_no" id="do_no" class="form-control" required />
	</div>
	
    <div class="form-group col-md-3">
		<label>Invoice No. <font color="red">*</font></label>
		<input name="invno" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" type="text" id="invno" class="form-control" required />
    </div>
   
	<div class="form-group col-md-3">
		<label>Shipment/BE/LC No. <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9]/,'')" style="text-transform:uppercase" name="shipno" id="shipno" class="form-control" required />
     </div>
	
	<div class="form-group col-md-3">
		<label>PO No. <font color="red">*</font></label>
		<input name="po_no" oninput="this.value=this.value.replace(/[^a-z A-Z-0-9,]/,'')" type="text" class="form-control" required />
    </div>
   
	</div>	
</div>	
	
<div class="form-group col-md-12">
	
	<div class="row">
	
	<div class="form-group col-md-3">
		<label>Act.Wt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '90';}else{echo '50';} ?>" name="act_wt" type="number" id="act_wt" step="any" class="form-control" required />
    </div> 

	<div class="form-group col-md-3"> 
		<label>GrossWt (in MT)<font color="red">*</font></label>
		<input min="0.010" max="<?php if($branch=='MAKRANA'){echo '90';}else{echo '50';} ?>" name="gross_wt" type="number" id="gross_wt" step="any" class="form-control" required />
    </div> 	
	
	<div class="form-group col-md-3">
       <label>Chrg.Wt (in MT) <font color="red">*</font></label>
	   <input onchange="ResetAll();" max="<?php if($branch=='MAKRANA'){echo '90';}else{echo '50';} ?>" oninput="ResetAll();;" onblur="ResetAll();" min="0.010" name="chrg_wt" type="number" id="chrg_wt" class="form-control" step="any" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>E-way Bill Number <font color="red"><sup>*</sup></font></label>
		<input id="eway_bill_no" <?php if($branch!='KORBA') { echo 'onblur="FetchEwayBill(this.value)"'; } ?> type="text" maxlength="12" name="eway_bill_no" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required />
	</div>
	
	<input value="1" type="hidden" name="is_validate" id="is_validate">
	
    </div>
</div>
	
<?php
if($branch=='KORBA' || $branch=='RAIPUR')
{
?>	
<div class="form-group col-md-12">
	
	<div class="row">	
	
	<div class="form-group col-md-2">
		<label>Bank </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,]/,'')" name="bank" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>Sold To PARTY </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.-,]/,'')" name="sold_to_pay" class="form-control" />
	</div>
	
	<div class="form-group col-md-3">
		<label>LR Name </label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.-,]/,'')" name="lr_name" class="form-control" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Consignee Address </label>
		<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-.,/]/,'')" name="con2_addr" class="form-control"></textarea>
	</div>
	
	</div>
</div>
<?php
}
else
{
		echo "<input type='hidden' name='bank' value='' />";
		echo "<input type='hidden' name='sold_to_pay' value='' />";
		echo "<input type='hidden' name='lr_name' value='' />";
		echo "<input type='hidden' name='con2_addr' value='' />";
}
?> 
	<input type="hidden" name="item_id" id="item_id">
	
	<!--
	<div class="form-group col-md-12">
		<span style="font-size:13px;font-weight:bold;color:red">----- Optional fields ------</span>
	</div>-->

	<div class="form-group col-md-2" id="EwayBill_copy_div" style="display:none">
		<label>Eway-bill Copy <font color="red">*</font></label>
		<input type="file" id="EwayBill_copy_file" accept="image/*,.pdf" name="ewb_copy" class="form-control" />
    </div>
	
	<div class="form-group col-md-3">
		<label>No of Articles </label>
		<input placeholder="No of Bags Etc." name="articles" type="text" oninput="this.value=this.value.replace(/[^a-z- A-Z,0-9]/,'')" class="form-control" />
    </div>

	<div class="form-group col-md-3">
		<label>Goods Value </label>
		<input placeholder="Goods Value" min="0" step="any" name="goods_value" type="number" class="form-control" />
    </div>
	
<script>	
function BiltyTypeLR(elem)
{
	if(elem=='TO_PAY')
	{
		$('#b_rate').attr('required',true);
		$('#b_amt').attr('required',true);
		$('.rate_req').html('*');
	}
	else
	{
		$('#b_rate').attr('required',false);
		$('#b_amt').attr('required',false);
		$('.rate_req').html('');
	}
}
</script>	
	
	<div class="form-group col-md-2">
		<label>Billing Rate <span style="color:red" class="rate_req" id="rate_req"></span></label>
		<input onchange="sum1();" onkeyup="sum1();" oninput="sum1();" onblur="sum1();" value="0" min="0" step="any" name="b_rate" type="number" id="b_rate" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Billing Amount <span style="color:red" class="rate_req" id="rate_req"></span></label>
		<input onblur="sum2();" onchange="sum2();" oninput="sum2();" onkeyup="sum2();" min="0" step="any" name="b_amt" type="number" id="b_amt" class="form-control" />
    </div>
	
	<div class="form-group col-md-2">
		<label>Truck Type</label>
		<select name="t_type" class="form-control">
			<option value="">--Select Option--</option>
			<?php
			$get_t_type = Qry($conn,"SELECT title FROM truck_type WHERE hide=0");
			if(numRows($get_t_type)>0)
			{
				while($row_t_type = fetchArray($get_t_type))
				{
					echo "<option value='$row_t_type[title]'>$row_t_type[title]</option>";	
				}
			}
			?>
		</select>
	</div>
	
	<?php
	if($branch=='KORBA')
	{
	?>	
		<div class="form-group col-md-2">
		   <label>Plant </label>
		   <select name="plant_name" class="form-control">
				<option value="">--select--</option>
				<option value="1101">1101</option>
				<option value="1102">1102</option>
			</select>
		</div>	
	<?php
	}
	else
	{
		echo "<input type='hidden' name='plant_name' value=''>";
	}
	?>
	
	<?php
	if($branch=='KORBA' || $branch=='RAIPUR')
	{
	?>	
		<div class="form-group col-md-2">
		   <label>Freight Type </label>
		   <select name="freight_type" class="form-control">
				<option value="">NULL</option>
				<option value="PREPAID">PREPAID</option>
				<option value="TOPAY">TOPAY</option>
			</select>
		</div>	
	<?php
	}
	else
	{
		echo "<input type='hidden' name='freight_type' value=''>";
	}
	?>
	
	<div class="form-group col-md-<?php if($branch=='KORBA') { echo "2"; } else { echo "3"; } ?>">
		<label>HSN Code </label>
		<input name="hsn_code" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" />
    </div>
	
	<div class="form-group col-md-3">
		<label>Goods Description </label>
		<textarea name="goods_desc" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')" class="form-control"></textarea>
    </div>
	
	<div class="form-group col-md-3">
		<label>Material/Item <font color="red">*</font></label>
		<textarea placeholder="Product/Item" required="required" id="item_name222" name="item_name" 
		oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" class="form-control"></textarea>
	</div>
	
	<?php	
	if($branch=='PUNE')
	{
	?>
	<div class="form-group col-md-3">
		<label>Route Type: <font color="red">*</font></label>
			<select name="route_type" required="required" class="form-control">	
				<option value="">Select an option</option>
				<option value="0">By Road</option>
				<option value="1">Express</option>
				<option value="2">Not BOSCH</option>
			</select>
	</div>
	
	<div id="trip_type_div" style="display:none" class="form-group col-md-12">
	
	<div class="row">
		<div class="form-group col-md-3">
			<label>Trip Type: <font color="red">*</font></label>
			<select onchange="TripTypeCheck(this.value)" name="trip_type" id="trip_type_pune" required="required" class="form-control">	
				<option value="">Select an option</option>
				<option value="One_Way">One way</option>
				<option value="Round">Round Trip</option>
			</select>
		</div>
		
		<div id="round_trip_lrno_div" style="display:none" class="form-group col-md-3">
			<label>Round Trip LR: <font color="red">*</font></label>
			<input name="round_trip_lrno" id="round_trip_lrno" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" required />
		</div>
		
	</div>
	
	</div>
	
	<script>
	function TripTypeCheck(elem)
	{
		if(elem=='Round'){
			$('#round_trip_lrno_div').show();
			$('#round_trip_lrno').attr('required',true);
		}
		else{
			$('#round_trip_lrno_div').hide();
			$('#round_trip_lrno').attr('required',false);
		}
	}
	</script>
	<?php
	}	
	else
	{
		echo "<input name='route_type' value='0' type='hidden' required />";
		echo "<input name='trip_type' value='0' type='hidden' required />";
	}
	?>
	 
	<div id="unit_name_vizag_div" style="display:none" class="form-group col-md-3">
		<label>Unit name <font color="red"><sup>*</sup></font> </label>
		<select name="unit_name_vizag" id="unit_name_vizag" class="form-control">
			<option value="">--select--</option>
			<option value="Unit: BEEKAY STRUCTURAL STEELS AUTONAGAR VIZAG">BEEKAY STRUCTURAL STEELS AUTONAGAR VIZAG</option>
			<option value="Unit: BEEKAY STRUCTURAL STEELS - TMT BAR DIVISION">BEEKAY STRUCTURAL STEELS - TMT BAR DIVISION</option>
		</select>
	</div>	
		
</div>	
	
	<div class="form-group col-md-12">	
		<button type="button" onclick="ResetVehicle()" class="btn btn-danger btn-sm pull-left">Reset Vehicle Data</button>
		<button type="submit" disabled="disabled" id="lr_sub" name="lr_sub" style="margin-left:10px;display:none" class="btn btn-sm btn-primary pull-right">ADD LR</button>
		<button type="button" class="btn btn-default btn-sm pull-right" onclick="window.location.href='_fetch_lr_entry.php'">Cancel</button>
	</div>	
 
	</div>	
   </div>
  </div>
 </div>
</div>
</form>   

<button id="button_main_lr_modal" type="button" data-toggle="modal" data-target="#add_new_record_modal" style="display:none"></button>

<script type="text/javascript">
function sum1() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_amt").val(Math.round(Number($("#chrg_wt").val()) * Number($("#b_rate").val())).toFixed(2));
	}
}

function sum2() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_rate").val(Math.round(Number($("#b_amt").val()) / Number($("#chrg_wt").val())).toFixed(2));
	}	
}

function ResetAll() 
{
	$("#b_amt").val('');
	$("#b_rate").val('');
}
</script>

<?php
if(isset($_POST['myVar']))
{
	echo "<script>
		$('#close_verify_modal')[0].click();
	</script>";	
}
?>

<script>
	$('#button_main_lr_modal')[0].click();
	$('#LRallInputs *').prop('disabled', true);
	// $('#LRallInputs').hide();
	$("#loadicon").fadeOut('slow');
</script>  