<?php
require('./connection.php');

$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$timestamp = date("Y-m-d H:i:s");

if(strlen($pan_no)!=10)
{
	echo "<script>
		alert('Please enter valid PAN card number !');
		$('#btn_verify_pan').attr('disabled',false);
		$('#add_broker_button').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_pan = Qry($conn,"select name_on_card,timestamp from zoop_pan where pan_number='$pan_no' ORDER BY id DESC LIMIT 1");

if(!$chk_pan){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_pan = fetchArray($chk_pan);

$hourdiff = round((strtotime($timestamp) - strtotime($row_pan['timestamp']))/3600, 1);

if(numRows($chk_pan)==0 || $hourdiff>36)
{
	$curl = curl_init();

	  curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://prod.aadhaarapi.com/pan",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>"{\"pan\": \"$pan_no\"}",
	  CURLOPT_HTTPHEADER => array(
	    "qt_api_key: 74b6283a-c1b0-4199-a7e6-f2bc2f176b83",
	    "qt_agency_id: 80c497b4-bc77-42f5-8e9f-56ec2c4beb0c",
	    "Content-Type: application/json"
	  ),
	));

	$output = curl_exec($curl); 
	curl_close($curl); 
 
	$response = json_decode($output, true);
	$row_data = $response['data'][0];
	$response_code = $response['response_code'];
	$pan_status = $row_data['pan_status'];
	
	if($response_code=="1")
	{
		if($pan_status=="VALID")
		{
			$pan_holder = $row_data['name_on_card'];
	
			$last_updated = date_format(date_create_from_format("d/m/Y",$row_data['pan_last_updated']), "Y-m-d");
		
			$insert_pan = Qry($conn,"INSERT INTO zoop_pan(json_data,request_id,request_timestamp,pan_number,pan_status,
			pan_holder_title,first_name,last_name,name_on_card,pan_last_updated,aadhaar_seeding_status,branch,
			branch_user,timestamp) VALUES ('$output','$response[id]','$response[request_timestamp]','$pan_no',
			'$pan_status','$row_data[pan_holder_title]','$row_data[first_name]','$row_data[last_name]','$row_data[name_on_card]',
			'$last_updated','$row_data[aadhaar_seeding_status]','$branch','$branch_sub_user','$timestamp')");

			if(!$insert_pan){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
		}
		else
		{
			echo "<script>
				alert('Invalid PAN card number !');
				$('#btn_verify_pan').attr('disabled',false);
				$('#add_broker_button').attr('disabled',true);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	} 
	else
	{
 		$content = "Code: ".$response['response_code']." ".$response['response_message'];
		$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
		
		$insert_err2 = Qry($conn,"INSERT INTO zoop_error_log(req_id,api_name,doc_no,branch,branch_user,
		error_code,error_desc,timestamp) VALUES ('$response[id]','PAN','$pan_no','$branch','$branch_sub_user',
		'$response[response_code]','$response[response_message]','$timestamp')");
			
		echo "<script>
			alert('API Error : $content !');
			$('#btn_verify_pan').attr('disabled',false);
			$('#add_broker_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
}
else
{
	$pan_holder = $row_pan['name_on_card'];
}

	echo "<script>
			$('#pan_no').attr('readonly',true);
			$('#pan_no').val('$pan_no');
			$('#broker_name_api').attr('readonly',true);
			$('#broker_name_api').val('$pan_holder');
			$('#btn_verify_pan').attr('disabled',true);
			$('#add_broker_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
		
?>