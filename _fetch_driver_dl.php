<?php
require('./connection.php');

$lic_no = escapeString($conn,strtoupper($_POST['lic_no']));
$timestamp = date("Y-m-d H:i:s");

if($lic_no=='')
{
	echo "<script>
		alert('Please enter license number !');
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',false);
		$('#button_sub').attr('disabled',true);
		$('#lic_no').attr('readonly',false);
		$('#button_sub').hide();
	</script>";
	exit();
}

$driver_mobile = "";

$check_driver_mobile = Qry($conn,"SELECT mo1 FROM mk_driver WHERE pan='$lic_no'");

if(!$check_driver_mobile){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',false);
		$('#button_sub').attr('disabled',true);
		$('#button_sub').hide();
	</script>";
	exit();
}

if(numRows($check_driver_mobile) > 0)
{
	$row_mobile = fetchArray($check_driver_mobile);
	
	$driver_mobile = $row_mobile['mo1'];
}

$qry_check = Qry($conn,"SELECT name,father_or_husband_name,permanent_address,permanent_zip,ola_name,dob,doe,transport_doe,doi,image 
FROM dl_api WHERE lic_no='$lic_no'");

if(!$qry_check){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',false);
		$('#button_sub').attr('disabled',true);
		$('#button_sub').hide();
	</script>";
	exit();
}

if(numRows($qry_check) > 0)
{
	$row = fetchArray($qry_check);
	
	echo "<script>
		$('#dl_driver_name').html('$row[name]');
		$('#dl_father_name').html('$row[father_or_husband_name]');
		$('#dl_dob').html('$row[dob]');
		$('#dl_exp').html('$row[doe]');
		$('#dl_exp_tp').html('$row[transport_doe]');
		$('#dl_issue_date').html('$row[doi]');
		$('#dl_addr').html('$row[permanent_address]-$row[permanent_zip]');
		$('#dl_issue_by').html('$row[ola_name]');
		$('#dl_driver_photo').attr('src','data:image/png;base64,$row[image]');
		// $('#dl_driver_photo').html('data:image/png;base64,$row[image]');
	</script>";
	
	if(strtotime($row['doe'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#dl_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#dl_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($row['transport_doe'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#dl_exp_tp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#dl_exp_tp').attr('style','color:red');</script>";
	}
	
	$datediff = strtotime(date("Y-m-d")) - strtotime($row['transport_doe']);
	$datediff = round($datediff / (60 * 60 * 24));
	
	if($datediff>=90)
	{
		echo "<script>
			$('#dl_exp_tp').attr('style','color:red');
			$('#dl_exp_tp').html('Not Transport License or Expired.<br>$row[transport_doe]');
			
			$('#spinner_icon_dl').hide();
			$('#fetch_button_dl').attr('disabled',false);
			
			$('#button_sub').attr('disabled',true);
			$('#button_sub').hide();
			$('.dl_res_div').show();
			$('.mobile_no_div').hide();
		</script>";
	}
	else
	{
	?>
	<script>
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',true);
		
		$('#button_sub').attr('disabled',false);
		$('#button_sub').show();
		$('.dl_res_div').show();
		$('.mobile_no_div').show();
	</script>
	<?php
	}
	
	if($driver_mobile!='')
	{
		echo "<script>
			$('#dl_mobile_no').val('$driver_mobile');
			$('#dl_mobile_no').attr('readonly',true);
		</script>";
	}
	
	exit();
}

$check_api = Qry($conn,"SELECT id FROM _verification_apis WHERE api_name='DL' AND status='0'");

if(!$check_api){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		// $('#button_sub').hide();
	</script>";
	exit();
}

if(numRows($check_api) > 0)
{
	$chk_driver = Qry($conn,"SELECT name,full FROM mk_driver WHERE pan='$lic_no'");
		
		if(numRows($chk_driver) == 0)
		{
			echo "<script>
				alert('Add driver first to continue !');
				$('#spinner_icon_dl').hide();
				$('#fetch_button_dl').attr('disabled',false);
				$('#button_sub').attr('disabled',true);
				$('#lic_no').attr('readonly',false);
				$('#button_sub').hide();
			</script>";
			exit();
		}
		
		$row_driver = fetchArray($chk_driver);
		
		echo "<script>
			$('#dl_driver_name').html('$row_driver[name]');
			$('#dl_father_name').html('NA');
			$('#dl_dob').html('NA');
			$('#dl_exp').html('NA');
			$('#dl_exp_tp').html('NA');
			$('#dl_issue_date').html('NA');
			$('#dl_addr').html('$row_driver[full]');
			$('#dl_issue_by').html('NA');
			$('#dl_driver_photo').attr('src','');
			
			$('#spinner_icon_dl').hide();
			$('#fetch_button_dl').attr('disabled',true);
			
			$('#button_sub').attr('disabled',false);
			$('#button_sub').show();
			$('.dl_res_div').show();
			$('.mobile_no_div').show();
		</script>";
		exit();	
}

$data = array(
"id_number"=>"$lic_no",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$aadhar_api_url/api/v1/driving-license/driving-license",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$aadhar_api_token.''
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response, true);
	
if($result['status_code']!='200')
{
	$error_msg = clean_input($result['message']);
	$error_code = $result['status_code'];
	
	$insert_error = Qry($conn,"INSERT INTO dl_api_error(lic_no,error_code,result,branch,branch_user,timestamp) VALUES ('$lic_no','$error_code',
	'$error_msg','$branch','$branch_sub_user','$timestamp')"); 
	
	if($error_code=='500')
	{
		$chk_driver = Qry($conn,"SELECT name,full FROM mk_driver WHERE pan='$lic_no'");
		
		if(numRows($chk_driver) == 0)
		{
			echo "<script>
				alert('Add driver first to continue !');
				$('#spinner_icon_dl').hide();
				$('#fetch_button_dl').attr('disabled',false);
				$('#button_sub').attr('disabled',true);
				$('#lic_no').attr('readonly',false);
				$('#button_sub').hide();
			</script>";
			exit();
		}
		
		$row_driver = fetchArray($chk_driver);
		
		echo "<script>
			$('#dl_driver_name').html('$row_driver[name]');
			$('#dl_father_name').html('NA');
			$('#dl_dob').html('NA');
			$('#dl_exp').html('NA');
			$('#dl_exp_tp').html('NA');
			$('#dl_issue_date').html('NA');
			$('#dl_addr').html('$row_driver[full]');
			$('#dl_issue_by').html('NA');
			$('#dl_driver_photo').attr('src','');
			
			$('#spinner_icon_dl').hide();
			$('#fetch_button_dl').attr('disabled',true);
			
			$('#button_sub').attr('disabled',false);
			$('#button_sub').show();
			$('.dl_res_div').show();
			$('.mobile_no_div').show();
		</script>";
		exit();
	}
	else if($error_code=='422')
	{
		$chk_dl_by_pass = Qry($conn,"SELECT id FROM _by_pass_dl_check WHERE lic_no='$lic_no'");
		
		if(numRows($chk_dl_by_pass) == 0)
		{
			echo "<script>
				alert('DL Verification Failed !');
				$('#spinner_icon_dl').hide();
				$('#fetch_button_dl').attr('disabled',false);
				$('#button_sub').attr('disabled',true);
				$('#button_sub').hide();
			</script>";
			exit();
		}
		else
		{
			$chk_driver = Qry($conn,"SELECT name,full FROM mk_driver WHERE pan='$lic_no'");
		
			if(numRows($chk_driver) == 0)
			{
				echo "<script>
					alert('Add driver first to continue !');
					$('#spinner_icon_dl').hide();
					$('#fetch_button_dl').attr('disabled',false);
					$('#button_sub').attr('disabled',true);
					$('#lic_no').attr('readonly',false);
					$('#button_sub').hide();
				</script>";
				exit();
			}
			
			$row_driver = fetchArray($chk_driver);
			
			echo "<script>
				$('#dl_driver_name').html('$row_driver[name]');
				$('#dl_father_name').html('NA');
				$('#dl_dob').html('NA');
				$('#dl_exp').html('NA');
				$('#dl_exp_tp').html('NA');
				$('#dl_issue_date').html('NA');
				$('#dl_addr').html('$row_driver[full]');
				$('#dl_issue_by').html('NA');
				$('#dl_driver_photo').attr('src','');
				
				$('#spinner_icon_dl').hide();
				$('#fetch_button_dl').attr('disabled',true);
				
				$('#button_sub').attr('disabled',false);
				$('#button_sub').show();
				$('.dl_res_div').show();
				$('.mobile_no_div').show();
			</script>";
			exit();
		}
	}
	
	echo $error_code." : ".$error_msg;
	echo "<script>
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',false);
		$('#button_sub').attr('disabled',true);
		$('#lic_no').attr('readonly',false);
		$('#button_sub').hide();
	</script>";
	exit();
}

$client_id = $result['data']['client_id'];	

$client_id = $result['data']['client_id'];	
$state = $result['data']['state'];	
$name = $result['data']['name'];
$permanent_address = $result['data']['permanent_address'];
$permanent_zip = $result['data']['permanent_zip'];
$temporary_address = $result['data']['temporary_address'];
$temporary_zip = $result['data']['temporary_zip'];
$citizenship = $result['data']['citizenship'];
$ola_name = $result['data']['ola_name'];
$ola_code = $result['data']['ola_code'];
$gender = $result['data']['gender'];
$father_or_husband_name = $result['data']['father_or_husband_name'];
$dob = $result['data']['dob'];
$doe = $result['data']['doe'];
$transport_doe = $result['data']['transport_doe'];
$doi = $result['data']['doi'];
$blood_group = $result['data']['blood_group'];
$vehicle_classes1 = array();
$vehicle_classes = $result['data']['vehicle_classes'];
$vehicle_classes1[] = $vehicle_classes;
$vehicle_classes1 = @implode(",",@$vehicle_classes1);
$image = $result['data']['profile_image'];  

$qry = Qry($conn,"INSERT INTO dl_api(lic_no,name,father_or_husband_name,permanent_address,permanent_zip,temporary_address,temporary_zip,
citizenship,ola_name,ola_code,gender,dob,doe,transport_doe,doi,blood_group,vehicle_classes,image,branch,branch_user,timestamp) VALUES 
('$lic_no','$name','$father_or_husband_name','$permanent_address','$permanent_zip','$temporary_address','$temporary_zip','$citizenship',
'$ola_name','$ola_code','$gender','$dob','$doe','$transport_doe','$doi','$blood_group','$vehicle_classes1','$image','$branch','$branch_sub_user',
'$timestamp')");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon_dl').hide();
		$('#fetch_button_dl').attr('disabled',false);
		$('#button_sub').attr('disabled',true);
		$('#button_sub').hide();
		$('#lic_no').attr('readonly',false);
	</script>";
	exit();
}

	echo "<script>
		$('#dl_driver_name').html('$name');
		$('#dl_father_name').html('$father_or_husband_name');
		$('#dl_dob').html('$dob');
		$('#dl_exp').html('$doe');
		$('#dl_exp_tp').html('$transport_doe');
		$('#dl_issue_date').html('$doi');
		$('#dl_addr').html('$permanent_address-$permanent_zip');
		$('#dl_issue_by').html('$ola_name');
		$('#dl_driver_photo').attr('src','data:image/png;base64,$image');
	</script>";
	
	if(strtotime($doe)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#dl_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#dl_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($transport_doe)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#dl_exp_tp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#dl_exp_tp').attr('style','color:red');</script>";
	}
	
	$datediff = strtotime(date("Y-m-d")) - strtotime($transport_doe);
	$datediff = round($datediff / (60 * 60 * 24));

	if($datediff>=90)
	{
		echo "<script>
			$('#dl_exp_tp').attr('style','color:red');
			$('#dl_exp_tp').html('Not Transport License or Expired.<br>$transport_doe');
			
			$('#spinner_icon_dl').hide();
			$('#fetch_button_dl').attr('disabled',false);
			
			$('#button_sub').attr('disabled',true);
			$('#button_sub').hide();
			$('.dl_res_div').show();
			$('.mobile_no_div').hide();
		</script>";
	}
	else
	{
?>
<script>
	$('#spinner_icon_dl').hide();
	$('#fetch_button_dl').attr('disabled',true);
	
	$('#button_sub').attr('disabled',false);
	$('#button_sub').show();
	$('.dl_res_div').show();
	$('.mobile_no_div').show();
</script>
<?php
	}
	
	if($driver_mobile!='')
	{
		echo "<script>
			$('#dl_mobile_no').val('$driver_mobile');
			$('#dl_mobile_no').attr('readonly',true);
		</script>";
	}
?>
