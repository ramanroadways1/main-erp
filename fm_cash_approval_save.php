<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$frno1 = escapeString($conn,strtoupper($_POST['fm_no']));

$frno = explode("_",$frno1)[0];
$lr_date = explode("_",$frno1)[1];
$create_date_fm = explode("_",$frno1)[2];
$create_date_fm2 = date("d-m-Y",strtotime($create_date_fm));
$adv_bal = escapeString($conn,($_POST['adv_bal']));
$narration = escapeString($conn,($_POST['narration']));
$amount = escapeString($conn,($_POST['amount']));

$get_record = Qry($conn,"SELECT id FROM allow_cash WHERE vou_no='$frno' AND adv_bal='$adv_bal'");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./fm_cash_approval.php");
	exit(); 
}

if(numRows($get_record)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO allow_cash(lrno,vou_no,lr_date,amount,adv_bal,narration,branch,branch_user,timestamp) VALUES ('$lrno','$frno',
'$lr_date','$amount','$adv_bal','$narration','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./fm_cash_approval.php");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./fm_cash_approval.php';
	</script>";
	exit();
?>