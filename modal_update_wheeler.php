<form id="WheelerForm" action="#" method="POST">
<div id="ModalWheeler" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Wheeler
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-4">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" id="tno_wheeler" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Vehicle Owner Name <font color="red"><sup>*</sup></font></label>
				<input id="wheeler_name" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input id="wheeler_mobile" readonly type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Wheeler <font color="red"><sup>*</sup></font></label>
				<select name="wheeler" id="wheeler_set" required="required" class="form-control" />
						<option value="">Select an option</option>
						<option value="4">4</option>
						<option value="6">6</option>
						<option value="10">10</option>
						<option value="12">12</option>
						<option value="14">14</option>
						<option value="16">16</option>
						<option value="18">18</option>
						<option value="22">22</option>
				</select>
			</div>	
		
		</div>
      </div>
	  <div id="result_Wheeler"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="updateWheelerBtn" class="btn btn-primary">Update</button>
        <button type="button" id="" onclick="$('#WheelerForm')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#WheelerForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#updateWheelerBtn").attr("disabled", true);
	$.ajax({
        	url: "./save_update_wheeler.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_driverForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>