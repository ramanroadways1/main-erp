<?php
require_once("connection.php");

echo "<div id='load_lr_modal_create_lr'></div>"; 

if($branch=='KORBA1')
{
	echo "
	<script>
		jQuery.ajax({
		url: './_lr_modal.php',
		data: 'ok=' + 'OK',
		type: 'POST',
		success: function(data) {
			$('#load_lr_modal_create_lr').html(data);
		},
		error: function() {}
	});
	</script>";
}
else
{
if(isset($_SESSION['verify_vehicle']) AND isset($_SESSION['verify_driver']))
{
	echo "
	<script>
		jQuery.ajax({
		url: './_lr_modal.php',
		data: 'ok=' + 'OK',
		type: 'POST',
		success: function(data) {
			$('#load_lr_modal_create_lr').html(data);
		},
		error: function() {}
	});
	</script>";
}
else
{
?>
<script>
$(function() {
		$("#veh_no_verify_own").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#veh_no_verify_own').val(ui.item.value);   
            $('#veh_no_verify_own_id').val(ui.item.id);      
            $('#veh_no_verify_own_wheeler').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#veh_no_verify_own').val("");   
			$('#veh_no_verify_own_id').val("");   
			$('#veh_no_verify_own_wheeler').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#veh_no_verify_market").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#veh_no_verify_market').val(ui.item.value);   
            $('#veh_no_verify_market_id').val(ui.item.oid);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#veh_no_verify_market').val("");   
			$('#veh_no_verify_market_id').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#d_lic_no").autocomplete({
		source: 'autofill/lic_no.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#d_lic_no').val(ui.item.value);   
            $('#driver_license_id').val(ui.item.id);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#d_lic_no').val("");   
			$('#driver_license_id').val("");   
			alert('Driver does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div id="VerifyVehicle" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Create NEW LR : Step 1
      </div>
   
   <div class="modal-body">
        <div class="row">
			
			<?php
			/*
			<option <?php if(isset($_SESSION['verify_vehicle_type'])) { if($_SESSION['verify_vehicle_type']=='MARKET') { echo "selected"; } else {echo "disabled"; }} ?> class="veh_sel_dropdown" id="VEH_MARKET" value="MARKET">Market vehicle</option>
			*/
			?>
			<div class="form-group col-md-8">
				<label>Vehicle type: <font color="red">*</font></label>
				<select onchange="VehTypeSet(this.value)" id="verify_vehicle_type" name="verify_vehicle_type" required="required" class="form-control">	
					<option <?php if(isset($_SESSION['verify_vehicle_type'])) { echo "disabled"; } ?> class="veh_sel_dropdown" value="">select an option</option>
					<option <?php if(isset($_SESSION['verify_vehicle_type'])) { if($_SESSION['verify_vehicle_type']=='OWN') { echo "selected"; } else { echo "disabled"; }} ?> class="veh_sel_dropdown" id="VEH_OWN" value="OWN">Own vehicle</option>
				</select>
			</div>
			
			<script>
			function VehTypeSet(elem)
			{
				if(elem=='OWN'){
					$('#veh_no_div_own').show();
					$('#veh_no_div_market').hide();
				}
				else{
					$('#veh_no_div_own').hide();
					$('#veh_no_div_market').show();
				}
			}
			</script>
			
			<div class="form-group col-md-8 col-xs-8" id="veh_no_div_own">
				<label>Enter Vehicle Number <font color="red"><sup>*</font></sup></label>
				<input type="text" <?php if(isset($_SESSION['verify_vehicle'])) { echo "readonly='readonly' value='$_SESSION[verify_vehicle]'"; } ?> autocomplete="off" 
				oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="veh_no_verify_own" name="veh_no_verify" 
				class="form-control" required="required">
			</div>
			
			<input type="hidden" <?php if(isset($_SESSION['verify_vehicle'])) { echo "value='$_SESSION[verify_vehicle_wheeler]'"; } ?> id="veh_no_verify_own_wheeler">
			<input type="hidden" <?php if(isset($_SESSION['verify_vehicle'])) { echo "value='$_SESSION[verify_vehicle_id]'"; } ?> id="veh_no_verify_own_id">
			
		<div id="veh_no_div_market">	
		
			<div class="form-group col-md-8 col-xs-8">
				<label>Enter Vehicle Number <font color="red"><sup>*</font></sup></label>
				<input type="text" <?php if(isset($_SESSION['verify_vehicle'])) { echo "readonly='readonly' value='$_SESSION[verify_vehicle]'"; } ?> autocomplete="off" 
				oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="veh_no_verify_market" name="veh_no_verify" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4 col-xs-4" style="display:none" id="verify_rc_div">
				<br />
				<img style="margin-top:3px;width:30px" src="verified.png" />
			</div>
			
			<?php
			if(isset($_SESSION['verify_vehicle']))
			{
			?>
			<div class="form-group col-md-4 col-xs-4">
				<br />
				<img style="margin-top:3px;width:30px" src="verified.png" />
			</div>
			<?php			
			}
			else
			{
			?>
			<div class="form-group col-md-4 col-xs-4" id="verify_btn_div_rc">
				<label>&nbsp;</label>
				<br />
				<button type="button" onclick="GetRC()" class="btn btn-sm btn-success">Check RC</button>
			</div>
			<?php			
			}
			?>
			
			<div class="form-group col-md-8 col-xs-8">
				<label>Enter License Number <font color="red"><sup>*</font> </sup></label>
				<input <?php if(isset($_SESSION['verify_driver'])) { echo "readonly='readonly' value='$_SESSION[verify_driver]'"; } ?> type="text" autocomplete="off" 
				oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-]/,'')" <?php if($branch=='PEN') { echo "readonly='readonly'"; } ?> id="d_lic_no" name="d_lic_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4 col-xs-4" style="display:none" id="verify_dl_div">
				<br />
				<img style="margin-top:3px;width:30px" src="verified.png" />
			</div>
			
			<?php
			if(isset($_SESSION['verify_driver']))
			{
			?>
			<div class="form-group col-md-4 col-xs-4">
				<br />
				<img style="margin-top:3px;width:30px" src="verified.png" />
			</div>
			<?php			
			}
			else
			{
			?>
			<div class="form-group col-md-4 col-xs-4"  id="verify_btn_div_dl">
				<label>&nbsp;</label>
				<br />
				<button type="button" <?php if($branch=='PEN') { echo "disabled='disabled'"; } ?> onclick="GetDL()" class="btn btn-sm btn-success">Check DL</button>
			</div>
			<?php			
			}
			?>
			
			
			<input type="hidden" id="is_rc_verified" name="is_rc_verified">
			<input type="hidden" id="is_dl_verified" name="is_dl_verified">
			<input type="hidden" id="veh_no_verify_market_id" name="veh_no_verify_market_id">
			<input type="hidden" id="driver_license_id" name="driver_license_id">
			
			<div class="col-md-12" style="color:red" id="result_VerifyVehicle"></div>
			  
		</div>
	</div>
 </div>
	
      <div class="modal-footer">
	  <button type="button" onclick="ResetVehicle()" class="btn btn-sm btn-danger pull-left">Reset Vehicle</button>
	  
        <button type="button" onclick="VerifyVehicle()" id="verify_button1" class="btn btn-sm btn-primary">Next step</button>
        <button type="button" onclick="window.location.href='_fetch_lr_entry.php'" class="btn btn-sm btn-default">Close</button>
		
		 <button type="button" id="close_verify_modal" data-dismiss="modal" style="display:none">Close</button>
	 </div>
    </div>

  </div>
</div>

<script>
function GetRC()
{
	var tno_id = $('#veh_no_verify_market_id').val();
	var tno = $('#veh_no_verify_market').val();
	
	if(tno=='')
	{
		alert('Enter vehicle number first !!');
	}
	else
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "./load_rc_data.php",
			data: 'tno_id=' + tno_id + '&tno=' + tno,
			type: "POST",
			success: function(data) {
			$("#modal_load_data").html(data);
			},
			error: function() {}
		});
	}
}

function GetDL()
{
	var lic_id = $('#driver_license_id').val();
	
	if(lic_id=='')
	{
		alert('Enter license number first !!');
	}
	else
	{
		$("#loadicon").show();
			jQuery.ajax({
			url: "./load_dl_data.php",
			data: 'lic_id=' + lic_id,
			type: "POST",
			success: function(data) {
			$("#modal_load_data").html(data);
			},
			error: function() {}
		});
	}
}

function VerifyVehicle()
{
	var veh_type = $('#verify_vehicle_type').val();
	
	if(veh_type=='')
	{
		Swal.fire({
			icon: 'error',
			title: 'Warning',
			text: 'select vehicle type first !'
		})
	}
	else if(veh_type=='OWN')
	{
		var tno = $('#veh_no_verify_own').val();
		var wheeler = $('#veh_no_verify_own_wheeler').val();
		var id = $('#veh_no_verify_own_id').val();
		
		if(tno=='')
		{
			Swal.fire({
			icon: 'error',
			title: 'Warning',
			text: 'Please enter own vehicle number !'
			})
		}
		else
		{
			$("#loadicon").show();
				jQuery.ajax({
						url: "load_lr_modal.php",
						data: 'veh_type=' + veh_type + '&tno=' + tno + '&wheeler=' + wheeler + '&id=' + id,
						type: "POST",
						success: function(data) {
						$("#load_lr_modal_create_lr").html(data);
					},
					error: function() {}
				});
		}
	}
	else
	{
		var rc = $('#is_rc_verified').val();
		var dl = $('#is_dl_verified').val();
		
		if(rc!='1')
		{
			Swal.fire({
				icon: 'error',
				title: 'Warning',
				text: 'RC not verified !'
				})
		}
		else
		{
			if(dl!='1')
			{
				Swal.fire({
				icon: 'error',
				title: 'Warning',
				text: 'DL not verified !'
				})
			}
			else
			{
				$("#loadicon").show();
				jQuery.ajax({
						url: "load_lr_modal.php",
						data: 'veh_type=' + veh_type,
						type: "POST",
						success: function(data) {
						$("#load_lr_modal_create_lr").html(data);
					},
					error: function() {}
				});
			}
		}
	}
}
</script>

<button id="button_verify_1" type="button" data-toggle="modal" data-target="#VerifyVehicle" style="display:none"></button>

<script>
	$('#button_verify_1')[0].click();
	$('#lr_create_btn').attr('disabled',true);
	$('#loadicon').hide();
</script>

	<?php
	if(isset($_SESSION['verify_vehicle_type']))
	{
		$vehicle_session = $_SESSION["verify_vehicle_type"];
		?>
		<script>
			VehTypeSet('<?php echo $vehicle_session; ?>');
		</script>
		<?php
	}
	else
	{
	?>
		<script>
			VehTypeSet('');
		</script>
	<?php
	}
}
}
?>