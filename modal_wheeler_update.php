<form id="UpdateWheelerForm" action="#" method="POST">
<div id="UpdateWheelerModal" style="background:#DDD" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
			<div id="update_wheeler_result"></div>
      <div class="modal-header bg-primary">
		Update Wheeler : <?php echo $_SESSION['freight_memo_vehicle']; ?>
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Vehicle Number.<font color="red"><sup>*</sup></font></label>
				<input type="text" name="tno" value="<?php echo $_SESSION['freight_memo_vehicle']; ?>" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Enter LR No.<font color="red"><sup>*</sup></font></label>
				<select name="wheeler" required="required" class="form-control">
					<option value="">Select an option</option>
					<?php
					$get_wheelerDb = Qry($conn,"SELECT wheeler FROM weight_lock_wheeler");
					if(numRows($get_wheelerDb)>0)
					{
						while($rowW_Db = fetchArray($get_wheelerDb))
						{
							echo "<option value='$rowW_Db[wheeler]'>$rowW_Db[wheeler]</option>";
						}
					}
					?>
				</select>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update Wheeler</button>
      </div>
    </div>

  </div>
</div>
</form>

<button style="display:none" type="button" id="open_update_wheeler" data-toggle="modal" data-target="#UpdateWheelerModal"></button>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateWheelerForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
		$.ajax({
        	url: "./update_wheeler.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#update_wheeler_result").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>