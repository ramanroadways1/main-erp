<?php
require_once("./connection.php");

// $check_lr_age = Qry($conn,"SELECT id FROM lr_sample_pending WHERE date(timestamp)>'2020-07-05' AND timestamp < DATE_SUB(NOW(), INTERVAL 10 DAY) 
// AND branch='$branch'");

// 1 mahine purane lr visible nahi rkhni h instruction by Rishab sir on 23-02-21 02:45 PM 

$check_lr_age = Qry($conn,"SELECT id FROM lr_sample_pending WHERE date(timestamp)>'2021-01-22' AND timestamp < DATE_SUB(NOW(), INTERVAL 10 DAY) 
AND branch='$branch'");

if(!$check_lr_age){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	$('#LRallInputs *').prop('disabled', true);
	$('#LRallInputs').hide();
</script>";

$chk_lr_lock_access = Qry($conn,"SELECT remind_on FROM lr_lock_extend WHERE branch='$branch' ORDER BY id DESC LIMIT 1");
if(!$chk_lr_lock_access){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_lr_lock_access)==0)
{
	$lr_lock_active="1";
}
else 
{
	$row_lr_lock = fetchArray($chk_lr_lock_access);	
	
	if(strtotime(date("Y-m-d"))>=strtotime($row_lr_lock['remind_on']))
	{
		$lr_lock_active="1";
	}
	else
	{
		$lr_lock_active="0";
	}
}

	
if(numRows($check_lr_age)>0 AND $lr_lock_active=="1"){
	echo "<script>
		$('#lr_error_text').show();
		$('#lr_create_btn').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
}
else{
	echo "<script>
		$('#lr_error_text').hide();
		$('#lr_create_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
}
?>