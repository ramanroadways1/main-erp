<form id="DrC2CForm" action="#" method="POST">
<div id="DrITRModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Company to Company Transfer (ITR)
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Credit to Company (+) <font color="red">*</font></label>
				<select onchange="ChkCompanyDR(this.value)" class="form-control" name="to_company" required="required">
					<option value="">--Select Company--</option>
					<option value="RRPL">RRPL</option>
					<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<script>
			function ChkCompanyDR(comp)
			{
				if(comp=='RRPL'){
					$('#debit_from_comp1').val('RAMAN_ROADWAYS');
				}
				else if(comp=='RAMAN_ROADWAYS'){
					$('#debit_from_comp1').val('RRPL');
				}
				else{
					$('#debit_from_comp1').val('');
				}
			}
			</script>
			
			<div class="form-group col-md-6">
				<label>Debit From Company (-) <font color="red">*</font></label>
				<input type="text" class="form-control" id="debit_from_comp1" name="from_company" required readonly />
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red">*</font></label>
				<input type="number" name="amount" min="1" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-8">
				<label>Narration <font color="red">*</font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'');" name="narration" class="form-control" required="required"></textarea>	
			</div>
			
		</div>
      </div>
	  
		<div id="result_c2c_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="dr_c2c_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	</div>
</div>
</form>	

<script type="text/javascript">
$(document).ready(function (e) {
	$("#DrC2CForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#dr_c2c_button").attr("disabled", true);
	$.ajax({
        	url: "./save_dr_comp_to_comp.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_c2c_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>