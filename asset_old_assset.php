<script>		
function GstInvoiceOldAsset(elem){
	
	$('#btn_old_asset_add').attr('disabled',true);
	$('#gst_validate_btn').attr('disabled',true);
	
	$('#amount_old_assset').val('');
	$('#gst_no_1').val('');
	$('#gst_type_old_asset').val('');
	$('#gst_value_old_asset').val('');
	$('#gst_amount_old_asset').val('');
	$('#discount_1').val('');
	$('#total_amount_old_asset').val('');
	
	if(elem=='YES')
	{
		$('#gst_no_1').attr('readonly',false);
		$('#gst_value_old_asset').attr('readonly',false);
		$('#gst_validate_btn').attr('disabled',false);
	}
	else
	{
		$('#btn_old_asset_add').attr('disabled',false);
		$('#gst_validate_btn').attr('disabled',true);
		$('#gst_no_1').attr('readonly',true);
		$('#gst_value_old_asset').attr('readonly',true);
		$('#gst_value_old_asset').val('0');
		$('#gst_amount_old_asset').val('0');
	}
}
</script>

<form id="FormAddOldAsset" style="font-size:12.5px" action="#" method="POST">
<div id="AddOldAsset" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add Old Asset : ( पुराने एसेट जोड़ने के लिए उपयोग करे | )
      </div>
      <div class="modal-body">
        <div class="row">
		
				<div class="form-group col-md-4">
					<label>Asset Catagory <font color="red"><sup>*</sup></font></label>
					<select class="form-control" required="required" name="asset_cat">
						<option value="">--Select option--</option>
						<?php
						$Getasset_Cat1 = Qry($conn,"SELECT id,title FROM asset_category ORDER BY title ASC");
						if(numRows($Getasset_Cat1)>0)
						{
							while($rowCat_1 = fetchArray($Getasset_Cat1))
							{
								echo "<option value='$rowCat_1[id]'>$rowCat_1[title]</option>";
							}
						}
						?>
					</select>
				</div>
				
				<div class="form-group col-md-4">
					<label>Maker Name <font color="red"><sup>* </sup></font></label>
					<textarea name="maker_name" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required"></textarea>
				</div>
				
				<div class="form-group col-md-4">
					<label>Model <font color="red"><sup>* </sup></font></label>
					<textarea name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9/-]/,'')" class="form-control" required="required"></textarea>
				</div>
				
				<div class="form-group col-md-4">
					<label>Invoice Date <font color="red"><sup>*</sup></font></label>
					<input name="invoice_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<div class="form-group col-md-4">
					<label>Invoice Copy <font color="red"><sup>* (image/pdf)</sup></font></label>
					<input type="file" accept=".jpg,.jpeg,.png,.pdf" name="invoice_copy" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>GST Invoice ? <font color="red"><sup>*</sup></font></label>
					<select class="form-control" onchange="GstInvoiceOldAsset(this.value)" required="required" id="gst_selectionOld_asset" name="gst_selection">
						<option class="gst_sel_opt" value="">--Select option--</option>
						<option class="gst_sel_opt" id="gst_sel_opt_YES" value="YES">YES</option>
						<option class="gst_sel_opt" id="gst_sel_opt_NO" value="NO">NO</option>
					</select>	
				</div>
				
				<div class="form-group col-md-3">
					<label>Amount <font color="red"><sup>* (w/o GST)</sup></font></label>
					<input type="number" oninput="CalcGstValueOldAsset('amount_old_assset')" id="amount_old_assset" name="amount" min="1" class="form-control" required="required">
				</div>
				
<script>				
function CalcGstValueOldAsset(idname)
{
	var gst_selection = $('#gst_selectionOld_asset').val();
	
	if(gst_selection=='')
	{
		$('#'+idname).val('');
		$('#gst_selectionOld_asset').focus();
	}
	else
	{
		if(gst_selection=='NO')
		{
			if($('#amount_old_assset').val()==''){
				var amount = 0;
			}
			else{
				var amount = Number($('#amount_old_assset').val());
			}
			
			if($('#discount_1').val()==''){
				var discount =  0;
			}
			else{
				var discount =  Number($('#discount_1').val());
			}
			
			$('#gst_type_old_asset').val('');
			$('#gst_value_old_asset').val('0');
			$('#gst_amount_old_asset').val('0');
			
			$('#total_amount_old_asset').val((amount-discount).toFixed(2));
		}
		else
		{
			if($('#amount_old_assset').val()==''){
				var amount = 0;
			}
			else{
				var amount = Number($('#amount_old_assset').val());
			}
			
			if($('#gst_value_old_asset').val()==''){
				var gst_value =  0;
			}
			else{
				var gst_value =  Number($('#gst_value_old_asset').val());
			}
			
			if($('#discount_1').val()==''){
				var discount =  0;
			}
			else{
				var discount =  Number($('#discount_1').val());
			}
			
			var gst_amount = Number(amount*gst_value/100);
			$('#gst_amount_old_asset').val(gst_amount.toFixed(2))
			$('#total_amount_old_asset').val(Number((amount+gst_amount)-discount).toFixed(2));
		}
	}
}

function ValidateGstValidateFunc()
{
	var gst_no = $('#gst_no_1').val();
	var amount = $('#amount_old_assset').val();
	var gst_selection = $('#gst_selectionOld_asset').val();
	
	if(gst_no=='')
	{
		alert('Enter GST number first !');
		$('#gst_no_1').focus();
	}
	else if(amount=='')
	{
		alert('Enter amount first !');
		$('#amount_old_assset').focus();
	}
	else if(gst_selection=='')
	{
		alert('Select invoice type first !');
		$('#gst_selectionOld_asset').focus();
	}
	else
	{
		$('#gst_no_1').attr('readonly',true);
		$('#gst_validate_btn').attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "./asset_gst_checked_old_asset.php",
			data: 'gst_no=' + gst_no + '&gst_selection=' + gst_selection,
			type: "POST",
			success: function(data) {
			$("#result_old_asset_add").html(data);
		},
		error: function() {}
	});
	}
}
</script>				
				
				<div class="form-inline col-md-6">
					<label>GST Number <font color="red"><sup>*</sup></font></label>
					<br />
					<div class="form-group">
						<input style="width:300px !important" type="text" maxlength="15" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateGst(this.value)" name="gst_no" readonly id="gst_no_1" class="form-control" required="required">
					</div>
					<div class="form-group">
						<button type="button" id="gst_validate_btn" onclick="ValidateGstValidateFunc()" disabled class="btn btn-sm btn-primary">Validate</button>
					</div>
				</div>
					
				<div class="form-group col-md-3">
					<label>GST-Type <font color="red"><sup>*</sup></font></label>
					<input type="text" id="gst_type_old_asset" name="gst_type" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>GST Value <font color="red"><sup>* in (%)</sup></font></label>
					<input step="any" oninput="CalcGstValueOldAsset('gst_value_old_asset')" readonly type="number" name="gst_value" id="gst_value_old_asset" min="1" max="30" class="form-control" required="required">
				</div>

<script>			
// function GetGstAmountOldAsset(gst_per){
	
	// var gst_selection = $('#gst_selectionOld_asset').val();
	
	// if(gst_selection=='')
	// {	
		// $('#gst_value_old_asset').val('');
		// $('#gst_selectionOld_asset').focus();
	// }
	// else
	// {
		// if(gst_selection=='NO')
		// {
			// $('#total_amount_old_asset').val(Number((amount+gst_amount)-discount).toFixed(2));
		// }
		// else
		// {
			
		// }
		// var amount = Number($('#amount_old_assset').val());
		// var discount = Number($('#discount_1').val());
		// var gst_amount = Number(amount*gst_per/100);
		// $('#gst_amount_old_asset').val(gst_amount.toFixed(2))
		// $('#total_amount_old_asset').val(Number((amount+gst_amount)-discount).toFixed(2));
	// }
// }

function DiscountFunc(discount){
	
	var amount = $('#amount_old_assset').val();
	
	if(amount=='')
	{
		$('#discount_1').val('');
		$('#amount_old_assset').focus();
	}
	else
	{
		if($('#gst_selectionOld_asset').val()=='NO')
		{
			$('.gst_sel_opt').attr('disabled',true);
			$('#gst_sel_opt_NO').attr('disabled',false);
		}
	}
}
</script>			
			
				<div class="form-group col-md-3">
					<label>GST Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" step="any" id="gst_amount_old_asset" name="gst_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Discount <font color="red"><sup>*</sup></font></label>
					<input oninput="CalcGstValueOldAsset('discount_1');DiscountFunc(this.value)" step="any" type="number" id="discount_1" name="discount" min="0" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Total Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="total_amount_old_asset" name="total_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Asset For <font color="red"><sup>*</sup></font></label>
					<select name="asset_for" class="form-control" onchange="AssetForUse(this.value)" required="required">
						<option value="">--option--</option>
						<option value="GENERAL">General (for branch)</option>
						<option value="SPECIFIC">Specific (for person)</option>
					</select>
				</div>
				
				<script>
				function AssetForUse(elem)
				{
					if(elem=='SPECIFIC'){
						$('#spec_div_asset').show();
						$('#emp_name_for_asset').attr('required',true);
					}
					else{
						$('#spec_div_asset').hide();
						$('#emp_name_for_asset').attr('required',false);
					}
				}
				</script>
				
				<div class="form-group col-md-4" id="spec_div_asset" style="display:none">
					<label>Select Employee <font color="red"><sup>*</sup></font></label>
					<select name="employee" class="form-control" id="emp_name_for_asset" required="required">
						<option value="">--option--</option>
						<?php
						$getEmp_For_old_asset = Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3'");
						if(numRows($getEmp_For_old_asset)>0)
						{
							while($rowEmpOldAsset = fetchArray($getEmp_For_old_asset))
							{
								echo "<option value='$rowEmpOldAsset[code]'>$rowEmpOldAsset[name] ($rowEmpOldAsset[code])</option>";
							}
						}
						?>
					</select>
				</div>
				
		</div>
      </div>
	  <div id="result_old_asset_add"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="btn_old_asset_add" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormAddOldAsset").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_old_asset_add").attr("disabled", true);
	$.ajax({
        	url: "./save_old_asset_add.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data){
				$("#result_old_asset_add").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script type="text/javascript">
function ValidateGst() {
	
	if($('#gst_selectionOld_asset').val()=='')
	{	
		$('#gst_no_1').val('');
		$('#gst_selectionOld_asset').focus();
	}
	else
	{
		var Obj = document.getElementById("gst_no_1");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("gst_no_1").setAttribute("style","background-color:red;color:#fff;width:300px !important");
				// document.getElementById("btn_old_asset_add").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("gst_no_1").setAttribute("style","background-color:green;color:#fff;width:300px !important");
				// document.getElementById("btn_old_asset_add").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("gst_no_1").setAttribute("style","background-color:white;color:#fff;width:300px !important");
			// document.getElementById("btn_old_asset_add").removeAttribute("disabled");
		}
	}
 } 			
</script>