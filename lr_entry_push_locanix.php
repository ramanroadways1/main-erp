<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$lrno = escapeString($conn,strtoupper($_POST['lrno']));

if($branch!='KORBA')
{
	echo "<script>
		alert('LR : $lrno. Created Successfully !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";	
	exit();
}
else
{
$get_lr_details = Qry($conn,"SELECT lrno,branch,consignor,consignee,con1_id,con2_id,con1_gst,con2_gst,lr_type,date,fstation,
tstation,invno,wt12,weight,truck_no,wheeler,t_type FROM lr_sample_pending WHERE id='$id'");

if(!$get_lr_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('LR : $lrno. Created Successfully !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_lr = fetchArray($get_lr_details);

$consignee_data = Qry($conn,"SELECT pincode FROM consignee WHERE id='$row_lr[con2_id]'");

if(!$consignee_data){
	errorLog(getMySQLError($conn)."LR No : $lrno.",$conn,$page_name,__LINE__);
	echo "<script>
		alert('LR : $lrno. Created Successfully !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_con2 = fetchArray($consignee_data);

$lrno=escapeString($conn,strtoupper($row_lr['lrno']));
$consignor=escapeString($conn,strtoupper($row_lr['consignor']));
$consignor_id=escapeString($conn,strtoupper($row_lr['con1_id']));
$con1_gst=escapeString($conn,strtoupper($row_lr['con1_gst']));
$lr_type=escapeString($conn,strtoupper($row_lr['lr_type']));
$lr_date=escapeString($conn,strtoupper($row_lr['date']));
$from=escapeString($conn,strtoupper($row_lr['fstation']));
$to=escapeString($conn,strtoupper($row_lr['tstation']));
$consignee=escapeString($conn,strtoupper($row_lr['consignee']));
$con2_gst=escapeString($conn,strtoupper($row_lr['con2_gst']));
$consignee_id=escapeString($conn,strtoupper($row_lr['con2_id']));
$invno=escapeString($conn,strtoupper(trim($row_lr['invno'])));
$actual_wt=escapeString($conn,strtoupper($row_lr['wt12']));
$charge_wt=escapeString($conn,strtoupper($row_lr['weight']));
$consignee_pincode=escapeString($conn,strtoupper($row_con2['pincode']));
$tno=escapeString($conn,strtoupper($row_lr['truck_no']));
$wheeler=escapeString($conn,strtoupper($row_lr['wheeler']));
$t_type=escapeString($conn,strtoupper($row_lr['t_type']));

if($lr_type=='MARKET')
{
	$fetch_driver_mobile = Qry($conn,"SELECT driver_mobile as mobile FROM mis WHERE truck_no='$tno' ORDER BY id DESC LIMIT 1");
}
else
{
	$fetch_driver_mobile = Qry($conn,"SELECT mobile as mobile FROM dairy.driver WHERE code=(SELECT driver_code FROM dairy.own_truck 
	WHERE tno='$tno')");
}

if(!$fetch_driver_mobile){
	errorLog(getMySQLError($conn)."LR No : $lrno.",$conn,$page_name,__LINE__);
	echo "<script>
		alert('LR : $lrno. Created Successfully !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$row_mobile = fetchArray($fetch_driver_mobile);

$driver_mobile = $row_mobile['mobile'];

$data_set = "LR_No: $lrno, Branch: $branch, TruckNo: $tno, LRDate: $lr_date, ActWt: $actual_wt, ChargeWt: $charge_wt, 
From: $from, ToLoc: $to, Consignor: $consignor, Consignee: $consignee, Wheeler: $wheeler, TruckType: $t_type, driver_mobile: $driver_mobile.";
   
$tab_id = getInsertID($conn);
   
   $data = array(
	"branch"=>"$branch",
	"truck_no"=>"$tno",
	"date"=>"$lr_date",
	"act_wt"=>"$actual_wt",
	"chrg_wt"=>"$charge_wt",
	"from_loc"=>"$from",
	"to_loc"=>"$to",
	"consignor"=>"$consignor",
	"consignee"=>"$consignee",
	"lrno"=>"$lrno",
	"truck_type"=>"$lr_type",
	"wheeler"=>"$wheeler",
	"invoice_no"=>"$invno",
	"pincode"=>"$consignee_pincode",
	"driver_mobile"=>"$driver_mobile"
	);
	$payload = json_encode($data);
	
	$url="http://apps.locanix.net/balcoapp/balco/LRDestinationReceiver"; // Locanix Url
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($payload)));
		
	$result = curl_exec($ch);
	
	if(curl_errno($ch))
	{
		$error_msg = curl_error($ch);
	}
	
	curl_close($ch);

	if(isset($error_msg))
	{
		errorLog("LR Api Error: $error_msg. LR_No: $lrno.",$conn,$page_name,__LINE__);
		
		$msg_err="RRPL- LR Api Error: $error_msg. VehicleNumber: $tno, LRNo: $lrno, Consignor: $consignor, Consignee: $consignee, Destination: $to.";
		exec("wget http://apps.locanix.net/Telegram/@locanixbot/-397449320/SendMessage?message='".$msg_err."'");
		
		echo "<script>
			alert('LR : $lrno. Created Successfully !');
			window.location.href='./_fetch_lr_entry.php';
		</script>";
		exit();
	}

	$result = json_decode($result, true);
	$status=$result['status'];
	$response=$result['response'];
	
	if($status!='ok')
	{
		$msg_err="RRPL- LR Api Error: $response. VehicleNumber: $tno, LRNo: $lrno, Consignor: $consignor, Consignee: $consignee, Destination: $to.";
		exec("wget http://apps.locanix.net/Telegram/@locanixbot/-397449320/SendMessage?message='".$msg_err."'");
	}
	else
	{
		// $update_api_call = Qry($conn,"UPDATE gps_push_lr_start SET api_call='1',api_timestamp='$timestamp' WHERE api_call='0' AND 
		// tno='$tno'");
		
			// if(!$update_api_call){
				// $flag = false;
				// errorLog(getMySQLError($conn)."LR No : $lrno.",$conn,$page_name,__LINE__);
			// }

		$msg="RRPL- New LR Created. VehicleNumber: $tno, LRNo: $lrno, Consignor: $consignor, Consignee: $consignee, Destination: $to.";
		exec("wget http://apps.locanix.net/Telegram/@locanixbot/-397449320/SendMessage?message='".$msg."'");
	}
	
StartCommit($conn);
$flag = true;
 
$insert_Log = Qry($conn,"INSERT INTO gps_push_lr_log(veh_no,data,lrno,timestamp) VALUES ('$tno','$data_set','$lrno','$timestamp')");

if(!$insert_Log){
	$flag = false;
	errorLog(getMySQLError($conn)."LR No : $lrno.",$conn,$page_name,__LINE__);
}
	
	$insert_Log_api = Qry($conn,"INSERT INTO gps_push_api_log(lrno,tab_id,status,response,json,timestamp) VALUES 
	('$lrno','$tab_id','$status','$response','$payload','$timestamp')");
	
	if(!$insert_Log_api){
		$flag = false;
		errorLog(getMySQLError($conn)."LR No : $lrno.",$conn,$page_name,__LINE__);
	}

	$api_log_insert_id = getInsertID($conn);
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
}

	echo "<script>
		alert('LR : $lrno. Created Successfully !');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
}
?>