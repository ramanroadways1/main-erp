<form id="ChangeFormOwner" action="#" method="POST">
<div id="OwnerChangeModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Owner Change Request
      </div>
      <div class="modal-body">
        <div class="row">
		
<script type="text/javascript">
	$(function() {
		$("#oc_tno").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		select: function (event, ui) { 
               $('#oc_tno').val(ui.item.value);   
              return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#oc_tno").val('');
			$("#oc_tno").focus();
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 
		
			<div class="form-group col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
						<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" name="tno" id="oc_tno" class="form-control" required="required">
					</div>
					<div class="form-group col-md-4">
						<label>&nbsp;</label>
						<br>
						<button type="button" id="Fetch_Owner_Btn" onclick="FetchOwner()" class="btn btn-danger btn-sm">Fetch Record</button>
					</div>
				</div>
			</div>
			
			<div id="ChangeDiv" style="display:none">
			
			<div class="form-group col-md-12 bg-primary">
				Current details :
			</div>
			
			<div class="form-group col-md-4">
				<label>Owner's Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="oc_name" readonly oninput="this.value=this.value.replace(/[^A-Z a-z-]/,'')" name="old_name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN No. <font color="red"><sup>*</sup></font></label>
				<input type="text" id="oc_pan_no" maxlength="10" readonly oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" name="old_pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="oc_mobile_no" readonly maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="old_mobile" class="form-control" required="required">
			</div>
			
			<!--
			<div class="form-group col-md-6">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea readonly oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:]/,'')" id="oc_addr" name="old_addr" class="form-control" required="required"></textarea>
			</div>-->
			
			<div class="form-group col-md-6">
				<label>&nbsp;</label>
				<br />
				<button onclick="ViewUpload('rc')" class="btn btn-sm btn-primary" type="button">Rc Front</button>
				<button onclick="ViewUpload('rc_rear')" class="btn btn-sm btn-primary" type="button">Rc Rear</button>
				<button onclick="ViewUpload('pan')" class="btn btn-sm btn-primary" type="button">PAN Card</button>
				<button onclick="ViewUpload('dec')" class="btn btn-sm btn-primary" type="button">Declaration</button>
			</div>
		
			<input type="hidden" id="old_rc" name="old_rc">
			<input type="hidden" id="old_rc_rear" name="old_rc_rear">
			<input type="hidden" id="old_pan" name="old_pan">
			<input type="hidden" id="old_dec" name="old_dec">
			
			<input type="hidden" id="old_rc_path">
			<input type="hidden" id="old_rc_rear_path">
			<input type="hidden" id="old_pan_path">
			<input type="hidden" id="old_dec_path">
			
			<input type="hidden" name="owner_id" id="owner_id">
			
			<div class="form-group col-md-12 bg-primary">
				New details to be updated :
			</div>
			
			<div class="form-group col-md-4">
				<label>Owner's Name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Z a-z-]/,'')" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN No. <font color="red"><sup>*</sup></font></label>
				<input type="text" id="pan_no_new" maxlength="10" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ValidatePanOwner(this.value)" name="pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">	
				<label>RC Copy (front) <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="rc_copy_f" class="form-control" required>
			</div>
		
			<div class="form-group col-md-4">	
				<label>RC Copy (rear) <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="rc_copy_r" class="form-control" required>
			</div>
			
			<div class="form-group col-md-4">	
				<label>PAN Card Copy <font color="red">*</font></label>
				<input type="file" accept=".png, .jpg, .jpeg, .pdf" name="pan_copy" class="form-control" required>
			</div>
				
			<div class="form-group col-md-12">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:]/,'')" name="addr" class="form-control" required="required">
			</div>
		</div>
		
		<div id="Prev_req" style="display:none1">
			<div class="form-group col-md-12 bg-primary">
				Pending Requests :
			</div>
			
			<div class="form-group col-md-12">
				<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr>
					<th>Id</th>
					<th>Vehicle_No</th>
					<th>Name</th>
					<th>Mobile</th>
					<th>PAN</th>
					<th>Address</th>
					<th>DateTime</th>
				</tr>
				
				<?php
				$qryPending_Req = Qry($conn,"SELECT tno,new_name,new_mobile,new_pan,new_addr,timestamp FROM owner_change_req WHERE done=0 AND branch='$branch'");
				
				if(numRows($qryPending_Req)==0)
				{
					echo "<tr>
						<td colspan='7'>No pending request found !!</td>
					</tr>";
				}
				else
				{
					$sn=1;
					while($rowPending_R = fetchArray($qryPending_Req))
					{
						echo "<tr>
							<td>$sn</td>
							<td>$rowPending_R[tno]</td>
							<td>$rowPending_R[new_name]</td>
							<td>$rowPending_R[new_mobile]</td>
							<td>$rowPending_R[new_pan]</td>
							<td>$rowPending_R[new_addr]</td>
							<td>$rowPending_R[timestamp]</td>
						</tr>";
						$sn++;
					}
				}
				?>
			</table>
			</div>
			
		</div>
			
		</div>
			
      </div>
	  <div id="result_OwnerForm"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="button_oc_change" class="btn btn-success">Submit</button>
        <button type="button" id="oc_change_modal_close" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

<script>
function ViewUpload(type)
{
	if(type=='rc')
	{
		var ext = $('#old_rc_path').val();
		var copy = $('#old_rc').val();
	}
	else if(type=='rc_rear')
	{
		var ext = $('#old_rc_rear_path').val();
		var copy = $('#old_rc_rear').val();
	}
	else if(type=='pan')
	{
		var ext = $('#old_pan_path').val();
		var copy = $('#old_pan').val();
	}
	else if(type=='dec')
	{
		var ext = $('#old_dec_path').val();
		var copy = $('#old_dec').val();
	}
	else
	{
		alert('Invalid Document Type !');
	}
	
	var name = $('#oc_name').val();
	var pan = $('#oc_pan_no').val();
	var mobile = $('#oc_mobile_no').val();
		
		
		if(ext=='')
		{
			alert('FILE NOT FOUND !');
		}
		else
		{
			if(ext=='pdf')
			{
				$('#img_path').attr('href',copy);
				document.getElementById("img_path").click();
			}
			else
			{
				$('.imagepreview').attr('src',copy);
				$('#name_box').html(name);
				$('#pan_box').html(pan);
				$('#FullLink').attr('href',copy);
				$('#btnmodal1').click();  
			}
		}
}
 
function FetchOwner()
{
	var tno = $('#oc_tno').val();
	if(tno=='')
	{
		alert('Enter Vehicle Number First !');
		$('#oc_tno').focus();
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./owner_fetch_for_change.php",
		data: 'tno=' + tno,
		type: "POST",
		success: function(data) {
			$("#result_OwnerForm").html(data);
		},
		error: function() {}
		});
	}	
}
</script>
				
<button style="display:none" id="btnmodal1" data-toggle="modal" data-target="#imagemodal"></button>
 
<script type="text/javascript">
$(document).ready(function (e) {
	$("#ChangeFormOwner").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button_oc_change").attr("disabled", true);
	$.ajax({
        	url: "./owner_change_req_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_OwnerForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">              
        <div class="modal-body" style="overflow:auto">
          <label style="color:blue"><b>Name : </b><span id="name_box"></span>, <b>PAN No :</b> <span id="pan_box"></span></label>
		  <img src="" class="imagepreview" style="width: 100%;" >
        </div> 
		
		<div class="modal-footer">
		<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>	
		<a target="_blank" id="FullLink">
			<button type="button" class="btn btn-primary pull-left">View FULL Size</button>
		</a>
        </div> 
	</div>
 </div>
 </div>

<script>
function ValidatePanOwner(pan)
{  
  var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
  var PanNo = document.getElementById("pan_no_new").value;
  
  if(PanNo!="")
  {
	  if (PanNo.search(panPat) == -1)
	  {
         document.getElementById("pan_no_new").setAttribute("style","background-color:red;color:#fff;");
         document.getElementById("button_oc_change").setAttribute("disabled",true);
        return false;
      }
      else
      {
         document.getElementById("pan_no_new").setAttribute("style","background-color:green;color:#fff;");
		 document.getElementById("button_oc_change").removeAttribute("disabled");
      }
  }
  else
  {
	document.getElementById("pan_no_new").setAttribute("style","background-color:white;color:#fff");
	document.getElementById("button_oc_change").removeAttribute("disabled");
  }
} 			
</script>