<?php
require_once 'connection.php';

if(!isset($_REQUEST['id'])){
	echo "<script>
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

$id = escapeString($conn,strtoupper($_REQUEST['id']));

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$fetch_emp_data = Qry($conn,"SELECT * FROM emp_attendance WHERE id='$id'");
if(!$fetch_emp_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($fetch_emp_data)==0)
{
	echo "<script>
		alert('Employee not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}
	
$row = fetchArray($fetch_emp_data);	
	
if($row['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}	

// if($branch=='GOPALPUR' AND $_SESSION['user_code']=='200')
// {
	
// }
// else
// {
	// if($_SESSION['user_code']!='032' AND $row['code']!=$_SESSION['user_code'])
	// {
		// echo "<script>
			// alert('Only employee him/her self can edit their information !');
			// window.location.href='./employee_view.php';
		// </script>";
		// exit();
	// }	
// }	

if($row['join_date']==0){
	$join_date = "NULL";
}
else{
	$join_date = convertDate("d-m-y",$row["join_date"]);
}

if($row['birth_date']==0){
	$birth_date = "NULL";
}
else{
	$birth_date = convertDate("d-m-y",$row["birth_date"]);
}

$date_16year_ago = date('Y-m-d', strtotime('-15 years', strtotime(date("Y-m-d"))));
$min_date_join = date('Y-m-d', strtotime('-10 days', strtotime(date("Y-m-d"))));
$date_60_year_before = date('Y-m-d', strtotime('-45 years', strtotime($date_16year_ago)));
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
	include("./modal_emp_edit.php");
?>

<a href="./employee_view.php"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary">
<span class="glyphicon glyphicon-chevron-left"></span> Go Back </button></a>
<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp; Dashboard</button></a>
<button class="btn btn-sm btn-warning" style="margin-top:10px;margin-left:10px" id="emp_edit_modal_btn" data-toggle="modal" data-target="#ModalEmpEdit1"><span class="fa fa-pencil-square-o"></span> Edit</button>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<br />
<br />
<div class="container-fluid"> 

<div class="form-group col-md-12">
	
	<div class="row">	
		<div class="form-group col-md-12">
			<h4><span class="glyphicon glyphicon-user"></span> &nbsp; Employee Profile : <font color="blue"><?php echo $row['name']; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12">
			<div class="row">
				<div class="form-group col-md-4">
					<img style="width:160px;height:160px" src="<?php if($row['image']=='') { echo "user_default.png"; } else {echo $row['image']; } ?>">
				</div>
			</div>
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-striped" border="1" style="border:0;font-size:14px;">
				<tr>
					<td>Full Name: &nbsp; <?php echo $row['name']; ?></td>
					<td>Mobile No: &nbsp; <?php echo $row['mobile_no']; ?></td>
					<td>Date of Birth: &nbsp; <?php echo $birth_date; ?></td>
					<td>Joining Date: <?php echo $join_date; ?></td>
				</tr>
				
				<tr>
					<td>Code: &nbsp; <?php echo $row['code']; ?></td>
					<td>Father: &nbsp; <?php echo $row['father_name']; ?></td>
					<td>Emergency No: &nbsp; <?php echo $row['emergencyphone']; ?></td>
					<td>Language Known: &nbsp; <?php echo $row['langknown']; ?></td>
				</tr>	

				<tr>
					<td>Blood Group: &nbsp; <?php echo $row['blood_group']; ?></td>
					<td>Qualification: &nbsp; <?php echo $row['qualification']; ?></td>
					<td>Work Exp. : &nbsp; <?php echo $row['work_experience']; ?></td>
					<td>Alternate MobileNo: &nbsp; <?php echo $row['alternate_mobile']; ?></td>
				</tr>	
				
				<tr>
					<td>Email Id: &nbsp; <?php echo $row['email_id']; ?></td>
					<td>Guarantor Name: &nbsp; <?php echo $row['guarantor_name']; ?></td>
					<td>Ac Holder. : &nbsp; <?php echo $row['acc_holder']; ?></td>
					<td>Ac No: &nbsp; <?php echo $row['acc_no']; ?></td>
				</tr>	

				<tr>
					<td>Bank Name: &nbsp; <?php echo $row['acc_bank']; ?></td>
					<td>IFSC: &nbsp; <?php echo $row['acc_ifsc']; ?></td>
					<td>PAN No: &nbsp; <?php echo $row['acc_pan']; ?></td>
					<td>Age: &nbsp; <?php echo $row['ageyrs']; ?></td>
				</tr>

				<tr>
					<td>Father Occupation: &nbsp; <?php echo $row['fatherocc']; ?></td>
					<td>Mother: &nbsp; <?php echo $row['mothername']; ?></td>
					<td>Mother Occupation : &nbsp; <?php echo $row['motherocc']; ?></td>
					<td>Aadhar No: &nbsp; <?php echo $row['aaadharno']; ?></td>
				</tr>

				<tr>
					<td>
						Aadhar Copy: &nbsp; <a class="btn btn-danger btn-xs" data-toggle="modal" href="#View_Aadhar">Front</a>
						<a class="btn btn-xs btn-danger" data-toggle="modal" href="#View_Aadhar_Rear">Rear</a>
					</td>
					<td>PAN Copy: &nbsp; <a class="btn btn-xs btn-danger" data-toggle="modal" href="#View_PAN">View</a></td>
					<td>Pincode : &nbsp; <?php echo $row['pincode']; ?></td>
					<td>NR. Police Station: &nbsp; <?php echo $row['policestation']; ?></td>
				</tr>

				<tr>
					<td>Maritial Status: &nbsp; <?php echo $row['maritialstatus']; ?></td>
					<td>Wife Name: &nbsp; <?php echo $row['wifename']; ?></td>
					<td>Wife Occupation: &nbsp; <?php echo $row['wifeocc']; ?></td>
					<td>No of Children: &nbsp; <?php echo $row['children']; ?></td>
				</tr>	

				<tr>
					<td colspan="2">Residential Address: &nbsp; <?php echo $row['residenceaddr']; ?></td>
					<td colspan="2">Current Address: &nbsp; <?php echo $row['currentaddr']; ?></td>
				</tr>					
				
			</table>
		</div>
		
	</div>
</div>
</div>

<div class="modal fade" id="View_Aadhar_Rear" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div style="font-size:14px;" class="modal-header bg-primary">
				Aadhar copy (Rear Side):
				<button type="button" class="pull-right btn btn-default btn-sm" data-dismiss="modal">Close</button>
		    </div>
			
			<div class="modal-body">
				<?php 
				if($row['aadharphoto_rear']==''){
					echo "<center><h5 style='color:red'>No attachment found..</h5></center>";
				}
				else{
					echo "<iframe height='400px' width='100%' src='$row[aadharphoto_rear]'></iframe>";
				}
				?>
           </div> 
		</div>
	</div>
</div>

<div class="modal fade" id="View_Aadhar" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div style="font-size:14px;" class="modal-header bg-primary">
				Aadhar copy (Front Side):
				<button type="button" class="pull-right btn btn-default btn-sm" data-dismiss="modal">Close</button>
		    </div>
			
			<div class="modal-body">
				<?php 
				if($row['aadharphoto']==''){
					echo "<center><h5 style='color:red'>No attachment found..</h5></center>";
				}
				else{
					echo "<iframe height='400px' width='100%' src='$row[aadharphoto]'></iframe>";
				}
				?>
           </div> 
		</div>
	</div>
</div>

<div class="modal fade" id="View_PAN" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div style="font-size:14px;" class="modal-header bg-primary">
				Uploaded PAN copy:
				<button type="button" class="pull-right btn btn-default btn-sm" data-dismiss="modal">Close</button>
		    </div>
	  
           <div class="modal-body">
				<?php 
				if($row['panphoto']==''){
					echo "<center><h5 style='color:red'>No attachment found..</h5></center>";
				}
				else{
					echo "<iframe height='400px' width='100%' src='$row[panphoto]'></iframe>";
				}
				?>
           </div> 
		</div>
	</div>
</div>