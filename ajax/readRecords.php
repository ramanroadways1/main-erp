<?php
require_once '../connection.php'; 

$vou_no = escapeString($conn,strtoupper($_POST['fids']));
$page_name11 = escapeString($conn,($_POST['page_name']));

if($page_name11=='pay_balance.php' || $page_name11=='rcv_pod_full.php')
{
$data = '<table class="table table-bordered table-striped" style="font-size:12px;">
<tr>    
	<th>LRNo</th>
	<th>From</th>
	<th>To</th>
	<th>Consignor & Consignee</th>
	<th>Actual<br>Weight</th>
	<th>Charge<br>Weight</th>
	<th>Rate</th>
	<th>ActFrt</th>
	<th>PODRcvBy</th>
	<th>PodDate</th>
	<th>PodCopy</th>
</tr>';

$query = Qry($conn,"SELECT l.id,l.lrno,l.branch,l.fstation,l.tstation,l.consignor,l.consignee,l.wt12,l.weight,l.ratepmt,l.actualf,pod.pod_copy,
pod.branch as pod_branch,pod.pod_date,l2.round_trip_lrno 
FROM freight_form_lr AS l 
LEFT OUTER JOIN rcv_pod as pod ON pod.frno=l.frno AND pod.lrno=l.lrno
LEFT OUTER JOIN lr_sample as l2 ON l2.lrno=l.lrno
where l.frno='$vou_no'");

if(!$query){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	if(numRows($query)>0)
    {
    	$number = 1;
		$total_actual =0;
		$total_charge =0;
		$total_actualf =0;
		
    	while($row = fetchArray($query))
    	{
			$copyNo=1;
			$pod_copy=array();
			foreach(explode(",",$row['pod_copy']) as $pod_copy1)
			{
				$pod_copy[]="<a href='../$pod_copy1' target='_blank'>Copy: $copyNo</a>";
				$copyNo++;
			}
	
			$total_actual += $row['wt12'];
			$total_charge += $row['weight'];
			$total_actualf += $row['actualf'];
			
		if($row['branch']=='PUNE' AND $row['fstation']=='CHAKAN' AND $row['tstation']=='SANAND' AND $row['round_trip_lrno']!='')
		{
			$lrno1=$row['lrno']."<br> RoundTrip: $row[round_trip_lrno]";
		}
		else
		{
			$lrno1=$row['lrno'];
		}
			
			$data .= '<tr>
				<td>'.$lrno1.'</td>
				<td>'.$row['fstation'].'</td>
				<td>'.$row['tstation'].'</td>
				<td>Consignor: '.$row['consignor']."<br>Consignee: ".$row['consignee'].'</td>
				<td>'.$row['wt12'].'</td>
				<td>'.$row['weight'].'</td>
				<td>'.$row['ratepmt'].'</td>	
		        <td>'.$row['actualf'].'</td>
		        <td>'.$row['pod_branch'].'</td>
		        <td>'.date("d-m-y",strtotime($row["pod_date"])).'</td>
		        <td>'.implode(" , ",$pod_copy).'</td>
		    </tr>';
    		$number++;
//<td class="hyd"><button onclick="GetUserDetails('.$row['id'].')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></button></td>							
    	}

		$data .= '
		<tr>
			<td colspan=4><b>The Total Calculation of Freight Form LR :</b></td>				
			<td><b>'.$total_actual.'</b></td>
			<td><b>'.$total_charge.'</b></td>
			<td></td>
			<td><b>'.$total_actualf.'</b></td>
			<td colspan="3"></td>
		</tr>';
	}
    else
    {
    	$data .= '<tr><td colspan="16"><b>Records not found!</b></td></tr>';
    }

    $data .= '</table>';
}
else
{
$data = '<table class="table table-bordered table-striped" style="font-size:12px;">

<tr>    
	<th>LRNo</th>
	<th>From</th>
	<th>To</th>
	<th>Consignor & Consignee</th>
	<th>Actual<br>Weight</th>
	<th>Charge<br>Weight</th>
	<th>Rate</th>
	<th>ActFrt</th>
</tr>';

$query = Qry($conn,"SELECT id,lrno,fstation,tstation,consignor,consignee,wt12,weight,ratepmt,actualf 
FROM freight_form_lr where frno='$vou_no'");

if(!$query){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	if(numRows($query)>0)
    {
    	$number = 1;
		$total_actual =0;
		$total_charge =0;
		$total_actualf =0;
		
    	while($row = fetchArray($query))
    	{
			$total_actual += $row['wt12'];
			$total_charge += $row['weight'];
			$total_actualf += $row['actualf'];
			
			$data .= '<tr>
				<td>'.$row['lrno'].'</td>
				<td>'.$row['fstation'].'</td>
				<td>'.$row['tstation'].'</td>
				<td>Consignor: '.$row['consignor']."<br>Consignee: ".$row['consignee'].'</td>
				<td>'.$row['wt12'].'</td>
				<td>'.$row['weight'].'</td>
				<td>'.$row['ratepmt'].'</td>	
		        <td>'.$row['actualf'].'</td>
		    </tr>';
    		$number++;
//<td class="hyd"><button onclick="GetUserDetails('.$row['id'].')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></button></td>							
    	}

		$data .= '
		<tr>
			<td colspan=4><b>The Total Calculation of Freight Form LR :</b></td>				
			<td><b>'.$total_actual.'</b></td>
			<td><b>'.$total_charge.'</b></td>
			<td></td>
			<td><b>'.$total_actualf.'</b></td>
		</tr>';
	}
    else
    {
    	$data .= '<tr><td colspan="16"><b>Records not found!</b></td></tr>';
    }

    $data .= '</table>';
}

echo $data;
HideLoadicon();
echo "<script>
	$('#window_loadicon').fadeOut();
</script>";
?>