<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
include("./_loadicon.php");
include("./disable_right_click.php");
?>
<style>
label{font-size:13px}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

<link href="css/styles.css" rel="stylesheet">

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">

<br />

	<div class="row">
		<div class="form-group col-md-12">
			<h3 style="color:#FFF">Credit : </h3>
		</div>
	</div>

<br />

		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#CrBankWdlModal">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/neft.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">Bank Withdrawal</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#CrFreightModal">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/truck_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px;padding-top:6px;">Own Truck Freight</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#CrHOModal">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/ho.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px;padding-top:6px;">Credit Head-Office</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#CrOthersModal">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/other_cr.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px;padding-top:6px;">Other Credits</div>
						</div>
					</div>
				</div>
			</div>	
          </div>
		  
		  <div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#CrClaimModal">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<center><img class="img-responsive" style="width:80px;height:80%" src="svg/claim1.png"></center>
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">Rcv Claim (in cash)</div>
						</div>
					</div>
				</div>
			</div>	
		 </div>	

</div>
</div>

<?php
include("./modal_cr_bank_wdl.php");
include("./modal_cr_freight.php");
include("./modal_cr_ho.php");
include("./modal_cr_others.php");
include("./modal_cr_claim.php");

include("./_footer.php");
?>