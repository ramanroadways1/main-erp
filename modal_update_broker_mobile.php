<form id="BrokerMobileForm" action="#" method="POST">
<div id="ModalUpdateMobileBroker" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Broker's Mobile No.
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Broker name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="name" id="broker_name_mobile_update" class="form-control" required="required">
			</div> 
			
			<div class="form-group col-md-6">
				<label>PAN number <font color="red"><sup>*</sup></font></label>
				<input id="update_mobile_broker_pan" readonly type="text" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input id="update_broker_mobile" type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="type" value="broker">
			<input type="hidden" name="id" id="broker_mobile_id" value="">
			
			<div class="form-group col-md-12" id="MobileAlertBroker" style="display:none;color:red">Branch can update mobile only once !!</div>
			
		</div>
      </div>
	  <div id="result_BrokerMobileForm"></div>
      <div class="modal-footer">
        <button type="submit" disabled="disabled" id="updateBrokerMobileBtn" class="btn btn-primary">Update</button>
        <button type="button" id="" onclick="$('#BrokerMobileForm')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#BrokerMobileForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#updateBrokerMobileBtn").attr("disabled", true);
	$.ajax({
        	url: "./save_update_mobile.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_BrokerMobileForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>