<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$asset_cat1 = escapeString($conn,strtoupper($_POST['asset_cat']));

$asset_cat = explode("_",$asset_cat1)[0];
$asset_cat_name = explode("_",$asset_cat1)[1];

// $payment_mode = escapeString($conn,strtoupper($_POST['payment_mode']));
$maker = trim(escapeString($conn,strtoupper($_POST['maker'])));
$model = trim(escapeString($conn,strtoupper($_POST['model'])));
$narration = trim(escapeString($conn,strtoupper($_POST['narration'])));
// $party_id = trim(escapeString($conn,strtoupper($_POST['party_id'])));

$getReqCode = GetAssetReqCode(substr($branch,0,3),$conn);

if(!$getReqCode || $getReqCode=="0" || $getReqCode==""){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
$reg_code = $getReqCode;

// if($payment_mode=='NEFT'){
	// $ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	// $ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	// $bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	// $ifsc = trim(escapeString($conn,strtoupper($_POST['ifsc'])));
	// $pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
// }
// else{
	// $ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	// $ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	// $bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	// $ifsc = trim(escapeString($conn,strtoupper($_POST['ifsc'])));
	// $pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
// }

$req_token = md5(rand(100000,999999));

$insert_reg = Qry($conn,"INSERT INTO asset_request(req_token,category,req_code,date,branch,branch_user,maker,model,narration,payment_mode,timestamp) 
VALUES ('$req_token','$asset_cat','$reg_code','$date','$branch','$branch_sub_user','$maker','$model','$narration','','$timestamp')");

if(!$insert_reg){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_manager_mobile = Qry($conn,"SELECT mobile_no FROM emp_attendance WHERE code = (SELECT emp_code FROM manager WHERE branch='$branch')");

if(!$get_manager_mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_emp_name = Qry($conn,"SELECT name FROM emp_attendance WHERE code = '$branch_sub_user'");

if(!$get_emp_name){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_manager_mobile = fetchArray($get_manager_mobile);
$row_emp_name = fetchArray($get_emp_name);

$link_token = md5($asset_cat_name.$branch_sub_user);

// $msg_templates = "*New Asset Request Generated*\nAsset Category: $asset_cat_name\nMaker: $maker\nModel: $model\nNarration: $narration\nUsername: $row_emp_name[name]\nApproval OTP: 123456\n\nApproval Link: https://rrpl.online/links/asset_approve_manager?token=$req_token";
$msg_templates = "*New Asset Request Generated*\nAsset Category: $asset_cat_name\nMaker: $maker\nModel: $model\nNarration: $narration\nUsername: $row_emp_name[name].";

SendWAMsg($conn,$row_manager_mobile['mobile_no'].",9024281599",$msg_templates);

	echo "<script>
			alert('Request Generated Successfully. Manager Approval Required !');
			window.location.href='assets_view.php';
		</script>";
	closeConnection($conn);
	exit();
	
?>