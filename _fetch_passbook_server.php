<?php
require_once 'connection.php';

$company =  escapeString($conn,strtoupper($_REQUEST['company'])); 
$from_date =  escapeString($conn,strtoupper($_REQUEST['from_date']));
$to_date =  escapeString($conn,strtoupper($_REQUEST['to_date']));

if($company=='RRPL'){
	$sql ="SELECT p.id,p.user_code,DATE_FORMAT(p.date,'%d-%m-%y') as date,DATE_FORMAT(p.vou_date,'%d-%m-%y') as vou_date,p.vou_no,
	p.vou_type,p.desct,p.debit as dr,e.name FROM passbook as p
LEFT OUTER JOIN emp_attendance AS e ON e.code=p.user_code
WHERE p.date BETWEEN '$from_date' AND '$to_date' AND p.comp='$company' AND p.user='$branch' ORDER BY p.id ASC";
}else{
$sql ="SELECT p.id,p.user_code,DATE_FORMAT(p.date,'%d-%m-%y') as date,DATE_FORMAT(p.vou_date,'%d-%m-%y') as vou_date,p.vou_no,
	p.vou_type,p.desct,p.debit2 as dr,e.name FROM passbook as p
LEFT OUTER JOIN emp_attendance AS e ON e.code=p.user_code
WHERE p.date BETWEEN '$from_date' AND '$to_date' AND p.comp='$company' AND p.user='$branch' ORDER BY p.id ASC";
}


$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'name', 'dt' => 1),
    array( 'db' => 'date', 'dt' => 2),
    array( 'db' => 'vou_date', 'dt' => 3), 
    array( 'db' => 'vou_no', 'dt' => 4), 
    array( 'db' => 'vou_type', 'dt' => 5),  
    array( 'db' => 'desct', 'dt' => 6),
    array( 'db' => 'dr', 'dt' => 7), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);