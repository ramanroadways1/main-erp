<?php
require_once './connection.php';

$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$name = escapeString($conn,strtoupper($_POST['name']));

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile Number !');
		$('#update_button').attr('disabled',true);
		$('#mobile_emp_update').attr('readonly',false);
		$('#send_otp_button').attr('disabled',false);
		$('#otp_div').hide();
		$('#loadicon').hide();
	</script>";
	exit();
}

$Check_Mobile = Qry($conn,"SELECT name FROM emp_attendance WHERE mobile_no='$mobile'");

if(!$Check_Mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
}

if(numRows($Check_Mobile)>0)
{
	$row_Check = fetchArray($Check_Mobile);
	
	echo "<script>
		alert('Duplicate Mobile Number. Mobile Number Registered with $row_Check[name] !');
		$('#update_button').attr('disabled',true);
		$('#mobile_emp_update').attr('readonly',false);
		$('#send_otp_button').attr('disabled',false);
		$('#otp_div').hide();
		$('#loadicon').hide();
	</script>";
	exit();
}

$date = date('Y-m-d');
$otp = rand(100000,999999);
$message="Hello $name, Enter OTP: $otp to verify your mobile number.";

SendMsgCustom($mobile,$message);

$_SESSION['session_otp'] = $otp;

	echo "<script>
			$('#update_button').attr('disabled',false);
			$('#send_otp_button').attr('disabled',true);
			$('#mobile_emp_update').attr('readonly',true);
			$('#otp_div').show();
			$('#loadicon').hide();
		</script>";	
?>