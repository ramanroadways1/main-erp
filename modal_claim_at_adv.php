<div id="ClaimModal" class="modal fade" style="background:#eee;overflow:hidden" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add Claim Amount
      </div>
      <div class="modal-body">
        <div class="row">
		<form id="ClaimForm" action="#" style="font-size:13px" method="POST">
			
			<span style="color:red">&nbsp; &nbsp;  जिस LR का पैसा काटना चाहते है </span>
			<br />
			<br />
			
			<div class="form-group col-md-3">
				<label>Enter LR No. <font color="red"><sup>*</sup></font></label>
				<input type="text" onblur="GetClaimVouNo(this.value)" id="claim_lrno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			
			<script>
			function GetClaimVouNo(lrno)
			{
				if(lrno!='')
				{
					$("#loadicon").show();
					jQuery.ajax({
						url: '<?php echo $url_of_get_claim_vou_no; ?>',
						data: 'lrno=' + lrno,
						type: "POST",
						success: function(data){
							$("#claim_modal_vou_no").html(data);
						},
						error: function() {}
					});
				}
			}
			</script>
			
			<div class="form-group col-md-4">
				<label>Select Voucher. <font color="red"><sup>*</sup></font></label>
				<select name="claim_vou_no" style="font-size:13px" id="claim_modal_vou_no" class="form-control" required="required">
					<option value="" style="font-size:13px">--select voucher--</option>
				</select>
			</div>
			
			<div class="form-group col-md-3">
				<label>Claim Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" min="1" name="claim_amount" id="claim_amt" class="form-control" required="required">
			</div>
			
			<?php
			if(basename($_SERVER['PHP_SELF'])=="pay_balance.php")
			{
				$vou_type_for_claim="BAL";
				$vou_no_for_claim = $vou_no;
			}
			else
			{
				$vou_type_for_claim="ADV";
				$vou_no_for_claim = $idmemo;
			}
			?> 
			
			<input type="hidden" value="<?php echo $vou_no_for_claim; ?>" name="vou_no">
			<input type="hidden" value="<?php echo $vou_type_for_claim; ?>" name="vou_type">
				
			<div class="form-group col-md-2">
				<label>&nbsp;</label>
				<br>
				<button type="submit" id="claim_add_btn_one" class="btn btn-success">Add claim</button>
			</div>
		</form>	
			
		<div class="form-group col-md-12 table-responsive" id="claim_cache_result">
		</div>
	
		</div>
      </div>
	  <div id="claimFormResult"></div>
      <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#ClaimForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
		$.ajax({
        	url: '<?php echo $url_of_claim_submit; ?>',
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#claimFormResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function LoadClaimLR(vou_no)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: '<?php echo $url_of_claim_fetch; ?>',
		data: 'vou_no=' + vou_no + '&type=' + '<?php echo $vou_type_for_claim; ?>',
		type: "POST",
		success: function(data){
			$("#claim_cache_result").html(data);
		},
		error: function() {}
	});
}

function DeleteClaim(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: '<?php echo $url_of_claim_delete; ?>',
		data: 'id=' + id + '&vou_no=' + '<?php echo $vou_no_for_claim; ?>',
		type: "POST",
		success: function(data){
			$("#claimFormResult").html(data);
		},
		error: function() {}
	});
}
</script>