<?php 
require_once './connection.php';

$company=escapeString($conn,($_POST['company']));
$ac_no = escapeString($conn,$_POST['ac_no']);
$bank_name = escapeString($conn,$_POST['bank_name']);
$amount=escapeString($conn,($_POST['amount']));
$chq_no=escapeString($conn,($_POST['chq_no']));
$date = date("Y-m-d"); 
$nrr = $bank_name."-".$ac_no.", ChequeNo: ".$chq_no.", Date: ".$date;
$timestamp = date("Y-m-d H:i:s");
	
if($amount<=0 || $amount=='')
{
	echo "<script>
		alert('Error : Invalid amount !');
		$('#loadicon').hide();
		$('#cr_bank_deposit_button').attr('disabled',false);
	</script>";
	exit();	
}

if($ac_no=="")
{
	echo "<script>
		alert('Account nummber: $ac_no is not valid !');
		$('#cr_bank_ac_list').val('');
		$('#cr_bank_name').val('');
		$('#dr_bank_ac_no').val('');
		$('#loadicon').hide();
		$('#cr_bank_deposit_button').attr('disabled',false);
	</script>";
	exit();	
}		

$check_bal=Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");

if(!$check_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./credit.php");
	exit();
}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash = $row_bal['balance'];
	$rr_cash = $row_bal['balance2'];
	$email = $row_bal['email'];
	
if($company=='RRPL') 
{	
	$newbal = $amount + $rrpl_cash;
	$credit_col = 'credit';		
	$balance_col = 'balance';		
}
else if($company=='RAMAN_ROADWAYS')
{
	$newbal=$amount + $rr_cash;
	$credit_col = 'credit2';		
	$balance_col = 'balance2';		
}
else
{
	echo "<script type='text/javascript'>
		alert('Something went wrong..');
		window.location.href='./credit.php';
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;
	
$query = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");

if(!$query){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$cashbook_entry = Qry($conn,"INSERT INTO cashbook(user,user_code,date,comp,vou_type,desct,`$credit_col`,`$balance_col`,timestamp) VALUES 
('$branch','$_SESSION[user_code]','$date','$company','CREDIT ADD BALANCE','$nrr','$amount','$newbal','$timestamp')");

if(!$cashbook_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Amount Credited Successfully !!');
		window.location.href='./credit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit.php");
	exit();
}
?>