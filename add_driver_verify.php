<?php
require_once './connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$sourcePath = $_FILES['lic_copy']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['lic_copy']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image Upload Allowed.');
		$('#market_driver_validate_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$check_ocr_active = Qry($conn,"SELECT id FROM _ocr_active WHERE branch='$branch' AND active='1'");
		
if(!$check_ocr_active){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo '<script>
		alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
	
if(numRows($check_ocr_active) >0 )
{	
	$ocr_pan = VerifyOCR_DL($aadhar_api_url,$_FILES['lic_copy']['tmp_name'],$aadhar_api_token);

	if($ocr_pan[0]=="ERROR")
	{
		$error_code = $ocr_pan[1];
		$error_msg = $ocr_pan[2];
		$api_response = escapeString($conn,($ocr_pan[3]));
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
		('OCR_DL','1','$api_response','$branch','$branch_sub_user','$timestamp')");
			
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		echo '<script>
			alert("OCR Error_Code '.$error_code.' : '.$error_msg.'");
			$("#market_driver_validate_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	else
	{
		$lic_no = $ocr_pan[1];
		$lic_no_confd = $ocr_pan[2];
		
		$dob = $ocr_pan[3];
		$dob_confidence = $ocr_pan[4];
		
		$api_response = escapeString($conn,($ocr_pan[5]));
		
		$chk_ocr_record = Qry($conn,"SELECT id FROM _data_ocr_dl WHERE lic_no='$lic_no'");
		
		if(numRows($chk_ocr_record)==0)
		{
			$insert_ocr = Qry($conn,"INSERT INTO _data_ocr_dl(lic_no,lic_no_confd,dob_ocr,dob_ocr_confd,branch,branch_user,timestamp) VALUES ('$lic_no',
			'$lic_no_confd','$dob','$dob_confidence','$branch','$branch_sub_user','$timestamp')");
			
			if(!$insert_ocr){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				echo '<script>
					alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
				</script>';
				exit();
			}
		}
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
		('OCR_DL','$api_response','$branch','$branch_sub_user','$timestamp')");
				
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	}

	$chk_dl = Qry($conn,"SELECT name FROM mk_driver WHERE pan='$lic_no' AND pan!='NA' AND pan!=''");

	if(!$chk_dl){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo '<script>
			alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
		
	if(numRows($chk_dl)>0)
	{
		$row_name = fetchArray($chk_dl);
		
		echo "<script type='text/javascript'>
			alert('License No : $lic_no already registered with Driver: $row_name[name] !'); 
			$('#market_driver_validate_btn').attr('disabled',false);
			$('#market_driver_lic_copy').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();	
	}

	$verify_dl_no = VerifyDL($lic_no,$aadhar_api_url,$aadhar_api_token);

	if($verify_dl_no[0]=="ERROR")
	{
		$error_code = $verify_dl_no[1];
		$error_msg = $verify_dl_no[2];
		$api_response = escapeString($conn,($verify_dl_no[3]));
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
		('VERIFY_DL','1','$api_response','$branch','$branch_sub_user','$timestamp')");
			
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		echo '<script>
			alert("Verification Error_Code '.$error_code.' : '.$error_msg.'");
			$("#market_driver_validate_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	else
	{
		$full_name_api = $verify_dl_no[3];
		$name_length = strlen($full_name_api);
		$permanent_addr = $verify_dl_no[4];
		$permanent_addr_pin = $verify_dl_no[5];
		$temp_addr = $verify_dl_no[6];
		$temp_addr_pin = $verify_dl_no[7];
		$father_husband_name = $verify_dl_no[12];
		$dob = $verify_dl_no[13];
		$expiry_date = $verify_dl_no[14];
		$issue_date = $verify_dl_no[15];
		$blood_group = $verify_dl_no[16];
		$vehicle_classes = $verify_dl_no[17];
		$image = $verify_dl_no[18];
		
		$api_response = escapeString($conn,$verify_dl_no[19]);
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
		('VERIFY_DL','$api_response','$branch','$branch_sub_user','$timestamp')");
				
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#market_driver_validate_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	}
	
	$fix_name = mt_rand().date('dmYHis');

	$targetPath = "driver_pan_temp/".$fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);
	$targetPath1 = $fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);

	// ImageUpload(1000,1000,$sourcePath);

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		echo '<script>
			alert("Error while uploading document !");
			$("#market_driver_validate_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}

	$_SESSION['add_driver_filepath'] = $targetPath1;	
	$_SESSION['add_driver_lic_no'] = $lic_no;
	$_SESSION['add_driver_name'] = $full_name_api;
	$_SESSION['add_driver_permanent_addr'] = $permanent_addr." $permanent_addr_pin.";
	$_SESSION['add_driver_temp_addr'] = $temp_addr." $temp_addr_pin.";
	$_SESSION['add_driver_father_husband'] = $father_husband_name;
	$_SESSION['add_driver_dob'] = $dob;
	$_SESSION['add_driver_expiry_date'] = $expiry_date;
	$_SESSION['add_driver_issue_date'] = $issue_date;
	$_SESSION['add_driver_blood_group'] = $blood_group;
	$_SESSION['add_driver_vehicle_classes'] = print_r($vehicle_classes);
	$_SESSION['add_driver_image'] = $image;
	
	echo "<script type='text/javascript'> 
		$('#market_driver_name').attr('readonly',true);
		$('#market_driver_addr').attr('readonly',true);
		$('#market_driver_lic_no').attr('readonly',true);
		
		$('#market_driver_name').val('$full_name_api');
		$('#market_driver_lic_no').val('$lic_no');
		$('#market_driver_addr').val('$_SESSION[add_driver_permanent_addr]');
		
		$('#market_driver_lic_copy').attr('disabled',true);
		$('#market_driver_validate_btn').attr('disabled',true);
		$('#market_driver_validate_btn').hide();
		
		$('#market_driver_aadhar_validate_btn').attr('disabled',false);
		$('#market_driver_aadhar_validate_btn').show();
		
		$('#add_driver_button').attr('disabled',true);
		$('#add_driver_button').hide();
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	$fix_name = mt_rand().date('dmYHis');

	$targetPath = "driver_pan_temp/".$fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);
	$targetPath1 = $fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);

	// ImageUpload(1000,1000,$sourcePath);

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		echo '<script>
			alert("Error while uploading document !");
			$("#market_driver_validate_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	
	$_SESSION['add_driver_filepath'] = $targetPath1;

	echo "<script type='text/javascript'> 
		$('#market_driver_name').attr('readonly',false);
		$('#market_driver_addr').attr('readonly',false);
		$('#market_driver_lic_no').attr('readonly',false);
		
		$('#aadhar_no_div_id').attr('class','form-group col-md-6 aadhar_no_div');
		
		$('#market_driver_validate_btn').attr('disabled',true);
		$('#market_driver_validate_btn').hide();
		
		$('#market_driver_lic_copy').attr('disabled',true);
		$('#market_driver_aadhar_validate_btn').attr('disabled',true);
		$('#market_driver_aadhar_validate_btn').hide();
		
		$('#add_driver_button').attr('disabled',false);
		$('#add_driver_button').show();
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>