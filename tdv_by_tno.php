<?php
require_once 'connection.php';

$branch=escapeString($conn,strtoupper($_SESSION['user']));
$tno=escapeString($conn,strtoupper($_POST['tdv_tno']));
?>
<html>

<?php include("./_header.php"); ?>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
</style>  

<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<button class="btn btn-sm btn-primary" onclick="window.close();" type="button" style="margin-top:10px;margin-left:10px">Close window</button>

        <div class="container-fluid">
          <div class="col-md-12">
			<center>
				<h4 style="letter-spacing:1px">Truck Vou Summary : <?php echo $tno; ?></h4>
			 </center>	
		
<div class="panel-body table-responsive">                         
    
<?php
$result=Qry($conn,"SELECT user,company,tdvid,newdate,truckno,dname,amt,dest FROM mk_tdv WHERE truckno='$tno' ORDER BY date DESC");
if(!$result){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($result)==0)
{
	echo "<script>
		alert('Voucher not found : $tno.');
		window.close();
	</script>";
	closeConnection($conn);
	exit();
}


echo "<table class='table table-bordered' style='font-family:;font-size:12px;'>
	<tr>
		<th>#</th>
		<th>Vou Id</th>
		<th>Branch</th>
		<th>Company</th>
		<th>Vou Date</th>
		<th>Truck No</th>
		<th>Driver</th>
		<th>Amount</th>
		<th>Destination/Remark</th>
	</tr>";

$no1=1;
while($row = fetchArray($result))
{
	$vou_no=$row['tdvid'];
	$vou_type='Truck_Voucher';

	$vou_date = date('d/m/y', strtotime($row['newdate']));
	
	echo "<tr>
		<td>$no1</td>
		<td>
			<form action='showvou.php' target='_blank' method='POST'>
			<input type='hidden' name='voutype' value='$vou_type' />
			<input type='hidden' name='vou_no' value='$vou_no' />
			<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
			</form>
		</td>
		<td>$row[user]</td>
		<td>$row[company]</td>
		<td>$vou_date</td>
		<td>$row[truckno]</td>
		<td>$row[dname]</td>
		<td>$row[amt]</td>
		<td>$row[dest]</td>
	<tr>";

$no1++;

}

closeConnection($conn);

echo "</tr>
</table>";
?>
</div>
</div>
    </div>

</body>
</html>