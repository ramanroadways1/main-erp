<script>
$(function() {
    $("#tno_freight").autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
            $(event.target).focus();
			$("#cr_freight_button").attr("disabled",true);
			alert('Truck No does not exists.');
		}
    }, 
    focus: function (event, ui){
		$("#cr_freight_button").attr("disabled",false);
        return false;
    }
    });
  });
</script>

<form id="CreditFreightForm" action="#" method="POST">
<div id="CrFreightModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Credit Own Truck Freight
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12" id="prev_credits" style="display:none">
			</div>
			
			<div class="form-group col-md-6">
				<label>Cash/Cheque <font color="red">*</font></label>
				<select class="form-control" onchange="FreightSel(this.value);" id="select_id" name="select_id" required="required">
					<option value="CASH">Cash</option>
					<option disabled value="CHEQUE">Cheque</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Freight Type Adv/Bal ? <font color="red">*</font></label>
				<select class="form-control" name="freight_type" required="required">
					<option value="">--Freight Type--</option>
					<option value="ADVANCE">ADVANCE</option>
					<option value="BALANCE">BALANCE</option>
				</select>
			</div>

<script type="text/javascript">
function FreightSel(elem)
{
	if(elem=='CASH')
	{
		$('#chq_div_freight').hide();
		$("#chqno_freight").val('');
	}
	else if(elem=='CHEQUE')
	{
		alert('Contact head-office for cheque entry !!');
		$('#select_id').val('');
	}
	else
	{
		$('#chq_div_freight').show();
		$("#chqno_freight").attr('required',true);
	}
}
</script>

			<div class="form-group col-md-6">
				<label>Vehicle No. <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');$('#bilty_type').val('');" name="tno" id="tno_freight" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Bilty Type <font color="red">*</font></label>
				<select class="form-control" onchange="BiltyType(this.value)" id="bilty_type" name="bilty_type" required="required">
					<option value="">Select an Option</option>
					<option value="MARKET">MARKET BILTY</option>
					<option value="OWN">OWN BILTY</option>
				</select>
			</div>

<script type="text/javascript">
function BiltyType(elem)
{
	$("#bilty_no").val('');
	$("#lr_no").val('');
	$("#lr_date").val('');
	$("#from_loc").val('');
	$("#to_loc").val('');
			
	$('#prev_credits').hide();
	
	if($('#tno_freight').val()=='')
	{
		alert('Enter Vehicle Number First.');
		$('#bilty_type').val('');
	}
	else
	{
		$('#tno_freight').attr('readonly',true);
	
		if(elem=='MARKET')
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "./fetch_mkt_bilty.php",
			data: 'tno=' + $('#tno_freight').val(),
			type: "POST",
			success: function(data) {
				$("#mkt_bilty_div").html(data);
			},
			error: function() {}
			});
		
			$('#mkt_bilty_div').show();
			$("#bilty_no").attr('required',true);
			
			$("#lr_no").attr('readonly',true);
			$("#lr_date").attr('readonly',true);
			$("#from_loc").attr('readonly',true);
			$("#to_loc").attr('readonly',true);
		}
		else
		{
			$('#mkt_bilty_div').hide();
			$("#bilty_no").val('');
			$("#bilty_no").attr('required',false);
			
			$("#lr_no").attr('readonly',false);
			$("#lr_date").attr('readonly',false);
			$("#from_loc").attr('readonly',false);
			$("#to_loc").attr('readonly',false);
		}
	
	}
}
</script>
			
		<div id="mkt_bilty_div" style="display:none" class="form-group col-md-6">
			<label>Mkt Bilty No. <font color="red">*</font></label>
			<select id="bilty_no" name="bilty_no" class="form-control" required>
				<option value="">Select bilty</option>
			</select>
		</div>
	
<script>											
function FetchBiltyData(bilty)
{
	if(bilty!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./fetch_mkt_bilty_data.php",
		data: 'bilty=' + bilty,
		type: "POST",
		success: function(data) {
			$("#prev_credits").html(data);
		},
		error: function() {}
		});
	}
}
</script>

	<div class="form-group col-md-6">
		<label>LR No <font color="red">*</font></label>
		<input name="lr_no" oninput="this.value=this.value.replace(/[^a-z,A-Z0-9]/,'')" id="lr_no" type="text" class="form-control" required />
	</div>

	<div class="form-group col-md-6">
		<label>LR Date <font color="red">*</font></label>
		<input name="lr_date" id="lr_date" max="<?php echo date("Y-m-d"); ?>" type="date" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	
	<div class="form-group col-md-6">
		<label>From Location <font color="red">*</font></label>
		<input name="from_loc" id="from_loc" type="text" class="form-control" required oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" />
	</div>
	
	<div class="form-group col-md-6">
		<label>To Location <font color="red">*</font></label>
		<input name="to_loc" id="to_loc" type="text" class="form-control" required oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" />
	</div>

	<div class="form-group col-md-6">
		<label>Credit Amount <font color="red">*</font></label>
		<input type="number" placeholder="Enter Amount" name="amount" min="1" class="form-control" required />	
	</div>

	<div class="form-group col-md-6" id="chq_div_freight" style="display:none">
		<label>Cheque No <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" name="chqno" id="chqno_freight" class="form-control" />
	</div>
	
	<div class="form-group col-md-6">
		<label>Narration <font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'')" name="narration" class="form-control" required="required"></textarea>
	</div>
			
	</div>
</div>
	  
	  <div id="result_cr_freight_modal"></div>
	  
      <div class="modal-footer">
        <button type="submit" id="cr_freight_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#CreditFreightForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#cr_freight_button").attr("disabled", true);
	$.ajax({
        	url: "./save_credit_freight.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_cr_freight_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>