<button style="display:none" id="modal_btn" data-toggle="modal" data-target="#ModalUpdateStatus"></button>

<form id="StatusForm" action="#" method="POST">
<div id="ModalUpdateStatus" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update eway bill status : <span id="ewb_no_html"></span>
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Select status <font color="red"><sup>*</sup></font></label>
				<select onchange="DelStatus(this.value)" class="form-control" required="required" name="ewb_status">
					<option value="">--select status--</option>
					<option value="Delivered">Delivered</option>
					<option value="In_transit">In-transit</option>
					<option value="Waiting_Inside">Waiting for unloading inside depot/customer</option>
					<option value="Waiting_Outside">Waiting for unloading outside depot/customer</option>
					<option value="Breakdown">Break down</option>
					<option value="Accident">Accident</option>
					<option value="Crossing">Crossing</option>
					<option value="Others">Others</option>
				</select>	
			</div>
			
			<script>
			function DelStatus(elem)
			{
				$('#del_date').val('');
				$('#crossing_location').val('');
				$('#narration').val('');
				
				if(elem=='Delivered')
				{
					$('#del_date').attr('readonly',false);
					$('#narration').attr('readonly',true);
					$('#crossing_location').attr('readonly',true);
				}
				else if(elem=='Others')
				{
					$('#del_date').attr('readonly',true);
					$('#narration').attr('readonly',false);
					$('#crossing_location').attr('readonly',true);
				}
				else if(elem=='Crossing')
				{
					$('#del_date').attr('readonly',true);
					$('#crossing_location').attr('readonly',false);
					$('#narration').attr('readonly',true);
				}
				else
				{
					$('#del_date').attr('readonly',true);
					$('#crossing_location').attr('readonly',true);
					$('#narration').attr('readonly',true);
					$('#narration').val(elem);
				}
			}
			</script>
			
			<div class="form-group col-md-6">
				<label>Delivery date <font color="red"><sup>*</sup></font></label>
				<input readonly id="del_date" name="del_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<input type="hidden" id="ewb_id" name="ewb_id">
			
			<div id="crossing_narr_div" class="form-group col-md-6">
				<label>Crossing Location <font color="red"><sup>*</sup></font></label>
				<textarea readonly oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:/]/,'')" id="crossing_location" name="crossing_location" class="form-control" required="required"></textarea>
			</div>
			
			<div id="other_nrr_div" class="form-group col-md-6">
				<label>Reason/Narration <font color="red"><sup>*</sup></font></label>
				<textarea readonly oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,:/]/,'')" id="narration" name="narration" class="form-control" required="required"></textarea>
			</div>
		
		</div>
      </div>
	  <div id="result_Form"></div>
      <div class="modal-footer">
        <button type="submit" id="btn_form" class="btn btn-primary">Submit</button>
        <button type="button" id="modal_close_btn" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#StatusForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_form").attr("disabled", true);
	$.ajax({
        	url: "./save_ewb_delivery_status.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>