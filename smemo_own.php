<?php
require_once 'connection.php';

// if($branch!=='CGROAD')
// {
// echo "<script>
		// alert('Temporary on hold.');
		// window.location.href='./';
	// </script>";
	// exit();
// }
$branch = escapeString($conn,strtoupper($_SESSION['user']));
$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");
?>
<!doctype html>
<html lang="en">
<head>
<title>RRPL: OWN TRUCK FORM</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}

.ui-autocomplete { z-index:2147483647; }

.form-control{
	text-transform:uppercase;
}
</style> 

<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $('#truck_wheeler').val(ui.item.wheeler);       
			  $("#get_lr_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#truck_wheeler").val('');
			$("#get_lr_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

</head>

<?php include("./modal_crossing_lr_add.php"); ?>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php 	
if(isset($_SESSION['voucher_olr'])){
	$vou_id = $_SESSION['voucher_olr'];
}
else{
	$vou_id = "NA";
}
?>

<div id="get_lr_result"></div>

<script>
function ClearCache()
{
	var retVal = confirm("Do You Really Want to Clear Cache ?");
   if(retVal == true)
   {
	$("#loadicon").show();
		jQuery.ajax({
			url: "./clear_olr_cache.php",
			data: 'ok=' + 'ok',
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
			error: function() {}
	});
   }
}

function LoadLrs(vou_no)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./get_lr_cache_olr.php",
			data: 'vou_id=' + vou_no,
			type: "POST",
			success: function(data){
				$("#get_lr_cache").html(data);
			},
			error: function() {}
	});
}
</script>

<div class="container-fluid">
<br />
<div class="row">
	<div class="form-group col-md-4">
		<a href="./" style="color:#FFF;" class="btn btn-sm btn-primary">
			<span class="glyphicon glyphicon-chevron-left"></span> Dashboard
		</a>
	</div>	

	<div class="form-group col-md-4">
		<center><h4>Own - Truck Trip Voucher</h4></center>
	</div>	
</div>

	<div class="row">
<script>
function GetLR(tno){
	if(tno==''){
		alert('Enter Vehicle Number First !!');
	}
	else{
		$('#truck_no').attr('readonly',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_lr_for_olr.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>
	
		<div class="form-group col-md-3">
			<label>Truck No <font color="red">*</font></label>
			<input value="<?php if(isset($_SESSION['voucher_olr_vehicle'])) { echo $_SESSION['voucher_olr_vehicle']; } ?>" <?php if(isset($_SESSION['voucher_olr'])){ echo "readonly"; } ?> type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="truck_no" id="truck_no" class="form-control" required>
		</div>
		
		<div class="form-group col-md-3">
			<label>&nbsp;</label>
			<br>
			<button onclick="GetLR($('#truck_no').val())" id="get_lr_button" type="button" class="btn btn-sm btn-primary">Get LRs</button>
		</div>
		
	</div>
	
	<div class="row" id="get_lr_cache">
	</div>
</div>	

</body>
</html>

<?php include("./_footer.php"); ?>

<script>
LoadLrs('<?php echo $vou_id; ?>');

function DeleteLrFromFm(id){
   var retVal = confirm("Do you really want to delete this LR ?");
   if(retVal == true)
   {
	 $("#loadicon").show();
		jQuery.ajax({
        url: "./delete_lr_from_cache.php",
        data: 'id=' + id + '&vou_no=' + '<?php echo $vou_id; ?>' + '&page_name=' + 'smemo_own.php',
        type: "POST",
        success: function(data){
            $("#get_lr_result").html(data);
        },
        error: function() {}
    });
   }
}

function SaveOlr(vou_no)
{
	$("#loadicon").show();
	$("#save_button_olr").attr('disabled',true);
		jQuery.ajax({
        url: "./save_own_truck_form.php",
        data: 'vou_no=' + vou_no,
        type: "POST",
        success: function(data){
            $("#get_lr_result").html(data);
        },
        error: function() {}
    });
}

function EditLR_Loc(id,from1,to1,from_id,to_id)
{
	$("#lr_id_edit_lr").val(id);
	$("#from_loc_edit_lr").val(from1);
	$("#to_loc_edit_lr").val(to1);
	$("#from_loc_edit_lr_id").val(from_id);
	$("#to_loc_edit_lr_id").val(to_id);
	
	$("#vou_lr_edit_modal_btn").click();
}
</script>

<?php
include("./modal_update_crossing_loc.php");
include("./modal_update_destination.php");
include("./modal_add_breaking_lr.php");
include("./modal_edit_voucher_lr.php");
?>