<?php
require_once 'connection.php';
$date = date("Y-m-d"); 

if(!isset($_POST['submit'])){
	echo "<script>window.location.href='./cashbook.php';</script>";
	exit();
}

$company =  escapeString($conn,strtoupper($_POST['company'])); 
$from_date =  escapeString($conn,strtoupper($_POST['from_date']));
$to_date =  escapeString($conn,strtoupper($_POST['to_date']));
?>
<html>
<head>
<title>Cashbook</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>


<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
		<div style="background-color:;padding:5px;" class="bg-primary form-group col-md-12">
			<a href="./cashbook.php"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
			<center><h5 style="margin-right:100px;">Cashbook Summary : <?php echo $company; ?></h5></center>
		</div>
		
		<div class="form-group col-md-12" id="getPAGEDIV"></div>
		
	</div>
</div>

</body>
</html>

<script>
function FetchCashbook()
{
	var from_date = '<?php echo $from_date; ?>';
	var to_date = '<?php echo $to_date; ?>';
	var company = '<?php echo $company; ?>';
	$.ajax({  
        type : 'POST',
        url  : '_fetch_cashbook.php',
        data : 'from_date=' + from_date + '&to_date=' + to_date + '&company=' + company,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
   });
}
FetchCashbook();
</script>