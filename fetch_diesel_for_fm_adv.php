<?php
require_once("connection.php");

$timestamp = date("Y-m-d H:i:s"); 

$type = escapeString($conn,strtoupper($_POST['type']));
$frno = escapeString($conn,strtoupper($_POST['frno']));

if(empty($type))
{
	echo "<script type='text/javascript'>
			alert('Unable to fetch Data : Type.');
			window.location.href='./';
		</script>";
	exit();
}

if(empty($frno))
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Data : Vou Id.');
		window.location.href='./';
	</script>";
	exit();
}

$sel_diesel = Qry($conn,"SELECT id,amount,card_pump,card_no,fuel_comp FROM diesel_sample WHERE frno='$frno' AND type='$type'");

if(!$sel_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	

if(numRows($sel_diesel)>0)
{
	echo "
	<button onclick=document.getElementById('modal_diesel_button').click() class='btn btn-sm btn-primary' type='button'> Add Diesel</button>
	<table class='table table-bordered' style='width:100%;font-size:12px'>
			<tr>
				<th>Id</th>
				<th>Card/Pump</th>
				<th>Amount</th>
				<th>Card_No</th>
				<th>Fuel_Company</th>
				<th>#</th>
			</tr>";
	$i=1;
	$sum_diesel = 0;	
	while($row_diesel=fetchArray($sel_diesel))
	{
		$sum_diesel = $sum_diesel+$row_diesel['amount'];
		echo "<tr>
				  <td>$i</td>
				  <td>$row_diesel[card_pump]</td>
				  <td>$row_diesel[amount]</td>
				  <td>$row_diesel[card_no]</td>
				  <td>$row_diesel[fuel_comp]</td>
				  <td><button type='button' class='btn btn-xs btn-danger' onclick=DeleteDiesel('$row_diesel[id]')>Delete</button></td>
			</tr>";
	$i++;
	}	
	
	echo "<tr>
			<td colspan='2'><b>Total : </b></td>
			<td><b> Rs : $sum_diesel/-</b></td>
			<td colspan='3'></td>
		</tr>";

	echo "</table>";
}
else
{
	$sum_diesel=0;
	echo "
	<button onclick=document.getElementById('modal_diesel_button').click() class='btn btn-sm btn-primary' type='button'> Add Diesel</button>
	<center>No diesel record found !</center>";
}

echo "<script>
	// var diesel_amount = Number($('#dsl_amt').val());
	// if(diesel_amount==''){
		// var diesel_amount = 0;
	// }
	// alert(diesel_amount);
	// var new_amount = Number(diesel_amount+$sum_diesel).toFixed(2);
	// alert(new_amount);
	// $('#dsl_amt').val(new_amount);
	
	$('#loadicon').hide();
</script>";
?>