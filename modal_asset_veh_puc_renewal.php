<form id="PUCRenewalForm" action="#" method="POST">
<div id="PUCRenewalModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Renew PUC
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="puc_ren_veh_no" name="veh_no" class="form-control" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC Copy <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="puc_copy" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC Start Date <font color="red">*</font></label>
				<input onchange="ChkPucDateRenewal(this.value)" id="puc_start_date_renewal" name="start_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-6">
				<label>PUC End Date <font color="red">*</font></label>
				<input onchange="if($('#puc_start_date_renewal').val()==''){alert('Select Inspection date first !');$(this).val('');}" id="puc_end_date_renewal" name="end_date" type="date" class="form-control" min="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<script>
			function ChkPucDateRenewal(from_date)
			{
				var nextDay = new Date(from_date);
				nextDay.setDate(nextDay.getDate() + 180);
				$('#puc_end_date_renewal').attr("min",nextDay.toISOString().slice(0,10));
			}
			</script>
	
		</div>
      </div>
	  <input type="hidden" name="id" id="puc_ren_form_id">
	  <div id="result_puc_renewal_form"></div>
      <div class="modal-footer">
        <button type="submit" id="puc_ren_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" onclick="$('#PUCRenewalForm')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#PUCRenewalForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#puc_ren_button").attr("disabled", true);
	$.ajax({
        	url: "./save_puc_renewal.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_puc_renewal_form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>