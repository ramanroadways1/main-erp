<form id="OwnerFormVerify" action="#" method="POST">
<div id="AddTruckModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD NEW Vehicle 
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<div class="row">
				<div class="col-md-4">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
					<select name="tno_1" data-live-search="true" id="tno_1" class="form-control" required>
					<option class="tno_1_sel_opt" value="">--Select--</option>
					<?php
					$get_state_code = Qry($conn,"SELECT state_code FROM dairy.states ORDER BY state_code ASC");
					if(!$get_state_code){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
						exit();
					}

					if(numRows($get_state_code)>0)
					{
						while($row=fetchArray($get_state_code))
						{
							echo "<option class='tno_1_sel_opt' id='state_list_$row[state_code]' value='$row[state_code]'>$row[state_code]</option>";
						}
					}
					?>
					</select>
				</div>
				<div class="col-md-4">
					<label>&nbsp;</label>
					<select name="tno_2" id="tno_2" class="form-control" required>
					<option class="tno_2_sel_opt" value="">--Select--</option>
					<?php
					for($i = 1; $i<=99; $i++) {
						// echo $i;
						echo "<option class='tno_2_sel_opt' id='".sprintf("%02d", $i)."' value='".sprintf("%02d", $i)."'>".sprintf("%02d", $i)."</option>";
					}
					?>
					</select>
				</div>
				
				<div class="col-md-4">
				<label>&nbsp;</label>
				<input type="text" maxlength="7" id="tno_3" placeholder="AX7902" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno_3" class="form-control" required="required">
				</div>
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label>Vehicle Owner Name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="market_veh_owner_name" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" id="market_tno_mobile" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Alternate Mobile Number </label>
				<input type="text" maxlength="10" id="market_tno_mobile2" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile2" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="pan_no2" maxlength="10" oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'');ValidatePAN2()" name="pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN Holder </label>
				<input type="text" id="pan_holder_name_vehicle" oninput="this.value=this.value.replace(/[^0-9]/,'')" readonly class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>Upload PAN Copy <font color="red"><sup>*</sup></font></label>
				<input type="file" id="market_vehicle_pan_copy" name="pan_copy" accept="image/*" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Upload RC <font color="red"><sup>(front)*</sup></font></label>
				<!--<input type="file" capture="camera" name="rc_front" accept="image/*" class="form-control" required="required">-->
				<input type="file" id="market_vehicle_rc_copy_front" name="rc_front" accept="image/*" class="form-control" required="required">
			</div>
			
			
			<div class="form-group col-md-4">
				<label>Upload RC <font color="red"><sup>(rear)*</sup></font></label>
				<input type="file" id="market_vehicle_rc_copy_rear" name="rc_rear" accept="image/*" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Upload Declaration</label>
				<input type="file" id="market_vehicle_dec_copy" name="dec_copy" accept="image/*" class="form-control">
			</div>
			
			<div class="form-group col-md-4">
				<label>Wheeler <font color="red"><sup>*</sup></font></label>
				<select name="wheeler" required="required" class="form-control" />
						<option class="market_vehicle_wheeler_sel" value="">Select an option</option>
						<option class="market_vehicle_wheeler_sel" value="4">4</option>
						<option class="market_vehicle_wheeler_sel" value="6">6</option>
						<option class="market_vehicle_wheeler_sel" value="10">10</option>
						<option class="market_vehicle_wheeler_sel" value="12">12</option>
						<option class="market_vehicle_wheeler_sel" value="14">14</option>
						<option class="market_vehicle_wheeler_sel" value="16">16</option>
						<option class="market_vehicle_wheeler_sel" value="18">18</option>
						<option class="market_vehicle_wheeler_sel" value="22">22</option>
				</select>
			</div>	
			
			<div class="form-group col-md-4">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" id="market_veh_addr" name="addr" class="form-control" required="required"></textarea>
			</div>
			
			<div class="form-group col-md-4">
				<label>&nbsp;</label>
				<?php
				if(!isMobile()){
					echo "<br />";
				}
				?>
				<button type="submit" id="verify_market_vehicle_btn" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-refresh"></span> Validate PAN</button>
			</div>
</form>
		</div>
      </div>
	<div id="result_OwnerForm"></div>
      <div class="modal-footer">
        <button type="button" disabled style="display:none" onclick="AddMarketVehicle()" id="add_owner_button" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" id="close_add_owner" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
<script type="text/javascript">
$(document).ready(function (e) {
	$("#OwnerFormVerify").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#verify_market_vehicle_btn").attr("disabled", true);
	$.ajax({
        	url: "./add_truck_verify.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_OwnerForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function AddMarketVehicle()
{
	var owner_name = $('#market_veh_owner_name').val();
	var mobile_no = $('#market_tno_mobile').val();
	var mobile_no2 = $('#market_tno_mobile2').val();
	var pan_holder = $('#pan_holder_name_vehicle').val();
	var addr = $('#market_veh_addr').val();
	
	jQuery.ajax({
	url: "./add_truck.php",
	data: 'owner_name=' + owner_name + '&mobile_no=' + mobile_no + '&mobile_no2=' + mobile_no2 + '&pan_holder=' + pan_holder + '&addr=' + addr,
	type: "POST",
	success: function(data) {
		$("#result_OwnerForm").html(data);
	},
	error: function() {}
	});
}
</script>

<script>
function ValidatePAN2() { 
  var Obj = document.getElementById("pan_no2");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no2").setAttribute("style","background-color:red;color:#FFF");
				// $("#add_owner_button").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no2").setAttribute("style","background-color:green;color:#FFF");
				// $("#add_owner_button").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no2").setAttribute("style","background-color:white;");
			// $("#add_owner_button").attr("disabled", false);
		}
  }
</script>	