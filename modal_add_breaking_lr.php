<form id="BreakingLRForm" action="#" method="POST">
<div id="BreakingLRModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD Breaking LR
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Enter LR No.<font color="red"><sup>*</sup></font></label>
				<input type="text" id="lrno_breaking_1" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			<div class="form-group col-md-3" id="break_chk_button">
				<label>&nbsp;</label>
				<br>
				<button type="button" onclick="GetBreakingLRs()" class="btn btn-danger">Check !</button>
			</div>
			
			<div class="form-group col-md-12">
				<label>Select LR <font color="red"><sup>*</sup></font></label>
				<select name="lr_tukda" id="lr_tukdas" onchange="GetWeight(this.value)" class="form-control" required="required">
					<option value="">--Select LR--</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>From Station <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="from_breaking_lr" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="from" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>To Station <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="to_breaking_lr" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="to" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Actual Weight <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="break_act_wt" name="actual_wt" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Charge Weight <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="break_charge_wt" name="charge_wt" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="breaking_id" id="breaking_lr_id">
			<input type="hidden" name="to_id" id="to_breaking_lr_id">
			<input type="hidden" name="from_id" id="from_breaking_lr_id">
			<input type="hidden" name="page_name1" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
			
		</div>
      </div>
	  <div id="result_BreakingLRForm"></div>
      <div class="modal-footer">
        <button type="submit" id="break_lr_add_btn" disabled class="btn btn-primary">ADD LR</button>
        <button type="button" class="btn btn-default" onclick="ResetModalBreaking()" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#BreakingLRForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#break_lr_add_btn").attr("disabled", true);
		$.ajax({
        	url: "./add_breaked_lr_to_fm.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_BreakingLRForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function GetBreakingLRs()
{
	var lrno = $('#lrno_breaking_1').val();
	
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_breaking_lrs.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data){
				$("#lr_tukdas").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		$('#break_lr_add_btn').atrr('disabled',true);
	}
}

function GetWeight(elem)
{
	var break_id = elem.split('_')[0];
	var act_wt = elem.split('_')[1];
	var chrg_wt = elem.split('_')[2];
	
	$('#breaking_lr_id').val(break_id);
	$('#break_act_wt').val(act_wt);
	$('#break_charge_wt').val(chrg_wt);
}

function ResetModalBreaking()
{
	document.getElementById("BreakingLRForm").reset();
	$('#break_lr_add_btn').attr('disabled',true);
	$('#lrno_breaking_1').attr('readonly',false);
	$('#break_chk_button').show();
}

$(function() {
		$("#from_breaking_lr").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#from_breaking_lr').val(ui.item.value);   
           $('#from_breaking_lr_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#from_breaking_lr").val('');
			$("#from_breaking_lr").focus();
			$("#from_breaking_lr_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		
$(function() {
		$("#to_breaking_lr").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#to_breaking_lr').val(ui.item.value);   
           $('#to_breaking_lr_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#to_breaking_lr").val('');
			$("#to_breaking_lr").focus();
			$("#to_breaking_lr_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});		
</script>