<div id="AddDriverModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD NEW DRIVER
      </div>
      <div class="modal-body">
        <div class="row">
			<form id="DriverForm" action="#" method="POST">
				<div class="form-group col-md-8"> 
					<label>Upload License Copy <font color="red"><sup>*</sup></font></label>
					<!--<input type="file" capture="camera" accept="image/*" name="lic_copy" class="form-control" required="required">-->
					<input type="file" accept="image/*" id="market_driver_lic_copy" name="lic_copy" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>&nbsp;</label>
					<?php
					if(!isMobile()){
						echo "<br />";
					}
					?>
					<button type="submit" id="market_driver_validate_btn" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-refresh"></span> Validate DL</button>
				</div>
			</form>	
			
			<div class="form-group col-md-6">
				<label>Driver Name <font color="red"><sup>*</sup></font></label>
				<input readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="market_driver_name" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" id="market_driver_mobile" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Alt. Mobile Number </label>
				<input type="text" maxlength="10" id="market_driver_mobile2" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile2" class="form-control">
			</div>
			
			<div class="form-group col-md-6">
				<label>License Number <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly placeholder="RJ041234567890" id="market_driver_lic_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lic_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-8 aadhar_no_div" id="aadhar_no_div_id"> 
				<label>Aadhar Number <font color="red"><sup>*</sup></font></label>
				<!--<input type="file" capture="camera" accept="image/*" name="lic_copy" class="form-control" required="required">-->
				<input type="text" maxlength="12" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="aadhar_no" id="market_driver_aadhar_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4 aadhar_no_div">
				<label>&nbsp;</label>
				<?php
				if(!isMobile()){
					echo "<br />";
				}
				?>
				<button type="button" disabled onclick="AadharSendOtp()" id="market_driver_aadhar_validate_btn" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-refresh"></span> Validate Aadhar</button>
			</div>
			
			<div class="form-group col-md-8 aadhar_otp_div" style="display:none"> 
				<label>Enter Aadhar OTP <font color="red"><sup>*</sup></font></label>
				<!--<input type="file" capture="camera" accept="image/*" name="lic_copy" class="form-control" required="required">-->
				<input type="text" maxlength="12" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="otp" id="market_driver_aadhar_otp" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4 aadhar_otp_div" style="display:none">
				<label>&nbsp;</label>
				<?php
				if(!isMobile()){
					echo "<br />";
				}
				?>
				<button type="button" disabled onclick="ValidateDriverAadharOtp()" id="market_driver_aadhar_validate_otp_btn" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-refresh"></span> Submit OTP</button>
			</div>
			
			<div class="form-group col-md-12 aadhar_holder_name_div" style="display:none">
				<label>Aadhar Holder Name <font color="red"><sup>*</sup></font></label>
				<input readonly id="market_driver_addhar_holder_name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" id="market_driver_addr" name="addr" class="form-control" required="required"></textarea>
			</div>
			
			<div id="result_aadhar_otp_res"></div>
			
		</div>
      </div>
	  <div id="result_driverForm"></div>
      <div class="modal-footer">
        <button type="button" disabled style="display:none" id="add_driver_button" onclick="AddMarketDriver()" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" id="close_add_driver" onclick="$('#DriverForm')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#DriverForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#market_driver_validate_btn").attr("disabled", true);
	$.ajax({
        	url: "./add_driver_verify.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_driverForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function AadharSendOtp()
{
	var aadhar_no = $('#market_driver_aadhar_no').val();
	var driver_name = $('#market_driver_name').val();
	
	if(driver_name=='')
	{
		alert('Upload license first !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./send_aadhar_otp.php",
		data: 'aadhar_no=' + aadhar_no,
		type: "POST",
		success: function(data) {
			$("#result_driverForm").html(data);
		},
		error: function() {}
		});
	}
}

function ValidateDriverAadharOtp()
{
	var otp = $('#market_driver_aadhar_otp').val();
	
	if(otp=='')
	{
		alert('Enter OTP first !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./validate_aadhar_otp.php",
		data: 'otp=' + otp,
		type: "POST",
		success: function(data) {
			$("#result_aadhar_otp_res").html(data);
		},
		error: function() {}
		});
	}
}

function AddMarketDriver()
{
	var driver_name = $('#market_driver_name').val();
	var lic_no = $('#market_driver_lic_no').val();
	var mobile1 = $('#market_driver_mobile').val();
	var mobile2 = $('#market_driver_mobile2').val();
	var addr = $('#market_driver_addr').val();
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "./add_driver.php",
	data: 'mobile1=' + mobile1 + '&mobile2=' + mobile2 + '&mobile2=' + '&addr=' + addr + '&driver_name=' + driver_name + '&lic_no=' + lic_no,
	type: "POST",
	success: function(data) {
		$("#result_driverForm").html(data);
	},
	error: function() {}
	});
}
</script>