<button style="display:none" type="button" id="open_crossing_lr" data-toggle="modal" data-target="#CrossStation"></button>

<form id="UpdateCrossStation" action="#" method="POST">
<div id="CrossStation" style="background:#eee" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Crossing LR : <span id="cross_lrno"></span>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Select Crossing Station <font color="red"><sup>*</sup></font></label>
				<input id="cross_location" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="cross_loc" 
				class="form-control" required="required">
			</div>
		</div>
      </div>
	  <input type="hidden" name="cross_loc_id" id="cross_location_id">
	  <input type="hidden" name="lrno" id="cross_lrno2">
	  <input type="hidden" name="table_id" id="checkboxId">
	  <input type="hidden" name="page_name1" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
	  
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" onclick="UnCheck($('#checkboxId').val())" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<div id="result_form_UpdateCrossStation"></div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdateCrossStation").on('submit',(function(e) {
		$("#loadicon").show();
		e.preventDefault();
		$.ajax({
        	url: "./update_crossing_loc.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_form_UpdateCrossStation").html(data);
				// $("#loadicon").hide();
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>