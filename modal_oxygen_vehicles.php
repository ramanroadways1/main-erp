 
<form style="font-size:12px" id="FormAddOxygenVehicle" action="#" method="POST">
<div id="OxyVehModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div style="font-size:14px;" class="modal-header bg-primary">
			Add Oxygen Vehicle
      </div>
      <div class="modal-body" style="height:350px !important;overflow:auto">
        <div class="row">
	
			<div class="form-group col-md-6">
				<label style="color:#000">Vehicle number <font color="red"><sup>*</sup></font></label>
				<input style="font-size:12.5px" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>&nbsp;</label>
				<br />
				<button type="submit" class="btn btn-primary btn-sm">Add vehicle</button>
			</div>
			
			<div class="form-group col-md-12 table-responsive">
				<table class="table table-bordered table-striped" style="font-size:12px">
					<tr>
						<th>#</th>
						<th>Vehicle number</th>
						<th>Username</th>
						<th>Added On</th>
					</tr>
					<?php
					$get_oxn_vehs = Qry($conn,"SELECT o.tno,o.timestamp,e.name as emp_name 
					FROM dairy.oxygn_tno AS o 
					LEFT OUTER JOIN emp_attendance AS e ON e.code = o.branch_user 
					WHERE o.is_active='1'");
					
					if(numRows($get_oxn_vehs)>0)
					{
						$sn_vhs=1;
						
						while($row_ox_veh = fetchArray($get_oxn_vehs))
						{
							echo "<tr>
								<td>$sn_vhs</td>
								<td>$row_ox_veh[tno]</td>
								<td>$row_ox_veh[emp_name]</td>
								<td>".date("d-m-y H:i A",strtotime($row_ox_veh['timestamp']))."</td>
							</tr>";
							$sn_vhs++;
						}
					}
					else
					{
						echo "<tr><td colspan='3'>No record added !</td></tr>";
					}
					?>					
				</table>
			</div>
			
		</div>
      </div>
	 
      <div class="modal-footer">
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							 
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormAddOxygenVehicle").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon_ewb_val").show();
	$("#ext_val_ewb_btn_save").attr("disabled", true);
	$.ajax({
        	url: "./save_oxygen_vehicle.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_Ewb_Extend_Form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>