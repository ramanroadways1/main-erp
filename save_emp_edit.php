<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$emp_id1 = escapeString($conn,($_POST['emp_id1']));
$emp_code1 = escapeString($conn,($_POST['emp_code1']));
$emp_name1 = escapeString($conn,($_POST['emp_name1']));

$chk_emp = Qry($conn,"SELECT name,code FROM emp_attendance WHERE id='$emp_id1'");

if(!$chk_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
}

if(numRows($chk_emp)==0)
{
	echo "<script>
		alert('Employee not found !');
		window.location.href='./';
	</script>";
	exit();
}

$row_emp = fetchArray($chk_emp);

if($row_emp['name']!=$emp_name1 || $row_emp['code']!=$emp_code1)
{
	// echo "Emp-id: $emp_id1. DB_name: $row_emp[name] and $chk_emp. Emp_code: $row_emp[code] and $emp_code1";
	errorLog("Emp-id: $emp_id1. DB_name: $row_emp[name] and $emp_name1. Emp_code: $row_emp[code] and $emp_code1",$conn,$page_name,__LINE__);	
	echo "<script>
		alert('Verification failed !');
		window.location.href='./';
		// $('#loadicon').hide();
		// $('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

// echo "<script>
		// alert('test !');
		// $('#loadicon').hide();
		// $('#emp_edit_save_btn').attr('disabled',false);
	// </script>";
	// exit();

$dob = escapeString($conn,strtoupper($_POST['birthdate']));
$age = escapeString($conn,strtoupper($_POST['age']));
$doj = escapeString($conn,strtoupper($_POST['joindate']));
$father = trim(escapeString($conn,strtoupper($_POST['fathername'])));
$father_occp = escapeString($conn,strtoupper($_POST['fatherocc']));

if($father_occp=='Others'){
	$father_occp2 = trim(escapeString($conn,strtoupper($_POST['fatherocc_other'])));
}
else{
	$father_occp2 = "";
}

$mother = trim(escapeString($conn,strtoupper($_POST['mothername'])));
$mother_occ = escapeString($conn,strtoupper($_POST['motherocc']));

if($mother_occ=='Others'){
	$mother_occ2 = trim(escapeString($conn,strtoupper($_POST['motherocc_other'])));
}
else{
	$mother_occ2 = "";
}

$aadhar_no = escapeString($conn,strtoupper($_POST['aadharno']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$addr_residence = trim(escapeString($conn,strtoupper($_POST['residence'])));
$addr_currentaddr = trim(escapeString($conn,strtoupper($_POST['currentaddr'])));
$pincode = escapeString($conn,strtoupper($_POST['pincode']));
$police_station = trim(escapeString($conn,strtoupper($_POST['police_station'])));
$qualification = escapeString($conn,strtoupper($_POST['qualification']));
$experience = escapeString($conn,strtoupper($_POST['experience']));
$bloodgroup = escapeString($conn,strtoupper($_POST['bloodgroup']));
// $mobileno = escapeString($conn,strtoupper($_POST['mobileno']));
$alternateno = escapeString($conn,strtoupper($_POST['alternateno']));
$emailid = escapeString($conn,($_POST['emailid']));
$emergency_mobile = escapeString($conn,strtoupper($_POST['emergencymo']));
$language = trim(escapeString($conn,strtoupper($_POST['language'])));
$dieases = trim(escapeString($conn,strtoupper($_POST['dieases'])));
$maritial = escapeString($conn,strtoupper($_POST['maritial']));

if($maritial=="MARRIED"){
	$wife_name = trim(escapeString($conn,strtoupper($_POST['wifename'])));
	$wife_occ = trim(escapeString($conn,strtoupper($_POST['wifeocc'])));
	$child_no = escapeString($conn,strtoupper($_POST['child']));
}
else{
	$wife_name = "";
	$wife_occ = "";
	$child_no = "";
}

if(isset($_POST['bank_checkbox']))
{
	$ac_holder = trim(escapeString($conn,strtoupper($_POST['acc_fullname'])));
	$ac_no = trim(escapeString($conn,strtoupper($_POST['acc_number'])));
	$ac_bank = trim(escapeString($conn,strtoupper($_POST['acc_bank'])));
	$ac_ifsc = trim(escapeString($conn,strtoupper($_POST['acc_ifsc'])));
	
	if($ac_holder==''){
		echo "<script>
			alert('Please Check A/c holder !');
			$('#loadicon').hide();
			$('#emp_edit_save_btn').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_no==''){
		echo "<script>
			alert('Please Check A/c number !');
			$('#loadicon').hide();
			$('#emp_edit_save_btn').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_bank==''){
		echo "<script>
			alert('Please Check Bank name !');
			$('#loadicon').hide();
			$('#emp_edit_save_btn').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_ifsc==''){
		echo "<script>
			alert('Please Check IFSC code !');
			$('#loadicon').hide();
			$('#emp_edit_save_btn').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(strlen($ac_ifsc)!='11'){
		echo "<script>
			alert('Please Check IFSC code !');
			$('#loadicon').hide();
			$('#emp_edit_save_btn').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	$ac_holder = "";
	$ac_no = "";
	$ac_bank = "";
	$ac_ifsc = "";
}

$gaurantor_name = trim(escapeString($conn,strtoupper($_POST['gaurantor_name'])));
$gaurantor_mobile = trim(escapeString($conn,strtoupper($_POST['gaurantor_mobile'])));
$gaurantor_address = trim(escapeString($conn,strtoupper($_POST['gaurantor_address'])));
$relative_name = escapeString($conn,strtoupper($_POST['relative_name']));
$relative_mobile = escapeString($conn,strtoupper($_POST['relative_mobile']));
$relative_address = escapeString($conn,strtoupper($_POST['relative_address']));
// $otp = escapeString($conn,strtoupper($_POST['otp']));

if(strlen($pincode)!=6){
	echo "<script>
		alert('Please Enter Valid PINCODE !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

// if($mobileno!='' AND strlen($mobileno)!=10){
	// echo "<script>
		// alert('Please Check Your Mobile Number !');
		// $('#loadicon').hide();
		// $('#emp_edit_save_btn').attr('disabled',false);
	// </script>";
	// exit();
// }

if($emergency_mobile!='' AND strlen($emergency_mobile)!=10){
	echo "<script>
		alert('Please Check Emergency Mobile Number !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

if($alternateno!='' AND strlen($alternateno)!=10){
	echo "<script>
		alert('Please Check Alternate Mobile Number. Enter Valid mobile number or leave empty !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

if($gaurantor_mobile!='' AND strlen($gaurantor_mobile)!=10){
	echo "<script>
		alert('Please Check Gaurantor\'s Mobile Number !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

if($relative_mobile!='' AND strlen($relative_mobile)!=10){
	echo "<script>
		alert('Please Check Relatives\'s Mobile Number !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

if(strlen($aadhar_no)!=12){
	echo "<script>
		alert('Please Check Aadhar Number !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

if($pan_no!='' AND (strlen($pan_no)!=10)){
	echo "<script>
		alert('Please Check PAN Number !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}

// $Check_Mobile = Qry($conn,"SELECT name FROM emp_attendance WHERE mobile_no='$mobileno'");

// if(!$Check_Mobile){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing request.","./");
// }

// if(numRows($Check_Mobile)>0)
// {
	// $row_Check = fetchArray($Check_Mobile);
	
	// echo "<script>
		// alert('Duplicate Mobile Number. Mobile Number Registered with $row_Check[name] !');
		// $('#emp_edit_save_btn').attr('disabled',true);
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

// VerifyOTP($otp,"button_add");

$emp_photo = $_FILES['emp_photo']['tmp_name'];
$aadhar_copy_front = $_FILES['aadhar_copy_front']['tmp_name'];
$aadhar_copy_rear = $_FILES['aadhar_copy_rear']['tmp_name'];

if($pan_no!="")
{
	$pan_copy = $_FILES['pan_copy']['tmp_name'];
	$targetPath_pan = "employee_data/pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
}

$targetPath_photo = "employee_data/photo/".mt_rand().date('dmYHis').".".pathinfo($_FILES['emp_photo']['name'],PATHINFO_EXTENSION);
$targetPath_aadhar_front = "employee_data/aadhar/".mt_rand().date('dmYHis').".".pathinfo($_FILES['aadhar_copy_front']['name'],PATHINFO_EXTENSION);
$targetPath_aadhar_rear = "employee_data/aadhar/".mt_rand().date('dmYHis').".".pathinfo($_FILES['aadhar_copy_rear']['name'],PATHINFO_EXTENSION);

StartCommit($conn);
$flag = true;

ImageUpload(600,400,$emp_photo);
			
if(!move_uploaded_file($emp_photo, $targetPath_photo)){
	$flag = false;
	errorLog("Error while uploading Employee\'s Photo !",$conn,$page_name,__LINE__);	
}

ImageUpload(600,400,$aadhar_copy_front);
ImageUpload(600,400,$aadhar_copy_rear);
			
if(!move_uploaded_file($aadhar_copy_front, $targetPath_aadhar_front)){
	$flag = false;
	errorLog("Error while uploading Employee\'s Aadhar - front !",$conn,$page_name,__LINE__);	
}

if(!move_uploaded_file($aadhar_copy_rear, $targetPath_aadhar_rear)){
	$flag = false;
	errorLog("Error while uploading Employee\'s Aadhar - rear !",$conn,$page_name,__LINE__);	
}

if($pan_no!="")
{
	ImageUpload(600,400,$pan_copy);
	
	if(!move_uploaded_file($pan_copy, $targetPath_pan)){
		$flag = false;
		errorLog("Error while uploading Employee\'s Pan-card !",$conn,$page_name,__LINE__);
	}
}
else
{
	$targetPath_pan="";
} 

$insert = Qry($conn,"UPDATE emp_attendance SET edit='1',join_date='$doj',father_name='$father',birth_date='$dob',residenceaddr='$addr_residence',
currentaddr='$addr_currentaddr',blood_group='$bloodgroup',qualification='$qualification',work_experience='$experience',
alternate_mobile='$alternateno',email_id='$emailid',guarantor_name='$gaurantor_name',guarantor_mobile='$gaurantor_mobile',
guarantor_addr='$gaurantor_address',acc_holder='$ac_holder',acc_no='$ac_no',acc_ifsc='$ac_ifsc',acc_bank='$ac_bank',acc_pan='$pan_no',
image='$targetPath_photo',ageyrs='$age',fatherocc='$father_occp',father_occ_other='$father_occp2',mother_occ_other='$mother_occ2',
mothername='$mother',motherocc='$mother_occ',aaadharno='$aadhar_no',aadharphoto='$targetPath_aadhar_front',
aadharphoto_rear='$targetPath_aadhar_rear',panphoto='$targetPath_pan',pincode='$pincode',policestation='$police_station',
emergencyphone='$emergency_mobile',langknown='$language',majordiseases='$dieases',maritialstatus='$maritial',wifename='$wife_name',
wifeocc='$wife_occ',children='$child_no',relativename='$relative_name' WHERE id='$emp_id1'");

if(!$insert){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$update_data = Qry($conn,"UPDATE emp_attendance_pending SET timestamp_updated='$timestamp' WHERE table_id='$emp_id1'");
	
if(!$update_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

if($flag)
{ 
	MySQLCommit($conn);
	closeConnection($conn);	
}
else
{
	unlink($targetPath_photo);		
	unlink($targetPath_aadhar_front);		
	unlink($targetPath_aadhar_rear);		
	
	if($pan_no!=""){
		unlink($targetPath_pan);	
	}
	
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').hide();
		$('#emp_edit_save_btn').attr('disabled',false);
	</script>";
	exit();
}	

echo "<script>
	alert('Details updated successfully.');
	window.location.href='./employee_view.php';
	// $('#loadicon').hide();
		// $('#emp_edit_save_btn').attr('disabled',false);
</script>";
exit();	   
?>