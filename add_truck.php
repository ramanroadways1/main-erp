<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$name = escapeString($conn,strtoupper($_POST['owner_name']));
$mobile = escapeString($conn,($_POST['mobile_no']));
$mobile2 = escapeString($conn,($_POST['mobile_no2']));
$addr = escapeString($conn,strtoupper($_POST['addr']));

$targetPath_Pan = $_SESSION['add_truck_pan_filepath'];
$targetPath_Rc_front = $_SESSION['add_truck_rc_front_filepath'];
$targetPath_Rc_rear = $_SESSION['add_truck_rc_rear_filepath'];
$targetPath_Dec = $_SESSION['add_truck_del_filepath'];

$tno = $_SESSION['add_truck_tno_no'];
$wheeler = $_SESSION['add_truck_tno_wheeler'];
$pan_no = $_SESSION['add_truck_pan_no'];
$pan_holder = $_SESSION['add_truck_pan_holder'];
$dec_timestamp = $_SESSION['add_truck_dec_timestamp'];

$chk_vehicle = Qry($conn,"SELECT id FROM mk_truck WHERE tno='$tno'");
	
if(!$chk_vehicle)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'> 
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#add_owner_button').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_vehicle) > 0)
{
	echo "<script type='text/javascript'> 
		alert('Duplicate vehicle found : $tno !');
		$('#loadicon').fadeOut('slow');
		$('#add_owner_button').attr('disabled',false);
	</script>";
	exit();
}

$compare = compareStrings($name, $pan_holder);

if($compare<50)
{
	echo "<script type='text/javascript'> 
		alert('Owner name not matching with PAN holder !');
		$('#loadicon').fadeOut('slow');
		$('#add_owner_button').attr('disabled',false);
	</script>";
	exit();
}

$pan_target = "truck_pan/$targetPath_Pan";
$rc_front_target = "truck_rc/$targetPath_Rc_front";
$rc_rear_target = "truck_rc/$targetPath_Rc_rear";

if($targetPath_Dec!='')
{
	$dec_target = "truck_dec/$targetPath_Dec";
}
else
{
	$dec_target = "";
}

if(!copy("owner_doc_temp/".$targetPath_Pan,$pan_target)){
	
    echo "<script>
		alert('Error while uploading document : PAN !');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!copy("owner_doc_temp/".$targetPath_Rc_front,$rc_front_target)){
	
    echo "<script>
		alert('Error while uploading document : Rc-front !');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!copy("owner_doc_temp/".$targetPath_Rc_rear,$rc_rear_target)){
	
    echo "<script>
		alert('Error while uploading document : Rc-rear !');
		$('#add_owner_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($targetPath_Dec!='')
{
	if(!copy("owner_doc_temp/".$targetPath_Dec,$dec_target)){
		
		echo "<script>
			alert('Error while uploading document : Declaration !');
			$('#add_owner_button').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

$insert = Qry($conn,"INSERT INTO mk_truck(tno,wheeler,name,mo1,mo2,pan,up1,rc_rear,up4,full,up6,dec_upload_time,branch,branch_user,
timestamp) VALUES ('$tno','$wheeler','$name','$mobile','$mobile2','$pan_no','$rc_front_target','$rc_rear_target',
'$pan_target','$addr','$dec_target','$dec_timestamp','$branch','$branch_sub_user','$timestamp')");
	
$oid = getInsertID($conn);

if(!$insert)
{
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($pan_no=="AAKCS4923M" || $pan_no=="AAMCS2305R")
{
	$insert2 = Qry($conn,"INSERT INTO _by_pass_pod_lock(party_id,pan,is_active,owner_broker,timestamp) VALUES ('$oid','$pan_no','1','OWNER','$timestamp')");

	if(!$insert2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Vehicle : $tno. Added Successfully.');
		window.location.href='all_functions.php';
	</script>";
	
	// echo "<script type='text/javascript'> 
		// alert('Vehicle : $tno. Added Successfully.');
		// $('#loadicon').fadeOut('slow');
		// $('#add_owner_button').attr('disabled',false);
		// $('#close_add_owner').click();
	// </script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#add_owner_button').attr('disabled',false);
	</script>";
	exit();
}
?>