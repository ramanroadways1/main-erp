<form id="DecForm" action="#" method="POST">
<div id="DecModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Declaration
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" id="tno_dec" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Vehicle Owner Name <font color="red"><sup>*</sup></font></label>
				<input id="dec_name" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="name" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input id="dec_mobile" readonly type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Upload Declaration <font color="red"><sup>*</sup></font></label>
				<input id="DecUploadId" type="file" name="dec_copy" accept="image/*" required="required" class="form-control">
			</div>
			
			<div class="form-group col-md-12" id="DecAlert" style="display:none;color:red">Declaration Already Uploaded for this vehicle.</div>
		</div>
      </div>
	  <div id="result_DecForm"></div>
      <div class="modal-footer">
        <button type="submit" disabled="disabled" id="updateDecBtn" class="btn btn-primary">Update</button>
        <button type="button" id="" onclick="$('#DecForm')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#DecForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#updateDecBtn").attr("disabled", true);
	$.ajax({
        	url: "./save_update_declaration.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_DecForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>