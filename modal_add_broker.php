<div id="AddBrokerModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD NEW BROKER
      </div>
      <div class="modal-body">
        <div class="row">
		
	<form id="BrokerFormVerify" action="#" method="POST">	
			
			<div class="form-group col-md-4">
				<label>PAN Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" id="pan_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN()" name="pan_no" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>PAN Copy <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="pan_copy" id="pan_copy_input" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>&nbsp;</label>
				<br />
				<button class="btn btn-success btn-sm" id="broker_verify_button" type="submit" style="color:#000"><span class="glyphicon glyphicon-refresh"></span> Validate PAN</button>
			</div>
	</form>
	
	<form id="BrokerForm" action="#" method="POST">		
			<div class="form-group col-md-12">
			
			<div class="row">
			
			<div class="form-group col-md-3">
				<label>PAN Type <font color="red"><sup>*</sup></font></label>
				<select onchange="PanType1(this.value)" class="form-control" id="pan_type" name="pan_type" class="form-control" required="required">
					<option value="">--select type--</option>
					<option value="Normal">Normal PAN</option>
					<option value="Proprietor">Firm Proprietor PAN</option>
				</select>
			</div>
			
			<script>
			function PanType1(elem)
			{
				var pan_holder = $('#pan_holder_name').val();
				
				if(pan_holder=='')
				{
					alert('Enter PAN number and Upload PAN Copy first !');
					$('#pan_type').val('');
				}
				else
				{
					$('#broker_name1').val('');
					
					if(elem=='Proprietor')
					{
						$('#broker_name1').attr('readonly',false);
					}
					else
					{
						$('#broker_name1').val($('#pan_holder_name').val());
						$('#broker_name1').attr('readonly',false);
					}
				}
			}
			</script>
			
			<div class="form-group col-md-4">
				<label>PAN Holder <font color="red"><sup>*</sup></font></label>
				<input readonly type="text" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" id="pan_holder_name" name="pan_holder" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-5">
				<label>Broker Name <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" id="broker_name1" name="name" class="form-control" required="required">
			</div>
			</div>
			</div>
			
			<div class="form-group col-md-3">
				<label>Mobile No. <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Alternate Mobile No. </label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="mobile2" class="form-control">
			</div>
			
			<div class="form-group col-md-5">
				<label>Address <font color="red"><sup>*</sup></font></label>
				<textarea oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" name="addr" class="form-control" required="required"></textarea>
			</div>
		
		<input type="hidden" name="pan_no_hidden" id="pan_no_hidden">
		
		</div>
      </div>
	  <div id="result_BrokerForm"></div>
      <div class="modal-footer">
        <button type="submit" style="display:none" id="add_broker_button" disabled class="btn btn-sm btn-primary">Submit</button>
        <button type="button" id="close_add_broker" onclick="$('#BrokerForm')[0].reset();" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
	
<script type="text/javascript">
$(document).ready(function (e) {
	$("#BrokerFormVerify").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#broker_verify_button").attr("disabled", true);
	$.ajax({
        	url: "./add_broker_verify.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_BrokerForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>
	
<script type="text/javascript">
$(document).ready(function (e) {
	$("#BrokerForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#add_broker_button").attr("disabled", true);
	$.ajax({
        	url: "./add_broker.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_BrokerForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no").setAttribute("style","background-color:red;color:#FFF");
				// $("#add_broker_button").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no").setAttribute("style","background-color:green;color:#FFF");
				// $("#add_broker_button").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no").setAttribute("style","background-color:white;");
			// $("#add_broker_button").attr("disabled", false);
		}
  }
</script>	