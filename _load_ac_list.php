<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$company = escapeString($conn,($_POST['company']));

$get_ac_list = Qry($conn,"SELECT branch,ac_no,bank_name FROM account_list WHERE company='$company' AND is_active='1' ORDER by ac_no ASC");
if(!$get_ac_list){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<option value=''>--select--</option>";

while($row = fetchArray($get_ac_list))
{
	echo "<option value='$row[branch]_$row[ac_no]_$row[bank_name]'>$row[branch] - $row[bank_name] - $row[ac_no]</option>";
}
 
if(isset($_POST['type']))
{
	echo "<script type='text/javascript'> 
		$('#cr_bank_ac_list').selectpicker('refresh');
		$('#loadicon').hide();
		$('#cr_bank_deposit_button').attr('disabled',false);
	</script>";
}
else
{
	echo "<script type='text/javascript'> 
		$('#dr_bank_ac_list').selectpicker('refresh');
		$('#loadicon').hide();
		$('#dr_bank_deposit_button').attr('disabled',false);
	</script>";
}
?>