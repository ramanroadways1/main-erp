<?php 
require_once 'connection.php';

$id = escapeString($conn,strtoupper($_POST['id'])); 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(empty($id))
{
	echo "<script>
		alert('ID not found !');
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}

$get_data = Qry($conn,"SELECT tno,dl_no,driver_mobile FROM vehicle_placement_req WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data) == 0)
{
	echo "<script>
		alert('Request not found !');
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}

$row = fetchArray($get_data);
	
$chk_vehicle = Qry($conn,"SELECT id,wheeler,mo1 FROM mk_truck WHERE tno='$row[tno]'");

if(!$chk_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_vehicle) == 0)
{
	echo "<script>
		alert('Vehicle not registered. Please add vehicle first !');
		$('#lr_create_btn_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}	

$chk_driver = Qry($conn,"SELECT id,mo1 FROM mk_driver WHERE pan='$row[dl_no]'");

if(!$chk_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_driver) == 0)
{
	echo "<script>
		alert('Driver not registered. Please add driver first !');
		$('#lr_create_btn_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}	

$row_truck = fetchArray($chk_vehicle);
$row_driver = fetchArray($chk_driver);

$_SESSION['verify_vehicle_type'] = "MARKET";
$_SESSION['verify_vehicle'] = $row['tno'];
$_SESSION['verify_vehicle_id'] = $row_truck['id'];
$_SESSION['verify_vehicle_wheeler'] = $row_truck['wheeler'];
$_SESSION['verify_vehicle_mobile_no'] = $row_truck['mo1'];
$_SESSION['vehicle_placement_req_id'] = $id;

$_SESSION['verify_driver'] = $row['dl_no'];
$_SESSION['verify_driver_id'] = $row_driver['id'];
$_SESSION['verify_driver_mobile_no'] = $row_driver['mo1'];

echo "<script>
	window.location.href='./_fetch_lr_entry.php';
</script>";	
exit();
?>