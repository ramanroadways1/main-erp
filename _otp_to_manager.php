<?php
require_once './connection.php';

$to_branch = escapeString($conn,strtoupper($_POST['to_branch']));
$to_company = escapeString($conn,strtoupper($_POST['to_company']));
$amount = escapeString($conn,strtoupper($_POST['amount']));

$qry_fetch=Qry($conn,"SELECT m.emp_code,e.mobile_no FROM manager AS m 
LEFT OUTER JOIN emp_attendance AS e ON e.code=m.emp_code 
WHERE m.branch='$to_branch'");

if(!$qry_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($qry_fetch)==0)
{
	echo "<script>
		alert('Manager not found.');
		window.location.href='./debit.php';
	</script>";
	exit();
}

$row_fetch = fetchArray($qry_fetch);

if($row_fetch['emp_code']=='')
{
	echo "<script>
		alert('Manager not found.');
		window.location.href='./debit.php';
	</script>";
	exit();
}

$mobile = $row_fetch['mobile_no'];

if(strlen($mobile)!='10')
{
	echo "<script>
		alert('Manager\'s mobile number not updated of $to_branch.');
		window.location.href='./debit.php';
	</script>";
	exit();
}

$date = date('Y-m-d');

$otp = rand(100000,999999);

$message="Branch: $branch wants to Credit: $amount/- to Company: $to_company.\nOTP is $otp";

// SendMsgCustom($mobile,$message);
// MsgBranchToBranchTransfer($mobile,$branch,$amount,$to_company,$otp);

$msg_wa = SendWAMsg($conn,$mobile,$message);

if($msg_wa!='1')
{
	echo "<script>
		alert('OTP sent via text msg to : $mobile.');
	</script>";	
	MsgBranchToBranchTransfer($mobile,$branch,$amount,$to_company,$otp);
}

$_SESSION['session_otp'] = $otp;
	
	echo "<script>
			alert('OTP Successfully sent !');
			$('#send_otp_to_manager').attr('disabled',true);
			$('#dr_b_to_b_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";	
	
?>