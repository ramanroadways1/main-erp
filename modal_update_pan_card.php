<?php
if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
  $url_of_update_pan = $protocol."://".$_SERVER['HTTP_HOST']."/b5aY6EZzK52NA8F/save_update_pan_card.php";
?>

<button id="UploadPanModal" onclick="LoadPanDetails()" type="button" style="display:none"></button>

<button style="display:none" id="UploadPanModalButton" data-toggle="modal" data-target="#UpdatePanModal"></button>

<form id="UpdatePanForm" action="#" method="POST">

<div id="UpdatePanModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update <span style="" id="party_title_head"></span> PAN Card
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Party Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="pan_party_name" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>PAN Card Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateNewPan()" onblur="ValidateNewPan()" name="pan_no" id="party_pan_new" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>PAN Card Copy <font color="red"><sup>*</sup></font></label>
				<input type="file" accept="image/*" name="pan_copy" class="form-control" required="required">
			</div>
			
			<?php
			if(basename($_SERVER['PHP_SELF'])=="pay_balance.php")
			{
				$vou_no_for_pan_new = $vou_no;
			}
			else
			{
				$vou_no_for_pan_new = $idmemo;
			}
			?>
			
			<input type="hidden" name="page_name" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
			<input type="hidden" name="vou_no" value="<?php echo $vou_no_for_pan_new; ?>">
			<input type="hidden" id="pan_party_id" name="id">
			<input type="hidden" id="pan_party_type" name="type">
		</div>
      </div>
	  <div id="PanUpdateResult"></div>
      <div class="modal-footer">
			<button type="submit" id="pan_update_button" class="btn btn-success">Update PAN</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

<script>
function ValidateNewPan() { 
  var Obj = document.getElementById("party_pan_new");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
      document.getElementById("party_pan_new").setAttribute("style","background-color:red;color:#fff;");
	document.getElementById("pan_update_button").setAttribute("disabled",true);
    Obj.focus();
                return false;
            }
          else
            {
              document.getElementById("party_pan_new").setAttribute("style","background-color:green;color:#fff;");
			  document.getElementById("pan_update_button").removeAttribute("disabled");
          }
        }
		else
		{
			document.getElementById("pan_update_button").removeAttribute("disabled");
		}
  } 			
</script>	

<script type="text/javascript">
$(document).ready(function (e) {
	$("#UpdatePanForm").on('submit',(function(e) {
	e.preventDefault();
	$('#loadicon').show();
	$('#pan_update_button').attr('disabled',true);
		$.ajax({
        	url: '<?php echo $url_of_update_pan; ?>',
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#PanUpdateResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function LoadPanDetails()
{
	if($('#pan_party_type').val()=='OWNER')
	{
		$('#party_title_head').html('Owner\'s');
	}
	else
	{
		$('#party_title_head').html('Broker\'s');
	}
	
	document.getElementById('UploadPanModalButton').click();	
}
</script>

<form action="smemo2.php" id="reloadAdvForm" method="POST">
	<input type="hidden" value="<?php echo $idmemo; ?>" name="idmemo" />
	<input type="hidden" value="FM" name="key" />
</form>