<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid"> 
<div class="form-group col-md-12">
<br />	
	<div class="row">	
		<div class="form-group col-md-4">
			<h4>Pending Asset Request : <font color="blue"><?php echo $branch; ?></font> </h4> 
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>Req.Id</th>
					<th>New/Old</th>
					<th>General/specific</th>
					<th>Asset holder</th>
					<th>Asset/Vehicle</th>
					<th>Category</th>
					<th>Asset Company & Model</th>
					<th>Wheeler</th>
					<th>Amount</th>
					<th>GST amt</th>
					<th>Total amt</th>
					<th>Approval status</th>
					<th>Reject reason</th>
					<th>Edit</th>
				</tr>	
				
<?php
$get_pending_req = Qry($conn,"SELECT v.id,v.vehicle_holder as asset_holder,v.reg_no as req_id,v.asset_type as typeof,'' as category,
v.veh_type as wheeler,v.maker,v.model,v.amount,v.gst_amount,v.total_amount,v.reject,v.reject_reason,v.add_type,v.ho_approval,v.active,e.name as asset_holder1 
FROM asset_vehicle AS v 
LEFT OUTER JOIN emp_attendance AS e ON e.code=v.vehicle_holder 
WHERE v.branch='$branch' AND v.ho_approval!='1'");

if(!$get_pending_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_pending_req2 = Qry($conn,"SELECT v.id,v.req_code as req_id,v.asset_company as maker,v.asset_model as model,v.typeof,
v.holder as asset_holder,'' as wheeler,v.amount,v.gst_amount,v.total_amount,v.reject,v.reject_reason,v.add_type,v.ho_approval,v.active,e.name as asset_holder1,
c.title as category 
FROM asset_main AS v 
LEFT OUTER JOIN emp_attendance AS e ON e.code=v.holder 
LEFT OUTER JOIN asset_category AS c ON c.id=v.category 
WHERE v.branch='$branch' AND v.ho_approval!='1'");

if(!$get_pending_req2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_pending_req)>0)
{
	$sn=1;
	
	while($row = fetchArray($get_pending_req))
	{
		if($row['asset_holder']==$branch || $row['asset_holder']==''){
			$asset_holder = $branch;
		}
		else{
			$asset_holder = $row['asset_holder1'];
		}
		
		if($row['reject']=="1"){
			$reject_status = "<font color='red'>Rejected</font>";
			$reject_reason = "<font color='red'>$row[reject_reason]</font>";
		}
		else{
			$reject_status = "<font color='red'>HO pending</font>";
			$reject_reason = "";
		}
		
		if($row['add_type']=="1"){
			$new_old = "<font color='red'>NEW</font>";
		}
		else if($row['add_type']=="2"){
			$new_old = "<font color='blue'>OLD</font>";
		}
		
	echo "<tr>
			<td>$row[req_id]</td>
			<td>$new_old</td>
			<td>$row[typeof]</td>
			<td>$asset_holder</td>
			<td>Vehicle</td>
			<td>$row[category]</td>
			<td>$row[maker]<br>$row[model]</td>
			<td>$row[wheeler]</td>
			<td>$row[amount]</td>
			<td>$row[gst_amount]</td>
			<td>$row[total_amount]</td>
			<td>$reject_status</td>
			<td>$reject_reason</td>
			<td><button type='button' disabled id='edit_btn$row[id]' onclick=EditAsset('$row[id]','VEHICLE') class='btn btn-xs btn-primary'>Edit</button></td>
		</tr>";
	
	$sn++;	
	}
}

if(numRows($get_pending_req2)>0)
{
	$sn=1;
	
	while($row = fetchArray($get_pending_req2))
	{
		if($row['asset_holder']==$branch || $row['asset_holder']==''){
			$asset_holder = $branch;
		}
		else{
			$asset_holder = $row['asset_holder1'];
		}
		
		if($row['reject']=="1"){
			$reject_status = "<font color='red'>Rejected</font>";
			$reject_reason = "<font color='red'>$row[reject_reason]</font>";
		}
		else{
			$reject_status = "<font color='red'>HO pending</font>";
			$reject_reason = "";
		}
		
		if($row['add_type']=="1"){
			$new_old = "<font color='red'>NEW</font>";
		}
		else if($row['add_type']=="2"){
			$new_old = "<font color='blue'>OLD</font>";
		}
		
	echo "<tr>
			<td>$row[req_id]</td>
			<td>$new_old</td>
			<td>$row[typeof]</td>
			<td>$asset_holder</td>
			<td>Vehicle</td>
			<td>$row[category]</td>
			<td>$row[maker]<br>$row[model]</td>
			<td>$row[wheeler]</td>
			<td>$row[amount]</td>
			<td>$row[gst_amount]</td>
			<td>$row[total_amount]</td>
			<td>$reject_status</td>
			<td>$reject_reason</td>
			<td><button type='button' disabled id='edit_btn$row[id]' onclick=EditAsset('$row[id]','ASSET') class='btn btn-xs btn-primary'>Edit</button></td>
		</tr>";
	
	$sn++;	
	}
}

			?>			
			</table>
		</div>
		
	</div>
</div>
</div>

<script>
function EditAsset(id,type)
{
	$('#edit_btn'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "./modal_edit_asset.php",
	data: 'type=' + type + '&id=' + id,
	type: "POST",
	success: function(data){
		$("#func_result").html(data);
		jQuery.noConflict();
		$('#EditAssetModel').modal(); 
		$('#loadicon').hide(); 
	},
	error: function() {}
	});
}
</script>

<div id="func_result"></div>
