<?php
require_once 'connection.php';
?>	
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>
<link href="./google_font.css" rel="stylesheet">
	
<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CreditForm").on('submit',(function(e) {
$('#loadicon').show();	
e.preventDefault();
$("#button_sub").attr("disabled",true);	
$.ajax({
	url: "./save_credit_other.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
{
	$("#function_result").html(data);
},
	error: function() 
	{} });}));});
</script>

<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
label{
	color:#FFF;
}
</style>

</head>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	<div class="col-md-2">
		<?php include 'sidebar.php';?>
	</div>
	
<form autocomplete="off" id="CreditForm">	
	
	<div class="col-md-6 col-md-offset-2 col-sm-offset-2">
	<br />
	<br />
		<div class="row">
		
	<div class="col-md-12">				
		<center><h4 style="color:#FFF">Credit - Others</h4></center>
		<br />
		<br />
	</div>

	<div class="form-group col-md-6">
		<label>Amount <font color="red">*</font></label>
		<input type="number" placeholder="Enter Amount" name="amount" min="1" id="amount" class="form-control" required />	
	</div>

	<div class="form-group col-md-6">
		<label>Company <font color="red">*</font></label>
		<select class="form-control" id="company" name="company" required="required">
			<option value="">--Select Company--</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div class="form-group col-md-6">
		<label>Narration <font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'');" id="narration" name="narration" class="form-control" required="required"></textarea>	
	</div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning pull-right" 
		name="submit" value="Credit Others" />
	</div>
	
</div>

</div>
</div>
</form>

<div id="function_result"></div>
</body>
</html>