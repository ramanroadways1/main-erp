<?php
require_once 'connection.php';
 
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$to_id = escapeString($conn,($_POST['to_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));
$elem = escapeString($conn,($_POST['elem']));

$chk_data = Qry($conn,"SELECT id FROM address_book_consignee WHERE to_id='$to_id' AND consignee='$con2_id'");

if(!$chk_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_data)>0)
{
	echo "<script>
			alert('Unloading point details already updated for this selection !');
			$('input:radio').removeAttr('checked');
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if($elem=='0')
{
	echo "<script>
		$('.gps_div').hide();
			$('.google_div').show();
			
			$('#owo_tno').attr('required',false);
			$('#visit_date').attr('required',false);
			$('#loading_point').attr('required',false);
			
			$('#search_loading_point').attr('required',true);
			$('#loading_pincode').attr('required',true);
			$('#loadicon').fadeOut('slow');
		</script>";
}
else
{
	echo "<script>
		$('.gps_div').show();
			$('.google_div').hide();
			
			$('#owo_tno').attr('required',true);
			$('#visit_date').attr('required',true);
			$('#loading_point').attr('required',true);
			
			$('#search_loading_point').attr('required',false);
			$('#loading_pincode').attr('required',false);
			$('#loadicon').fadeOut('slow');
	</script>";
}
?>