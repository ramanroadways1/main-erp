<?php
require_once './connection.php'; 

$branch=escapeString($conn,strtoupper($_SESSION['user']));
$tno=escapeString($conn,strtoupper($_POST['tno']));
$date = date('Y-m-d');
$timestamp = date('Y-m-d H:i:s');

$chk_copy = Qry($conn,"SELECT up6 as dec1 FROM mk_truck WHERE tno='$tno'");

if(numRows($chk_copy)==0)
{
	echo "<script type='text/javascript'>
		alert('No record found.'); 
		window.location.href='./all_functions.php';
	</script>";
	exit();	
}

$row = fetchArray($chk_copy);
	
if($row['dec1']!='')
{	
	echo "<script type='text/javascript'>
		alert('Declaration already updated for vehicle : $tno.'); 
		$('#updateDecBtn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$sourcePath = $_FILES['dec_copy']['tmp_name'];
$fix_name = mt_rand().date('dmYHis');
$targetPath="truck_dec/".$fix_name.".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);

ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	// Redirect("Unable to Upload Declaration Copy.","./all_functions.php");
	exit();
}
else
{
	$update = Qry($conn,"UPDATE mk_truck SET up6='$targetPath',dec_upload_time='$timestamp',tds='0' WHERE tno='$tno'");
	
	if(!$update)
	{
		unlink($targetPath);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	echo "<script type='text/javascript'> 
		alert('Declaration Updated Successfully.');
		window.location.href='./all_functions.php';
	</script>";
	exit();
}
?>