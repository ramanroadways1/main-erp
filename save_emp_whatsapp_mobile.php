<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$sel_option = escapeString($conn,($_POST['sel_option']));
$other_mobile = escapeString($conn,($_POST['other_mobile']));
$id = escapeString($conn,($_POST['id']));

if($sel_option=="OTHER")
{
	if(strlen($other_mobile)!=10)
	{
		echo "<script>
			alert('Please enter a valid mobile number !');
			$('#loadicon').fadeOut('slow');
			$('#whatsapp_mobile_save_btn_$id').attr('disabled',false);
		</script>";
		exit();
	}
	
	$mobile = $other_mobile;
}
else
{
	$mobile = $sel_option;
}

	if(strlen($mobile)!=10)
	{
		echo "<script>
			alert('Please select a valid mobile number !');
			$('#loadicon').fadeOut('slow');
			$('#whatsapp_mobile_save_btn_$id').attr('disabled',false);
		</script>";
		exit();
	}
	
if($id=="")
{
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

if($sel_option=='OTHER')
{
	$chk_mobile = Qry($conn,"SELECT name,branch FROM emp_attendance WHERE (mobile_no='$mobile' || alternate_mobile='$mobile')");

	if(!$chk_mobile){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($chk_mobile)>0)
	{
		$row_chk_mob = fetchArray($chk_mobile);
		
		if($row_chk_mob['branch']!='')
		{
			$emp_name1 = $row_chk_mob["name"]." => $row_chk_mob[branch] Branch.";
		}
		else
		{
			$emp_name1 = $row_chk_mob["name"];
		}

		echo "<script>
			alert('Duplicate mobile number. Mobile number belongs to : $emp_name1 !');
			$('#loadicon').fadeOut('slow');
			$('#whatsapp_mobile_save_btn_$id').attr('disabled',false);
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

$update_whatsApp_mobile = Qry($conn,"UPDATE emp_attendance SET whatsapp_mobile='$mobile' WHERE id='$id'");

if(!$update_whatsApp_mobile){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES ('$id','Emp_Mobile_Update',
'WhatsApp_Mobile_Update','Updated to $mobile','$branch','$branch_sub_user','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Update success !');
		$('#whatsapp_mobile_col_$id').html('$mobile');
		$('#loadicon').fadeOut('slow');
		$('#whatsapp_mobile_save_btn_$id').attr('disabled',true);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error !');
		$('#loadicon').fadeOut('slow');
		$('#whatsapp_mobile_save_btn_$id').attr('disabled',false);
	</script>";
	exit();
}
?>