<?php
require_once 'connection.php';

$timestamp = date("Y-m-d H:i:s");

$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$id = escapeString($conn,strtoupper($_POST['id']));
$start_date = escapeString($conn,strtoupper($_POST['start_date']));
$end_date = escapeString($conn,strtoupper($_POST['end_date']));

$days = round((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24));
if($days<=170)
{
	echo "<script>
		alert('Error : Please Check PUC Dates.');
		$('#puc_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$verify = Qry($conn,"SELECT token_no,reg_no,branch,puc FROM asset_vehicle WHERE id='$id'");
if(!$verify){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($verify)==0)
{
	echo "<script>
		alert('Vehicle not found.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$row_veh = fetchArray($verify);

if($row_veh['reg_no']==''){
	$veh_no_db = $row_veh['token_no'];
}
else{
	$veh_no_db = $row_veh['reg_no'];
}

if($veh_no_db!=$veh_no)
{
	echo "<script>
		alert('Vehicle not verified.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['branch']!=$branch)
{
	echo "<script>
		alert('Vehicle not belongs to your branch.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['puc']=="1")
{
	echo "<script>
		alert('PUC COPY already updated of this vehicle.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	if(!in_array($_FILES['puc_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed. Insurance Copy');
			$('#puc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
$sourcePath = $_FILES['puc_copy']['tmp_name'];

	$targetPath = "asset_upload/puc/".date('dmYHis').mt_rand().".".pathinfo($_FILES['puc_copy']['name'],PATHINFO_EXTENSION);
	
	ImageUpload(1000,700,$sourcePath);
	
	if(!move_uploaded_file($sourcePath, $targetPath)) 
	{
		echo "<script>
			alert('Error : Unable to upload PUC Copy.');
			$('#puc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
StartCommit($conn);
$flag = true;

$update1 = Qry($conn,"UPDATE asset_vehicle SET puc='1' WHERE id='$id'");

if(!$update1){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update2 = Qry($conn,"UPDATE asset_vehicle_docs SET puc_copy='$targetPath',puc_start='$start_date',puc_end='$end_date',puc_time='$timestamp' 
WHERE veh_id='$id'");

if(!$update2){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
			alert('PUC Updated Successfully !');
			window.location.href='asset_vehicle_view.php';
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>