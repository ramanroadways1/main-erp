<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

	$amount = strtoupper(escapeString($conn,$_POST['amount']));
	$company = strtoupper(escapeString($conn,$_POST['company']));
	$narration = strtoupper(escapeString($conn,$_POST['narration']));
	
if($company=='')
{
	echo '<script>
		alert("Error : Company not found !");
		$("#loadicon").hide();
		$("#cr_others_button").attr("disabled", false);
	</script>';
	exit();
}	
	
if($branch=='')
{
	echo '<script>
		alert("Error : Branch not found !");
		window.location.href="/";
	</script>';
	exit();
}	
	
if($amount<=0)
{
	echo '<script>
		alert("Error : Invalid amount entered !");
		$("#loadicon").hide();
		$("#cr_others_button").attr("disabled", false);
	</script>';
	exit();
}	
	
StartCommit($conn);
$flag = true;

	$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	if(!$fetch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
     $row_bal = fetchArray($fetch_bal);
     $rrpl_cash = $row_bal['balance'];
     $rr_cash = $row_bal['balance2'];
		
	if($company=='RRPL') 
	{
		$credit_col="credit";
		$balance_col="balance";
		$newbal =  $amount + $rrpl_cash;
	}	
	else
	{
		$credit_col="credit2";
		$balance_col="balance2";
		$newbal =  $amount + $rr_cash;
	}
	
	$update_balance = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook = Qry($conn,"INSERT INTO cashbook(user_code,vou_type,`$credit_col`,desct,user,`$balance_col`,comp,date,timestamp) VALUES 
		('$_SESSION[user_code]','CREDIT-OTHER','$amount','$narration','$branch','$newbal','$company','$date','$timestamp')");
		
	if(!$insert_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$cash_id = getInsertID($conn);
	
$insert_credit = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,branch,branch_user,company,amount,narr,date,timestamp) VALUES 
('CASH','$cash_id','OTHER','$branch','$_SESSION[user_code]','$company','$amount','$narration','$date','$timestamp')");		
	
	if(!$insert_credit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Amount Credited Successfully !!');
		window.location.href='./credit_other.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit_other.php");
	exit();
}
?>