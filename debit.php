<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<style>
label{font-size:13px}
</style>

<link href="css/styles.css" rel="stylesheet">

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">

<br />

	<div class="row">
		<div class="form-group col-md-12">
			<h3 style="color:#FFF">Debit : </h3>
		</div>
	</div>

<br />

		<div class="row">
		
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#DrB2BModal">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/b_to_b.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">Branch to Branch</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#DrITRModal">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/itr.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px;padding-top:6px;">Comp. to Comp.</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#DrHOModal">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/ho.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px;padding-top:6px;">Head-Office</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#BankDebitModal">
				<div class="panel panel-red panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/neft.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">Cash Deposit to bank</div>
						</div>
					</div>
				</div>
			</div>	
                        		
</div>
</div>

<?php
include("./modal_dr_branch_to_branch.php");
include("./modal_dr_company_to_company.php");
include("./modal_dr_ho.php");
include("./modal_dr_bank_deposit.php");

include("./_footer.php");
?>