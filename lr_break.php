<?php
require_once 'connection.php';

// echo "<script>
		// alert('Temporary on hold.');
		// window.location.href='./';
	// </script>";
	// exit();
	
$check = Qry($conn,"SELECT id FROM break_branch WHERE branch='$branch'");
if(!$check){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check)==0)
{
	echo "<script>
		alert('Invalid Entry..');
		window.location.href='./logout.php';
	</script>";
	exit();	
}

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">

<div class="row">
	
<div class="form-group col-md-1"></div>
	
<div class="form-group col-md-10">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">LR - Breaking</h4></center>
		
	<button class="btn btn-danger pull-right" type="button" data-toggle="modal" data-target="#BreakingLR">SELECT LR HERE</button>
	</div>

		<table class="table table-bordered table-striped" style="font-size:12px;">								   
	<tr>
		<th>#</th>
		<th>LR No</th>
		<th>Date</th>
		<th>Actual Wt</th>
		<th>Chrg Wt</th>
	</tr>
	
<?php
$fetch_lrs = Qry($conn,"SELECT id,lrno,act_wt,chrg_wt,date FROM lr_break WHERE crossing='' AND branch='$branch' ORDER by lrno");
if(!$fetch_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_lrs)>0)
{
	$i1=1;

	while($row = fetchArray($fetch_lrs))
    {

		echo "<tr>
				<td>$i1</td>
				<td>$row[lrno]</td>
				<td>".convertDate("d-m-y",$row["date"])."</td>
				<td>$row[act_wt]</td>
				<td>$row[chrg_wt]</td>
			</tr>
		";
		$i1++;
	}	
}
else
{
	echo '<tr><td colspan="7"><b>Records not found..!</b></td></tr>';
}
echo "</table>";
?>

	</div>
	
</div>

</div>
</div>
</body>
</html>

<?php include("./modal_lr_break.php"); ?>