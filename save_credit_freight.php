<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

	$mode = strtoupper(escapeString($conn,$_POST['select_id']));
	$tno = strtoupper(escapeString($conn,$_POST['tno']));
	$bilty_type = strtoupper(escapeString($conn,$_POST['bilty_type']));
	$bilty_no = strtoupper(escapeString($conn,$_POST['bilty_no']));
	$lrno = strtoupper(escapeString($conn,$_POST['lr_no']));
	$lr_date = strtoupper(escapeString($conn,$_POST['lr_date']));
	$amount = strtoupper(escapeString($conn,$_POST['amount']));
	$cheque_no = strtoupper(escapeString($conn,$_POST['chqno']));
	$narration = strtoupper(escapeString($conn,$_POST['narration']));
	$from_loc = strtoupper(escapeString($conn,$_POST['from_loc']));
	$to_loc = strtoupper(escapeString($conn,$_POST['to_loc']));
	$freight_type = strtoupper(escapeString($conn,$_POST['freight_type']));
	
	$fetch_comp = Qry($conn,"SELECT comp FROM dairy.own_truck WHERE tno='$tno'");
	if(!$fetch_comp){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		Redirect("Error while processing Request","./");
		exit();
	}
	$row_company =fetchArray($fetch_comp);
	$company = $row_company['comp'];
	
	if($company=='')
	{
		echo "<script>
			alert('Unable to fetch Company.');
			window.location.href='./credit.php';
		</script>";
		exit();	
	}
	
	
$nrr = "LR No: ".$lrno.", LR_Date: ".$lr_date.", TruckNo: ".$tno.", Narration: ".$narration;

if($company=='RRPL') 
{
	$credit_col="credit";
	$balance_col="balance";
}	
else
{
	$credit_col="credit2";
	$balance_col="balance2";
}
	
StartCommit($conn);
$flag = true;
	
if($mode=='CASH')
{
	$fetch_bal = Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	if(!$fetch_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
     $row_bal = fetchArray($fetch_bal);
     $rrpl_cash = $row_bal['balance'];
     $rr_cash = $row_bal['balance2'];
		
	if($company=='RRPL') {
		$newbal =  $amount + $rrpl_cash;
	}	
	else{
		$newbal =  $amount + $rr_cash;
	}
	
	$update_balance = Qry($conn,"update user set `$balance_col`='$newbal' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$insert_cashbook = Qry($conn,"INSERT INTO cashbook(user_code,vou_type,`$credit_col`,desct,user,`$balance_col`,comp,date,timestamp) VALUES 
		('$_SESSION[user_code]','CREDIT-FREIGHT','$amount','$nrr','$branch','$newbal','$company','$date','$timestamp')");
		
	if(!$insert_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$cash_id = getInsertID($conn);
}
else if($mode=='CHEQUE')
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,date,comp,vou_type,desct,chq_no,`$credit_col`,timestamp) VALUES 
		('$branch','$date','$company','CREDIT-FREIGHT','$nrr','$cheque_no','$amount','$timestamp')");	
		
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	$cash_id = "0";
}	

if($bilty_type=='MARKET')
{
	$bilty_no1=$bilty_no;
}
else
{
	$bilty_no1=$lrno;
}

	$insert_credit = Qry($conn,"INSERT INTO credit(credit_by,cash_id,section,tno,lr_date,bilty_type,ediary_credit,from_stn,to_stn,branch,
	branch_user,company,bilty_no,amount,chq_no,narr,date,timestamp) VALUES ('$mode','$cash_id','FREIGHT','$tno','$lr_date','$bilty_type','2',
	'$from_loc','$to_loc','$branch','$_SESSION[user_code]','$company','$bilty_no1','$amount','$cheque_no','$narration','$date','$timestamp')");		
	
	if(!$insert_credit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
if($bilty_type=='MARKET')
{
	$sel_bilty_book = Qry($conn,"SELECT balance FROM dairy.bilty_book WHERE bilty_no='$bilty_no' ORDER BY id DESC LIMIT 1");
	if(!$sel_bilty_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	
	if(numRows($sel_bilty_book)>0)
	{
		$row_book = fetchArray($sel_bilty_book);
		$old_balance_bilty = $row_book['balance'];
		
		$new_balance=$old_balance_bilty-$amount;
		
		$insert_into_bilty_book=Qry($conn,"INSERT INTO dairy.bilty_book (bilty_no,branch,branch_user,date,trans_type,advbal,debit,balance,timestamp) VALUES 
		('$bilty_no','$branch','$_SESSION[user_code]','$date','$mode','$freight_type','$amount','$new_balance','$timestamp')");
		
		if(!$insert_into_bilty_book){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
		
		$update_bilty_balance = Qry($conn,"UPDATE dairy.bilty_balance SET rcvd=rcvd+'$amount',balance='$new_balance' WHERE bilty_no='$bilty_no'");
		
		if(!$update_bilty_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		}
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Freight Credited Successfully !!');
		window.location.href='./credit.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./credit.php");
	exit();
}
?>