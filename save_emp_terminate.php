<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT name,code,branch,status FROM emp_attendance WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_data = fetchArray($get_data);

$chk_manager = Qry($conn,"SELECT id FROM manager WHERE emp_code='$row_data[code]'");
if(!$chk_manager){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_manager)>0)
{
	echo "<script>
		alert('Seleted Employee is manager of your branch. You Can\'t Terminate !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your branch !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}

if($row_data['status']=="2")
{	
	echo "<script>
		alert('Employee Transfer to another Branch. You Can\'t perform any action !');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}
	
if($row_data['status']!="3")
{	
	echo "<script>
		alert('Employee is not active. You can not terminate !');
		$('#loadicon').hide();
		$('#terminate_button$id').attr('disabled',false);
	</script>";
	exit();
}

$ChkManager = Qry($conn,"SELECT id FROM manager WHERE emp_code='$row_data[code]'");
if(!$ChkManager){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($ChkManager)>0)
{
	echo "<script>
		alert('Employee role is manager. Please change manager first !');
		$('#loadicon').hide();
		$('#terminate_button$id').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_termination = Qry($conn,"INSERT INTO emp_terminated(code,branch,user_code,timestamp) VALUES 
('$row_data[code]','$row_data[branch]','$branch_sub_user','$timestamp')");

if(!$insert_termination){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update = Qry($conn,"UPDATE emp_attendance SET active_login='0',status='-1',terminate='0' WHERE id='$id'");
if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// $chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
// if(!$chk_Today_attd){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }

// if(numRows($chk_Today_attd)>0)
// {
	// $row_attd = fetchArray($chk_Today_attd);
	
	// $update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total-1 WHERE id='$row_attd[id]'");
	// if(!$update_today_data){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
// }

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Termination of $row_data[name]. Initiated Successfully. Manager Approval required to confirm termination..');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_view.php");
	exit();
}
?>