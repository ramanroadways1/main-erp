<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$token = escapeString($conn,strtoupper($_POST['token']));

$GetReq = Qry($conn,"SELECT a.party_id,a.branch,a.veh_type,a.maker_name,a.model_name,a.payment_mode,a.amount,a.gst_invoice,a.gst_type,a.gst_value,
a.gst_amount,a.discount,a.payment_amount,a.approval,a.ho_approval,p.legal_name as party_name,p.gst_no,p.ac_holder,p.ac_no,p.bank_name,p.ifsc_code 
FROM asset_vehicle_req AS a
LEFT OUTER JOIN asset_party AS p ON p.id=party_id 
WHERE a.req_code='$token'");

if(!$GetReq){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./asset_vehicle_view.php");
	exit();
}

if(numRows($GetReq)==0)
{
	echo "<script>
		alert('Invalid Token Number !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($GetReq);

if($row['branch']!=$branch){
	echo "<script>
		alert('Token does not belongs to your branch !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['approval']==0 || $row['ho_approval']==0){
	
	if($row['approval']==0){
		$text="Manager";
	}
	else{
		$text="Head-Office";
	}
	
	echo "<script>
		alert('$text\'s Approval Pending !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

	echo "<script>
		$('#token_no_div').hide();
		$('#token_button_div').hide();
		$('#main_div').show();
		$('#tokenButton').attr('disabled',true);
		$('#btn_asset_add').attr('disabled',true);
		
		$('#d_veh_type').val('$row[veh_type]');
		// $('#d_maker_name').val('$row[maker_name]');
		// $('#d_model').val('$row[model_name]');
		$('#d_payment_mode').val('$row[payment_mode]');
	</script>";
	

	echo "<script>
			$('#amount_add_new_asset_modal').val('$row[amount]');
			$('#gst_selection_modal').val('$row[gst_invoice]');
			$('#gst_type').val('$row[gst_type]');
			$('#gst_value').val('$row[gst_value]');
			$('#gst_amount').val('$row[gst_amount]');
			$('#discount_amount').val('$row[discount]');
			
			$('#total_amount').val('$row[payment_amount]');
			$('#new_veh_add_modal_maker_name').val('$row[maker_name]');
			$('#new_veh_add_modal_model_name').val('$row[model_name]');
			
			$('#party_name').val('$row[party_name]');
			$('#partyId2').val('$row[party_id]');
			$('#loadicon').hide();
		</script>";
	exit();
?>