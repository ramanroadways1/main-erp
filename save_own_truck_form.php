<?php
require_once 'connection.php';

// echo "<script>
		// alert('Voucher Creation Temporary on hold till 1:40 PM.');
		// window.location.href='./';
	// </script>";
	// exit();
	
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(!isset($_SESSION['voucher_olr']))
{
	echo "<script>
		window.location.href='smemo_own.php';
	</script>";
	exit();
}

$vou_no = escapeString($conn,$_SESSION['voucher_olr']);

$delete_cache_ewb_old = Qry($conn,"DELETE FROM lr_pre_ewb WHERE date(timestamp)<'$date'");

if(!$delete_cache_ewb_old){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_cache = Qry($conn,"SELECT id FROM lr_pre WHERE frno='$vou_no'");

if(!$get_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_cache)==0)
{
	echo "<script>
		alert('No record found !');
		window.location.href='./smemo_own.php';
	</script>";
	exit();
}

$oxy_lr = "NO";

$chk_oxygen_lr = Qry($conn,"SELECT lr_id,to_id,to_id_lr FROM lr_pre WHERE frno='$vou_no' AND oxygen_lr='1'");

if(!$chk_oxygen_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_oxygen_lr)>0)
{
	if(numRows($get_cache)>1)
	{ 
		errorLog("Multiple LRs found in Oxygen LR Own-Truck Form. Frno: $vou_id.",$conn,$page_name,__LINE__);
		Redirect("Mulitple LRs found in Oxygen Vehicle. Delete unwanter LRs first !","./");
		exit();
	}
	
	$row_oxygn = fetchArray($chk_oxygen_lr);
	
	if($row_oxygn['to_id']==$row_oxygn['to_id_lr'])
	{
		echo "<script>
			alert('Error: Change destination first !');
			window.location.href='./smemo_own.php';
		</script>";
		exit();
	}
		
	$oxy_lr="YES";
}
	
StartCommit($conn);
$flag = true;

$lrcopy = Qry($conn,"INSERT INTO freight_form_lr(frno,company,branch,branch_user,date,create_date,truck_no,lrno,mother_lr_id,fstation,
tstation,tstation_bak,consignor,consignee,from_id,to_id,to_id_bak,con1_id,con2_id,dest_zone,dest_zone_id,wt12,weight,
crossing,cross_to,cross_stn_id,brk_id,oxygen_lr,timestamp) SELECT frno,company,branch,'$branch_sub_user',date,create_date,truck_no,
lrno,lr_id,fstation,tstation,tstation,consignor,consignee,from_id,to_id,to_id,con1_id,con2_id,dest_zone,dest_zone_id,wt12,weight,
crossing,cross_to,cross_stn_id,brk_id,oxygen_lr,timestamp FROM lr_pre WHERE frno='$vou_no'");

if(!$lrcopy){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$close_breaked_lrs = Qry($conn,"SELECT GROUP_CONCAT(brk_id SEPARATOR ',') as brk_id FROM lr_pre WHERE frno='$vou_no' AND brk_id!=''");
if(!$close_breaked_lrs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($close_breaked_lrs)>0)
{
	$row_breaking_ids = fetchArray($close_breaked_lrs);
	
	if($row_breaking_ids['brk_id']!='')
	{
		$breaking_ids = $row_breaking_ids['brk_id'];
	}
	else
	{
		$breaking_ids = "0";
	}
	
	$close_breaking = Qry($conn,"UPDATE lr_break SET crossing='YES' WHERE id IN($breaking_ids)");
		if(!$close_breaking){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
}
else
{
	$breaking_ids = "0";
}

$update_crossing_data = Qry($conn,"UPDATE lr_sample lr1,lr_pre lr2 SET lr1.crossing = lr2.crossing WHERE lr2.frno='$vou_no' AND 
lr1.id = lr2.lr_id");

// UPDATE lr_sample SET crossing=(SELECT crossing FROM lr_pre WHERE lr_pre.lr_id=lr_sample.id) t1 
        // INNER JOIN lr_pre t2 
        // ON t1.id = t2.lr_id
		// SET t1.crossing = t2.crossing 
		// WHERE t2.frno='$vou_no' AND t1.id = t2.lr_id
		
if(!$update_crossing_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_lr_cache = Qry($conn,"DELETE FROM lr_sample_pending WHERE lrno IN(SELECT lrno FROM lr_pre WHERE frno='$vou_no')");

if(!$delete_lr_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_branch_code = Qry($conn,"SELECT branch_code FROM user WHERE username='$branch'");

if(!$get_branch_code){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_branch_code) == 0)
{
	$flag = false;
	errorLog("Branch not found. $branch.",$conn,$page_name,__LINE__);
}

$row_branch_code = fetchArray($get_branch_code);

if(strlen($row_branch_code['branch_code']) != 3)
{
	$flag = false;
	errorLog("Branch code not found. $branch.",$conn,$page_name,__LINE__);
}
	
$branch2 = strtoupper($row_branch_code['branch_code'])."OLR".date("dmY");
    
$ramanc = Qry($conn,"SELECT frno FROM freight_form_lr where frno like '$branch2%' ORDER BY id DESC LIMIT 1");

if(!$ramanc){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
		$row=fetchArray($ramanc);
		if(numRows($ramanc)>0)
		{
			$count=$row['frno'];
			$newstring = substr($count,14);
			$newstr = ++$newstring;
			$memoid=$branch2.$newstr;
		}
		else
		{
			$count2=1;
			$memoid=$branch2.$count2;
		}
		
// $update_ewb_vou_no = Qry($conn,"UPDATE _eway_bill_validity t1 
// INNER JOIN lr_pre t2 
// ON t1.lrno = t2.lrno
// SET t1.vou_no = '$memoid' 
// WHERE t1.lrno = t2.lrno");

// if(!$update_ewb_vou_no){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }	

// insert duplicate by ewb no

$qry_get_ewb_no = Qry($conn,"SELECT vou_no,ewb_no,lrno FROM _eway_bill_validity WHERE ewb_no in(SELECT ewb_no FROM lr_pre_ewb WHERE frno='$vou_no')");

if(!$qry_get_ewb_no){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if(numRows($qry_get_ewb_no) > 0)
{
	while($row_duplicate_ewb = fetchArray($qry_get_ewb_no))
	{
		$chk_crossing = Qry($conn,"SELECT id FROM lr_pre_ewb WHERE frno='$vou_no' AND lrno='$row_duplicate_ewb[lrno]' AND 
		ewb_no='$row_duplicate_ewb[ewb_no]'");
		
		if(!$chk_crossing){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($chk_crossing) > 0){
			$crossing_flag="YES";
		}
		else{
			$crossing_flag="NO";
		}
		
		$insert_ewb_duplicate_vou = Qry($conn,"INSERT INTO _ewb_multiple_fm(ewb_no,prev_vou,prev_lrno,current_vou,current_lrno,branch,
		branch_user,crossing,timestamp) VALUES ('$row_duplicate_ewb[ewb_no]','$row_duplicate_ewb[vou_no]',
		(SELECT GROUP_CONCAT(lrno SEPARATOR ',') FROM lr_pre_ewb),'$row_duplicate_ewb[lrno])','$memoid','$branch','$branch_sub_user',
		'$crossing_flag','$timestamp')");
		
		if(!$insert_ewb_duplicate_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
	
// insert duplicate by ewb no	

$lr_update=Qry($conn,"UPDATE freight_form_lr SET frno='$memoid' WHERE frno='$vou_no'");
if(!$lr_update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$select_main_LR_Count = Qry($conn,"SELECT COUNT(id) as total_main_lr FROM lr_pre WHERE frno='$vou_no' AND lr_type=0");
if(!$select_main_LR_Count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$rowLRCount = fetchArray($select_main_LR_Count);

$update_lr_count = Qry($conn,"UPDATE _pending_lr SET own_lr=own_lr-'$rowLRCount[total_main_lr]' WHERE branch='$branch'");
if(!$update_lr_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($oxy_lr=="YES")
{
	$get_lr_from_to_ids = Qry($conn,"SELECT from_id,to_id FROM lr_sample WHERE id='$row_oxygn[lr_id]'");
	
	if(!$get_lr_from_to_ids){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$row_lr_main = fetchArray($get_lr_from_to_ids);
	
	$insert_oxygn_olr = Qry($conn,"INSERT INTO oxygen_olr(veh_no,frno,lrno,from_loc,to_loc,from_id,to_id,from_id_main,to_id_main,
	route_type,count,lr_id,branch,branch_user,timestamp) SELECT truck_no,'$memoid',lrno,fstation,tstation,from_id,to_id,'$row_lr_main[from_id]',
	'$row_lr_main[to_id]','By_Road','1',lr_id,'$branch','$_SESSION[user_code]','$timestamp' FROM lr_pre WHERE frno='$vou_no'");
	
	if(!$insert_oxygn_olr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$chk_ewb_cache = Qry($conn,"SELECT id,by_pass FROM lr_pre_ewb WHERE frno='$vou_no'");

if(!$chk_ewb_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_ewb_cache)>0)
{
	while($row_ewb = fetchArray($chk_ewb_cache))
	{
		$insert_ewb_data = Qry($conn,"INSERT INTO _eway_bill_lr_wise(lrno,lr_id,branch,ewb_no,ewb_flag_desc,by_pass,ewb_copy,ewayBillDate,ewb_date,
		ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp) SELECT lrno,lr_id,branch,ewb_no,ewb_flag_desc,
		by_pass,ewb_copy,ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp 
		FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

		if(!$insert_ewb_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$ewb_id = getInsertID($conn);

		$insert_ewb_data2 = Qry($conn,"INSERT INTO _eway_bill_validity(owner_mobile,driver_mobile,vou_no,lrno,truck_no,lr_date,from_loc,to_loc,
		consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,ewb_id,ewb_date,ewb_expiry,ewb_extended,timestamp) SELECT '','','$memoid',lrno,truck_no,lr_date,
		from_loc,to_loc,consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,'$ewb_id',ewb_date,ewb_expiry,ewb_extended,timestamp 
		FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

		if(!$insert_ewb_data2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($row_ewb['by_pass']=="YES")
		{
			$insert_ewb_by_pass = Qry($conn,"INSERT INTO _eway_bill_by_pass(lrno,by_pass_id,lr_id,ewb_no,branch,narration,timestamp) SELECT 
			lrno,by_pass_id,lr_id,ewb_no,branch,by_pass_narration,timestamp FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

			if(!$insert_ewb_by_pass){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

$delete_cache = Qry($conn,"DELETE FROM lr_pre WHERE frno='$vou_no'");
if(!$delete_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_cache_ewb = Qry($conn,"DELETE FROM lr_pre_ewb WHERE frno='$vou_no'");
if(!$delete_cache_ewb){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	unset($_SESSION['voucher_olr']);
	unset($_SESSION['voucher_olr_vehicle']);
	closeConnection($conn);
	echo "<script>
		alert('Voucher: $memoid Successfully Created.');
		window.location.href='./smemo_own.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./smemo_own.php");
	exit();
}
?>