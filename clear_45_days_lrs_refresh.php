<?php
require_once('../_connect.php');

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;	

$empty_table = Qry($conn,"DELETE FROM _pending_lr_list");

if(!$empty_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$copy_pending_lr = Qry($conn,"INSERT INTO _pending_lr_list(vou_id,vou_no,bid,oid,branch,lr_date,create_date,lrno,timestamp) 
SELECT f.id,f.frno,f1.bid,f1.oid,f.branch,f.date,f.create_date,f.lrno,f.timestamp FROM freight_form AS f1 
LEFT OUTER JOIN freight_form_lr AS f ON f.frno=f1.frno 
WHERE f.date>='2019-01-01' and f1.actualf>0 AND f.market_pod_date=0 ORDER BY f.id ASC");

if(!$copy_pending_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$copy_record = Qry($conn,"UPDATE _pending_lr t1 
INNER JOIN (SELECT branch,COALESCE(COUNT(id),0) as count1 FROM _pending_lr_list GROUP BY branch) as t2
  ON t1.branch = t2.branch 
SET t1.lr_pod_pending = t2.count1");

if(!$copy_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$update_zero_pod1 = Qry($conn,"UPDATE _pending_lr SET lr_30_days='0',lr_45_days='0',lr_45_days_not_shivani='0'");

if(!$update_zero_pod1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

// copy 30 days record

$copy_record_30days = Qry($conn,"UPDATE _pending_lr t1 
INNER JOIN (SELECT branch,COALESCE(COUNT(id),0) as count1 FROM _pending_lr_list 
WHERE DATEDIFF('$date',lr_date)>30 GROUP BY branch) as t2
ON t1.branch = t2.branch 
SET t1.lr_30_days = t2.count1");

if(!$copy_record_30days){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

// copy 45 days record

$copy_record_45days = Qry($conn,"UPDATE _pending_lr t1 
INNER JOIN (SELECT branch,COALESCE(COUNT(id),0) as count1 FROM _pending_lr_list 
WHERE DATEDIFF('$date',lr_date)>45 GROUP BY branch) as t2
  ON t1.branch = t2.branch 
SET t1.lr_45_days = t2.count1");

if(!$copy_record_45days){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$copy_record_45days_not_shivani = Qry($conn,"UPDATE _pending_lr t1 
INNER JOIN (SELECT branch,COALESCE(COUNT(id),0) as count1 FROM _pending_lr_list 
WHERE DATEDIFF('$date',lr_date)>45 AND oid NOT IN(SELECT party_id FROM _by_pass_pod_lock WHERE is_active='1') AND 
bid NOT IN(SELECT party_id FROM _by_pass_pod_lock WHERE is_active='1') GROUP BY branch) as t2
ON t1.branch = t2.branch 
SET t1.lr_45_days_not_shivani = t2.count1");

if(!$copy_record_45days_not_shivani){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

$update_zero_pod = Qry($conn,"UPDATE _pending_lr SET lr_30_days='0',lr_45_days='0',lr_45_days_not_shivani='0' WHERE lr_pod_pending=0");

if(!$update_zero_pod){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		Load_pods();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./clear_45_days_lrs.php");
	exit();
}

?>