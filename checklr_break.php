<?php
require_once 'connection.php';

$check = Qry($conn,"SELECT id FROM break_branch WHERE branch='$branch'");
if(!$check){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check)==0)
{
	echo "<script>
		window.location.href='./lr_break.php';
	</script>";
	exit();	
}

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

if($lrno=='')
{
	echo "<script type='text/javascript'>
		$('#lrno1').focus();
		$('#lrno1').val('');
		$('#total_1').val('');
		$('#left_1').val('');
		$('#used_1').val('');
		
		$('#total_2').val('');
		$('#left_2').val('');
		$('#used_2').val('');
		$('#break_lr_add_btn').attr('disabled', true);
		$('#loadicon').hide();
	</script>";		
	exit();
}

$qry=Qry($conn,"SELECT branch,wt12,weight,break,diesel_req,crossing,item_id FROM lr_sample WHERE lrno='$lrno' AND cancel!='1' AND crossing!='NO'");
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script type='text/javascript'>
		alert('LR Not found.')
		$('#lrno1').val('');
		$('#total_1').val('');
		$('#left_1').val('');
		$('#used_1').val('');
		
		$('#total_2').val('');
		$('#left_2').val('');
		$('#used_2').val('');
		$('#break_lr_add_btn').attr('disabled', true);
		$('#loadicon').hide();
	</script>";		
	exit();
}

	$row=fetchArray($qry);
	
	if($row['diesel_req']!=0)
	{
		echo "<script>
			alert('Diesel Requested for this LR. You can not break this LR.');
			window.location.href='./lr_break.php';
		</script>";
		exit();	
	}
	
	if($row['item_id']!="101")
	{
		echo "<script>
			alert('Warning : LR does not belongs to salt.');
			window.location.href='./lr_break.php';
		</script>";
		exit();	
	}
	
	
	$lr_actual=$row['wt12'];
	$lr_charge=$row['weight'];
	
	$lr_branch=$row['branch'];
	$crossing=$row['crossing'];
	
		
	if($crossing=='' AND $branch!='GANDHIDHAM')
	{
		echo "<script>
			alert('LR not crossing enabled. Contact GDM branch.');
			window.location.href='./lr_break.php';
		</script>";
		exit();	
	}
	
	if($lr_branch!='GANDHIDHAM')
	{
		echo "<script>
			alert('LR not belongs to GANDHIDHAM.');
			window.location.href='./lr_break.php';
		</script>";
		exit();	
	}
	

$sum=Qry($conn,"SELECT IFNULL(SUM(act_wt),0) as total_act,IFNULL(SUM(chrg_wt),0) as total_chrg FROM lr_break WHERE lrno='$lrno' AND branch='$branch'");
if(!$sum){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
$row2=fetchArray($sum);

$break_actual=$row2['total_act'];
$break_charge=$row2['total_chrg'];

if($break_actual>=$lr_actual)
{
	echo "<script>
	alert('No weight left for breaking. Breaking already done.');
	$('#lrno1').val('');
		$('#total_1').val('');
		$('#left_1').val('');
		$('#used_1').val('');
		
		$('#total_2').val('');
		$('#left_2').val('');
		$('#used_2').val('');
		$('#break_lr_add_btn').attr('disabled', true);
		$('#loadicon').hide();
	</script>";
exit();
}

$left_actual=$lr_actual-$break_actual;
$left_charge=$lr_charge-$break_charge;

echo "<script>
		$('#total_1').val('".$lr_actual."');
		$('#left_1').val('".$left_actual."');
		$('#used_1').val('".$break_actual."');
		
		$('#total_2').val('".$lr_charge."');
		$('#left_2').val('".$left_charge."');
		$('#used_2').val('".$break_charge."');
		$('#break_lr_add_btn').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();

?>