<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$get_page_id_db = Qry($conn,"SELECT id FROM _page_all WHERE page_name='$actual_link'");
if(!$get_page_id_db){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($get_page_id_db)>0)
{
	$row_page_id = fetchArray($get_page_id_db);
	
	$check_access = Qry($conn,"SELECT id FROM _user_access WHERE role='$_SESSION[user_role]' AND FIND_IN_SET($row_page_id[id],page_id)");
	if(!$check_access){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}

	if(numRows($check_access)>0)
	{
		?>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo $baseUrl1 ?>font-awesome-4.7.0/css/font-awesome.min.css">
		<link href="<?php echo $baseUrl1 ?>google_font.css" rel="stylesheet">
		<br />
		<br />
		<br />
		<div class="container" style="font-family: 'Open Sans', sans-serif !important">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="bg-warning col-md-12" style="font-size:20px">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> &nbsp; Unauthorized Access.
					</div>				
				</div>
			</div>
		</div>
		<?php
		exit();
	}
}
?>