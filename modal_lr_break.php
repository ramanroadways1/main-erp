<form id="BreakingLRForm" action="#" method="POST">
<div id="BreakingLR" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Break LR
      </div>
	  
<script>	  
function VerifyLR(lrno){
	if(lrno!='')
	{	
		$("#loadicon").show();
		jQuery.ajax({
			url: "./checklr_break.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data) {
				$("#result_BreakingLRForm").html(data);
				$("#loadicon").hide();
			},
			error: function() {}
		});
	}
}
</script>	  
	  
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-6">
				<label>Enter LR No.<font color="red"><sup>*</sup></font></label>
				<input onblur="VerifyLR(this.value)" type="text" id="lrno1" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12" style="background:gray">
				<label style="padding-top:3px;color:#FFF">Actual Weight :</label>
			</div>	  
		
		<div class="form-group col-md-2">
			<label>Total </label>                               
			<input name="total_1" type="number" id="total_1" step="any" class="form-control" required readonly />
		</div>

		<div class="form-group col-md-2">
			<label>Used </label>                                
			<input name="used_1" type="number" id="used_1" step="any" class="form-control" required readonly />
		</div>

		<div class="form-group col-md-2">
		  <label>Left </label>                                
		  <input name="left_1" type="number" id="left_1" step="any" class="form-control" required readonly />
		</div>	
				
		<div class="form-group col-md-3">
		   <label> Actual Weight <font color="red">*</font></label>
		<input name="act_wt" type="number" onblur="" id="act_wt" step="any" class="form-control" required />
		</div> 


		<div class="form-group col-md-12" style="background:gray">
			<label style="padding-top:3px;color:#FFF">Charge Weight :</label>
		</div>	  
		
		<div class="form-group col-md-2">
		  <label>Total </label>                                 
		  <input name="total_2" type="number" id="total_2" step="any" class="form-control" required readonly />
		</div>

		<div class="form-group col-md-2">
		  <label>Used </label>                                 
			<input name="used_2" type="number" id="used_2" step="any" class="form-control" required readonly />
		</div>

		<div class="form-group col-md-2">
		  <label>Left  </label>                                
			<input name="left_2" type="number" id="left_2" step="any" class="form-control" required readonly />
		</div>	
				
		<div class="form-group col-md-3">
			<label> Charge Weight <font color="red">*</font></label>
			<input name="chrg_wt" type="number" id="chrg_wt" step="any" class="form-control" required />
		</div> 	

<script>
$(function() {
$("#chrg_wt,#act_wt").on("keydown keyup", sum);
	
function sum() {
		
if($("#lrno1").val() == '')
{
	alert('Please enter LR Number !');
	$("#lrno1").val('');
	$("#act_wt").val('');
	$("#chrg_wt").val('');
	$("#total_1").val('');
	$("#total_2").val('');
	$("#left_1").val('');
	$("#left_2").val('');
	$("#used_1").val('');
	$("#used_2").val('');
}		
else
{
	left1 = Number($("#left_1").val());
	left2 = Number($("#left_2").val());
	act = Number($("#act_wt").val());
	chrg = Number($("#chrg_wt").val());
	
if(act > left1)
{
	alert('Please check Actual Weight it is greater than Left Actual Weight.');
	$("#act_wt").val('');
	$("#chrg_wt").val('');
}

if(chrg > left2)
{
	alert('Please check Charge Weight it is greater than Left Charge Weight.');
	$("#act_wt").val('');
	$("#chrg_wt").val('');
}
	
}	
}});
</script>	
		</div>
      </div>
	  <div id="result_BreakingLRForm"></div>
      <div class="modal-footer">
        <button type="submit" id="break_lr_add_btn" disabled class="btn btn-primary">Break LR</button>
        <button type="button" onclick="ResetModalBreaking()" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#BreakingLRForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#break_lr_add_btn").attr("disabled", true);
		$.ajax({
        	url: "./lr_break_submit.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_BreakingLRForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function ResetModalBreaking()
{
	document.getElementById("BreakingLRForm").reset();
	$('#break_lr_add_btn').attr('disabled',true);
	$('#lrno1').attr('readonly',false);
}
</script>
     