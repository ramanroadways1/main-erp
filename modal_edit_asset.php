<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

if($type=='VEHICLE')
{
	$get_asset = Qry($conn,"SELECT a.id,a.vehicle_holder,a.reg_no,a.branch_user,a.asset_type,a.veh_type,a.reg_date,a.owner_name,
	a.veh_class,a.chasis_no,a.engine_no,a.fuel_type,a.maker,a.model,a.rc,a.insurance,a.puc,a.add_type,a.ho_approval,a.active,
	e.name as vehicle_holder1 
	FROM asset_vehicle as a 
	LEFT OUTER JOIN emp_attendance AS e ON e.code = a.vehicle_holder
	WHERE a.id='$id'");
}
else if($type=='ASSET')
{
	$get_asset = Qry($conn,"SELECT a.id,a.req_code,a.asset_company,a.asset_model,a.invoice_date,a.typeof,a.holder,a.branch_user,
	e.name as holder1,a.add_type,a.ho_approval,a.active,c.title as category 
	FROM asset_vehicle as a 
	LEFT OUTER JOIN asset_category AS c ON c.id = a.category
	LEFT OUTER JOIN emp_attendance AS e ON e.code = a.holder
	WHERE a.id='$id'");
}
else
{
	Redirect("Invalid asset type.","./pending_asset_req.php");
	exit();
}

if(!$get_asset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
?>

<form id="FormEditAsset" action="#" method="POST">
<div id="EditAssetModel" class="modal fade" style="overflow:auto" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		 Edit Asset : 
      </div>
      <div class="modal-body">
        <div class="row">
		
<script>	
function GetTokenInfo(){
	var token = $('#token_no2').val();
	
	if(token!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./asset_get_request_data_veh.php",
			data: 'token=' + token,
			type: "POST",
			success: function(data) {
				$("#result_req_veh_add").html(data);
			},
		error: function() {}
		});
	}
	else
	{
		alert('Select Token Number First !');
		$('#main_div').hide();
	}
}
</script>	
	
			<div class="form-group col-md-4" id="token_no_div">
				<label>Token Number <font color="red"><sup>*</sup></font></label>
				<select class="form-control" id="token_no2" name="token_no" required="required">
					<option value="">--Select option--</option>
					<?php
						$GetTokenPending = Qry($conn,"SELECT req_code,model_name FROM asset_vehicle_req 
						WHERE asset_added!='1' AND branch='$branch' ORDER BY id ASC");
						if(numRows($GetTokenPending)>0)
						{
							while($rowToken = fetchArray($GetTokenPending))
							{
								echo "<option value='$rowToken[req_code]'>$rowToken[req_code] ($rowToken[model_name])</option>";
							}
						}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-8" id="token_button_div">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="tokenButton" class="btn btn-sm btn-danger" onclick="GetTokenInfo()">Get details</button>
			</div>
			
		<div class="form-group col-md-12" id="main_div" style="display:none">
			<div class="row">
			
				<div class="form-group col-md-3">
					<label>Registration Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="reg_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Registration Date <font color="red"><sup>* (as per RC)</sup></font></label>
					<input name="reg_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<div class="form-group col-md-3">
					<label>Vehicle Type <font color="red"><sup>*</sup></font></label>
					<input type="text" id="d_veh_type" name="veh_type" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Fuel Type <font color="red"><sup>*</sup></font></label>
					<select name="fuel_type" class="form-control" required="required">
						<option value="">--option--</option>
						<option value="PETROL">PETROL</option>
						<option value="DIESEL">DIESEL</option>
					</select>
				</div>
				
				<div class="form-group col-md-3">
					<label>Vehicle Class <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="veh_class" oninput="this.value=this.value.replace(/[^a-z A-Z0-9()/-]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Vehicle Owner <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="rc_holder" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Maker Name <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="maker_name" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Model <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9/-]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Engine Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="engine_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Chasis Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="chasis_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Vehicle For <font color="red"><sup>*</sup></font></label>
					<select name="veh_for" class="form-control" onchange="VehFor1(this.value)" required="required">
						<option value="">--option--</option>
						<option value="GENERAL">General (for branch)</option>
						<option value="SPECIFIC">Specific (for person)</option>
					</select>
				</div>
				
				<script>
				function VehFor1(elem)
				{
					if(elem=='SPECIFIC'){
						$('#spec_div1').show();
						$('#emp_name_2').attr('required',true);
					}
					else{
						$('#spec_div1').hide();
						$('#emp_name_2').attr('required',false);
					}
				}
				</script>
				
				<div class="form-group col-md-3" id="spec_div1" style="display:none">
					<label>Select Employee <font color="red"><sup>*</sup></font></label>
					<select name="employee" class="form-control" id="emp_name_2" required="required">
						<option value="">--option--</option>
						<?php
						$getEmp2 = Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3'");
						if(numRows($getEmp2)>0)
						{
							while($rowEmp2 = fetchArray($getEmp2))
							{
								echo "<option value='$rowEmp2[code]'>$rowEmp2[name] ($rowEmp2[code])</option>";
							}
						}
						?>
					</select>
				</div>
				
				<div class="form-group col-md-3">
					<label>Payment Mode <font color="red"><sup>*</sup></font></label>
					<input type="text" id="d_payment_mode" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Invoice Date <font color="red"><sup>*</sup></font></label>
					<input name="invoice_date" type="date" class="form-control" max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<div class="form-group col-md-3">
					<label>Invoice Copy <font color="red"><sup>*</sup></font></label>
					<input type="file" accept="image/*" name="invoice_copy" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="amount" name="amount" min="1" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
					<input type="text" id="party_name" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>GST Invoice ? <font color="red"><sup>*</sup></font></label>
					<select class="form-control" onchange="GstInvoice(this.value)" required="required" id="gst_selection" name="gst_selection">
						<option value="">--Select option--</option>
						<option value="YES">YES</option>
						<option value="NO">NO</option>
					</select>	
				</div>

<script>		
function GstInvoice(elem){
	
	if($('#amount').val()=='' || $('#amount').val()==0)
	{
		alert('Please enter amount first !');
		$('#gst_selection').val('');
		$('#gst_type').val('');
		$('#gst_value').val('0');
		$('#gst_value').attr('readonly',true);
		$('#gst_amount').val('0');
		$('#total_amount').val($('#amount').val());
	}
	else
	{
		$('#amount').attr('readonly',true);
		
		if(elem=='YES'){
			$("#loadicon").show();
			jQuery.ajax({
				url: "./asset_gst_checked.php",
				data: 'token=' + $('#token_no2').val() + '&party_id=' + $('#partyId2').val() + '&tab_name=' + 'asset_vehicle_req',
				type: "POST",
				success: function(data) {
					$("#result_req_veh_add").html(data);
				},
			error: function() {}
			});
		}
		else{
			$('#gst_type').val('');
			$('#gst_value').val('0');
			$('#gst_value').attr('readonly',true);
			$('#gst_amount').val('0');
			$('#total_amount').val($('#amount').val());
		}
	}
}
</script>		
		
				<input type="hidden" id="partyId2" name="party_id">
		
				<div class="form-group col-md-3">
					<label>GST Type <font color="red"><sup>*</sup></font></label>
					<input type="text" id="gst_type" name="gst_type" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>GST Value <font color="red"><sup>* in (%)</sup></font></label>
					<input oninput="GetGstAmount(this.value)" type="number" name="gst_value" id="gst_value" min="1" max="30" class="form-control" required="required">
				</div>

<script>			
function GetGstAmount(gst_per){
	var amount = Number($('#amount').val());
	var gst_amount = Number(amount*gst_per/100);
	$('#gst_amount').val(gst_amount.toFixed(2))
	$('#total_amount').val(Number(amount+gst_amount).toFixed(2));
}
</script>			
			
				<div class="form-group col-md-3">
					<label>GST Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="gst_amount" name="gst_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Total Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="total_amount" name="total_amount" min="1" readonly class="form-control" required="required">
				</div>
				
			</div>
		</div>
	
		</div>
      </div>
	  <div id="result_form_asset"></div>
      <div class="modal-footer">
        <button type="submit" id="btn_save" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormEditAsset").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_save").attr("disabled", true);
	$.ajax({
        	url: "./save_asset_edit.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_form_asset").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>