<?php
require_once './connection.php'; 

$branch = escapeString($conn,strtoupper($_SESSION['user']));
$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$date_attendance = escapeString($conn,$_POST['date_attendance']);

$check_active_emp = Qry($conn,"SELECT id FROM emp_attendance WHERE branch='$branch' 
AND (status IN('3','2') OR (status='-1' AND terminate='0'))");
				
if(!$check_active_emp){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$check_total_emp_1 = Qry($conn,"SELECT total FROM emp_attd_check WHERE date='$date_attendance' AND branch='$branch'");
if(!$check_total_emp_1){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_total_emp = fetchArray($check_total_emp_1);		
		
if(numRows($check_active_emp)!=$row_total_emp['total'])
{
	echo "<script>
		alert('Employee\'s approval pending !');
		$('#loadicon').hide();
		$('#save_attd').attr('disabled',false);	
	</script>";
	exit();				
}

$select_chk = Qry($conn,"SELECT id FROM emp_attendance_save WHERE date='$date_attendance' AND branch='$branch'");
if(!$select_chk){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($select_chk)>0)
{
	echo "<script>
			alert('Attendance already updated for date : $date_attendance');
			$('#loadicon').hide();
			$('#save_attd').attr('disabled',false);	
		</script>";
	exit();
}

StartCommit($conn);
$flag = true;

foreach($_POST['attendance'] as $id2=>$attendance)
{
	$code=$_POST['code'][$id2];
	$remarks=$_POST['remark'][$id2];
	$qry = Qry($conn,"INSERT INTO emp_attendance_save(code,status,remarks,branch,date,fill_by_user,timestamp) VALUES 
	('$code','$attendance','$remarks','$branch','$date_attendance','$branch_sub_user','$timestamp')");
	if(!$qry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$qry2=Qry($conn,"SELECT COUNT(id) as p FROM emp_attendance_save WHERE date='$date_attendance' AND branch='$branch' AND status='P'");

if(!$qry2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	
	
$qry3=Qry($conn,"SELECT COUNT(id) as a FROM emp_attendance_save WHERE date='$date_attendance' AND branch='$branch' AND status='A'");	

if(!$qry3){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$qry4=Qry($conn,"SELECT COUNT(id) as hd FROM emp_attendance_save WHERE date='$date_attendance' AND branch='$branch' AND status='HD'");	

if(!$qry4){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$row_p = fetchArray($qry2);
$row_a = fetchArray($qry3);
$row_hd = fetchArray($qry4);
	
$update=Qry($conn,"UPDATE emp_attd_check SET p='$row_p[p]',a='$row_a[a]',hd='$row_hd[hd]' WHERE date='$date_attendance' AND 
branch='$branch'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
			alert('Attendance updated successfully of : $date_attendance.');
			window.location.href='./';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
			alert('Error While Processing Request.');
			window.location.href='./';
	</script>";
	exit();
}
?>