<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#PassForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button_sub").attr("disabled", true);
	$.ajax({
        	url: "./change_password2.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#resultPass").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>

<div id="resultPass"></div>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" id="PassForm" method="POST">	
	
<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Change Password</h4></center>
	</div>

	<div class="form-group col-md-12">
		<label>Current Password <font color="red">*</font></label>
		<input type="password" class="form-control" id="password_old" name="password_old" 
		oninput="this.value=this.value.replace(/[^a-zA-Z.-@#0-9]/,'');" required="required" />
	</div>
	
<script>	
function ShowPassFunc(){
	$('#PassNew1').attr('type','text');
	$('#PassNew2').attr('type','text');
	$('#ShowBtn').hide();
	$('#HideBtn').show();
}
function HidePassFunc(){
	$('#PassNew1').attr('type','password');
	$('#PassNew2').attr('type','password');
	$('#HideBtn').hide();
	$('#ShowBtn').show();
}

/* Password strength indicator */
function passwordStrength(password) {

	var desc = [{'width':'0px'}, {'width':'20%'}, {'width':'40%'}, {'width':'60%'}, {'width':'80%'}, {'width':'100%'}];
	
	var descClass = ['', 'progress-bar-danger', 'progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success'];

	var score = 0;

	//if password bigger than 6 give 1 point
	if (password.length > 3) score++;

	//if password has both lower and uppercase characters give 1 point	
	if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

	//if password has at least one number give 1 point
	if (password.match(/d+/)) score++;

	//if password has at least one special caracther give 1 point
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

	//if password bigger than 12 give another 1 point
	if (password.length > 3) score++;
	
	// display indicator
	$("#jak_pstrength").removeClass(descClass[score-1]).addClass(descClass[score]).css(desc[score]);
}

jQuery(document).ready(function(){
	jQuery("#password_old").focus();
	jQuery("#PassNew1").keyup(function() {
	  passwordStrength(jQuery(this).val());
	  var class1 = $("#jak_pstrength").attr('class');
		if(class1=='progress-bar progress-bar-success')
		{
			$('#button_sub').attr('disabled',false);
			$('#button_sub').html('Change Password');
		}
		else
		{
			$('#button_sub').attr('disabled',true);
			$('#button_sub').html('Weak Password');
		}
	});
});
</script>	
	
	<div class="form-group col-md-12">
		<label>New Password <font color="red">*</font> 
		&nbsp; &nbsp; <span id="ShowBtn" onclick="ShowPassFunc()" class="btn btn-xs btn-default" style="">show Password</span>
		<span id="HideBtn" style="display:none" onclick="HidePassFunc()" class="btn btn-xs btn-default">hide Password</span>
		</label>
		<input id="PassNew1" type="password" class="form-control" name="password_new" 
		oninput="this.value=this.value.replace(/[^a-zA-Z.-@#0-9]/,'');" required="required" />
	</div>
	
	<div class="form-group col-md-12">
		<label>Retype New Password <font color="red">*</font></label>
		<input id="PassNew2" type="password" class="form-control" name="password_new2" 
		oninput="this.value=this.value.replace(/[^a-zA-Z.-@#0-9]/,'');" required="required" />
	</div>
	
	<div class="col-md-12">
		<div class="progress progress-striped active">
			<div id="jak_pstrength" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
		</div>
	</div>

	<div class="form-group col-md-12">	
		<button id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit">Change Password</button>
	</div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>