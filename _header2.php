<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!--<script src="help/tphead.js" type="text/javascript"></script>-->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<script src="js/lumino.glyphs.js"></script>  
	<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="./google_font.css" rel="stylesheet">
</head>

<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

body {
    overflow-x: auto;
}

.ui-autocomplete { z-index:2147483647; }

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

.ui-widget {
  font-family: Verdana;
  font-size: 12px;
}
input{
	font-size:12px;
}
</style>

<style>
label{
	font-size:12px !important;
}

input[type='text'],input[type='number'],input[type='file'],input[type='date'],input[type='date'],input[type='file'],select,textarea{
	font-size:12px !important;
}
select>option,option{
	font-size:12px !important;
}
</style>
