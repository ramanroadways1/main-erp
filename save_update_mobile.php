<?php
require_once './connection.php'; 

$date = date('Y-m-d');
$timestamp = date('Y-m-d H:i:s');

$type=escapeString($conn,($_POST['type']));
$id=escapeString($conn,($_POST['id']));
$mobile=escapeString($conn,($_POST['mobile']));

if($type=='broker'){
	$table_name="mk_broker";
}
else if($type=='owner'){
	$table_name="mk_truck";
}
else{
	echo "<script type='text/javascript'> 
	 	alert('Party not found..');
		$('#updateOwnerMobileBtn').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_ext_data = Qry($conn,"SELECT mo1 as mobile,branch_mobile_update FROM `$table_name` WHERE id='$id'");

if(!$get_ext_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error.');$('#updateOwnerMobileBtn').attr('disabled',false);$('#loadicon').hide();</script>";
	exit();
}
	
if(numRows($get_ext_data)==0)
{
	echo "<script>alert('No record found.');$('#updateOwnerMobileBtn').attr('disabled',false);$('#loadicon').hide();</script>";
	exit();	
}

$row = fetchArray($get_ext_data);
	
if($row['branch_mobile_update']!=0)
{
	echo "<script type='text/javascript'>
		alert('Error: Branch can update mobile only once !!'); 
		$('#updateOwnerMobileBtn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$ext_mobile = $row['mobile'];

if($ext_mobile==$mobile)
{	
	echo "<script type='text/javascript'>
		alert('Nothing to update..'); 
		$('#updateOwnerMobileBtn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

if(strlen($mobile)!=10)
{	
	echo "<script type='text/javascript'>
		alert('Check mobile number..'); 
		$('#updateOwnerMobileBtn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Check mobile number..'); 
		$('#updateOwnerMobileBtn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$update = Qry($conn,"UPDATE `$table_name` SET mo1='$mobile',branch_mobile_update='1' WHERE id='$id'");
	
if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$edit_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,branch_user,timestamp) VALUES 
('$id','Mobile_Number_Update','$type','$ext_mobile to $mobile','$branch','$_SESSION[user_code]','$timestamp')");
	
if(!$edit_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
} 

// $update_sms_error = Qry($conn,"UPDATE _webhook_pinnacle_sms_invalid_number SET is_updated='1' WHERE ")

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Successfully Updated !');
		window.location.href='./all_functions.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
	</script>";
	exit();
}
?>