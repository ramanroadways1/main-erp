<?php
require_once './connection.php'; 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$mobile = strtoupper(escapeString($conn,$_POST['mobile']));
$otp = strtoupper(escapeString($conn,$_POST['otp']));
$emp_code = strtoupper(escapeString($conn,$_POST['emp_code']));

if($emp_code==''){
	echo "<script>
		alert('Employee not found.');
		window.location.href='./employee_view.php';
	</script>";
	exit();
}
	
$chk_code = Qry($conn,"SELECT name FROM emp_attendance WHERE code='$emp_code'");	
if(!$chk_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request.","./employee_view.php");
	exit();
}

if(numRows($chk_code)==0){
	errorLog("Employee not found. Code $emp_code.",$conn,$page_name,__LINE__);	
	Redirect("Employee not found.","./employee_view.php");
	exit();
}

$Check_Mobile = Qry($conn,"SELECT name FROM emp_attendance WHERE mobile_no='$mobile'");

if(!$Check_Mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
}

if(numRows($Check_Mobile)>0)
{
	$row_Check = fetchArray($Check_Mobile);
	
	echo "<script>
		alert('Duplicate Mobile Number. Mobile Number Registered with $row_Check[name] !');
		$('#update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

// For Verify otp
$url="https://www.smsschool.in/api/verifyRequestOTP.php";
$postData = array(
    'authkey' => "242484ARRP024r65bc082c9",
    'mobile' => "$mobile",
    'otp' => "$otp"
);

$ch = curl_init();
curl_setopt_array($ch, array(
CURLOPT_URL => $url,
CURLOPT_RETURNTRANSFER => true,
CURLOPT_POST => true,
CURLOPT_POSTFIELDS => $postData
));

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

$output = curl_exec($ch);

if(curl_errno($ch))
{
	echo "<script>
		alert('API Failure !');
		window.location.href='./employee_view.php';
	</script>";
	exit();	
}

$result = json_decode($output, true);
curl_close($ch);

// echo $output;

if($result['type']=='error')
{
	echo "<script>
			alert('Error : $result[message].');
			$('#update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();	
}
else
{
	if($result['message']!='otp_verified')
	{
		echo "<script>
			alert('Error : $result[message].');
			$('#update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();	
	}
}
	
// StartCommit($conn);
// $flag = true;
	

$update = Qry($conn,"UPDATE emp_attendance SET mobile_no='$mobile' WHERE code='$emp_code'");
	if(!$update){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
	

echo "<script> 	
		alert('Mobile Number Updated Successfully !');
		 window.location.href='./employee_view.php';
	</script>";
exit();	
  
// if($flag)
// {
	// MySQLCommit($conn);
	// closeConnection($conn);
	// echo "<script> 	
		// alert('HO Credited Successfully !!');
		// window.location.href='./credit_head_office.php';
	// </script>";
	// exit();
// }
// else
// {
	// MySQLRollBack($conn);
	// closeConnection($conn);
	// Redirect("Error While Processing Request.","./credit_head_office.php");
	// exit();
// }
?>