<?php
require_once 'connection.php';

$company =  escapeString($conn,strtoupper($_POST['company'])); 
$from_date =  escapeString($conn,strtoupper($_POST['from_date']));
$to_date =  escapeString($conn,strtoupper($_POST['to_date']));

$sql = "";
	
	echo '
    	<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>#</th>
					<th>Username <span class="glyphicon glyphicon-filter"></span></th>
					<th>Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Vou_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Vou_No <span class="glyphicon glyphicon-filter"></span></th>
					<th>Vou_Type <span class="glyphicon glyphicon-filter"></span></th>
					<th>Particulars <span class="glyphicon glyphicon-filter"></span></th>
					<th>Credit <span class="glyphicon glyphicon-filter"></span></th>
					<th>Debit <span class="glyphicon glyphicon-filter"></span></th>
					<th>Balance <span class="glyphicon glyphicon-filter"></span></th>
				</tr>
		      </thead> 
		 	</table>
		</div>
 <style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
 </style>

<script type="text/javascript">
$(document).ready(function() {
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 

        "scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
// 		"sAjaxSource": "master_expense_data.php?company='.$company.'&from_date='.$from_date.'&to_date='.$to_date.'",
// 		"bPaginate": true,
		"sPaginationType":"full_numbers",
// 		"iDisplayLength": 15,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "excel", "print", "colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=load_table.gif height=20> </center>"
        },
		"order": [[0, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
		], 
        "serverSide": true,
        "ajax": "_fetch_cashbook_server.php?company='.$company.'&from_date='.$from_date.'&to_date='.$to_date.'",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
} );
';
 
?>