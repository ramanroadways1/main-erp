<?php
require_once './connection.php'; 

$branch=escapeString($conn,strtoupper($_SESSION['user']));
// exit();
$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$aadhar_no = escapeString($conn,strtoupper($_POST['aadhar_no']));
// $lic_no = trim(escapeString($conn,strtoupper($_POST['lic_no_1'])))."-".trim(escapeString($conn,strtoupper($_POST['lic_no_2'])));
$lic_no = trim(escapeString($conn,strtoupper($_POST['lic_no_1'])));
$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));
$supervisor = trim(escapeString($conn,strtoupper($_POST['supervisor'])));
$vehicle_class = trim(escapeString($conn,strtoupper($_POST['vehicle_class'])));
$perm_addr = trim(escapeString($conn,strtoupper($_POST['perm_addr'])));
$temp_addr = trim(escapeString($conn,strtoupper($_POST['temp_addr'])));
$state = trim(escapeString($conn,strtoupper($_POST['state'])));
$issued_by = trim(escapeString($conn,strtoupper($_POST['issued_by'])));
$gender = trim(escapeString($conn,strtoupper($_POST['gender'])));
$father_or_husband_name = trim(escapeString($conn,strtoupper($_POST['father_or_husband_name'])));
$dob = trim(escapeString($conn,strtoupper($_POST['dob'])));
$doe = trim(escapeString($conn,strtoupper($_POST['doe'])));
$transport_doe = trim(escapeString($conn,strtoupper($_POST['transport_doe'])));
$transport_doi = trim(escapeString($conn,strtoupper($_POST['transport_doi'])));
$doi = trim(escapeString($conn,strtoupper($_POST['doi'])));
$blood_group = trim(escapeString($conn,strtoupper($_POST['blood_group'])));

if($supervisor=='' || $supervisor==0)
{
	echo "<script>
		alert('Error: Supervisor: $supervisor not found !');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(isset($_POST['ac_details']))
{
	$ac_holder = trim(escapeString($conn,strtoupper($_POST['ac_holder'])));
	$ac_no = trim(escapeString($conn,strtoupper($_POST['ac_no'])));
	$bank_name = trim(escapeString($conn,strtoupper($_POST['bank_name'])));
	$ifsc = trim(escapeString($conn,strtoupper($_POST['ifsc'])));
	
	if(strlen($ifsc)!=11)
	{
		echo "<script>
			alert('Invalid IFSC Code !');
			$('#add_driver_button_own_truck').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
else
{
	$ac_holder = "";
	$ac_no = "";
	$bank_name = "";
	$ifsc = "";
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number !');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile Number !');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($aadhar_no)!=12)
{
	echo "<script>
		alert('Invalid Aadhar Number !');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($lic_no)<6)
{
	echo "<script>
		alert('Check License number !');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$chk_lic_no = Qry($conn,"SELECT name FROM dairy.driver WHERE lic='$lic_no'");

if(!$chk_lic_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($chk_lic_no)>0)
{
	$row_name = fetchArray($chk_lic_no);
	
	echo "<script type='text/javascript'>
		alert('License number : $lic_no already exists with driver: $row_name[name].'); 
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();	
}

$chk_mobile = Qry($conn,"SELECT name FROM dairy.driver WHERE mobile='$mobile'");

if(!$chk_mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_mobile)>0)
{
	$row_mobile = fetchArray($chk_mobile);
	
		echo "<script type='text/javascript'>
			alert('Mobile number already exists with driver: $row_mobile[name] !'); 
			$('#add_driver_button_own_truck').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();	
}

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['driver_photo']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. Driver-Photo');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!in_array($_FILES['lic_copy_front']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. License-copy-front');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!in_array($_FILES['lic_copy_rear']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. License-copy-rear');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!in_array($_FILES['aadhar_front']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. Aadhar-copy-front');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(!in_array($_FILES['aadhar_rear']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed. Aadhar-copy-rear');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$driver_photo = $_FILES['driver_photo']['tmp_name'];
$lic_copy_front = $_FILES['lic_copy_front']['tmp_name'];
$lic_copy_rear = $_FILES['lic_copy_rear']['tmp_name'];
$aadhar_front = $_FILES['aadhar_front']['tmp_name'];
$aadhar_rear = $_FILES['aadhar_rear']['tmp_name'];

$unq_string = mt_rand().date("dmy");

$target_driver_photo = "../diary_admin/driver_photo/photo_".$mobile.$unq_string.".".pathinfo($_FILES['driver_photo']['name'],PATHINFO_EXTENSION);
$target_lic_front = "../diary_admin/lic_copy/lic_front_".$mobile.$unq_string.".".pathinfo($_FILES['lic_copy_front']['name'],PATHINFO_EXTENSION);
$target_lic_rear = "../diary_admin/lic_copy/lic_rear_".$mobile.$unq_string.".".pathinfo($_FILES['lic_copy_rear']['name'],PATHINFO_EXTENSION);
$target_aadhar_front = "../diary_admin/aadhar_copy/aadhar_front_".$mobile.$unq_string.".".pathinfo($_FILES['aadhar_front']['name'],PATHINFO_EXTENSION);
$target_aadhar_rear = "../diary_admin/aadhar_copy/aadhar_rear_".$mobile.$unq_string.".".pathinfo($_FILES['aadhar_rear']['name'],PATHINFO_EXTENSION);

$target_driver_photo1 = "driver_photo/photo_".$mobile.$unq_string.".".pathinfo($_FILES['driver_photo']['name'],PATHINFO_EXTENSION);
$target_lic_front1 = "lic_copy/lic_front_".$mobile.$unq_string.".".pathinfo($_FILES['lic_copy_front']['name'],PATHINFO_EXTENSION);
$target_lic_rear1 = "lic_copy/lic_rear_".$mobile.$unq_string.".".pathinfo($_FILES['lic_copy_rear']['name'],PATHINFO_EXTENSION);
$target_aadhar_front1 = "aadhar_copy/aadhar_front_".$mobile.$unq_string.".".pathinfo($_FILES['aadhar_front']['name'],PATHINFO_EXTENSION);
$target_aadhar_rear1 = "aadhar_copy/aadhar_rear_".$mobile.$unq_string.".".pathinfo($_FILES['aadhar_rear']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['driver_photo']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$driver_photo);
}

if(pathinfo($_FILES['lic_copy_front']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$lic_copy_front);
}

if(pathinfo($_FILES['lic_copy_rear']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$lic_copy_rear);
}

if(pathinfo($_FILES['aadhar_front']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$aadhar_front);
}

if(pathinfo($_FILES['aadhar_rear']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$aadhar_rear);
}

StartCommit($conn);
$flag = true;

if(!move_uploaded_file($driver_photo,$target_driver_photo))
{
	$flag = false;
	errorLog("Driver photo upload error.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($lic_copy_front,$target_lic_front))
{
	$flag = false;
	errorLog("Driver license-front upload error.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($lic_copy_rear,$target_lic_rear))
{
	$flag = false;
	errorLog("Driver license-rear upload error.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($aadhar_front,$target_aadhar_front))
{
	$flag = false;
	errorLog("Driver aadhar-front upload error.",$conn,$page_name,__LINE__);
}

if(!move_uploaded_file($aadhar_rear,$target_aadhar_rear))
{
	$flag = false;
	errorLog("Driver aadhar-rear upload error.",$conn,$page_name,__LINE__);
}

$insert_driver = Qry($conn,"INSERT INTO dairy.driver_temp(name,father_husband_name,mobile,lic,vehicle_class,transport_doi,transport_doe,
date_of_issue,val_date,dob,blood_group,gender,issued_by,state,lic_front,lic_rear,driver_photo,aadhar_no,aadhar_front,aadhar_rear,date,
branch,branch_user,addr,temp_addr,ac_holder,ac_no,bank,ifsc,supervisor,timestamp) VALUES ('$name','$father_or_husband_name','$mobile','$lic_no',
'$vehicle_class','$transport_doi','$transport_doe','$doi','$doe','$dob','$blood_group','$gender','$issued_by','$state','$target_lic_front1',
'$target_lic_rear1','$target_driver_photo1','$aadhar_no','$target_aadhar_front1','$target_aadhar_rear1','$date','$branch','$branch_sub_user',
'$perm_addr','$temp_addr','$ac_holder','$ac_no','$bank_name','$ifsc','$supervisor','$timestamp')");
	
if(!$insert_driver){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'> 
		alert('Driver : $name. Added Successfully..');
		$('#loadicon').fadeOut('slow');
		$('#add_driver_button_own_truck').attr('disabled',false);
		$('#close_add_driver_own_truck').click();
		var pending_req = Number($('#pending_req_html').html());
		$('#pending_req_html').html(Number(pending_req+1));
		$('#DriverFormOwnTruck').trigger('reset');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	unlink($target_driver_photo);
	unlink($target_lic_front);
	unlink($target_lic_rear);
	unlink($target_aadhar_front);
	unlink($target_aadhar_rear);
	
	echo "<script type='text/javascript'> 
		alert('Error..');
		$('#loadicon').fadeOut('slow');
		$('#add_driver_button_own_truck').attr('disabled',false);
	</script>";
	exit();
}
?>