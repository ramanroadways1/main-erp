<?php
include ('../connection.php');

if(isset($_POST['search'])){
 $search = $_POST['search'];
 
 $sql = Qry($conn,"SELECT id,iname FROM lritems WHERE iname LIKE '%".$search."%' ORDER BY iname ASC limit 10");
 
if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

 $response = array();
 while($row = fetchArray($sql)){
   $response[] = array("value"=>$row['iname'],"label"=>$row['iname'],"id"=>$row['id']);
 }
echo json_encode($response);
}
?>