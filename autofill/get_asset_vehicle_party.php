<?php
require_once("../connection.php");

if(isset($_REQUEST['term']))
{
$search = escapeString($conn,$_REQUEST['term']);
$query = $conn->query("SELECT id,legal_name,pan_no,gst_no,CONCAT('Ac Holder: ',ac_holder,', A/c No: ',ac_no,', Bank: ',bank_name,', IFSC: ',ifsc_code) as ac_details,branch FROM asset_party WHERE legal_name LIKE 
'%".$search."%' AND party_type='1' ORDER BY legal_name ASC LIMIT 8");

while($row = $query->fetch_assoc()) 
{ 
	 $data[] = array("value"=>$row['legal_name'],"label"=>$row['branch']."- ".$row['legal_name'],"id"=>$row['id'],"pan_no"=>$row['pan_no'],"gst_no"=>$row['gst_no'],"ac_details"=>$row['ac_details']);
}
	echo json_encode($data);
	mysqli_close($conn);
}
?>