<?php
require_once("../connection.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];

 $sql = Qry($conn, "SELECT id,name,mo1,pan,branch_mobile_update FROM `mk_broker` WHERE name LIKE '%".$search."%' limit 10");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['name'],"label"=>$row['name'].' - '.$row['pan'],"pan"=>$row['pan'],"mo1"=>$row['mo1'],"bid"=>$row['id'],"branch_update"=>$row['branch_mobile_update']);
 }

 echo json_encode($response);
}
?>