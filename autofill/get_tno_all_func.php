<?php
require_once("../connection.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];

 $sql = Qry($conn, "SELECT id,tno,name,mo1,branch_mobile_update FROM `mk_truck` WHERE tno LIKE '%".$search."%' AND blocked!='1' limit 10");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['tno'],"label"=>$row['tno'].' - '.$row['name'],"name"=>$row['name'],"mo1"=>$row['mo1'],"oid"=>$row['id'],"branch_update"=>$row['branch_mobile_update']);
 }

 echo json_encode($response);
}
?>