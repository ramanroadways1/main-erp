<?php
require_once './connection.php'; 

$date1 = escapeString($conn,($_POST['date']));
$date = date("d-m-Y",strtotime($date1));
$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));
$con2_id = escapeString($conn,strtoupper($_POST['con2_id']));
$start_date = $date."T00:00:01";
$end_date = $date."T23:59:59";
$Duration1= "2"; // in minutes
$timestamp =date("Y-m-d H:i:s");

echo '<option style="font-size:12px" value="">--select unloading point--</option>';

if($to_id=='' || $con2_id=='')
{
	echo "<script>
		alert('Location or consignee not found !');
		$('#visit_date').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$chk_entry = Qry($conn,"SELECT id FROM address_book_consignee_branch WHERE to_id='$to_id' AND consignee='$con2_id'");
		
if(!$chk_entry){
	echo "Error: ".mysqli_error($conn);
}

if(numRows($chk_entry)>0)
{
	echo "<script>
		alert('Data already update for this location and consignee !');
		$('#visit_date').val('');
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://apps2.locanix.net/RuningHourReport/api/Stoppages?VehicleName=$veh_no&FromDateTime=$start_date&ToDateTime=$end_date&Duration=$Duration1",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 900,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
'Content-Type: text/plain'
),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
if($err)
{
	echo "<script>
		alert('Error: $err.');
		$('#owo_tno').attr('readonly',false);
		$('#button_sub').attr('disabled',true);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
		
$response2 = json_decode($response, true);

if($response=='null' || $response=='')
{
	echo "<script>
		$('#to_loc').attr('readonly',false);
		$('#consignee').attr('readonly',false);
	</script>";
	
	$chk_err_msg = Qry($conn,"SELECT id FROM gps_device_error_log WHERE tno='$veh_no' AND date='$date1' AND error_name='NOT_FOUND'");
				
	if(!$chk_err_msg){
		echo "Error: ".mysqli_error($conn);
	}
				
	if(numRows($chk_err_msg)==0)
	{
		$insert_error = Qry($conn,"INSERT INTO gps_device_error_log(tno,date,error_name,error_desc,is_error,branch,branch_user,timestamp) 
		VALUES ('$veh_no','$date1','NOT_FOUND','NOT_FOUND','1','$branch','$_SESSION[user_code]','$timestamp')");
		
		if(!$insert_error){
			echo "Error: ".mysqli_error($conn);
		}
	}
}
else
{
	if($response2['Stoppages']!='')
	{
		foreach($response2['Stoppages'] as $Data1)
		{
			$from_time = str_replace("+05:30","",str_replace("T"," ",$Data1['From']));
			$to_time = str_replace("+05:30","",str_replace("T"," ",$Data1['To']));
			$duration = $Data1['Duration'];
			$address = $Data1['Address'];
			
			$zipcode = preg_match("/\b\d{6}\b/", $address, $matches);
			
			if(empty($matches[0])){
				$pincode = "NA";
			}else {
				$pincode = $matches[0];
			}
			
			$pincode = str_replace(" ","",$pincode);
			$pincode = str_replace(".","",$pincode);
			
			$lat = $Data1['Latitude'];
			$long = $Data1['Longitude'];
			$lat_long = $lat.",".$long.",".$pincode;
			$from_time_new = date("h:i A",strtotime($from_time));
			$to_time_new = date("h:i A",strtotime($to_time));
			// echo $from_time." to ".$to_time."<br>".$duration."<br>".$address."<br>".$lat."<br>".$long."<br>";
			echo "<option style='font-size:12px' value='$lat_long'>$address - $from_time_new to $to_time_new</option>";
			
			if(strlen($address)<=3)
			{
				$chk_err_msg = Qry($conn,"SELECT id FROM gps_device_error_log WHERE tno='$veh_no' AND date='$date1' AND error_name='ADDR_NOT_FOUND'");
				
				if(!$chk_err_msg){
					echo "Error: ".mysqli_error($conn);
				}
				
				if(numRows($chk_err_msg)==0)
				{
					$insert_error = Qry($conn,"INSERT INTO gps_device_error_log(tno,date,error_name,error_desc,is_error,branch,branch_user,timestamp) 
					VALUES ('$veh_no','$date1','ADDR_NOT_FOUND','$address','1','$branch','$_SESSION[user_code]','$timestamp')");
					
					if(!$insert_error){
						echo "Error: ".mysqli_error($conn);
					}
				}
			}
			
			if(strlen($lat_long)<=3)
			{
				$chk_err_msg = Qry($conn,"SELECT id FROM gps_device_error_log WHERE tno='$veh_no' AND date='$date1' AND error_name='LAT_LNG_NOT_FOUND'");
				
				if(!$chk_err_msg){
					echo "Error: ".mysqli_error($conn);
				}
				
				if(numRows($chk_err_msg)==0)
				{
					$insert_error = Qry($conn,"INSERT INTO gps_device_error_log(tno,date,error_name,error_desc,is_error,branch,branch_user,timestamp) 
					VALUES ('$veh_no','$date1','LAT_LNG_NOT_FOUND','$lat_long','1','$branch','$_SESSION[user_code]','$timestamp')");
					
					if(!$insert_error){
						echo "Error: ".mysqli_error($conn);
					}
				}
			}
		}
	}
	
	echo "<script>
		$('#to_loc').attr('readonly',true);
		$('#consignee').attr('readonly',true);
	</script>";
}

echo "<script>
	$('#loadicon').fadeOut('slow');
</script>";
?>