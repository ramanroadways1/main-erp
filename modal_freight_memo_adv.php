
<button data-toggle="modal" data-target="#PayAdvModal" id="adv_modal_btn_1" style="display:none"></button>
	
<div class="modal fade" id="PayAdvModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="background:#AAA;overflow:auto" 
data-keyboard="false" data-backdrop="static">
<div class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
<form id="verifyAdvForm" autocomplete="off">

<div class="modal-body">

<input type="hidden" id="owner_id" name="owner_id" value="<?php echo $o_id; ?>" />
<input type="hidden" id="broker_id" name="broker_id" value="<?php echo $b_id; ?>" />
<input type="hidden" id="company_name" name="company_name" value="<?php echo $comp1; ?>" />
<input type="hidden" id="truck_no" name="truck_no" value="<?php echo $truck_no; ?>" />
<input type="hidden" id="broker_pan" name="broker_pan" value="<?php echo $broker_pan; ?>" />
<input type="hidden" id="owner_pan" name="owner_pan" value="<?php echo $owner_pan; ?>" />

<input type="hidden" id="b_bank" name="b_bank" value="<?php echo $b_bank; ?>" />
<input type="hidden" id="b_acname" name="b_acname" value="<?php echo $b_acname; ?>" />
<input type="hidden" id="b_acno" name="b_acno" value="<?php echo $b_acno; ?>" />
<input type="hidden" id="b_ifsc" name="b_ifsc" value="<?php echo $b_ifsc; ?>" />

<input type="hidden" id="o_bank" name="o_bank" value="<?php echo $o_bank; ?>" />
<input type="hidden" id="o_acname" name="o_acname" value="<?php echo $o_acname; ?>" />
<input type="hidden" id="o_acno" name="o_acno" value="<?php echo $o_acno; ?>" />
<input type="hidden" id="o_ifsc" name="o_ifsc" value="<?php echo $o_ifsc; ?>" />

<input type="hidden" name="vou_date" value="<?php echo $fmdt; ?>" />
<input type="hidden" name="broker_name" id="broker_name" value="<?php echo $broker_name; ?>" />
<input type="hidden" name="owner_name" id="owner_name" value="<?php echo $owner_name; ?>" />
<input type="hidden" name="owner_mobile" value="<?php echo $owner_mobile; ?>" />
<input type="hidden" name="broker_mobile" value="<?php echo $broker_mobile; ?>" />

<input type="hidden" value="<?php echo $db_actual; ?>" name="db_af" id="db_af" />
<input type="hidden" value="<?php echo $db_weight; ?>" name="db_weight"  />
<input type="hidden" value="<?php echo $db_lrno; ?>" name="db_lrno" />
<input type="hidden" value="<?php echo $db_lrno2; ?>" name="db_lrno2" />
<input type="hidden" value="<?php echo $lr_count; ?>" name="db_lr_count" />

<input type="hidden" value="<?php echo $from_loc_db1; ?>" name="from_loc_db1" />
<input type="hidden" value="<?php echo $to_loc_db1; ?>" name="to_loc_db1" />
				

<div id="result_freight"></div>
<?php
$chk_lr_pending = Qry($conn,"SELECT id FROM diesel_lr_pending WHERE tno='$truck_no' AND branch='$branch'");
if(!$chk_lr_pending){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_lr_pending)>0)
{
	echo "<script>
			alert('Warning : LR Number pending against Diesel Recharge. Please Clear it first !');
			window.location.href='./pre_entry/';	
		</script>";
	exit();
}

$qry_pre_fetch = Qry($conn,"SELECT GROUP_CONCAT(QUOTE(diesel_id) SEPARATOR ',') as diesel_id,fno FROM _pending_diesel WHERE 
lrno IN($db_lrno2) AND branch='$branch' GROUP BY fno");

if(!$qry_pre_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry_pre_fetch)==0)
{
	$pre_entry_set=0;
	$sys_diesel=0;
	$token_no="";
}
else if(numRows($qry_pre_fetch)>1)
{
	echo "<script>
		alert('Warning : Advance Diesel Requested Multiple Time. You Can not Proceed !');
		window.location.href='./';	
	</script>";
	exit();
}
else if(numRows($qry_pre_fetch)==1)
{
	
$row_request = fetchArray($qry_pre_fetch);
	
$get_diesel_details = Qry($conn,"SELECT branch,fno,tno,com,disamt,cash,tno,dsl_nrr,done,no_lr,crossing FROM diesel_fm WHERE id 
IN($row_request[diesel_id]) AND branch='$branch'");	

if(!$get_diesel_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

	$sys_narration=array();
	$sys_cash=0;
	$sys_diesel=0;
	$no_lr_set=array();
	
	while($row_pre_req=fetchArray($get_diesel_details))
	{
		$sys_tno=$row_pre_req['tno'];
		$sys_cash=$sys_cash+$row_pre_req['cash'];
		$sys_diesel=$sys_diesel+$row_pre_req['disamt'];
		$sys_narration[]=$row_pre_req['dsl_nrr'];
		$crossing=$row_pre_req['crossing'];
		$diesel_comp=$row_pre_req['com'];
		$done_diesel=$row_pre_req['done'];
		$no_lr_set[]=$row_pre_req['no_lr'];
		$token_no = $row_pre_req['fno'];
	}
	
	if($token_no!=$row_request['fno'])
	{
		echo "<script>
			alert('Warning : Token Number not matching with Requested Diesel.');
			window.location.href='./';	
		</script>";
		exit();
	}	
	
	if($sys_cash>0)
	{
		echo "<script>
			alert('Warning : You can not pay cash advance !');
			window.location.href='./';	
		</script>";
		exit();
	}
	
	if(in_array("1",$no_lr_set))
	{
		echo "<script>
			alert('Error: LR No pending against Diesel Recharge.');
			window.location.href='./';	
		</script>";
		exit();
	}
	
	$sys_narration=implode(', ', $sys_narration);
	
	// if($crossing==1)
	// {
		if($sys_tno!=$truck_no)
		{
			echo "<script>
					alert('Warning : Vehicle Number not matching with requested diesel.');
					window.location.href='./';	
				</script>";
			exit();
		}	
	// }
	
	if($sys_diesel>0 && $done_diesel==0)
	{
		echo "<script>
				alert('Warning : Requested diesel not marked as done yet. Contact Diesel Department.');
				window.location.href='./';	
			</script>";
		exit();
	}
	
	$pre_entry_set=1;
}

$gps_rent_charge = 0;

$get_multiple_device_record = Qry($conn,"SELECT id FROM gps_device_log_multiple WHERE tno='$truck_no'");

if(!$get_multiple_device_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_multiple_device_record)>0)
{
	$gps_rent_charge = 0;
	echo "<script>
		alert('Warning : Multiple gps device found.');
		window.close();	
	</script>";
	exit(); 
}
else
{
	$get_gps_data = Qry($conn,"SELECT fix_attach,pending_charges,validity_end,current_validity FROM gps_device_inventory WHERE active_on='$truck_no'");
	
	if(!$get_gps_data){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_gps_data)==0)
	{
		$gps_rent_charge = 0;
	}
	else if(numRows($get_gps_data)>1)
	{
		echo "<script>
			alert('Multiple GPS devices active on this vehicle !!')
			window.close();
		</script>";
		exit();
	}
	else
	{
		$row_Chk_Gps = fetchArray($get_gps_data);
		
		if($row_Chk_Gps['pending_charges']=="1")
		{
			if($row_Chk_Gps['fix_attach']=="Fix"){
				$gps_rent_charge = 1880;
			}
			else{
				$gps_rent_charge = 380;
			}
		}
		else
		{
			if($row_Chk_Gps['current_validity']==0 || $row_Chk_Gps['current_validity']==""){
				$gps_validity_date = $row_Chk_Gps['validity_end'];
			}
			else{
				$gps_validity_date = $row_Chk_Gps['current_validity'];
			}
			
			if($gps_validity_date==0 || $gps_validity_date=="")
			{
				echo "<script>
					alert('Warning : GPS validity date is not valid.');
					window.close();	
				</script>";
				exit();
			}
			
			if(strtotime(date("Y-m-d"))>strtotime($gps_validity_date))
			{
				if($row_Chk_Gps['fix_attach']=="Fix")
				{
					$gps_rent_charge = 380;
				}
				else
				{
					$gps_day = round((strtotime(date("Y-m-d")) - strtotime($gps_validity_date)) / (60 * 60 * 24));	
						$count1 = floor($gps_day/30)+1;

						if($gps_day % 30 == 0){
							$count1=$count1-1;
						}
												
						$gps_rent_charge = $count1 * 380;
												
						if($gps_rent_charge>1880){
							$gps_rent_charge=1880;
						}
				}
			}
			else
			{
				$gps_rent_charge = 0;
			}
		}
	}
}
?>
<script type="text/javascript">
function ChkPreDiesel(dsl){
	var dsl_pre='<?php echo $sys_diesel; ?>';
	if(Number(dsl)<Number(dsl_pre)){
		alert('Diesel value must be less than or equal to '+dsl_pre);
		$('#dsl_amt').val(dsl_pre);
	}
}
</script>

<input type="hidden" id="pre_entry_box" name="pre_entry_box" value="<?php echo $pre_entry_set; ?>">
<input type="hidden" name="token_no" value="<?php echo $token_no; ?>">
<input type="hidden" name="diesel_comp" value="<?php echo $diesel_comp; ?>">
<input type="hidden" id="sys_diesel" name="sys_diesel" value="<?php echo $sys_diesel; ?>">

<div class="row">

<div class="form-group col-md-2">
	<label>Act. Freight</label> <font color="red">*</font>
	<input min="0" value="<?php echo $db_actual; ?>" class="form-control" name="actual_freight" id="af" type="number" required readonly /> 
</div>

<div class="form-group col-md-2">
	<label>Loading(+) <font color="red">*</font></label>
	<input step="any" value="0" class="form-control" min="0" name="load" id="load" type="number" required /> 
</div>

	<input type="hidden" value="0" name="dsl_inc" id="dsl_inc" type="number" readonly required />

	<div class="form-group col-md-2">
		<label>GPS(-) <font color="red">*</font></label>
		<input readonly class="form-control" min="0" value="<?php echo $gps_rent_charge; ?>" name="gps_charges" id="gps_charges" type="number" required /> 
	</div>

	<div class="form-group col-md-2">
		<label>Other(-) <font color="red">*</font></label>
		<input value="0" class="form-control" min="<?php echo $other_min; ?>" name="other" id="other" type="number" required /> 
	</div>
	
	<input type="hidden" min="0" value="0" id="tds" name="tds">
	
	<div class="form-group col-md-2">
		<label>TDS(-) <font color="red">*</font></label>
		<input min="0" value="0" class="form-control" name="tds2" id="tds2" type="number" readonly required /> 
	</div>
	
	<div class="form-group col-md-2">
		<label>Claim(-) <font color="red">*</font> <button type="button" data-toggle="modal" data-target="#ClaimModal" 
		class="btn btn-xs btn-primary">ADD</button></label>
		<input min="0" value="0" class="form-control" name="claim_amount" id="claim_amount" type="number" readonly required /> 
	</div>

<!-- AJAX FOR PAID TO PAN VALIDATION -->
 
<script type="text/javascript">			
function PaidtoNew(elem){
	$("#loadicon").show();
	$.ajax({
		url: "pan_validation.php",
		method: "post",
		data:'adv_to=' + elem.value + '&broker_pan=' + $("#broker_pan").val() + '&owner_pan=' + $("#owner_pan").val() + '&oid=' + $("#owner_id").val() + '&bid=' + $("#broker_id").val() + '&af=' + $("#db_af").val() + '&b_name=' + $("#broker_name").val() + '&tno=' + $("#truck_no").val() + '&pre_entry=' + $("#pre_entry_box").val() + '&frno=' + '<?php echo $idmemo; ?>' + '&branch=' + '<?php echo $branch; ?>' + '&token_no=' + '<?php echo $token_no; ?>' + '&sys_diesel=' + '<?php echo $sys_diesel; ?>' + '&tds_deduct=' + '<?php echo $tds_deduct; ?>' + '&tds_broker=' + '<?php echo $tds_amount_broker; ?>' + '&tds_owner=' + '<?php echo $tds_amount_owner; ?>',
		success: function(data){
		$("#result_freight").html(data);
	}})
}
</script>

	<div class="form-group col-md-2" id="adv_div">
		<label>Adv to <font color="red">*</font></label>
		<select class="form-control" onchange="PaidtoNew(this)" name="ptob" id="ptob" required="required">
			<option value="">Select Option</option>
			<option <?php if($pre_entry_set==1) {echo "disabled";} ?> value="NULL">ZERO/NULL ADV</option>
			<option value="BROKER">Broker</option>
			<option value="OWNER">Owner</option>
		</select>
	</div>

<script type="text/javascript">
function checkPan(pan) {
	
	var adv_to = $("#ptob").val();
	
	if(adv_to=='' || adv_to=='NULL')
	{
		$("#ptob").focus();
	}
	else
	{
		if(adv_to=='OWNER')
		{
			var pan_verify = $("#owner_pan").val();
		}
		else
		{
			var pan_verify = $("#broker_pan").val();
		}	
		
		if(pan_verify == pan.toUpperCase())
		{
			$("#pan_no").css({"background-color": "#66ff66", "color": "#000"});
			$("#btn_submit").attr("disabled",false);
		}
		else
		{
			$("#pan_no").focus();
			$("#pan_no").css({"background-color": "#ff6666", "color": "#FFF"});
			$("#btn_submit").attr("disabled",true);
		}
	}
}
</script>	
	
	<div class="form-group col-md-3" id="verify_div" style="display:none">
		<label><font color='red'>Verify</font> <span id="pan_label"></span> <font color="red"> *</font></label>
		<input type="text" id="pan_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');checkPan(this.value)" 
		onblur="checkPan(this.value);" class="form-control" maxlength="10" minlength="10" />
	</div>

	<script type="text/javascript">
	function checkCash(cash) {
		
		if($("#ptob").val()=='' || $("#ptob").val()=='NULL')
		{
			$("#ptob").focus();
			$("#casmt").val('0');
		}
		
		if(cash>35000)
		{
			alert('Max Cash allowed is : 35\,000');	
			$("#casmt").val('');
		}
	}
	</script>
	
	<?php 
	$sel_cash_limit=Qry($conn,"SELECT amount FROM allow_cash WHERE vou_no='$idmemo' AND adv_bal='1' AND admin_timestamp IS NOT NULL");
	if(numRows($sel_cash_limit)==0){
		$max_cash_limit=0;
	}
	else{
		$row_cash_limit=fetchArray($sel_cash_limit);
		$max_cash_limit=$row_cash_limit['amount'];
	}
	?>
	
	<div class="form-group col-md-3" id="cash_div">
		<label>Cash Adv. <font color="red">*</font></label>
		<input step="any" max="<?php echo $max_cash_limit; ?>" oninput="checkCash(this.value);" onblur="checkCash(this.value);" 
		placeholder="Cash Advance" min="0" class="form-control" id="casmt" name="casmt" type="number" required /> 
	</div>
	
<script type="text/javascript">
function chqDiv(elem){
	
	$('#chq_amt').val('0');
	
	if(elem==1)
	{
		if($("#ptob").val()=='' || $("#ptob").val()=='NULL')
		{
			$("#ptob").focus();
			$("#chq_sel").val('0');
		}
		else
		{
			$('#chq_input').show();
			$("#chq_input :input").prop('required','required');
		}	
	}
	else if(elem==0)
	{
		$('#chq_input').hide();
		$("#chq_input :input").prop('required',false);
	}
}			

function dslDiv(elem)
{
	$('#dsl_amt').val('0');
	
	if(elem==1)
	{
		if($("#ptob").val()=='' || $("#ptob").val()=='NULL')
		{
			$("#ptob").focus();
			$("#dsl_sel").val('0');
		}
		else
		{
			$('#dsl_input').show();
			$("#dsl_input :input").prop('required',true);
		}	
	}
	else if(elem==0)
	{
		$('#dsl_input').hide();
		$("#dsl_input :input").prop('required',false);
	}
}

function rtgsDiv(elem){
	
	$("#loadicon").show();
	$("#rtgs_amt").val("0");
	var advance_to = $("#ptob").val();
	
	if(advance_to!='NULL' && advance_to!='')
	{
		if(elem==1)
		{
			var fm_prev_year1 = '<?php echo $fm_prev_year; ?>';
			
			if(fm_prev_year1=='YES')
			{
				alert('This is last year Freight Memo. You can not pay throught RTGS.');
				window.locatin.href='./';
			}
			else
			{
				if(advance_to=='BROKER')
				{
					var bank_name = $("#b_bank").val()
					var ac_holder = $("#b_acname").val()
					var ac_no = $("#b_acno").val()
					var ifsc_code = $("#b_ifsc").val()
				}
				else if(advance_to=='OWNER')
				{
					var bank_name = $("#o_bank").val()
					var ac_holder = $("#o_acname").val()
					var ac_no = $("#o_acno").val()
					var ifsc_code = $("#o_ifsc").val()
				}
				else
				{
					alert("Please Select Payment to option first !");
					window.location.href='./';
				}
				
				$("#acname").val(ac_holder);  
				$("#acno").val(ac_no);
				$("#bank").val(bank_name);
				$("#ifsc").val(ifsc_code);
					
				if(ac_no=='')
				{
					$("#rtgs_input :input").val('');
					$("#rtgs_input :input").prop('readonly',false);
					$("#rtgs_input :input").prop('required','required');
				}
				else
				{
					$("#rtgs_input :input").prop('readonly','readonly');
					$("#rtgs_input :input").prop('required','required');
					$("#rtgs_amt").attr('readonly',false);
				}
				
				$('#rtgs_input').show();
				$('#loadicon').hide();
			}
		}
		else if(elem==0)
		{
			$('#rtgs_input').hide();
			$("#rtgs_input :input").prop('required',false);
			$("#rtgs_input :input").prop('readonly','readonly');
			$("#rtgs_amt").val("0");  
			$("#loadicon").hide();
		}
	}
	else
	{
		$("#ptob").focus();
		$("#rtgs_sel").val('0');
		$("#loadicon").hide();
	}
}			
</script>

<div class="form-group col-md-2" id="chq_div">
	<label>Chq Adv. <font color="red">*</font></label>
	<select class="form-control" name="chq_sel" onchange="chqDiv(this.value)" id="chq_sel" required="required">
		<option value="0">NO</option>
		<option value="1">YES</option>
	</select> 
</div>

<div class="form-group col-md-2" id="dsl_div">
	<label>Diesel Adv. <font color="red">*</font></label>
	<select class="form-control" name="dsl_sel" onchange="dslDiv(this.value)" id="dsl_sel" required="required">
		<option id="no_diesel" value="0">NO</option>
		<option value="1">YES</option>
	</select> 
</div>

<div class="form-group col-md-2" id="rtgs_div">
	<label>RTGS/NEFT <font color="red">*</font></label>
	<select class="form-control" name="rtgs_sel" onchange="rtgsDiv(this.value);" id="rtgs_sel" required="required">
		<option value="0">NO</option>
		<option <?php if($fm_prev_year=='YES') {echo "disabled";} ?> value="1">YES</option>
	</select>
</div>

<?php 
$chk_gps_deposit = Qry($conn,"SELECT deposit FROM _gps_deposit_others WHERE consignor='$consignor_id'");

if(!$chk_gps_deposit){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_gps_deposit)>0)
{
	$row_chk_gps_deposit = fetchArray($chk_gps_deposit); 
	
	echo "<div class='form-group col-md-2' id='gps_deposit_div'>
		<label>GPS Deposit <font color='red'>*</font></label>
		<input type='number' value='$row_chk_gps_deposit[deposit]' readonly id='gps_deposit_at_adv' name='gps_deposit_at_adv' class='form-control' required='required' />
	</div>"; 
}
else
{
	echo "<input type='hidden' value='0' id='gps_deposit_at_adv' name='gps_deposit_at_adv'>";
}
?>

<!-- CHQ DIV START -->
 
	<div style="display:none" id="chq_input">
		<div class="form-group col-md-3">
			<label>Chq Adv Amt <font color="red">*</font></label>
			<input step="any" min="0" class="form-control" placeholder="Chq Adv" id="chq_amt" name="chqamt" type="number" /> 
		</div>
		
		<div class="form-group col-md-3">
			<label>Chq No. <font color="red">*</font></label>
			<input style="text-transform:uppercase" oninput='this.value=this.value.replace(/[^a-zA-Z-0-9]/,"")' class="form-control" placeholder="Chq No." name="chqno" type="text" />
		</div>
	</div>

<!-- CHQ DIV ENDS -->

</div>

 <!-- DIESEL DIV START -->
<div class="row" id="dsl_input" style="display:none;background:#FEE55F">

	<div class="form-group col-md-12">
	</div>
	
	<div class="form-group col-md-3">
		<label>Total Diesel Amount <font color="red">*</font></label>
		<input type="number" class="form-control" onblur="ChkPreDiesel(this.value)" min="0" id="dsl_amt" name="disamt" /> 
	</div>

	<div class="form-group col-md-9">
		<div class="row">
<?php
if($sys_diesel>0)
{
	echo "<div class='form-group col-md-12'>
		<b>Requested Diesel:</b> $sys_narration.
	</div>";
}
?>		
			<div id="result_adv_diesel" class="form-group col-md-12">
			</div>
		</div>
	</div>

<script>
function DeleteDiesel(id)
{
	var retVal = confirm("Do you really want to delete this Diesel Entry ?");
   if(retVal == true)
   {
	// alert(id);
	$("#loadicon").show();
	$.ajax({
		url: "delete_diesel_adv.php",
		method: "post",
		data:'id=' + id, 
		success: function(data){
		$("#result_freight").html(data);
	}})
   }	
}
</script>

<script type="text/javascript">
function LoadDiesel()
{	
	$('#loadicon').show();
	$.ajax({
		url: "fetch_diesel_for_fm_adv.php",
		method: "post",
		data:'frno=' + '<?php echo $idmemo; ?>' + '&type=' + 'ADV', 
		success: function(data){
		$("#result_adv_diesel").html(data);
	}})
}
</script>

</div> <!-- DIESEL DIV ENDS -->

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ifsc").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("btn_submit").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ifsc").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("btn_submit").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ifsc").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("btn_submit").removeAttribute("disabled");
		}
 } 			
</script>

<!-- RTGS DIV START -->
<div class="row" id="rtgs_input" style="display:none;background:#94DAEE;">

	<div class="form-group col-md-12">
		<label style="margin-top:5px;color:maroon">*** RTGS Section *** 
		<?php
		if($fm_prev_year=='YES')
		{
			echo "<span style='color:red'>Note : This is last year FM. You Can not pay by Rtgs.</span>";
		}
		?>
		</label>
	</div>
	
		<div class="form-group col-md-2">
			<label>Amount <font color="red">*</font></label>
			<input min="0" type="number" oninput="ValidateIFSC()" id="rtgs_amt" name="rtgs" class="form-control" placeholder="Rtgs Amount" />
		</div>
	
		<div class="form-group col-md-3">
			<label>Ac Holder <font color="red">*</font></label>
			<input oninput='this.value=this.value.replace(/[^a-z A-Z]/,"")' id="acname" type="text" name="holder" class="form-control" placeholder="AC Holder" />
		</div>
		
		<div class="form-group col-md-2">
			<label>Bank <font color="red">*</font></label>
			<input oninput='this.value=this.value.replace(/[^a-z A-Z]/,"")' id="bank" type="text" name="bank" class="form-control" placeholder="Bank Name" />
		</div>
		
		<div class="form-group col-md-3">
			<label>AC No <font color="red">*</font></label>
			<input id="acno" oninput='this.value=this.value.replace(/[^a-zA-Z-0-9]/,"")' type="number" minlength="6" name="acno" class="form-control" placeholder="Account No" />
		</div>
	
		<div class="form-group col-md-2">
			<label>IFSC <font color="red">*</font></label>
			<input id="ifsc" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" onblur="ValidateIFSC();" 
			pattern="[a-zA-Z]{4}[0]{1}[a-zA-Z0-9]{6}" minlength="11" maxlength="11" type="text" name="ifsc" class="form-control" 
			placeholder="IFSC Code" />
		</div>

</div>
<!-- RTGS DIV END -->

<div class="row" style="margin-top:5px;">

	<div class="form-group col-md-3">
		<label>Total Freight</label> <font color="red">*</font>
		<input step="any" min="0" class="form-control" id="total_freight" name="total_freight" readonly /> 
	</div>

	<div class="form-group col-md-3">
		<label>Total Advance</label> <font color="red">*</font>
		<input step="any" min="0" class="form-control" id="total_adv" name="total_adv_amount" type="number" readonly /> 
	</div>

	<div class="form-group col-md-3">
		<label>Balance Amount</label><font color="red">*</font>
		<input step="any" min="0" class="form-control" id="bamt" name="balance_left" type="number" readonly /> 
	</div>

</div> 			

<div class="row">

<div class="form-group col-md-4">
	<label>Narration</label>
	<textarea class="form-control" oninput='this.value=this.value.replace(/[^a-z, A-Z-0-9]/,"")' name="narration" placeholder="Narration"></textarea> 
</div>

<input type="hidden" name="fmemoid" value="<?php echo $idmemo; ?>" />

<div class="form-group col-md-4">
	<label>&nbsp;</label>
	<br />
	<button type="button" style="" class="btn btn-primary" data-dismiss="modal">Cancel</button>
	<input type="submit" style="" id="btn_submit" disabled class="btn btn-danger" value="Submit Advance" />
</div>

</div>

</div>
</form>
</div>
</div>
</div>

<?php
if($sys_diesel>0)
{
echo "<script>
		dslDiv('1');
		$('#dsl_amt').val('$sys_diesel');
		$('#dsl_amt').attr('min','$sys_diesel');
	</script>";
}

include("modal_claim_at_adv.php");
include("modal_diesel_adv.php");
?>	
 
<script type="text/javascript">
$(document).ready(function (e) {
$("#verifyAdvForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#btn_submit").attr("disabled", true);
	$.ajax({
	url: "./save_freight_memo_adv.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_freight").html(data);
	},
	error: function() 
	{} });}));});
</script>