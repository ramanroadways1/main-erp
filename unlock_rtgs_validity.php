<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form2").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./save_unlock_rtgs_validity.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_function1").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-8 col-md-offset-3">

<form autocomplete="off" id="Form2">	

<div class="row">
	
<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Request : Extend RTGS Approval Validity
		<br>
		<br>
		RTGS अप्रूवल की वैधता बढ़ाने के लिए अनुरोध करे
		</h4></center>
		<br />
	</div>
	
<script>	
function GetCRN(crn)
{
	if(crn!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
				url: "get_crn_details.php",
				data: 'crn=' + crn,
				type: "POST",
				success: function(data) {
				$("#result1").html(data);
			},
			error: function() {}
		});
	}
}
</script>	

<div id="result1"></div>
	
	<div class="form-group col-md-3">
		<label>Enter CRN <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="crn_no" onblur="GetCRN(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" name="crn_no" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>Party Name </label> 
		<input type="text" class="form-control" readonly id="party_name">
    </div>
	
	<div class="form-group col-md-3">
		<label>Payment Amount </label> 
		<input type="text" class="form-control" readonly id="amount">
    </div>
	
	<input type="hidden" name="rtgs_id" id="rtgs_id">

	<div class="form-group col-md-3">
		<label>Narration <font color="red"><sup>*</sup></font></label>
		<textarea style="font-size:12px" rows="1" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-/,.]/,'')" required="required" id="narration" name="narration" class="form-control"></textarea>
    </div>

	<div class="form-group col-md-6">  
		<input id="button_sub" disabled type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" name="submit" value="Submit Request" />
	</div>
	
</form>	 
	
	<div id="result_function1"></div>
	
	<div class="form-group col-md-12">
	<label>Pending Approvals. </label>
		<table class="table table-bordered" style="font-size:12px">
			<tr>
				<th>#</th>
				<th>Vou No</th>
				<th>Vehicle No</th>
				<th>CRN</th>
				<th>Party Name</th>
				<th>Amount</th>
				<th>Payment Date</th>
				<th>Narration</th>
				<th>Timestamp</th>
			</tr>
			
		<?php
		$qry = Qry($conn,"SELECT r.crn,r1.acname,r.narration,r1.fno,r1.tno,r1.amount,r1.pay_date,r.timestamp 
		FROM extend_rtgs_approval_validity AS r 
		LEFT OUTER JOIN rtgs_fm AS r1 ON r1.id = r.rtgs_id 
		WHERE r.approval='0' AND r.branch='$branch'");
		
		if(numRows($qry)==0)
		{
			echo "<tr><td colspan='9'>No record found..</td></tr>";
		}
		else
		{
			$sn=1;
			while($row = fetchArray($qry))
			{
				$pay_date = date("d-m-y",strtotime($row['pay_date']));
				$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
				
				echo "<tr>
					<td>$sn</td>
					<td>$row[fno]</td>
					<td>$row[tno]</td>
					<td>$row[crn]</td>
					<td>$row[acname]</td>
					<td>$row[amount]</td>
					<td>$pay_date</td>
					<td>$row[narration]</td>
					<td>$timestamp</td>
				</tr>";
			$sn++;
			}
		}
		?>		
		</table>
	</div>
	
</div>

</div>

</div>
</div>
</body>
</html>