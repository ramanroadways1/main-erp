<?php
require_once './connection.php';
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../../favicon.png" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
<link href="./google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="./data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 

::-webkit-scrollbar{
    width: 6px;
	height:6px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

<?php
include("./_loadicon.php");
include("./disable_right_click_for_index.php");
?> 

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Oxygen Movemenet Summary :</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>Vou_No</th>
					<th>Vehicle_No</th>
					<th>LR_Number</th>
					<th>From</th>
					<th>To</th>
					<th>Route_Type</th>
					<th>Branch</th>
					<th>User</th>
					<th>Vou_Date</th>
					<th>Vou_Count</th>
					<th>LR_Destination</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<style type="text/css">
  .by_rail_row_bg {
	background-color: lightblue !important;
  }
  .by_road_row_bg {
  background-color: #e1ffe1 !important;
  } 
</style>

<script type="text/javascript">
jQuery( document ).ready(function() {
$("#loadicon").show();
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
 
	"createdRow": function( row, data, dataIndex ) {
        if ( data[6] == "By_Rail" ) {        
        $(row).addClass('by_rail_row_bg'); 
        }
        // if ( data[6] == "By_Road" ) {        
        // $(row).addClass('by_road_row_bg'); 
        // }
	},
	  
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"bPaginate": true,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=./load_table.gif height=20> </center>"
        },
		// "dom": '<"toolbar">Bfrtip',
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
        "ajax": "_load_trip_history.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
	});  	
	// $("div.toolbar").html('<select class="form-control" style="font-size:10px;margin-bottom: 11.5px; width: 180px; max-height:28px; float:right; margin-left:10px;" id="table-filter"><option value="">Show ALL</option><option value="By_Rail">By_Rail</option> <option value="By_Road">By_Road</option></select>');	
});

 $(document).ready(function() { 
    var table = $("#user_data").DataTable(); 
    $("#table-filter").on("change", function(){
    table.search(this.value).draw();   
    });
    } );   
</script>

<script>
// FetchLRs();
</script>


<div id="func_results"></div>