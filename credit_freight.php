<?php
require_once 'connection.php';
?>	
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>
<link href="./google_font.css" rel="stylesheet">
	
<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CreditForm").on('submit',(function(e) {
$('#loadicon').show();	
e.preventDefault();
$("#button_sub").attr("disabled",true);	
$.ajax({
	url: "./save_credit_freight.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
{
	$("#function_result").html(data);
},
	error: function() 
	{} });}));});
</script>

<style>
.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
label{
	color:#FFF;
}
</style>

</head>

<script>
$(function() {
    $("#tno").autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
            $(event.target).focus();
			$("#button_sub").attr("disabled",true);
			alert('Truck No does not exists.');
		}
    }, 
    focus: function (event, ui){
		$("#button_sub").attr("disabled",false);
        return false;
    }
    });
  });
</script>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	<div class="col-md-2">
		<?php include 'sidebar.php';?>
	</div>
	
<form autocomplete="off" id="CreditForm">	
	
	<div class="col-md-7 col-md-offset-1 col-sm-offset-1">
	<br />
	<br />
		<div class="row">
		
	<div class="col-md-12">				
		<center><h4 style="color:#FFF">Credit - Own Truck Freight</h4></center>
		<br />
		<br />
		<br />
	</div>

	<div class="form-group col-md-12" id="prev_credits" style="display:none">
	</div>

	<div class="form-group col-md-4">
		<label>Cash/Cheque <font color="red">*</font></label>
		<select class="form-control" onchange="FreightSel(this.value);" name="select_id" required="required">
			<option value="CASH">Cash</option>
			<option value="CHEQUE">Cheque</option>
		</select>
	</div>
	
	<div class="form-group col-md-4">
		<label>Freight Type Adv/Bal ? <font color="red">*</font></label>
		<select class="form-control" name="freight_type" required="required">
			<option value="">--Freight Type--</option>
			<option value="ADVANCE">ADVANCE</option>
			<option value="BALANCE">BALANCE</option>
		</select>
	</div>
	
<script type="text/javascript">
function FreightSel(elem)
{
	if(elem=='CASH')
	{
		$('#chq_div').hide();
		$("#chqno").val('');
	}
	else
	{
		$('#chq_div').show();
		$("#chqno").attr('required',true);
	}
}
</script>	
	
	<div class="form-group col-md-4">			
		<label>Vehicle No. <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');$('#bilty_type').val('');" name="tno" id="tno" class="form-control" required />
	</div>
	
	<div class="form-group col-md-6">
		<label>Bilty Type <font color="red">*</font></label>
		<select class="form-control" onchange="BiltyType(this.value)" id="bilty_type" name="bilty_type" required="required">
			<option value="">Select an Option</option>
			<option value="MARKET">MARKET BILTY</option>
			<option value="OWN">OWN BILTY</option>
		</select>
	</div>
	
<script type="text/javascript">
function BiltyType(elem)
{
	$("#bilty_no").val('');
	$("#lr_no").val('');
	$("#lr_date").val('');
	$("#from_loc").val('');
	$("#to_loc").val('');
			
	$('#prev_credits').hide();
	
	if($('#tno').val()=='')
	{
		alert('Enter Vehicle Number First.');
		$('#bilty_type').val('');
	}
	else
	{
		$('#tno').attr('readonly',true);
	
		if(elem=='MARKET')
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "./fetch_mkt_bilty.php",
			data: 'tno=' + $('#tno').val(),
			type: "POST",
			success: function(data) {
				$("#bilty_no").html(data);
			},
			error: function() {}
			});
		
			$('#mkt_bilty_div').show();
			$("#bilty_no").attr('required',true);
			
			$("#lr_no").attr('readonly',true);
			$("#lr_date").attr('readonly',true);
			$("#from_loc").attr('readonly',true);
			$("#to_loc").attr('readonly',true);
		}
		else
		{
			$('#mkt_bilty_div').hide();
			$("#bilty_no").val('');
			
			$("#lr_no").attr('readonly',false);
			$("#lr_date").attr('readonly',false);
			$("#from_loc").attr('readonly',false);
			$("#to_loc").attr('readonly',false);
		}
	
	}
}
</script>

	<div id="mkt_bilty_div" style="display:none" class="form-group col-md-6">
		<label>Mkt Bilty No. <font color="red">*</font></label>
		<select id="bilty_no" onchange="FetchBiltyData(this.value)" name="bilty_no" class="form-control" required>
			<option value="">Select bilty</option>
		</select>
	</div>
	
<script>											
function FetchBiltyData(bilty)
{
	if(bilty!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./fetch_mkt_bilty_data.php",
		data: 'bilty=' + bilty,
		type: "POST",
		success: function(data) {
			$("#prev_credits").html(data);
		},
		error: function() {}
		});
	}
}
</script>

	<div class="form-group col-md-6">
		<label>LR No <font color="red">*</font></label>
		<input name="lr_no" id="lr_no" type="text" class="form-control" required />
	</div>

	<div class="form-group col-md-6">
		<label>LR Date <font color="red">*</font></label>
		<input name="lr_date" id="lr_date" max="<?php echo date("Y-m-d"); ?>" type="date" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	
	<div class="form-group col-md-6">
		<label>From Location <font color="red">*</font></label>
		<input name="from_loc" id="from_loc" type="text" class="form-control" required oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" />
	</div>
	
	<div class="form-group col-md-6">
		<label>To Location <font color="red">*</font></label>
		<input name="to_loc" id="to_loc" type="text" class="form-control" required oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" />
	</div>

	<div class="form-group col-md-6">
		<label>Credit Amount <font color="red">*</font></label>
		<input type="number" placeholder="Enter Amount" name="amount" min="1" id="amt" class="form-control" required />	
	</div>

	<div class="form-group col-md-6" id="chq_div" style="display:none">
		<label>Cheque No <font color="red">*</font></label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9]/,'')" name="chqno" id="chqno" class="form-control" />
	</div>
	
	<div class="form-group col-md-6">
		<label>Narration <font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'')" id="nrr" name="narration" class="form-control" required="required"></textarea>	
	</div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning pull-right" 
		name="submit" value="Credit Freight" />
	</div>
	
</div>

</div>
</div>
</form>

<div id="function_result"></div>
</body>
</html>