<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form2").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./save_hisab_approval.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_function1").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-8 col-md-offset-3">

<form autocomplete="off" id="Form2">	

<div class="row">
	
<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Request : e-Diary Hisab Approval
		<br>
		<br>
		<span style="font-size:14px;color:#FFF">ई-डायरी हिसाब अप्रूवल के लिए अनुरोध करे !</span>
		</h4></center>
		<br />
	</div>
	
<script>	
function GetTripNo(tno)
{
	if(tno!='')
	{
		// $("#loadicon").show();
		jQuery.ajax({
				url: "get_ediary_trip_no.php",
				data: 'tno=' + tno,
				type: "POST",
				success: function(data) {
				$("#trip_no").html(data);
			},
			error: function() {}
		});
	}
}

function DeleteReq(id)
{
	$("#loadicon").show();
	jQuery.ajax({
				url: "hisab_approval_delete.php",
				data: 'id=' + id,
				type: "POST",
				success: function(data) {
				$("#result_function1").html(data);
			},
			error: function() {}
		});
	
}
</script>	

<script type="text/javascript">
$(function() {
    $( "#tno" ).autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			alert('Truck No does not exists.');
			$("#tno").val("");
			$("#tno").focus();
		}
    }, 
    focus: function (event, ui){
        return false;
    }
    });
  });
</script>
	
	<div class="form-group col-md-4">
		<label>Vehicle Number <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="tno" onblur="GetTripNo(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-4">
		<label>Trip Number <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="trip_no" name="trip_no" type="text" class="form-control" readonly required />
    </div>

	<div class="form-group col-md-4">
		<label>Driver Name <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="driver_name" name="driver_name" type="text" class="form-control" required />
    </div>

	<div class="form-group col-md-4">
		<label>Trip <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="trip_station" name="trip_station" type="text" class="form-control" readonly required />
    </div>
	
	<div class="form-group col-md-12">  
		<input id="button_sub" disabled type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" name="submit" value="Submit Request" />
	</div>
	
</form>	 
	
	<div id="result_function1"></div>
	
	<div class="form-group col-md-12">
	<label>Pending Approvals. </label>
		<table class="table table-bordered" style="font-size:12px">
			<tr>
				<th>#</th>
				<th>Vehicle Number</th>
				<th>Trip Number</th>
				<th>Supervisor Name</th>
				<th>Branch Username</th>
				<th>Timestamp</th>
				<th>#Delete</th>
			</tr>
			
		<?php
		$qry = Qry($conn,"SELECT r.id,r.tno,r.trip_no,r.supervisor_name,r.branch,e.name,r.timestamp 
		FROM dairy.hisab_approval AS r 
		LEFT OUTER JOIN emp_attendance AS e ON e.code = r.branch_user 
		WHERE r.branch='$branch'");
		
		if(numRows($qry)==0)
		{
			echo "<tr><td colspan='6'>No record found..</td></tr>";
		}
		else
		{
			$sn=1;
			while($row = fetchArray($qry))
			{
				$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
				
				echo "<tr>
					<td>$sn</td>
					<td>$row[tno]</td>
					<td>$row[trip_no]</td>
					<td>$row[supervisor_name]</td>
					<td>$row[name]</td>
					<td>$timestamp</td>
					<td><button class='btn btn-xs btn-danger' type='button' onclick='DeleteReq($row[id])'><span class='fa fa-trash'></span> Delete</button></td>
				</tr>";
			$sn++;
			}
		}
		?>		
		</table>
	</div>
	
</div>

</div>

</div>
</div>
</body>
</html>