<?php
require_once './connection.php';

$date_today = date("Y-m-d");

$sql ="SELECT o.id,o.veh_no,o.frno,o.lrno,o.from_loc,o.to_loc,o.route_type,o.count,o.branch,DATE_FORMAT(o.timestamp,'%d-%m-%y') as date1,
e.name as emp_name,loc.name as lr_dest,o.running_trip,o.to_id,o.to_id_main 
FROM oxygen_olr AS o 
LEFT OUTER JOIN emp_attendance AS e ON e.code = o.branch_user 
LEFT OUTER JOIN station AS loc ON loc.id = o.to_id_main 
WHERE o.vou_status='1' AND o.branch='$branch' ORDER BY o.id ASC,o.lrno ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'frno', 'dt' => 1),
    array( 'db' => 'veh_no', 'dt' => 2),
    array( 'db' => 'lrno', 'dt' => 3),
    array( 'db' => 'from_loc', 'dt' => 4),
    array( 'db' => 'to_loc', 'dt' => 5),
    array( 'db' => 'route_type', 'dt' => 6),
    array( 'db' => 'branch', 'dt' => 7),
    array( 'db' => 'emp_name', 'dt' => 8), 
    array( 'db' => 'date1', 'dt' => 9), 
    array( 'db' => 'count', 'dt' => 10), 
    array( 'db' => 'lr_dest', 'dt' => 11), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('./scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);