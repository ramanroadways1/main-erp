<form id="attdForm" action="#" method="POST" autocomplete="off">
<div id="attd_modal" class="modal fade" role="dialog" style="background:#299C9B" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Attendance : <?php echo date("d-m-y",strtotime($date_attendance)); ?>
      </div>

	  <div id="attd1" style="display:none" class="alert alert-success"></div>
	 
	<div class="modal-body">
	  <div class="row">
			<div class="col-md-12 table-striped">
				<?php
				$attd=Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' 
				AND (status IN('3','2') OR (status='-1' AND terminate='0'))");
				
				if(numRows($attd)>0)
				{
				echo "<table class='table table-bordered' style='color:#000;font-size:11.5px;'>
						<tr>
							<th>Id</th>
							<th>Employee Name</th>
							<th>Emp. Code</th>
							<th>Action</th>
							<th>Remarks</th>
						</tr>";
				$srno=1;
				$counter=0;
					while($row_emp=fetchArray($attd))
					{
						echo "<tr>
								<td>$srno</td>
								<td>$row_emp[name]</td>
								<td>$row_emp[code]</td>
								<td>
									<input type='hidden' name='code[]' value='$row_emp[code]'>
									<input type='hidden' name='date_attendance' value='$date_attendance'>
									<label class='radio-inline'><input type='radio' value='P' name='attendance[$counter]' required>P</label>
									<label class='radio-inline'><input type='radio' value='A' name='attendance[$counter]'>A</label>
									<label class='radio-inline'><input type='radio' value='HD' name='attendance[$counter]'>Half day</label>
								</td>
								<td>
									<select style='border:1px solid #000;font-size:11px;height:28px;' name='remark[]' class='form-control' required>
										<option value='N'>NO Remarks</option>
										<option value='T'>Traveling</option>
										<option value='H'>Hospital</option>
										<option value='E'>Emergency</option>
									</select>
								</td>
							</tr>
						";
					$srno++;
					$counter++;
					}	
				echo "</table>";	
				}
				else
				{
					echo "<center><br><b><font color='red'>Employee data not found.</b></font>
					<br />
					<br />
					</center>
					<style>
					#save_attd{display:none}
					</style>
					";
				}
				?>
			</div>
      </div>
      </div>
	  
	   <div id="attd_result"></div>
	   
      <div class="modal-footer">
        <input type="submit" id="save_attd" style="color:#FFF;font-weight:bold;" class="btn btn-danger" value="Save Attendance" />
      </div>
    </div>

  </div>
</div>
</form>