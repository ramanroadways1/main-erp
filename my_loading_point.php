<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-md-offset-2">

<form autocomplete="off" id="Form2">	

<div class="row">
	
<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Branch's Loading Points</h4></center>
		<br />
	</div>
	
<div class="form-group col-md-12">
	<table class="table table-bordered" style="font-size:11px !important">
			<tr>
				<th>#</th>
				<th>Label</th>
				<th>Consignor</th>
				<th>Loading Location</th>
				<th>Pincode</th>
				<th>Username</th>
				<th>Timestamp</th>
				<th>#Map</th>
			</tr>
			
		<?php
		$qry = Qry($conn,"SELECT p.label,c.name as party,s.name as location,p.pincode,p._lat,p._long,p.branch_user,u.name as username,p.timestamp 
		FROM address_book_consignor AS p 
		LEFT OUTER JOIN consignor AS c ON c.id = p.consignor 
		LEFT OUTER JOIN station AS s ON s.id = p.from_id 
		LEFT OUTER JOIN emp_attendance AS u ON u.id = p.branch_user 
		WHERE p.branch='$branch'");
		
		if(numRows($qry)==0)
		{
			echo "<tr><td colspan='7'>No record found..</td></tr>";
		}
		else
		{
			$sn=1;
			while($row = fetchArray($qry))
			{
				if($row['username']=='')
				{
					$username1 = $row['branch_user'];
				}
				else
				{
					$username1 = $row['username'];
				}
				
				$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
				echo "<tr>
					<td>$sn</td>
					<td>$row[label]</td>
					<td>$row[party]</td>
					<td>$row[location]</td>
					<td>$row[pincode]</td>
					<td>$username1</td>
					<td>$timestamp</td>
					<td><a href='https://www.google.com/maps/place/$row[_lat],$row[_long]' target='_blank'><button class='btn btn-xs btn-warning'>View GMap</button></a></td>
				</tr>";
			$sn++;
			}
		}
		?>		
		</table>
	</div>
	
</div>

</div>

</div>
</div>
</body>
</html>