<?php
require("./connection.php");

$distance = escapeString($conn,($_POST['distance']));
$pincode = escapeString($conn,($_POST['pincode']));
$google_addr = escapeString($conn,($_POST['google_addr']));
$google_lat = escapeString($conn,($_POST['google_lat']));
$google_lng = escapeString($conn,($_POST['google_lng']));
$from_lat_long = escapeString($conn,($_POST['from_lat_long']));
$from_id = escapeString($conn,($_POST['from_id']));
$con1_id = escapeString($conn,($_POST['con1_id']));

if($distance>6)
{
	echo "<script>alert('Invalid loading point. Distance between location and loading point is: $distance KMs !');$('#loadicon').fadeOut('slow');$('#button_sub').attr('disabled',true);</script>";
	exit();
}

$_SESSION['loading_point_distance_km1'] = $distance;
$_SESSION['loading_point_google_pincode'] = $pincode;
$_SESSION['loading_point_google_addr'] = $google_addr;
$_SESSION['loading_point_google_lat'] = $google_lat;
$_SESSION['loading_point_google_lng'] = $google_lng;
$_SESSION['loading_point_lat_lng'] = $from_lat_long;
$_SESSION['loading_point_add_con1_id'] = $con1_id;
$_SESSION['loading_point_add_from_id'] = $from_id;
?>