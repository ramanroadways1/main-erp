<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$check_lr = Qry($conn,"SELECT c.vou_no,t.truck_no,t.date,t.fstation,t.tstation 
FROM claim_book_trans AS c 
LEFT JOIN freight_form_lr AS t ON t.frno=c.vou_no AND t.lrno=c.lrno 
WHERE c.lrno='$lrno' AND c.vehicle_type='MARKET' AND c.main_entry='1'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo '<option value="" style="font-size:13px">--select voucher--</option>';

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found in claim. Check LR number or update claim amount in POD.');
		$('#claim_add_btn_one').attr('disabled',true);
	</script>";
	HideLoadicon();
	exit();
}

while($row = fetchArray($check_lr))
{
	echo "<option style='font-size:13px' value='$row[vou_no]'>$row[vou_no]-> $row[truck_no]: $row[fstation] to $row[tstation]</option>";
}
 
echo "<script>
		$('#claim_add_btn_one').attr('disabled',false);
	</script>";
	HideLoadicon();
	exit();
?>