<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$name = trim(escapeString($conn,strtoupper($_POST['fullname'])));
$dob = escapeString($conn,strtoupper($_POST['birthdate']));
$age = escapeString($conn,strtoupper($_POST['age']));
$doj = escapeString($conn,strtoupper($_POST['joindate']));
$father = trim(escapeString($conn,strtoupper($_POST['fathername'])));
$father_occp = escapeString($conn,strtoupper($_POST['fatherocc']));

if($father_occp=='Others'){
	$father_occp2 = trim(escapeString($conn,strtoupper($_POST['fatherocc_other'])));
}
else{
	$father_occp2 = "";
}

$mother = trim(escapeString($conn,strtoupper($_POST['mothername'])));
$mother_occ = escapeString($conn,strtoupper($_POST['motherocc']));

if($mother_occ=='Others'){
	$mother_occ2 = trim(escapeString($conn,strtoupper($_POST['motherocc_other'])));
}
else{
	$mother_occ2 = "";
}

$aadhar_no = escapeString($conn,strtoupper($_POST['aadharno']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$addr_residence = trim(escapeString($conn,strtoupper($_POST['residence'])));
$addr_currentaddr = trim(escapeString($conn,strtoupper($_POST['currentaddr'])));
$pincode = escapeString($conn,strtoupper($_POST['pincode']));
$police_station = trim(escapeString($conn,strtoupper($_POST['police_station'])));
$qualification = escapeString($conn,strtoupper($_POST['qualification']));
$experience = escapeString($conn,strtoupper($_POST['experience']));
$bloodgroup = escapeString($conn,strtoupper($_POST['bloodgroup']));
$mobileno = escapeString($conn,strtoupper($_POST['mobileno']));
$alternateno = escapeString($conn,strtoupper($_POST['alternateno']));
$emailid = escapeString($conn,strtoupper($_POST['emailid']));
$emergency_mobile = escapeString($conn,strtoupper($_POST['emergencymo']));
$language = trim(escapeString($conn,strtoupper($_POST['language'])));
$dieases = trim(escapeString($conn,strtoupper($_POST['dieases'])));
$maritial = escapeString($conn,strtoupper($_POST['maritial']));

if($maritial=="MARRIED"){
	$wife_name = trim(escapeString($conn,strtoupper($_POST['wifename'])));
	$wife_occ = trim(escapeString($conn,strtoupper($_POST['wifeocc'])));
	$child_no = escapeString($conn,strtoupper($_POST['child']));
}
else{
	$wife_name = "";
	$wife_occ = "";
	$child_no = "";
}

if(isset($_POST['bank_checkbox']))
{
	$ac_holder = trim(escapeString($conn,strtoupper($_POST['acc_fullname'])));
	$ac_no = trim(escapeString($conn,strtoupper($_POST['acc_number'])));
	$ac_bank = trim(escapeString($conn,strtoupper($_POST['acc_bank'])));
	$ac_ifsc = trim(escapeString($conn,strtoupper($_POST['acc_ifsc'])));
	
	if($ac_holder==''){
		echo "<script>
			alert('Please Check A/c holder !');
			$('#loadicon').hide();
			$('#button_add').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_no==''){
		echo "<script>
			alert('Please Check A/c number !');
			$('#loadicon').hide();
			$('#button_add').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_bank==''){
		echo "<script>
			alert('Please Check Bank name !');
			$('#loadicon').hide();
			$('#button_add').attr('disabled',false);
		</script>";
		exit();
	}
	
	if($ac_ifsc==''){
		echo "<script>
			alert('Please Check IFSC code !');
			$('#loadicon').hide();
			$('#button_add').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(strlen($ac_ifsc)!='11'){
		echo "<script>
			alert('Please Check IFSC code !');
			$('#loadicon').hide();
			$('#button_add').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	$ac_holder = "";
	$ac_no = "";
	$ac_bank = "";
	$ac_ifsc = "";
}

$gaurantor_name = trim(escapeString($conn,strtoupper($_POST['gaurantor_name'])));
$gaurantor_mobile = trim(escapeString($conn,strtoupper($_POST['gaurantor_mobile'])));
$gaurantor_address = trim(escapeString($conn,strtoupper($_POST['gaurantor_address'])));
$relative_name = escapeString($conn,strtoupper($_POST['relative_name']));
$relative_mobile = escapeString($conn,strtoupper($_POST['relative_mobile']));
$relative_address = escapeString($conn,strtoupper($_POST['relative_address']));
$otp = escapeString($conn,strtoupper($_POST['otp']));

if(strlen($pincode)!=6){
	echo "<script>
		alert('Please Enter Valid PINCODE !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($mobileno!='' AND strlen($mobileno)!=10){
	echo "<script>
		alert('Please Check Your Mobile Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($emergency_mobile!='' AND strlen($emergency_mobile)!=10){
	echo "<script>
		alert('Please Check Emergency Mobile Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($alternateno!='' AND strlen($alternateno)!=10){
	echo "<script>
		alert('Please Check Alternate Mobile Number. Enter Valid mobile number or leave empty !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($gaurantor_mobile!='' AND strlen($gaurantor_mobile)!=10){
	echo "<script>
		alert('Please Check Gaurantor\'s Mobile Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($relative_mobile!='' AND strlen($relative_mobile)!=10){
	echo "<script>
		alert('Please Check Relatives\'s Mobile Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if(strlen($aadhar_no)!=12){
	echo "<script>
		alert('Please Check Aadhar Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}

if($pan_no!='' AND (strlen($pan_no)!=10)){
	echo "<script>
		alert('Please Check PAN Number !');
		$('#loadicon').hide();
		$('#button_add').attr('disabled',false);
	</script>";
	exit();
}


$Check_Mobile = Qry($conn,"SELECT name FROM emp_attendance WHERE mobile_no='$mobileno'");

if(!$Check_Mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","./");
}

if(numRows($Check_Mobile)>0)
{
	$row_Check = fetchArray($Check_Mobile);
	
	echo "<script>
		alert('Duplicate Mobile Number. Mobile Number Registered with $row_Check[name] !');
		$('#button_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

VerifyOTP($otp,"button_add");

$get_emp_code = Qry($conn,"SELECT code FROM emp_attendance ORDER BY id DESC LIMIT 1");

if(!$get_emp_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_code = fetchArray($get_emp_code);
$emp_code = ++$row_code['code'];

$emp_photo = $_FILES['emp_photo']['tmp_name'];
$aadhar_copy_front = $_FILES['aadhar_copy_front']['tmp_name'];
$aadhar_copy_rear = $_FILES['aadhar_copy_rear']['tmp_name'];

if($pan_no!="")
{
	$pan_copy = $_FILES['pan_copy']['tmp_name'];
	$targetPath_pan = "employee_data/pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
}

$targetPath_photo = "employee_data/photo/".mt_rand().date('dmYHis').".".pathinfo($_FILES['emp_photo']['name'],PATHINFO_EXTENSION);
$targetPath_aadhar_front = "employee_data/aadhar/".mt_rand().date('dmYHis').".".pathinfo($_FILES['aadhar_copy_front']['name'],PATHINFO_EXTENSION);
$targetPath_aadhar_rear = "employee_data/aadhar/".mt_rand().date('dmYHis').".".pathinfo($_FILES['aadhar_copy_rear']['name'],PATHINFO_EXTENSION);

ImageUpload(600,400,$emp_photo);
			
if(!move_uploaded_file($emp_photo, $targetPath_photo)){
	echo "<script>alert('Error while uploading Employee\'s Photo !');$('#loadicon').hide();$('#button_add').attr('disabled',false);</script>";
	exit();
}

ImageUpload(600,400,$aadhar_copy_front);
ImageUpload(600,400,$aadhar_copy_rear);
			
if(!move_uploaded_file($aadhar_copy_front, $targetPath_aadhar_front)){
	unlink($targetPath_aadhar_front);
	echo "<script>alert('Error while uploading Employee\'s Aadhar - front !');$('#loadicon').hide();$('#button_add').attr('disabled',false);</script>";
	exit();
}

if(!move_uploaded_file($aadhar_copy_rear, $targetPath_aadhar_rear)){
	unlink($targetPath_aadhar_rear);
	echo "<script>alert('Error while uploading Employee\'s Aadhar - rear !');$('#loadicon').hide();$('#button_add').attr('disabled',false);</script>";
	exit();
}

if($pan_no!="")
{
	ImageUpload(600,400,$pan_copy);
	if(!move_uploaded_file($pan_copy, $targetPath_pan)){
		unlink($targetPath_photo);
		unlink($targetPath_aadhar_front);
		unlink($targetPath_aadhar_rear);
		echo "<script>alert('Error while uploading Employee\'s Photo !');$('#loadicon').hide();$('#button_add').attr('disabled',false);</script>";
		exit();
	}
}
else
{
	$targetPath_pan="";
}
 
$insert = Qry($conn,"INSERT INTO emp_attendance(name,code,branch,user_code,join_date,father_name,birth_date,residenceaddr,currentaddr,blood_group,
qualification,work_experience,mobile_no,alternate_mobile,email_id,guarantor_name,guarantor_mobile,guarantor_addr,acc_holder,acc_no,
acc_ifsc,acc_bank,acc_pan,image,ageyrs,fatherocc,father_occ_other,mother_occ_other,mothername,motherocc,aaadharno,aadharphoto,aadharphoto_rear,panphoto,pincode,policestation,emergencyphone,
langknown,majordiseases,maritialstatus,wifename,wifeocc,children,relativename,status,timestamp) VALUES ('$name','$emp_code',
'$branch','$branch_sub_user','$doj','$father','$dob','$addr_residence','$addr_currentaddr','$bloodgroup','$qualification','$experience','$mobileno',
'$alternateno','$emailid','$gaurantor_name','$gaurantor_mobile','$gaurantor_address','$ac_holder','$ac_no','$ac_ifsc','$ac_bank','$pan_no',
'$targetPath_photo','$age','$father_occp','$father_occp2','$mother_occ2','$mother','$mother_occ','$aadhar_no','$targetPath_aadhar_front','$targetPath_aadhar_rear','$targetPath_pan','$pincode',
'$police_station','$emergency_mobile','$language','$dieases','$maritial','$wife_name','$wife_occ','$child_no','$relative_name','0',
'$timestamp')");

if(!$get_emp_code){
	unlink($targetPath_photo);
	unlink($targetPath_aadhar_front);	
	unlink($targetPath_aadhar_rear);	
	if($pan_no!="")
	{
		unlink($targetPath_pan);	
	}
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
	echo "<script>
		alert('Employee: $name added successfully. Manager Approved required !');
		window.location.href='./employee_add_new.php';
	</script>";
	exit();		   
?>