<?php
require_once("./connection.php");

$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

echo "<script>
	$('#ext_val_ewb_btn_save').attr('disabled',true);
	$('#fetch_btn_ewb_ext_modal').attr('disabled',true);
	$('#ewb_extd_verify_btn').attr('disabled',true);
	$('#loadicon_ewb_val').show();
</script>";

$search_by = escapeString($conn,($_POST['search_by']));
$vou_no = escapeString($conn,($_POST['vou_no']));

if($vou_no=='')
{
	echo "<span style='color:red;font-size:13px'>Error: Voucher number not found !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if($search_by=='LR')
{
	$get_ewb_details = Qry($conn,"SELECT e.id,e2.lrno,e2.branch,e2.ewb_no,date(e2.ewb_expiry) as exp_date,e2.ewb_expiry as ewb_expiry2,e.toPincode,l.fstation,l.tstation,l.truck_no,l.company 
	FROM _eway_bill_lr_wise AS e 
	LEFT JOIN _eway_bill_validity AS e2 ON e2.ewb_id=e.id 
	LEFT JOIN lr_sample AS l ON l.id=e.lr_id 
	WHERE e.lrno='$vou_no' AND e2.branch='$branch'");
}
else if($search_by=='EWB')
{
	$get_ewb_details = Qry($conn,"SELECT e.id,e2.lrno,e2.branch,e2.ewb_no,date(e2.ewb_expiry) as exp_date,e2.ewb_expiry as ewb_expiry2,e.toPincode,l.fstation,l.tstation,l.truck_no,l.company 
	FROM _eway_bill_lr_wise AS e 
	LEFT JOIN _eway_bill_validity AS e2 ON e2.ewb_id=e.id 
	LEFT JOIN lr_sample AS l ON l.id=e.lr_id 
	WHERE e.ewb_no='$vou_no' AND e2.branch='$branch'");
}
else
{
	echo "<span style='color:red;font-size:13px'>Error: invalid option selected !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if(!$get_ewb_details)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if(numRows($get_ewb_details)==0)
{
	echo "<span style='color:red;font-size:13px'>No record found ! Please check eway-bill number or LR number.</span>";
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$row_ewb = fetchArray($get_ewb_details);

// if($row_ewb['branch'] != $branch)
// {
	// echo "<span style='color:red;font-size:13px'>Error: Voucher not created from your branch !</span>";
	// echo "<script>
		// $('#ext_val_ewb_btn_save').attr('disabled',true);
		// $('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		// $('#loadicon_ewb_val').fadeOut('slow');
	// </script>";
	// exit();
// }

if($row_ewb['exp_date']==0 || $row_ewb['exp_date']=='')
{
	echo "<span style='color:red;font-size:13px'>Eway-bill does not belongs to our company !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if($row_ewb['exp_date']!=date("Y-m-d"))
{
	echo "<span style='color:red;font-size:13px'>Eway-bill expiry date is: $row_ewb[exp_date] !</span>";
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$fromTime_ewb_ext = DateTime::createFromFormat('H:i a', "18:00 pm");
$toTime_ewb_ext = DateTime::createFromFormat('H:i a', "23:40 pm");

if(DateTime::createFromFormat('H:i a', date("H:i a")) > $fromTime_ewb_ext && DateTime::createFromFormat('H:i a', date("H:i a")) < $toTime_ewb_ext)		
{
	
}
else
{
	echo "<span style='color:red;font-size:13px'>Extension Timing is: 06:00 PM to 11:30 PM !</span>
	<script>
		$('#fetch_btn_ewb_ext_modal').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$dest_pincode = $row_ewb['toPincode'];
$truck_no = $row_ewb['truck_no'];
$from = $row_ewb['fstation'];
$to = $row_ewb['tstation'];

$_SESSION['modal_ewb_ext_dest_pincode'] = $dest_pincode;
$_SESSION['modal_ewb_ext_ewb_no'] = $row_ewb['ewb_no'];
$_SESSION['modal_ewb_ext_company'] = $row_ewb['company'];
$_SESSION['modal_ewb_ext_ewb_id'] = $row_ewb['id'];
$_SESSION['modal_ewb_ext_prev_exp_date'] = $row_ewb['ewb_expiry2'];

echo "<script>
		$('.ewb_search_by_sel').attr('disabled',true);
		$('#ewb_search_by_$search_by').attr('disabled',false);
		$('#modal_ewb_ewb_no').attr('readonly',true);
		$('#modal_ewb_lrno').attr('readonly',true);
		$('#fetch_btn_ewb_ext_modal').attr('disabled',true);
		
		$('#modal_ewb_ewb_no_ip').val('$row_ewb[ewb_no]');
		$('#modal_ewb_from').val('$from');
		$('#ewb_ext_modal_dest_pincode').html('Pincode: $dest_pincode');
		$('#modal_ewb_to').val('$to');
		$('#modal_ewb_truck_no').val('$truck_no');
		
		$('#ewb_extd_verify_btn').attr('disabled',false);
		$('#ext_val_ewb_btn_save').attr('disabled',true);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
?>