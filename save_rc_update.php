<?php
require_once 'connection.php';

$timestamp = date("Y-m-d H:i:s");

$veh_no = escapeString($conn,strtoupper($_POST['veh_no']));
$id = escapeString($conn,strtoupper($_POST['id']));

$verify = Qry($conn,"SELECT token_no,reg_no,branch,rc FROM asset_vehicle WHERE id='$id'");
if(!$verify){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($verify)==0)
{
	echo "<script>
		alert('Vehicle not found.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$row_veh = fetchArray($verify);

if($row_veh['reg_no']==''){
	$veh_no_db = $row_veh['token_no'];
}
else{
	$veh_no_db = $row_veh['reg_no'];
}

if($veh_no_db!=$veh_no)
{
	echo "<script>
		alert('Vehicle not verified.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['branch']!=$branch)
{
	echo "<script>
		alert('Vehicle not belongs to your branch.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

if($row_veh['rc']=="1")
{
	echo "<script>
		alert('RC COPY already updated of this vehicle.');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	if(!in_array($_FILES['rc_front']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed. Rc front');
			$('#rc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if(!in_array($_FILES['rc_rear']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed. Rc rear');
			$('#rc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
    
$sourcePath_front = $_FILES['rc_front']['tmp_name'];
$sourcePath_rear = $_FILES['rc_rear']['tmp_name'];

	$targetPath_front = "asset_upload/veh_rc/".date('dmYHis').mt_rand().".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);
	$targetPath_rear = "asset_upload/veh_rc/".date('dmYHis').mt_rand().".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);

	ImageUpload(1000,700,$sourcePath_front);
	
	if(!move_uploaded_file($sourcePath_front, $targetPath_front)) 
	{
		echo "<script>
			alert('Error : Unable to upload Rc Front Copy.');
			$('#rc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	ImageUpload(1000,700,$sourcePath_rear);
	
	if(!move_uploaded_file($sourcePath_rear, $targetPath_rear)) 
	{
		unlink($targetPath_front);
		echo "<script>
			alert('Error : Unable to upload Rc Rear Copy.');
			$('#rc_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

StartCommit($conn);
$flag = true;

$update_rc = Qry($conn,"UPDATE asset_vehicle SET rc='1' WHERE id='$id'");

if(!$update_rc){
	unlink($targetPath_front);
	unlink($targetPath_rear);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_rc_copy = Qry($conn,"UPDATE asset_vehicle_docs SET rc_front='$targetPath_front',rc_rear='$targetPath_rear',rc_time='$timestamp' 
WHERE veh_id='$id'");

if(!$update_rc_copy){
	unlink($targetPath_front);
	unlink($targetPath_rear);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
			alert('RC Updated Successfully !');
			window.location.href='asset_vehicle_view.php';
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>