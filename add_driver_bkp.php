<?php
require_once './connection.php'; 

$branch=escapeString($conn,strtoupper($_SESSION['user']));

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

echo "<script>
		alert('Error:  On hold !');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
	
$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$mobile2 = escapeString($conn,strtoupper($_POST['mobile2']));
$lic_no = trim(escapeString($conn,strtoupper($_POST['lic_no'])));
$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($mobile2,'0')===0 AND $mobile2!='')
{
	echo "<script>
		alert('Error: Check mobile number-2.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($lic_no)<6)
{
	echo "<script>
		alert('Lic number must be atleast 6 digit.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$crop_lic=substr($lic_no,-5);
//$crop_lic2=substr($d_lic_no,-8);
$name = $name." ".$crop_lic;

$chk_lic_no = Qry($conn,"SELECT name FROM mk_driver WHERE pan='$lic_no' AND pan!='NA' AND pan!=''");

if(numRows($chk_lic_no)>0)
{
	$row_name = fetchArray($chk_lic_no);
	
	echo "<script type='text/javascript'>
		alert('License No : $lic_no. Already exists with Driver $row_name[name].'); 
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$chk_name = Qry($conn,"SELECT pan FROM mk_driver WHERE name='$name'");

if(numRows($chk_name)>0)
{
	$row_pan = fetchArray($chk_name);
	
		echo "<script type='text/javascript'>
			alert('Driver : $name already exists with License No $row_pan[pan].'); 
			$('#add_driver_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();	
}

$sourcePath = $_FILES['lic_copy']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['lic_copy']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fix_name = mt_rand().date('dmYHis');

$targetPath="driver_pan/".$fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);

// ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	Redirect("Unable to Upload License Copy.","./smemo.php");
	exit();
}
else
{
	$chk_ocr = Qry($conn,"SELECT active FROM _ocr WHERE branch='$branch'");

	$row_ocr = fetchArray($chk_ocr);
	
	if($row_ocr['active']=="1")
	{
		$fileName = $targetPath;
		// $documentNumber = $pan_no;
		include '../Google_Cloud_Vision/dl_validation.php'; 
		
		if(isset($_SESSION['error_log_dl']))
		{
			$_SESSION['error_log_dl'] = mysqli_real_escape_string($conn,$_SESSION['error_log_dl']);
		}
		
		if(isset($_SESSION['error_log_pan']))
		{
			$_SESSION['error_log_pan'] = mysqli_real_escape_string($conn,$_SESSION['error_log_pan']);
		}
		
		
		if($response_photo == 'NOT_FOUND')
		{
			mysqli_set_charset($conn, 'utf8');
			
			$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('MKT_DRIVER_DL','$lic_no','$response_photo','NOT_FOUND','".$_SESSION["error_log_dl"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			unlink($targetPath);
			// errorLog("OCR DL: DOC NO. NOT_FOUND. DL: $lic_no. Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
			echo "<script type='text/javascript'> 
				alert('Error while scanning the document. Please upload a valid and scannable document !');
				$('#loadicon').hide();
				$('#add_driver_button').attr('disabled',false);
			</script>";
			exit();
		}
		else if($response_photo == 'Invalid_Response')
		{
			mysqli_set_charset($conn, 'utf8');
			
			$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('MKT_DRIVER_DL','$lic_no','$response_photo','Invalid_Response','".$_SESSION["error_log_dl"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			unlink($targetPath);
			// errorLog("OCR DL: Invalid response. DL: $lic_no. Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
			echo "<script type='text/javascript'> 
				alert('Error while scanning the document. Please upload a valid and scannable document !');
				$('#loadicon').hide();
				$('#add_driver_button').attr('disabled',false);
			</script>";
			exit();
		}
		
		if($response_photo!=$lic_no)
		{
			mysqli_set_charset($conn, 'utf8');
			
			$error_insert = Qry($conn,"INSERT INTO _ocr_error_log(doc_type,input_doc_no,ocr_doc_no,error_desc,ocr_response,branch,branch_user,
			timestamp) VALUES ('MKT_DRIVER_DL','$lic_no','$response_photo','DL_NOT_MATCHING','".$_SESSION["error_log_dl"]."','$branch','$branch_sub_user',
			'$timestamp')");
				
			if(!$error_insert){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			unlink($targetPath);
			// errorLog("OCR DL: DL VERIFICATION FAILED. DL:$lic_no. OCR-DL:$response_photo  Branch: $branch. User: $branch_sub_user.",$conn,$page_name,__LINE__);
			echo "<script type='text/javascript'> 
				alert('DL verification failed ! Please check DL number.');
				$('#loadicon').hide();
				$('#add_driver_button').attr('disabled',false);
			</script>";
			exit();
		}
	}
	
	$insert = Qry($conn,"INSERT INTO mk_driver(name,mo1,mo2,pan,full,up5,branch,branch_user,timestamp) VALUES 
	('$name','$mobile','$mobile2','$lic_no','$addr','$targetPath','$branch','$branch_sub_user','$timestamp')");
	
	if(!$insert){
		unlink($targetPath);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	echo "<script type='text/javascript'> 
		alert('Driver : $name. Added Successfully..');
		$('#loadicon').hide();
		$('#add_driver_button').attr('disabled',false);
		$('#close_add_driver').click();
		$('#DriverForm').trigger('reset');
	</script>";
	exit();
}
?>