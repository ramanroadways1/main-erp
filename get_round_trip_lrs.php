<?php
require_once 'connection.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT r.id,r.round_trip_lrno,l.lrno,l.truck_no,DATE_FORMAT(l.date,'%d-%m-%y') as lr_date,l.fstation,l.tstation 
FROM lr_round_trip AS r 
LEFT JOIN lr_sample AS l ON l.id=r.lr_id  
WHERE r.is_cleared='0' ORDER BY r.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'round_trip_lrno', 'dt' => 1),
    array( 'db' => 'lrno', 'dt' => 2),
    array( 'db' => 'truck_no', 'dt' => 3), 
    array( 'db' => 'lr_date', 'dt' => 4), 
    array( 'db' => 'fstation', 'dt' => 5),  
    array( 'db' => 'tstation', 'dt' => 6),
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);