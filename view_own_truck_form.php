<?php
require_once 'connection.php';
$vou_no=escapeString($conn,$_POST['vou_no']);
?>
<!DOCTYPE html>
<html lang="en">
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

</head>

<?php 
	// include("./_loadicon.php");
	include("./disable_right_click_for_index.php");
?>

<style>
@media (min-width: 768px)
{
  .modal-xl {
	width: 100%;
   max-width:1300px;
  }
}

.ui-autocomplete { z-index:2147483647; }

.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}

.modal {
  overflow-y:auto;
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>

<style type="text/css" media="print">
@media print {
body {
   zoom:70%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.container-fluid * { visibility: visible}
.container-fluid { position: absolute; top: 0; left: 0; }
}

label{
	font-size:13px;
}
</style>

<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php
$getRecord = Qry($conn,"SELECT lrno,frno,branch,date,truck_no,fstation,tstation,consignor,consignee,wt12,weight,crossing,cross_to,
done FROM freight_form_lr WHERE frno='$vou_no'");

if(!$getRecord){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getRecord)==0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

echo "<button type='button' onclick='window.close();' class='btn btn-sm btn-primary' style='margin-top:10px;margin-left:10px;'>Close Window</button>
<br>
<br>
<div class='container-fluid'>
<span style='color:red;font-size:16px;'>Own Truck Form : $vou_no</span>
<br />
<br />
	<table style='font-size:12px;' class='table table-bordered'>
	<tr>
		<th>Id</th>
		<th>LR No</th>
		<th>Branch</th>
		<th>LR_Date</th>
		<th>Vehicle_No</th>
		<th>From & To Loc.</th>
		<th>Consignor & Consignee</th>
		<th>ActWt</th>
		<th>ChargeWt</th>
		<th>Crossing</th>
		<th>Crossing<br>Station</th>
		<th>e-Diary Trip</th>
	</tr>";

	$no=1;	
	$act_wt_total = 0;
	$chrg_wt_total = 0;
	while($row = fetchArray($getRecord))
	{
		if($row['done']==1)
		{
			$trip_done="<span style='color:green'>Done</span>";
		}
		else
		{
			$trip_done="";
		}
	
	$act_wt_total=$act_wt_total+$row['wt12'];
	$chrg_wt_total=$chrg_wt_total+$row['weight'];
	
	echo "<tr>
			<td>$no</td>
			<td>$row[lrno]</td>
			<td>$row[branch]</td>
			<td>$row[date]</td>
			<td>$row[truck_no]</td>
			<td>From: $row[fstation] <br> To: $row[tstation]</td>
			<td>Consignor: $row[consignor]<br>Consignee: $row[consignee]</td>
			<td>$row[wt12]</td>
			<td>$row[weight]</td>
			<td>$row[crossing]</td>
			<td>$row[cross_to]</td>
			<td>$trip_done</td>
		</tr>";
		$no++;	
	}
	echo "<tr>
			<td colspan='7'>Total : </td>
			<td><b>$act_wt_total</b></td>
			<td><b>$chrg_wt_total</b></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>";
		
echo "</table></div>";
	echo "<script>$('#window_loadicon').fadeOut();</script>";
exit();
?>