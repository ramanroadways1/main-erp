<?php
require_once("connection.php");

$lic_id = escapeString($conn,strtoupper($_POST['lic_id']));
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");
	
$get_driver = Qry($conn,"SELECT pan as lic_no,mo1,last_verify FROM mk_driver WHERE id='$lic_id'");
	
if(!$get_driver){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($get_driver)==0){
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'Warning !!!',
	text: 'Driver not found..'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}

$row_driver = fetchArray($get_driver);
$lic_no = preg_replace("/[^a-zA-Z0-9]+/", "",$row_driver['lic_no']);

// for hold api

$_SESSION['verify_driver'] = $lic_no;
$_SESSION['verify_driver_id'] = $lic_id;
$_SESSION['verify_driver_mobile_no'] = $row_driver['mo1'];

echo "<script>
		// $('#close_verify_modal')[0].click();
		// $('#function_result').html('');
		// $('#button_print_modal_dl')[0].click();
		// $('#button_verify_1')[0].click();
		$('#d_lic_no').attr('readonly',true);
		$('#verify_dl_div').show();
		$('#verify_btn_div_dl').hide();
		$('#is_dl_verified').val('1')
		$('#loadicon').hide();
	</script>";
	exit();

// for hold api

/*

$hourdiff = round((strtotime($timestamp) - strtotime($row_driver['last_verify']))/3600, 1);

if($hourdiff==0 || $hourdiff>36)
{
	$curl = curl_init();
	$dl_no = preg_replace("/[^a-zA-Z0-9]+/", "",$lic_no);
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://prod.aadhaarapi.com/verify-dl-advance/v2",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>"{
	  \"dl_no\": \"$dl_no\",
	  \"consent\": \"Y\",
	  \"dob\":\"\",
	  \"consent_text\": \"Details fetched for $dl_no verification on behalf of RRPL.\"}",
	  CURLOPT_HTTPHEADER => array(
		"qt_api_key: 74b6283a-c1b0-4199-a7e6-f2bc2f176b83",
	    "qt_agency_id: 80c497b4-bc77-42f5-8e9f-56ec2c4beb0c",
	    "Content-Type: application/json"
	  ),
	));

	$output = curl_exec($curl); 
	curl_close($curl); 
 
	$response = json_decode($output, true);
	@$row = $response['result'];
	@$rowcode = $response['transaction_status'];
	@$req_id = $response['id'];
	
	if($rowcode=="1")
	{ 
		$covDetails = json_encode($row['cov_details']);
		@$dob = date_format(date_create_from_format("d-m-Y",$row['dob']), "Y-m-d");
		@$expiryDate = date_format(date_create_from_format("d-m-Y",$row['expiryDate']), "Y-m-d");
		@$issue_date = date_format(date_create_from_format("d-m-Y",$row['issue_date']), "Y-m-d");
		@$endorse_dt = date_format(date_create_from_format("d-m-Y",$row['endorse_dt']), "Y-m-d");
		@$statusFrom = date_format(date_create_from_format("d-m-Y",$row['statusDetails']['from']), "Y-m-d");
		@$statusTo = date_format(date_create_from_format("d-m-Y",$row['statusDetails']['to']), "Y-m-d");
		
		@$nonTransportfrom = date_format(date_create_from_format("d-m-Y",$row['validity']['nonTransport']['from']), "Y-m-d");
		@$nonTransportto = date_format(date_create_from_format("d-m-Y",$row['validity']['nonTransport']['to']), "Y-m-d");
		@$transportfrom = date_format(date_create_from_format("d-m-Y",$row['validity']['transport']['from']), "Y-m-d");
		@$transportto = date_format(date_create_from_format("d-m-Y",$row['validity']['transport']['to']), "Y-m-d");

		$b64 = $row['img'];
		$bin = base64_decode($b64);
		@$im = imageCreateFromString($bin);
		// if (!$im) {
		//   die('Base64 value is not a valid image');
		// }
		$img_file = 'images/'.$dl_no.uniqid().'.png';
		imagepng($im, $img_file, 0);

			$dlNumber = preg_replace("/[^0-9a-zA-Z]/", "", $row['dlNumber']);

			$insert_fetched_dl_data = Qry($conn,"INSERT INTO zoop_dl (`request_id`, `request_timestamp`, `addressLine1`, 
			`completeAddress1`, `country1`, 
			`district1`, `pin1`, `state1`, `addressLine2`, `completeAddress2`, `country2`, `district2`, `pin2`, `state2`, 
			`blood_group`, `cov_details`, `dl_number`, `dob`, `expiryDate`, `father/husband`, `img`, `issue_date`, `name`, 
			`state`, `endorse_dt`, `endorse_no`, `status`, `statusDetails_from`, `statusDetails_remarks`, `statusDetails_to`,
			`nonTransportFrom`, `nonTransportTo`, `TransportFrom`, `TransportTo`, `branch`, `branch_user`) VALUES 
			('$response[id]', '$response[request_timestamp]', '".$row['address'][0]['addressLine1']."', 
			'".$row['address'][0]['completeAddress']."', '".$row['address'][0]['country']."', 
			'".$row['address'][0]['district']."', '".$row['address'][0]['pin']."', '".$row['address'][0]['state']."', 
			'".$row['address'][1]['addressLine1']."', '".$row['address'][1]['completeAddress']."', 
			'".$row['address'][1]['country']."', '".$row['address'][1]['district']."', '".$row['address'][1]['pin']."', 
			'".$row['address'][1]['state']."', '$row[blood_group]', '".$covDetails."', '$dlNumber', '$dob', '$expiryDate', 
			'".$row['father/husband']."', '$img_file', '$issue_date', '$row[name]', '$row[state]', '$endorse_dt', 
			'$row[endorse_no]', '$row[status]', '$statusFrom', '".$row['statusDetails']['remarks']."', '$statusTo', 
			'$nonTransportfrom', '$nonTransportto', '$transportfrom', '$transportto', '$branch', '$branch_sub_user')");


			if(!$insert_fetched_dl_data){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$udpate_new_timestamp = Qry($conn,"UPDATE mk_driver SET last_verify='$timestamp' WHERE id='$lic_id'");

			if(!$udpate_new_timestamp){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$insert_err2 = Qry($conn,"INSERT INTO zoop_error_log(req_id,api_name,doc_no,doc_no2,branch,branch_user,timestamp) 
			VALUES ('$req_id','DL','$lic_id','$lic_no','$branch','$branch_sub_user','$timestamp')");
		
			if(!$insert_err2){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
	}
	else 
	{
		$api_err_code = $response['response_code'];
		$api_err_msg = $response['response_msg'];
		
		if($api_err_code=="110")
		{
			$insert_err = Qry($conn,"INSERT INTO zoop_error_log(is_error,req_id,api_name,doc_no,doc_no2,branch,branch_user,error_code,error_desc,timestamp) 
			VALUES ('1','$req_id','DL','$lic_id','$lic_no','$branch','$branch_sub_user','$api_err_code','$api_err_msg','$timestamp')");
		
			if(!$insert_err){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$_SESSION['verify_driver'] = $lic_no;
			$_SESSION['verify_driver_id'] = $lic_id;

			echo "<script>
					// $('#close_verify_modal')[0].click();
					// $('#function_result').html('');
					// $('#button_print_modal_dl')[0].click();
					// $('#button_verify_1')[0].click();
					$('#d_lic_no').attr('readonly',true);
					$('#verify_dl_div').show();
					$('#verify_btn_div_dl').hide();
					$('#is_dl_verified').val('1')
					$('#loadicon').hide();
				</script>";
				exit();
		}
		else
		{
			$insert_err = Qry($conn,"INSERT INTO zoop_error_log(is_error,req_id,api_name,doc_no,doc_no2,branch,branch_user,error_code,error_desc,timestamp) 
			VALUES ('1','$req_id','DL','$lic_id','$lic_no','$branch','$branch_sub_user','$api_err_code','$api_err_msg','$timestamp')");
		
			if(!$insert_err){
				$content = mysqli_error($conn);
				$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
				echo "<script>Swal.fire({
				icon: 'error',
				title: 'SQL Error !!!',
				text: '$content'
				})
				$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$_SESSION['verify_driver'] = $lic_no;
			$_SESSION['verify_driver_id'] = $lic_id;

			echo "<script>
					// $('#close_verify_modal')[0].click();
					// $('#function_result').html('');
					// $('#button_print_modal_dl')[0].click();
					// $('#button_verify_1')[0].click();
					$('#d_lic_no').attr('readonly',true);
					$('#verify_dl_div').show();
					$('#verify_btn_div_dl').hide();
					$('#is_dl_verified').val('1')
					$('#loadicon').hide();
				</script>";
				exit();
		
			// $content = "Code: ".$api_err_code." ".$api_err_msg;
			// $content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
			// echo "<script>Swal.fire({
				// icon: 'error',
				// title: 'API Error : $dl_no !!!',
				// text: '$content'
				// })
				// $('#loadicon').hide();		
			// </script>";
			// exit();
		}
	}
}

$get_data1  = Qry($conn,"select * from zoop_dl where dl_number='$lic_no' order by id desc limit 1");

if(!$get_data1){
	$content = mysqli_error($conn);
	$content = preg_replace("/[^0-9a-zA-Z_: ]/", "", $content);  
	echo "<script>Swal.fire({
	icon: 'error',
	title: 'SQL Error !!!',
	text: '$content'
	})
	$('#loadicon').hide();
	</script>";
	exit();
}
	
$row = fetchArray($get_data1);

		$diff_exp_date = (strtotime($row['expiryDate'])- strtotime($date))/24/3600; 
		$diff_transport_exp = (strtotime($row['TransportTo'])- strtotime($date))/24/3600; 
		
		if($row['TransportTo']=='' || $row['TransportTo']==0)
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'License Error !!!',
				text: 'License number not fit for HMV vehicles !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($diff_transport_exp<=5 AND strtotime(date("Y-m-d"))>strtotime("2021-03-31"))
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'License Error !!!',
				text: 'License (Transport) already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($diff_exp_date<=5 AND strtotime("Y-m-d")>strtotime("2021-03-31"))
		{
			echo "<script>Swal.fire({
				icon: 'error',
				title: 'License Error !!!',
				text: 'License already expired or expiring in next 5 days !!'
				})
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
$_SESSION['verify_driver'] = $lic_no;
$_SESSION['verify_driver_id'] = $lic_id;

// echo '<script>
				// Swal.fire({
				// title: "Success",
				// text: "RC Checked Successfully.",
				// icon: "success"
				// }).then(function() {
					// window.location = "rc_api_print.php?id='.$row["rc_regn_no"].'";
				// })
				// </script>';
				// exit();
?>
<style>
table td[class*=col-], table th[class*=col-]{
	font-weight: bold !important;
	color: black;
	line-height: 20px;
}
table td[class*=col-] label, table th[class*=col-] label{ 
	font-weight: normal !important;
	color: black;
}

</style>

<style type="text/css">
@media print
{

body {
   zoom:75%;
 }	
body * { visibility: hidden; }
.container-fluid * { visibility: visible; }
.container-fluid { position: absolute; top: 0; left: 0; }
 

.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
  float: left;
}
.col-md-12 {
  width: 100%;
}
.col-md-11 {
  width: 91.66666666666666%;
}
.col-md-10 {
  width: 83.33333333333334%;
}
.col-md-9 {
  width: 75%;
}
.col-md-8 {
  width: 66.66666666666666%;
}
.col-md-7 {
  width: 58.333333333333336%;
}
.col-md-6 {
  width: 50%;
}
.col-md-5 {
  width: 41.66666666666667%;
}
.col-md-4 {
  width: 33.33333333333333%;
 }
 .col-md-3 {
   width: 25%;
 }
 .col-md-2 {
   width: 16.666666666666664%;
 }
 .col-md-1 {
  width: 8.333333333333332%;
 }

 	td {
  	 font-weight: bold !important;
  	 font-size: 14px !important;
  	 	line-height: 25px !important;

 	}
 	label {
  	 font-weight: normal !important;
  	 font-size: 12px !important;
	}	

}

</style>

<script type="text/javascript">
	function callprint(){
		var css = '@page { size: landscape; }',
		head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');

		style.type = 'text/css';
		style.media = 'print';

		if (style.styleSheet){
		style.styleSheet.cssText = css;
		} else {
		style.appendChild(document.createTextNode(css));
		}

		head.appendChild(style);

		window.print();
	}
</script>

<div style="text-transform" class="modal fade" id="DLPrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content"> 
            <div class="modal-body" style="max-height: calc(100vh - 70px);overflow: auto;">

	<div class="row">
	
	<div class="form-group col-md-4">
		<button type="button" style="margin:10px" onclick="$('#button_verify_1')[0].click();$('#d_lic_no').attr('readonly',true);$('#verify_dl_div').show();$('#verify_btn_div_dl').hide();$('#is_dl_verified').val('1')" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>		
	
	<div class="form-group col-md-4">
		<center><span style="font-weight: bold; font-size: 18px;">Driving License No: <?php echo $lic_no; ?> </span> </center>
	</div>
	
	 <div class="form-group col-md-4" style="font-size:12px;">
		<span class="pull-right"></span>
	</div>

</div>

<div class="row">
<style type="text/css">

@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}

@media all {    
    .watermark {
        display: inline;
        position: fixed !important;
        opacity: 0.1;
        font-size: 5em;
        font-weight: bold;
        width: 100%;
        text-align: center;
        z-index: 1000;
        top: 350px;
        right:150px;
        transform:rotate(330deg);
    -webkit-transform:rotate(330deg);
    }
}
</style>
<?php


?>
	<div class="watermark">RAMAN ROADWAYS PVT LTD</div>

<div class="col-md-12 table-responsive" style='overflow-x:auto'>
<table class="alldata" border="0" style="width:100%;font-size:12px">
<tr>
<td class="col-md-6" colspan="2"><label>DL Last Updated at </label> <?php echo $row['request_timestamp']; ?></td>
</tr>
<tr> <td> &nbsp;</td> </tr>

<tr>
	<td class="col-md-6" colspan="2"> <LABEL>PRESENT ADDRESS</LABEL> </td>
</tr>
<tr>
<td class="col-md-6"><label> Address: </label> <?php echo $row['addressLine1']; ?> </td>
<td class="col-md-6" rowspan="7"> <img src="<?php echo $row['img']; ?>"> </td>
</tr><tr>
<td class="col-md-6"><label> Country: </label> <?php echo $row['country1']; ?> </td>
</tr><tr>
<td class="col-md-6"><label> Pin: </label> <?php echo $row['pin1']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label> Complete Address: </label> <?php echo $row['completeAddress1']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label> District: </label> <?php echo $row['district1']; ?> </td>
</tr>
<tr>
<td class="col-md-6"><label> State: </label> <?php echo $row['state1']; ?> </td>
</tr>

<tr> <td> &nbsp;</td> </tr>
<tr>
	<td class="col-md-6" colspan="2"> <LABEL>PERMANENT ADDRESS</LABEL> </td>
</tr>
<tr>
<td class="col-md-6"><label> Address: </label> <?php echo $row['addressLine2']; ?> </td>
<td class="col-md-6"><label> Complete Address: </label> <?php echo $row['completeAddress2']; ?> </td>
</tr><tr>
<td class="col-md-6"><label> Country: </label> <?php echo $row['country2']; ?> </td>
<td class="col-md-6"><label> District: </label> <?php echo $row['district2']; ?> </td>
</tr><tr>
<td class="col-md-6"><label> Pin: </label> <?php echo $row['pin2']; ?> </td>
<td class="col-md-6"><label> State: </label> <?php echo $row['state2']; ?> </td>
</tr>

<tr> <td> &nbsp;</td> </tr>

<tr>
<td class="col-md-6"><label>Blood Group: </label> <?php echo $row['blood_group']; ?> </td>
<td class="col-md-6"><label>Cov Details: </label> <?php 

$data = json_decode($row['cov_details'], true);

// $data[0]['cov']
// $data[0]['expiryDate']
// $data[0]['issueDate']


foreach ($data as $key => $value) {
	echo $value['cov']." <sup>($value[issueDate])</sup> &nbsp; ";
}

?> </td>
</tr><tr>
<td class="col-md-6"><label>DL Number: </label> <?php echo $row['dl_number']; ?> </td>
<td class="col-md-6"><label>Date of Birth: </label> <?php if($row['dob']!=NULL) echo date('d-M-Y', strtotime($row['dob'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Expiry Date: </label> <?php if($row['expiryDate']!=NULL) echo date('d-M-Y', strtotime($row['expiryDate'])); ?> </td>
<td class="col-md-6"><label>Father/Husband: </label> <?php echo $row['father/husband']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Photo: </label> <a class="no-print" href="<?php echo $row['img']; ?>" target="_blank"> View  </a>  </td>
<td class="col-md-6"><label>Issue Date: </label> <?php if($row['issue_date']!=NULL) echo date('d-M-Y', strtotime($row['issue_date'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Name: </label> <?php echo $row['name']; ?> </td>
<td class="col-md-6"><label>State: </label> <?php echo $row['state']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Endorse Date: </label> <?php if($row['endorse_dt']!=NULL) echo date('d-M-Y', strtotime($row['endorse_dt'])); ?> </td>
<td class="col-md-6"><label>Endorse No: </label> <?php echo $row['endorse_no']; ?> </td>
</tr><tr>
<td class="col-md-6"><label>Status: </label> <?php echo $row['status']; ?> </td>
<td class="col-md-6"><label>Status Details:  From </label> <?php if($row['statusDetails_from']!=NULL) echo date('d-M-Y', strtotime($row['statusDetails_from'])); ?> <label>To </label> <?php if($row['statusDetails_to']!=NULL) echo date('d-M-Y', strtotime($row['statusDetails_to'])); ?> <label>Remarks </label> <?php echo $row['statusDetails_remarks']; ?></td>
</tr>
<tr>
<td class="col-md-6"><label>Non Transport - From: </label> <?php if($row['nonTransportFrom']!=NULL) echo date('d-M-Y', strtotime($row['nonTransportFrom'])); ?> </td>
<td class="col-md-6"><label>Non Transport - To: </label> <?php if($row['nonTransportTo']!=NULL) echo date('d-M-Y', strtotime($row['nonTransportTo'])); ?> </td>
</tr><tr>
<td class="col-md-6"><label>Transport - From: </label> <?php if($row['TransportFrom']!=NULL) echo date('d-M-Y', strtotime($row['TransportFrom'])); ?> </td>
<td class="col-md-6"><label>Transport - To: </label> <?php if($row['TransportTo']!=NULL) echo date('d-M-Y', strtotime($row['TransportTo'])); ?> </td>
</tr>

</table>
</div>
</div>



	</div>	
   </div>
  </div>
 </div>
 
 <button id="button_print_modal_dl" type="button" data-toggle="modal" data-target="#DLPrintModal" style="display:none"></button>
 
<script>
	$('#close_verify_modal')[0].click();
	// $('#function_result').html('');
	$('#button_print_modal_dl')[0].click();
	$('#loadicon').hide();
</script>
