<?php
require('./connection.php');

$tno = escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s");

if($tno=='')
{
	echo "<script>
		alert('Please enter vehicle number !');
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		$('#tno_market').attr('readonly',false);
		// $('#button_sub').hide();
	</script>";
	exit();
}

echo "<script>$('#vehicle_check').val('NO');</script>";

$qry_check = Qry($conn,"SELECT owner_name,father_name,fit_up_to,insurance_upto,tax_upto,pucc_upto,permit_type,permit_valid_upto,insurance_company,
national_permit_upto FROM rc_api WHERE tno='$tno'");

if(!$qry_check){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		// $('#button_sub').hide();
	</script>";
	exit();
}


if(numRows($qry_check) > 0)
{
	$row = fetchArray($qry_check);
	
	echo "<script>
		$('#owner_name').html('$row[owner_name]');
		$('#father_name').html('$row[father_name]');
		$('#puc_exp').html('$row[pucc_upto]');
		$('#fitness_exp').html('$row[fit_up_to]');
		$('#ins_exp').html('$row[insurance_upto]');
		$('#p1_exp').html('$row[national_permit_upto]');
		$('#p_ltt_exp').html('$row[permit_valid_upto]');
		$('#permit_type').html('$row[permit_type]');
		$('#ins_comp_name').html('$row[insurance_company]');
		$('#vehicle_check').val('YES');
	</script>";
	
	if(strtotime($row['pucc_upto'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#puc_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#puc_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($row['fit_up_to'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#fitness_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#fitness_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($row['insurance_upto'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#ins_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#ins_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($row['national_permit_upto'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#p1_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#p1_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($row['permit_valid_upto'])>=strtotime(date("Y-m-d"))){
		echo "<script>$('#p_ltt_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#p_ltt_exp').attr('style','color:red');</script>";
	}
	
	?>
	<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',true);
		
		// $('#button_sub').attr('disabled',false);
		// $('#button_sub').show();
		$('.rc_res_div').show();
	</script>
	<?php
	exit();
}

$check_api = Qry($conn,"SELECT id FROM _verification_apis WHERE api_name='RC' AND status='0'");

if(!$check_api){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		// $('#button_sub').hide();
	</script>";
	exit();
}

if(numRows($check_api) > 0)
{
	$chk_tno = Qry($conn,"SELECT name FROM mk_truck WHERE tno='$tno'");
		
	if(numRows($chk_tno) == 0)
	{
		echo "<script>
			alert('Add vehicle first to continue !');
			$('#spinner_icon').hide();
			$('#fetch_button').attr('disabled',false);
		</script>";
		exit();
	}
		
	$row_tno = fetchArray($chk_tno);
	
	echo "<script>
			$('#owner_name').html('$row_tno[name]');
			$('#father_name').html('NA');
			$('#puc_exp').html('NA');
			$('#fitness_exp').html('NA');
			$('#ins_exp').html('NA');
			$('#p1_exp').html('NA');
			$('#p_ltt_exp').html('NA');
			$('#permit_type').html('NA');
			$('#ins_comp_name').html('NA');
			$('#vehicle_check').val('YES');
			
			$('#spinner_icon').hide();
			$('#fetch_button').attr('disabled',true);
			
			// $('#button_sub').attr('disabled',false);
			// $('#button_sub').show();
			$('.rc_res_div').show();
		</script>";
	
	?>
	<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',true);
		
		// $('#button_sub').attr('disabled',false);
		// $('#button_sub').show();
		$('.rc_res_div').show();
	</script>
	<?php
	exit();
}

$data = array(
"id_number"=>"$tno",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$aadhar_api_url/api/v1/rc/rc",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$aadhar_api_token.''
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$result = json_decode($response, true);
	
if($result['status_code']!='200')
{
	$error_msg = clean_input($result['message']);
	$error_code = $result['status_code'];
	
	$insert_error = Qry($conn,"INSERT INTO rc_api_error(tno,error_code,result,branch,branch_user,timestamp) VALUES ('$tno','$error_code',
	'$error_msg','$branch','$branch_sub_user','$timestamp')"); 
	
	if (strpos($error_msg, 'more than one office') !== false OR $error_code=='500' OR $error_code=='422')
	{
		$chk_tno = Qry($conn,"SELECT name FROM mk_truck WHERE tno='$tno'");
		
		if(numRows($chk_tno) == 0)
		{
			echo "<script>
				alert('Add vehicle first to continue !');
				$('#spinner_icon').hide();
				$('#fetch_button').attr('disabled',false);
			</script>";
			exit();
		}
		
		$row_tno = fetchArray($chk_tno);
		
		echo "<script>
			$('#owner_name').html('$row_tno[name]');
			$('#father_name').html('NA');
			$('#puc_exp').html('NA');
			$('#fitness_exp').html('NA');
			$('#ins_exp').html('NA');
			$('#p1_exp').html('NA');
			$('#p_ltt_exp').html('NA');
			$('#permit_type').html('NA');
			$('#ins_comp_name').html('NA');
			$('#vehicle_check').val('YES');
			
			$('#spinner_icon').hide();
			$('#fetch_button').attr('disabled',true);
			
			// $('#button_sub').attr('disabled',false);
			// $('#button_sub').show();
			$('.rc_res_div').show();
		</script>";
	
		exit();
	}

	echo $error_code." : ".$error_msg;
	echo "<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		$('#tno_market').attr('readonly',false);
		// $('#button_sub').hide();
	</script>";
	exit();
}

$client_id = $result['data']['client_id'];	

$rc_number = escapeString($conn,$result['data']['rc_number']);	
$registration_date = escapeString($conn,$result['data']['registration_date']);
$owner_name = escapeString($conn,$result['data']['owner_name']);
$father_name = escapeString($conn,$result['data']['father_name']);
$present_address = escapeString($conn,$result['data']['present_address']);
$permanent_address = escapeString($conn,$result['data']['permanent_address']);
$vehicle_category = escapeString($conn,$result['data']['vehicle_category']);
$vehicle_chasi_number = escapeString($conn,$result['data']['vehicle_chasi_number']);
$vehicle_engine_number = escapeString($conn,$result['data']['vehicle_engine_number']);
$maker_description = escapeString($conn,$result['data']['maker_description']);
$maker_model = escapeString($conn,$result['data']['maker_model']);
$body_type = escapeString($conn,$result['data']['body_type']);
$fuel_type = escapeString($conn,$result['data']['fuel_type']);
$color = escapeString($conn,$result['data']['color']);
$norms_type = escapeString($conn,$result['data']['norms_type']);
$fit_up_to = escapeString($conn,$result['data']['fit_up_to']);
$financer = escapeString($conn,$result['data']['financer']);
$insurance_company = escapeString($conn,$result['data']['insurance_company']);
$insurance_policy_number = escapeString($conn,$result['data']['insurance_policy_number']);
$insurance_upto = escapeString($conn,$result['data']['insurance_upto']);
$manufacturing_date = escapeString($conn,$result['data']['manufacturing_date']);
$registered_at = escapeString($conn,$result['data']['registered_at']);
$tax_upto = escapeString($conn,$result['data']['tax_upto']);
$tax_paid_upto = escapeString($conn,$result['data']['tax_paid_upto']);
$cubic_capacity = escapeString($conn,$result['data']['cubic_capacity']);
$vehicle_gross_weight = escapeString($conn,$result['data']['vehicle_gross_weight']);
$no_cylinders = escapeString($conn,$result['data']['no_cylinders']);
$seat_capacity = escapeString($conn,$result['data']['seat_capacity']);
$wheelbase = escapeString($conn,$result['data']['wheelbase']);
$unladen_weight = escapeString($conn,$result['data']['unladen_weight']);
$vehicle_category_description = escapeString($conn,$result['data']['vehicle_category_description']);
$pucc_number = escapeString($conn,$result['data']['pucc_number']);
$pucc_upto = escapeString($conn,$result['data']['pucc_upto']);
$permit_number = escapeString($conn,$result['data']['permit_number']);
$permit_issue_date = escapeString($conn,$result['data']['permit_issue_date']);
$permit_valid_from = escapeString($conn,$result['data']['permit_valid_from']);
$permit_valid_upto = escapeString($conn,$result['data']['permit_valid_upto']);
$permit_type = escapeString($conn,$result['data']['permit_type']);
$national_permit_number = escapeString($conn,$result['data']['national_permit_number']);
$national_permit_upto = escapeString($conn,$result['data']['national_permit_upto']);
$national_permit_issued_by = escapeString($conn,$result['data']['national_permit_issued_by']);
$blacklist_status = escapeString($conn,$result['data']['blacklist_status']);
$noc_details = escapeString($conn,$result['data']['noc_details']);
$owner_number = escapeString($conn,$result['data']['owner_number']);
$rc_status = escapeString($conn,$result['data']['rc_status']);
$masked_name = escapeString($conn,$result['data']['masked_name']);
$challan_details = escapeString($conn,$result['data']['challan_details']);

$qry = Qry($conn,"INSERT INTO rc_api(tno,registration_date,owner_name,father_name,present_address,permanent_address,vehicle_category,
vehicle_chasi_number,vehicle_engine_number,maker_description,maker_model,body_type,fuel_type,color,norms_type,fit_up_to,
financer,insurance_company,insurance_policy_number,insurance_upto,manufacturing_date,registered_at,tax_upto,tax_paid_upto,
cubic_capacity,vehicle_gross_weight,no_cylinders,seat_capacity,wheelbase,unladen_weight,vehicle_category_description,
pucc_number,pucc_upto,permit_number,permit_issue_date,permit_valid_from,permit_valid_upto,permit_type,national_permit_number,
national_permit_upto,national_permit_issued_by,blacklist_status,noc_details,owner_number,rc_status,masked_name,
challan_details,branch,branch_user,timestamp) VALUES ('$tno','$registration_date','$owner_name','$father_name','$present_address',
'$permanent_address','$vehicle_category','$vehicle_chasi_number','$vehicle_engine_number','$maker_description','$maker_model','$body_type',
'$fuel_type','$color','$norms_type','$fit_up_to','$financer','$insurance_company','$insurance_policy_number','$insurance_upto',
'$manufacturing_date','$registered_at','$tax_upto','$tax_paid_upto','$cubic_capacity','$vehicle_gross_weight','$no_cylinders','$seat_capacity',
'$wheelbase','$unladen_weight','$vehicle_category_description','$pucc_number','$pucc_upto','$permit_number','$permit_issue_date',
'$permit_valid_from','$permit_valid_upto','$permit_type','$national_permit_number','$national_permit_upto','$national_permit_issued_by',
'$blacklist_status','$noc_details','$owner_number','$rc_status','$masked_name','$challan_details','$branch','$branch_sub_user','$timestamp')");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		// $('#button_sub').hide();
		$('#tno_market').attr('readonly',false);
	</script>";
	exit();
}

// $insert_vehicle = Qry($conn,"INSERT INTO mk_truck(tno,name,branch,branch_user,timestamp) VALUES ('$tno','$registration_date','$owner_name','$father_name','$present_address',
// '$permanent_address','$vehicle_category','$vehicle_chasi_number','$vehicle_engine_number','$maker_description','$maker_model','$body_type',
// '$fuel_type','$color','$norms_type','$fit_up_to','$financer','$insurance_company','$insurance_policy_number','$insurance_upto',
// '$manufacturing_date','$registered_at','$tax_upto','$tax_paid_upto','$cubic_capacity','$vehicle_gross_weight','$no_cylinders','$seat_capacity',
// '$wheelbase','$unladen_weight','$vehicle_category_description','$pucc_number','$pucc_upto','$permit_number','$permit_issue_date',
// '$permit_valid_from','$permit_valid_upto','$permit_type','$national_permit_number','$national_permit_upto','$national_permit_issued_by',
// '$blacklist_status','$noc_details','$owner_number','$rc_status','$masked_name','$challan_details','$branch','$branch_sub_user','$timestamp')");

// if(!$qry){
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// echo "<script>
		// $('#spinner_icon').hide();
		// $('#fetch_button').attr('disabled',false);
		// $('#button_sub').attr('disabled',true);
		// $('#button_sub').hide();
		// $('#tno_market').attr('readonly',false);
	// </script>";
	// exit();
// }

echo "<script>
		$('#owner_name').html('$owner_name');
		$('#father_name').html('$father_name');
		$('#puc_exp').html('$pucc_upto');
		$('#fitness_exp').html('$fit_up_to');
		$('#ins_exp').html('$insurance_upto');
		$('#p1_exp').html('$national_permit_upto');
		$('#p_ltt_exp').html('$permit_valid_upto');
		$('#ins_comp_name').html('$insurance_company');
		$('#permit_type').html('$permit_type');
		$('#vehicle_check').val('YES');
	</script>";
	
	if(strtotime($pucc_upto)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#puc_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#puc_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($fit_up_to)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#fitness_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#fitness_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($insurance_upto)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#ins_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#ins_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($national_permit_upto)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#p1_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#p1_exp').attr('style','color:red');</script>";
	}
	
	if(strtotime($permit_valid_upto)>=strtotime(date("Y-m-d"))){
		echo "<script>$('#p_ltt_exp').attr('style','color:green');</script>";
	}
	else{
		echo "<script>$('#p_ltt_exp').attr('style','color:red');</script>";
	}
?>

<script>
	$('#spinner_icon').hide();
	$('#fetch_button').attr('disabled',true);
	
	// $('#button_sub').attr('disabled',false);
	// $('#button_sub').show();
	$('.rc_res_div').show();
</script>
