<?php
require_once 'connection.php'; 

$date_tdy = date("Y-m-d");

$sql ="SELECT e.id,e.lrno,e.ewb_no,e.truck_no,DATE_FORMAT(e.ewb_date,'%d-%m-%y') as ewb_date,
DATE_FORMAT(e.ewb_expiry,'%d-%m-%y') as ewb_exp_date,DATE_FORMAT(e.lr_date,'%d-%m-%y') as lr_date,
IF(date(e.ewb_expiry)='$date_tdy','EXP_TODAY','BACK_DATED') as exp_date1,
CONCAT(e.from_loc,' --to-- ',e.to_loc,'<br>',e.consignor,'<br>',e.consignee) as location_and_party,
IF(e.crossing_lr='0','NO','YES') as crossing_lr,e.cross_veh_no,e.owner_mobile,e.driver_mobile,e.branch_narration,e.del_date
FROM _eway_bill_validity AS e 
WHERE date(e.ewb_expiry)<='$date_tdy' AND e.ewb_expiry!=0 AND e.branch_timestamp IS NULL AND e.branch='$branch' ORDER BY e.ewb_expiry ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'lrno', 'dt' => 1),
    array( 'db' => 'truck_no', 'dt' => 2),
	array( 'db' => 'owner_mobile', 'dt' => 3),
    array( 'db' => 'driver_mobile', 'dt' => 4),
    array( 'db' => 'crossing_lr', 'dt' => 5),
    array( 'db' => 'cross_veh_no', 'dt' => 6),
    array( 'db' => 'lr_date', 'dt' => 7), 
    array( 'db' => 'ewb_date', 'dt' => 8), 
    array( 'db' => 'ewb_exp_date', 'dt' => 9), 
    array( 'db' => 'location_and_party', 'dt' => 10),  
    array( 'db' => 'ewb_no', 'dt' => 11),  
    array( 'db' => 'del_date', 'dt' => 12),  
    array( 'db' => 'exp_date1', 'dt' => 13),  
    array( 'db' => 'branch_narration', 'dt' => 14),  
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);