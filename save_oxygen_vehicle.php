<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$tno = trim(escapeString($conn,strtoupper($_POST['tno'])));

$chk_vehicle = Qry($conn,"SELECT tno FROM dairy.own_truck WHERE tno='$tno'");

if(!$chk_vehicle){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_vehicle)==0)
{
	echo "<script>
		alert('Vehicle number is not valid !');
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}


$row_tno = fetchArray($chk_vehicle);

$tno = $row_tno['tno'];

$chk_duplicate = Qry($conn,"SELECT id FROM dairy.oxygn_tno WHERE tno='$tno'");

if(!$chk_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_duplicate)>0)
{
	echo "<script>
		alert('Duplicate record !');
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

StartCommit($conn);
$flag = true; 

$insert_vehicle = Qry($conn,"INSERT INTO dairy.oxygn_tno(tno,is_active,branch,branch_user,timestamp) VALUES ('$tno','1','$branch',
'$_SESSION[user_code]','$timestamp')");

if(!$insert_vehicle){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('OK : Vehicle $tno Added !');
		$('#loadicon_ewb_val').fadeOut('slow');
		$('#FormAddOxygenVehicle')[0].reset();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}
?>