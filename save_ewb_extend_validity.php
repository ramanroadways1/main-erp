<?php
require_once("./connection.php");

if(!isset($_SESSION['modal_ewb_ext_dest_pincode']))
{
	echo "<script>
		alert('Destination pincode not found.');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if(!isset($_SESSION['modal_ewb_ext_ewb_no']))
{
	echo "<script>
		alert('Eway-bill number not found.');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

if(!isset($_SESSION['modal_ewb_ext_company']))
{
	echo "<script>
		alert('Company not found.');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$company = $_SESSION['modal_ewb_ext_company'];

if($company!='RRPL' AND $company!='RAMAN_ROADWAYS')
{
	echo "<script>
		alert('Company not found !');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

// $ewbNo = "651292556962";
// $vehicleNo = "";
// $fromPlace = "DAPOLI";
// $fromState = "27";
// $remainingDistance = "100";
// $ext_reason = "in transit";
// $from_pincode = "415712";
// $addr1 = "DAPOLI";
// $addr2 = "RATNAGIRI";
// $addr3 = "Maharashtra";
// $extnRsnCode = "99";

$ewb_no = $_SESSION['modal_ewb_ext_ewb_no'];
$ext_reason_code = escapeString($conn,($_POST['reason']));
$ext_remark = escapeString($conn,($_POST['remark']));
$current_place = escapeString($conn,($_POST['current_place']));
$current_pincode = escapeString($conn,($_POST['pincode']));
$state = escapeString($conn,($_POST['state']));
$distance = escapeString($conn,($_POST['distance']));
$addr_1 = escapeString($conn,($_POST['addr_1']));
$addr_2 = escapeString($conn,($_POST['addr_2']));
$addr_3 = escapeString($conn,($_POST['addr_3']));

$data = array(
"ewbNo"=>$ewb_no,
"vehicleNo"=>"",
"fromPlace"=>$current_place,  // Current location of vehicle eg. Bengaluru
"fromState"=>$state, // Current state // Refer Master eg. state code
"remainingDistance"=>$distance, // remainingDistance to destination
"transDocNo"=>"", // non complusory if exists in main EWB
"transDocDate"=>"", // non complusory if exists in main EWB - 12/10/2017
"transMode"=>5, // Refer Master // 5-inTransit , 1-Road , 2-Rail, 3-Air, 4-Ship
"extnRsnCode"=>$ext_reason_code, //1-Natural Calamity , 2-Law and Order Situation , 4-Transshipment , 5-Accident ,99-Others
"extnRemarks"=>$ext_remark, // Refer Master // write other reason of extension
"fromPincode"=>$current_pincode, // Current place pincode
"consignmentStatus"=>"T", //M-inMovement & T-inTransit
"transitType"=>"R",  // R- Road, W - Warehouse, O-Others
"addressLine1"=>$addr_1,
"addressLine2"=>$addr_2,
"addressLine3"=>$addr_3,
);

$timestamp=date("Y-m-d H:i:s");
$payload = json_encode($data);

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];

if($company=='RRPL')
{
	$gst_no_company=$rrpl_gst_no;
	$gst_username=$ewb_rrpl_username;
	$gst_password=$ewb_rrpl_password;
}
else
{
	$gst_no_company=$rr_gst_no;
	$gst_username=$ewb_rr_username;
	$gst_password=$ewb_rr_password;
}

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];
	
$ch = curl_init("$tax_pro_url/v1.03/dec/ewayapi?action=EXTENDVALIDITY&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&authtoken=$authToken");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($payload)));
		
$result = curl_exec($ch);
$err1 = curl_error($ch);
curl_close($ch);
	
if($err1)
{
	$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES ('$company','cURL Error : $err1','COAL','$branch','$timestamp')");
	
	if(!$insert_error){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","../");
		exit();
	}
			
	echo "<script>
		alert('API Error !!');
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$EwayArray = json_decode($result, true);
		
if(@$EwayArray['error'])
{
	if($EwayArray['error']['error_cd']=="GSP102")
	{
		$token_gen_url = "$tax_pro_url/v1.03/dec/authenticate?action=ACCESSTOKEN&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&ewbpwd=$gst_password";
		$curlToken = curl_init();
		curl_setopt_array($curlToken, array(
		CURLOPT_URL => $token_gen_url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 300,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
	));
	
	$response_Token = curl_exec($curlToken);
	$errToken = curl_error($curlToken);
	curl_close($curlToken);
				
	if($errToken)
	{
		$insert_error2 = Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
		('$company','cURL Error : $errToken','RRPL.ONLINE','$branch','$timestamp')");
		
		if(!$insert_error2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","../");
			exit();
		}
		
		echo "<span style='color:maroon;font-size:13px'>$errToken</span>";			
		echo "<script>
			alert('Error while getting token !');
			$('#ext_val_ewb_btn_save').attr('disabled',false);
			$('#loadicon_ewb_val').fadeOut('slow');
		</script>";
		exit();
	}
			
			// echo $token_gen_url;
	$response_decodedToken1 = json_decode($response_Token, true);
	$authToken_new = $response_decodedToken1['authtoken'];
				
	if($authToken_new=='')
	{
		echo "<script>
			alert('Error : empty token generated. Please try once again..');
			$('#ext_val_ewb_btn_save').attr('disabled',false);
			$('#loadicon_ewb_val').fadeOut('slow');
		</script>";
		exit();
	}
				
	$insert_token=Qry($conn,"INSERT INTO ship.api_token (token,company,user,branch_user,timestamp) VALUES 
	('$authToken_new','$company','$branch','$branch_sub_user','$timestamp')");
	
	if(!$insert_token){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","../");
		exit();
	}
				
		echo "<script>
			alert('NEW Token generated ! Please try again.');
			$('#ext_val_ewb_btn_save').attr('disabled',false);
			$('#loadicon_ewb_val').fadeOut('slow');
		</script>";
		exit();
				
	}
	else
	{
		$errorMsg = $EwayArray['error']['message'];
		$errorMsgCode = $EwayArray['error']['error_cd'];
				
		$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,msg,branch,timestamp) VALUES 
		('$company','$result','RRPL.ONLINE','$errorMsg','$branch','$timestamp')");
				
		if(!$insert_error){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","../");
			exit();
		}
			
		echo "<span style='color:maroon;font-size:13px'>$errorMsg</span>";			
		echo "<script>
			$('#ext_val_ewb_btn_save').attr('disabled',false);
			$('#loadicon_ewb_val').fadeOut('slow');
		</script>";
		exit();
	}
}

if(empty($EwayArray['validUpto']))
{
	echo "<span style='color:maroon;font-size:13px'>Error..</span>";			
	echo "<script>
		$('#ext_val_ewb_btn_save').attr('disabled',false);
		$('#loadicon_ewb_val').fadeOut('slow');
	</script>";
	exit();
}

$ext_timestamp = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$EwayArray['updatedDate'])));
$expiry_date = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$EwayArray['validUpto'])));
$expiry_date2 = date("d-m-Y",strtotime($expiry_date));

$ewb_id = $_SESSION['modal_ewb_ext_ewb_id'];
$prev_exp_date = $_SESSION['modal_ewb_ext_prev_exp_date'];
$address = "$addr_1, $addr_2, $addr_3";

$update_val = Qry($conn,"UPDATE _eway_bill_validity SET ewb_expiry='$expiry_date',ewb_extended=ewb_extended+'1',ext_remark='$ext_remark',
ext_timestamp='$ext_timestamp',ext_branch='$branch',ext_user='$_SESSION[user_code]' WHERE ewb_no='$ewb_no'");
				
if(!$update_val){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$udpate_val2 = Qry($conn,"UPDATE _eway_bill_lr_wise SET ewb_expiry='$expiry_date',ewb_extended=ewb_extended+'1' WHERE ewb_no='$ewb_no'");
				
if(!$udpate_val2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$insert_log = Qry($conn,"INSERT INTO _ewb_extension_log(ewb_no,ewb_id,prev_exp_date,new_exp_date,ext_reason,remark,current_place,
current_pincode,state,address,branch,branch_user,timestamp) VALUES ('$ewb_no','$ewb_id','$prev_exp_date','$expiry_date',
'$ext_reason_code','$ext_remark','$current_place','$current_pincode','$state','$address','$branch','$_SESSION[user_code]','$timestamp')");

if(!$insert_log){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

unset($_SESSION['modal_ewb_ext_dest_pincode']);
unset($_SESSION['modal_ewb_ext_ewb_no']);
unset($_SESSION['modal_ewb_ext_company']);
unset($_SESSION['modal_ewb_ext_ewb_id']);
unset($_SESSION['modal_ewb_ext_prev_exp_date']);

echo "<script>
	alert('Ewb: $ewb_no extension success ! New validity is: $expiry_date2.');
	window.location.href='./';
</script>";
exit();

// echo $result."<br>";	
// echo $EwayArray['updatedDate']."<br>";
// echo $EwayArray['validUpto'];
?>