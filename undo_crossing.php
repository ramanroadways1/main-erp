<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$id = escapeString($conn,strtoupper($_POST['id']));
$page_name1 = escapeString($conn,($_POST['page_name']));

$get_lr_dest = Qry($conn,"SELECT id,tstation FROM lr_sample WHERE lrno='$lrno'");

if(!$get_lr_dest){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lr_dest)==0){
	errorLog("LR Entry not found in Table.",$conn,$page_name,__LINE__);
	Redirect("LR not found","./");
	exit();
}

$row_dest = fetchArray($get_lr_dest);

$update = Qry($conn,"UPDATE lr_pre SET tstation='$row_dest[tstation]',to_id='$row_dest[id]',crossing='NO',cross_to='',cross_stn_id='' 
WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('LR Number : $lrno. Marked as Direct Delivery !');
	window.location.href='./$page_name1';
</script>";
exit();
?>