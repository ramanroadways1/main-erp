<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$frno1 = escapeString($conn,strtoupper($_POST['fm_no']));

$frno = explode("_",$frno1)[0];
$lr_date = explode("_",$frno1)[1];
$create_date_fm = explode("_",$frno1)[2];
$create_date_fm2 = date("d-m-Y",strtotime($create_date_fm));
$narration = escapeString($conn,($_POST['narration']));

$get_record = Qry($conn,"SELECT id FROM block_fm WHERE frno='$frno'");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./block_fm_balance.php");
	exit(); 
}

if(numRows($get_record)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO block_fm(lrno,frno,lr_date,set_by,active,branch,branch_user,timestamp) VALUES 
('$lrno','$frno','$lr_date','$narration','1','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./block_fm_balance.php");
	exit();
}

echo "<script>
		alert('Request submitted successfully !');
		window.location.href='./block_fm_balance.php';
	</script>";
	exit();
?>