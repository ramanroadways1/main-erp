<form style="font-size:12px" id="FormEmpEditData" action="#" method="POST">
<div id="ModalEmpEdit1" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
	<div class="modal-content">
      <div style="font-size:14px;" class="modal-header bg-primary">
		Edit Employee Form: <?php echo $row['name']; ?>
      </div>
      <div class="modal-body">
        <div class="row">
      <div class="form-group col-md-12">
		
	<div class="row bg-warning" style="padding:4px;">Personal Information</div>

<div class="form-group col-md-12"></div>

<div class="row">	
	
	<div class="form-group col-md-3">
		<label>Employee Photo <font color="red"><sup>*</sup></font></label>
		<input class="form-control" type="file" accept="image/*" name="emp_photo" required="required">
   </div>
      
   <div class="form-group col-md-3">
        <label>Date of birth <font color="red"><sup>*</sup></font></label> 
        <input max="<?php echo $date_16year_ago; ?>" min="<?php echo $date_60_year_before; ?>" type="date" class="form-control" id="birthdate" name="birthdate" required="required" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"> 
	</div>
     
   <div class="form-group col-md-3">
        <label>Age <font color="red"><sup>*</sup></font></label>
        <input type="number" id="age" name="age" class="form-control" required="required" readonly="readonly">
    </div>  

	<div class="form-group col-md-3">
         <label>Date of Joining <font color="red"><sup>*</sup></font></label>
         <input max="<?php echo $date; ?>" type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" name="joindate" required="required"> 
    </div>
	
   <div class="form-group col-md-3">
		<label>Father's Name <font color="red"><sup>*</sup></font></label>
       <input type="text" name="fathername" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
 
   <div class="form-group col-md-3">
       <label>Father's Occupation <font color="red"><sup>*</sup></font></label>
       <select style="font-size:12px;cursor: pointer;" name="fatherocc" onchange="Occupation(this.value)" class="form-control" class="form-control" required="required">	
				<option style="font-size:12px" value="">--Select option--</option>
				<option style="font-size:12px" value="Farmer">Farmer</option>
				<option style="font-size:12px" value="ShopOwner">Shop Owner</option>
				<option style="font-size:12px" value="Businessman">Businessman</option>
				<option style="font-size:12px" value="Govt">Govt. Employee</option>
				<option style="font-size:12px" value="Pvt">Pvt. Sector Employee</option>
				<option style="font-size:12px" value="Others">Others</option>
		</select>	
	</div>

<script>
function Occupation(elem)
{
	if(elem=='Others')
	{
		$('#occp_other').show();
		$('#fatherocc_other').attr('required',true);
	}
	else
	{
		$('#occp_other').hide();
		$('#fatherocc_other').attr('required',false);
	}
}
</script>

	<div class="form-group col-md-3" style="display:none" id="occp_other">
       <label>Write Father's Occupation <font color="red"><sup>*</sup></font></label>
       <input type="text" name="fatherocc_other" id="fatherocc_other" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
   
   <div class="form-group col-md-3">
       <label>Mother's Name <font color="red"><sup>*</sup></font></label>
       <input type="text" name="mothername" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>

<script>
function OccupationMother(elem)
{
	if(elem=='Others')
	{
		$('#occp_other_mother').show();
		$('#motherocc_other').attr('required',true);
	}
	else
	{
		$('#occp_other_mother').hide();
		$('#motherocc_other').attr('required',false);
	}
}
</script>

   <div class="form-group col-md-3">
       <label>Mother's Occupation <font color="red"><sup>*</sup></font></label>
       <select style="font-size:12px;cursor: pointer;" name="motherocc" onchange="OccupationMother(this.value)" class="form-control" class="form-control" required="required">	
				<option style="font-size:12px" value="">--Select option--</option>
				<option style="font-size:12px" value="House-Wife">House Wife</option>
				<option style="font-size:12px" value="Govt">Govt. Employee</option>
				<option style="font-size:12px" value="Pvt">Pvt. Sector Employee</option>
				<option style="font-size:12px" value="Others">Others</option>
		</select>	
	</div>  

	<div class="form-group col-md-3" style="display:none" id="occp_other_mother">
       <label>Write Mother's Occupation <font color="red"><sup>*</sup></font></label>
       <input type="text" name="motherocc_other" id="motherocc_other" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>	

   <div class="form-group col-md-3">
       <label>Aadhar Number <font color="red"><sup>*</sup></font></label>
       <input type="text" name="aadharno" class="form-control" maxlength="12" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required">
   </div> 

   <div class="form-group col-md-3">
       <label>Aadhar Card's Copy - Front <font color="red"><sup>*</sup></font></label>
       <input class="form-control" type="file" accept="image/*" name="aadhar_copy_front" required="required">
   </div>
   
   <div class="form-group col-md-3">
       <label>Aadhar Card's Copy - Rear<font color="red"><sup>*</sup></font></label>
       <input class="form-control" type="file" accept="image/*" name="aadhar_copy_rear" required="required">
   </div>

	<div class="form-group col-md-3">
        <label>PAN Number </label>
        <input type="text" maxlength="10" name="pan_no" id="pan_no" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateNewPan()">
    </div>

   <div class="form-group col-md-3">
       <label>PAN Card's Copy </label>
       <input class="form-control" type="file" accept="image/*" id="pan_copy" name="pan_copy">
   </div>

<!-- <div class="col-md-3">
  <div class="form-group">
    <label>SALARY</label>
    <select class="form-control" style="cursor: pointer;" id="selectsalary" required="required">
    <option value=""> --- select --- </option>
    <option value="LATER"> LATER </option>
    <option value="NOW"> NOW </option>
    </select> 
  </div>
</div>

      <div class="col-md-3">
        <div class="form-group">
          <label>AMOUNT</label>
          <input type="text" name="salary" id="salary" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')"  disabled="disabled">
        </div>
      </div> -->

    <div class="form-group col-md-3">
         <label>Pincode <font color="red"><sup>*</sup></font></label>
         <input type="text" name="pincode" maxlength="6" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required">
    </div>


     <div class="form-group col-md-3">
          <label>Nearest Police Station <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="police_station" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" >
      </div>

	 <div class="form-group col-md-3">
          <label>Qualification <font color="red"><sup>*</sup></font></label>
		  <select style="font-size:12px;cursor: pointer;" name="qualification" class="form-control" class="form-control" required="required">	
				<option style="font-size:12px" value="">--Select option--</option>
				<option style="font-size:12px" value="NO">No Qualification</option>
				<option style="font-size:12px" value="Under_10">Under 10th</option>
				<option style="font-size:12px" value="10">10th</option>
				<option style="font-size:12px" value="12">12th</option>
				<option style="font-size:12px" value="Graduate">Graduate</option>
		  </select>		  
	 </div>

    <div class="form-group col-md-3">
          <label>Work Experience <font color="red"><sup>*</sup></font></label>
		  <select style="font-size:12px;cursor: pointer;" name="experience" class="form-control" required="required">	
				<option style="font-size:12px" value="">--Select option--</option>
				<option style="font-size:12px" value="NO">No Experience</option>
				<option style="font-size:12px" value="1">1 Year</option>
				<option style="font-size:12px" value="2">2 Years</option>
				<option style="font-size:12px" value="2-5">2-5 Years</option>
				<option style="font-size:12px" value="5+">More than 5Yrs</option>
		</select>
    </div>

	<div class="form-group col-md-3">
        <label>Blood Group <font color="red"><sup>*</sup></font></label>
		<select style="font-size:12px;cursor: pointer;" class="form-control" name="bloodgroup" required="required">
			<option style="font-size:12px" value=""> --- select --- </option>
			<option style="font-size:12px" value="NO">Don't Know</option>
			<option style="font-size:12px" value="O+"> O+ </option>
			<option style="font-size:12px" value="O−"> O− </option>
			<option style="font-size:12px" value="A−"> A− </option>
			<option style="font-size:12px" value="A+"> A+ </option>
			<option style="font-size:12px" value="B−"> B− </option>
			<option style="font-size:12px" value="B+"> B+ </option>
			<option style="font-size:12px" value="AB−"> AB− </option>
			<option style="font-size:12px" value="AB+"> AB+ </option>
		</select> 
    </div>

<script>
function FuncOtp()
{
	$('#emp_edit_save_btn').attr('disabled',true);
	$('#send_otp_button').hide();
	var name = '<?php echo $row["name"]; ?>'
	$('#mobileno').attr('readonly',true);
	
	var mobile = $('#mobileno').val();
	
	if(mobile!='' && name!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./_send_otp_verify_mobile_add_employee.php",
			data: 'mobile=' + mobile + '&name=' + name,
			type: "POST",
			success: function(data){
				$("#result_emp").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		alert('Please enter mobile number first !');
		$('#mobileno').attr('readonly',false);
		$('#send_otp_button').show();
	}
}
</script>

    <div class="form-group col-md-3">
         <label>Mobile No. <sup><font color="green">Verified</font></sup></label>
         <input value="<?php echo $row['mobile_no']; ?>" readonly type="text" name="mobileno" id="mobileno" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required" maxlength="10">
    </div>
	
	<div class="form-group col-md-3">
          <label>Alternate Mobile No. </label>
          <input type="text" name="alternateno" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')"  maxlength="10">
     </div>

   <div class="form-group col-md-3">
        <label>Email Id </label>
        <input type="email" name="emailid" style="text-transform:lowercase" class="form-control" oninput="this.value=this.value.replace(/[^a-z-A-Z0-9-_@#.]/,'')">
   </div>

	<div class="form-group col-md-3">
		<label>Emergency Mobile No. <font color="red"><sup>*</sup></font></label>
         <input type="text" name="emergencymo" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" required="required" maxlength="10">
    </div>

	<div class="form-group col-md-3">
          <label>Languages Known <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="language" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z,]/,'')">
    </div>

	<div class="form-group col-md-3">
		<label>Any Major Diseases ? <font color="red"><sup>*</sup></font></label>
        <input type="text" name="dieases" required="required" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')">
    </div>
	
	<div class="form-group col-md-5">
        <label>Permanent Address <font color="red"><sup>*</sup></font></label>
        <textarea name="residence" id="residence_addr" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'');$('#currentaddr').val('');$('#addr_checkbox').prop('checked', false);" required="required"></textarea>
   </div> 

   <div class="form-group col-md-5">
        <label>Current Address <font color="red"><sup>*</sup></font> &nbsp; <input id="addr_checkbox" name="addr_checkbox" type="checkbox" onchange="AddrCheckBox()" /> <font color="maroon"><sup>Same as per. addr</sup></font></label>
        <textarea name="currentaddr" id="currentaddr" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')" required="required"></textarea>
    </div>

</div>

<div class="row bg-warning" style="padding:4px;">Maritial Information</div>
<div class="form-group col-md-12"></div>

<div class="row">
	
<script>
function MaritialStatus(elem)
{
  if(elem=='MARRIED')
  {
      $("#wife_name").prop('readonly',false);  
      $("#wife_occ").prop('readonly',false);  
      $('#no_of_child').attr("disabled", false); 

      $("#wife_name").prop('required',true);  
      $("#wife_occ").prop('required',true);  
      $('#no_of_child').attr("required", true);  

  }
  else
  {
      $("#wife_name").prop('readonly',true);  
      $("#wife_occ").prop('readonly',true);  
      $('#no_of_child').attr("disabled", true);  
 
      $("#wife_name").prop('required',false);  
      $("#wife_occ").prop('required',false);  
      $('#no_of_child').attr("required", false);  

	}  
}  
</script>	

    <div class="form-group col-md-3">
          <label>Maritial Staus <font color="red"><sup>*</sup></font></label>
			<select style="font-size:12px;cursor: pointer;" id="maritial" class="form-control" onchange="MaritialStatus(this.value)" name="maritial" required="required">
				<option style="font-size:12px" value=""> --- select --- </option>
				<option style="font-size:12px" value="MARRIED"> MARRIED </option>
				<option style="font-size:12px" value="UNMARRIED"> UNMARRIED </option> 
			</select> 
     </div>
     
	<div class="form-group col-md-3">
          <label>Wife's Name <font color="red"><sup>*</sup></font></label>
          <input type="text" id="wife_name" required="required" name="wifename" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" readonly="readonly">
     </div>
     
	<div class="form-group col-md-3">
          <label>Wife's Occupation <font color="red"><sup>*</sup></font></label>
          <input type="text" id="wife_occ" name="wifeocc" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z]/,'')"  readonly="readonly">
    </div>
    
	<div class="form-group col-md-3">
		<label>No of Childrens <font color="red"><sup>*</sup></font></label>
		<select style="font-size:12px;cursor: pointer;" class="form-control" id="no_of_child" required="required" name="child" disabled="">
		  <option style="font-size:12px" value="">-- select --</option>
		  <option style="font-size:12px" value="0"> 0 </option>
		  <option style="font-size:12px" value="1"> 1 </option>
		  <option style="font-size:12px" value="2"> 2 </option>
		  <option style="font-size:12px" value="3"> 3 </option>
		  <option style="font-size:12px" value="4"> 4 </option>
		  <option style="font-size:12px" value="5"> 5 </option>
		  <option style="font-size:12px" value="6"> 6 </option>
		  <option style="font-size:12px" value="7"> 7 </option>
		  <option style="font-size:12px" value="8"> 8 </option> 
		</select> 
    </div>

</div>

<script>
function AddrCheckBox()
{
	if($('#addr_checkbox').is(":checked")){
      $('#currentaddr').val($('#residence_addr').val());
    }
	else{
       $('#currentaddr').val('');
    }
}

function Rule_BankAc()
{
	$("#acc_fullname").val('');
	  $("#acc_number").val('');
	  $("#acc_ifsc").val('');
	  $("#acc_bank").val('');
	  
	if($('#bank_checkbox').is(":checked")){
       
	   $("#acc_fullname").attr('readonly',false);
	   $("#acc_number").attr('readonly',false);
	   $("#acc_ifsc").attr('readonly',false);
	   $("#acc_bank").attr('readonly',false);
	   
	   $("#acc_fullname").attr('required',true);
	   $("#acc_number").attr('required',true);
	   $("#acc_ifsc").attr('required',true);
	   $("#acc_bank").attr('required',true);
    }
	else{
       $("#acc_fullname").attr('readonly',true);
	   $("#acc_number").attr('readonly',true);
	   $("#acc_ifsc").attr('readonly',true);
	   $("#acc_bank").attr('readonly',true);
	   
	   $("#acc_fullname").attr('required',false);
	   $("#acc_number").attr('required',false);
	   $("#acc_ifsc").attr('required',false);
	   $("#acc_bank").attr('required',false);
    }
}
</script>

<div class="row bg-warning" style="padding:4px;">Bank A/c Information &nbsp; 
	<input id="bank_checkbox" checked="checked" name="bank_checkbox" type="checkbox" onchange="Rule_BankAc()" /></div>
<div class="form-group col-md-12"></div>

	
<div class="row">

	<div class="form-group col-md-4">
          <label>A/c Holder (as per bank) <font color="red"><sup>*</sup></font></label>
          <input type="text" id="acc_fullname" name="acc_fullname" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
     </div>
     
     <div class="form-group col-md-3">
          <label>Account Number <font color="red"><sup>*</sup></font></label>
          <input type="text" id="acc_number" name="acc_number" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" required="required">
    </div>

    <div class="form-group col-md-2">
          <label>IFSC Code <font color="red"><sup>*</sup></font></label>
          <input type="text" name="acc_ifsc" maxlength="11" id="acc_ifsc" class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" required="required">
    </div>

    <div class="form-group col-md-3">
          <label>Bank Name <font color="red"><sup>*</sup></font></label>
          <input id="acc_bank" type="text" name="acc_bank" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" required="required">
   </div>
   
</div>

<div class="row bg-warning" style="padding:4px;">Reference/Guarantor Information</div>
<div class="form-group col-md-12"></div>

<div class="row">

    <div class="form-group col-md-4">
          <label>Reference Person's Name <font color="red"><sup>*</sup></font></label>
          <input type="text" required="required" name="gaurantor_name" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')">
    </div>

	<div class="form-group col-md-4">
          <label>Mobile Number <font color="red"><sup>*</sup></font></label> 
          <input type="text" required="required" name="gaurantor_mobile" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="10">
	</div>

   <div class="form-group col-md-4">
        <label>Address <font color="red"><sup>*</sup></font></label>
        <textarea name="gaurantor_address" required="required" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,-0-9]/,'')"></textarea>
    </div>

</div>

<div class="row bg-warning" style="padding:4px;">Relative in company ?</div>
<div class="form-group col-md-12"></div>
	
<div class="row">

<!--       <div class="col-md-4">
        <div class="form-group">
          <label> RELATIVE IN COMPANY (NAME) </label>
          <input type="text" name="relative_name" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9]/,'')" >
        </div>
      </div>
 -->
    <div class="form-group col-md-4">
		<label>Any Relative in Company ? <font color="red"><sup>*</sup></font></label>
		<select style="font-size:12px;cursor: pointer;" class="form-control" name="relative_name" id="relativeid"  required="required" onchange="FetchRelative(this.value)">
			<option style="font-size:12px" value="">--Select Employee--</option>
			<option style="font-size:12px" value="NO" style="color: red;">NO RELATIVE</option>
			<?php 
			$get_emp_name = Qry($conn,"SELECT name,branch,code,mobile_no,currentaddr FROM emp_attendance WHERE status='3' ORDER BY name ASC");
			if(numRows($get_emp_name)>0)
			{
				while($row_emp=fetchArray($get_emp_name))
				{
					echo "<option style='font-size:12px' value=".$row_emp['code'].">".$row_emp['name']." (".$row_emp['code'].") - ".$row_emp['branch']."</option>";
				}
			}
			?>  
		</select>
    </div>
   
<script>   
function FetchRelative(code)
{
	if(code!='')
	{
		$("#loadicon").show();
		$.ajax({
		url: "fetch_relative.php",
		method: "post",
		data:'code=' + code,
		success: function(data){
			$("#result_emp").html(data);	
		}})
	}
}
</script>   
     
	<div class="form-group col-md-3">
		<label>Relative's Mobile No. <font color="red"><sup>*</sup></font></label> 
        <input type="text" id="relative_mobile" name="relative_mobile" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" readonly="readonly">
	</div>

    
	<div class="form-group col-md-5">
         <label>Relative's Address <font color="red"><sup>*</sup></font></label>
         <textarea type="text" id="relative_address" name="relative_address" class="form-control" readonly="readonly"></textarea>
    </div>
	
</div>
		</div>
		</div>
      </div>
	  
	  <input type="hidden" name="emp_name1" value="<?php echo $row['name']; ?>" />
	  <input type="hidden" name="emp_code1" value="<?php echo $row['code']; ?>" />
	  <input type="hidden" name="emp_id1" value="<?php echo $id; ?>" />
	 
      <div class="modal-footer">
		<button type="submit" disabled id="emp_edit_save_btn" class="btn btn-sm btn-primary">Save</button>
		<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormEmpEditData").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#emp_edit_save_btn").attr("disabled", true);
	$.ajax({
        	url: "./save_emp_edit.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_emp").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

$('#birthdate').change(function() {
dob = new Date($('#birthdate').val());
var today = new Date();
var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
$('#age').val(age);
});
</script>

<script>
function ValidateNewPan() { 
  var Obj = document.getElementById("pan_no");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
      document.getElementById("pan_no").setAttribute("style","background-color:red;color:#fff;");
	document.getElementById("emp_edit_save_btn").setAttribute("disabled",true);
    Obj.focus();
                return false;
            }
          else
            {
              document.getElementById("pan_no").setAttribute("style","background-color:green;color:#fff;");
			  document.getElementById("emp_edit_save_btn").removeAttribute("disabled");
          }
        }
		else
		{
			document.getElementById("emp_edit_save_btn").removeAttribute("disabled");
		}
  } 			
</script>

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("acc_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("acc_ifsc").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("emp_edit_save_btn").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("acc_ifsc").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("emp_edit_save_btn").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("acc_ifsc").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("emp_edit_save_btn").removeAttribute("disabled");
		}
 } 			
</script>

<div id="result_emp"></div>  