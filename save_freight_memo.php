<?php
require_once 'connection.php';

// if($branch!=='CGROAD')
// {
	// echo "<script>
		// alert('Voucher Creation Temporary on hold till 1:40 PM.');
		// window.location.href='./';
	// </script>";
	// exit();
// }

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(!isset($_SESSION['freight_memo']))
{
	echo "<script>
		window.location.href='smemo.php';
	</script>";
	exit();
}

$vou_no = escapeString($conn,$_SESSION['freight_memo']);

$delete_cache_ewb_old = Qry($conn,"DELETE FROM lr_pre_ewb WHERE date(timestamp)<'$date'");

if(!$delete_cache_ewb_old){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_cache = Qry($conn,"SELECT id FROM lr_pre WHERE frno='$vou_no'");

if(!$get_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_cache)==0)
{
	echo "<script>
		alert('LR Cache not found !');
		window.location.href='./smemo.php';
	</script>";
	exit();
}

if(empty($_POST['signature']))
{
	echo '<script>
			alert("Please Save Signature First !");
			$("#freight_memo_button").attr("disabled", false);
		</script>';
	HideLoadicon();
	exit();
}
else
{
	$signature = $_POST['signature'];
}

$broker = escapeString($conn,strtoupper($_POST['broker_id']));
$driver = escapeString($conn,strtoupper($_POST['driver_id']));
$owner = escapeString($conn,strtoupper($_POST['owner_id']));
$company = escapeString($conn,strtoupper($_POST['company']));

$broker_pan_1 = escapeString($conn,strtoupper($_POST['broker_pan_1']));
$owner_pan_1 = escapeString($conn,strtoupper($_POST['owner_pan_1']));

$from_loc = explode("_", escapeString($conn,strtoupper($_POST['start_location'])))[0];  
$from_id = explode("_", escapeString($conn,strtoupper($_POST['start_location'])))[1];  
$to_loc = explode("_", escapeString($conn,strtoupper($_POST['last_location'])))[0];  
$to_id = explode("_", escapeString($conn,strtoupper($_POST['last_location'])))[1];  

$weight = escapeString($conn,strtoupper($_POST['weight_g']));
$known = escapeString($conn,strtoupper($_POST['known']));
$rate = escapeString($conn,strtoupper($_POST['rate']));
$freight = escapeString($conn,strtoupper($_POST['actual']));

$Check_ZeroFreight_Party = Qry($conn,"SELECT id FROM _zero_freight_party WHERE (pan_no='$broker_pan_1' || pan_no='$owner_pan_1') AND is_active='1'");
if(!$Check_ZeroFreight_Party){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($Check_ZeroFreight_Party)>0 AND $freight>0)
{
	echo "<script>
			alert('Freight amount must be Zero.');
			$('#freight_memo_button').attr('disabled', false);
		</script>";
	HideLoadicon();
	exit();
}

		if($known=='FIX')
		{
			if($weight<=0 OR $rate<=0 OR $freight<=0)
			{	
				echo "<script>
						alert('Please check weight,rate or freight..');
						$('#freight_memo_button').attr('disabled', false);
					</script>";
					HideLoadicon();
				exit();
			}
			
			if(abs($rate-sprintf("%.2f",($freight/$weight)))>1)
			{
				echo "<script>
						alert('Something Wrong Error : Please Check Rate: $rate or Feright: $freight.');
						$('#freight_memo_button').attr('disabled', false);
					</script>";
					HideLoadicon();
				exit();
			}
		}
		else if($known=='RATE')
		{
			if($weight<=0 OR $rate<=0 OR $freight<=0)
			{	
				echo "<script>
					alert('Please check weight,rate or freight..');
					$('#freight_memo_button').attr('disabled', false);
				</script>";
				HideLoadicon();
				exit();
			}
			
			if(abs($freight-round($rate*$weight))>1)
			{
				$sys_freight = round($rate*$weight);
				
				echo "<script>
					alert('Something Wrong Error : Please Check Freight: $sys_freight or Rate: $rate !!');
					$('#freight_memo_button').attr('disabled', false);
				</script>";
				HideLoadicon();
				exit();
			}
		}
		else if($known=='ZERO')
		{
			$rate=0;
			$freight=0;
		}

$sel_act_sum=Qry($conn,"SELECT sum(wt12) as total_aw FROM lr_pre WHERE frno='$vou_no'");
if(!$sel_act_sum){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_act_sum = fetchArray($sel_act_sum);
$total_aw=$row_act_sum['total_aw'];

$fetch_data = Qry($conn,"SELECT id,wt12 FROM lr_pre WHERE frno='$vou_no'");
if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_data)==0)
{
	Redirect("LR not found.","./");
	errorLog("Unable to fetch LR Data. VouNo : $vou_no.",$conn,$page_name,__LINE__);
	exit();
}
	
StartCommit($conn);
$flag = true;
	
	if(numRows($fetch_data)==1)
	{
		$update_data=Qry($conn,"UPDATE lr_pre SET weight='$weight',ratepmt='$rate',fix_rate='$known',actualf='$freight' WHERE frno='$vou_no'");
		if(!$update_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		while($data_row=fetchArray($fetch_data))
		{
			$db_id=$data_row['id'];
			$db_aw=$data_row['wt12'];
			$x=($db_aw*100)/$total_aw;
			$cw_db = ($weight*$x)/100;
			$update_frt = $cw_db*$rate;

			$update_data=Qry($conn,"UPDATE lr_pre SET weight='$cw_db',ratepmt='$rate',fix_rate='$known',actualf='$update_frt' WHERE 
			id='$db_id' AND frno='$vou_no'");
			if(!$update_data){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}

		$check_sum=Qry($conn,"SELECT SUM(weight) as lr_weight,SUM(actualf) as freight1 FROM lr_pre WHERE frno='$vou_no'");
		if(!$check_sum){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$row_sum=fetchArray($check_sum);

		if($weight!=$row_sum['lr_weight'])
		{
			$weight_diff = sprintf("%.3f",($weight-$row_sum['lr_weight']));
		}
		else
		{
			$weight_diff=0;
		}
	
		if($freight!=$row_sum['freight1'])
		{
			$freight_diff = sprintf("%.2f",($freight-$row_sum['freight1']));
		}
		else
		{
			$freight_diff=0;
		}

		$update_data_lr=Qry($conn,"UPDATE lr_pre SET weight=weight+'$weight_diff',actualf=actualf+'$freight_diff' WHERE frno='$vou_no' 
		ORDER BY wt12 DESC, id DESC LIMIT 1");
		
		if(!$update_data_lr){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

$select_weight_sum=Qry($conn,"SELECT sum(weight) as total_charge FROM lr_pre WHERE frno='$vou_no'");
if(!$select_weight_sum){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row1_sum=fetchArray($select_weight_sum);

if($row1_sum['total_charge'] != $weight)
{
	$weight_from_lr = $row1_sum['total_charge'];
	$wt_enter = $weight;
	$flag = false;
	errorLog("Charge weight Error. LR Weight: $weight_from_lr, Entered : $wt_enter.",$conn,$page_name,__LINE__);
}
			
$lrcopy = Qry($conn,"INSERT INTO freight_form_lr(frno,company,branch,date,create_date,truck_no,lrno,mother_lr_id,fstation,tstation,
tstation_bak,consignor,consignee,from_id,to_id,to_id_bak,con1_id,con2_id,dest_zone,dest_zone_id,wt12,weight,ratepmt,fix_rate,actualf,
crossing,cross_to,cross_stn_id,brk_id,timestamp) SELECT frno,company,branch,date,create_date,truck_no,lrno,lr_id,fstation,tstation,tstation,
consignor,consignee,from_id,to_id,to_id,con1_id,con2_id,dest_zone,dest_zone_id,wt12,weight,ratepmt,fix_rate,actualf,crossing,
cross_to,cross_stn_id,brk_id,timestamp FROM lr_pre WHERE frno='$vou_no'");

if(!$lrcopy){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$close_breaked_lrs = Qry($conn,"SELECT GROUP_CONCAT(brk_id SEPARATOR ',') as brk_id FROM lr_pre WHERE frno='$vou_no' AND brk_id!=''");
if(!$close_breaked_lrs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($close_breaked_lrs)>0)
{
	$row_breaking_ids = fetchArray($close_breaked_lrs);
	
	if($row_breaking_ids['brk_id']!='')
	{
		$breaking_ids = $row_breaking_ids['brk_id'];
	}
	else
	{
		$breaking_ids = "0";
	}
	
	$close_breaking = Qry($conn,"UPDATE lr_break SET crossing='YES' WHERE id IN($breaking_ids)");
		if(!$close_breaking){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
}
else
{
	$breaking_ids = "0";
}

$update_crossing_data = Qry($conn,"UPDATE lr_sample lr1,lr_pre lr2 SET lr1.crossing = lr2.crossing WHERE lr2.frno='$vou_no' AND 
lr1.id = lr2.lr_id");

if(!$update_crossing_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// UPDATE lr_sample SET crossing=(SELECT crossing FROM lr_pre WHERE lr_pre.lr_id=lr_sample.id) t1 
        // INNER JOIN lr_pre t2 
        // ON t1.id = t2.lr_id
		// SET t1.crossing = t2.crossing 
		// WHERE t2.frno='$vou_no' AND t1.id = t2.lr_id
				
$delete_lr_cache = Qry($conn,"DELETE FROM lr_sample_pending WHERE lrno IN(SELECT lrno FROM lr_pre WHERE frno='$vou_no')");

if(!$delete_lr_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_branch_code = Qry($conn,"SELECT branch_code FROM user WHERE username='$branch'");

if(!$get_branch_code){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_branch_code) == 0)
{
	$flag = false;
	errorLog("Branch not found. $branch.",$conn,$page_name,__LINE__);
}

$row_branch_code = fetchArray($get_branch_code);

if(strlen($row_branch_code['branch_code']) != 3)
{
	$flag = false;
	errorLog("Branch code not found. $branch.",$conn,$page_name,__LINE__);
}

$branch2 = strtoupper($row_branch_code['branch_code'])."F".date("dmY");
   
$ramanc = Qry($conn,"SELECT frno FROM freight_form where frno like '$branch2%' ORDER BY id DESC LIMIT 1");

if(!$ramanc){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$row=fetchArray($ramanc);

if(numRows($ramanc)>0)
{
	$count=$row['frno'];
	$newstring = substr($count,12);
	$newstr = ++$newstring;
	$memoid=$branch2.$newstr;
}
else
{
	$count2=1;
	$memoid=$branch2.$count2;
}
		
// $update_ewb_vou_no = Qry($conn,"UPDATE _eway_bill_validity t1 
// INNER JOIN lr_pre t2 
// ON t1.lrno = t2.lrno
// SET t1.vou_no = '$memoid' 
// WHERE t1.lrno = t2.lrno");

// if(!$update_ewb_vou_no){
	// $flag = false;
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
// }		

// insert duplicate by ewb no

$qry_get_ewb_no = Qry($conn,"SELECT vou_no,ewb_no,lrno FROM _eway_bill_validity WHERE ewb_no in(SELECT ewb_no FROM lr_pre_ewb WHERE frno='$vou_no')");

if(!$qry_get_ewb_no){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if(numRows($qry_get_ewb_no) > 0)
{
	while($row_duplicate_ewb = fetchArray($qry_get_ewb_no))
	{
		$chk_crossing = Qry($conn,"SELECT id FROM lr_pre_ewb WHERE frno='$vou_no' AND lrno='$row_duplicate_ewb[lrno]' AND 
		ewb_no='$row_duplicate_ewb[ewb_no]'");
		
		if(!$chk_crossing){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($chk_crossing) > 0){
			$crossing_flag="YES";
		}
		else{
			$crossing_flag="NO";
		}
		
		$insert_ewb_duplicate_vou = Qry($conn,"INSERT INTO _ewb_multiple_fm(ewb_no,prev_vou,prev_lrno,current_vou,current_lrno,branch,
		branch_user,crossing,timestamp) VALUES ('$row_duplicate_ewb[ewb_no]','$row_duplicate_ewb[vou_no]',
		(SELECT GROUP_CONCAT(lrno SEPARATOR ',') FROM lr_pre_ewb),'$row_duplicate_ewb[lrno]','$memoid','$branch','$branch_sub_user',
		'$crossing_flag','$timestamp')");
		
		if(!$insert_ewb_duplicate_vou){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
	
// insert duplicate by ewb no	

$lr_update=Qry($conn,"UPDATE freight_form_lr SET frno='$memoid' WHERE frno='$vou_no'");
if(!$lr_update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$img = str_replace('data:image/png;base64,', '', $signature);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
file_put_contents('sign/fm/'.$memoid.'.png',$data);

$cash_sign="sign/fm/$memoid.png";

$query=Qry($conn,"INSERT INTO freight_form(frno,date,company,newdate,branch,branch_user,bid,did,oid,from1,to1,from_id,to_id,weight,truck_no,
sign_cash,timestamp) VALUES ('$memoid','$date','$company','$date','$branch','$branch_sub_user','$broker','$driver','$owner','$from_loc','$to_loc',
'$from_id','$to_id','$weight','$_SESSION[freight_memo_vehicle]','$cash_sign','$timestamp')");

if(!$query){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$select_main_LR_Count = Qry($conn,"SELECT COUNT(id) as total_main_lr FROM lr_pre WHERE frno='$vou_no' AND lr_type=0");
if(!$select_main_LR_Count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$rowLRCount = fetchArray($select_main_LR_Count);

$update_lr_count = Qry($conn,"UPDATE _pending_lr SET market_lr=market_lr-'$rowLRCount[total_main_lr]' WHERE branch='$branch'");
if(!$update_lr_count){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_ewb_cache = Qry($conn,"SELECT id,by_pass FROM lr_pre_ewb WHERE frno='$vou_no'");

if(!$chk_ewb_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_ewb_cache)>0)
{
	while($row_ewb = fetchArray($chk_ewb_cache))
	{
		$insert_ewb_data = Qry($conn,"INSERT INTO _eway_bill_lr_wise(lrno,lr_id,branch,ewb_no,ewb_flag_desc,by_pass,ewb_copy,ewayBillDate,ewb_date,
		ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp) SELECT lrno,lr_id,branch,ewb_no,ewb_flag_desc,
		by_pass,ewb_copy,ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp 
		FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

		if(!$insert_ewb_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$ewb_id = getInsertID($conn);

		$insert_ewb_data2 = Qry($conn,"INSERT INTO _eway_bill_validity(owner_mobile,driver_mobile,vou_no,lrno,truck_no,lr_date,from_loc,to_loc,
		consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,ewb_id,ewb_date,ewb_expiry,ewb_extended,timestamp) SELECT '','','$memoid',lrno,truck_no,lr_date,
		from_loc,to_loc,consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,'$ewb_id',ewb_date,ewb_expiry,ewb_extended,timestamp 
		FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

		if(!$insert_ewb_data2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($row_ewb['by_pass']=="YES")
		{
			$insert_ewb_by_pass = Qry($conn,"INSERT INTO _eway_bill_by_pass(lrno,by_pass_id,lr_id,ewb_no,branch,narration,timestamp) SELECT 
			lrno,by_pass_id,lr_id,ewb_no,branch,by_pass_narration,timestamp FROM lr_pre_ewb WHERE frno='$vou_no' AND id='$row_ewb[id]'");

			if(!$insert_ewb_by_pass){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

$delete_cache = Qry($conn,"DELETE FROM lr_pre WHERE frno='$vou_no'");
if(!$delete_cache){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_cache_ewb = Qry($conn,"DELETE FROM lr_pre_ewb WHERE frno='$vou_no'");
if(!$delete_cache_ewb){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}


if($flag)
{
	// echo "<form action='select_step.php' id='myForm' method='POST'>
			// <input type='hidden' value='$memoid' name='fid'>		
		// </form>";
	MySQLCommit($conn);
	unset($_SESSION['freight_memo']);
	unset($_SESSION['freight_memo_vehicle']);
	echo "<script>
		// alert('Freight Memo Saved : $memoid.');
		$('#freight_memo_id_val').html('$memoid');
		$('#freight_memo_id_val2').val('$memoid');
		document.getElementById('button_modal2').click();
	</script>";
	HideLoadicon();
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./smemo.php");
	exit();
}
?>