<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
	font-size:12.5px !important;
}
</style>

<script>
$(function() {
		$("#location").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#location').val(ui.item.value);   
            $('#location_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#location').val("");   
			$('#location_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#tno_market").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_market').val(ui.item.value);   
            $('#vehicle_id').val(ui.item.oid);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			// $(event.target).val("");
            // $(event.target).focus();
			$('#vehicle_id').val("");   
			// $('#wheeler_market_truck').val("");   
			// alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});


function FetchRC()
{
	var tno = $('#tno_market').val();
	
	if(tno!='')
	{
		$('#spinner_icon').show();
		$('#fetch_button').attr('disabled',true);
		$('#tno_market').attr('readonly',true);
		jQuery.ajax({
			url: "./_fetch_vehicle_rc.php",
			data: 'tno=' + tno,
			type: "POST",
			success: function(data) {
				$("#rc_result").html(data);
		},
		error: function() {}
		});
	}
}

function FetchDL()
{
	var lic_no = $('#lic_no').val();
	var vehicle_check = $('#vehicle_check').val();
	
	if(lic_no!='')
	{
		if(vehicle_check!='YES')
		{
			alert('Warning : Fetch RC First !');
		}
		else
		{
			$('#spinner_icon_dl').show();
			$('#fetch_button_dl').attr('disabled',true);
			$('#lic_no').attr('readonly',true);
			jQuery.ajax({
				url: "./_fetch_driver_dl.php",
				data: 'lic_no=' + lic_no,
				type: "POST",
				success: function(data) {
					$("#rc_result").html(data);
			},
			error: function() {}
			});
		}
	}
}
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#trip_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_vehicle_placement_req.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#rc_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<form autocomplete="off" id="Form1" method="POST">	

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	
<div class="row">
	
	<div class="col-md-12" id="rc_result"></div>
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Vehicle Placement Request</h4><span style="color:#FFF;font-size:12px">(for market vehicles only)</span></center>
		<br />
	</div>

<div class="form-group col-md-12">&nbsp;</div>			


	<div class="form-group col-md-6">

	<div class="form-group col-md-7">
		<label><span id="loc_type">Vehicle Number <font color="red"><sup style="color:#FFF">*</sup></font></label>
		<input style="text-transform:uppercase" name="tno_market" id="tno_market" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">	
		<label>&nbsp;</label>
		<?php 
		if(!isMobile()){
			echo "<br>";
		}
		?>
		<button id="fetch_button" onclick="FetchRC()" type="button" style="color:#000;letter-spacing:1px;font-weight:bold;" class="btn btn-sm btn-warning">
		<i id="spinner_icon" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> Fetch RC</button>
	</div>
	
	<input type="hidden" name="vehicle_id" id="vehicle_id">
	<input type="hidden" name="vehicle_check" id="vehicle_check">
	
	<div class="form-group col-md-12 rc_res_div" style="display:none">&nbsp;</div>
	
	<div class="form-group col-md-10 rc_res_div" style="display:none">
		<table class="table table-bordered" style="font-size:11px !important">
			<tr>
				<td colspan="2">Vehicle Owner : <span id="owner_name">NA</span></td>
			</tr>
			
			<tr>
				<td colspan="2">Father Name : <span id="father_name">NA</span></td>
			</tr>
			
			<tr>
				<td colspan="2">PUC Expiry : <span id="puc_exp">NA</span></td>
			</tr>
			
			<tr>
				<td>Fitness Expiry : <span id="fitness_exp">NA</span></td>
				<td>Insurance Expiry : <span id="ins_exp">NA</span></td>
			</tr>
			
			<tr>
				<td colspan="2">Insurance Company : <span id="ins_comp_name">NA</span></td>
			</tr>
			
			
			<tr>
				<td>Permit Expiry<sup> (1 Yr)</sup> : <span id="p1_exp">NA</span></td>
				<td>Permit Expiry<sup> (5 Yr)</sup> : <span id="p_ltt_exp">NA</span></td>
			</tr>
			
			<tr>
				<td colspan="2">Permit Type : <br><span id="permit_type">NA</span></td>
			</tr>
			
		</table>
	</div>
</div>

<div class="form-group col-md-6">
	
	<div class="form-group col-md-7">
		<label><span id="loc_type">Driver's License Number <font color="red"><sup style="color:#FFF">*</sup></font></label>
		<input name="lic_no" style="text-transform:uppercase" id="lic_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">	
		<label>&nbsp;</label>
		<?php 
		if(!isMobile()){
			echo "<br>";
		}
		?>
		<button id="fetch_button_dl" onclick="FetchDL()" type="button" style="color:#000;letter-spacing:1px;font-weight:bold;" class="btn btn-sm btn-warning">
		<i id="spinner_icon_dl" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> Fetch DL</button>
	</div>
	
	<div class="form-group col-md-12 dl_res_div" style="display:none">&nbsp;</div>
	
	<div class="form-group col-md-10 dl_res_div" style="display:none">
		<table class="table table-bordered" style="font-size:11px !important">
			<tr>
				<td><img style="width:100%;height:100px" id="dl_driver_photo" src="data:image/png;base64," /></td>
				<td>
					Driver Name : <span id="dl_driver_name">NA</span>
					<br />
					<br />
					Father Name : <span id="dl_father_name">NA</span>
					<br />
					<br />
					Date of Birth : <span id="dl_dob">NA</span>
				</td>
			</tr>
			
			<tr>
				<td>License Expiry : <span id="dl_exp">NA</span></td>
				<td>License Expiry <sup>(TP)</sup> : <span id="dl_exp_tp">NA</span></td>
			</tr>
			
			<tr>
				<td>Date of issue : <span id="dl_issue_date">NA</span></td>
				<td>Address : <span id="dl_addr">NA</span></td>
			</tr>
			
			<tr>
				<td colspan="2">Issued By : <span id="dl_issue_by">NA</span></td>
			</tr>
			
		</table>
	</div>

</div>
	
	<!--
	<div class="form-group col-md-3">	
		<label>&nbsp;</label>
		<?php 
		// if(!isMobile()){
			// echo "<br>";
		// }
		?>
		<button id="fetch_button_dl" type="button" style="color:#000;letter-spacing:1px;font-weight:bold;" class="btn btn-sm btn-warning">
		<i id="spinner_icon_dl" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> Fetch DL</button>
	</div>
	
	<div class="form-group col-md-12">&nbsp;</div>
	
	<div class="form-group col-md-10 col-md-offset-1">
		<table class="table table-bordered" style="font-size:12px !important">
			<tr>
				<td colspan="2">Vehicle Owner: <span id="owner_name">NA</span></td>
			</tr>
			
			<tr>
				<td>Father Name : <span id="father_name">NA</span></td>
				<td>PUC Expiry : <span id="puc_exp">NA</span></td>
			</tr>
			
			<tr>
				<td>Fitness Expiry : <span id="fitness_exp">NA</span></td>
				<td>Insurance Expiry : <span id="ins_exp">NA</span></td>
			</tr>
			
			<tr>
				<td>Permit Expiry<sup> (1 Yr)</sup> : <span id="p1_exp">NA</span></td>
				<td>Permit Expiry<sup> (LTT)</sup> : <span id="p_ltt_exp">NA</span></td>
			</tr>
			
		</table>
	</div>
	-->
	
	<div class="form-group col-md-12 mobile_no_div" style="display:none">
		<div class="form-group col-md-4">
			<label><span id="loc_type">Driver's Mobile Number <font color="red"><sup style="color:#FFF">*</sup></font></label>
			<input name="dl_mobile_no" maxlength="10" style="text-transform:uppercase" id="dl_mobile_no" oninput="this.value=this.value.replace(/[^0-9]/,'');" type="text" class="form-control" required />
		</div>
	
		<div class="form-group col-md-12">	
			<input id="button_sub" type="submit" disabled style="display:none;color:#000;letter-spacing:1px; font-weight:bold;" 
			class="btn btn-warning" name="submit" value="Save Placement Request" />
		</div>
	</div>

</div>

</div>
</div>
</form>
</body>
</html>