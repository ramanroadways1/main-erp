<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#LiveLocForm").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./upload_veh_data_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#function_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" id="LiveLocForm">	
	
<div class="row">
	
<div class="form-group col-md-6">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Update Live Location : Own Vehicle</h4></center>
		<br />
	</div>
	
<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: '../diary/autofill/own_tno.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 	

	<div class="form-group col-md-6">
		<label>Vehicle Number <font color="red">*</font></label>
		<input id="truck_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="truck_no" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-6">
		<label>Start Point <sup>(optional)</sup></label>
		<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="start_point" type="text" class="form-control" />
    </div>


	<div class="form-group col-md-6">
		<label>End Point <sup>(optional)</sup></label>
		<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="end_point" type="text" class="form-control" />
    </div>

	<div class="form-group col-md-6">
		<label>Current Location <sup>(optional)</sup></label>
		<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" name="current_location" type="text" class="form-control" />
    </div>
	
	<div class="form-group col-md-6">
		<label>Upload Screenshot <font color="red">*</font></label>
		<input name="file_name" type="file" accept="image/*" class="form-control" required />
    </div>
	
	<div class="form-group col-md-6">
		<label>&nbsp;</label>	
		<br />
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="Update Location" />
	</div>
	
	<div id="function_result"></div>
	
</div>
	
	
<div class="form-group col-md-6">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Share Vehicle Status</h4></center>
		<br />
		<br />
	</div>
	
	<div class="form-group col-md-12" style="max-height:350px">	
	
	<table id="example" class="table table-striped table-bordered" style="width:100%;font-size:12px">
        <thead>
            <tr>
                <th>Vehicle Number</th>
                <th>Link</th>
            </tr>
        </thead>
		
		<tbody>
<?php
$chk_veh = Qry($conn,"SELECT id,tno,tno_unq FROM _vehicle_live_location");

if(!$chk_veh){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_veh)>0)
{
	while($row2 = fetchArray($chk_veh))
	{
		echo "<tr>
			<td>$row2[tno]</td>
			<input type='hidden' value='https://rrpl.online/veh_status.php?tno=$row2[tno_unq]' id='link_location$row2[id]'>
			<td><button class='btn btn-default btn-xs' type='button' onclick=myFunction('$row2[id]')>Copy Link</button></td>
		</tr>";
	}
}
else
{
	echo "<tr><td colspan='2'>No record found..</td></tr>";
}
			 ?>
		</tbody>
		
    </table>
	</div>
	
</div>

</div>

</div>
</div>
</form>
</body>
</html>

<script>
function myFunction(id) {
var copyText = document.getElementById("link_location"+id);
copyText.type = 'text';
copyText.select();
document.execCommand("copy");
copyText.type = 'hidden';
alert("Copied");
}
</script>

<script>
function myFunction22(id) {
  var copyText = document.getElementById("link_location"+id);

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>