<form id="DrBankDepositForm" action="#" method="POST">
<div id="BankDebitModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Debit : Cash deposit to bank
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label style="font-size:13px">Debit From Company (-) <font color="red">*</font></label>
				<select onchange="DebitFromComp(this.value)" class="form-control" id="debit_from_comp" name="company" required="required">
					<option class="company_sel_bank" value="">--Select Company--</option>
					<option class="company_sel_bank" id="dr_comp_RRPL" value="RRPL">RRPL</option>
					<option class="company_sel_bank" id="dr_comp_RR" value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				</select>
			</div>
			
			<script>
			function DebitFromComp(elem)
			{
				$('#dr_bank_ac_list').val('');
				
				if(elem!='')
				{
					$('#loadicon').show();
					$.ajax({
					url: "_load_ac_list.php",
					method: "post",
					data:'company=' + elem,
					success: function(data){
						$("#dr_bank_ac_list").html(data);
					}})
				}
			}
			
			function CheckAcSelected(elem)
			{
				if(elem!='')
				{
					var res = elem.split("_");
					$('#dr_bank_name').val(res[2]);
					$('#dr_bank_ac_no').val(res[1]);
				}
				else
				{
					$('#dr_bank_name').val('');
					$('#dr_bank_ac_no').val('');
				}
			}
			</script>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Select Account number <font color="red">*</font></label>
				<select onchange="CheckAcSelected(this.value)" id="dr_bank_ac_list" data-size="8" name="tno_1" data-live-search="true" data-live-search-style="" class="form-control selectpicker" required>
					<option value="">--Select--</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Bank Name <font color="red">*</font></label>
				<input type="text" id="dr_bank_name" name="bank_name" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">A/c Number <font color="red">*</font></label>
				<input type="text" id="dr_bank_ac_no" name="ac_no_deposit" class="form-control" readonly required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Amount <font color="red">*</font></label>
				<input type="number" name="amount" min="1" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Deposit Slip <font color="red">*</font></label>
				<input type="file" accept="image/*" name="deposit_slip" class="form-control" required />	
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Deposit Date <font color="red">*</font></label>
				<input name="deposit_date" type="date" class="form-control" min="<?php echo date("Y-m-d", strtotime("-3 day"));?>" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-6">
				<label style="font-size:13px">Narration <font color="red">*</font></label>
				<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9/-]/,'');" name="narration" class="form-control" required="required"></textarea>	
			</div>
			
		</div>
      </div>
	 
	 <div id="result_dr_bank_modal"></div>
	  
    <div class="modal-footer">
        <button type="submit" disabled id="dr_bank_deposit_button" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    </div>

  </div>
</div>
</form>	

					
<script type="text/javascript">
$(document).ready(function (e) {
	$("#DrBankDepositForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#dr_bank_deposit_button").attr("disabled", true);
	$.ajax({
        	url: "./save_dr_bank_deposit.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data){
				$("#result_dr_bank_modal").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>