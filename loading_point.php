<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
	?>

<style>
label{
	font-size:12px;
	color:#FFF;
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormPOI").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./save_loading_point.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#function_result").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<style>
.ui-widget {
  font-family: Verdana;
  font-size: 12px;
}
</style>

<script>
$(function() {
		$("#from_loc").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_loc').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            $('#from_pincode').val(ui.item.pincode);      
            $('#from_state').val(ui.item.state);      
            $('#from_lat_long').val(ui.item.lat_lng);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_loc').val("");   
			$('#from_id').val("");   
			$('#from_pincode').val("");   
			$('#from_state').val("");   
			$('#from_lat_long').val("");   
			alert('Loading location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#owo_tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#owo_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#owo_tno').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {  
      $("#consignor").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignor').val(ui.item.value);   
               $('#con1_id').val(ui.item.id);     
               $('#con1_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignor").val('');
				$("#con1_id").val('');
				$("#con1_gst").val('');
                alert('Consignor does not exist !'); 
              } 
              },
			}); 
});  	
</script>

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" id="FormPOI">	
	
<div class="row">
	
<div class="form-group col-md-10 col-md-offset-1">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<?php if(isMobile()){ echo '<br>'; } ?>
		<center><h4 style="color:#FFF;letter-spacing:1px">Add loading point (लोडिंग पॉइंट ) :</h4></center>
		<br />
	</div>
	
	<div class="form-group col-md-4">
		<label>From/Loading location <font color="red">*</font></label>
		<input id="from_loc" oninput="this.value=this.value.replace(/[^a-z A-Z-]/,'')" name="from_loc" type="text" class="form-control" required />
    </div>
	 
	<input type="hidden" name="from_id" id="from_id">
	<input type="hidden" name="from_lat_long" id="from_lat_long">
	
	<div class="form-group col-md-4">
		<label>State &nbsp; <sup style="font-size:10px;letter-spacing:.5px;cursor:pointer">Incorrect details ?</sup></label>
		<input readonly id="from_state" type="text" class="form-control" />
    </div>
	
	<div class="form-group col-md-4">
		<label>From pincode &nbsp; <sup style="font-size:10px;letter-spacing:.5px;cursor:pointer">Incorrect details ?</sup></label>
		<input readonly id="from_pincode" type="text" class="form-control" />
    </div>

	<div class="form-group col-md-6">
		<label>Select consignor <font color="red">*</font></label>
		<input id="consignor" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,/]/,'')" name="consignor" type="text" class="form-control" required />
    </div>
	 
	<input type="hidden" name="con1_id" id="con1_id">
	
	<div class="form-group col-md-6">
		<label>Consignor GST</label>
		<input readonly id="con1_gst" name="con1_gst" type="text" class="form-control" />
    </div>
	
	<div class="form-group col-md-12">
		<label style="font-size:13px;margin-top:10px;letter-spacing:1px;">Does own truck visited the loading point ? क्या कंपनी का गाड़ी कभी  इस लोडिंग पॉइंट पर गया है ?</label> 
		<br />
		<label style="font-size:13px;margin-top:5px;letter-spacing:1px;">
		Yes <input value="1" onchange="CallRadioBtn(this.value)" required="required" type="radio" name="own_truck_visited">
		No <input type="radio" disabled value="0" required="required" onchange="CallRadioBtn(this.value)" name="own_truck_visited">
		</label>
	</div>

<script> 
function CallRadioBtn(elem)
{
	var from_lat_long = $('#from_lat_long').val();	
	var con1_id = $('#con1_id').val();
	var from_id = $('#from_id').val();
	
	if(from_lat_long=='')
	{
		$('#from_loc').focus();
		$("input:radio").removeAttr("checked");
	}
	else if(con1_id=='')
	{
		$('#consignor').focus();
		$("input:radio").removeAttr("checked");
	}
	else if(from_id=='')
	{
		$('#from_loc').focus();
		$("input:radio").removeAttr("checked");
	}
	else
	{
		$('#from_loc').attr('readonly',true);
		$('#consignor').attr('readonly',true);
		
		$('#owo_tno').val('');
		$('#visit_date').val('');
		$('#loading_point').html('<option valule="" style="font-size:12px">--select loading point--</option>');
		$('#button_sub').attr('disabled',true);
		
		$("#loadicon").show();
			jQuery.ajax({
				url: "./chk_loading_point_data.php",
				data: 'from_id=' + from_id + '&con1_id=' + con1_id + '&elem=' + elem,
				type: "POST",
				success: function(data) {
					$("#function_result").html(data);
				},
				error: function() {}
			});
	}
}

function GetStoppages(date1)
{
	var veh_no = $('#owo_tno').val();
	
	var from_id = $('#from_id').val();
	var con1_id = $('#con1_id').val();
	
	if(from_id=='' || con1_id=='')
	{
		if(from_id=='')
		{
			$('#from_loc').focus();
			$('#visit_date').val('');
		}
		else
		{
			$('#consignor').focus();
			$('#visit_date').val('');
		}
	}
	else
	{
		if(veh_no=='')
		{
			$('#owo_tno').focus();
			$('#visit_date').val('');
		}
		else
		{
			$('#owo_tno').attr('readonly',true);
			$('#button_sub').attr('disabled',true);
			$("#loadicon").show();
			jQuery.ajax({
				url: "./_fetch_stops.php",
				data: 'date=' + date1 + '&veh_no=' + veh_no + '&from_id=' + from_id + '&con1_id=' + con1_id,
				type: "POST",
				success: function(data) {
					$("#loading_point").html(data);
				},
				error: function() {}
			});
		}
	}
}

function VerifyStop(loading_point)
{
	var con1_id = $('#con1_id').val();
	var from_id = $('#from_id').val();
	
	if(loading_point!='')
	{
		var from_lat_long = $('#from_lat_long').val();
		
		if(from_lat_long=='')
		{
			alert('Location POI not found !');
			$('#loading_point').val('');
		}
		else if(con1_id=='')
		{
			$('#consignor').focus();
		}
		else if(from_id=='')
		{
			$('#from_loc').focus();
		}
		else
		{
			$("#loadicon").show();
			jQuery.ajax({
				url: "./fetch_distance_bw_two_points.php",
				data: 'loading_point=' + loading_point + '&from_lat_long=' + from_lat_long + '&from_id=' + from_id + '&con1_id=' + con1_id,
				type: "POST",
				success: function(data) {
					$("#function_result").html(data);
				},
				error: function() {}
			});
		}
	}
}

function ValidateGoogleLoc()
{
	var pincode = $('#pincode1').val();
	var google_addr = $('#google_addr').val();
	var google_lat = $('#google_lat').val();
	var google_lng = $('#google_lng').val();
	var from_lat_long = $('#from_lat_long').val();	
	var con1_id = $('#con1_id').val();
	var from_id = $('#from_id').val();
	
	if(from_lat_long=='')
	{
		$('#from_loc').focus();
	}
	else if(con1_id=='')
	{
		$('#consignor').focus();
	}
	else if(google_lat=='')
	{
		$('#search_loading_point').focus();
	}
	else if(from_id=='')
	{
		$('#from_loc').focus();
	} 
	else
	{
			$("#loadicon").show();
			jQuery.ajax({
				url: "./validate_google_loc.php",
				data: 'from_id=' + from_id + '&con1_id=' + con1_id + '&pincode=' + pincode + '&google_addr=' + google_addr + '&google_lat=' + google_lat + '&google_lng=' + google_lng + '&from_lat_long=' + from_lat_long,
				type: "POST",
				success: function(data) {
					$("#function_result").html(data);
				},
				error: function() {}
			});
	}
}
</script>

	<div class="gps_div form-group col-md-4">
		<label>Select own truck <font color="red">*</font></label>
		<input name="owo_tno" type="text" id="owo_tno" class="form-control" required oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" />
    </div>
	
	<div class="gps_div form-group col-md-4">
		<label>Visited on date <font color="red">*</font></label>
		<input onchange="GetStoppages(this.value)" name="visit_date" id="visit_date" type="date"  min="2021-01-01" class="form-control" max="<?php echo date("Y-m-d");?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
    </div>
	
	<div class="gps_div form-group col-md-4">
		<label>Select loading point <font color="red">*</font></label>
		<select onchange="VerifyStop(this.value)" style="font-size:12px" name="loading_point" id="loading_point" class="form-control" required="required">
			<option style="font-size:12px" value="">--select loading point--</option>
		</select>
    </div>
	
	<div style="display:none" class="google_div form-group col-md-6">
		<label>Search loading point (Google search) <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-/.@#&*()]/,'')" id="search_loading_point" name="search_loading_point" type="text" class="form-control" required />
    </div>
	
	<div style="display:none" class="google_div form-group col-md-3">
		<label>Pincode <font color="red">*</font></label>
		<input name="loading_pincode" id="loading_pincode" type="text" class="form-control" readonly required />
	</div>
	
	<div style="display:none" class="google_div form-group col-md-3">
		<label>&nbsp;</label>
		<?php if(!isMobile()){ echo '<br />'; } ?>
		<button style="color:#000;letter-spacing:1px; font-weight:bold;" disabled id="validate_btn_id" type="button" onclick="ValidateGoogleLoc()" class="btn btn-warning btn-sm">Validate</button>
    </div>
	
	<div class="form-group col-md-6">
		<label style="margin-top:10px;letter-spacing:.5px;">Set Label लोडिंग पॉइंट का नामकरण करे <font color="red">*</font></label>
		<textarea name="label_name" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-/.@#&*()]/,'')" class="form-control" required></textarea>
		<br />
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" 
		name="submit" value="Save Loading Point" />
    </div>
	
	<input type="hidden" id="google_addr" name="google_addr">
    <input type="hidden" id="google_lat" name="google_lat">
    <input type="hidden" id="google_lng" name="google_lng">
    <input type="hidden" id="pincode1" name="pincode1">
    <input type="hidden" id="pincode2" name="pincode2">
</ul>
	
	<div class="form-group col-md-6">
		<label>&nbsp;</label>
		<div class="map" id="map_div" style="border:.5px solid #666;margin-top:5px;width:100%;height:250px">
			<img src="../diary/map_def.jpg" style="width:100%;max-height:250px !important" />
		</div>
    </div>
	
	<div id="function_result"></div>
	
</div>
</div>

</div>
</div>
</form>
</body>
</html>

<script>
function initMap() {
    var input = document.getElementById('search_loading_point');
  
    var autocomplete = new google.maps.places.Autocomplete(input);
   
    autocomplete.addListener('place_changed', function() {
		$('#validate_btn_id').attr('disabled',true);
        var place = autocomplete.getPlace();
        var address = place.formatted_address;
        document.getElementById('google_addr').value = address;
        document.getElementById('google_lat').value = place.geometry.location.lat();
        document.getElementById('google_lng').value = place.geometry.location.lng();
		
		var addressComponent = place.address_components;
			
		for (var x = 0 ; x < addressComponent.length; x++) {
                var chk = addressComponent[x];
                if (chk.types[0] == 'postal_code') {
                    var zipCode = chk.long_name;
                }
            }
			
			 if (zipCode) {
                $('#loading_pincode').val(zipCode);
                $('#pincode1').val(zipCode);
				$('#validate_btn_id').attr('disabled',false);
            }
            else {
                document.getElementById('pincode2').value = 'No pincode';
				$('#validate_btn_id').attr('disabled',true);
            }
	});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw" async defer></script>