<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

// echo "<script>
		// alert('Temporary disabled.');
		// $('#loadicon').hide();
		// $('#delete_button_$id').attr('disabled',true);
	// </script>";
	// exit();

$id = escapeString($conn,strtoupper($_POST['id']));

$getLRData = Qry($conn,"SELECT lr_id,lrno,lr_type,branch,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignee,con2_addr,
from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,weight,bill_rate,bill_amt,t_type,do_no,shipno,
invno,item,item_id,plant,articles,goods_desc,goods_value,con2_gst,crossing,cancel,break,diesel_req,download FROM lr_sample_pending 
WHERE id='$id' AND crossing=''");

if(!$getLRData){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getLRData)==0){
	echo "<script>
		alert('LR not found.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

$row = fetchArray($getLRData);

$datediff = strtotime($timestamp) - strtotime($row['create_date']);
$diff_value=round($datediff / (60 * 60 * 24));	

if($diff_value>3)
{
	echo "<script>
		alert('LR older than 3 days. You can\'t delete this LR.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

$lr_id = $row['lr_id'];
$lrno = $row['lrno'];

if($row['download']=="1")
{
	echo "<script>
		alert('You can\'t delete this LR. Contact Head-Office.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

if($row['branch']!=$branch)
{
	echo "<script>
		alert('LR belongs to $row[branch] Branch.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['crossing']!="")
{
	echo "<script>
		alert('You Can\'t edit this LR. LR has been Closed.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['cancel']==1)
{
	echo "<script>
		alert('LR Marked as Canceled.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['break']>0)
{
	echo "<script>
		alert('This is Breaking LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['diesel_req']>0)
{
	echo "<script>
		alert('Diesel Request From This LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$get_ewb_no = Qry($conn,"SELECT id,ewb_no FROM _eway_bill_lr_wise WHERE lr_id='$lr_id'");
if(!$get_ewb_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_ewb_no)==0)
{
	$ewb_no = "";
}
else
{
	$row_ewb = fetchArray($get_ewb_no);
	$ewb_no = $row_ewb['ewb_no'];
}

$fetch_open_LR = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$row[con1_id]'");
if(!$fetch_open_LR){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$check_open_lr = numRows($fetch_open_LR);

if($check_open_lr==0)
{
	$fetch_bilty_id = Qry($conn,"SELECT bilty_id FROM lr_check WHERE lrno='$lrno'");
	if(!$fetch_bilty_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($fetch_bilty_id)==0){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Bilty id not found.","./");
		exit();
	}
	
	$row_bilty_id = fetchArray($fetch_bilty_id);
}

StartCommit($conn);
$flag = true;

if($check_open_lr==0)
{
	$update_stock = Qry($conn,"UPDATE lr_bank SET `stock_left`=`stock_left`+1 WHERE id='$row_bilty_id[bilty_id]'");
	
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Unable to update Bilty Stock Id: $row_bilty_id[bilty_id], LRNo: $lrno.",$conn,$page_name,__LINE__);
	}
} 

$copy_lr_data = Qry($conn,"INSERT INTO lr_sample_deleted(dlt_timestamp,deleted_branch,deleted_branch_user,lr_id,company,branch,branch_user,lrno,ewb_no,lr_type,
wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,consignee,con2_addr,from_id,to_id,con1_id,con2_id,
zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,item_id,plant,
articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,
billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) SELECT '$timestamp','$branch','$branch_sub_user',lr_id,company,branch,branch_user,
lrno,'$ewb_no',lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,consignee,con2_addr,from_id,to_id,
con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,
item,item_id,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,
downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus FROM lr_sample_pending WHERE id='$id'");

if(!$copy_lr_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_insert_id = getInsertID($conn);

$query_dlt = Qry($conn,"DELETE FROM lr_sample_pending WHERE id='$id' AND branch='$branch' AND crossing='' AND diesel_req=0 AND download=0");
if(!$query_dlt){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR lr_sample_pending. LRNo: $lrno.",$conn,$page_name,__LINE__);
}

$query_dlt_main = Qry($conn,"DELETE FROM lr_sample WHERE id='$lr_id' AND branch='$branch' AND crossing='' AND diesel_req=0 AND download=0");

if(!$query_dlt_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR lr_sample. LRNo: $lrno.",$conn,$page_name,__LINE__);
}
	
$query_dlt2=Qry($conn,"DELETE FROM lr_check WHERE lrno='$lrno'");
if(!$query_dlt2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR lr_check. LRNo: $lrno.",$conn,$page_name,__LINE__);
}

if($ewb_no!='')
{
	$dlt_ewb_lr_wise=Qry($conn,"DELETE FROM _eway_bill_lr_wise WHERE lr_id='$lr_id'");
	if(!$dlt_ewb_lr_wise){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$dlt_ewb_val=Qry($conn,"DELETE FROM _eway_bill_validity WHERE lr_id='$lr_id'");
	if(!$dlt_ewb_val){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($row['lr_type']=='MARKET')
{
	$update_lr_count = Qry($conn,"UPDATE _pending_lr SET market_lr=market_lr-1 WHERE branch='$branch'");
}
else
{
	$update_lr_count = Qry($conn,"UPDATE _pending_lr SET own_lr=own_lr-1 WHERE branch='$branch'");
}

if(!$update_lr_count){
	$flag = false; 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to update LR count. Branch : $branch. LRNo: $lrno.",$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp,branch_user) VALUES 
('$lrno','$lr_id','LR_DELETE','$dlt_insert_id','$branch','BRANCH','$timestamp','$branch_sub_user')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('LR Successfully Deleted.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./_fetch_lr_entry.php");
	exit();
}
?>									