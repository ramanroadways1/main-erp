<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$token = escapeString($conn,strtoupper($_POST['token']));
$party_id2 = escapeString($conn,strtoupper($_POST['party_id']));
$tab_name = escapeString($conn,($_POST['tab_name']));

if($party_id2=="" || $token=="")
{
	Redirect("Invalid Token Number or Party !","./");
	exit();
}

$getParty = Qry($conn,"SELECT party_id FROM `$tab_name` WHERE req_code='$token'");
if(!$getParty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getParty)==0){
	Redirect("Invalid Token Number !","./");
	exit();
}

$rowParty = fetchArray($getParty);

if($rowParty['party_id']==0){
	Redirect("Party not matching with token !","./");
	exit();
}

if($rowParty['party_id']!=$party_id2){
	Redirect("Party not assigned !","./");
	exit();
}

$PartyDetails = Qry($conn,"SELECT gst_no FROM asset_party WHERE id='$rowParty[party_id]'");

if(!$PartyDetails){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($PartyDetails)==0){
	Redirect("Party not found !","./");
	exit();
}

$rowDetails = fetchArray($PartyDetails);

if($rowDetails['gst_no']==""){
	echo "<script>
		alert('GST Number not found ! You can not enter GST Value.');
		$('#gst_selection').val('NO');
		$('#gst_value').attr('readonly',true);
		$('#gst_value').val('0');
		$('#gst_amount').val('0');
		$('#gst_type').val('');
		$('#total_amount').val($('#amount').val());
		$('#loadicon').hide();
		$('#btn_asset_add').attr('disabled',false);
	</script>";
}
else{
	if(substr($rowDetails['gst_no'], 0, 2)=="24"){
		echo "<script>$('#gst_type').val('CGST+SGST');</script>";
	}
	else{
		echo "<script>$('#gst_type').val('IGST');</script>";
	}

echo "<script>
		$('#gst_value').attr('readonly',false);
		$('#gst_value').val('');
		$('#gst_amount').val('0');
		$('#total_amount').val($('#amount').val());
		$('#loadicon').hide();
		$('#btn_asset_add').attr('disabled',false);
</script>";
}
?>