<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$check_lr = Qry($conn,"SELECT branch,company,date as lr_date,tstation,wt12 as actual_weight,weight as charge_weight,to_id,con1_id,crossing,
item_id,break,date(timestamp) as date_created FROM lr_sample WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found.');
		$('#crossing_lr_add_btn').attr('disabled',true);
		
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		
		$('#from_crossing_loc').val('');
		$('#to_crossing_loc').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('');
		$('#charge_weight_crossing').val('');
	</script>";
	HideLoadicon();
	exit();
}

echo "<script>$('#ewb_no_ip').attr('required',true);</script>";

$row_check_lr = fetchArray($check_lr);

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_check_lr['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

$chk_max_days_to_add = Qry($conn,"SELECT days_value FROM _max_days_for_lr_add WHERE branch='$branch'");
if(!$chk_max_days_to_add){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_max_days_to_add)>0)
{
	$row_max_days = fetchArray($chk_max_days_to_add);
	$max_days_to_add=$row_max_days['days_value'];
}
else
{
	$max_days_to_add=5;
}

$chk_validity_allow = Qry($conn,"SELECT id FROM allow_lr_exceed_validity WHERE lrno='$lrno' AND branch='$branch' AND is_pending='1'");
if(!$chk_validity_allow){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($diff_days>$max_days_to_add AND numRows($chk_validity_allow)==0)
{
	echo "<script>
		alert('Error : LR exceed $max_days_to_add days. LR date is: $row_check_lr[lr_date]');
		$('#crossing_lr_add_btn').attr('disabled',true);
		
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		
		$('#from_crossing_loc').val('');
		$('#to_crossing_loc').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('');
		$('#charge_weight_crossing').val('');
	</script>";
	HideLoadicon();
	exit();
}
	
if($row_check_lr['break']>0)
{
	echo "<script>
		alert('Error : This is breaking LR. Click on Add Breaking LR button.');
		$('#crossing_lr_add_btn').attr('disabled',true);
		
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		
		$('#from_crossing_loc').val('');
		$('#to_crossing_loc').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('');
		$('#charge_weight_crossing').val('');
	</script>";
	HideLoadicon();
	exit();	
}
	
if($row_check_lr['crossing']!='YES'){
	echo "<script>
		alert('LR not crossing enabled.');
		$('#crossing_lr_add_btn').attr('disabled',true);
		
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		$('#from_crossing_loc').val('');
		$('#to_crossing_loc').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('');
		$('#charge_weight_crossing').val('');
	</script>";
	HideLoadicon();
	exit();
}

$get_info = Qry($conn,"SELECT tstation,to_id FROM freight_form_lr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");
if(!$get_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_info)==0){
	echo "<script>
		alert('Previous Records not found. LR Number : $lrno.');
		$('#crossing_lr_add_btn').attr('disabled',true);
		
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		$('#from_crossing_loc').val('');
		$('#to_crossing_loc').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('');
		$('#charge_weight_crossing').val('');
	</script>";
	HideLoadicon();
	exit();
}

$row_prev = fetchArray($get_info);

$item_id = $row_check_lr['item_id'];
$con1_id = $row_check_lr['con1_id'];
$company = $row_check_lr['company'];
 
$chk_exempt_lrs="YES";

if($item_id=='101') // SALT LRs
{
	echo "<script>
		$('#ewb_no_ip').attr('readonly',true);
		$('#crossing_modal_ewb_desc').val('Salt Exempt');
	</script>";
	
	$chk_exempt_lrs="NO";
}
else
{
	if($con1_id=='56' || $con1_id=='675' || $con1_id=='158')
	{
		echo "<script>
			$('#ewb_no_ip').attr('readonly',false);
			$('#crossing_modal_ewb_desc').val('Consignor Exempt');
		</script>";
		
		$chk_exempt_lrs="NO";
	}
	else
	{
		echo "<script>
			$('#ewb_no_ip').attr('readonly',false);
			$('#crossing_modal_ewb_desc').val('Success');
		</script>";
	}
}


if($chk_exempt_lrs="YES")
{
	// $chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE item='$row_check_lr[item_id]' || consignor='$row_check_lr[con1_id]' || lrno='$lrno' AND status='1'");
	$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE (consignor='$con1_id' || lrno='$lrno') AND status='1'");
	if(!$chkEwayBillFree){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}

	if(numRows($chkEwayBillFree)>0) 
	{
		echo "<script>
			$('#ewb_no_ip').attr('readonly',true);
			$('#crossing_modal_ewb_desc').val('Admin Exempt');
		</script>";
	}
}

echo "<script>
		$('#from_crossing_loc').attr('readonly',true);
		$('#to_crossing_loc').attr('readonly',true);
		$('#from_crossing_loc').val('$row_prev[tstation]');
		$('#to_crossing_loc').val('$row_check_lr[tstation]');
		$('#cross_loc_from_id').val('$row_prev[to_id]');
		$('#cross_loc_to_id').val('$row_check_lr[to_id]');
		$('#cross_modal_con1_id').val('$row_check_lr[con1_id]');
		$('#cross_modal_item_id').val('$row_check_lr[item_id]');
		
		$('#crossing_lr_add_btn').attr('disabled',false);
		$('#crossing_fetch_btn').hide();
		$('#lrno_crossing_1').attr('readonly',true);
		
		$('#actual_weight_crossing').attr('readonly',true);
		$('#charge_weight_crossing').attr('readonly',true);
		$('#actual_weight_crossing').val('$row_check_lr[actual_weight]');
		$('#charge_weight_crossing').val('$row_check_lr[charge_weight]');
		$('#crossing_modal_company_name').val('$company');
</script>";

if($row_check_lr['branch']=='MUNDRA' AND ($row_check_lr['actual_weight']=='2' || $row_check_lr['actual_weight']=='4'))
{
	echo "<script>
		$('#actual_weight_crossing').attr('readonly',false);
		$('#charge_weight_crossing').attr('readonly',false);
		$('#to_crossing_loc').attr('readonly',false);
	</script>";
}
	HideLoadicon();
	exit();
?>