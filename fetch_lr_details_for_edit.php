<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");

$id= escapeString($conn,strtoupper($_POST['id']));

$getLRData = Qry($conn,"SELECT lrno,lr_type,branch,wheeler,date,truck_no,fstation,tstation,dest_zone,consignee,con2_addr,from_id,to_id,con1_id,
con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,item_id,plant,
articles,goods_desc,goods_value,con2_gst,hsn_code,crossing,cancel,break,diesel_req,download FROM lr_sample_pending WHERE id='$id' AND crossing=''");

if(!$getLRData){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getLRData)==0){
	echo "<script>
		alert('LR not found.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

$row = fetchArray($getLRData);

if($row['download']=="1")
{
	echo "<script>
		alert('You can\'t Edit this LR. Contact Head-Office.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',true);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['branch']!=$branch)
{
	echo "<script>
		alert('LR belongs to $row[branch] Branch.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['crossing']!="")
{
	echo "<script>
		alert('You Can\'t edit this LR. LR has been Closed.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['cancel']==1)
{
	echo "<script>
		alert('LR Marked as Canceled.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['break']>0)
{
	echo "<script>
		alert('This is Breaking LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['diesel_req']>0)
{
	echo "<script>
		alert('Advance Diesel Requested From This LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',true);
	</script>";
	exit();
}

if($row['lr_type']=='MARKET')
{
	echo "<script>
		$('#div_market_edit').show();
		$('#tno_market_edit').attr('required',true);
		$('#tno_market_edit').val('$row[truck_no]');
		$('#wheeler_market_truck_edit').val('$row[wheeler]');
		
		$('#div_own_edit').hide();
		$('#tno_own_edit').attr('required',false);
		$('#tno_own_edit').val('');
		$('#wheeler_own_truck_edit').val('');
	</script>";
}
else
{
	echo "<script>
		$('#div_market_edit').hide();
		$('#tno_market_edit').attr('required',false);
		$('#tno_market_edit').val('');
		$('#wheeler_market_truck_edit').val('');
		
		$('#div_own_edit').show();
		$('#tno_own_edit').attr('required',true);
		$('#tno_own_edit').val('$row[truck_no]');
		$('#wheeler_own_truck_edit').val('$row[wheeler]');
	</script>";
}

if($row['con1_id']=='56' || $row['con1_id']=='675')
{
	echo "<script>
		$('#dest_zone_div_edit').show();
		$('#dest_zone_edit').attr('required',true);
	</script>";
}
else
{
	echo "<script>
		$('#dest_zone_div_edit').hide();
		$('#dest_zone_edit').attr('required',false);
	</script>";
}

$chk_ewb_item = Qry($conn,"SELECT id FROM _eway_bill_free WHERE item='$row[item_id]'");
if(!$chk_ewb_item){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_ewb_item)>0){
	echo "<script>
		$('#item1_edit').attr('readonly',true);
	</script>";
}
else{
	echo "<script>
		$('#item1_edit').attr('readonly',false);
	</script>";
}

echo "<script>
		$('#lrno_edit').val('$row[lrno]');
		$('#lr_type_edit').val('$row[lr_type]');
		$('#lr_date_edit').val('$row[date]');
		$('#from_edit').val('$row[fstation]');
		$('#from_id_edit').val('$row[from_id]');
		$('#to_edit').val('$row[tstation]');
		$('#to_id_edit').val('$row[to_id]');
		$('#dest_zone_edit').val('$row[dest_zone]');
		$('#dest_zone_id_edit').val('$row[zone_id]');
		$('#consignee_edit').val('$row[consignee]');
		$('#con2_gst_edit').val('$row[con2_gst]');
		$('#consignee_id_edit').val('$row[con2_id]');
		$('#do_no_edit').val('$row[do_no]');
		$('#invno_edit').val('$row[invno]');
		$('#shipno_edit').val('$row[shipno]');
		$('#po_no_edit').val('$row[po_no]');
		$('#act_wt_edit').val('$row[wt12]');
		$('#gross_wt_edit').val('$row[gross_wt]');
		$('#chrg_wt_edit').val('$row[weight]');
		$('#item1_edit').val('$row[item]');
		$('#item_id_edit').val('$row[item_id]');
		$('#articles_edit').val('$row[articles]');
		$('#goods_value_edit').val('$row[goods_value]');
		$('#b_rate_edit').val('$row[bill_rate]');
		$('#b_amt_edit').val('$row[bill_amt]');
		$('#t_type_edit').val('$row[t_type]');
		$('#freight_type_edit').val('$row[freight_type]');
		$('#goods_desc_edit').val('$row[goods_desc]');
		$('#bank_edit').val('$row[bank]');
		$('#sold_to_pay_edit').val('$row[sold_to_pay]');
		$('#lr_name_edit').val('$row[lr_name]');
		$('#con2_addr_edit').val('$row[con2_addr]');
		$('#hsn_code_edit').val('$row[hsn_code]');
		$('#loadicon').hide();
		$('#edit_button_$id').attr('disabled',false);
		$('#lr_update_edit').attr('disabled',false);
		$('#edit_modal_button').click();
	</script>";
	exit();
?>