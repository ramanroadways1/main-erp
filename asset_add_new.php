<form id="AssetAddForm" action="#" method="POST">
<div id="AssetAddnewReq" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add New Asset: ( नए ख़रीदे गए एसेट जोड़ने के लिए उपयोग करे  )
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-4" id="token_no_div">
				<label>Token Number <font color="red"><sup>*</sup></font></label>
				<select class="form-control" id="token_no2" name="token_no" required="required">
					<option value="">--Select option--</option>
					<?php
						$GetTokenPending = Qry($conn,"SELECT t.req_code,t.model,c.title FROM asset_request AS t 
						LEFT OUTER JOIN asset_category AS c ON c.id=t.category 
						WHERE t.asset_added!='1' AND branch='$branch' ORDER BY t.id ASC");
						if(numRows($GetTokenPending)>0)
						{
							while($rowToken = fetchArray($GetTokenPending))
							{
								echo "<option value='$rowToken[req_code]'>$rowToken[title] ($rowToken[model])</option>";
							}
						}
					?>
				</select>	
			</div>
			
			<div class="form-group col-md-8" id="token_button_div">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="tokenButton" class="btn btn-sm btn-danger" onclick="GetTokenInfo()">Get details</button>
			</div>

<script>	
function GetTokenInfo(){
	var token = $('#token_no2').val();
	
	if(token!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./asset_get_request_data.php",
			data: 'token=' + token,
			type: "POST",
			success: function(data) {
				$("#result_asset_add").html(data);
			},
		error: function() {}
		});
	}
	else
	{
		alert('Select Token Number First !');
		$('#main_div').hide();
	}
}
</script>	
	
		<div class="form-group col-md-12" id="main_div" style="display:none">
			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Asset Category <font color="red"><sup>*</sup></font></label>
					<input type="text" id="d_asset_cat" name="asset_category" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Maker Name <font color="red"><sup>*</sup></font></label>
					<input type="text" readonly id="d_maker_name" name="maker_name" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Model <font color="red"><sup>*</sup></font></label>
					<input type="text" readonly id="d_model" name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9/-]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Invoice Date <font color="red"><sup>*</sup></font></label>
					<input name="invoice_date" type="date" class="form-control" max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<div class="form-group col-md-4">
					<label>Invoice Copy <font color="red"><sup>*</sup></font></label>
					<input type="file" accept="image/*,application/pdf" name="invoice_copy" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Payment Mode <font color="red"><sup>*</sup></font></label>
					<input type="text" id="d_payment_mode" readonly class="form-control" required="required">
				</div>
				
				<!--
				<div class="form-group col-md-12" id="paymentDiv" style="display:none">
					<div class="row" id="cheque_div">
						<div class="form-group col-md-6">
							<label>Cheque Number <font color="red"><sup>*</sup></font></label>
							<input type="text" name="cheque_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-6">
							<label>Bank Name <font color="red"><sup>*</sup></font></label>
							<input type="text" name="bank_name" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" class="form-control" required="required">
						</div>
					</div>
					
					<div class="row" id="rtgs_div2">
						<div class="form-group col-md-4">
							<label>A/c Holder <font color="red"><sup>*</sup></font></label>
							<input type="text" readonly class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-4">
							<label>A/c Number <font color="red"><sup>*</sup></font></label>
							<input type="text" readonly class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-4">
							<label>Bank Name <font color="red"><sup>*</sup></font></label>
							<input type="text" readonly class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-4">
							<label>IFSC Code <font color="red"><sup>*</sup></font></label>
							<input type="text" readonly class="form-control" required="required">
						</div>
						
						<div class="form-group col-md-4">
							<label>PAN Number <font color="red"><sup>*</sup></font></label>
							<input type="text" readonly class="form-control" required="required">
						</div>
					</div>
				</div>-->
				
				<div class="form-group col-md-4">
					<label>Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="amount" name="amount" readonly min="1" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-8">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
					<input type="text" id="party_name" readonly class="form-control" required="required">
				</div>
				
				<!--
				<div class="form-group col-md-3">
					<label>GST Invoice ? <font color="red"><sup>*</sup></font></label>
					<select class="form-control" onchange="GstInvoice(this.value)" required="required" id="gst_selection" name="gst_selection">
						<option value="">--Select option--</option>
						<option value="YES">YES</option>
						<option value="NO">NO</option>
					</select>	
				</div>-->
				
				<div class="form-group col-md-3">
					<label>GST Invoice <font color="red"><sup>*</sup></font></label>
					<input type="text" id="gst_selection" name="gst_selection" readonly class="form-control" required="required">
				</div>

<script>		
// function GstInvoice(elem){
	
	// if($('#amount').val()=='' || $('#amount').val()==0)
	// {
		// alert('Please enter amount first !');
		// $('#gst_selection').val('');
		// $('#gst_type').val('');
		// $('#gst_value').val('0');
		// $('#gst_value').attr('readonly',true);
		// $('#gst_amount').val('0');
		// $('#total_amount').val(Math.round($('#amount').val()));
	// }
	// else
	// {
		// $('#amount').attr('readonly',true);
		
		// if(elem=='YES'){
			// $("#loadicon").show();
			// jQuery.ajax({
				// url: "./asset_gst_checked.php",
				// data: 'token=' + $('#token_no2').val() + '&party_id=' + $('#partyId2').val() + '&tab_name=' + 'asset_request',
				// type: "POST",
				// success: function(data) {
					// $("#result_asset_add").html(data);
				// },
			// error: function() {}
			// });
		// }
		// else{
			// $('#gst_type').val('');
			// $('#gst_value').val('0');
			// $('#gst_value').attr('readonly',true);
			// $('#gst_amount').val('0');
			// $('#total_amount').val(Math.round($('#amount').val()));
		// }
	// }
// }
</script>		
		
				<input type="hidden" id="partyId2" name="party_id">
				<input type="hidden" id="d_asset_cat_id" name="d_asset_cat_id">
		
				<div class="form-group col-md-3">
					<label>GST Type <font color="red"><sup>*</sup></font></label>
					<input type="text" id="gst_type" name="gst_type" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>GST Value <font color="red"><sup>* in (%)</sup></font></label>
					<input type="number" name="gst_value" id="gst_value" readonly class="form-control" required="required">
				</div>

<script>			
// function GetGstAmount(gst_per){
	// var amount = Number($('#amount').val());
	// var gst_amount = Number(amount*gst_per/100);
	// $('#gst_amount').val(gst_amount.toFixed(2))
	// $('#total_amount').val(Math.round(Number(amount+gst_amount).toFixed(2)));
// }
</script>			
			
				<div class="form-group col-md-3">
					<label>GST Amount <font color="red"><sup>*</sup></font></label>
					<input step="any" type="number" id="gst_amount" name="gst_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-3">
					<label>Discount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="discount_new_asset" name="discount" readonly class="form-control" required="required">
				</div>
				 
				<div class="form-group col-md-4">
					<label>Total Amount <font color="red"><sup>*</sup></font></label>
					<input type="number" id="total_amount" name="total_amount" min="1" readonly class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Asset For <font color="red"><sup>*</sup></font></label>
					<select name="asset_for" class="form-control" onchange="VehFor(this.value)" required="required">
						<option value="">--option--</option>
						<option value="GENERAL">General (for branch)</option>
						<option value="SPECIFIC">Specific (for person)</option>
					</select>
				</div>
				
				<script>
				function VehFor(elem)
				{
					if(elem=='SPECIFIC'){
						$('#spec_div').show();
						$('#emp_name_1').attr('required',true);
					}
					else{
						$('#spec_div').hide();
						$('#emp_name_1').attr('required',false);
					}
				}
				</script>
				
				<div class="form-group col-md-4" id="spec_div" style="display:none">
					<label>Select Employee <font color="red"><sup>*</sup></font></label>
					<select name="employee" class="form-control" id="emp_name_1" required="required">
						<option value="">--option--</option>
						<?php
						$getEmp = Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3'");
						if(numRows($getEmp)>0)
						{
							while($rowEmp = fetchArray($getEmp))
							{
								echo "<option value='$rowEmp[code]'>$rowEmp[name] ($rowEmp[code])</option>";
							}
						}
						?>
					</select>
				</div>
				
			</div>
		</div>
	
		</div>
      </div>
	  <div id="result_asset_add"></div>
      <div class="modal-footer">
        <button type="submit" disabled id="btn_asset_add" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#AssetAddForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#btn_asset_add").attr("disabled", true);
	$.ajax({
        	url: "./save_asset_add_by_req.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_asset_add").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>