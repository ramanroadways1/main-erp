<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s");

$passwordOld = escapeString($conn,$_POST['password_old']); 
$Newpassword = escapeString($conn,$_POST['password_new']); 
$Newpassword2 = escapeString($conn,$_POST['password_new2']); 

if($Newpassword!=$Newpassword2)
{
	echo "<script>
		alert('Please check new passwords !');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($branch_sub_user==$branch)
{
	$GetCurrentPass = Qry($conn,"SELECT password FROM user WHERE username='$branch'");
}
else
{
	$GetCurrentPass = Qry($conn,"SELECT password FROM emp_attendance where code='$branch_sub_user'");
}

if(!$GetCurrentPass){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","change_password.php");
}

if(numRows($GetCurrentPass)==0)
{
	errorLog("Username not found. BranchName: $branch. EmpCode : $branch_sub_user.",$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","change_password.php");
}

$row = fetchArray($GetCurrentPass);

if($row['password']!=md5($passwordOld))
{
	echo "<script>
		alert('Current Password in invalid !');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($branch_sub_user==$branch)
{
	$UpdateNewpass = Qry($conn,"UPDATE user SET password='".md5($Newpassword)."' WHERE username='$branch'");
}
else
{
	$UpdateNewpass = Qry($conn,"UPDATE emp_attendance SET password='".md5($Newpassword)."' WHERE code='$branch_sub_user'");
}

if(!$UpdateNewpass){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","change_password.php");
}

$UpdateLog = Qry($conn,"INSERT INTO log_login(username,branch_name,action,timestamp) VALUES ('$branch_sub_user','$branch',
'CHANGE_PASSWORD','$timestamp')");
			
if(!$UpdateLog){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing request.","change_password.php");
}	

echo "<script>
		alert('Password Successfully Changed ! Please log in again with your new password.');
		window.location.href='./logout.php';
	</script>";
	exit();		
?>