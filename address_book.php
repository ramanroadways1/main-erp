<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<script>
$(function() {
		$("#location").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#location').val(ui.item.value);   
            $('#location_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#location').val("");   
			$('#location_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">
	<form autocomplete="off" target="_blank" action="excel_download_file.php" method="POST">	
	
<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="color:#FFF">Address - Book</h4></center>
		<br />
	</div>

	<div class="form-group col-md-12">
		<label>Address type <font color="red">*</font></label>
		<select onchange="Option(this.value)" name="addr_type" class="form-control" required="required">
			<option value="">-- Select --</option>
			<option value="Load">Loading point</option>
			<option value="Unload">Unloading point</option>
		</select>
	</div>
	
<script>	
function Option(elem)
{
	if(elem=='Unload')
	{
		$('#loc_type').html('Unloading');
		$('#loc_type2').html('Unloading');
	}
	else
	{
		$('#loc_type').html('Loading');
		$('#loc_type2').html('Loading');
	}
}
</script>	
	
	<div class="form-group col-md-12">
		<label><span id="loc_type">Loading</span> Location <font color="red">*</font></label>
		<input name="location" id="location" oninput="this.value=this.value.replace(/[^a-z A-Z.]/,'');" type="text" class="form-control" required />
    </div>
	
	<input type="hidden" name="location_id" id="location_id">
	
	<div class="form-group col-md-12">
		<label><span id="loc_type2">Loading</span> Point name <font color="red">*</font></label>
		<input name="point_name" id="point_name" oninput="this.value=this.value.replace(/[^a-z ,-A-Z0-9./]/,'');" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" disabled type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="Save Address" />
	</div>
	
	</div>
	
</div>

</div>
</div>
</form>
</body>
</html>