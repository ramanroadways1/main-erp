<script>
$(function() {
		$("#to_crossing_loc").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_crossing_loc').val(ui.item.value);   
            $('#cross_loc_to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_crossing_loc').val("");   
			$('#cross_loc_to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<form id="CrossingLRForm" action="#" method="POST">
<div id="CrossingLRModal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		ADD Crossing LR
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="form-group col-md-7">
				<label>Enter LR No.<font color="red"><sup>*</sup></font></label>
				<input type="text" id="lrno_crossing_1" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			<div class="form-group col-md-5" id="crossing_fetch_btn">
				<label>&nbsp;</label>
				<br>
				<button type="button" onclick="GetCrossingInfo()" class="btn btn-danger">Check !</button>
			</div>
			
			<div class="form-group col-md-6">
				<label>From Station <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="from_crossing_loc" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="from" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>To Station <font color="red"><sup>*</sup></font></label>
				<input type="text" readonly id="to_crossing_loc" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="to" class="form-control" required="required">
			</div>
			 
			<div class="form-group col-md-6">
				<label>Actual Weight <font color="red"><sup>*</sup></font></label>
				<input type="number" min="0.01" step="any" readonly id="actual_weight_crossing" name="actual_weight" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Charge Weight <font color="red"><sup>*</sup></font></label>
				<input type="number" step="any" min="0.01" readonly id="charge_weight_crossing" name="charge_weight" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>E-way Bill Number <font color="red"><sup>*</sup></font></label>
				<input type="text" maxlength="12" id="ewb_no_ip" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="ewb_no" class="form-control" required="required">
			</div> 
			
			<input type="hidden" name="crossing_modal_ewb_desc" id="crossing_modal_ewb_desc">
			<input type="hidden" name="company" id="crossing_modal_company_name">
			<input type="hidden" name="from_id" id="cross_loc_from_id">
			<input type="hidden" name="to_id" id="cross_loc_to_id">
			<input type="hidden" name="salt_lr" id="salt_lr">
			<input type="hidden" name="con1_id" id="cross_modal_con1_id">
			<input type="hidden" name="item_id" id="cross_modal_item_id">
			<input type="hidden" name="page_name1" value="<?php echo basename($_SERVER['PHP_SELF']); ?>">
			
		</div>
      </div>
	  <div id="result_CrossingLRForm"></div>
      <div class="modal-footer">
        <button type="submit" id="crossing_lr_add_btn" disabled class="btn btn-primary">ADD LR</button>
        <button type="button" class="btn btn-default" onclick="ResetModalCrossing()" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#CrossingLRForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#crossing_lr_add_btn").attr("disabled", true);
		$.ajax({
        	url: "./add_crossed_lr_to_fm.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_CrossingLRForm").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function ResetModalCrossing()
{
	document.getElementById("CrossingLRForm").reset();
	$('#crossing_lr_add_btn').attr('disabled',true);
	$('#lrno_crossing_1').attr('readonly',false);
	$('#crossing_fetch_btn').show();
}
</script>