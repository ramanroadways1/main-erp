<?php
require_once 'connection.php';

$branch=escapeString($conn,strtoupper($_SESSION['user']));
$date = date("Y-m-d"); 
$timestamp=date("Y-m-d H:i:s");

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif", "image/png" ,"application/pdf");
	
	if(!in_array($_FILES['rc_copy_f']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed : RC FRONT SIDE');
			$('#loadicon').hide();
			$('#button_oc_change').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(!in_array($_FILES['rc_copy_r']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed : RC REAR SIDE');
			$('#loadicon').hide();
			$('#button_oc_change').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(!in_array($_FILES['pan_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed : PAN COPY');
			$('#loadicon').hide();
			$('#button_oc_change').attr('disabled',false);
		</script>";
		exit();
	}
	
$tno=escapeString($conn,strtoupper($_POST['tno'])); 
$o_id=escapeString($conn,strtoupper($_POST['owner_id'])); 

$sel_ac_details=Qry($conn,"SELECT tno,acname,accno,bank,ifsc FROM mk_truck WHERE id='$o_id'");
if(!$sel_ac_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($sel_ac_details)==0)
{
	echo "
	<script type='text/javascript'> 
		alert('Vehicle not found');
		window.location.href='./all_functions.php';
	</script>";
	exit();
}

$row1=fetchArray($sel_ac_details);

if($row1['tno']!=$tno)
{
	echo "
	<script type='text/javascript'> 
		alert('Vehicle not found');
		window.location.href='./all_functions.php';
	</script>";
	exit();
}

if($row1['accno']!='')
{
	$ac_details="AcHolder : ".$row1['acname'].", AcNo : ".$row1['accno'].", Bank : ".$row1['bank'].", IFSC : ".$row1['ifsc'];
}
else
{
	$ac_details="NA";
}

// old
$owner_current=escapeString($conn,strtoupper($_POST['old_name']));
$mobile_current=escapeString($conn,strtoupper($_POST['old_mobile']));
$pan_no_current=escapeString($conn,strtoupper($_POST['old_pan_no']));

$rc_front_current=escapeString($conn,($_POST['old_rc']));
$rc_rear_current=escapeString($conn,($_POST['old_rc_rear']));
$pan_current=escapeString($conn,($_POST['old_pan']));

//new
$name=escapeString($conn,strtoupper($_POST['name']));
$mobile=escapeString($conn,strtoupper($_POST['mobile']));
$pan=escapeString($conn,strtoupper($_POST['pan_no']));
$addr=escapeString($conn,strtoupper($_POST['addr']));

if(strlen($mobile)!=10)
{
	 echo "<script>
			alert('Invalid Mobile Number');
			$('#loadicon').hide();
			$('#button_oc_change').attr('disabled',false);
		</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan))
{
	echo "<script>
		alert('Invalid PAN No.');
		$('#loadicon').hide();
		$('#button_oc_change').attr('disabled',false);
	</script>";
	exit();
}

$sourcePath_rc = $_FILES['rc_copy_f']['tmp_name']; 
$sourcePath_rc_rear = $_FILES['rc_copy_r']['tmp_name']; 
$sourcePath_pan = $_FILES['pan_copy']['tmp_name']; 

$targetPath_Rc_front="truck_rc/Front_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_copy_f']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_rear="truck_rc/Rear_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_copy_r']['name'],PATHINFO_EXTENSION);
$targetPath_Pan="truck_pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);

ImageUpload(1000,1000,$sourcePath_rc);

if(!move_uploaded_file($sourcePath_rc,$targetPath_Rc_front)){
	Redirect("Unable to Upload PAN Card Copy.","./all_functions.php");
	exit();
}

ImageUpload(1000,1000,$sourcePath_rc_rear);

if(!move_uploaded_file($sourcePath_rc_rear,$targetPath_Rc_rear)){
	unlink($targetPath_Rc_front);
	Redirect("Unable to Upload RC Front Copy.","./all_functions.php");
	exit();
}

ImageUpload(1000,1000,$sourcePath_pan);

if(!move_uploaded_file($sourcePath_pan,$targetPath_Pan)){
	unlink($targetPath_Rc_front);
	unlink($targetPath_Rc_rear);
	Redirect("Unable to Upload RC Rear Copy.","./all_functions.php");
	exit();
}
	
$insert=Qry($conn,"INSERT INTO owner_change_req(tno,wheeler,oid,old_name,old_mobile,old_pan,old_addr,rc_copy_old,rc_copy_rear_old,
pan_copy_old,rc_copy_new,rc_copy_rear_new,pan_copy_new,ac_details,new_name,new_mobile,new_pan,new_addr,branch,date,timestamp) 
VALUES ('$tno','','$o_id','$owner_current','$mobile_current','$pan_no_current','','$rc_front_current','$rc_rear_current',
'$pan_current','$targetPath_Rc_front','$targetPath_Rc_rear','$targetPath_Pan','$ac_details','$name','$mobile','$pan','$addr',
'$branch','$date','$timestamp')");

	echo "
	<script type='text/javascript'> 
		alert('Request Submitted Successfully..');
		window.location.href='./all_functions.php';
	</script>";
	exit();
	
?>	