<?php
require_once 'connection.php';

if(!isset($_POST['submit'])){
	echo "<script>window.location.href='./neft_details.php';</script>";
	exit();
}

$type1 =  escapeString($conn,($_POST['type1']));

if($type1=='BROKER')
{
	$id_search =  escapeString($conn,strtoupper($_POST['broker_id']));
	$value1 =  escapeString($conn,strtoupper($_POST['broker_name']));
	
	$get_pan = Qry($conn,"SELECT pan FROM mk_broker WHERE id='$id_search'");
}
else
{
	$id_search =  escapeString($conn,strtoupper($_POST['owner_id']));
	$value1 =  escapeString($conn,strtoupper($_POST['truck_no']));
	
	$get_pan = Qry($conn,"SELECT pan FROM mk_truck WHERE id='$id_search'");
}

$row_pan = fetchArray($get_pan);
$from_date =  escapeString($conn,strtoupper($_POST['from_date']));
$to_date =  escapeString($conn,strtoupper($_POST['to_date']));
$pan_no = $row_pan['pan'];

// echo $pan_no;
?>
<html>
<head>
<title>Rtgs/Neft</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
</style>


<?php
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
		<div style="background-color:;padding:5px;" class="bg-primary form-group col-md-12">
			<a href="./passbook.php"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
			<center><h5 style="margin-right:100px;">Rtgs/Neft Summary : <?php echo $value1; ?></h5></center>
		</div>
		
		<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Vou_No</th>
					<th>Vou_Date</th>
					<th>Company</th>
					<th>Truck_No</th>
					<th>Amount</th>
					<th>Ac_Holder</th>
					<th>Ac_No.</th>
					<th>IFSC</th>
					<th>PAN</th>
					<th>Adv/Bal</th>
					<th>CRN</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function FetchPassbook(type,pan_no,from_date,to_date) {
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
// 		"sAjaxSource": "master_expense_data.php?company='.$company.'&from_date='.$from_date.'&to_date='.$to_date.'",
// 		"bPaginate": true,
		"sPaginationType":"full_numbers",
// 		"iDisplayLength": 15,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "excel", "print", "colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
{ 
    // "targets": [0,1,2,3,4], //Comma separated values
    // "visible": true,
    // "searchable": true 
}
		], 
        "serverSide": true,
        "ajax": "_fetch_neft_details_server.php?type="+type+"&from_date="+from_date+"&to_date="+to_date+"&pan_no="+pan_no,
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchPassbook('<?php echo $type1; ?>','<?php echo $pan_no; ?>','<?php echo $from_date; ?>','<?php echo $to_date; ?>');
</script>