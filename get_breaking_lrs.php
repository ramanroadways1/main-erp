<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$get_tukdas = Qry($conn,"SELECT id,act_wt,chrg_wt FROM lr_break WHERE lrno='$lrno' AND crossing='' AND branch='$branch'");

if(!$get_tukdas){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_tukdas)==0)
{
	$lrs = "<option value=''>--Select LR--</option>";
}
else
{
	$lrs = "<option value=''>--Select LR--</option>";
	
$get_locations = Qry($conn,"SELECT fstation,tstation,from_id,to_id,date(timestamp) as date_created FROM lr_sample WHERE lrno='$lrno'");
if(!$get_locations){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_locations)==0)
{
	echo "<script>
		alert('LR not found !');
		window.location.href='./';
	</script>";
	exit();
}
	
$row_loc = fetchArray($get_locations);	

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_loc['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

if($diff_days>60)
{
	$lrs = "<option style='background-color:yellow' value=''>--LR exceed 60 days--</option>";
}
else
{
	while($row = fetchArray($get_tukdas))
	{
		$lrs .="<option style='text-transform:lowercase' value='$row[id]_$row[act_wt]_$row[chrg_wt]'>$lrno => Act Wt: $row[act_wt], Chrg Wt: $row[chrg_wt]</option>";
	}
	echo "<script>
		$('#from_breaking_lr').val('$row_loc[fstation]');
		$('#to_breaking_lr').val('$row_loc[tstation]');
		$('#from_breaking_lr_id').val('$row_loc[from_id]');
		$('#to_breaking_lr_id').val('$row_loc[to_id]');
		$('#break_lr_add_btn').attr('disabled',false);
		$('#lrno_breaking_1').attr('readonly',true);
		$('#break_chk_button').hide();
		$('#from_breaking_lr').attr('readonly',false);
		// $('#to_breaking_lr').attr('readonly',false);
	</script>";
}
}

echo $lrs;
HideLoadicon();
?>