<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$dest_id = escapeString($conn,strtoupper($_POST['dest_id']));
$vou_no = escapeString($conn,$_SESSION['voucher_olr']);
  
$chk_dest = Qry($conn,"SELECT tstation,tstation_bak,to_id,to_id_bak,dest_zone,dest_zone_id FROM lr_pre WHERE id='$id'");
 
if(!$chk_dest){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_dest)==0)
{
	echo "<script>alert('Record not found !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row = fetchArray($chk_dest);

if($dest_id != $row['to_id'])
{
	if($dest_id == $row['dest_zone_id'])
	{
		$loc_name = $row['dest_zone'];
	}
	else
	{
		$loc_name = $row['tstation_bak'];
	}
	
	$update_loc = Qry($conn,"UPDATE lr_pre SET tstation='$loc_name',to_id='$dest_id' WHERE id='$id'");
	
	if(!$update_loc){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	if(AffectedRows($conn)==0)
	{
		echo "<script>alert('No Change !');$('#loadicon').fadeOut('slow');</script>";
		exit();
	}

	echo "<script>LoadLrs('$vou_no');</script>";
}
else
{
	echo "<script>alert('No Change !');$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>