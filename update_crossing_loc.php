<?php
require_once("./connection.php");

$cross_loc = escapeString($conn,strtoupper($_POST['cross_loc']));
$loc_id = escapeString($conn,strtoupper($_POST['cross_loc_id']));
$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$id = escapeString($conn,strtoupper($_POST['table_id']));
$page_name1 = escapeString($conn,($_POST['page_name1']));

$update = Qry($conn,"UPDATE lr_pre SET tstation='$cross_loc',to_id='$loc_id',crossing='YES',cross_to='$cross_loc',cross_stn_id='$loc_id' 
WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	alert('Crossing Station Updated to : $cross_loc. LR Number : $lrno');
	window.location.href='./$page_name1';
</script>";
exit();
?>