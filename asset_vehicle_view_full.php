<?php
require_once 'connection.php';

if(!isset($_REQUEST['id'])){
	echo "<script>
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}

$id = escapeString($conn,strtoupper($_REQUEST['id']));

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$fetch_data = Qry($conn,"SELECT v.vehicle_holder,v.token_no,v.reg_no,v.branch,v.reg_date,v.owner_name,v.veh_class,v.chasis_no,v.engine_no,
v.fuel_type,v.maker,d.rc_front,d.rc_rear,d.ins_copy,d.ins_start,d.ins_end,d.puc_copy,d.puc_start,d.puc_end FROM asset_vehicle AS v 
LEFT OUTER JOIN asset_vehicle_docs AS d ON d.veh_id=v.id 
WHERE v.id='$id'");

if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($fetch_data)==0)
{
	echo "<script>
		alert('Vehicle not found !');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}
	
$row = fetchArray($fetch_data);	
	
if($row['branch']!=$branch)
{
	echo "<script>
		alert('Vehicle does not belongs to your branch !');
		window.location.href='./asset_vehicle_view.php';
	</script>";
	exit();
}	

if($row['reg_no']==""){
	$veh_no = $row['token_no'];
}
else{
	$veh_no = $row['reg_no'];
}
?>

<style type="text/css">
input{
  text-transform: uppercase;
}
</style> 

<html>

<?php 
	include("./_header2.php");
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<a href="./asset_vehicle_view.php"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary">
<span class="glyphicon glyphicon-chevron-left"></span> Go Back </button></a>
<a href="./"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp; Dashboard</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<br />
<br />
<div class="container"> 

<div class="form-group col-md-12">
<br />	
	<div class="row">	
		<div class="form-group col-md-12">
			<h4><span class="fa fa-truck"></span> &nbsp; Vehicle Details : <font color="blue"><?php echo $veh_no; ?></font> </h4> 
		<br />
		</div>
		
		<div class="form-group col-md-12">
			<table class="table table-striped" border="1" style="border:0;font-size:14px;">
				<tr>
					<td>Reg. No: &nbsp; <?php echo $veh_no; ?></td>
					<td>Vehicle Holder: &nbsp; <?php echo $row['vehicle_holder']; ?></td>
					<td>Owner Name: <?php echo $row['owner_name']; ?></td>
				</tr>
				
				<tr>
					<td>Reg. Date: &nbsp; <?php echo $row['reg_date']; ?></td>
					<td>Veh. Class: &nbsp; <?php echo $row['veh_class']; ?></td>
					<td>Fule Type: &nbsp; <?php echo $row['fuel_type']; ?></td>
				</tr>	

				<tr>
					<td>Maker: &nbsp; <?php echo $row['maker']; ?></td>
					<td>Chasis No: &nbsp; <?php echo $row['chasis_no']; ?></td>
					<td>Engine No: &nbsp; <?php echo $row['engine_no']; ?></td>
				</tr>	
				
				<tr>
					<td>RC Copy: &nbsp; <a data-toggle="modal" href="#View_RcFront"> Front Copy </a>, <a data-toggle="modal" href="#View_RcRear"> Rear Copy </a></td>
					<td>Insurance Copy: &nbsp; <a data-toggle="modal" href="#View_Insurance"> VIEW </a></td>
					<td>PUC Copy: &nbsp; <a data-toggle="modal" href="#View_Puc"> VIEW </a></td>
				</tr>

			<!--
				<tr>
					<td colspan="2">Residential Address: &nbsp; <?php// echo $row['residenceaddr']; ?></td>
					<td colspan="2">Current Address: &nbsp; <?php //echo $row['currentaddr']; ?></td>
				</tr>					
			-->	
			</table>
		</div>
		
	</div>
</div>
</div>

<div class="modal fade" id="View_RcFront" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h4 class="modal-title">Uploaded RC Copy (front) </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="400px" width="100%" src="<?php echo $row['rc_front']; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>

<div class="modal fade" id="View_RcRear" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h4 class="modal-title">Uploaded RC Copy (rear) </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="400px" width="100%" src="<?php echo $row['rc_rear']; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>

<div class="modal fade" id="View_Insurance" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h4 class="modal-title">Uploaded Insurance Copy </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="400px" width="100%" src="<?php echo $row['ins_copy']; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>

<div class="modal fade" id="View_Puc" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h4 class="modal-title">Uploaded PUC Copy </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="400px" width="100%" src="<?php echo $row['puc_copy']; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>