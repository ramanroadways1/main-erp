<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$lrno = escapeString($conn,$_POST['lrno']);
$id = escapeString($conn,$_POST['id']);

echo "<script>$('#voucher_btn_close_$id').attr('disabled',true);$('#voucher_btn_$id').attr('disabled',true);</script>";

$check_record = Qry($conn,"SELECT o.frno,o.vou_status,o.lrno,o.trip_id,o.from_id,l.tstation,l.to_id 
FROM oxygen_olr AS o 
LEFT JOIN lr_sample AS l ON l.id = o.lr_id 
WHERE o.id='$id'");

if(!$check_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_record)==0)
{
	echo "<script>
		alert('No record found !');
		$('#loadicon').fadeOut('slow');
		$('#voucher_btn_close_$id').attr('disabled',false);
	</script>";
	exit();
}

$row = fetchArray($check_record);

$vou_no = $row['frno'];
$from_id = $row['from_id'];
$to_id = $row['to_id'];
$to_loc = $row['tstation'];

if($row['lrno']!=$lrno)
{
	echo "<script>
		alert('Error : LR number not verified !');
		$('#loadicon').fadeOut('slow');
		$('#voucher_btn_close_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['vou_status']=='1')
{
	echo "<script>
		alert('Error : Voucher is not active !');
		$('#loadicon').fadeOut('slow');
		$('#voucher_btn_close_$id').attr('disabled',false);
	</script>";
	exit();
}

$get_last_record = Qry($conn,"SELECT count FROM oxygen_olr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");

if(!$get_last_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row2 = fetchArray($get_last_record);

if($row2['count']=='3')
{
	echo "<script>
		alert('Error : LR closed already !');
		$('#loadicon').fadeOut('slow');
		// $('#voucher_btn_close_$id').attr('disabled',false);
	</script>";
	exit();
}
	
StartCommit($conn);
$flag = true;

$mark_by_road = Qry($conn,"UPDATE oxygen_olr SET count='3',running_trip='1' WHERE id='$id'");

if(!$mark_by_road){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$close_lr = Qry($conn,"UPDATE oxygen_olr SET close_user='$_SESSION[user_code]',close_timestamp='$timestamp',vou_status='1' WHERE lrno='$lrno'");
	
if(!$close_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('OK : Success.');
		window.location.href='./oxygen_movement.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#voucher_btn_close_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>