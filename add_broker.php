<?php
require_once './connection.php'; 

$date=date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$mobile2 = escapeString($conn,strtoupper($_POST['mobile2']));
$pan_no_hidden = escapeString($conn,strtoupper($_POST['pan_no_hidden']));
$pan_type = escapeString($conn,($_POST['pan_type']));

$pan_holder = $_SESSION['add_broker_pan_holder'];
$pan_holder_lngth = strlen($pan_holder);

$pan_no = $_SESSION['add_broker_pan_no'];

$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));

if($pan_no != $pan_no_hidden)
{
	echo "<script>
		alert('PAN number not verified !');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
	
if(strpos($name,'RECT') !== false){
   echo "<script>
		alert('Invalid Broker Name !');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strpos($mobile2,'0')===0 AND $mobile2!='')
{
	echo "<script>
		alert('Error: Check mobile number-2.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($pan_type=='Normal')
{
	if($pan_holder != substr($name,0,$pan_holder_lngth))
	{
		echo "<script>
			alert('Broker name must contains PAN holder name !');
			$('#add_broker_button').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}

$chk_broker = Qry($conn,"SELECT id FROM mk_broker WHERE name='$name'");
	
if(!$chk_broker)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'> 
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#add_broker_button').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_broker) > 0)
{
	echo "<script type='text/javascript'> 
		alert('Duplicate broker found : $name !');
		$('#loadicon').fadeOut('slow');
		$('#add_broker_button').attr('disabled',false);
	</script>";
	exit();
}

$temp_path = $_SESSION['add_broker_filepath'];
$new_path = "broker_pan/$temp_path";

if(!copy("broker_pan_temp/".$temp_path,$new_path)){
	
    echo "<script>
		alert('Error while uploading document !');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO mk_broker(name,mo1,mo2,pan,full,up5,branch,branch_user,timestamp) VALUES ('$name','$mobile','$mobile2','$pan_no',
'$addr','$new_path','$branch','$branch_sub_user','$timestamp')");
	
if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	unlink($new_path);
	
	echo "<script type='text/javascript'> 
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#add_broker_button').attr('disabled',false);
	</script>";
	exit();
}

unset($_SESSION['add_broker_filepath']);
unset($_SESSION['add_broker_pan_no']);
unset($_SESSION['add_broker_pan_holder']);
	
	echo "<script type='text/javascript'> 
		alert('Broker : $name. Added Successfully.');
		window.location.href='./all_functions.php';
	</script>";
	exit();

?>