<?php
require_once("./connection.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$branch = escapeString($conn,strtoupper($_SESSION['user']));

$chk_diesel_Req = Qry($conn,"SELECT id FROM diesel_lr_pending WHERE tno='$tno' AND branch='$branch'");

if(!$chk_diesel_Req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_diesel_Req)>0)
{
	echo "<script>
			alert('Error: LR Number Pending Against Diesel Recharge of Vehicle : $tno. Please clear it first !');
			window.location.href='./smemo.php';	
		</script>";
	exit();
}

if(!isset($_SESSION['freight_memo']))
{
	$_SESSION['freight_memo'] = $tno."_".date("YmdHis").mt_rand();
	$_SESSION['freight_memo_vehicle'] = $tno;
	$vou_no = $_SESSION['freight_memo'];
}
else
{
	$vou_no = $_SESSION['freight_memo'];
}	
	
$getLrs = Qry($conn,"INSERT INTO lr_pre(d_id,frno,company,branch,date,create_date,truck_no,lrno,lr_type,lr_id,fstation,tstation,tstation_bak,
consignor,consignee,from_id,to_id,to_id_bak,con1_id,con2_id,dest_zone,dest_zone_id,wt12,weight,crossing,timestamp) SELECT dl_id,'$vou_no',company,
branch,date,'$date','$tno',lrno,'0',lr_id,fstation,tstation,tstation,consignor,consignee,from_id,to_id,to_id,con1_id,con2_id,dest_zone,zone_id,
wt12,weight,'NO','$timestamp' FROM lr_sample_pending WHERE date(timestamp)>'2021-01-22' AND truck_no='$tno' AND branch='$branch' 
AND lr_id NOT IN(SELECT lr_id FROM lr_pre WHERE frno='$vou_no')");

if(!$getLrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(AffectedRows($conn)==0){
	// echo "<script>alert('No LR Created. Vehicle Number : $tno.');</script>";
}
else{
	
	// echo "<script>alert('ALL Pending LRs of Vehicle Number : $tno. Successfully Added to Freight Memo.');</script>";
}

echo "<script>LoadLrs('$vou_no');</script>";
HideLoadicon();
?>