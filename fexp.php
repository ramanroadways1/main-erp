<?php
require_once 'connection.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 
$min2="-2 days";

$min = date("Y-m-d", strtotime("$min2"));

// if(!isset($_SESSION['_exp_voucher']))
// {
	// $_SESSION['_exp_voucher'] = $branch.$timestamp.mt_rand();
// }

// $exp_vou_id = $_SESSION['_exp_voucher'];

// $get_cache = Qry($conn,"SELECT * FROM exp_vou_cache WHERE vou_id='$exp_vou_id'");

// if(!$get_cache){
	// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// Redirect("Error while processing Request","./");
	// exit();
// }
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
<link rel="icon" type="image/png" href="<?php echo $baseUrl1 ?>favicon.png" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="./google_font.css" rel="stylesheet">

<script type="text/javascript">
	$(function() {
		$("#pan_no").autocomplete({
		source: './autofill/exp_ac.php',
		select: function (event, ui) { 
               $('#pan_no').val(ui.item.value);   
               $('#ac_holder').val(ui.item.name);     
               $('#ac_no').val(ui.item.acno);     
               $('#ac_bank').val(ui.item.bank);     
               $('#ac_ifsc').val(ui.item.ifsc);  
               $('#ac_id').val(ui.item.id);  
			   $("#neft_div :input").attr('readonly',true);
				$('#pan_no').attr('readonly',false);			   
            return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    alert('New Account Found.');
			$('#ac_holder').val("");     
            $('#ac_no').val("");     
            $('#ac_bank').val("");     
            $('#ac_ifsc').val(""); 
            $('#ac_id').val("0"); 
			$("#neft_div :input").attr('readonly',false);
			$('#pan_no').attr('readonly',false);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<script type="text/javascript">
        function convert_number(number) {
            if ((number < 0) || (number > 999999999)) {
                return "Number is out of range";
            }
            var Gn = Math.floor(number / 10000000); /* Crore */
            number -= Gn * 10000000;
            var kn = Math.floor(number / 100000); /* lakhs */
            number -= kn * 100000;
            var Hn = Math.floor(number / 1000); /* thousand */
            number -= Hn * 1000;
            var Dn = Math.floor(number / 100); /* Tens (deca) */
            number = number % 100; /* Ones */
            var tn = Math.floor(number / 10);
            var one = Math.floor(number % 10);
            var res = "";

            if (Gn > 0) {
                res += (convert_number(Gn) + " Crore");
            }
            if (kn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " Lakhs");
            }
            if (Hn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " Thousand");
            }

            if (Dn) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " Hundred");
            }


            var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

            if (tn > 0 || one > 0) {
                if (!(res == "")) {
                    res += " and ";
                }
                if (tn < 2) {
                    res += ones[tn * 10 + one];
                } else {

                    res += tens[tn];
                    if (one > 0) {
                        res += ("-" + ones[one]);
                    }
                }
            }

            if (res == "") {
                res = "zero";
            }
            return res;
        }

        function toWords() {

            document.getElementById("amountw").value = convert_number(document.getElementById("amount").value);

        }
    </script>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}

.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
 </style> 
</head>

<a href="../"><button style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<form action="./fexp2.php" method="post" autocomplete="off">    

<div class="container-fluid">
		<div class="col-md-8 col-md-offset-2">
		
<div class="row">
	
	<div class="form-group col-md-12">
		<center><h4>Expense Voucher</h4></center>
		<br />
	</div>	
	
	<div class="form-group col-md-4">
		<label>Vou Date <font color="red">*</font></label>
        <input value="" name="vou_date" type="date" class="form-control" min="<?php echo $min; ?>" max="<?php echo $date; ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Company <font color="red">*</font></label>
		<select style="border:1px solid #000" class="form-control" name="company" required="required">
			<option value="">Select Company</option>
            <option value="RRPL">RRPL</option>
            <option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div class="form-group col-md-4">
		<label>Expense Heading <font color="red">*</font></label>
			<select id="exp_name" name="exp" onchange="copytext(this.value); " class="form-control" required="required">
			<option value="">Select Expense</option>
<?php 
 //show all to cgroad otherwise hide(1)		
if($branch=='CGROAD')
{ 
	$exp_list = Qry($conn,"SELECT * FROM expenses ORDER BY exp ASC");
}
else
{ 
	$exp_list = Qry($conn,"SELECT * FROM expenses where hide='0' ORDER BY exp ASC");
} 

if(!$exp_list){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	
 
 
while($fetch = fetchArray($exp_list))
{
	//show to all esle specific branch
	if($fetch['branch']=="ALL" || $fetch['branch']==$branch)  
	{
		echo "<option value='$fetch[id]_$fetch[cash_limit]_$fetch[comm]'>$fetch[exp]</option>";
	}
}
?>
	</select>
</div>
	
	<div class="form-group col-md-4" id="employee" style="display:none;">
		<label>Employee/Person <font color="red">*</font></label>
			<select id="empcode" name="empcode" class="form-control" required="required">
			<option value="">-- Select Employee --</option>
			<option value="KKG">KISHORE GANDHI</option>
			<option value="RKG">RISHAB GANDHI</option>
			<option value="JKG">JAYANT GANDHI</option>
			<option value="SKG">SANKET GANDHI</option>
			<option value="MKJAIN">MAHENDRA JAIN</option>
			<option value="TEJAS_SHAH">TEJAS SHAH</option>
			<?php 
			$emp_list = Qry($conn,"SELECT code,name FROM emp_attendance where branch='$branch' AND status='3' ORDER BY code ASC ");
			if(!$emp_list){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			while($fetchemp = fetchArray($emp_list))
			{
				echo "<option value='$fetchemp[code]'>$fetchemp[name] ($fetchemp[code])</option>";
			}
			?>
		</select>
	</div>

	<div class="form-group col-md-4" id="vehicle" style="display:none;">
		<label>Vehicle Number <font color="red">*</font></label>
			<select id="vehno" name="vehno" class="form-control" required="required">
			<option value="">-- Select Vehicle --</option>
			<?php 
			$veh_list = Qry($conn,"SELECT reg_no,owner_name FROM asset_vehicle where branch='$branch' AND active='1' ORDER BY 
			`reg_no` ASC ");
			
			if(!$veh_list){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
				while($fetchveh = fetchArray($veh_list))
				{
					echo "<option value='$fetchveh[reg_no]'>$fetchveh[reg_no] &nbsp; ($fetchveh[owner_name])</option>";
				}
				
			$veh_listReq = Qry($conn,"SELECT req_code,veh_type,model_name FROM asset_vehicle_req where branch='$branch' AND 
			ho_approval='1' AND asset_added=0 ORDER BY id ASC ");
			
			if(!$veh_listReq){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
				while($fetchvehReq = fetchArray($veh_listReq))
				{
					echo "<option value='$fetchvehReq[req_code]'>$fetchvehReq[req_code] &nbsp; ($fetchvehReq[model_name])</option>";
				}	
			?>
		</select>
	</div>

	<div class="form-group col-md-4">
		<label>Payment Mode <font color="red">*</font></label>
			<select class="form-control" id="payment_mode" onchange="payBy(this.value);if($('#exp_name').val()==''){$('#exp_name').focus();$(this).val('');payBy('');}" name="payment_mode" required="required">
			<option value="">Mode of payment</option>
			<option value="CASH">CASH</option>
			<option value="CHEQUE">CHEQUE</option>
			<option value="NEFT">RTGS/NEFT</option>
		</select>
	</div>
	
	<div class="form-group col-md-4">
		<label>Amount <font color="red">*</font></label>
		<input class="form-control" min="10" id="amount" type="number" oninput="toWords();CheckCash(this.value);" name="amount" required />
	</div>
	
	<script>
	function CheckCash(cash)
	{
		var mode = $('#payment_mode').val();
		
		if(mode=='')
		{
			$('#payment_mode').focus();
			$('#amount').val('');
			$('#amountw').val('');
		}
		else
		{
			var cash_limit = $('#cash_limit').val();
			
			if(mode=='CASH' && cash_limit==0 && cash>10000)
			{
				alert('Max Cash Allowed is 10,000 !');
				$('#amount').val('');
				$('#amountw').val('');				
			}
		}
	}
	</script>
	
	<div class="form-group col-md-4">
		<label>Amount (in words) <font color="red">*</font></label>
		<input class="form-control" type="text" name="amount_word" id="amountw" required readonly />
	</div>
	
	<div class="form-group col-md-8" id="chq_div" style="display:none">
		<div class="row">
			<div class="form-group col-md-6">
				<label>Cheque No. <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9]/,'')" class="form-control" type="text" name="cheque_no" />
			</div>
			
			<div class="form-group col-md-6">
				<label>Bank Name <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" class="form-control" name="bank" />
			</div>
		</div>
	</div>
	
	<div class="form-group col-md-12" id="neft_div" style="display:none;">
		<div class="row">
			<div class="form-group col-md-4">
				<label>PAN No. <font color="red">*</font> </label>
				<input pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN()" 
				type="text" id="pan_no" onblur="ValidatePAN();" class="form-control" minlength="10" maxlength="10" name="pan" />
			</div>
		
			<div class="form-group col-md-4">
				<label>A/c Holder <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="ac_holder" class="form-control" type="text" 
				name="acname" readonly />
			</div>
		
			<div class="form-group col-md-4">
				<label>A/c No <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z-0-9]/,'')" id="ac_no" type="text" minlength="6" 
				class="form-control" readonly name="acno" />
			</div>
	
			<div class="form-group col-md-4">
				<label>Bank Name <font color="red">*</font> </label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" id="ac_bank" type="text" 
				class="form-control" minlength="3" readonly name="bank_name" />
			</div>
	
			<input type="hidden" name="ac_id" id="ac_id" />
			
			<div class="form-group col-md-4">
				<label>IFSC Code <font color="red">*</font> </label>
				<input pattern="[a-zA-Z]{4}[0]{1}[a-zA-Z0-9]{6}" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC()" type="text" 
				id="ac_ifsc" class="form-control" minlength="11" readonly maxlength="11" name="ifsc" />
			</div>
		</div>	
	</div>
	
	<div class="form-group col-md-6">
		<label>Expense Description <font color="red">*</font></label>
		<textarea readonly class="form-control" id="comments"></textarea>
	</div>
	
   <div class="form-group col-md-6">
		<label>Narration <font color="red">*</font></label>
		<textarea oninput="this.value=this.value.replace(/[^a-z,. A-Z-0-9]/,'')" type="text" class="form-control" name="narration" required="required"></textarea>
	</div>
	
	<div class="form-group col-md-6">
		<label>Cashier Signature <font color="red"><sup>*</sup></font></label>
		<div id="newsign">
			<div class="m-signature-pad--body">
				<canvas class="signature" style="width:100%;height:180px;max-width:100%;border:1.5px #CCC solid;background-color:white;"></canvas>
			</div>
			<div class="m-signature-pad--footer" style="padding-top:10px;">
				<button type="button" class="btn btn-primary" data-action="clear">Clear</button>
				<button type="button" class="btn btn-primary" data-action="save">Save</button>
			</div>
		</div>
	</div>

	<div class="form-group col-md-6">
		<label> Signature Preview</label>
		<br>
		 <script src="js/signature_pad.js"></script>
		 <script src="js/app_exp_vou.js"></script>
		<img id="myImg" name="myImg" style="width:40%;" />
		<input type="hidden" id="sign" name="signature" />
	</div>
	
	<div class="form-group col-md-12">
		<div class="row">
			<div class="form-group col-md-6" style="color:red"><span id="sign_result">Please upload signature first.</span></div>
			
			<div class="form-group col-md-6">
				<button type="submit" style="display:none" id="expbtn" class="btn pull-right btn-md btn-danger"><span class="glyphicon glyphicon-chevron-right"></span> NEXT Step</button>
			</div>
		</div>
	</div>

	</div>
</div>
</div>
</form>
</body>
</html>

<input type="hidden" id="cash_limit">

<script type="text/javascript">
setInterval(function(){ 
 var sign = $("#sign").val();
 if(sign == '')
 {
	$("#expbtn").hide(); 
	$("#sign_result").html('Please upload signature first.'); 
 }
 else
 {
	 $("#expbtn").show(); 
	 $("#sign_result").hide(); 
 }
	 
}, 500);					
</script>

<!-- // fetch description title -->
<script type="text/javascript">
function copytext(sel) {

	$('#payment_mode').val('');
	payBy('');
	
	var exp_code = sel.split("_")[0];
	var cash_limit = sel.split("_")[1];
	var comment = sel.split("_")[2].toLowerCase();
		
	$('#comments').html(comment);
	$('#cash_limit').val(cash_limit);

	// 7 = CONVEYANCE , 36= TOURS & TRAVELLING EXPS.
 		
		if(exp_code == 36 || exp_code == 7)
		{
			$('#employee').show();
			$('#empcode').attr('required',true);
		}
		else
		{
			$('#employee').hide();
			$('#empcode').attr('required',false);
		}
		
		// 43= VEHICLE FUEL , 44=VEHICLE REPAIR & MAINTENANCE
		
		if(exp_code == 43 || exp_code == 44)
		{
			$('#vehicle').show();
			$('#vehno').attr('required',true);
		}
		else
		{
			$('#vehicle').hide();
			$('#vehno').attr('required',false);
		}
} 
</script>


<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("pan_no").setAttribute("style","background-color:red;color:#FFF");
				$("#expbtn").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("pan_no").setAttribute("style","background-color:green;color:#FFF");
				$("#expbtn").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("pan_no").setAttribute("style","background-color:white;");
			$("#expbtn").attr("disabled", false);
		}
  }
</script>	

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ac_ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ac_ifsc").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("expbtn").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ac_ifsc").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("expbtn").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ac_ifsc").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("expbtn").removeAttribute("disabled");
		}
 } 			
</script>

<script>	
function payBy(elem){
	
	$('#amount').val("");
	$('#amountw').val("");
	
	if(elem=="CHEQUE")
	{
		$('#chq_div').show();
		$("#chq_div :input").attr('required',true);
		
		$('#neft_div').hide();
		$("#neft_div :input").attr('required',false);
	}
	else if(elem == "NEFT")
	{
		$('#chq_div').hide();
		$("#chq_div :input").attr('required',false);
		
		$('#neft_div').show();
		$("#neft_div :input").attr('required',true);
	}
	else
	{
		$('#chq_div').hide();
		$("#chq_div :input").attr('required',false);
		
		$('#neft_div').hide();
		$("#neft_div :input").attr('required',false);
	}
}
</script>

<?php
include("./_footer.php");
?>