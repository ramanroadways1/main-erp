<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php
include("./_header.php");
include("./_loadicon.php");
include("./disable_right_click.php");
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
	font-size:12px !important; 
}
</style>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form2").on('submit',(function(e) {
e.preventDefault();
$("#loadicon").show();
$("#button_sub").attr("disabled",true);
$.ajax({
	url: "./unload_detention_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_function1").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-8 col-md-offset-3">

<form autocomplete="off" id="Form2">	

<div class="row">
	
<div class="form-group col-md-12">
	
	<div class="form-group col-md-12">				
		<br />
		<br />
		<center><h4 style="font-size:16px;color:#FFF">Request : Remove Late POD/Add Unloading/Detention
		<br />
		<br />
		लेट पहोच चार्ज हटाने एवं अनलोडिंग व डिटेंशन जोड़ने के लिए अनुरोध करे
		</h4></center>
		<br />
	</div>
	
<script>	
function GetFm(lrno)
{
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
				url: "get_fm_number.php",
				data: 'lrno=' + lrno,
				type: "POST",
				success: function(data) {
				$("#fm_no").html(data);
			},
			error: function() {}
		});
	}
}
</script>	
	
	<div class="form-group col-md-4">
		<label>LR Number <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="lrno" onblur="GetFm(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" type="text" class="form-control" required />
    </div>
	
	<div class="form-group col-md-4">
		<label>Select FM Number</label> 
		<select style="font-size:12px;height:32px;" id="fm_no" name="fm_no" required="required" class="form-control">
			<option style="font-size:12px" value="">--select fm--</option>
		</select>
    </div>

	<div class="form-group col-md-4">
		<label>Deduct Late POD</label> 
		<select style="font-size:12px;height:32px;" onchange="PODAmtSelection(this.value)" id="late_pod" name="late_pod" required="required" class="form-control">
			<option style="font-size:12px" value="">--select option--</option>
			<option style="font-size:12px" value="1">NO</option>
			<option style="font-size:12px" value="0">Yes (system default)</option>
			<option style="font-size:12px" value="2">Yes (entered value)</option>
		</select>
    </div>
	
	<script>
	function PODAmtSelection(elem)
	{
		if(elem=='2')
		{
			$('#late_pod_amt').attr('readonly',false);
		}
		else
		{
			$('#late_pod_amt').attr('readonly',true);
			$('#late_pod_amt').val('0');
		}
	}
	</script>
	
	<div class="form-group col-md-3">
		<label>LatePOD Amt <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" value="0" id="late_pod_amt" readonly name="late_pod_amt" type="number" min="0" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>Unloading(+) <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="unloading" name="add_unloading" type="number" min="0" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>Detention(+) <font color="red"><sup>*</sup></font></label>
		<input style="font-size:12px;height:32px;" id="detention" name="add_detention" type="number" min="0" class="form-control" required />
    </div>
	
	<div class="form-group col-md-3">
		<label>Narration <font color="red"><sup>*</sup></font></label>
		<textarea style="font-size:12px" rows="1" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-/,.]/,'')" required="required" id="narration" name="narration" class="form-control"></textarea>
    </div>

	<div class="form-group col-md-6">
		<input id="button_sub" disabled type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-sm btn-warning" name="submit" value="Submit Request" />
	</div>
	
</form>	
	
	<div id="result_function1"></div>
	
	<div class="form-group col-md-12">
	<label>Pending Approvals. </label>
		<table class="table table-bordered" style="font-size:12px">
			<tr>
				<th>#</th>
				<th>LR Number</th>
				<th>FM Number</th>
				<th>Unloading</th>
				<th>Detention</th>
				<th>Narration</th>
				<th>Timestamp</th>
			</tr>
			
		<?php
		$qry = Qry($conn,"SELECT lrno,frno,add_detention,add_unloading,narration,timestamp FROM rcv_pod_free WHERE is_allowed!='1' AND branch='$branch'");
		
		if(numRows($qry)==0)
		{
			echo "<tr><td colspan='7'>No record found..</td></tr>";
		}
		else
		{
			$sn=1; 
			while($row = fetchArray($qry))
			{
				echo "<tr>
					<td>$sn</td>
					<td>$row[lrno]</td>
					<td>$row[frno]</td>
					<td>$row[add_unloading]</td>
					<td>$row[add_detention]</td>
					<td>$row[narration]</td>
					<td>$row[timestamp]</td>
				</tr>";
			$sn++;
			}
		}
		?>		
		</table>
	</div>
	
</div>

</div>

</div>
</div>
</body> 
</html>