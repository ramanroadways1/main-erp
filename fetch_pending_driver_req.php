<?php
require_once("./connection.php");

$branch = escapeString($conn,($_SESSION['user']));

$fetch_req = Qry($conn,"SELECT name,mobile,lic,timestamp,supervisor_timestamp FROM dairy.driver_temp WHERE branch='$branch'");
	
if(!$fetch_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}
	
if(numRows($fetch_req)==0)
{
	echo "<script>
		alert('No pending request found !')
		$('#PendingReqDiv').hide();
		$('#FormDivDriver').show();
		$('#add_driver_button_own_truck').show();
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

echo "
<script>
	function FuncDriverPending()
	{
		$('#PendingReqDiv').hide();
		$('#FormDivDriver').show();
		$('#add_driver_button_own_truck').show();
	}
</script>

<button class='btn btn-xs btn-primary' onclick='FuncDriverPending()' type='button'>Hide</button>
<br />
<br />
<table class='table table-bordered' style='font-size:12px'>
<tr>
	<th>#</td>
	<th>Driver Name</td>
	<th>Mobile</td>
	<th>License Number</td>
	<th>Request Timestamp</td>
	<th>Request Status</td>
</tr>";		

$sn=1;
while($row = fetchArray($fetch_req))
{
	$timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
	
	if($row['supervisor_timestamp']!='' AND $row['supervisor_timestamp']!=0)
	{
		$status="Ho Approval Pending";
	}else
	{
		$status="Supervisor Approval Pending";
	}
	
	echo "<tr>
		<td>$sn</td>
		<td>$row[name]</td>
		<td>$row[mobile]</td>
		<td>$row[lic]</td>
		<td>$timestamp</td>
		<td style='color:red'>$status</td>
	</tr>";
$sn++;
}

echo "</table>
</div>

<script>
	$('#PendingReqDiv').show();
	$('#FormDivDriver').hide();
	$('#add_driver_button_own_truck').hide();
	$('#loadicon').fadeOut('slow');
</script>";
?>