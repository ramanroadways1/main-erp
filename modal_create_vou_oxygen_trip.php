<button style="display:none" type="button" id="open_oxygen_trip_modal" data-toggle="modal" data-target="#OxygenTripModal"></button>

<form style="font-size:13px" id="FormOxygenTrip" action="#" method="POST">
<div id="OxygenTripModal" style="background:#eee" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		LR Number : <span id="modal_oxygent_trip_lrno_html"></span>, Voucher Count: <span id="modal_oxygent_count"></span>
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-6">
				<label>Voucher number <font color="red"><sup>*</sup></font></label>
				<input style="font-size:13px" class="form-control" id="modal_oxn_vou_no" type="text" readonly>
			</div>
			
			<div class="form-group col-md-6">
				<label>Vehicle number <font color="red"><sup>*</sup></font></label>
				<input style="font-size:13px" class="form-control" id="modal_oxn_tno" type="text" readonly>
			</div>
			
			<div class="form-group col-md-6">
				<label>LR's Destination <font color="red"><sup>*</sup></font></label>
				<input style="font-size:13px" class="form-control" id="modal_oxn_lr_loc" type="text" readonly>
			</div>
			
			<div class="form-group col-md-6">
				<label>Destination Pincode <font color="red"><sup>*</sup></font></label>
				<input readonly style="font-size:13px" class="form-control" type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="6" required="required" />
			</div>
			
			<div class="form-group col-md-6">
				<label>From Location <font color="red"><sup>*</sup></font></label>
				<input style="font-size:13px" class="form-control" id="modal_oxn_from_loc" type="text" readonly>
			</div>
			
			<div class="form-group col-md-6">
				<label>To Location <font color="red"><sup>*</sup></font></label>
				<input style="font-size:13px" id="dest_location_oxygen_trip" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="dest_location" 
				class="form-control" required="required">
			</div>
		</div>
      </div>
	  <input type="hidden" name="dest_location_id" id="dest_location_oxygen_trip_id">
	  <input type="hidden" name="lrno" id="modal_oxygent_trip_lrno">
	  <input type="hidden" name="table_id" id="table_id">
	  
      <div class="modal-footer">
        <button type="submit" id="create_vou_btn_modal" class="btn btn-sm btn-primary">Create Voucher</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>

<div id="result_modal_oxygen_trip"></div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormOxygenTrip").on('submit',(function(e) {
		$("#loadicon").show();
		$('#create_vou_btn_modal').attr('disabled',true);
		e.preventDefault();
		$.ajax({
        	url: "./save_create_vou_oxygen_trip.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_modal_oxygen_trip").html(data);
				// $("#loadicon").hide();
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>