<?php 
require_once './connection.php';

$output ='';

$branch2=substr($branch,0,3)."OLR";

$from = escapeString($conn,$_POST['from_date']);
$to = escapeString($conn,$_POST['to_date']);
$company = escapeString($conn,$_POST['company']);
$file = escapeString($conn,$_POST['file']);

if($branch=='CGROAD')
{
	$branch='ABUROAD';
}

if($file=='FM_ADV' || $file=='LR_FM')
{
	
$delete_cache1 = Qry($conn,"DELETE FROM freight_memo_adv_cache_lr_report");
$delete_cache2 = Qry($conn,"DELETE FROM freight_memo_adv_cache_report");
	
if($file=='FM_ADV')
{
	$qry = Qry($conn,"INSERT INTO freight_memo_adv_cache_report(frno,branch,oid,bid,did,newdate,actualf,newtds,dsl_inc,gps,adv_claim,newother,
tds,totalf,totaladv,ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,chqno,disadv,rtgsneftamt,rtgs_adv,narre,baladv) SELECT frno,branch,
oid,bid,did,newdate,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,
chqno,disadv,rtgsneftamt,rtgs_adv,narre,baladv FROM freight_form WHERE newdate BETWEEN '$from' AND '$to' AND branch='$branch'");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
$qry_lr = Qry($conn,"INSERT INTO freight_memo_adv_cache_lr_report(frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,
	consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp) 
	SELECT frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,
	crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp FROM freight_form_lr WHERE frno 
	in(SELECT frno from freight_memo_adv_cache_report WHERE branch='$branch')");
	
	if(!$qry_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
$by_search="FM_date";	
}	
else
{
	$by_search="LR_date";	
	
	$qry_lr = Qry($conn,"INSERT INTO freight_memo_adv_cache_lr_report(frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,
	consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp) 
	SELECT frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,
	crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp FROM freight_form_lr WHERE frno 
	IN(SELECT frno FROM freight_form_lr WHERE date BETWEEN '$from' and '$to' AND branch='$branch' AND frno like '___F%')");
	
	if(!$qry_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$qry = Qry($conn,"INSERT INTO freight_memo_adv_cache_report(frno,branch,oid,bid,did,newdate,actualf,newtds,dsl_inc,gps,adv_claim,newother,
tds,totalf,totaladv,ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,chqno,disadv,rtgsneftamt,rtgs_adv,narre,baladv) SELECT frno,branch,
oid,bid,did,newdate,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,
chqno,disadv,rtgsneftamt,rtgs_adv,narre,baladv FROM freight_form WHERE frno in(SELECT frno from freight_memo_adv_cache_lr_report WHERE 
branch='$branch')");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
}	

	
$get_fm_data = Qry($conn,"SELECT frno,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,cashadv,chqadv,disadv,
	rtgsneftamt,baladv FROM freight_memo_adv_cache_report WHERE branch='$branch'");
	
	if(!$get_fm_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	

	while($row_fm = fetchArray($get_fm_data))
	{
			$db_af=$row_fm['actualf'];
			$load=$row_fm['newtds'];
			$dsl_inc=$row_fm['dsl_inc'];
			$gps=$row_fm['gps'];
			$claim=$row_fm['adv_claim'];
			$other=$row_fm['newother'];
			$tds=$row_fm['tds'];	
			$cash=$row_fm['cashadv'];	
			$disamt=$row_fm['disadv'];	
			$chqamt=$row_fm['chqadv'];	
			$rtgs=$row_fm['rtgsneftamt'];	
			$db_advance=$cash+$disamt+$chqamt+$rtgs;	
			
		if($db_af>0)
		{			
			$get_lr_data = Qry($conn,"SELECT id,lrno,actualf FROM freight_memo_adv_cache_lr_report WHERE frno='$row_fm[frno]'");
			if(!$get_lr_data){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}
			
			if(numRows($get_lr_data)==1)
			{
				$db_af_one=($db_af+$load+$dsl_inc)-($gps+$claim+$other+$tds);	
				$db_advance=$cash+$disamt+$chqamt+$rtgs;
				$db_balance=$db_af_one-$db_advance;
				
				$update_data=Qry($conn,"UPDATE freight_memo_adv_cache_lr_report SET loading='$load',dsl_inc='$dsl_inc',gps='$gps',
				claim='$claim',other='$other',tds='$tds',total_freight='$db_af_one',total_adv='$db_advance',cash='$cash',
				cheque='$chqamt',diesel='$disamt',rtgs='$rtgs',balance='$db_balance' WHERE frno='$row_fm[frno]'");
				if(!$update_data){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
			}
			else
			{
				while($row_lr_data = fetchArray($get_lr_data))
				{
					$db_lr=$row_lr_data['lrno'];
					$lr_actualf=$row_lr_data['actualf'];
					$lr_id=$row_lr_data['id'];
							
					$x=($lr_actualf*100)/$db_af;

					$load_db=sprintf("%.2f",((($load)*$x)/100));
					$dsl_inc_db=sprintf("%.2f",((($dsl_inc)*$x)/100));
					$gps_db=sprintf("%.2f",((($gps)*$x)/100));
					$claim_db=sprintf("%.2f",((($claim)*$x)/100));
					$other_db=sprintf("%.2f",((($other)*$x)/100));
					$tds_db=sprintf("%.2f",((($tds)*$x)/100));
					$cash_db=sprintf("%.2f",((($cash)*$x)/100));
					$diesel_db=sprintf("%.2f",((($disamt)*$x)/100));
					$cheq_db=sprintf("%.2f",((($chqamt)*$x)/100));
					$rtgs_db=sprintf("%.2f",((($rtgs)*$x)/100));

					$totalf_db=sprintf("%.2f",(($lr_actualf+$load_db+$dsl_inc_db)-($gps_db+$claim_db+$other_db+$tds_db)));
					$advance_db=sprintf("%.2f",($cash_db+$cheq_db+$diesel_db+$rtgs_db));
					$balance_db=sprintf("%.2f",($totalf_db-$advance_db));
					
					$update_data=Qry($conn,"UPDATE freight_memo_adv_cache_lr_report SET loading='$load_db',dsl_inc='$dsl_inc_db',
					gps='$gps_db',claim='$claim_db',other='$other_db',tds='$tds_db',total_freight='$totalf_db',
					total_adv='$advance_db',cash='$cash_db',cheque='$cheq_db',diesel='$diesel_db',rtgs='$rtgs_db',
					balance='$balance_db' WHERE id='$lr_id'");
					if(!$update_data){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					} 
				}
				
				$check_sum = Qry($conn,"SELECT SUM(loading) as load_lr,SUM(dsl_inc) as dsl_inc_lr,SUM(gps) as gps_lr,
					SUM(claim) as claim_lr,SUM(other) as other_lr,SUM(tds) as tds_lr,SUM(cash) as cash_lr,
					SUM(cheque) as cheque_lr,SUM(diesel) as diesel_lr,SUM(rtgs) as rtgs_lr FROM freight_memo_adv_cache_lr_report 
					WHERE frno='$row_fm[frno]'");
					if(!$check_sum){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
					
					$row_sum=fetchArray($check_sum);

					if($load!=$row_sum['load_lr'])
					{
						$load_diff=sprintf("%.2f",($load-$row_sum['load_lr']));
					}
					else
					{
						$load_diff=0;
					}
					
					if($gps!=$row_sum['gps_lr'])
					{
						$gps_diff=sprintf("%.2f",($gps-$row_sum['gps_lr']));
					}
					else
					{
						$gps_diff=0;
					}
					
					if($claim!=$row_sum['claim_lr'])
					{
						$claim_diff=sprintf("%.2f",($claim-$row_sum['claim_lr']));
					}
					else
					{
						$claim_diff=0;
					}

					if($dsl_inc!=$row_sum['dsl_inc_lr'])
					{
						$dsl_inc_diff=sprintf("%.2f",($dsl_inc-$row_sum['dsl_inc_lr']));
					}
					else
					{
						$dsl_inc_diff=0;
					}

					if($other!=$row_sum['other_lr'])
					{
						$other_diff=sprintf("%.2f",($other-$row_sum['other_lr']));
					}
					else
					{
						$other_diff=0;
					}

					if($tds!=$row_sum['tds_lr'])
					{
						$tds_diff=sprintf("%.2f",($tds-$row_sum['tds_lr']));
					}
					else
					{
						$tds_diff=0;
					}

					if($cash!=$row_sum['cash_lr'])
					{
						$cash_diff2=sprintf("%.2f",($cash-$row_sum['cash_lr']));
					}
					else
					{
						$cash_diff2=0;
					}

					if($chqamt!=$row_sum['cheque_lr'])
					{
						$cheque_diff=sprintf("%.2f",($chqamt-$row_sum['cheque_lr']));
					}
					else
					{
						$cheque_diff=0;
					}

					if($disamt!=$row_sum['diesel_lr'])
					{
						$diesel_diff=sprintf("%.2f",($disamt-$row_sum['diesel_lr']));
					}
					else
					{
						$diesel_diff=0;
					}

					if($rtgs!=$row_sum['rtgs_lr'])
					{
						$rtgs_diff=sprintf("%.2f",($rtgs-$row_sum['rtgs_lr']));
					}
					else
					{
						$rtgs_diff=0;
					}
					
					$diff1 = ($load_diff+$dsl_inc_diff)-($gps_diff+$claim_diff+$other_diff+$tds_diff);
					$diff2 = ($cash_diff2+$cheque_diff+$diesel_diff+$rtgs_diff);
					
					$update_data_lr = Qry($conn,"UPDATE freight_memo_adv_cache_lr_report SET loading=loading+'$load_diff',
					dsl_inc=dsl_inc+'$dsl_inc_diff',gps=gps+'$gps_diff',claim=claim+'$claim_diff',
					other=other+'$other_diff',tds=tds+'$tds_diff',total_freight=total_freight+'$diff1',
					total_adv=total_adv+'$diff2',cash=cash+'$cash_diff2',cheque=cheque+'$cheque_diff',
					diesel=diesel+'$diesel_diff',rtgs=rtgs+'$rtgs_diff',balance=total_freight-total_adv 
					WHERE frno='$row_fm[frno]' ORDER BY actualf DESC,id DESC LIMIT 1");
					if(!$update_data_lr){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
			}
		} // is freight > 0
	}
		
$result = Qry($conn,"SELECT f.frno,f.newdate,c.date as lr_date,c.truck_no,f.branch,c.company,f.ptob,
		f.adv_date,f.pto_adv_name,f.adv_pan,f.chqno,f.narre,c.lrno,c.fstation,c.tstation,c.consignor,
		c.consignee,c.wt12,c.weight,c.ratepmt,c.fix_rate,c.actualf,c.loading,c.dsl_inc,c.gps,c.claim,
		c.other,c.tds,c.total_freight,c.total_adv,c.cash,c.cheque,c.diesel,c.rtgs, c.balance,c.crossing,
		c.cross_to,b.name as broker_name,b.pan as broker_pan,o.name as owner_name,o.pan as owner_pan,
		d.name as driver_name,d.pan as driver_lic,l.po_no,l.do_no,l.shipno,l.invno,l.item,l.articles,l.goods_desc,
		l.goods_value,l.hsn_code
		FROM freight_memo_adv_cache_report AS f 
		LEFT OUTER JOIN freight_memo_adv_cache_lr_report AS c ON c.frno = f.frno 
		LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
		LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
		LEFT OUTER JOIN mk_driver AS d ON d.id = f.did 
		LEFT OUTER JOIN lr_sample AS l ON l.lrno = c.lrno 
		WHERE f.branch='$branch'");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '<table border="1">  
               <tr>  
                         <th>Vou_No</th>  						 
						 <th>Company</th>  
						 <th>Branch</th>  
						 <th>FM_Date</th>  
						 <th>LR_Date</th>  
						 <th>Truck No</th>  
						 <th>LR_No</th>  
                         <th>From_loc</th>  
                         <th>To_loc</th>  
                         <th>Consignor</th>  
                         <th>Consignee</th>  
                         <th>Act_wt</th>  
                         <th>Charge_wt</th>  
                         <th>Rate</th>  
                         <th>Fix/Rate</th>  
                         <th>Freight</th>  
                         <th>Load(+)</th>  
                         <th>Diesel Inc(+)</th>  
                         <th>Gps(-)</th>  
                         <th>Claim(-)</th>  
                         <th>Other(-)</th>  
                         <th>TDS(-)</th>  
                         <th>Total_Freight</th>  
                         <th>Advance_Amt</th>  
						 <th>Cash</th>  
                         <th>Cheq</th>  
						 <th>Cheq No</th>  
                         <th>Diesel</th>  
                         <th>Rtgs</th>  
						 <th>Balance</th>   
						 <th>Adv_Date</th>  
                         <th>Adv_Party_Name</th>  
                         <th>PAN_No</th>  
                         <th>Narration/Remark</th>  
                         <th>Crossing</th>  
                         <th>Crossing_To</th>  
                         <th>Owner_Name</th>  
                         <th>Owner_Pan</th>  
                         <th>Broker_Name</th>  
                         <th>Broker_Pan</th>  
                         <th>Driver_Name</th>  
                         <th>Driver_Lic</th>';
						if($file=='LR_FM')	
						{
							$output .= '
							<th>PO_No</th>
							<th>Do_No</th>
							<th>Shipment_No</th>
							<th>Inv.No</th>
							<th>Item</th>
							<th>Articles</th>
							<th>Goods_Desc.</th>
							<th>Goods_Value</th>
							<th>HSN_Code</th>
							';
						}
		$output .= '				 
      <tr>';
	
while($row = fetchArray($result))
{
	$output .= '<tr> 
						<td>'.$row["frno"].'</td>  
						<td>'.$row["company"].'</td>
						<td>'.$row["branch"].'</td>
						<td>'.$row["newdate"].'</td>
						<td>'.$row["lr_date"].'</td>
							<td>'.$row["truck_no"].'</td>
							<td>'.$row["lrno"].'</td>
							<td>'.$row["fstation"].'</td>  
							<td>'.$row["tstation"].'</td>  
							<td>'.$row["consignor"].'</td>  
							<td>'.$row["consignee"].'</td>  
							<td>'.$row["wt12"].'</td>  
							<td>'.$row["weight"].'</td>  
							<td>'.$row["ratepmt"].'</td>  
							<td>'.$row["fix_rate"].'</td>  
						   <td>'.$row["actualf"].'</td>
						   <td>'.$row["loading"].'</td>
						   <td>'.$row["dsl_inc"].'</td>
						   <td>'.$row["gps"].'</td>
						   <td>'.$row["claim"].'</td>
						   <td>'.$row["other"].'</td>
						   <td>'.$row["tds"].'</td>
						   <td>'.$row["total_freight"].'</td>
						   <td>'.$row["total_adv"].'</td>
						   <td>'.$row["cash"].'</td>
						   <td>'.$row["cheque"].'</td>
						   <td>'.$row["chqno"].'</td>
						   <td>'.$row["diesel"].'</td>
						   <td>'.$row["rtgs"].'</td>
						   <td>'.$row["balance"].'</td>
						   <td>'.$row["adv_date"].'</td>
						  <td>'.$row["pto_adv_name"].'</td>
							<td>'.$row["adv_pan"].'</td>
							<td>'.$row["narre"].'</td>  
							<td>'.$row["crossing"].'</td>  
							<td>'.$row["cross_to"].'</td>  
							<td>'.$row["owner_name"].'</td>  
							<td>'.$row["owner_pan"].'</td>  
							<td>'.$row["broker_name"].'</td>  
							<td>'.$row["broker_pan"].'</td>  
							<td>'.$row["driver_name"].'</td>  
							<td>'.$row["driver_lic"].'</td>';
						if($file=='LR_FM')	
						{
							$output .= '
							<td>'."'".$row["po_no"].'</td>
							<td>'."'".$row["do_no"].'</td>
							<td>'."'".$row["shipno"].'</td>
							<td>'."'".$row["invno"].'</td>
							<td>'.$row["item"].'</td>
							<td>'.$row["articles"].'</td>
							<td>'.$row["goods_desc"].'</td>
							<td>'.$row["goods_value"].'</td>
							<td>'.$row["hsn_code"].'</td>
							';
						}
	$output .= '					   
	</tr>';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_ADV_'.$by_search.'_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
}
else if($file=='FM_BAL')
{
$delete_cache1 = Qry($conn,"DELETE FROM freight_memo_bal_cache_report");
$delete_cache2 = Qry($conn,"DELETE FROM freight_memo_bal_cache_lr_report");
	
$qry = Qry($conn,"INSERT INTO freight_memo_bal_cache_report(frno,newdate,truck_no,branch,company,actualf,newtds,dsl_inc,gps,adv_claim,
	newother,tds,totalf,totaladv,cashadv,chqadv,disadv,rtgsneftamt,baladv,unloadd,detention,gps_rent,gps_device_charge,bal_tds,otherfr,
	claim,late_pod,totalbal,paidto,bal_date,pto_bal_name,bal_pan,paycash,paycheq,paycheqno,paydsl,newrtgsamt,narra) SELECT 
	frno,newdate,truck_no,branch_bal,company,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,cashadv,chqadv,disadv,
	rtgsneftamt,baladv,unloadd,detention,gps_rent,gps_device_charge,bal_tds,otherfr,claim,late_pod,totalbal,paidto,bal_date,pto_bal_name,
	bal_pan,paycash,paycheq,paycheqno,paydsl,newrtgsamt,narra 
	FROM freight_form WHERE bal_date BETWEEN '$from' AND '$to' AND branch_bal='$branch'");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$qry_lr = Qry($conn,"INSERT INTO freight_memo_bal_cache_lr_report(frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,
	consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf) SELECT frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,
	consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf FROM freight_form_lr WHERE frno 
	in(SELECT frno from freight_memo_bal_cache_report WHERE branch='$branch')");
	
	if(!$qry_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$get_fm_data = Qry($conn,"SELECT frno,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,cashadv,chqadv,disadv,
	rtgsneftamt,baladv,unloadd,detention,gps_rent,gps_device_charge,bal_tds,otherfr,claim,late_pod,totalbal,paycash,paycheq,paydsl,
	newrtgsamt FROM freight_memo_bal_cache_report WHERE branch='$branch'");
	
	if(!$get_fm_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
		
	while($row_fm = fetchArray($get_fm_data))
	{
			$db_af=$row_fm['actualf'];
			$load=$row_fm['newtds'];
			$dsl_inc=$row_fm['dsl_inc'];
			$gps=$row_fm['gps'];
			$claim=$row_fm['adv_claim'];
			$other=$row_fm['newother'];
			$tds=$row_fm['tds'];	
			$cash=$row_fm['cashadv'];	
			$disamt=$row_fm['disadv'];	
			$chqamt=$row_fm['chqadv'];	
			$rtgs=$row_fm['rtgsneftamt'];	
			$db_advance=$cash+$disamt+$chqamt+$rtgs;	
			$unloading=$row_fm['unloadd'];	
			$detention=$row_fm['detention'];	
			$gps_rent=$row_fm['gps_rent'];	
			$gps_device_charge=$row_fm['gps_device_charge'];	
			$bal_tds=$row_fm['bal_tds'];	
			$other2=$row_fm['otherfr'];	
			$bal_claim=$row_fm['claim'];	
			$late_pod=$row_fm['late_pod'];	
			$total_balance=$row_fm['totalbal'];	
			$cash2=$row_fm['paycash'];	
			$diesel2=$row_fm['paydsl'];	
			$cheque2=$row_fm['paycheq'];	
			$rtgs2=$row_fm['newrtgsamt'];	
			
		if($row_fm['baladv']>0)
		{			
			$get_lr_data = Qry($conn,"SELECT id,lrno,actualf FROM freight_memo_bal_cache_lr_report WHERE frno='$row_fm[frno]'");
			if(!$get_lr_data){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}
			
			if(numRows($get_lr_data)==1)
			{
				$db_af_one=($db_af+$load+$dsl_inc)-($gps+$claim+$other+$tds);	
				$db_advance=$cash+$disamt+$chqamt+$rtgs;
				$db_balance=$db_af_one-$db_advance;
				$db_total_balance=$cash2+$diesel2+$cheque2+$rtgs2;
				
				$update_data=Qry($conn,"UPDATE freight_memo_bal_cache_lr_report SET total_freight='$db_af_one',total_adv='$db_advance',
				balance='$db_balance',unloading='$unloading',detention='$detention',gps_rent='$gps_rent',gps_device_charge='$gps_device_charge',
				bal_tds='$bal_tds',other2='$other2',bal_claim='$bal_claim',late_pod='$late_pod',total_balance='$db_total_balance',
				cash2='$cash2',cheque2='$cheque2',diesel2='$diesel2',rtgs2='$rtgs2' WHERE frno='$row_fm[frno]'");
				if(!$update_data){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
			}
			else
			{
				while($row_lr_data = fetchArray($get_lr_data))
				{
					$db_lr=$row_lr_data['lrno'];
					$lr_actualf=$row_lr_data['actualf'];
					$lr_id=$row_lr_data['id'];
							
					$x=($lr_actualf*100)/$db_af;

					$load_db=sprintf("%.2f",((($load)*$x)/100));
					$dsl_inc_db=sprintf("%.2f",((($dsl_inc)*$x)/100));
					$gps_db=sprintf("%.2f",((($gps)*$x)/100));
					$claim_db=sprintf("%.2f",((($claim)*$x)/100));
					$other_db=sprintf("%.2f",((($other)*$x)/100));
					$tds_db=sprintf("%.2f",((($tds)*$x)/100));
					$cash_db=sprintf("%.2f",((($cash)*$x)/100));
					$diesel_db=sprintf("%.2f",((($disamt)*$x)/100));
					$cheq_db=sprintf("%.2f",((($chqamt)*$x)/100));
					$rtgs_db=sprintf("%.2f",((($rtgs)*$x)/100));
					
					$unloading_db=sprintf("%.2f",((($unloading)*$x)/100));
					$detention_db=sprintf("%.2f",((($detention)*$x)/100));
					$gps_rent_db=sprintf("%.2f",((($gps_rent)*$x)/100));
					$gps_device_charge_db=sprintf("%.2f",((($gps_device_charge)*$x)/100));
					$bal_tds_db=sprintf("%.2f",((($bal_tds)*$x)/100));
					$other2_db=sprintf("%.2f",((($other2)*$x)/100));
					$bal_claim_db=sprintf("%.2f",((($bal_claim)*$x)/100));
					$late_pod_db=sprintf("%.2f",((($late_pod)*$x)/100));
					
					$cash2_db=sprintf("%.2f",((($cash2)*$x)/100));
					$diesel2_db=sprintf("%.2f",((($diesel2)*$x)/100));
					$cheque2_db=sprintf("%.2f",((($cheque2)*$x)/100));
					$rtgs2_db=sprintf("%.2f",((($rtgs2)*$x)/100));

					$totalf_db=sprintf("%.2f",(($lr_actualf+$load_db+$dsl_inc_db)-($gps_db+$claim_db+$other_db+$tds_db)));
					$advance_db=sprintf("%.2f",($cash_db+$cheq_db+$diesel_db+$rtgs_db));
					$balance_db=sprintf("%.2f",($totalf_db-$advance_db));
					$total_balance_db=sprintf("%.2f",(($balance_db+$unloading_db+$detention_db)-($gps_rent_db+$gps_device_charge_db+$bal_tds_db+$other2_db+$bal_claim_db+$late_pod_db)));
				
					$update_data=Qry($conn,"UPDATE freight_memo_bal_cache_lr_report SET total_freight='$totalf_db',total_adv='$advance_db',
					balance='$balance_db',unloading='$unloading_db',detention='$detention_db',gps_rent='$gps_rent_db',
					gps_device_charge='$gps_device_charge_db',bal_tds='$bal_tds_db',other2='$other2_db',bal_claim='$bal_claim_db',
					late_pod='$late_pod_db',total_balance='$total_balance_db',cash2='$cash2_db',cheque2='$cheque2_db',
					diesel2='$diesel2_db',rtgs2='$rtgs2_db' WHERE id='$lr_id'");
					if(!$update_data){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					} 
				}
			}
		} // if balance > 0
	}
		
$result = Qry($conn,"SELECT l.frno,l.company,l.branch,l.date as lr_date,l.truck_no,l.lrno,l.fstation,l.tstation,l.consignor,l.consignee,
l.wt12,l.weight,l.ratepmt,l.fix_rate,l.actualf,l.total_freight,l.total_adv,l.balance,l.unloading,l.detention,l.gps_rent,
l.gps_device_charge,l.bal_tds,l.other2,l.bal_claim,l.late_pod,l.total_balance,l.cash2,l.cheque2,l.diesel2,l.rtgs2,f.newdate,
f.paidto as bal_to,f.bal_date,f.pto_bal_name,f.bal_pan,f.paycheqno,f.narra 
FROM freight_memo_bal_cache_report AS f 
LEFT OUTER JOIN freight_memo_bal_cache_lr_report AS l ON l.frno=f.frno
WHERE f.branch='$branch'");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
<table border="1">    
       <tr>  
						 <th>FM_No</th>  				
                         <th>Company</th>  
                         <th>Branch</th>  
                         <th>FM_date</th>  
                         <th>LR_date</th>  
                         <th>Truck_No</th>  
                         <th>LR_No</th>  
                         <th>From_loc</th>  
                         <th>To_loc</th>  
                         <th>Consignor</th>  
                         <th>Consignee</th>  
                         <th>Act_wt</th>  
                         <th>Charge_wt</th>  
                         <th>Rate</th>  
                         <th>Fix/Rate</th>  
                         <th>Freight</th>  
                         <th>Total_Freight</th>  
                         <th>Total_Advance</th>  
                         <th>Balance</th>  
                         <th>Unloading</th>  
                         <th>Detention</th>  
                         <th>Gps_Rent</th>  
                         <th>Gps_Device_Charge</th>  
                         <th>TDS</th>  
                         <th>Others</th>  
                         <th>Claim</th>  
                         <th>Late_POD</th>  
                         <th>Total_Balance</th>  
                         <th>Cash</th>  
                         <th>Cheque</th>  
                         <th>Cheque_No</th>  
                         <th>Diesel</th>  
                         <th>RTGS</th>  
                         <th>Balance_To</th>  
                         <th>Balance_Date</th>  
                         <th>Balance_Party</th>  
                         <th>PAN_No</th>  
                         <th>Narration/Remark</th>  
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
							<td>'.$row["frno"].'</td>  
							<td>'.$row["company"].'</td>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["lr_date"].'</td>  
							<td>'.$row["newdate"].'</td>  
							<td>'.$row["truck_no"].'</td>  
							<td>'.$row["lrno"].'</td>  
							<td>'.$row["fstation"].'</td>  
							<td>'.$row["tstation"].'</td>  
							<td>'.$row["consignor"].'</td>  
							<td>'.$row["consignee"].'</td>  
							<td>'.$row["wt12"].'</td>
							<td>'.$row["weight"].'</td>
							<td>'.$row["ratepmt"].'</td>
							<td>'.$row["fix_rate"].'</td>
							<td>'.$row["actualf"].'</td>
							<td>'.$row["total_freight"].'</td>
							<td>'.$row["total_adv"].'</td>
							<td>'.$row["balance"].'</td>
						   <td>'.$row["unloading"].'</td>
						   <td>'.$row["detention"].'</td>
						   <td>'.$row["gps_rent"].'</td>
						   <td>'.$row["gps_device_charge"].'</td>
						   <td>'.$row["bal_tds"].'</td>
						   <td>'.$row["other2"].'</td>
						   <td>'.$row["bal_claim"].'</td>
						   <td>'.$row["late_pod"].'</td>
						   <td>'.$row["total_balance"].'</td>
						   <td>'.$row["cash2"].'</td>
						   <td>'.$row["cheque2"].'</td>
						   <td>'.$row["paycheqno"].'</td>
						   <td>'.$row["diesel2"].'</td>
						   <td>'.$row["rtgs2"].'</td>
						   <td>'.$row["bal_to"].'</td>
						   <td>'.$row["bal_date"].'</td>
						   <td>'.$row["bal_pan"].'</td>
						   <td>'.$row["bal_pan"].'</td>
						   <td>'.$row["narra"].'</td>
	                </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_BAL_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;

}
else if($file=='LR' || $file=='LR_T')
{
	
if($file=='LR_T')
{
$result = Qry($conn,"SELECT l.company,l.branch,l.lrno,l.lr_type,l.date,l.truck_no,l.fstation,l.tstation,l.dest_zone,l.consignor,
l.consignee,l.po_no,l.bank,l.freight_type,l.sold_to_pay as sold_party,l.lr_name,l.wt12,l.gross_wt,l.weight,l.bill_rate,
l.bill_amt,l.t_type,l.do_no,l.shipno,l.invno,l.item,l.articles,l.goods_desc,l.goods_value,l.con1_gst,l.con2_gst,
l.hsn_code,f.crossing,l.round_trip_lrno FROM freight_form_lr AS f 
LEFT JOIN lr_sample AS l ON l.lrno = f.lrno 
WHERE f.date BETWEEN '$from' AND '$to' AND f.branch='$branch' ORDER BY f.id ASC");	
}
else
{	
$result = Qry($conn,"SELECT company,branch,lrno,lr_type,date,truck_no,fstation,tstation,dest_zone,consignor,consignee,po_no,bank,
freight_type,sold_to_pay as sold_party,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,articles,
goods_desc,goods_value,con1_gst,con2_gst,hsn_code,round_trip_lrno FROM lr_sample WHERE date BETWEEN '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");
}

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
        <tr>  
		<th>Company</th>
		<th>Branch</th>
		<th>LR_No</th>';
	if($branch=='PUNE')
	{
		$output .= '<th>Round_Trip LR</th>';
	}	
	$output .= '
		<th>Vehicle_Type</th>
		<th>LR_Date</th>
		<th>Truck No</th>
		<th>From_loc</th>
		<th>To_loc</th>
		<th>Dest.Zone</th>
		<th>Consignor</th>
		<th>Consignor_GST</th>
		<th>Consignee</th>
		<th>Consignee_GST</th>
		<th>PO_No</th>
		<th>Bank</th>
		<th>Freight_Type</th>
		<th>Sold_To_Party</th>
		<th>LR_Name</th>
		<th>Act.Wt</th>
		<th>Gross_Wt</th>
		<th>Chrg.Wt</th>
		<th>Bill.Rate</th>
		<th>Bill.Amt</th>
		<th>Truck_Type</th>
		<th>Do_No</th>
		<th>Shipment_No</th>
		<th>Inv.No</th>
		<th>Item</th>
		<th>Articles</th>
		<th>Goods_Desc.</th>
		<th>Goods_Value</th>
		<th>HSN_Code</th>';
	
	if($file=='LR_T')
	{
		$output .= '<th>Is_TransShipment</th>';
	}
	
 $output .= '</tr>';
 
  while($row = fetchArray($result))
  {
  $output .= '
    <tr> 
			<td>'.$row["company"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["lrno"].'</td>';
	if($branch=='PUNE')
	{
		$output .= '<td>'.$row["round_trip_lrno"].'</td>';
	}	
	 $output .= '
			<td>'.$row["lr_type"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["truck_no"].'</td>
			<td>'.$row["fstation"].'</td>
			<td>'.$row["tstation"].'</td>
			<td>'.$row["dest_zone"].'</td>
			<td>'.$row["consignor"].'</td>
			<td>'.$row["con1_gst"].'</td>
			<td>'.$row["consignee"].'</td>
			<td>'.$row["con2_gst"].'</td>
			<td>'."'".$row["po_no"].'</td>
			<td>'.$row["bank"].'</td>
			<td>'.$row["freight_type"].'</td>
			<td>'.$row["sold_party"].'</td>
			<td>'.$row["lr_name"].'</td>
			<td>'.$row["wt12"].'</td>
			<td>'.$row["gross_wt"].'</td>
			<td>'.$row["weight"].'</td>
			<td>'.$row["bill_rate"].'</td>
			<td>'.$row["bill_amt"].'</td>
			<td>'.$row["t_type"].'</td>
			<td>'."'".$row["do_no"].'</td>
			<td>'."'".$row["shipno"].'</td>
			<td>'."'".$row["invno"].'</td>
			<td>'.$row["item"].'</td>
			<td>'.$row["articles"].'</td>
			<td>'.$row["goods_desc"].'</td>
			<td>'.$row["goods_value"].'</td>
			<td>'.$row["hsn_code"].'</td>';
			if($file=='LR_T')
			{
				$output .= '<td>'.$row["crossing"].'</td>';
			}
	   $output .= '</tr>';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=LR-ENTRY_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
	
}
else if($file=='EXP')
{
$result = Qry($conn,"SELECT e.user,e.vno,e.newdate,e.comp,e.des,e.amt,e.chq,e.chq_no,e.chq_bnk_n,e.neft_bank,e.neft_acname,e.neft_acno,
e.neft_ifsc,e.pan,e.narrat,emp.name,e.vehno 
FROM mk_venf AS e 
LEFT OUTER JOIN emp_attendance as emp ON emp.code=e.empcode 
WHERE e.newdate BETWEEN '$from' AND '$to' AND e.user='$branch' ORDER BY e.id ASC");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">   
    <tr>  
		<th>Branch</th>
		<th>Vou No</th>
		<th>Vou Date</th>
		<th>Company</th>
		<th>Expense Desc</th>
		<th>Amount</th>
		<th>Cash/ChQ/Neft</th>
		<th>Chq No</th>
		<th>Chq Bank</th>
		<th>NEFT A/c</th>
		<th>A/c No</th>
		<th>NEFT Bank</th>
		<th>IFSC</th>
		<th>PAN No</th>
		<th>Narration</th>
		<th>Emp.Name</th>
		<th>Veh.No</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["user"].'</td>
			<td>'.$row["vno"].'</td>
			<td>'.$row["newdate"].'</td>
			<td>'.$row["comp"].'</td>
			<td>'.$row["des"].'</td>
			<td>'.$row["amt"].'</td>
			<td>'.$row["chq"].'</td>
			<td>'.$row["chq_no"].'</td>
			<td>'.$row["chq_bnk_n"].'</td>
			<td>'.$row["neft_bank"].'</td>
			<td>'.$row["neft_acname"].'</td>
			<td>'.$row["neft_acno"].'</td>
			<td>'.$row["neft_ifsc"].'</td>
			<td>'.$row["pan"].'</td>
			<td>'.$row["narrat"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$row["vehno"].'</td>
            </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=EXP-VOU_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
}
else if($file=='TDV')
{
	
$result = Qry($conn,"SELECT user,company,tdvid,newdate,truckno,dname,amt,mode,chq_no,chq_bank,ac_name,ac_no,bank,ifsc,pan,
dest FROM mk_tdv WHERE newdate BETWEEN '$from' AND '$to' AND user='$branch' ORDER BY id ASC");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">    
    <tr>  
		<th>Company</th>
		<th>Branch</th>
		<th>Vou No</th>
		<th>Vou date</th>
		<th>Truck No</th>
		<th>Driver Name</th>
		<th>Adv Amount</th>
		<th>Cash/Chq/NEFT</th>
		<th>Chq No</th>
		<th>Chq Bank Name</th>
		<th>A/c Holder</th>
		<th>A/c No</th>
		<th>Bank Name</th>
		<th>IFSC</th>
		<th>PAN NO</th>
		<th>Destination/Remark</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["company"].'</td>
			<td>'.$row["user"].'</td>
			<td>'.$row["tdvid"].'</td>
			<td>'.$row["newdate"].'</td>
			<td>'.$row["truckno"].'</td>
			<td>'.$row["dname"].'</td>
			<td>'.$row["amt"].'</td>
			<td>'.$row["mode"].'</td>
			<td>'.$row["chq_no"].'</td>
			<td>'.$row["chq_bank"].'</td>
			<td>'."'".$row["ac_name"].'</td>
			<td>'."'".$row["ac_no"].'</td>
			<td>'."'".$row["bank"].'</td>
			<td>'.$row["ifsc"].'</td>
			<td>'.$row["pan"].'</td>
			<td>'.$row["dest"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Truck-Vou_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
}
else if($file=='OWN')
{
$result = Qry($conn,"SELECT frno,company,branch,date,truck_no,lrno,fstation,tstation,consignor,wt12,weight FROM 
freight_form_lr WHERE date BETWEEN '$from' AND '$to' AND branch='$branch' AND frno like '$branch2%' ORDER BY id ASC");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">  
    <tr>  
		<th>Company</th>
		<th>Branch</th>
		<th>FR No</th>
		<th>Date</th>
		<th>Truck No</th>
		<th>LR NO</th>
		<th>FROM</th>
		<th>To</th>
		<th>Consignor</th>
		<th>Act Wt</th>
		<th>Chrg Wt</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["company"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["frno"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["truck_no"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["fstation"].'</td>
			<td>'.$row["tstation"].'</td>
			<td>'.$row["consignor"].'</td>
			<td>'.$row["wt12"].'</td>
			<td>'.$row["weight"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=OWN_TRUCK_FORM_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
}
else if($file=='MB')
{
$result = Qry($conn,"SELECT company,date,lrdate,bilty_no,billing_type,veh_placer,plr,broker,billing_party,tno,frmstn,tostn,
awt,cwt,rate,tamt,branch FROM mkt_bilty WHERE lrdate BETWEEN '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}


 $output .= '
   <table border="1">    
    <tr>  
		<th>Company</th>
		<th>Sys Date</th>
		<th>LR Date</th>
		<th>Bilty No</th>
		<th>Billing Type</th>
		<th>Vehicle Placer</th>
		<th>PARTY LR No</th>
		<th>Broker</th>
		<th>Billing Party</th>
		<th>Truck No</th>
		<th>FROM</th>
		<th>TO</th>
		<th>Act_Weight</th>
		<th>Charge_Weight</th>
		<th>Rate</th>
		<th>Amount</th>
		<th>Branch</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["company"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["lrdate"].'</td>
			<td>'.$row["bilty_no"].'</td>
			<td>'.$row["billing_type"].'</td>
			<td>'.$row["veh_placer"].'</td>
			<td>'.$row["plr"].'</td>
			<td>'.$row["broker"].'</td>
			<td>'.$row["billing_party"].'</td>
			<td>'.$row["tno"].'</td>
			<td>'.$row["frmstn"].'</td>
			<td>'.$row["tostn"].'</td>
			<td>'.$row["awt"].'</td>
			<td>'.$row["cwt"].'</td>
			<td>'.$row["rate"].'</td>
			<td>'.$row["tamt"].'</td>
			<td>'.$row["branch"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=MKT_BILTY_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
	
}
else if($file=='CASH')
{
	
if($company=='RRPL')
{
$result=Qry($conn,"SELECT user,date,vou_date,comp,vou_no,vou_type,desct,lrno,debit,credit,balance FROM cashbook WHERE 
date BETWEEN '$from' AND '$to' AND user='$branch' AND comp='RRPL' ORDER BY id ASC");	
}	
else
{	
$result=Qry($conn,"SELECT user,date,vou_date,comp,vou_no,vou_type,desct,lrno,debit2,credit2,balance2 FROM cashbook WHERE 
date BETWEEN '$from' AND '$to' AND user='$branch' AND comp='RAMAN_ROADWAYS' ORDER BY id ASC");	
}


if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}


 $output .= '
  <table border="1">    
    <tr>  
		<th>Branch</th>
		<th>Date</th>
		<th>Company</th>
		<th>Vou Date</th>
		<th>Vou No</th>
		<th>Vou Type</th>
		<th>Description</th>
		<th>LR No</th>
		<th>Dr.</th>
		<th>Cr.</th>
		<th>Balance</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
	 if($company=='RRPL')
	 {
		 $dr = $row['debit'];
		 $cr = $row['credit'];
		 $bal1 = $row['balance'];
	 }	
		else if($company=='RAMAN_ROADWAYS')
		{
			$dr = $row['debit2'];
		 $cr = $row['credit2'];
		 $bal1 = $row['balance2'];
		}
	  
   $output .= '
    <tr> 
			<td>'.$row["user"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["comp"].'</td>
			<td>'.$row["vou_date"].'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$row["vou_type"].'</td>
			<td>'.$row["desct"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$dr.'</td>
			<td>'.$cr.'</td>
			<td>'.$bal1.'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=CASH-BOOK_'.$company.'_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
		
}
else if($file=='PASS')
{
	
if($company=='RRPL')
{
$result = Qry($conn,"SELECT user,vou_no,date,vou_date,comp,vou_type,desct,lrno,chq_no,debit,credit FROM passbook WHERE 
date BETWEEN '$from' AND '$to' AND user='$branch' AND comp='RRPL' ORDER BY id ASC");	
}	
else
{	
$result = Qry($conn,"SELECT user,vou_no,date,vou_date,comp,vou_type,desct,lrno,chq_no,debit,credit FROM passbook WHERE 
date BETWEEN '$from' AND '$to' AND user='$branch' AND comp='RAMAN_ROADWAYS' ORDER BY id ASC");	
}

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}


 $output .= '
  <table border="1">   
    <tr>  
		<th>Branch</th>
		<th>Vou No</th>
		<th>Date</th>
		<th>Vou Date</th>
		<th>Company</th>
		<th>Vou Type</th>
		<th>Description</th>
		<th>LR No</th>
		<th>Cheq No</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
	 if($company=='RRPL')
	 {
		 $dr = $row['debit'];
		 $cr = $row['credit'];
	 }	
		else if($company=='RAMAN_ROADWAYS')
		{
			$dr = $row['debit2'];
		 $cr = $row['credit2'];
		}
	  
   $output .= '
    <tr> 
			<td>'.$row["user"].'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["vou_date"].'</td>
			<td>'.$row["comp"].'</td>
			<td>'.$row["vou_type"].'</td>
			<td>'.$row["desct"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["chq_no"].'</td>
			<td>'.$dr.'</td>
			<td>'.$cr.'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=PASSBOOK_'.$company.'_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
	
}
else if($file=='DSL')
{
$result = Qry($conn,"SELECT branch,fno,token_no,com,qty,rate,disamt,tno,lrno,type,dsl_by,dcard,dcom,dsl_nrr,pay_date,fm_date FROM 
diesel_fm WHERE pay_date between '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}


 $output .= '
  <table border="1">    
    <tr>  
		<th>Branch</th>
		<th>Vou_No</th>
		<th>Token_No</th>
		<th>Vou_Date</th>
		<th>Recharge Date</th>
		<th>Company</th>
		<th>Qty</th>
		<th>Rate</th>
		<th>Amount</th>
		<th>Truck No</th>
		<th>LR No</th>
		<th>ADV/BAL</th>
		<th>Card/Pump</th>
		<th>Card No/Pump Code</th>
		<th>Diesel Company</th>
		<th>Narration</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["branch"].'</td>
			<td>'.$row["fno"].'</td>
			<td>'.$row["token_no"].'</td>
			<td>'.$row["fm_date"].'</td>
			<td>'.$row["pay_date"].'</td>
			<td>'.$row["com"].'</td>
			<td>'.$row["qty"].'</td>
			<td>'.$row["rate"].'</td>
			<td>'.$row["disamt"].'</td>
			<td>'.$row["tno"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["type"].'</td>
			<td>'.$row["dsl_by"].'</td>
			<td>'.$row["dcard"].'</td>
			<td>'.$row["dcom"].'</td>
			<td>'.$row["dsl_nrr"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=DIESEL_BOOK_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
		
}
else if($file=='RTGS')
{
$result=Qry($conn,"SELECT fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,lrno,tno,type,crn,bank 
FROM rtgs_fm WHERE pay_date BETWEEN '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
 <table border="1">    
    <tr>  
		<th>Branch</th>
		<th>Company</th>
		<th>FM Date</th>
		<th>Payment Date</th>
		<th>FM No</th>
		<th>Truck No</th>
		<th>LR No</th>
		<th>Total Freight</th>
		<th>RTGS Amount</th>
		<th>AC Name</th>
		<th>AC No</th>
		<th>IFSC</th>
		<th>PAN NO</th>
		<th>ADV/BAL</th>
		<th>Ref. No.</th>
		<th>UTR No.</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["branch"].'</td>
			<td>'.$row["com"].'</td>
			<td>'.$row["fm_date"].'</td>
			<td>'.$row["pay_date"].'</td>
			<td>'.$row["fno"].'</td>
			<td>'.$row["tno"].'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["totalamt"].'</td>
			<td>'.$row["amount"].'</td>
			<td>'.$row["acname"].'</td>
			<td>'."'".$row["acno"].'</td>
			<td>'.$row["ifsc"].'</td>
			<td>'.$row["pan"].'</td>
			<td>'.$row["type"].'</td>
			<td>'.$row["crn"].'</td>
			<td>'.$row["bank"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=RTGS_SHEET_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;

}
else if($file=='CR')
{
$result = Qry($conn,"SELECT section,tno,lr_date,from_stn,to_stn,branch,company,bilty_no,amount,narr,date FROM credit WHERE 
date BETWEEN '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
    <tr>  
		<th>A/c For</th>
		<th>Date</th>
		<th>Truck No</th>
		<th>LR Date</th>
		<th>From</th>
		<th>To</th>
		<th>Branch</th>
		<th>Company</th>
		<th>Bilty No</th>
		<th>Amount</th>
		<th>Narration</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["section"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["tno"].'</td>
			<td>'.$row["lr_date"].'</td>
			<td>'.$row["from_stn"].'</td>
			<td>'.$row["to_stn"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["company"].'</td>
			<td>'.$row["bilty_no"].'</td>
			<td>'.$row["amount"].'</td>
			<td>'.$row["narr"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=CREDITS_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;

}
else if($file=='DR')
{
$result = Qry($conn,"SELECT section,to_branch,branch,company,amount,narr,date FROM debit WHERE 
date BETWEEN '$from' AND '$to' AND branch='$branch' ORDER BY id ASC");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>Branch</th>
		<th>Company</th>
		<th>Date</th>
		<th>A/c For</th>
		<th>To Branch</th>
		<th>Amount</th>
		<th>Narration</th>
	</tr>
  ';
  while($row = fetchArray($result))
  {
   $output .= '
    <tr> 
			<td>'.$row["branch"].'</td>
			<td>'.$row["company"].'</td>
			<td>'.$row["date"].'</td>
			<td>'.$row["section"].'</td>
			<td>'.$row["to_branch"].'</td>
			<td>'.$row["amount"].'</td>
			<td>'.$row["narr"].'</td>
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=DEBITS_'.$branch.'_'.$from.'_To_'.$to.'.xls');
  echo $output;
}
else
{
	echo "<script type='text/javascript'>
		alert('Something went wrong..');
		window.location.href='./';
		</script>";
}
?>