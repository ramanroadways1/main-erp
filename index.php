<?php
require_once 'connection.php';

if(!isset($_SESSION['user'])){
	echo "<script>
		window.location.href='./logout.php';
	</script>";
	exit();
}

// include '_user_role.php';

$branch = escapeString($conn,strtoupper($_SESSION['user']));
$date = date("Y-m-d"); 

//////////////////////////////////////// ATTENDANCE //////////////////////////////////////////////

$check_today_attd = Qry($conn,"SELECT branch FROM emp_attd_check WHERE date='$date'");

if(!$check_today_attd){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($check_today_attd)==0)
{
	$insert_all_branch = Qry($conn,"INSERT INTO emp_attd_check(branch,total,date) SELECT branch,
	SUM(case when status in('3','2') then 1 when terminate='0' and status='-1' then 1 else 0 end),'$date' 
	FROM emp_attendance WHERE branch!='' GROUP by branch");
	
	if(!$insert_all_branch)
	{
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
}

$chk_old_attd = Qry($conn,"SELECT date FROM emp_attd_check WHERE date!='$date' AND total!=p+a+hd AND branch='$branch' 
ORDER BY id ASC LIMIT 1");

if(!$chk_old_attd){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
	
if(numRows($chk_old_attd)>0)
{
	$row_old = fetchArray($chk_old_attd);
	$date_attendance = $row_old['date'];	
	$set_value=1;
}
else
{
	$date_attendance = $date;
	$set_value=0;
}

//////////////////////////////////////  Attendance ENDS /////////////////////////////////////////////

$ChkTodayDataValue = Qry($conn,"SELECT * FROM today_data WHERE date='$date'");

if(!$ChkTodayDataValue){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($ChkTodayDataValue)==0)
{
	$dltOldRecords = Qry($conn,"DELETE FROM today_data");
	if(!$dltOldRecords){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$insertAllBranches = Qry($conn,"INSERT INTO today_data(branch,date) SELECT username,'$date' FROM user where role='2'");
	if(!$insertAllBranches){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
}

$get_today_data = Qry($conn,"SELECT * FROM today_data WHERE date='$date' AND branch='$branch'");

if(!$get_today_data){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$get_LRs = Qry($conn,"SELECT * FROM _pending_lr WHERE branch='$branch'");

if(!$get_LRs){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$get_LRs_pending_m = Qry($conn,"SELECT id FROM lr_sample_pending WHERE branch='$branch' AND lr_type='MARKET'");
$get_LRs_pending_o = Qry($conn,"SELECT id FROM lr_sample_pending WHERE branch='$branch' AND lr_type='OWN'");

if(!$get_LRs_pending_m){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(!$get_LRs_pending_o){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$get_new_emp_approval = Qry($conn,"SELECT id FROM emp_attendance WHERE branch='$branch' AND status='0'");

if(!$get_new_emp_approval){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$new_emp_approval = numRows($get_new_emp_approval);

$get_terminated_emp_approval = Qry($conn,"SELECT id FROM emp_terminated WHERE branch='$branch' AND approval='0'");

if(!$get_terminated_emp_approval){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$terminated_emp_approval = numRows($get_terminated_emp_approval);

$get_tranfer_emp_approval_from = Qry($conn,"SELECT id FROM emp_transfer WHERE oldbranch='$branch' AND approval_from='0'");

if(!$get_tranfer_emp_approval_from){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$get_tranfer_emp_approval_to = Qry($conn,"SELECT id FROM emp_transfer WHERE newbranch='$branch' AND approval_to='0'");

if(!$get_tranfer_emp_approval_to){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$transfer_emp_approval_from = numRows($get_tranfer_emp_approval_from);
$transfer_emp_approval_to = numRows($get_tranfer_emp_approval_to);

$total_emp_approval_pending = $transfer_emp_approval_from+$transfer_emp_approval_to+$terminated_emp_approval+$new_emp_approval;

$row_data = fetchArray($get_today_data);
$row_LRs_Pending = fetchArray($get_LRs);

$truck_vou_count = $row_data['truck_vou'];
$truck_vou_amount = $row_data['truck_vou_amount'];
$exp_vou_count = $row_data['exp_vou'];
$exp_vou_amount = $row_data['exp_vou_amount'];
$fm_adv_count = $row_data['fm_adv'];
$fm_adv_amount = $row_data['fm_adv_amount'];
$fm_bal_count = $row_data['fm_bal'];
$fm_bal_amount = $row_data['fm_bal_amount'];
$diesel_amount_market = $row_data['diesel_amount_market'];
$diesel_amount_own = $row_data['diesel_amount_own'];
$neft_count = $row_data['neft'];
$neft_amount = $row_data['neft_amount'];

$pending_market_lr = numRows($get_LRs_pending_m);
$pending_own_lr = numRows($get_LRs_pending_o);

?>
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
include("./disable_right_click_for_index.php");
?>

<link href="css/styles.css" rel="stylesheet">

<script type="text/javascript">
$(document).ready(function (e) {
$("#attdForm").on('submit',(function(e) {
$('#save_attd').attr('disabled',true);	
$("#loadicon").show();
e.preventDefault();
 $("#save_attd").attr("disabled",true);
	$.ajax({
	url: "./save_attendance.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#attd_result").html(data);
		// $("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<script type="text/javascript">
	$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $("#button_tdv").attr('disabled',false);
			return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#truck_no").val('');
			$("#button_tdv").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script> 

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">

	<?php
	$chk_a=Qry($conn,"SELECT total,p+a+hd as new FROM emp_attd_check WHERE date='$date_attendance' AND branch='$branch'");
	
	if(!$chk_a)
	{
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
			
	if(numRows($chk_a)>0)
	{
		$row_a = fetchArray($chk_a);
		
		if($row_a['total']!=$row_a['new'])
		{
			// echo '<br><a style="text-decoration:none;" data-toggle="modal" data-target="#view_attd">
			// <span class="page-header" style="color:#FFF;">
			// <span class="glyphicon glyphicon-list-alt"></span>&nbsp; View Attendance : '.date("d/m/y",strtotime($date_attendance)).'</span>
			// </a>';
		// }
		// else
		// {
			if($date_attendance==$date)
			{
				$from_time = "17:00 pm";
				$to_time = "23:30 pm";
				$current_time = DateTime::createFromFormat('H:i a', date("H:i a"));
				$from1 = DateTime::createFromFormat('H:i a', $from_time);
				$to1 = DateTime::createFromFormat('H:i a', $to_time);
				if($current_time > $from1 && $current_time < $to1)		
				{		
					$set_value=1;
				}
			}
			else
			{
				$set_value=1;
			}
		}
	}
	// else
	// {
		// echo '<br>
		// <span class="page-header" style="color:#FFF;">
		// <i class="fa fa-users" aria-hidden="true"></i> No employee data</span>';
	// }
	echo '<br>
		<div class="form-inline">
			<label style="font-size:12px;color:#FFF">View Attendance :</label>
			&nbsp; <input id="attd_viewDate" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" onchange="ViewAttandance(this.value)" max="'.date("Y-m-d").'" style="height:25px;font-size:11px;" type="date" class="form-control">
		';
		
		if($_SESSION['user_code']=='032')
		{
		?>
		<script>	
		function SwitchBranchAdmin(branch)
		{
			$("#loadicon").show();
			jQuery.ajax({
				url: "./_switch_branch_admin.php",
				data: 'branch=' + branch,
				type: "POST",
				success: function(data) {
					$("#branch_switch_res1").html(data);
				},
				error: function() {}
			});
		}
		</script>	
			<?php
			echo '
			&nbsp; <label style="font-size:12px;color:#FFF">Switch Branch :</label>
			&nbsp; <select class="form-control" onchange="SwitchBranchAdmin(this.value)" style="height:28px;font-size:10px;">';
				$get_bb = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('DUMMY','HEAD') ORDER BY username ASC");
				
				while($row_bb = fetchArray($get_bb))
				{
					?>
					<option <?php if($row_bb['username']==$_SESSION['user']) { echo "selected='selected'"; } ?> style='font-size:10px' value="<?php echo $row_bb['username']; ?>"><?php echo $row_bb['username']; ?></option>
					<?php
				}
			echo '</select>
		
		';
		}
		// if(isset($_SESSION['manager_login']))
		// {
			// echo '<a href="./manager/"><button type="button" class="btn btn-warning btn-xs">Employee Approval Pending ('.$total_emp_approval_pending.')</button></a>';
		// }
		// else
		// {
			// $chk_this_user = Qry($conn,"SELECT id FROM manager WHERE branch='$branch' AND emp_code='$_SESSION[user_code]'");
			// if(!$chk_this_user){
				// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
				// Redirect("Error while processing Request","./");
				// exit();
			// }
			
			// if(numRows($chk_this_user)>0)
			// {
				// echo '<button onclick="OpenModalFunction()" type="button" class="btn btn-warning btn-xs">Employee Approval Pending ('.$total_emp_approval_pending.')</button>';
			// }
			// else
			// {
				// echo '<button onclick="AlertFunction()" type="button" class="btn btn-warning btn-xs">Employee Approval Pending ('.$total_emp_approval_pending.')</button>';
			// }
		// }
		echo '</div>'; //
	?>
	
<div id="branch_switch_res1"></div>		
<script>	
function AlertFunction()
{
	alert('You are not manager of this branch !!');
}

function OpenModalFunction()
{
	document.getElementById("mgr_login_modal_btn").click(); 
}

function ViewAttandance(date)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "./fetch_attendance.php",
		data: 'date=' + date,
		type: "POST",
		success: function(data) {
			$("#result_attd_fetch").html(data);
		},
	error: function() {}
	});
}
</script>	

<div id="result_attd_fetch"></div>

<a style="display:none" id="modal_link" data-toggle="modal" data-target="#attd_modal"></a>
<a style="display:none" id="modal_pod_alert" data-toggle="modal" data-target="#pod_alert_modal"></a>
<a style="display:none" id="mgr_login_modal_btn" data-toggle="modal" data-target="#MgrLoginModal"></a>

<br />
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/exp_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $exp_vou_amount; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $exp_vou_count; ?></b> 
							Exp Vou.</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/truck_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $truck_vou_amount; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $truck_vou_count; ?></b> 
							Truck Vou.</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/fm_adv.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $fm_adv_amount; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $fm_adv_count; ?></b> 
							FM Adv</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/fm_bal.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $fm_bal_amount; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $fm_bal_count; ?></b> 
							FM Bal</div>
						</div>
					</div>
				</div>
			</div>	
          </div>
	<br />
	<!-- Second -->
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/pending_lr.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000"> <?php echo $pending_own_lr; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">Pending Own LR</div>
						</div>
					</div>
				</div>
			</div>	
			
                        		
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/pending_lr.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000"> <?php echo $pending_market_lr; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">Pending Mkt LR</div>
						</div>
					</div>
				</div>
			</div>	
            
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/diesel.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $diesel_amount_market; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">
							 <?php echo ""; ?> Diesel (Market Truck)</div>
						</div>
					</div>
				</div>
		</div>	
           
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/diesel.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $diesel_amount_own; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo ""; ?></b> 
							Diesel (Own Truck)</div>
						</div>
					</div>
				</div>
		</div>	
        </div>
<br />		  
<!-- Third -->

	<div class="row">
		<a target="_bank" href="./report/dashboard/lr_30_days.php">
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/time.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000"> <?php echo $row_LRs_Pending['lr_30_days']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">LR > 30 Days</div>
						</div>
					</div>
				</div>
			</div>	
		</a>	
		
	<a target="_bank" href="./report/dashboard/pod_pending.php">		
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/pod.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000"> <?php echo $row_LRs_Pending['lr_pod_pending']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">POD Pending</div>
						</div>
					</div>
				</div>
		</div>	
	</a>	
          
	<a target="_bank" href="./report/dashboard/balance_pending.php">  
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/pending.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000"> <?php echo $row_LRs_Pending['bal_pending']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">
							Bal. Pending</div>
						</div>
					</div>
				</div>
		</div>	
      </a>    
		
	<a target="_bank" href="./report/dashboard/today_neft.php">	
		<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="svg/reject.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $neft_amount; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;">
							<?php echo $neft_count; ?> Today's Neft</div>
						</div>
					</div>
				</div>
			</div>
	</a>		
          </div>
		
<br />

<style>
#lbl{
	color:#FFF;
	font-size:12px;
}
</style>

<div class="row">
	<div class="form-group col-md-6 col-md-offset-3">
<?php
$branch_bal = Qry($conn,"SELECT username,balance,balance2 FROM user where username='$branch'");
if(!$branch_bal)
{
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
			
   echo "<table border='1' style='width:100%;font-family:Verdana;background:#FFF;color:#000;font-size:12px'>";
    echo "<tr>";
    echo "<th style='padding:10px;'>Branch </th>";
    echo "<th style='padding:10px;'>RRPL Cash Bal.</th>";
	echo "<th style='padding:10px;'>RR Cash Bal.</th>";
	echo "</tr>";
    while($row_bal = fetchArray($branch_bal)){
    echo "<tr>";
	echo "<td style='padding:10px;'>" . $row_bal['username'] . "</td>";
    echo "<td style='padding:10px;'>&#8377; " . $row_bal['balance'] . "</td>";
    echo "<td style='padding:10px;'>&#8377; " . $row_bal['balance2'] . "</td>";
   echo "</tr>";
    }
    echo "</table>";
?>
	</div>

</div>	
 
<div class="row">
	<div class="form-group col-md-3">
	<form autocomplete="off" method="post" action="smemo2.php" target="_blank">	
		<label id="lbl">Search FM/OLR/MKT Bilty </label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" name="value1" class="form-control" required />
		<input type="hidden" name="key" value="SEARCH" />
		<input type="submit" name="submit" class="btn btn-warning btn-sm" style="color:#000;margin-top:5px;" /> 
	</form>	
	</div>
	
	<div class="form-group col-md-3">
		<label id="lbl">Search Truck Vou</label>
		<form autocomplete="off" method="post" action="showvou.php" target="_blank">		
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" name="vou_no" class="form-control" required />
		<input value="Truck_Voucher" type="hidden" name="voutype" required />
		<input type="submit" name="submit" class="btn btn-warning btn-sm" style="color:#000;margin-top:5px;" /> 
		</form>
	</div>
	
	<div class="form-group col-md-3">
		<label id="lbl">Search Exp Vou</label>
		<form autocomplete="off" method="post" action="showvou.php" target="_blank">		
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" name="vou_no" class="form-control" required />
		<input value="Expense_Voucher" type="hidden" name="voutype" required />
		<input type="submit" name="submit" class="btn btn-warning btn-sm" style="color:#000;margin-top:5px;" /> 
		</form>
	</div>
	
	<div class="form-group col-md-3">
	<form autocomplete="off" method="post" action="tdv_by_tno.php" target="_blank">
		<label id="lbl">Truck Vou by Truck No</label>
		<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tdv_tno" id="truck_no" class="form-control" required />
		<input type="submit" id="button_tdv" name="submit" class="btn btn-warning btn-sm" style="color:#000;margin-top:5px;" /> 
	</form>	
	</div>
</div> 

</div>
</div>

<div id="pod_alert_modal" class="modal fade" role="dialog" style="background:#299C9B" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-warning">
		Alert !!!
      </div>

	  
	<div class="modal-body">
	  <div class="row">
			<div class="col-md-12">
				<h4>From 1st November 2020 Charges for Late POD </h4>
				<h5>After 30 days : 50 rs per day</h5>
				<h5>After 60 days : 100 rs per day</h5>
				<h5>No  payment after 90 days will be entertained. In any case.</h5> 
				<h5>50 rs will be deducted for balance payment. </h5>
				<br />
				<h5 style="color:red">Please inform to all brokers, truck owners.</h5>
			</div>
      </div>
      </div>
	  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
	</div>
</div>

<?php 
include("./_modal_attd.php");

if($set_value==1 AND $_SESSION['user_code']!='032' AND $_SESSION['user']!='HEAD' AND $_SESSION['user']!='PANIPAT')
{
	if($_SESSION['user']=='CGROAD')
	{
		if($_SESSION['user_code']=='016')
		{
		?>
		<script>document.getElementById("modal_link").click();</script>
		<?php		
		}
	}
	else
	{
	?>
	<script>document.getElementById("modal_link").click();</script>
	<?php
	}
}

include("./_footer.php");

include("./_modal_manager_login.php");

if(date("Y-m-d")=="2020-10-31")
{
?>
<script>
document.getElementById("modal_pod_alert").click(); 
</script>
<?php
}
?>