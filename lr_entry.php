<?php
require_once 'connection.php';

$check_lr_age = Qry($conn,"SELECT id FROM lr_sample WHERE crossing='' AND branch='$branch' AND cancel!='1' AND timestamp < DATE_SUB(NOW(), INTERVAL 10 DAY)");
if(!$check_lr_age){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr_age)>0)
{
	$lock_lr="1";
}
else
{
	$lock_lr="0";
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$lr_min_date_qry = Qry($conn,"SELECT lr FROM date_lock WHERE branch='$branch'");
if(!$lr_min_date_qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($lr_min_date_qry)>0)
{
	$row_date = fetchArray($lr_min_date_qry);
	$min_lr_date=$row_date['lr']." day";
}
else
{
	$min_lr_date="-2 day";	
}

$min_lr_date = date("Y-m-d", strtotime("$min_lr_date"));
$results_per_page=20;

$check_open_lr = Qry($conn,"SELECT party_id FROM open_lr");
if(!$check_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_open_lr)>0)
{
	$array_party=array();
	
	while($row_open_lr = fetchArray($check_open_lr))
	{
		$array_party[] = '"'.$row_open_lr['party_id'].'"';
	}
	
	$open_lr_party_id = implode(",",$array_party);
}
else
{
	$open_lr_party_id = '"0"';
}
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="help/tphead.js" type="text/javascript"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="./google_font.css" rel="stylesheet">

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#LRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./lr_entry_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_lr").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div id="result_lr"></div>

<script>
$(function() {  
      $("#item1").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/lr_items.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#item1').val(ui.item.value);   
               $('#item_id').val(ui.item.id);
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#item1").val('');
				$("#item_id").val('');
                alert('Item does not exist !'); 
              } 
              },
			}); 
}); 


$(function() {  
      $("#item1_edit").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/lr_items.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#item1_edit').val(ui.item.value);   
               $('#item_id_edit').val(ui.item.id);
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#item1_edit").val('');
				$("#item_id_edit").val('');
                alert('Item does not exist !'); 
              } 
              },
			}); 
}); 

$(function() {  
      $("#consignor").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignor').val(ui.item.value);   
               $('#consignor_id').val(ui.item.id);     
               $('#con1_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignor").val('');
				$("#consignor_id").val('');
				$("#con1_gst").val('');
                alert('Consignor does not exist !'); 
              } 
              },
			}); 
});  

$(function() {  
      $("#consignee").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee').val(ui.item.value);   
               $('#consignee_id').val(ui.item.id);     
               $('#con2_gst').val(ui.item.gst);     
               $('#consignee_pincode').val(ui.item.pincode);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee").val('');
				$("#consignee_id").val('');
				$("#con2_gst").val('');
				$("#consignee_pincode").val('');
                alert('Consignee does not exist !'); 
              } 
              },
			}); 
});

$(function() {  
      $("#consignee_edit").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee_edit').val(ui.item.value);   
               $('#consignee_id_edit').val(ui.item.id);     
               $('#con2_gst_edit').val(ui.item.gst);     
               $('#consignee_pincode').val(ui.item.pincode);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee_edit").val('');
				$("#consignee_id_edit").val('');
				$("#con2_gst_edit").val('');
				$("#consignee_pincode").val('');
                alert('Consignee does not exist !'); 
              } 
              },
			}); 
});

$(function() {
		$("#tno_market").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_market').val(ui.item.value);   
            $('#wheeler_market_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_market').val("");   
			$('#wheeler_market_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#tno_market_edit").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_market_edit').val(ui.item.value);   
            $('#wheeler_market_truck_edit').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_market_edit').val("");   
			$('#wheeler_market_truck_edit').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#tno_own").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_own').val(ui.item.value);   
            $('#wheeler_own_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_own').val("");   
			$('#wheeler_own_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});	

$(function() {
		$("#tno_own_edit").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_own_edit').val(ui.item.value);   
            $('#wheeler_own_truck_edit').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_own_edit').val("");   
			$('#wheeler_own_truck_edit').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script type="text/javascript">
$(function() {
		$("#from").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from').val("");   
			$('#from_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to').val("");   
			$('#to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#dest_zone").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#dest_zone').val(ui.item.value);   
            $('#dest_zone_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#dest_zone').val("");   
			$('#dest_zone_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});



$(function() {
		$("#from_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#from_edit').val(ui.item.value);   
            $('#from_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_edit').val("");   
			$('#from_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to_edit').val(ui.item.value);   
            $('#to_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_edit').val("");   
			$('#to_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#dest_zone_edit").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#dest_zone_edit').val(ui.item.value);   
            $('#dest_zone_id_edit').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#dest_zone_edit').val("");   
			$('#dest_zone_id_edit').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<style>
 .ui-autocomplete { z-index:2147483647; }
</style>

<style>
  .form-control
  {
	  border:1px solid #000;
	 text-transform:uppercase;
  }
</style>

<style> 
/*
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
*/
#active{
	font-weight:bold;
	font-size:16px;
}

#table_row:hover{
	background:#eee;
}
</style> 
 
<script> 
$(document).ready(function() {
    var table = $('#example').DataTable( {
        "scrollY": "600px",
        "paging": false
    } );
 
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );
} ); 
</script> 
</head>

<body style="background:lightblue;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<br />

<div class="container-fluid">

<div class="row">
	
	<div class="form-group col-md-4">
		<a href="./"><button class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
	</div>
	
	<div class="form-group col-md-4">
        <center>
			<h4 style="letter-spacing:1px;">LR/Dispatch - Entry</h4>
		</center>
	</div>

	<div class="form-group col-md-4">
		<button <?php if($lock_lr=='1') {echo "disabled";} ?> class="btn btn-success pull-right" data-toggle="modal" data-target="#add_new_record_modal">
		<span class="glyphicon glyphicon-pencil"></span> <strong>CREATE NEW LR</strong></button>
	</div>		
	
</div>	

<?php
if($lock_lr=='1')
{
	echo "<div class='row' style='background:#FFF'>
		<div class='form-group col-md-12'>
			<h5 style='color:red'>Some of your LRs are older than 10 Days and not Created any Voucher against them. So You Can't Create NEW LR. Contact Head-Office or Clear them first.</h5>
		</div>
	</div>
<br />	
";
}	
?>

<div class="row">	
<?php
if(isset($_POST["page"]))
{
	$page=$_POST["page"];
}
else
{
	$page=1;
} 

$start_from=($page-1) * $results_per_page;

$total_lrs = Qry($conn,"SELECT COUNT(id) AS total FROM lr_sample WHERE crossing='' AND branch='$branch' AND cancel!='1'");
if(!$total_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_fetch_total_lr = fetchArray($total_lrs);
$total_pages = ceil($row_fetch_total_lr["total"] / $results_per_page);

echo "<div class='col-md-12'>
		<span>Pages - </span>
		<table style='margin-top:5px;'>";
for($i=1; $i<=$total_pages; $i++) {  
            echo "
			<td>
			<form action='lr_entry.php' method='POST'>
			<input type='hidden' name='page' value='$i'>
			<button type='submit' class='btn btn-xs btn-default' style='margin-left:3px;color:#000'";
			if ($i==$page)  echo " id='active'";
            echo ">".$i."</button></form></td>"; 
}; 
echo "</table>";
?>
</div>

<div class="form-group col-md-12 table-responsive">

<table class="table table-bordered" style="border:0;background:#FFF;width:100%;font-size:11px;color:#000">								   
	<thead class="bg-primary" style="font-size:13px;color:#FFF">
	<tr>
		<th>LR_No</th>
		<th>VehicleType</th>
		<th>Truck No</th>
		<th>Date</th>
		<th>From</th>
		<th>To</th>
		<th>Consignor & Consignee</th>
		<th>DoNo/InvNo/ShipNo</th>
		<th>Awt</th>
		<th>Cwt</th>
		<th>Item</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
<?php 
$get_lrs = Qry($conn,"SELECT id,lrno,lr_type,date,truck_no,fstation,tstation,consignor,consignee,do_no,invno,shipno,wt12,weight,item 
FROM lr_sample where crossing='' AND branch='$branch' AND cancel!='1' ORDER BY date ASC LIMIT $start_from,$results_per_page");
if(!$get_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lrs)>0)
{
	while($row_lr = fetchArray($get_lrs))
    { 
		$lr_date = date('d/m/y', strtotime($row_lr['date']));

	if($row_lr['lr_type']=='MARKET')
	{
		$lr_type="<span style='color:blue'>$row_lr[lr_type]</span>";
	}
	else if($row_lr['lr_type']=='OWN')
	{
		$lr_type="<span style='color:brown'>$row_lr[lr_type]</span>";
	}
	else
	{
		$lr_type="<span style='color:red'>NOT DEFINED</span>";
	}
	
	echo '
	<tr id="table_row">
				<td>
				<form action="e_lr2.php" method="POST" target="_blank">
				<input type="hidden" name="by" value="LR">
				<input type="hidden" name="lrno" value="'.$row_lr["lrno"].'">
				<a href="#" onclick="this.parentNode.submit();" style="color:blue;font-weight:bold">'.$row_lr['lrno'].'</a>
				</form>
				</td>
				<td>'.$lr_type.'</td>
				<td>'.$row_lr['truck_no'].'</td>
				<td>'.$lr_date.'</td>
				<td>'.$row_lr['fstation'].'</td>
				<td>'.$row_lr['tstation'].'</td>
				<td>Consignor: '.$row_lr['consignor'].'<br>Consignee: '.$row_lr['consignee'].'</td>
				<td>DoNo: '.$row_lr['do_no'].'<br>InvNo: '.$row_lr['invno'].'<br>ShipNo: '.$row_lr['shipno'].'</td>
				<td>'.$row_lr['wt12'].'</td>
				<td>'.$row_lr['weight'].'</td>
				<td>'.$row_lr['item'].'</td>
				<td>
<button type="button" id="edit_button'.$row_lr["id"].'" onclick="EditLR('.$row_lr["id"].')" style="color:blue;font-weight:bold" class="btn btn-xs btn-default">Edit</button>
	<br />
<button type="button" id="delete_button'.$row_lr["id"].'" onclick="DeleteLR('.$row_lr["id"].')" style="margin-top:10px;color:red;font-weight:bold;" class="btn btn-xs btn-default">Delete</button>
				</form>
				</td>
			</tr>
                ';
    	//	$number++;
    	}
}
else
{
	echo '<tr><td colspan=""><b>Records not found..!</b></td></tr>';
}
echo " </tbody></table>
</div>
</div>";

if($branch=='DHULE')
{
	$fetch_last_lr = Qry($conn,"SELECT lrno FROM lr_check WHERE branch='DHULE' ORDER BY id DESC LIMIT 1");
	if(!$fetch_last_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
						
	$row_last_lr = fetchArray($fetch_last_lr);
	$last_lrno=++$row_last_lr['lrno'];
}
else
{
	$last_lrno="";
}
?>
</div>

<style>
label{font-size:13px;}
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>

<div id="function_result"></div>

<script type="text/javascript">
function EditLR(id)
{
	$('#edit_button'+id).attr('disabled',true);
	$('#lr_edit_id').val(id);
	$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_lr_details_for_edit.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#function_result").html(data);
	},
	error: function() {}
});
}

function DeleteLR(id)
{
	// alert(id);
	$("#loadicon").show();
		jQuery.ajax({
		url: "delete_lr.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#function_result").html(data);
		},
		error: function() {}
	});
}
</script>

<input type="hidden" id="api_inv_no">
<input type="hidden" id="api_con1">
<input type="hidden" id="api_con2">
<input type="hidden" id="api_from">
<input type="hidden" id="api_to">

</body>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>  
<link href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" rel="stylesheet">
</html>

<script>
$('#window_loadicon').fadeOut();
</script>

<?php include("./modal_create_lr.php"); ?>
<?php include("./modal_edit_lr.php"); ?>