<?php
require("./connection.php");

$timestamp = date("Y-m-d H:i:s");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$frno1 = escapeString($conn,strtoupper($_POST['fm_no']));

$frno = explode("_",$frno1)[0];
$lr_date = explode("_",$frno1)[1];
$create_date_fm = explode("_",$frno1)[2];
$create_date_fm2 = date("d-m-Y",strtotime($create_date_fm));
$narration = escapeString($conn,($_POST['narration']));

$get_balance_max_val = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='BALANCE_LOCK' AND is_active='1'");

if(!$get_balance_max_val){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit(); 
}

if(numRows($get_balance_max_val)==0)
{
	$max_balance_days = 60;
}
else
{
	$row_bal_lock_val = fetchArray($get_balance_max_val);
	$max_balance_days = $row_bal_lock_val['func_value'];
}

$diff_days = round((strtotime(date("Y-m-d"))-strtotime($create_date_fm)) / (60 * 60 * 24));

if($diff_days<=$max_balance_days)
{
	echo "<script>
		alert('FM is not $max_balance_days days older. FM date is: $create_date_fm2 !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$chk_req = Qry($conn,"SELECT id FROM extend_bal_pending_validity WHERE frno='$frno'");

if(!$chk_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_req)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#loadicon').fadeOut('slow');
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$insert_req = Qry($conn,"INSERT INTO extend_bal_pending_validity(lrno,frno,lr_date,narration,branch,branch_user,timestamp) VALUES 
('$lrno','$frno','$lr_date','$narration','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Request submitted successfully. Head-office approval required !');
		window.location.href='./unblock_balance.php';
	</script>";
	exit();
?>