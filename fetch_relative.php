<?php
require_once 'connection.php';

$code = escapeString($conn,strtoupper($_POST['code']));

if($code=='NO')
{
	echo "<script>
		$('#relative_address').val('');
		$('#relative_mobile').val('');
		$('#emp_edit_save_btn').attr('disabled', false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	$fetch_emp = Qry($conn,"SELECT currentaddr,mobile_no FROM emp_attendance WHERE code='$code'");

	if(!$fetch_emp){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($fetch_emp)==0)
	{
		echo "<script>
			alert('Employee not found.');
			$('#relativeid').val('');
			$('#emp_edit_save_btn').attr('disabled', true);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}

	$row = fetchArray($fetch_emp);

	echo "<script>
		$('#relative_address').val('$row[currentaddr]');
		$('#relative_mobile').val('$row[mobile_no]');
		$('#emp_edit_save_btn').attr('disabled', false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>