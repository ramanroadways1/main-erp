<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$lrno = escapeString($conn,$_POST['lrno']);
$id = escapeString($conn,$_POST['table_id']);
$to_id = escapeString($conn,$_POST['dest_location_id']);
$to_loc = escapeString($conn,$_POST['dest_location']);

echo "<script>$('#create_vou_btn_modal').attr('disabled',true);</script>";

$chk_location = Qry($conn,"SELECT name FROM station WHERE id='$to_id'");

if(!$chk_location){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_location)==0)
{
	echo "<script>
		alert('Location not found !');
		$('#loadicon').fadeOut('slow');
		$('#create_vou_btn_modal').attr('disabled',false);
	</script>";
	exit();
}

$row_location = fetchArray($chk_location);

if($row_location['name']!=$to_loc)
{
	echo "<script>
		alert('Error : Invalid destination !');
		$('#loadicon').fadeOut('slow');
		$('#create_vou_btn_modal').attr('disabled',false);
	</script>";
	exit();
}

$check_record = Qry($conn,"SELECT o.veh_no,o.frno,o.vou_status,o.lrno,o.trip_id,o.from_id_main,o.to_id_main,o.lr_id,o.from_id,
o.to_loc as to_loc_ext,o.to_id as to_id_ext,l.date as lr_date,l.tstation,l.to_id,l.consignor,l.consignee,l.con1_id,l.con2_id,
l.wt12,l.weight 
FROM oxygen_olr AS o 
LEFT JOIN lr_sample AS l ON l.id = o.lr_id 
WHERE o.id='$id'");

if(!$check_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_record)==0)
{
	echo "<script>
		alert('No record found !');
		$('#loadicon').fadeOut('slow');
		$('#create_vou_btn_modal').attr('disabled',false);
	</script>";
	exit();
}

$row = fetchArray($check_record);

$vou_no = $row['frno'];
// $from_id = $row['from_id'];
$to_id_db = $row['to_id'];
$to_loc_ext = $row['to_loc_ext'];
$to_loc_ext_id = $row['to_id_ext'];
$lr_date = $row['lr_date'];
$veh_no = $row['veh_no'];
$consignor = $row['consignor'];
$consignee = $row['consignee'];
$con1_id = $row['con1_id'];
$con2_id = $row['con2_id'];
$act_wt = $row['wt12'];
$chrg_wt = $row['weight'];
$from_id_main = $row['from_id_main'];
$to_id_main = $row['to_id_main'];
$lr_id = $row['lr_id'];

if($to_loc_ext_id==$to_id)
{
	echo "<script>
		alert('Error : Check destination !');
		$('#loadicon').fadeOut('slow');
		$('#create_vou_btn_modal').attr('disabled',false);
	</script>";
	exit();
}

// if($to_id_db==$to_id)
// {
	// $close_voucher="YES";
// }
// else
// {
	$close_voucher="NO";
// }

if($row['lrno']!=$lrno)
{
	echo "<script>
		alert('Error : LR number not verified !');
		$('#loadicon').fadeOut('slow');
		$('#create_vou_btn_modal').attr('disabled',false);
	</script>";
	exit();
}

if($row['vou_status']=='1')
{
	echo "<script>
		alert('Error : Voucher is not active !');
		$('#loadicon').fadeOut('slow');
		// $('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$get_last_record = Qry($conn,"SELECT id,count FROM oxygen_olr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");

if(!$get_last_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row2 = fetchArray($get_last_record);

if($row2['id']!=$id)
{
	echo "<script>
		alert('Error : Please select last LR record to create a voucher !');
		$('#loadicon').fadeOut('slow');
		$('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row2['count']>=3)
{
	echo "<script>
		alert('Error : Voucher closed already !');
		$('#loadicon').fadeOut('slow');
		// $('#mark_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$vou_count_new = $row2['count']+1;
	
StartCommit($conn);
$flag = true;

$branch2=strtoupper(substr($branch,0,3))."OLR".date("dmY");
$ramanc=Qry($conn,"SELECT frno FROM freight_form_lr where frno like '$branch2%' ORDER BY id DESC LIMIT 1");

if(!$ramanc){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$row=fetchArray($ramanc);

if(numRows($ramanc)>0)
{
	$newstring = substr($row['frno'],14);
	$newstr = ++$newstring;
	$new_vou_no = $branch2.$newstr;
}
else
{
	$new_vou_no = $branch2."1";
}
		
$create_voucher = Qry($conn,"INSERT INTO freight_form_lr(frno,company,branch,branch_user,date,create_date,truck_no,lrno,fstation,
tstation,consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,crossing,oxygen_lr,timestamp) VALUES ('$new_vou_no','RRPL',
'$branch','$_SESSION[user_code]','$lr_date','$date','$veh_no','$lrno','$to_loc_ext','$to_loc','$consignor','$consignee','$to_loc_ext_id',
'$to_id','$con1_id','$con2_id','$act_wt','$chrg_wt','NO','1','$timestamp')");

if(!$create_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($close_voucher=="YES")
{
	$close_voucher_Qry = Qry($conn,"INSERT INTO oxygen_olr(veh_no,frno,lrno,from_loc,to_loc,from_id,to_id,from_id_main,to_id_main,
	count,lr_id,branch,branch_user,timestamp) VALUES ('$veh_no','$new_vou_no','$lrno','$to_loc_ext','$to_loc','$to_loc_ext_id','$to_id',
	'$from_id_main','$to_id_main','3','$lr_id','$branch','$_SESSION[user_code]','$timestamp')");
	
	if(!$close_voucher_Qry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$close_voucher_Qry2 = Qry($conn,"UPDATE oxygen_olr SET close_user='$_SESSION[user_code]',close_timestamp='$timestamp',vou_status='1',
	running_trip='1' WHERE lrno='$lrno'");
	
	if(!$close_voucher_Qry2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$insert_vou_2 = Qry($conn,"INSERT INTO oxygen_olr(veh_no,frno,lrno,from_loc,to_loc,from_id,to_id,from_id_main,to_id_main,
	count,lr_id,branch,branch_user,timestamp) VALUES ('$veh_no','$new_vou_no','$lrno','$to_loc_ext','$to_loc','$to_loc_ext_id','$to_id',
	'$from_id_main','$to_id_main','$vou_count_new','$lr_id','$branch','$_SESSION[user_code]','$timestamp')");
	
	if(!$insert_vou_2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$new_vou_id = getInsertID($conn);
	
	if($vou_count_new>=3)
	{
		$close_prev_trips = Qry($conn,"UPDATE oxygen_olr SET close_user='$_SESSION[user_code]',close_timestamp='$timestamp',vou_status='1',
		running_trip='1' WHERE lrno='$lrno'");
		
		if(!$close_prev_trips){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	} 
	else
	{
		$close_prev_trips = Qry($conn,"UPDATE oxygen_olr SET running_trip='1' WHERE lrno='$lrno' AND id!='$new_vou_id'");
		
		if(!$close_prev_trips){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('OK : Success.');
		window.location.href='./oxygen_movement.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#mark_button_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>