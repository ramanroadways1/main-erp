<?php
require_once './connection.php';

// if($branch!='CGROAD')
// {
	// echo "<script>
		// alert('Please Wait.');
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

$gst = escapeString($conn,strtoupper($_POST['gst']));
$type = escapeString($conn,($_POST['type']));

$chkGst = Qry($conn,"SELECT name FROM `$type` WHERE gst='$gst'");
if(!$chkGst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

// if(numRows($chkGst)>0 AND $branch!='CGROAD')
if(numRows($chkGst)>0)
{
	$row_find = fetchArray($chkGst);
	
	echo "<script>
		alert('Duplicate GST Number : $gst. Registered with : $row_find[name] !');
		$('#party_name_$type').val('');
		$('#party_name_legal_$type').val('');
		$('#party_pan_$type').val('');
		$('#gst_no_$type').attr('readonly',false);
		$('#ValidateBtn_$type').attr('disabled',false);
		$('#buttonPartyADD_$type').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$gst_state = substr($gst,0,2);

$get_state = Qry($conn,"SELECT name FROM state_codes WHERE code='$gst_state'");
if(!$get_state){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

$row_state = fetchArray($get_state);
$state_name = $row_state['name']; 
			
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => ''.$url_verify_gst_common_api.'/commonapi/v1.1/search?aspid='.$tax_pro_asp_id.'&password='.$tax_pro_asp_password.'&Action=TP&Gstin='.$gst.'',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);
$result = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
		
		if($err)
		{
			echo "<script>
				alert('Unable to Get GST Details from Server.');
					$('#party_name_$type').val('');
					$('#party_name_legal_$type').val('');
					$('#party_pan_$type').val('');
					$('#gst_no_$type').attr('readonly',false);
					$('#ValidateBtn_$type').attr('disabled',false);
					$('#buttonPartyADD_$type').attr('disabled',true);
					$('#loadicon').hide();
			</script>";
			exit();
		}
		else
		{
			$response = json_decode($result, true);
			
			if(@$response['error'])
			{
				$errMsg = $response['error']['message'];
				echo "<script>
					alert('Error : $errMsg.');
					$('#party_name_$type').val('');
					$('#party_name_legal_$type').val('');
					$('#party_pan_$type').val('');
					$('#state_name_$type').val('');
					$('#gst_no_$type').attr('readonly',false);
					$('#ValidateBtn_$type').attr('disabled',false);
					$('#buttonPartyADD_$type').attr('disabled',true);
					$('#loadicon').hide();
				</script>";
				exit();
			}
			else
			{
				$LegalName = $response['lgnm'];
				$trade_name = $response['tradeNam'];
				$Pan_No = substr($gst,2,-3);
				
				if($LegalName=='' AND $trade_name=='')
				{
					// echo $result;
					
					echo "<script>
						alert('Party not found !');
						$('#party_name_$type').val('');
						$('#party_name_legal_$type').val('');
						$('#party_pan_$type').val('');
						$('#state_name_$type').val('');
						$('#gst_no_$type').attr('readonly',false);
						$('#ValidateBtn_$type').attr('disabled',false);
						$('#buttonPartyADD_$type').attr('disabled',true);
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if($response['sts']!='Active')
				{
					echo "<script>
						alert('Error : GST status is: $response[sts] !!');
						$('#party_name_$type').val('');
						$('#party_name_legal_$type').val('');
						$('#party_pan_$type').val('');
						$('#state_name_$type').val('');
						$('#gst_no_$type').attr('readonly',false);
						$('#ValidateBtn_$type').attr('disabled',false);
						$('#buttonPartyADD_$type').attr('disabled',true);
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if($trade_name=='')
				{
					$trade_name = $LegalName;
				}
				
				if($LegalName=='')
				{
					$LegalName = $trade_name;
				}
			}
			
			echo "<script>
				$('#party_trade_$type').val('$trade_name');
				$('#party_legal_$type').val('$LegalName');
				$('#party_trade_$type').html('$trade_name');
				$('#party_legal_$type').html('$LegalName');
				$('#party_trade_$type').show();
				$('#party_legal_$type').show();
				$('#party_name_$type').val('$trade_name');
				$('#party_name_legal_$type').val('$LegalName');
				$('#party_pan_$type').val('$Pan_No');
				$('#state_name_$type').val('$state_name');
				$('#gst_no_$type').attr('readonly',true);
				$('#ValidateBtn_$type').attr('disabled',true);
				$('#buttonPartyADD_$type').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}	
?>