<?php
require_once '../../connection.php';

$date_today = date("Y-m-d");

$sql ="SELECT f.id,f.vou_no,f.branch,DATE_FORMAT(f.lr_date,'%d-%m-%y') as lr_date1,DATE_FORMAT(f.create_date,'%d-%m-%y') as create_date,
f.lrno,o.tno,o.name as owner_name,b.name as broker_name FROM _pending_lr_list f 
LEFT OUTER JOIN mk_truck AS o ON o.id=f.oid
LEFT OUTER JOIN mk_broker AS b ON b.id=f.bid
WHERE DATEDIFF('$date_today',f.lr_date)>30 AND f.branch='$branch' ORDER BY f.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'vou_no', 'dt' => 1),
    array( 'db' => 'tno', 'dt' => 2),
    array( 'db' => 'owner_name', 'dt' => 3),
    array( 'db' => 'broker_name', 'dt' => 4),
    array( 'db' => 'lrno', 'dt' => 5),
    array( 'db' => 'branch', 'dt' => 6),
    array( 'db' => 'lr_date1', 'dt' => 7), 
    array( 'db' => 'create_date', 'dt' => 8), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../../scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);