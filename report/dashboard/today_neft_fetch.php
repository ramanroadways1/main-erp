<?php
require_once '../../connection.php';

$date1 = date("Y-m-d");

$sql ="SELECT id,fno,fm_date,pay_date,com,amount,acname,acno,ifsc,pan,type,tno,crn FROM rtgs_fm WHERE pay_date='$date1' AND 
branch='$branch'";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'fno', 'dt' => 1),
    array( 'db' => 'fm_date', 'dt' => 2),
    array( 'db' => 'com', 'dt' => 3),
    array( 'db' => 'tno', 'dt' => 4),
    array( 'db' => 'amount', 'dt' => 5),
    array( 'db' => 'acname', 'dt' => 6), 
    array( 'db' => 'acno', 'dt' => 7), 
    array( 'db' => 'ifsc', 'dt' => 8), 
    array( 'db' => 'pan', 'dt' => 9), 
    array( 'db' => 'type', 'dt' => 10), 
    array( 'db' => 'crn', 'dt' => 11), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../../scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);