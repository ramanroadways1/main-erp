<?php
require_once '../../connection.php';
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
<html>

<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../../favicon.png" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../../google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../../data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 
</style>

<?php
include("../../_loadicon.php");
include("../../disable_right_click_for_index.php");
?>

<style>
label{
	font-size:13px;
	text-transform:none;
}
@media (min-width: 768px) {
  .modal-xl {
    width: 95%;
   max-width:100%;
  }
}
</style>

<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="../../"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">LR > 30 days :</span></h5>
		</div>
	</div>	
</div>

	<div class="form-group col-md-12" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>Id</th>
					<th>Vou_No</th>
					<th>Vehicle_No</th>
					<th>Owner_Name</th>
					<th>Broker_Name</th>
					<th>LR_Number</th>
					<th>Branch</th>
					<th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>
					<th>Create_Date <span class="glyphicon glyphicon-filter"></span></th>
				</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function FetchLRs(){
$("#loadicon").show();
// $("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../../load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 // {
        // targets: 12, // edit column order
        // data: null,
        // mRender: function(data, type, full){
         // return '<button type="button" class="btn-xs btn-primary" onclick="EditButton('+full[0]+')" id="edit_button_'+full[0]+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>';
        // }
      // },
      // {
		// targets: 13, // edit column order
        // data: null,
        // mRender: function(data, type, full){
         // return '<button type="button" class="btn-xs btn-danger" onclick="DeleteButton('+full[0]+')" id="delete_button_'+full[0]+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>';
        // }
      // },
	  // {
		// targets: 14, // edit column order
        // data: null,
        // mRender: function(data, type, full){
         // return '<input type="hidden" id="lrno_input_'+full[0]+'" value="'+full[1]+'"><button type="button" class="btn-xs btn-warning" onclick="PrintLR('+full[0]+')" id="print_button_'+full[0]+'"><i class="fa fa-print" aria-hidden="true"></i> Print</button>';
        // }
      // }
		], 
        "serverSide": true,
        "ajax": "fetch_lr_30_days.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchLRs();
</script>