<?php
require_once '../../connection.php';

$sql ="SELECT f.id,f.frno,f.truck_no,GROUP_CONCAT(f2.lrno SEPARATOR ',') as lrno,f.from1,f.to1,f.weight,
DATE_FORMAT(f.date,'%d-%m-%y') as fm_date,f.actualf,f.totaladv,
DATE_FORMAT(f.adv_date,'%d-%m-%y') as adv_date,b.name as broker,o.name as owner FROM freight_form AS f 
LEFT OUTER JOIN mk_broker as b ON b.id=f.bid 
LEFT OUTER JOIN mk_truck as o ON o.id=f.oid 
LEFT OUTER JOIN freight_form_lr as f2 ON f2.frno=f.frno 
WHERE f.date>='2019-01-01' AND f.branch='$branch' AND f.paidto='' GROUP BY f.frno ORDER BY f.id ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'frno', 'dt' => 1),
    array( 'db' => 'truck_no', 'dt' => 2),
    array( 'db' => 'lrno', 'dt' => 3),
    array( 'db' => 'from1', 'dt' => 4),
    array( 'db' => 'to1', 'dt' => 5),
    array( 'db' => 'weight', 'dt' =>  6), 
    array( 'db' => 'fm_date', 'dt' => 7), 
    array( 'db' => 'broker', 'dt' => 8), 
    array( 'db' => 'owner', 'dt' => 9), 
    array( 'db' => 'actualf', 'dt' => 10), 
    array( 'db' => 'totaladv', 'dt' => 11), 
    array( 'db' => 'adv_date', 'dt' => 12), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../../scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);