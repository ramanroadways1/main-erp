<?php
require_once("./connection.php");

$api_url = $aadhar_api_url;
$token = $aadhar_api_token;

// function clean_input($data) {
  // return  htmlspecialchars(stripslashes(trim($data)));
// }

$id_number = strtoupper($_POST['lic_no']);

if(empty($id_number))
{
	echo "<script>
		alert('Enter license number first !');
		$('#loadicon').fadeOut('slow');
		$('#get_lic_details_btn').attr('disabled',false);
	</script>";
	exit();
}

$chk_driver = Qry($conn,"SELECT name FROM dairy.driver WHERE lic='$id_number'");

if(!$chk_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}
	
if(numRows($chk_driver) > 0)
{
	$row_d = fetchArray($chk_driver);
	
	echo '<script>
		alert("********** Duplicate license number found ! **********\nAlready registered with driver : '.$row_d["name"].'");
		$("#loadicon").fadeOut("slow");
		$("#get_lic_details_btn").attr("disabled",false);
	</script>';
	exit();
}

// $lic_no_new = str_replace("  "," ",trim($result['data']['name']));

$data = array(
"id_number"=>"$id_number",
);

$payload = json_encode($data);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "$api_url/api/v1/driving-license/driving-license",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $payload,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
	'Content-Length: ' . strlen($payload),
	'Authorization: Bearer '.$token.''
  ),
));

$response = curl_exec($curl);
// echo $response;
curl_close($curl);

$result = json_decode($response, true);

if($result['status_code']!='200')
{
	$error_msg = clean_input($result['message']);
	$error_code = $result['status_code'];
	
	echo "<font color='red'>$error_code : $error_msg.</font>
	<script>
		$('#loadicon').fadeOut('slow');
		$('#get_lic_details_btn').attr('disabled',false);
	</script>";
	exit();
}

// $b64 = $row['img'];
		// $bin = base64_decode($b64);
		// @$im = imageCreateFromString($bin);
		// if (!$im) {
		  // die('Base64 value is not a valid image');
		// }
		// $img_file = 'images/'.$dl_no.uniqid().'.png';
		// imagepng($im, $img_file, 0);

$client_id = $result['data']['client_id'];	
$state = $result['data']['state'];	
$name = str_replace("  "," ",trim($result['data']['name']));
$permanent_address = trim($result['data']['permanent_address']);
$permanent_zip = $result['data']['permanent_zip'];
$temporary_address = trim($result['data']['temporary_address']);
$temporary_zip = $result['data']['temporary_zip'];
$citizenship = $result['data']['citizenship'];
$ola_name = $result['data']['ola_name'];
$ola_code = $result['data']['ola_code'];
$gender = $result['data']['gender'];
$father_or_husband_name =  str_replace("  "," ",trim($result['data']['father_or_husband_name']));
$dob = $result['data']['dob'];
$doe = $result['data']['doe'];
$transport_doe = $result['data']['transport_doe'];
$transport_doi = $result['data']['transport_doi'];
$doi = $result['data']['doi'];
$blood_group = $result['data']['blood_group'];
$vehicle_classes = $result['data']['vehicle_classes'];
$image = $result['data']['profile_image']; 

$full_name = $name." $father_or_husband_name";

$veh_class_array = array();
$transport_lic = "NO";

foreach ($vehicle_classes as $key => $item) {
	foreach(explode(" ",$item) as $item_new)
	{
		$veh_class_array[] = str_replace(",","",str_replace(" ","",str_replace("  ","",trim($item_new))));
	}
			
	if($item=='TRANS')
	{
		$transport_lic = "YES";
	}
	reset($vehicle_classes);
}

if(strtotime($transport_doe) <= strtotime(date("Y-m-d")))
{
	echo '<script>
		alert("Transport license validity expired !");
		$("#loadicon").fadeOut("slow");
		$("#get_lic_details_btn").attr("disabled",false);
	</script>';
	exit();
}	
// if($transport_lic!='YES')
// {
	// echo '<script>
		// alert("Given license does not belongs to transport license !");
		// $("#loadicon").fadeOut("slow");
		// $("#get_lic_details_btn").attr("disabled",false);
	// </script>';
	// exit();
// }	
		
$veh_class_array = str_replace(",,","",str_replace(" ","",str_replace("  ","",trim(implode(",",$veh_class_array)))));
?>
<div class="form-group col-md-6" style="border:1px solid #DDD;border-radius:10px">
	
	<div class="row">
		<div class="col-md-4" style="margin-top:10px;">
			<center><img class="img-responsive" src="./satyamev_jayate.png" style="height:50px"></center>
		</div>	
		
		<div class="col-md-8" style="margin-top:10px;">
			<span style="font-size:12px;font-weight:bold">UNION OF INDIA</span>
			&nbsp; <span style="font-family:Arial;font-size:13px;font-weight:bold">Driving License</span>
			<br>
			<span style="padding:20px;font-family:Arial;font-size:16px;font-weight:bold"><?php echo $id_number; ?></span>
		</div>	
	</div>
	
	<div style="margin-top:-10px;" class="row">
		<hr />
	</div>
	
	<div style="font-size:10px" class="col-md-4">
		Date of issue<br>जारी करने की तिथि
		<br>
		<?php echo $doi; ?>
		<br>
		<br>
		Date of birth<br>जन्म तिथि
		<br>
		<?php echo $dob; ?>
	</div>
	
	<div style="font-size:10px" class="col-md-4">
		Validity<br>वैधता
		<br>
		<?php echo $doe; ?>
		<br>
		<br>
		Blood group
		<br>
		<?php echo $blood_group; ?>
	</div>
	
	<div class="col-md-4">
		<img class="img-responsive pull-right" src="data:image/png;base64,<?php echo $image; ?>" style="border-radius:5px;width:100px;height:110px" />
	</div>
	

	<div style="font-size:10px" class="form-group col-md-12">
		Name / नाम
		<br>
		<?php echo $name; ?>
	</div>
	
	<div style="font-size:10px" class="form-group col-md-8">
		Son/Daughter/Wife of / पुत्र/पुत्री/पत्नी का नाम 
		<br>
		<?php echo $father_or_husband_name; ?>
	</div>
	
	<div style="font-size:10px" class="form-group col-md-4">
		Gender / लिंग
		<br>
		<?php echo $gender; ?>
	</div>
	
</div>

<div class="form-group col-md-6" style="border:1px solid #DDD;border-radius:10px">
	
	<div class="row">
		<div class="col-md-12" style="margin-top:10px;">
			<span style="padding:20px;font-family:Arial;font-size:16px;font-weight:bold"><?php echo $id_number; ?></span>
			<br />
			<span style="padding:20px;font-family:Arial;font-size:14px;font-weight:bold">
			<?php 
			foreach ($vehicle_classes as $key => $item) {
			  echo "$item\n";
				reset($vehicle_classes);
			}
			
			$addr_full = $permanent_address." ($permanent_zip)";
			$addr_full_temp = $temporary_address." ($temporary_zip)";
			?>
			</span>
		</div>	
	</div>
	
<input type="hidden" value="<?php echo $veh_class_array; ?>" name="vehicle_class">	
<input type="hidden" value="<?php echo $addr_full; ?>" name="perm_addr">	
<input type="hidden" value="<?php echo $addr_full_temp; ?>" name="temp_addr">	
<input type="hidden" value="<?php echo $state; ?>" name="state">	
<input type="hidden" value="<?php echo $ola_name; ?>" name="issued_by">	
<input type="hidden" value="<?php echo $gender; ?>" name="gender">	
<input type="hidden" value="<?php echo $father_or_husband_name; ?>" name="father_or_husband_name">	
<input type="hidden" value="<?php echo $dob; ?>" name="dob">	
<input type="hidden" value="<?php echo $doe; ?>" name="doe">	
<input type="hidden" value="<?php echo $transport_doe; ?>" name="transport_doe">	
<input type="hidden" value="<?php echo $transport_doi; ?>" name="transport_doi">	
<input type="hidden" value="<?php echo $doi; ?>" name="doi">	
<input type="hidden" value="<?php echo $blood_group; ?>" name="blood_group">	
	
	<div style="margin-top:-10px;" class="row">
		<hr />
	</div>
	
	<div style="font-size:10px" class="col-md-6">
		Permanent Address / स्थायी पता
		<br>
		<br>
		<?php echo $permanent_address; ?>
	</div>
	
	<div style="font-size:10px" class="col-md-6">
		Temporary Address / अस्थायी पता
		<br>
		<br>
		<?php echo $temporary_address; ?>
	</div>
	
	<div class="row">
		<div class="col-md-12">&nbsp;</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">&nbsp;</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">&nbsp;</div>
	</div>
	
	<div style="font-size:10px" class="form-group col-md-6">
		Transport Validity / ट्रांसपोर्ट वैधता
		<br>
		<br>
		<?php echo $transport_doe; ?>
	</div>
	
	
	<div style="font-size:10px" class="form-group col-md-6">
		Issued By / जारीकर्ता
		<br>
		<br>
		<?php echo $ola_name; ?>
	</div>
	
	<div class="row">
		<div class="col-md-12">&nbsp;</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">&nbsp;</div>
	</div>
</div>

<?php
/*
<div class="form-group col-md-4">
	<img src="data:image/png;base64,<?php echo $image; ?>" />
</div>

<div class="form-group col-md-8">
	<table class="table table-bordered table-striped" style="font-size:14px">
		<tr>
			<th>Full Name</th>
			<td><?php echo $name; ?></td>
			
			<th>Father/Husband</th>
			<td><?php echo $father_or_husband_name; ?></td>
		</tr>	
	
		<tr>
			<th>DOB</th>
			<td><?php echo $dob; ?></td>
			<th>Citizenship</th>
			<td><?php echo $citizenship; ?></td>
		</tr>
		
		<tr>
			<th>Blood Group</th>
			<td><?php echo $blood_group; ?></td>
			<th>Gender</th>
			<td><?php echo $gender; ?></td>
		</tr>
		
		<tr>
			<th>Date of Issue</th>
			<td><?php echo $doi; ?></td>
			<th>Date of Expiry</th>
			<td><?php echo $doe; ?></td>
		</tr>
		
		
		<tr>
			<th>Transport: date of issue</th>
			<td colspan="3"><?php echo $transport_doe; ?></td>
		</tr>
		
		<tr>
			<th>Vehicle Class</th>
			<td colspan="3">
			<?php 
			foreach ($vehicle_classes as $key => $item) {
			  echo "$item\n";
			  reset($vehicle_classes);
			} ?>
			</td>
		</tr>
		
		
		<tr>
			<th>DTO</th>
			<td colspan="3"><?php echo $ola_name." ($ola_code)"; ?></td>
		</tr>
		
		<tr>
			<th>Current Address</th>
			<td colspan="3"><?php echo $temporary_address." ($temporary_zip)"; ?></td>
		</tr>
		
		<tr>
			<th>Permanent Address</th>
			<td colspan="3"><?php echo $permanent_address." ($permanent_zip)"; ?></td>
		</tr>
		
	</table>
</div>

<?php
*/
?>
<script>
	$('#add_driver_button_own_truck').show();
	$('#loadicon').fadeOut('slow');
	$('#d_full_name1').val('<?php echo $full_name; ?>');
	$('#driver_addr1').val('<?php echo $addr_full; ?>');
	$('#lic_no_1').attr('readonly',true);
	$('#add_driver_button_own_truck').attr('disabled',false);
	$('#get_lic_details_btn').attr('disabled',true);
</script>
