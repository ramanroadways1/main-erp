<?php
require_once 'connection.php';

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$con1_id = escapeString($conn,strtoupper($_POST['con1_id']));
$con2_id = escapeString($conn,strtoupper($_POST['con2_id']));
$consignor = escapeString($conn,strtoupper($_POST['consignor']));
$consignee = escapeString($conn,strtoupper($_POST['consignee']));
$billing_type = escapeString($conn,($_POST['billing_type']));
$consignor_sub_id = escapeString($conn,($_POST['consignor_sub_id']));
$shipment_party_sel = escapeString($conn,($_POST['shipment_party_sel']));
$shipment_party_id = escapeString($conn,($_POST['shipment_party_id']));

if(strpos($lrno,'0')===0 AND $con1_id!='848')
{
	echo "<script>
		alert('Error: Hey you have entered invalid LR number.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		$('#loadicon').fadeOut('slow');
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
	</script>";
	exit();
}

if(empty($lrno))
{
	echo "<script>
		alert('Error: LR number not found.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		$('#loadicon').fadeOut('slow');
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
	</script>";
	exit();
}	

// if(!ctype_alnum($lrno))
if(!preg_match('/^[a-zA-Z0-9_\.]*$/', $lrno))
{
	echo "<script>
		alert('Error: LR number is not valid.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		$('#loadicon').fadeOut('slow');
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
	</script>";
	exit();
}

if(count(explode(' ', $lrno)) > 1)
{
	echo "<script>
		alert('Error: Please check LR number.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		$('#loadicon').fadeOut('slow');
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
	</script>";
	exit();
}
	
if(empty($con1_id))
{
	echo "<script>
		alert('Error: Consignor not found.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con1_id=='56' AND $consignor_sub_id=='')
{
	echo "<script>
		alert('Error: Enter Actual Consignor First !');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(empty($con2_id))
{
	echo "<script>
		alert('Error: Consignee not found.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($shipment_party_sel=='')
{
	echo "<script>
		alert('Error: Select Delivery/Shipment Party option first !');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($shipment_party_sel=='DIFF' AND empty($shipment_party_id))
{
	echo "<script>
		alert('Error: Select Delivery To/Shipment Party option first !');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con1_id=='128')
{
	$lrno = "M".$lrno;
}

// if($con1_id=='848')
// {
	// $lrno = "00".$lrno;
// }

if($con1_id=='1130')
{
	echo "<script>
		$('#unit_name_vizag').val('');
		$('#unit_name_vizag_div').show();
		$('#unit_name_vizag').attr('required', true);
	</script>";
}
else
{ 
	echo "<script>
		$('#unit_name_vizag').val('');
		$('#unit_name_vizag_div').hide();
		$('#unit_name_vizag').attr('required', false);
	</script>";
}

// if($con1_id=='56')
// {
	// echo "<script>
		// $('.qwik_party').show();
		// $('#consignor_sub').val('');
		// $('#consignor_sub_id').val('');
		// $('#consignor_sub_gst').val('');
		
		// $('#consignor_sub').attr('required', true);
		// $('#consignor_sub_id').attr('required', true);
		// $('#consignor_sub_gst').attr('required', true);
	// </script>";
// }
// else
// {
	// echo "<script>
		// $('.qwik_party').hide();
		// $('#consignor_sub').val('');
		// $('#consignor_sub_id').val('');
		// $('#consignor_sub_gst').val('');
		
		// $('#consignor_sub').attr('required', false);
		// $('#consignor_sub_id').attr('required', false);
		// $('#consignor_sub_gst').attr('required', false);
	// </script>";
// }

$fetch=Qry($conn,"SELECT id,party_id FROM open_lr WHERE party_id = '$con1_id'");

if(!$fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch)>0)
{
	if(!preg_match('/^[a-zA-Z0-9_\.]*$/', $lrno))
	{
		echo "<script>
			alert('Error : Invalid LR number !');
			$('#lrno').val('');
			$('#lrno').focus();
			$('#LRallInputs *').prop('disabled', true);
			// $('#LRallInputs').hide();
			$('#loadicon').fadeOut('slow');
				
			$('#lrno').attr('readonly',false);
			$('#lrchk_button').attr('disabled',false);
		</script>";
		exit();
	}

	$company_var="";
		
	echo "<script type='text/javascript'>
			$('#bilty_id').val('OTHERLR');
			$('#bilty_belongs_to').val('OTHERLR');
			
			$('#auto_company_div').hide();
			$('#company_auto').val('');
			$('#company_auto').attr('required',false);
			
			$('#man_company_div').show();
			$('#company_man').val('');
			$('#company_man').attr('required',true);
			
			$('#LRallInputs *').prop('disabled', false);
			// $('#LRallInputs').show();
			$('#lrno').attr('readonly',true);
			$('#lrchk_button').attr('disabled',true);
			$('#lr_sub').attr('disabled',false);
			$('#lr_sub').show();
			$('#loadicon').fadeOut('slow');
	</script>";
}
else
{
	if(!preg_match('/^[0-9\.]*$/', $lrno))
	{
		echo "<script>
			alert('Error : Invalid LR number !');
			$('#lrno').val('');
			$('#lrno').focus();
			$('#LRallInputs *').prop('disabled', true);
			// $('#LRallInputs').hide();
			$('#loadicon').fadeOut('slow');
				
			$('#lrno').attr('readonly',false);
			$('#lrchk_button').attr('disabled',false);
		</script>";
		exit();
	}
		
	$chk_bank = Qry($conn,"SELECT id,branch,company,stock_left,lr_belongs_to FROM lr_bank WHERE $lrno>=from_range AND $lrno<=to_range");

	if(!$chk_bank){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	$numRows_bilty = numRows($chk_bank);
		
	if($numRows_bilty==0)
	{
		echo "<script type='text/javascript'>
				alert('LR stock not updated. Please contact head-office.');
				$('#lrno').val('');
				$('#lrno').focus();
				
				$('#LRallInputs *').prop('disabled', true);
				// $('#LRallInputs').hide();
				$('#lrno').attr('readonly',false);
				$('#lrchk_button').attr('disabled',false);
				$('#lr_sub').attr('disabled',true);
				$('#lr_sub').hide();
				$('#loadicon').fadeOut('slow');
		</script>";	
		exit();
	}
		
		$row_stock=fetchArray($chk_bank);
		
		$company = $row_stock['company'];
		$stock_left = $row_stock['stock_left'];
		$bilty_id = $row_stock['id'];
		$bilty_branch = $row_stock['branch'];
		
		if($numRows_bilty>1)
		{
			echo "<script type='text/javascript'>
				alert('Error: Duplicate Series found. Bilty Id $bilty_id. Please contact head-office.');
				$('#lrno').val('');
				$('#lrno').focus();
				
				$('#LRallInputs *').prop('disabled', true);
				// $('#LRallInputs').hide();
				$('#lrno').attr('readonly',false);
				$('#lrchk_button').attr('disabled',false);
				$('#lr_sub').attr('disabled',true);
				$('#lr_sub').hide();
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
		
		if($stock_left!='' AND $stock_left<=0)
		{
			echo "<script type='text/javascript'>
				alert('Bilty Book not in stock. Please contact head-office.');
				$('#lrno').val('');
				$('#lrno').focus();
				$('#bilty_id').val('ZERO');
				$('#bilty_belongs_to').val('ZERO');
				
				$('#LRallInputs *').prop('disabled', true);
				// $('#LRallInputs').hide();
				$('#lrno').attr('readonly',false);
				$('#lrchk_button').attr('disabled',false);
				$('#lr_sub').attr('disabled',true);
				$('#lr_sub').hide();
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
			
		if($bilty_branch!=$branch)
		{
			echo "<script type='text/javascript'>
				alert('Not your LR. LR Book belongs to $bilty_branch Branch !');
				$('#lrno').val('');
				$('#lrno').focus();
				$('#bilty_id').val('ZERO');
				$('#bilty_belongs_to').val('ZERO');
				
				$('#LRallInputs *').prop('disabled', true);
				// $('#LRallInputs').hide();
				$('#lrno').attr('readonly',false);
				$('#lrchk_button').attr('disabled',false);
				$('#lr_sub').attr('disabled',true);
				$('#lr_sub').hide();
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
			
		echo "
		<script type='text/javascript'>
			$('#bilty_id').val('$bilty_id');
			$('#bilty_belongs_to').val('$row_stock[lr_belongs_to]');
			$('#auto_company_div').show();
			$('#company_auto').val('$company');
			$('#company_auto').attr('required',true);
			
			$('#man_company_div').hide();
			$('#company_man').val('');
			$('#company_man').attr('required',false);
			
				$('#LRallInputs *').prop('disabled', false);
				// $('#LRallInputs').show();
				$('#lrno').attr('readonly',true);
				$('#lrchk_button').attr('disabled',true);
				$('#lr_sub').attr('disabled',false);
				$('#lr_sub').show();
				$('#loadicon').fadeOut('slow');
		</script>";	
		
		$company_var=$company;
		
	if($row_stock['lr_belongs_to']=='HINDALCO')
	{
		$lrno = "RRPL".$lrno;
	}
}

if($con1_id=='848')
{
	$lrno = "AOR".$lrno;
}

$get_consignor_data = Qry($conn,"SELECT name,gst FROM consignor WHERE id='$con1_id'");

if(!$get_consignor_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_con1 = fetchArray($get_consignor_data);
	
// echo "
	// <script type='text/javascript'>
		// $('#consignor_sub_id').val('$con1_id');
		// $('#consignor_sub').val('$row_con1[name]');
		// $('#consignor_sub_gst').val('$row_con1[gst]');
	// </script>";	
	
$qry = Qry($conn,"SELECT lrno FROM lr_check WHERE lrno='$lrno'");
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
	echo "
	<script type='text/javascript'>
		alert('Duplicate LR Number: $lrno entered.');
		$('#lrno').val('');
		$('#lrno').focus();
		$('#LRallInputs *').prop('disabled', true);
		// $('#LRallInputs').hide();
		
		$('#lrno').attr('readonly',false);
		$('#lrchk_button').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";	
	exit();
}
	
$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE (consignor='$con1_id' || lrno='$lrno') AND status='1'");
if(!$chkEwayBillFree){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

// echo "SELECT id FROM _eway_bill_free WHERE branch='$branch' || consignor='$con1_id' || lrno='$lrno'";

// if(numRows($chkEwayBillFree)>0 || $company_var=='RAMAN_ROADWAYS')
if(numRows($chkEwayBillFree)>0)
{
	echo "<script>
		$('#EwayBillFlag').val('2');
		$('#EwayBillFlag_desc').val('Admin Exempt');
		$('#eway_bill_no').attr('readonly',true);
		$('#eway_bill_no').attr('onblur','');
	</script>";
}
else
{
	echo "<script>
		$('#EwayBillFlag').val('');
		$('#EwayBillFlag_desc').val('');
		$('#eway_bill_no').attr('readonly',false);
		$('#eway_bill_no').attr('onblur','FetchEwayBill(this.value)');
	</script>";
}

if($branch=='KORBA')
{
	echo "<script>
		$('#EwayBillFlag').val('2');
		$('#EwayBillFlag_desc').val('Admin Exempt');
		$('#eway_bill_no').attr('readonly',true);
		$('#eway_bill_no').attr('onblur','');
	</script>";
}

if($billing_type=='BILL_TO_SHIP_TO')
{
	echo "<script>
		$('#bill_to_party_div').show();
		$('#bill_to_party').attr('required',true);
		$('#bill_2').attr('disabled',true);
		$('#bill_to_party').val('');
		$('#bill_to_party_id').val('');
		$('#ship_to').show();
	</script>";
}
else
{
	echo "<script>
		$('#bill_1').attr('disabled',true);
		$('#bill_to_party_div').hide();
		$('#bill_to_party').attr('required',false);
		$('#bill_to_party').val('');
		$('#bill_to_party_id').val('');
		$('#ship_to').hide();
	</script>";
}

if($con1_id=='56')
{
	$get_loading_points = Qry($conn,"SELECT p.id,p.label,p.from_id,s.name 
	FROM address_book_consignor AS p 
	LEFT OUTER JOIN station AS s ON s.id = p.from_id 
	WHERE p.consignor='$consignor_sub_id'");
}
else
{
	$get_loading_points = Qry($conn,"SELECT p.id,p.label,p.from_id,s.name 
	FROM address_book_consignor AS p 
	LEFT OUTER JOIN station AS s ON s.id = p.from_id 
	WHERE p.consignor='$con1_id'");
}

if(!$get_loading_points){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

// echo "<script>
	// alert('$shipment_party_sel - $shipment_party_id - $con2_id');
// </script>";


if($shipment_party_sel=='DIFF')
{
	$get_unloading_points = Qry($conn,"SELECT p.id,p.label,p.to_id,s.name 
	FROM address_book_consignee AS p 
	LEFT OUTER JOIN station AS s ON s.id = p.to_id 
	WHERE p.consignee='$shipment_party_id'");
}
else
{
	$get_unloading_points = Qry($conn,"SELECT p.id,p.label,p.to_id,s.name 
	FROM address_book_consignee AS p 
	LEFT OUTER JOIN station AS s ON s.id = p.to_id 
	WHERE p.consignee='$con2_id'");
}

if(!$get_unloading_points){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
	$('.del_party_sel_opt').attr('disabled',true);
	$('#shipment_party').attr('readonly',true);
</script>";

if($shipment_party_sel=='SAME')
{
	echo "<script>$('#del_party_SAME').attr('disabled',false);</script>";
}
else
{
	echo "<script>$('#del_party_DIFF').attr('disabled',false);</script>";
}
?>
<div class="form-group col-md-3">
	<label>Loading Point <font color="red">*</font></label>
	<select onchange="LoadingPointFunc(this.value)" name="loading_point" id="loading_point" class="form-control" required="required">
		<option value="">--select loading point--</option>
	<?php
	if(numRows($get_loading_points) > 0)
	{
		while($row_lp = fetchArray($get_loading_points))
		{
			echo "<option value='$row_lp[id]_$row_lp[from_id]_$row_lp[name]'>$row_lp[name] - $row_lp[label]</option>";
		}
	}	
	?>
	</select>
</div>


<div class="form-group col-md-3">
	<label>Unloading Point <font color="red">*</font></label>
	<select onchange="UnloadingPointFunc(this.value)" name="unloading_point" id="unloading_point" class="form-control" required="required">
		<option value="">--select unloading point--</option>
	<?php
	if(numRows($get_unloading_points) > 0)
	{
		while($row_ulp = fetchArray($get_unloading_points))
		{
			echo "<option value='$row_ulp[id]_$row_ulp[to_id]_$row_ulp[name]'>$row_ulp[name] - $row_ulp[label]</option>";
		}
	}	
	?>
	</select>
</div>