<?php
require_once('../connection.php');

$url1 = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
$url11 =substr($url1,0,-21);

$tno= escapeString($conn,$_POST['tno']);
$id= escapeString($conn,$_POST['id']);


$qry=Qry($conn,"SELECT name,pan,mo1,full,up1,rc_rear,up4,up6 FROM mk_truck WHERE id='$id'");
	
	if(numRows($qry)==0)
	{
		echo "
		<br />
		<span class='bg-danger' style='color:#000;padding:8px;'><center>No result found from Truck No. $tno.</center></span>";
		exit();
	}
	else
	{
		
	$row=fetchArray($qry);
	$pan=$row['pan'];
	$name=$row['name'];
	$mo=$row['mo1'];
	$addr=$row['full'];
	$rc_copy=$row['up1'];
	$rc_copy_rear=$row['rc_rear'];
	$pan_copy=$row['up4'];
	$dec_copy=$row['up6'];
	
	$rc_copy1 = substr($rc_copy, strpos($rc_copy, "truck_rc/") + 9);    
	$rc_copy_rear = substr($rc_copy_rear, strpos($rc_copy_rear, "truck_rc/") + 9);    
	
	$pan_copy1 = substr($pan_copy, strpos($pan_copy, "truck_pan/") + 10);    
	$dec_copy1 = substr($dec_copy, strpos($dec_copy, "truck_dec/") + 10);    
	
	echo "
	<div class='col-md-12 table-responsive' style='background:#FFF;overflow:auto'>
	<br />
	<table class='table table-bordered' style='font-size:13px;'>
	<tr>
	<th>Truck No</th>
	<th>Pan No</th>
	<th>Owner</th>
	<th>Mobile</th>
	<th>Address</th>
	<th>RC Front</th>
	<th>RC Rear</th>
	<th>PAN Copy</th>
	<th>DEC. Copy</th>
	</tr>
	<tr>
	<td>$tno</td>
	<td>$pan</td>
	<td>$name</td>
	<td>$mo</td>
	<td>$addr</td>
	";
	if($rc_copy1=='')
	{
	echo "
	<td>No Attachment</td>
	";
	}	
	else
	{
	echo "
	<td><a href='".$url11."truck_rc/".$rc_copy1."' target='_blank'>Click Here</a></td>";
	}
	
	if($rc_copy_rear=='')
	{
	echo "
	<td>No Attachment</td>
	";
	}	
	else
	{
	echo "
	<td><a href='".$url11."truck_rc/".$rc_copy_rear."' target='_blank'>Click Here</a></td>";
	}
	
	if($pan_copy1=='')
	{
	echo "
	<td>No Attachment</td>
	";
	}	
	else
	{
	echo "
	<td><a href='".$url11."truck_pan/".$pan_copy1."' target='_blank'>Click Here</a></td>";
	}
	if($dec_copy1=='')
	{
	echo "
	<td>No Attachment</td>";
	}	
	else
	{
	echo "
		<td><a href='".$url11."truck_dec/".$dec_copy1."' target='_blank'>Click Here</a></td>";
	}
	echo "
	</tr></table>
	</div>";
	}
?>