<?php 
require_once '_connect.php';
require_once("../_connect_all.php");
// include('../mail_lib/Classes/PHPExcel.php');

// $objPHPExcel = new PHPExcel();

$timestamp = date("Y-m-d_H:i:s");

$from_date = escp($conn,$_POST['from_date']);
$to_date = escp($conn,$_POST['to_date']);
$branch = escp($conn,$_POST['branch']);

if($branch=='ALL')
{
	$branch_var="";
}
else
{
	$branch_var="freight_form_lr.branch='$branch' and";
}

$email = escp($conn,$_POST['email']);
$email_id = escp($conn,$_POST['email_id']);

// ini_set('memory_limit', '-1');

$delete_ext_records = Qry($conn,"DELETE FROM fm_all");

if(!$delete_ext_records){
	echo mysqlerror($conn);
	exit();
}

$qry_copy = Qry($conn,"INSERT into fm_all(fm_no, company ,lr_date, branch, lrno,tno,wheeler, from_stn, to_stn,lr_dest,crossing,act_wt,
charge_weight,lr_charge_wt,freight, loading, gps,adv_claim,dsl_inc, other, tds, total_freight,adv_to, adv_date, adv_party, adv_party_pan,
total_adv, adv_cash, adv_cheque, adv_diesel, adv_rtgs, balance_amount, unloading_detention,detention,gps_rent,gps_device,
bal_tds, balance_other, claim, late_pod, total_balance,bal_to,bal_date, balance_party, balance_party_pan, bal_cash, bal_cheque,
bal_diesel, bal_rtgs, broker, owner, pod_date,pod_branch) SELECT freight_form_lr.frno,freight_form_lr.company ,
freight_form_lr.date,freight_form_lr.branch,GROUP_CONCAT(freight_form_lr.lrno SEPARATOR ','),freight_form_lr.truck_no,
mk_truck.wheeler,freight_form_lr.fstation,freight_form_lr.tstation,lr_sample.tstation,freight_form_lr.crossing,
ROUND(SUM(freight_form_lr.wt12),2),ROUND(SUM(freight_form_lr.weight),2),ROUND(SUM(lr_sample.weight),2),freight_form.actualf,
freight_form.newtds,freight_form.dsl_inc,freight_form.gps,freight_form.adv_claim,freight_form.newother,freight_form.tds,
freight_form.totalf,freight_form.ptob,freight_form.adv_date,freight_form.pto_adv_name,freight_form.adv_pan,freight_form.totaladv,
freight_form.cashadv,freight_form.chqadv,freight_form.disadv,freight_form.rtgsneftamt,freight_form.baladv,freight_form.unloadd,
freight_form.detention,freight_form.gps_rent,freight_form.gps_device_charge,freight_form.bal_tds,freight_form.otherfr,
freight_form.claim,freight_form.late_pod,freight_form.totalbal,freight_form.paidto,freight_form.bal_date,
freight_form.pto_bal_name,freight_form.bal_pan,freight_form.paycash,freight_form.paycheq,freight_form.paydsl,
freight_form.newrtgsamt,mk_broker.name,mk_truck.name,(SELECT pod_date FROM rcv_pod where frno=freight_form.frno ORDER BY id 
DESC LIMIT 1),(SELECT branch FROM rcv_pod where frno=freight_form.frno ORDER by id DESC LIMIT 1)
FROM freight_form as freight_form
LEFT OUTER JOIN freight_form_lr as freight_form_lr ON freight_form_lr.frno=freight_form.frno
LEFT OUTER JOIN mk_truck as mk_truck ON mk_truck.id=freight_form.oid 
LEFT OUTER JOIN mk_broker as mk_broker ON mk_broker.id=freight_form.bid 
LEFT OUTER JOIN lr_sample as lr_sample ON lr_sample.lrno=freight_form_lr.lrno 
WHERE freight_form_lr.date BETWEEN '$from_date' AND '$to_date' and $branch_var freight_form_lr.frno LIKE '___F%' 
GROUP by freight_form.frno");

if(!$qry_copy){
	echo mysqlerror($conn);
	exit();
}

$update_rate  =  Qry($conn,"UPDATE fm_all SET hire_rate=freight/charge_weight");
if(!$update_rate){
	echo mysqlerror($conn);
	exit();
}

// $qry  =  Qry($conn,"SELECT * FROM fm_all");
// if(!$qry){
	// echo mysqlerror($conn);
	// exit();
// }
?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/extensions/Scroller/js/dataTables.scroller.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
</style>


<body style="background-color:#FFF;font-family: 'Open Sans', sans-serif !important">

<div class="container-fluid">
	
	<div class="row">
		
<div style="background-color:;padding-top:6px;padding-bottom:6px;" class="bg-primary form-group col-md-12">
	<div class="row">
		<div class="col-md-4">
			<a href="./freight_memo_download.php"><button class="btn btn-sm btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
		</div>
		<div class="col-md-4">
			<center><h5 style="">Freight - Memo</span></h5>
		</div>
	</div>	
</div>

	
<div class="form-group col-md-12 table-responsive" id="getPAGEDIV">
			<div class="card-body" style="min-height: 670px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
	<tr>
		<th>Id</th>  
		<th>Vou_No</th>  
        <th>Company</th>  
        <th>LR_Date <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Branch</th>  
        <th>LR_No <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Truck_No <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Wheeler</th>  
        <th>From_Loc <span class="glyphicon glyphicon-filter"></span></th>  
        <th>To_Loc <span class="glyphicon glyphicon-filter"></span></th>  
        <th>LR_dest</th>  
        <th>is_Crossing</th>  
        <th>Act.Wt</th>  
        <th>Chrg.Wt</th>  
        <th>LR_Chrg.Wt</th>  
        <th>Freight</th>  
        <th>Loading(+)</th>  
        <th>Dsl Inc(+)</th>  
        <th>GPS(-)</th>  
        <th>Adv Claim(-)</th>  
        <th>Others(-)</th>  
        <th>TDS(-)</th>  
        <th>Total_Freight</th>  
        <th>Adv.To</th>  
        <th>Adv.Date <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Adv.Party <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Adv.Party_Pan <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Total_Adv</th>  
        <th>Adv_Cash</th>  
        <th>Adv_Cheque</th>  
        <th>Adv_Diesel</th>  
        <th>Adv_Rtgs</th>  
        <th>Balance</th>  
        <th>Unloading(+)</th>  
        <th>Detention(+)</th>  
        <th>GPS_Rent(-)</th>  
        <th>GPS_Device_Charge(-)</th>  
        <th>Bal_TDS(-)</th>  
        <th>Bal_Other(-)</th>  
        <th>Bal_Claim(-)</th>  
        <th>Late_POD(-)</th>  
        <th>Total_Bal</th>  
        <th>Bal.To</th>  
        <th>Bal.Date <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Bal.Party <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Bal.Party_Pan <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Bal_Cash</th>  
        <th>Bal_Cheque</th>  
        <th>Bal_Diesel</th>  
        <th>Bal_Rtgs</th>  
        <th>Broker_Name <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Owner_Name <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Pod_Date <span class="glyphicon glyphicon-filter"></span></th>  
        <th>Pod_Rcvd_By <span class="glyphicon glyphicon-filter"></span></th>  
	</tr>
		      </thead> 
		 	</table>
		</div>
	</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function FetchLRs(){
$("#loadicon").show();
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 100, 500, -1], [50, 100, 500, "All"] ], 
		"bProcessing": true,
		"scroller": true,
        "scrollCollapse": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"responsive": true,
		"ordering": true,
		"buttons": [
			"copy", "excel", "print", "colvis",
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 ], 
        "serverSide": true,
		 "ajax": {
            "url": "_fetch_fm_server.php",
            "type": "POST"
        },
        "columns": [
           {"data": "0"},
			{"data": "1"},
			{"data": "2"},
			{"data": "3"},
			{"data": "4"},
			{"data": "5"},
			{"data": "6"},
			{"data": "7"},
			{"data": "8"},
			{"data": "9"},
			{"data": "52"},
			{"data": "53"},
			{"data": "10"},
			{"data": "11"},
			{"data": "12"},
			{"data": "13"},
			{"data": "14"},
			{"data": "15"},
			{"data": "16"},
			{"data": "17"},
			{"data": "18"},
			{"data": "19"},
			{"data": "20"},
			{"data": "21"},
			{"data": "22"},
			{"data": "23"},
			{"data": "24"},
			{"data": "25"},
			{"data": "26"},
			{"data": "27"},
			{"data": "28"},
			{"data": "29"},
			{"data": "30"},
			{"data": "31"},
			{"data": "32"},
			{"data": "33"},
			{"data": "34"},
			{"data": "35"},
			{"data": "36"},
			{"data": "37"},
			{"data": "38"},
			{"data": "39"},
			{"data": "40"},
			{"data": "41"},
			{"data": "42"},
			{"data": "43"},
			{"data": "44"},
			{"data": "45"},
			{"data": "46"},
			{"data": "47"},
			{"data": "48"},
			{"data": "49"},
			{"data": "50"},
			{"data": "51"},
		],
		"initComplete": function(settings, json) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
FetchLRs();
</script>