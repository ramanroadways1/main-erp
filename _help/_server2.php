<?php
require_once '_connect.php';
require_once("../_connect_all.php");

// ini_set('memory_limit', '-1');

$sql ="SELECT id,fm_no,company,lr_date,branch,CONCAT('\'',lrno) as lrno,tno,wheeler,from_stn,to_stn,lr_dest,crossing,act_wt,charge_weight,
lr_charge_wt,hire_rate,freight,loading,
dsl_inc,gps,adv_claim,other,tds,total_freight,adv_to,adv_date,adv_party,adv_party_pan,total_adv,adv_cash,adv_cheque,adv_diesel,
adv_rtgs,balance_amount,unloading_detention,detention,gps_rent,gps_device,bal_tds,balance_other,claim,late_pod,total_balance,
bal_to,bal_date,balance_party,balance_party_pan,bal_cash,bal_cheque,bal_diesel,bal_rtgs,broker,broker_pan,owner,owner_pan,driver,
driver_lic_no,pod_date,pod_branch FROM fm_all";
 
$table = "(
    ".$sql."
) temp";
  
  
$primaryKey = 'id';
 
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'fm_no', 'dt' => 1),
    array( 'db' => 'company', 'dt' => 2),
    array( 'db' => 'lr_date', 'dt' => 3), 
    array( 'db' => 'branch', 'dt' => 4), 
    array( 'db' => 'lrno', 'dt' => 5),  
    array( 'db' => 'tno', 'dt' => 6),
    array( 'db' => 'wheeler', 'dt' => 7), 
    array( 'db' => 'from_stn', 'dt' => 8), 
    array( 'db' => 'to_stn', 'dt' => 9), 
    array( 'db' => 'act_wt', 'dt' => 10), 
    array( 'db' => 'charge_weight', 'dt' => 11), 
    array( 'db' => 'lr_charge_wt', 'dt' => 12), 
    array( 'db' => 'freight', 'dt' => 13), 
    array( 'db' => 'loading', 'dt' => 14), 
    array( 'db' => 'dsl_inc', 'dt' => 15), 
    array( 'db' => 'gps', 'dt' => 16), 
    array( 'db' => 'adv_claim', 'dt' => 17), 
    array( 'db' => 'other', 'dt' => 18), 
    array( 'db' => 'tds', 'dt' => 19), 
    array( 'db' => 'total_freight', 'dt' => 20), 
    array( 'db' => 'adv_to', 'dt' => 21), 
    array( 'db' => 'adv_date', 'dt' => 22), 
    array( 'db' => 'adv_party', 'dt' => 23), 
    array( 'db' => 'adv_party_pan', 'dt' => 24), 
    array( 'db' => 'total_adv', 'dt' => 25), 
    array( 'db' => 'adv_cash', 'dt' => 26), 
    array( 'db' => 'adv_cheque', 'dt' => 27), 
    array( 'db' => 'adv_diesel', 'dt' => 28), 
    array( 'db' => 'adv_rtgs', 'dt' => 29), 
    array( 'db' => 'balance_amount', 'dt' => 30), 
    array( 'db' => 'unloading_detention', 'dt' => 31), 
    array( 'db' => 'detention', 'dt' => 32), 
    array( 'db' => 'gps_rent', 'dt' => 33), 
    array( 'db' => 'gps_device', 'dt' => 34), 
    array( 'db' => 'bal_tds', 'dt' => 35), 
    array( 'db' => 'balance_other', 'dt' => 36), 
    array( 'db' => 'claim', 'dt' => 37), 
    array( 'db' => 'late_pod', 'dt' => 38), 
    array( 'db' => 'total_balance', 'dt' => 39), 
    array( 'db' => 'bal_to', 'dt' => 40), 
    array( 'db' => 'bal_date', 'dt' => 41), 
    array( 'db' => 'balance_party', 'dt' => 42), 
    array( 'db' => 'balance_party_pan', 'dt' => 43), 
    array( 'db' => 'bal_cash', 'dt' => 44), 
    array( 'db' => 'bal_cheque', 'dt' => 45), 
    array( 'db' => 'bal_diesel', 'dt' => 46), 
    array( 'db' => 'bal_rtgs', 'dt' => 47), 
    array( 'db' => 'broker', 'dt' => 48), 
    array( 'db' => 'owner', 'dt' => 49), 
    array( 'db' => 'pod_date', 'dt' => 50), 
    array( 'db' => 'pod_branch', 'dt' => 51), 
    array( 'db' => 'lr_dest', 'dt' => 52), 
    array( 'db' => 'crossing', 'dt' => 53), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name_main,
    'host' => $server_name
);
 
require( '../b5aY6EZzK52NA8F/scripts/ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
);
?>