<?php
require_once('../connection.php');

$chq_no=escapeString($conn,$_POST['cheq_no']);

$branch=escapeString($conn,strtoupper($_SESSION['user']));

if($chq_no!='')
{
$qry=Qry($conn,"SELECT vou_no,vou_type,pay_type,vou_date,lrno,amount,date FROM cheque_book WHERE cheq_no='$chq_no' AND branch='$branch'");
$qry2=Qry($conn,"SELECT SUM(amount) as total_amount FROM cheque_book WHERE cheq_no='$chq_no' AND branch='$branch'");	

$row2=fetchArray($qry2);	

	if(numRows($qry)==0)
	{
		echo "<script>
			alert('No result found from Cheque No. $chq_no.');
			window.close();
		</script>";
		exit();
	}
	else
	{
		?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
<button type="button" onclick="window.close();" style="color:#FFF;margin-top:10px;margin-left:10px" class="btn btn-danger">Close</button>
		<?php
	echo "
	<div class='container' style='font-family:Verdana'>
	<div class='row'>
	<div class='col-md-12 table-responsive' style='background:#FFF;overflow:auto'>
	<center><h4>Cheque Summary : $chq_no</h4></center>
	<br />
	<table class='table table-bordered' style='font-family:Verdana;font-size:12px;'>
	<tr>
	<th>Id</th>
	<th>Vou No</th>
	<th>Vou Type</th>
	<th>Type</th>
	<th>Vou Date</th>
	<th>LR No</th>
	<th>Amount</th>
	<th>Date</th>
	</tr>";
$sn=1;
	
	while($row=fetchArray($qry))
	{
	echo "
	<tr>
		<td>$sn</td>
		<td>$row[vou_no]</td>
		<td>$row[vou_type]</td>
		<td>$row[pay_type]</td>
		<td>$row[vou_date]</td>
		<td>$row[lrno]</td>
		<td>$row[amount]</td>
		<td>$row[date]</td>
	</tr>
	";	
	$sn++;
	}
	echo "
	<tr>
		<td colspan='6'><b>Total Amount : </b></td>
		<td><b>$row2[total_amount] /-</b></td>
		<td></td>
	</tr>
	</table>
	</div>
	</div>
	</div>
";

}
}
?>
