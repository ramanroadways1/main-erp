<?php
require_once('../connection.php');

$oid=escapeString($conn,$_POST['oid']);
$tno=escapeString($conn,$_POST['tno']);
$branch=escapeString($conn,strtoupper($_SESSION['user']));
$date2=date("Y-m-d");

$from_date = escapeString($conn,strtoupper($_POST['from_date']));
$to_date = escapeString($conn,strtoupper($_POST['to_date']));

if($_POST['record_by']=='adv_date'){
	$date_col_name="f.adv_date";
}
else if($_POST['record_by']=='bal_date'){
	$date_col_name="f.bal_date";
}
else{
	$date_col_name="f.date";
}


$output='';

$qry=Qry($conn,"SELECT f.frno,f.newdate,f.truck_no,f.from1,f.to1,f.weight,f.company,f.actualf,f.newtds as load1,f.dsl_inc,
f.gps,f.adv_claim,f.newother,f.tds,f.totalf,f.totaladv,f.ptob,f.adv_date,f.pto_adv_name,f.adv_pan,f.cashadv,f.chqadv,f.chqno,
f.disadv,f.rtgsneftamt,f.narre,f.baladv,f.unloadd,f.detention,f.gps_rent,f.gps_device_charge,f.bal_tds,f.otherfr,f.claim,f.late_pod,
f.totalbal,f.paidto,f.bal_date,f.pto_bal_name,f.bal_pan,f.paycash,f.paycheq,f.paycheqno,f.paydsl,f.newrtgsamt,f.narra,
f.branch_bal,GROUP_CONCAT(l.lrno SEPARATOR ',') as lrno 
FROM freight_form AS f 
LEFT OUTER JOIN freight_form_lr AS l ON l.frno=f.frno 
WHERE $date_col_name between '$from_date' AND '$to_date' AND f.oid='$oid' AND f.branch='$branch' GROUP by f.frno ORDER BY f.id ASC");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('No result found');
		window.close();
	</script>";
	exit();
}
	
if(isset($_POST['download']))
{
	$output .= '
   <table border="1">  
       <tr>  
			<th>FM_No</th>
			<th>LR_No</th>
			<th>FM_Date</th>
			<th>Truck No</th>
			<th>From</th>
			<th>To</th>
			<th>Weight</th>
			<th>Company</th>
			<th>Rate</th>
			<th>Freight</th>
			<th>Load(+)</th>
			<th>Dsl_Inc(+)</th>
			<th>GPS(-)</th>
			<th>Claim(-)</th>
			<th>Other(-)</th>
			<th>TDS(-)</th>
			<th>Total_Freight</th>
			<th>Advance</th>
			<th>Advance_To</th>
			<th>Advance_Date</th>
			<th>Advance_Party</th>
			<th>Advance_Party_Pan</th>
			<th>Cash_Adv</th>
			<th>Cheque_Adv</th>
			<th>Cheque_No</th>
			<th>Diesel_Adv</th>
			<th>Rtgs_Adv</th>
			<th>Narration</th>
			<th>Balance</th>
			<th>Unloading(+)</th>
			<th>Detention(+)</th>
			<th>GPS_Rent(-)</th>
			<th>GPS_Device_Charge(-)</th>
			<th>Bal_Tds(-)</th>
			<th>Bal_Others(-)</th>
			<th>Bal_Claim(-)</th>
			<th>Late_Pod(-)</th>
			<th>Total_Balance</th>
			<th>Balance_To</th>
			<th>Balance_Date</th>
			<th>Balance_Party</th>
			<th>Balance_Pan</th> 
			<th>Cash_Bal</th> 
			<th>Cheque_Bal</th> 
			<th>Cheque_No2</th> 
			<th>Diesel_Bal</th> 
			<th>Rtgs_Bal</th> 
			<th>Narration2</th> 
			<th>Balance_Branch</th> 
	</tr>';
	
  while($row = fetchArray($qry))
  {
	   $rate = sprintf("%.2f",$row["actualf"]/$row["weight"]);
	   
   $output .= '
				<tr> 
							<td>'.$row["frno"].'</td> 
							<td>'."'".$row["lrno"].'</td> 
							<td>'.$row["newdate"].'</td> 
							<td>'.$row["truck_no"].'</td> 
							<td>'.$row["from1"].'</td>  
							<td>'.$row["to1"].'</td>  
							<td>'.$row["weight"].'</td>  
							<td>'.$row["company"].'</td>  
							<td>'.$rate.'</td>  
							<td>'.$row["actualf"].'</td>  
							<td>'.$row["load1"].'</td>  
							<td>'.$row["dsl_inc"].'</td>  
							<td>'.$row["gps"].'</td>  
							<td>'.$row["adv_claim"].'</td>  
							<td>'.$row["newother"].'</td>  
							<td>'.$row["tds"].'</td>  
							<td>'.$row["totalf"].'</td>  
							<td>'.$row["totaladv"].'</td>  
							<td>'.$row["ptob"].'</td>  
							<td>'.$row["adv_date"].'</td>  
							<td>'.$row["pto_adv_name"].'</td>  
							<td>'.$row["adv_pan"].'</td>  
							<td>'.$row["cashadv"].'</td>  
							<td>'.$row["chqadv"].'</td>  
							<td>'.$row["chqno"].'</td>  
							<td>'.$row["disadv"].'</td>  
							<td>'.$row["rtgsneftamt"].'</td>  
							<td>'.$row["narre"].'</td>  
							<td>'.$row["baladv"].'</td>  
							<td>'.$row["unloadd"].'</td>  
							<td>'.$row["detention"].'</td>  
							<td>'.$row["gps_rent"].'</td>  
							<td>'.$row["gps_device_charge"].'</td>  
							<td>'.$row["bal_tds"].'</td>  
							<td>'.$row["otherfr"].'</td>  
							<td>'.$row["claim"].'</td>  
							<td>'.$row["late_pod"].'</td>  
							<td>'.$row["totalbal"].'</td>  
							<td>'.$row["paidto"].'</td>  
							<td>'.$row["bal_date"].'</td>  
							<td>'.$row["pto_bal_name"].'</td>  
							<td>'.$row["bal_pan"].'</td>  
							<td>'.$row["paycash"].'</td>  
							<td>'.$row["paycheq"].'</td>  
							<td>'.$row["paycheqno"].'</td>  
							<td>'.$row["paydsl"].'</td>  
							<td>'.$row["newrtgsamt"].'</td>  
							<td>'.$row["narra"].'</td>  
							<td>'.$row["branch_bal"].'</td>  
				</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_by_TruckNo.xls');
  echo $output;
  
  echo "<script>
			window.close();
		</script>";
	exit();
}				
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
<button type="button" onclick="window.close();" style="color:#FFF;margin-top:10px;margin-left:10px" class="btn btn-danger">Close</button>
		<?php
	echo "
	<div class='container-fluid' style='font-family:Verdana'>
	<div class='row'>
	<div class='col-md-12 table-responsive' style='background:#FFF;overflow:auto'>
	<center><h4 style='background:#CCC;padding:8px;'>Freight-Memo Truck No : $tno</h4></center>
	<br />
	<table class='table table-bordered' style='font-family:Verdana;font-size:12px;'>
	<tr>
		<td colspan='7'><b>Total Calculation : </b></td>
		<td style='font-weight:bold' id='freight_span'></td>
		<td style='font-weight:bold' id='adv_span'></td>
		<td style='font-weight:bold' id='bal_span'></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
	<th>Id</th>
	<th>FM No</th>
	<th>FM Date</th>
	<th>Truck No</th>
	<th>LRNo</th>
	<th>From</th>
	<th>To</th>
	<th>Freight</th>
	<th>Advance</th>
	<th>Balance</th>
	<th>Advance Date</th>
	<th>Balance Date</th>
	</tr>";
$sn=1;
	
	$total_af=0;
	$total_adv=0;
	$total_bal=0;
	while($row=fetchArray($qry))
	{
		$total_af=$total_af+$row['actualf'];
		$total_adv=$total_adv+$row['totaladv'];
		$total_bal=$total_bal+$row['totalbal'];
	
	echo "
	<tr>
		<td>$sn</td>
		<td>
		<form action='../smemo2.php' target='_blank' method='POST'>
			<input type='hidden' name='key' value='SEARCH'>
			<input type='hidden' name='value1' value='$row[frno]'>
			<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$row[frno]</a>
		</form>
		</td>
		<td>$row[newdate]</td>
		<td>$row[truck_no]</td>
		<td>$row[lrno]</td>
		<td>$row[from1]</td>
		<td>$row[to1]</td>
		<td>$row[actualf]</td>
		<td>$row[totaladv]</td>
		<td>$row[totalbal]</td>
		<td>$row[adv_date]</td>
		<td>$row[bal_date]</td>
	</tr>
	";	
	$sn++;
	}
	echo "
	</table>
	</div>
	</div>
	</div>
";

echo "<script>
	$('#freight_span').html('$total_af');
	$('#adv_span').html('$total_adv');
	$('#bal_span').html('$total_bal');
</script>";

?>
