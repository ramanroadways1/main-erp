<?php
require_once 'connection.php';
$date = date("Y-m-d"); 
?>
<html>

<?php include("./_header.php"); ?>

<link href="css/styles.css" rel="stylesheet">

<?php 
	include("./_loadicon.php");
	include("./disable_right_click.php");
?>

<style>
label{
	color:#FFF;
}
</style>

<body style="background-color:#078388;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">
	
<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">	

<form autocomplete="off" action="_fetch_attendance.php" method="POST">	

<div class="row">
	
<div class="form-group col-md-3"></div>
	
<div class="form-group col-md-5">
	
	<div class="col-md-12">	
		<br />
		<br />
		<center><h4 style="color:#FFF">Attendance Sheet</h4></center>
		<br />
		<br />
	</div>

	<div class="form-group col-md-12">
		<label>Select Month <font color="red">*</font></label>
		<select class="form-control" name="month" required>
				<option value="">Select Month</option>
				<option value="1">JAN</option>
				<option value="2">FEB</option>
				<option value="3">MARCH</option>
				<option value="4">APRIL</option>
				<option value="5">MAY</option>
				<option value="6">JUNE</option>
				<option value="7">JULY</option>
				<option value="8">AUG</option>
				<option value="9">SEP</option>
				<option value="10">OCT</option>
				<option value="11">NOV</option>
				<option value="12">DEC</option>
		</select>
    </div>
	
	<div class="form-group col-md-12">
		<label>Select Year <font color="red">*</font></label>
		<select class="form-control" name="year" required>
		<option value="">Select Year</option>
		<?php
		$date_past = date("Y", strtotime('-5 year')); 
		$date_year = date("Y");
		
		for($i=$date_past;$i<=$date_year;$i++)
		{
			echo "<option value='$i'>$i</option>";
		}
		?>
		</select>
    </div>
	
	<div class="form-group col-md-12">	
		<input id="button_sub" type="submit" style="color:#000;letter-spacing:1px; font-weight:bold;" class="btn btn-warning" 
		name="submit" value="View Attendance" />
	</div>
</div>

</div>
</div>
</div>
</form>

</body>
</html>