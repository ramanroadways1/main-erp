<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$token = escapeString($conn,strtoupper($_POST['token']));

$GetReq = Qry($conn,"SELECT a.party_id,a.category,a.branch,a.maker,a.model,a.payment_mode,a.approval,a.ho_approval,a.amount,
a.gst_invoice,a.gst_type,a.gst_value,a.gst_amount,a.discount,a.payment_amount,c.title as cat_name,p.legal_name as party_name,p.gst_no,
p.ac_holder,p.ac_no,p.bank_name,p.ifsc_code 
FROM asset_request AS a
LEFT OUTER JOIN asset_party AS p ON p.id=party_id 
LEFT OUTER JOIN asset_category AS c ON c.id=category 
WHERE a.req_code='$token'");

if(!$GetReq){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./assets_view.php");
	exit();
}

if(numRows($GetReq)==0){
	echo "<script>
		alert('Invalid Token Number !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($GetReq);

if($row['branch']!=$branch){
	echo "<script>
		alert('Token Number not belongs to your branch !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['approval']==0 || $row['ho_approval']==0){
	
	if($row['approval']==0){
		$text="Manager";
	}
	else{
		$text="Head-Office";
	}
	
	echo "<script>
		alert('$text\'s Approval Pending !');
		$('#token_no_div').show();
		$('#token_button_div').show();
		$('#main_div').hide();
		$('#tokenButton').attr('disabled',false);
		$('#btn_asset_add').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

	echo "<script>
		$('#token_no_div').hide();
		$('#token_button_div').hide();
		$('#main_div').show();
		$('#tokenButton').attr('disabled',true);
		$('#btn_asset_add').attr('disabled',false);
		
		$('#d_asset_cat').val('$row[cat_name]');
		$('#d_asset_cat_id').val('$row[category]');
		$('#d_maker_name').val('$row[maker]');
		$('#d_model').val('$row[model]');
		$('#d_payment_mode').val('$row[payment_mode]');
		
		$('#amount').val('$row[amount]');
		$('#gst_selection').val('$row[gst_invoice]');
		$('#gst_type').val('$row[gst_type]');
		$('#gst_value').val('$row[gst_value]');
		$('#gst_amount').val('$row[gst_amount]');
		$('#discount_new_asset').val('$row[discount]'); 
		$('#total_amount').val('$row[payment_amount]');
		 
		// $('#amount').attr('max','$row[payment_amount]');
		// $('#total_amount').attr('max','$row[payment_amount]');
		// $('#total_amount').attr('min','$row[payment_amount]');
		// $('#total_amount').val('$row[payment_amount]');
	</script>";
	
// if($row['payment_mode']=='CHEQUE')
// {
	// echo "<script>
		// $('#paymentDiv').show();
		// $('#cheque_div').show();
		// $('#cheque_div :input').prop('required',true);
		// $('#rtgs_div2').hide();
		// $('#rtgs_div2 :input').prop('required',false);
	// </script>";
// }
// else if($row['payment_mode']=='NEFT')
// {
	// echo "<script>
		// $('#paymentDiv').show();
		// $('#cheque_div').hide();
		// $('#cheque_div :input').prop('required',false);
		// $('#rtgs_div2').show();
		// $('#rtgs_div2 :input').prop('required',true);
	// </script>";
// }
// else
// {
	// echo "<script>
		// $('#paymentDiv').hide();
		// $('#cheque_div').hide();
		// $('#cheque_div :input').prop('required',false);
		// $('#rtgs_div2').hide();
		// $('#rtgs_div2 :input').prop('required',false);
	// </script>";
// }

	echo "<script>
			$('#party_name').val('$row[party_name]');
			$('#partyId2').val('$row[party_id]');
			$('#loadicon').hide();
		</script>";
	exit();
?>