<?php
require_once("./connection.php");
 
$vou_id = escapeString($conn,strtoupper($_POST['vou_id']));

$delte_old_cache = Qry($conn,"DELETE FROM lr_pre WHERE timestamp < (NOW() - INTERVAL 30 MINUTE) AND frno!='$vou_id'");

$getLrs = Qry($conn,"SELECT id,oxygen_lr,company,truck_no,date,lrno,lr_type,lr_id,fstation,tstation,tstation_bak,consignor,consignee,wt12,weight,
crossing,cross_to,from_id,to_id,to_id_bak,dest_zone,dest_zone_id,to_id_lr,con1_id,brk_id FROM lr_pre WHERE frno='$vou_id'");

if(!$getLrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

// if(isset($_SESSION['voucher_olr_vehicle']))
// {
	// $tno_oxygn_search = $_SESSION['voucher_olr_vehicle'];
// }
// else 
// {
	// $tno_oxygn_search = "NA";
// }

$chk_oxygen_lr = Qry($conn,"SELECT id FROM lr_pre WHERE frno='$vou_id' AND oxygen_lr='1'");

if(!$chk_oxygen_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_oxygen_lr)>0)
{
	// if(numRows($getLrs)>1)
	// { 
		// errorLog("Multiple LRs found in Oxygen LR series. Frno: $vou_id.",$conn,$page_name,__LINE__);
		// Redirect("Error while processing Request","./");
		// exit();
	// }
	
	$oxy_lr="YES";
}
else
{
	$oxy_lr="NO";
}
?>
<div class="form-group col-md-12 table-responsive" style="border:0;overflow:auto">
<?php
if(isset($_SESSION['voucher_olr']) AND $oxy_lr=="NO")
{
?>
<div class="row">
	<div class="col-md-3 pull-right">
		<button onclick="ClearCache();" style="margin-left:4px;" type="button" class="btn btn-danger btn-sm pull-right">Clear Cache</button>
		<button type="button" style="margin-left:4px;" data-toggle="modal" data-target="#CrossingLRModal" class="btn btn-primary btn-sm pull-right">
			ADD Crossing LR
		</button>
		<?php
		$chk_break_branch = Qry($conn,"SELECT id FROM break_branch WHERE branch='$branch'");
		if(numRows($chk_break_branch)>0)
		{
		?>
		<button type="button" style="margin-left:4px;" data-toggle="modal" data-target="#BreakingLRModal" class="btn btn-warning btn-sm pull-right">ADD Breaking LR</button>
		<?php
		}
		?>
	</div>
</div>
<?php
}
?>

<style>
tr:hover{
	background:lightblue;
}
</style>

	<table class="table table-bordered" style="font-size:11.5px;background">
		<tr>
			<th>#</th>
			<th>LR<br>Date</th>
			<th>LR<br>Number</th>
			<th>LR Type</th>
			<th>From<br>Location</th>
			<th>To<br>Location</th>
			<th>Consignor & Consignee</th>
			<th>Actual<br>Weight</th>
			<th>Charge<br>Weight</th>
			<?php
			if($oxy_lr=='YES')
			{
				echo "<th colspan='2'>Update Destination</th>";
			}
			else
			{
				echo "<th>Crossing</th>
				<th>Cross Station</th>";
			}
			?>
			<th>Update Unloading<br>Location</th>
			<?php if(isset($_SESSION['voucher_olr_vehicle']) AND $_SESSION['voucher_olr_vehicle']=='MP14HA0973')
			{
			?>
			<th>#</th>
			<?php
			}
			?>
			<th>#</th>
		</tr>	
<?php

if(numRows($getLrs)==0){
	echo "<tr>
			<td colspan='15'><center><font color='red'>NO LR FOUND !!!</font></center></td>
		</tr>";
	$act_weight_db = 0;	
	$charge_weight_db = 0;	
	$company_db = "";	
}
else{
$sn=1;
	
	$act_weight_db = 0;	
	$charge_weight_db = 0;	
	
	while($row = fetchArray($getLrs))
	{
		$act_weight_db = $act_weight_db+$row['wt12'];	
		$charge_weight_db = $charge_weight_db+$row['weight'];	
		$company_db = $row['company'];	
	
		if($row['lr_type']==0){
			$crossing_lr = "Same Veh.";
		}else{
			$crossing_lr = "<font color='red'>Other Veh.</font>";
		}
	
		if($row['crossing']=='YES'){
			$checkbox_set = "checked";
		}else{
			$checkbox_set = "";
		}
		
		if($row['to_id']!=$row['to_id_lr']){
			$checkbox_dest = "checked";
		}else{
			$checkbox_dest = "";
		}
		
		// if($row['lr_type']==0 AND $row['brk_id']=='')
		// {
			// $delete_button="";
		// }
		// else
		// {
			$delete_button="<button onclick='DeleteLrFromFm($row[id])' type='button' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-trash'></span></button>";
		// }
		
		echo "<tr>
				<td>$sn</td>
				<td>".convertDate('d/m/y',$row['date'])."</td>
				<td>$row[lrno]</td>
				<td>$crossing_lr</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td>
				Consignor: $row[consignor]
				<br>
				Consignee: $row[consignee]
				</td>
				<td>$row[wt12]</td>
				<td>$row[weight]</td>";
				
				if($oxy_lr=="NO")
				{
					echo "<td><input type='checkbox' class='checkbox_crossing' $checkbox_set id='cross$row[id]' value='$row[id]' /></td>
					<td>$row[cross_to]</td>";
				}
				else
				{
					echo "<td colspan='2'><input class=checkbox_dest_update' type='checkbox' $checkbox_dest id='update_dest_$row[id]' value='$row[id]' /></td>";
				}	
				echo "				
				<input type='hidden' id='get_lrno$row[id]' value='$row[lrno]'>";
				
				if(($row['con1_id']=='56' || $row['con1_id']=='675') AND $row['lr_type']=="0")
				{
					echo "<td>
					<select style='font-size:10px;height:28px !important' onchange='DestUpdate($row[id])' class='form-control' id='dest_loc_change_$row[id]'>
						";
					?>	
						<option <?php if($row['to_id']==$row['to_id_bak']) { echo "selected"; } ?> style='font-size:12px' value="<?php echo $row["to_id_bak"]; ?>"><?php echo $row["tstation_bak"]; ?></option>
						<option <?php if($row['to_id']==$row['dest_zone_id']) { echo "selected"; } ?> style='font-size:12px' value="<?php echo $row["dest_zone_id"]; ?>"><?php echo $row["dest_zone"]; ?></option>
					<?php	
					echo "</select>
					</td>";
				}
				else
				{
					echo "<td></td>";
				}
				
			if($_SESSION['voucher_olr_vehicle']=='MP14HA0973')
			{
				echo "<td><button onclick=EditLR_Loc('$row[id]','$row[fstation]','$row[tstation]','$row[from_id]','$row[to_id]') type='button' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-pencil'></span></button></td>";
			}
				echo "<td>$delete_button</td>
			</tr>";
	$sn++;		
	}
}
?>
	<tr>
		<td colspan="7"><b>Total Calculation :</b></td>
		<td><?php echo sprintf("%.3f",$act_weight_db); ?></td>
		<td><?php echo sprintf("%.3f",$charge_weight_db); ?></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</div>

<div id="dest_update_res"></div>

<script>
function DestUpdate(id)
{
	var dest_id = $('#dest_loc_change_'+id).val();
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "./save_destination_new.php",
	data: 'id=' + id + '&dest_id=' + dest_id,
	type: "POST",
	success: function(data){
		$("#dest_update_res").html(data);
	},
	error: function() {}
	 });
}
</script>

<script>
<?php
if($oxy_lr=="NO")
{
?>	
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
			var myId = $(this).val();
			var lrno = $('#get_lrno'+myId).val();
			
            if($(this).is(":checked"))
			{
				$('#cross_lrno').html(lrno);
				$('#cross_lrno2').val(lrno);
				$('#checkboxId').val(myId);
				$('#open_crossing_lr').click();
            }
            else if($(this).is(":not(:checked)"))
			{
              $("#loadicon").show();
				jQuery.ajax({
					url: "./undo_crossing.php",
					data: 'lrno=' + lrno + '&id=' + myId + '&page_name=' + 'smemo_own.php',
					type: "POST",
					success: function(data){
						$("#get_lr_result").html(data);
					},
					error: function() {}
			  });
            }
        });
    });
<?php
}
else
{
?>	
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
			var myId_dest = $(this).val();
			var lrno_dest = $('#get_lrno'+myId_dest).val();
			
            if($(this).is(":checked"))
			{
				$('#dest_cache_lrno').html(lrno_dest);
				$('#dest_cache_lrno2').val(lrno_dest);
				$('#checkboxIdDestUpdate').val(myId_dest);
				$('#open_destination_modal_lr').click();
            }
            else if($(this).is(":not(:checked)"))
			{
				$("#loadicon").show();
				jQuery.ajax({
				url: "./undo_cache_destination.php",
				data: 'lrno=' + lrno_dest + '&id=' + myId_dest + '&page_name=' + 'smemo_own.php',
				type: "POST",
				success: function(data){
					$("#get_lr_result").html(data);
				},
				error: function() {}
				});
            }
        });
    });	
<?php
}
?>		
$(function() {
		$("#cross_location").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#cross_location').val(ui.item.value);   
           $('#cross_location_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#cross_location").val('');
			$("#cross_location").focus();
			$("#cross_location_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

$(function() {
		$("#dest_cache_location").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
           $('#dest_cache_location').val(ui.item.value);   
           $('#dest_cache_loc_id').val(ui.item.id);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Location does not exists.');
			$("#dest_cache_location").val('');
			$("#dest_cache_location").focus();
			$("#dest_cache_loc_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
});});

function UnCheck(id)
{
	$('#cross'+id).attr('checked', false);
}

function UnCheckDestCache(id)
{
	$('#update_dest_'+id).attr('checked', false);
}

function GetCrossingInfo()
{
	var lrno = $('#lrno_crossing_1').val();
	
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_crossing_info_by_lrno.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data){
				$("#get_lr_result").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		$('#crossing_lr_add_btn').atrr('disabled',true);
	}
}
</script>

<?php
if(numRows($getLrs)>0){
	echo "<div class='form-group col-md-12'>	
<button type='button' class='btn pull-right btn-success' id='save_button_olr' onclick=SaveOlr('$vou_id')>Save Own-Truck Form</button>
</div>";
}
HideLoadicon();
?>