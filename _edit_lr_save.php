<?php
require_once 'connection.php';
require($_SERVER['DOCUMENT_ROOT']."/graph/smtp/PHPMailerAutoload.php");

// $date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id= escapeString($conn,strtoupper($_POST['id']));
$lrno= escapeString($conn,strtoupper($_POST['lrno']));

$do_no= escapeString($conn,strtoupper(trim($_POST['do_no'])));
$invno= escapeString($conn,strtoupper(trim($_POST['invno'])));
$shipno= escapeString($conn,strtoupper(trim($_POST['shipno'])));
$po_no= escapeString($conn,strtoupper(trim($_POST['po_no'])));
$act_wt= escapeString($conn,strtoupper($_POST['act_wt']));
$gross_wt= escapeString($conn,strtoupper($_POST['gross_wt']));
$chrg_wt= escapeString($conn,strtoupper($_POST['chrg_wt']));

$bank= escapeString($conn,strtoupper(trim($_POST['bank'])));
$sold_to_party= escapeString($conn,strtoupper(trim($_POST['sold_to_party'])));
$lr_name= escapeString($conn,strtoupper(trim($_POST['lr_name'])));
$con2_addr= escapeString($conn,strtoupper(trim($_POST['con2_addr'])));

$articles= escapeString($conn,strtoupper(trim($_POST['articles'])));
$b_rate= escapeString($conn,strtoupper($_POST['b_rate']));
$b_amt= escapeString($conn,strtoupper($_POST['b_amt']));
$t_type= escapeString($conn,strtoupper($_POST['t_type']));
$goods_desc= escapeString($conn,strtoupper(trim($_POST['goods_desc'])));
$hsn_code= escapeString($conn,strtoupper(trim($_POST['hsn_code'])));

$getLRData = Qry($conn,"SELECT lr_id,lrno,branch,con2_addr,po_no,bank,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,
do_no,shipno,invno,articles,goods_desc,hsn_code,crossing,cancel,break,diesel_req,download,create_date 
FROM lr_sample_pending WHERE id='$id' AND crossing=''");

if(!$getLRData){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getLRData)==0)
{
	echo "<script>alert('LR not found.');window.location.href='./_fetch_lr_entry.php';</script>";
	exit();
}

$row = fetchArray($getLRData);

$lr_id = $row['lr_id'];

$datediff = strtotime($timestamp) - strtotime($row['create_date']);
$diff_value=round($datediff / (60 * 60 * 24));	

if($diff_value>3)
{
	echo "<script>alert('LR is 3 days older. You can\'t edit this LR.');window.location.href='./_fetch_lr_entry.php';</script>";
	exit();
}

if($row['lrno']!=$lrno)
{
	echo "<script>
		alert('LR not verified.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($row['branch']!=$branch)
{
	echo "<script>
		alert('LR belongs to $row[branch] Branch.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($row['crossing']!="")
{
	echo "<script>
		alert('You Can\'t edit this LR. LR has been Closed');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($row['break']>0)
{
	echo "<script>
		alert('This is Breaking LR. You Can\'t edit.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

if($row['diesel_req']>0)
{
	echo "<script>
		alert('Diesel Request From This LR. You Can\'t edit.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}

$update_log = array();
$update_Qry = array();

// if($tno!=$row['truck_no'])
// {
	// $update_log[]="Vehicle_No : $row[truck_no] to $tno";
	// $update_Qry[]="truck_no='$tno'";
	// $update_Qry[]="wheeler='$wheeler'";
// }

// if($lr_date!=$row['date'])
// {
	// $update_log[]="LR_Date : $row[date] to $lr_date";
	// $update_Qry[]="date='$lr_date'";
// }

// if($from!=$row['fstation'])
// {
	// $update_log[]="From_Loc : $row[fstation] to $from";
	// $update_Qry[]="fstation='$from'";
	// $update_Qry[]="from_id='$from_id'";
// }

// if($to!=$row['tstation'])
// {
	// $update_log[]="To_Loc : $row[tstation] to $to";
	// $update_Qry[]="tstation='$to'";
	// $update_Qry[]="to_id='$to_id'";
// }

// if($dest_zone!=$row['dest_zone'])
// {
	// $update_log[]="Dest_zone : $row[dest_zone] to $dest_zone";
	// $update_Qry[]="dest_zone='$dest_zone'";
	// $update_Qry[]="zone_id='$dest_zone_id'";
// }

// if($consignee_id!=$row['con2_id'])
// {
	// $update_log[]="Consignee : $row[consignee] to $consignee";
	// $update_Qry[]="consignee='$consignee'";
	// $update_Qry[]="con2_id='$consignee_id'";
	// $update_Qry[]="con2_gst='$con2_gst'";
// }

$table_data = '<html><body style="font-family:Verdana">';
$table_data .= '<span style="color:red;font-size:13px"><b>LR '.$lrno.'. Updated By Branch</b></span>';
$table_data .= '<br><br><table rules="all" border="1" cellpadding="10" style="font-family:Verdana;font-size:12px">';

$table_data .= "<tr>
	<td style='padding:3px'><strong>Heading</strong></td>
	<td style='padding:3px'><strong>Old Value</strong></td>
	<td style='padding:3px'><strong>New Value</strong></td>
</tr>";

if($do_no!=$row['do_no'])
{
	$update_log[]="do_no : $row[do_no] to $do_no";
	$update_Qry[]="do_no='$do_no'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>DO_Number</td>
		<td style='padding:3px'>$row[do_no]</td>
		<td style='padding:3px'>$do_no</td>
	</tr>";
}

if($invno!=$row['invno'])
{
	$update_log[]="invno : $row[invno] to $invno";
	$update_Qry[]="invno='$invno'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Invoice_Number</td>
		<td style='padding:3px'>$row[invno]</td>
		<td style='padding:3px'>$invno</td>
	</tr>";
}

if($shipno!=$row['shipno'])
{
	$update_log[]="shipno : $row[shipno] to $shipno";
	$update_Qry[]="shipno='$shipno'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Shipment_Number</td>
		<td style='padding:3px'>$row[shipno]</td>
		<td style='padding:3px'>$shipno</td>
	</tr>";
}

if($po_no!=$row['po_no'])
{
	$update_log[]="po_no : $row[po_no] to $po_no";
	$update_Qry[]="po_no='$po_no'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>PO_Number</td>
		<td style='padding:3px'>$row[po_no]</td>
		<td style='padding:3px'>$po_no</td>
	</tr>";
}

if($bank!=$row['bank'])
{
	$update_log[]="bank : $row[bank] to $bank";
	$update_Qry[]="bank='$bank'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Bank_Name</td>
		<td style='padding:3px'>$row[bank]</td>
		<td style='padding:3px'>$bank</td>
	</tr>";
}

if($sold_to_party!=$row['sold_to_pay'])
{
	$update_log[]="sold_to_party : $row[sold_to_pay] to $sold_to_party";
	$update_Qry[]="sold_to_pay='$sold_to_party'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Sold_To_Party</td>
		<td style='padding:3px'>$row[sold_to_pay]</td>
		<td style='padding:3px'>$sold_to_party</td>
	</tr>";
}

if($lr_name!=$row['lr_name'])
{
	$update_log[]="lr_name : $row[lr_name] to $lr_name";
	$update_Qry[]="lr_name='$lr_name'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>LR_Name</td>
		<td style='padding:3px'>$row[lr_name]</td>
		<td style='padding:3px'>$lr_name</td>
	</tr>";
}

if($con2_addr!=$row['con2_addr'])
{
	$update_log[]="consignee_addr : $row[con2_addr] to $con2_addr";
	$update_Qry[]="con2_addr='$con2_addr'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Consginee_Addr.</td>
		<td style='padding:3px'>$row[con2_addr]</td>
		<td style='padding:3px'>$con2_addr</td>
	</tr>";
}

if($act_wt!=$row['wt12'])
{
	$update_log[]="act_wt : $row[wt12] to $act_wt";
	$update_Qry[]="wt12='$act_wt'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Act_Wt.</td>
		<td style='padding:3px'>$row[wt12]</td>
		<td style='padding:3px'>$act_wt</td>
	</tr>";
}

if($gross_wt!=$row['gross_wt'])
{
	$update_log[]="gross_wt : $row[gross_wt] to $gross_wt";
	$update_Qry[]="gross_wt='$gross_wt'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Gross_Wt.</td>
		<td style='padding:3px'>$row[gross_wt]</td>
		<td style='padding:3px'>$gross_wt</td>
	</tr>";
}

if($chrg_wt!=$row['weight'])
{
	$update_log[]="charge_wt : $row[weight] to $chrg_wt";
	$update_Qry[]="weight='$chrg_wt'";
	$update_Qry[]="weight2='$chrg_wt'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Charge_Wt.</td>
		<td style='padding:3px'>$row[weight]</td>
		<td style='padding:3px'>$chrg_wt</td>
	</tr>";
}

// if($item!=$row['item'])
// {
	// $update_log[]="item : $row[item] to $item";
	// $update_Qry[]="item='$item'";
	// $update_Qry[]="item_id='$item_id'";
	
	// $table_data .= "<tr>
		// <td style='padding:3px'>$row[do_no]</td>
		// <td style='padding:3px'>$do_no</td>
	// </tr>";
// }

if($articles!=$row['articles'])
{
	$update_log[]="articles : $row[articles] to $articles";
	$update_Qry[]="articles='$articles'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Articles</td>
		<td style='padding:3px'>$row[articles]</td>
		<td style='padding:3px'>$articles</td>
	</tr>";
}

// if($goods_value!=$row['goods_value'])
// {
	// $update_log[]="goods_value : $row[goods_value] to $goods_value";
	// $update_Qry[]="goods_value='$goods_value'";
	
	// $table_data .= "<tr>
		// <td style='padding:3px'>$row[do_no]</td>
		// <td style='padding:3px'>$do_no</td>
	// </tr>";
// }

if($b_rate!=$row['bill_rate'])
{
	$update_log[]="billing_rate : $row[bill_rate] to $b_rate";
	$update_Qry[]="bill_rate='$b_rate'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Billing_Rate</td>
		<td style='padding:3px'>$row[bill_rate]</td>
		<td style='padding:3px'>$b_rate</td>
	</tr>";
}

if($b_amt!=$row['bill_amt'])
{
	$update_log[]="billing_amt : $row[bill_amt] to $b_amt";
	$update_Qry[]="bill_amt='$b_amt'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Billing_Amount</td>
		<td style='padding:3px'>$row[bill_amt]</td>
		<td style='padding:3px'>$b_amt</td>
	</tr>";
}

if($t_type!=$row['t_type'])
{
	$update_log[]="t_type : $row[t_type] to $t_type";
	$update_Qry[]="t_type='$t_type'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Truck_Type</td>
		<td style='padding:3px'>$row[t_type]</td>
		<td style='padding:3px'>$t_type</td>
	</tr>";
}

// if($freight_type!=$row['freight_type'])
// {
	// $update_log[]="freight_type : $row[freight_type] to $freight_type";
	// $update_Qry[]="freight_type='$freight_type'";
	
	// $table_data .= "<tr>
		// <td style='padding:3px'>$row[do_no]</td>
		// <td style='padding:3px'>$do_no</td>
	// </tr>";
// }

if($goods_desc!=$row['goods_desc'])
{
	$update_log[]="goods_desc : $row[goods_desc] to $goods_desc";
	$update_Qry[]="goods_desc='$goods_desc'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Goods_Description</td>
		<td style='padding:3px'>$row[goods_desc]</td>
		<td style='padding:3px'>$goods_desc</td>
	</tr>";
}

if($hsn_code!=$row['hsn_code'])
{
	$update_log[]="hsn_code : $row[hsn_code] to $hsn_code";
	$update_Qry[]="hsn_code='$hsn_code'";
	
	$table_data .= "<tr>
		<td style='padding:3px'>Hsn_Code</td>
		<td style='padding:3px'>$row[hsn_code]</td>
		<td style='padding:3px'>$hsn_code</td>
	</tr>";
}


$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#lr_update_edit').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$table_data .= "</table>";
$table_data .= "</body></html>";

StartCommit($conn);
$flag = true;

$update_lr = Qry($conn,"UPDATE lr_sample_pending SET $update_Qry WHERE id='$id'");

if(!$update_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_main = Qry($conn,"UPDATE lr_sample SET $update_Qry WHERE id='$lr_id'");

if(!$update_lr_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp,branch_user) VALUES 
('$lrno','$lr_id','LR_EDIT','$update_log','$branch','BRANCH','$timestamp','$branch_sub_user')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($row['download']=="1")
{
	$email = 'noreply@ramanroadways.com';                   
	$password = 'rrpl123#';
	$to_id = "sales@ramanroadways.com";
	$subject = 'LR Edit By Branch - '.$lrno.'';
	$mail = new PHPMailer;
    $mail->isSMTP();
	$mail->Host = 'smtp.rediffmailpro.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = $email;
	$mail->Password = $password;
	$mail->setFrom('noreply@ramanroadways.com', 'Raman Roadways');
	// $mail->addReplyTo('accounts@ramanroadways', 'Accounts RamanRoadways');
	$mail->addAddress($to_id);
	$mail->AddCC('system@ramanroadways.com', 'RRPL SYSTEM ADMIN');
	$mail->Subject = $subject;
	$mail->msgHTML($table_data);
	// $mail->AddAttachment("./_excel_autosave/Tyre_Exp.xlsx");
		
	if(!$mail->send())
	{
		$flag = false;
		$mail_error = "Mailer Error: " . $mail->ErrorInfo;
		errorLog($mail_error,$conn,$page_name,__LINE__);
	} 
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('LR Successfully Updated.');
		window.location.href='./_fetch_lr_entry.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>