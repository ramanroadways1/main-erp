<form id="FormAddVeh2" action="#" method="POST">
<div id="VehAddnewModal2" class="modal fade" style="" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add New Vehicle : ( पुराने व्हीकल को जोड़ने के लिए उपयोग करे।  )
      </div>
      <div class="modal-body">
        <div class="row">
		
				<div class="form-group col-md-4">
					<label>Registration Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="reg_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Registration Date <font color="red"><sup>* (as per RC)</sup></font></label>
					<input name="reg_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
				</div>
				
				<div class="form-group col-md-4">
					<label>Vehicle Type <font color="red"><sup>*</sup></font></label>
					<select class="form-control" required="required" name="veh_type">
						<option value="">--Select option--</option>
						<option value="TWO_WHEELER">Two Wheeler</option>
						<option value="FOUR_WHEELER">Four Wheeler</option>
					</select>
				</div>
				
				<div class="form-group col-md-4">
					<label>Fuel Type <font color="red"><sup>*</sup></font></label>
					<select name="fuel_type" class="form-control" required="required">
						<option value="">--option--</option>
						<option value="PETROL">PETROL</option>
						<option value="DIESEL">DIESEL</option>
					</select>
				</div>
				
				<div class="form-group col-md-4">
					<label>Vehicle Class <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="veh_class" oninput="this.value=this.value.replace(/[^a-z A-Z0-9()/-]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Vehicle Owner <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="rc_holder" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Maker Name <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="maker_name" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Model <font color="red"><sup>* (as per RC)</sup></font></label>
					<input type="text" name="model" oninput="this.value=this.value.replace(/[^a-z A-Z0-9/-]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Engine Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="engine_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-6">
					<label>Chasis Number <font color="red"><sup>*</sup></font></label>
					<input type="text" name="chasis_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required="required">
				</div>
				
				<div class="form-group col-md-4">
					<label>Vehicle For <font color="red"><sup>*</sup></font></label>
					<select name="veh_for" class="form-control" onchange="VehFor(this.value)" required="required">
						<option value="">--option--</option>
						<option value="GENERAL">General (for branch)</option>
						<option value="SPECIFIC">Specific (for person)</option>
					</select>
				</div>
				
				<script>
				function VehFor(elem)
				{
					if(elem=='SPECIFIC'){
						$('#spec_div').show();
						$('#emp_name_1').attr('required',true);
					}
					else{
						$('#spec_div').hide();
						$('#emp_name_1').attr('required',false);
					}
				}
				</script>
				
				<div class="form-group col-md-4" id="spec_div" style="display:none">
					<label>Select Employee <font color="red"><sup>*</sup></font></label>
					<select name="employee" class="form-control" id="emp_name_1" required="required">
						<option value="">--option--</option>
						<?php
						$getEmp = Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3'");
						if(numRows($getEmp)>0)
						{
							while($rowEmp = fetchArray($getEmp))
							{
								echo "<option value='$rowEmp[code]'>$rowEmp[name] ($rowEmp[code])</option>";
							}
						}
						?>
					</select>
				</div>
				
		</div>
      </div>
	  <div id="result_add_veh2"></div>
      <div class="modal-footer">
        <button type="submit" id="button_addNewVeh2" class="btn btn-sm btn-primary">Submit</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormAddVeh2").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button_addNewVeh2").attr("disabled", true);
	$.ajax({
        	url: "./save_asset_vehicle_add_new.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_add_veh2").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>