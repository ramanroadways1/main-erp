<?php
require_once("connection.php");
	
$timestamp = date("Y-m-d H:i:s"); 
$date_sys = date("Y-m-d"); 
$veh_no = escapeString($conn,($_POST['veh_no']));
$diesel_type = escapeString($conn,($_POST['diesel_type']));
$date = escapeString($conn,($_POST['date']));
$amount = escapeString($conn,$_POST['amount']);
$signature = $_POST['signature'];

if($signature=='')
{
	echo "<script type='text/javascript'>
		alert('Please upload signature.');
		$('#loadicon').hide();
		$('#button_sub').attr('disabled',false);
	</script>";
	exit();
}

$img1=$signature;
$img1=str_replace('data:image/png;base64,', '', $img1);
$img1=str_replace(' ', '+', $img1);
$data1=base64_decode($img1);

// echo "<script> 	
		// alert('hold !!');
		// $('#button_sub').attr('disabled',false);
		// $('#loadicon').hide();	
	// </script>";
	// exit();
	
if($diesel_type=='CARD')
{
	$card_str = explode("_",escapeString($conn,$_POST['card_no']));

	$card = $card_str[0];
	$type='CARD';
	$rate=0;
	$qty=0;
	$card_id = $card_str[2];
	$phy_card_no = $card_str[1];;
	$fuel_company = escapeString($conn,($_POST['card_company']));
	$trans_date = $date;
	$mobile_no="";	
	$bill_no="";	
	$narration="";	
}
else if($diesel_type=='OTP')
{
	$card_id="0";
	$bill_no="";
	$card = escapeString($conn,strtoupper($_POST['mobile_no']));
	$mobile_no = escapeString($conn,strtoupper($_POST['mobile_no']));
	$type="OTP";
	$rate=0;
	$qty=0;
	$phy_card_no = $mobile_no;
	$fuel_company = escapeString($conn,strtoupper($_POST['card_company']));
	$trans_date = $date;
	$narration = "";
}
else if($diesel_type=='PUMP')
{
	$pump_str = explode("_",escapeString($conn,$_POST['pump_list']));
	
	$mobile_no="";	
	$card_id="0";		
	$card = $pump_str[0];	
	$phy_card_no=$card;
	$fuel_company = $pump_str[1];
	$type='PUMP';
	$rate = escapeString($conn,$_POST['rate']);
	$qty = escapeString($conn,$_POST['qty']);
	$bill_no = escapeString($conn,strtoupper($_POST['bill_no']));
	$trans_date = $date;
	$narration = "";
}
else if($diesel_type=='CASH')
{
	$narration = escapeString($conn,$_POST['narration']);
	$bill_no = escapeString($conn,strtoupper($_POST['bill_no']));
	$fuel_company = escapeString($conn,strtoupper($_POST['cash_company']));
	$cash_debit_company = escapeString($conn,strtoupper($_POST['cash_debit_company']));
	$type='CASH';
	$rate = escapeString($conn,$_POST['rate']);
	$qty = escapeString($conn,$_POST['qty']);
	$trans_date = $date;
	$card="";
	$phy_card_no="";
}
else
{
	echo "<script> 	
		alert('Error !!');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}

if($diesel_type=='PUMP')
{
	if(!empty($_FILES["pump_slip"]["name"]))
	{
		$sourcePath = $_FILES['pump_slip']['tmp_name'];
		$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

		if(!in_array($_FILES['pump_slip']['type'], $valid_types))
		{
			echo "<script>
				alert('Error : Only image or pdf upload allowed.');
				$('#button_sub').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}

		$targetPath="diesel_voucher/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pump_slip']['name'],PATHINFO_EXTENSION);
		ImageUpload(600,600,$sourcePath);

		if(!move_uploaded_file($sourcePath,$targetPath))
		{
			echo "<script>
				alert('Error : Unable to upload file.');
				$('#button_sub').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$uploaded_file=$targetPath;
	}
	else
	{
		$uploaded_file="";
	}
}
else if($diesel_type=='CASH')
{
	$check_bal=Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");
	if(!$check_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash=$row_bal['balance'];
	$rr_cash=$row_bal['balance2'];
	$email=$row_bal['email'];
	
	if($cash_debit_company=='RRPL' && $rrpl_cash>=$amount)
	{
		$new_amount = $rrpl_cash-$amount;
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($cash_debit_company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
	{
		$new_amount = $rr_cash-$amount;
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Insufficient balance in company : $cash_debit_company.');
			$('#button_sub').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$sourcePath = $_FILES['pump_slip']['tmp_name'];
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

	if(!in_array($_FILES['pump_slip']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only image or pdf upload allowed.');
			$('#button_sub').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	$targetPath="diesel_voucher/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pump_slip']['name'],PATHINFO_EXTENSION);
	ImageUpload(600,600,$sourcePath);

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		echo "<script>
			alert('Error : Unable to upload file.');
			$('#button_sub').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$uploaded_file=$targetPath;
}
else
{
	$uploaded_file="";
}

$MainString = "DV".date("y");
$StrCount=strlen($MainString);

$get_vou_no = Qry($conn,"SELECT unq_id FROM diesel_voucher where unq_id like '$MainString%' ORDER BY id DESC LIMIT 1");

if(!$get_vou_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_vou_no)>0)
{
	$row_dv_no=fetchArray($get_vou_no);
	$ToBeSet2=substr($row_dv_no['unq_id'],$StrCount);
	$dv_no = $MainString.sprintf("%'.05d",++$ToBeSet2);
}
else
{
	$dv_no = $MainString.sprintf("%'.05d","1");
}
	
StartCommit($conn);
$flag = true;	

if($diesel_type!='CASH')
{
	$insert_diesel = Qry($conn,"INSERT INTO dairy.diesel(unq_id,tno,rate,qty,amount,date,narration,branch,branch_user,timestamp) VALUES 
	('$dv_no','$veh_no','$rate','$qty','$amount','$trans_date','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$branch_sub_user',
	'$timestamp')");

	if(!$insert_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	

	$insert_diesel_entry = Qry($conn,"INSERT INTO dairy.diesel_entry(unq_id,bill_no,tno,diesel,card_pump,card,veh_no,dsl_company,narration,
	branch,date,timestamp,dsl_mobileno,ho) VALUES ('$dv_no','$bill_no','$veh_no','$amount','$diesel_type','$card','$phy_card_no',
	'$fuel_company','$fuel_company-$phy_card_no Rs: $amount/-','$branch','$trans_date','$timestamp','$mobile_no','2')");

	if(!$insert_diesel_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$diesel_entry_id = getInsertID($conn);
}
else
{
	$user2=substr($branch,0,3)."E".date("dmY");  
       
	$ramanc=Qry($conn,"SELECT vno FROM mk_venf where vno like '$user2%' ORDER BY id DESC LIMIT 1");
	if(!$ramanc){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	$row2 = fetchArray($ramanc);
    
	if(numRows($ramanc) > 0){
		$count = $row2['vno'];
		$newstring = substr($count,12);
		$newstr = ++$newstring;
		$expid=$user2.$newstr;
	}
	else{
		$count2=1;
		$expid=$user2.$count2;
	}
	
	file_put_contents('./sign/exp_vou/cashier/'.$expid.'.png',$data1);
	$cash_sign="sign/exp_vou/cashier/$expid.png";
	
	$update_balance = Qry($conn,"update user set `$balance_col`='$new_amount' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$branch','$date_sys','$date','$cash_debit_company','$expid','Expense_Voucher','VEHICLE FUEL','$amount','$new_amount',
	'$timestamp')");

	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_exp_voucher = Qry($conn,"INSERT INTO mk_venf(user,branch_user,vno,newdate,date,comp,des,desid,amt,chq,narrat,vehno,cash_sign,
	timestamp,upload) VALUES ('$branch','$_SESSION[user_code]','$expid','$date','$date_sys','$cash_debit_company','VEHICLE FUEL','43',
	'$amount','CASH','$narration','$veh_no','$cash_sign','$timestamp','$uploaded_file')");

	if(!$insert_exp_voucher){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
		
	$diesel_entry_id = getInsertID($conn);
	
	$TodayData = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou+1,exp_vou_amount=exp_vou_amount+'$amount' WHERE branch='$branch'");

	if(!$TodayData){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}

$insert_voucher = Qry($conn,"INSERT INTO diesel_voucher(unq_id,diesel_entry_id,bill_no,veh_no,diesel_by,rate,qty,amount,date,narration,
fuel_company,card_pump_no,card_no2,upload,branch,branch_user,timestamp) VALUES ('$dv_no','$diesel_entry_id','$bill_no','$veh_no',
'$diesel_type','$rate','$qty','$amount','$trans_date','$narration','$fuel_company','$card','$phy_card_no','$uploaded_file','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Voucher created successfully !');
		window.location.href='./diesel_vou.php';
	</script>";
	exit();
}
else
{
	echo "<script>
		alert('Error !!');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>