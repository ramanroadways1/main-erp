<?php
require_once 'connection.php'; 

$sql ="SELECT id,lrno,lr_type,truck_no,DATE_FORMAT(date,'%d-%m-%y') as date,fstation,tstation,
IF(sub_consignor='',CONCAT(consignor,'<br>',consignee),CONCAT(consignor,'<br>','(',sub_consignor,')','<br>',consignee)) as party,
CONCAT(do_no,'<br>',invno,'<br>',shipno) as nos,wt12,weight,item FROM lr_sample_pending WHERE date(timestamp)>'2021-01-22' 
AND branch='$branch' ORDER BY date ASC";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'lrno', 'dt' => 1),
    array( 'db' => 'lr_type', 'dt' => 2),
    array( 'db' => 'truck_no', 'dt' => 3), 
    array( 'db' => 'date', 'dt' => 4), 
    array( 'db' => 'fstation', 'dt' => 5),  
    array( 'db' => 'tstation', 'dt' => 6),
    array( 'db' => 'party', 'dt' => 7), 
    array( 'db' => 'nos', 'dt' => 8), 
    array( 'db' => 'wt12', 'dt' => 9), 
    array( 'db' => 'weight', 'dt' => 10), 
    array( 'db' => 'item', 'dt' => 11), 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);