<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$from = escapeString($conn,strtoupper($_POST['from']));
$to = escapeString($conn,strtoupper($_POST['to']));
$from_id = escapeString($conn,strtoupper($_POST['from_id']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));
$page_name1 = escapeString($conn,($_POST['page_name1']));
$ewbNo = escapeString($conn,($_POST['ewb_no']));
// $salt_lr = escapeString($conn,($_POST['salt_lr']));
$con1_id = escapeString($conn,($_POST['con1_id']));
$item_id = escapeString($conn,($_POST['item_id']));
$actual_weight = escapeString($conn,($_POST['actual_weight']));
$charge_weight = escapeString($conn,($_POST['charge_weight']));
$ewb_desc = escapeString($conn,($_POST['crossing_modal_ewb_desc']));

if($page_name1=='smemo_own.php')
{
	$vou_no = $_SESSION['voucher_olr'];
	$veh_no = $_SESSION['voucher_olr_vehicle'];
}
else
{
	$vou_no = $_SESSION['freight_memo'];
	$veh_no = $_SESSION['freight_memo_vehicle'];
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$check_for_duplicate = Qry($conn,"SELECT id FROM lr_pre WHERE frno='$vou_no' AND lrno='$lrno'");
if(!$check_for_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_for_duplicate)>0){
	echo "<script>
		alert('You have already added this LR.');
		$('#crossing_lr_add_btn').attr('disabled',false);
	</script>";
	HideLoadicon();
	exit();
}

$check_lr = Qry($conn,"SELECT id,date as lr_date,fstation,consignor,consignee,tstation,company,to_id,crossing,item,date(timestamp) as date_created 
FROM lr_sample WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found.');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$row_check_lr = fetchArray($check_lr);

$lr_id = $row_check_lr['id'];

$lr_date = $row_check_lr['lr_date'];
$lr_from = $row_check_lr['fstation'];
$lr_to = $row_check_lr['tstation'];
$lr_con1 = $row_check_lr['consignor'];
$lr_con2 = $row_check_lr['consignee'];

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_check_lr['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

$chk_max_days_to_add = Qry($conn,"SELECT days_value FROM _max_days_for_lr_add WHERE branch='$branch'");
if(!$chk_max_days_to_add){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_max_days_to_add)>0)
{
	$row_max_days = fetchArray($chk_max_days_to_add);
	$max_days_to_add=$row_max_days['days_value'];
}
else
{
	$max_days_to_add=5;
}

$get_company = Qry($conn,"SELECT company FROM lr_pre WHERE frno='$vou_no'");

if(!$get_company){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_company)==0){
	$company = $row_check_lr['company'];
}
else
{
	$row_company = fetchArray($get_company);
	$company = $row_company['company'];
}

$chk_validity_allow = Qry($conn,"SELECT id FROM allow_lr_exceed_validity WHERE lrno='$lrno' AND branch='$branch' AND is_pending='1'");
if(!$chk_validity_allow){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($diff_days>$max_days_to_add AND numRows($chk_validity_allow)==0)
{
	echo "<script>
		alert('Error : LR exceed $max_days_to_add days. LR date is: $row_check_lr[lr_date]');
		$('#crossing_lr_add_btn').attr('disabled',false);
	</script>";
	HideLoadicon();
	exit();
}

// $chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE (item='$item_id' || consignor='$con1_id' || lrno='$lrno') AND status='1'");
$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE (consignor='$con1_id' || lrno='$lrno') AND status='1'");
if(!$chkEwayBillFree){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

if(numRows($chkEwayBillFree)>0)
{
	$check_eway_bill="0";
}
else
{
	$check_eway_bill="1";
}	

$by_pass_ewb="NO";
$by_pass_reason="";
$by_pass_id="";

// if($con1_id=='56' || $con1_id=='675' || $item_id=='101' || $con1_id=='158')
// {
	// $check_eway_bill="0";
// }

if($item_id=='101')
{
	$check_eway_bill="0";
}

if($ewbNo!="" AND strlen($ewbNo)!=12)
{
	echo "<script>
		alert('Please check eway bill number !');
		$('#crossing_lr_add_btn').attr('disabled',false);
	</script>";
	HideLoadicon();
	exit();
}

$check_by_pass=Qry($conn,"SELECT id,narration FROM _by_pass_ewb WHERE is_active='1'");
if(!$check_by_pass){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

if(numRows($check_by_pass)>0)
{
	$row_by_pass = fetchArray($check_by_pass);
	$by_pass_ewb="YES";
	$by_pass_reason=$row_by_pass['narration'];
	$by_pass_id=$row_by_pass['id'];
}

if($row_check_lr['crossing']!='YES'){
	echo "<script>
		alert('LR not crossing enabled.');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$get_info = Qry($conn,"SELECT tstation,to_id FROM freight_form_lr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");
if(!$get_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_info)==0){
	echo "<script>
		alert('Previous Records not found. LR Number : $lrno.');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$row_prev = fetchArray($get_info);

if($row_prev['tstation']!=$from){
	echo "<script>
		alert('Invalid Data Input Found !');
		window.location.href='./$page_name1';
	</script>";
	exit();
}

$validUpto="";
$billDate2="";
$extendedTimes="";
$ewb_doc_no="";
$ewb_doc_date="";
$ewb_from_pincode="";
$ewb_total_inv_value="";
$ewb_total_value="";
$ewb_to_pincode="";

if($check_eway_bill=="1" AND $by_pass_ewb=='NO')
{
// ----- Eway Bill Verification

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];
// echo "TKEN".$authToken."<br>";

if($company=='RRPL')
{
	$gst_no_company="24AAGCR0742P1Z5";
	$gst_username="RRPL@AHD_API_111";
	$gst_password="RamanRoadways12";
}
else
{
	$gst_no_company="24AAEHK7360R1ZO";
	$gst_username="RR@AHD_API_RR";
	$gst_password="Raman@API1990";
}

	$api_url = "https://einvapi.charteredinfo.com";
	$Curl_EwbFetch = curl_init();

	curl_setopt_array($Curl_EwbFetch, array(
	  CURLOPT_URL => "$api_url/v1.03/dec/ewayapi?action=GetEwayBill&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&authtoken=$authToken&ewbNo=$ewbNo",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	));

		$response_gen = curl_exec($Curl_EwbFetch);
		// echo $response_gen;
		$err = curl_error($Curl_EwbFetch);
		curl_close($Curl_EwbFetch);
		
		if($err)
		{
			$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
			('$company','cURL Error : $err','RRPL.ONLINE','$branch','$timestamp')");
			if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
			}
				
			echo "<script>
				alert('Error: While Fetching Eway-Bill.');
				$('#crossing_lr_add_btn').attr('disabled',false);
			</script>";
			HideLoadicon();
			exit();
		}
		
		$response_decoded = json_decode($response_gen, true);
		
		if(@$response_decoded['error'])
		{
			if($response_decoded['error']['error_cd']=="GSP102")
			{
				$curlToken = curl_init();
				curl_setopt_array($curlToken, array(
				  CURLOPT_URL => "$api_url/v1.03/dec/auth?action=ACCESSTOKEN&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&ewbpwd=$gst_password",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				));
				
				$response_Token = curl_exec($curlToken);
				$errToken = curl_error($curlToken);
				curl_close($curlToken);
				
				if($errToken)
				{
					$insert_error2 = Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
					('$company','cURL Error : $errToken','RRPL.ONLINE','$branch','$timestamp')");
					if(!$insert_error2){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while processing Request","../");
							exit();
					}
						
					echo "<script>
							alert('Error: While Fetching Eway-Bill Token.');
							$('#crossing_lr_add_btn').attr('disabled',false);
						</script>";
						HideLoadicon();
						exit();
				}
				
				// echo $response_Token;
				$response_decodedToken = json_decode($response_Token, true);
				$authToken=$response_decodedToken['authtoken'];
				
				$insert_token=Qry($conn,"INSERT INTO ship.api_token (token,company,user,branch_user,timestamp) VALUES 
				('$authToken','$company','$branch','$branch_sub_user','$timestamp')");
				if(!$insert_token){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
					echo "<script>
							alert('Token Expired and System Generated new Token. Please try again !');
							$('#crossing_lr_add_btn').attr('disabled',false);
						</script>";
					HideLoadicon();
					exit();
				
			}
			else if($response_decoded['error']['error_cd']=="325")
			{
				echo "<script>
						alert('Error: Invalid Eway-Bill Number : $ewbNo.');
						$('#crossing_lr_add_btn').attr('disabled',false);
					</script>";
					HideLoadicon();
					exit();
			}
			else
			{
				// echo $response_gen."<br>";
				$errorMsg = $response_decoded['error']['message'];
				$errorMsgCode = $response_decoded['error']['error_cd'];
				
				$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,msg,branch,timestamp) VALUES 
				('$company','$response_gen','RRPL.ONLINE','$errorMsg','$branch','$timestamp')");
				
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
				echo "<script>
						alert('Error Code : $errorMsgCode. Message : $errorMsg. EwbNo : $ewbNo');
						$('#crossing_lr_add_btn').attr('disabled',false);
					</script>";
					HideLoadicon();
					exit();
			}
		}
	
	$i_veh_no = 0;
	
	$extendedTimes = $response_decoded['extendedTimes'];
	$validUpto = $response_decoded['validUpto'];
	$ewb_doc_no = $response_decoded['docNo'];
	
	$ewb_doc_date = $response_decoded['docDate'];
	$ewb_from_pincode = $response_decoded['fromPincode'];
	$ewb_total_inv_value = $response_decoded['totInvValue'];
	$ewb_total_value = $response_decoded['totalValue'];
	$ewb_to_pincode = $response_decoded['toPincode'];

	$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$validUpto)));
	$billDate2 = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$response_decoded['ewayBillDate'])));
	
	foreach($response_decoded['VehiclListDetails'] as $Data1){
		if($i_veh_no == 0) {
			$Veh_no_Ewb=$Data1['vehicleNo'];
		}
	$i_veh_no++;
	}
		
	if($Veh_no_Ewb!=$veh_no)
	{
		echo "<script>
				alert('Error: Vehicle Number : $veh_no not matching with e-way Bill.');
				$('#crossing_lr_add_btn').attr('disabled',false);
			</script>";
		HideLoadicon();
		exit();
	}		
	
// --- Verification Ends
}

StartCommit($conn);
$flag = true; 

$insert_crossed_lr = Qry($conn,"INSERT INTO lr_pre(frno,company,branch,date,create_date,truck_no,lrno,lr_type,lr_id,fstation,tstation,
consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,crossing,timestamp) SELECT '$vou_no','$company','$branch',date,'$date',
'$veh_no',lrno,'1',id,'$from','$to',consignor,consignee,'$from_id','$to_id',con1_id,con2_id,'$actual_weight','$charge_weight','NO',
'$timestamp' FROM lr_sample WHERE id='$lr_id'");

if(!$insert_crossed_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_ewb_data = Qry($conn,"INSERT INTO lr_pre_ewb(frno,check_eway_bill,lrno,lr_id,truck_no,lr_date,from_loc,to_loc,consignor,consignee,branch,
ewb_no,ewb_flag_desc,by_pass,by_pass_id,by_pass_narration,ewb_copy,ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,
timestamp) VALUES ('$vou_no','$check_eway_bill','$lrno','$lr_id','$veh_no','$lr_date','$lr_from','$lr_to','$lr_con1','$lr_con2','$branch','$ewbNo',
'$ewb_desc','$by_pass_ewb','$by_pass_id','$by_pass_reason','','$billDate2','$billDate2','$validUpto','$extendedTimes','$ewb_doc_no','$ewb_doc_date','$ewb_from_pincode',
'$ewb_total_inv_value','$ewb_total_value','$ewb_to_pincode','$timestamp')");

if(!$insert_ewb_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag) 
{
	MySQLCommit($conn);
	closeConnection($conn);

	echo "<script>
		alert('LR Number : $lrno. Successfully added !');
		window.location.href='./$page_name1';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !!');
		window.location.href='./$page_name1';
	</script>";
	exit();
}
?>