<?php 
require_once './connection.php';

$date = date('Y-m-d');
$timestamp = date('Y-m-d H:i:s');

$vou_date =  escapeString($conn,strtoupper($_POST['vou_date']));        
$company =  escapeString($conn,strtoupper($_POST['company']));        
$exp_name =  escapeString($conn,strtoupper($_POST['exp_name']));        
$exp_id =  escapeString($conn,strtoupper($_POST['exp_head']));        
$emp_code =  escapeString($conn,strtoupper($_POST['emp_code']));   
$veh_no =  escapeString($conn,strtoupper($_POST['veh_no']));        
$payment_mode =  escapeString($conn,strtoupper($_POST['payment_mode']));        
$amount =  escapeString($conn,strtoupper($_POST['amount']));
$amount_word =  escapeString($conn,strtoupper($_POST['amount_word']));
$cheque_no =  escapeString($conn,strtoupper($_POST['cheque_no']));
$bank =  escapeString($conn,strtoupper($_POST['bank']));        
$pan =  trim(escapeString($conn,strtoupper($_POST['pan'])));        
$acname =  trim(escapeString($conn,strtoupper($_POST['acname'])));        
$acno =  trim(escapeString($conn,strtoupper($_POST['acno'])));        
$bank_name =  trim(escapeString($conn,strtoupper($_POST['bank_name'])));        
$ifsc =  trim(escapeString($conn,strtoupper($_POST['ifsc'])));        
$narration =  escapeString($conn,strtoupper($_POST['narration']));        
$signature =  escapeString($conn,($_POST['signature']));
$signature2 =  escapeString($conn,($_POST['signature2']));

if($signature2=='')
{
	echo "<script type='text/javascript'>
		alert('Please upload signature.');
		$('#loadicon').hide();
		$('#expbtn').attr('disabled',false);
	</script>";
	exit();
}

$img1=$signature;
$img1=str_replace('data:image/png;base64,', '', $img1);
$img1=str_replace(' ', '+', $img1);
$data1=base64_decode($img1);

$img2=$signature2;
$img2=str_replace('data:image/png;base64,', '', $img2);
$img2=str_replace(' ', '+', $img2);
$data2=base64_decode($img2);

if(count($_FILES['vou_file']['name'])==0)
{
   echo "<script>
		alert('No attachment found please try again !');
		$('#loadicon').hide();
		$('#expbtn').attr('disabled', false);
	</script>";
	exit();
}
	
		$user2=substr($branch,0,3)."E".date("dmY");  
        $ramanc=Qry($conn,"SELECT DISTINCT vno FROM mk_venf where vno like '$user2%' ORDER BY id DESC LIMIT 1");
		if(!$ramanc){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
			Redirect("Error while processing Request","./");
			exit();
		}
		
        $row2 = fetchArray($ramanc);
        if(numRows($ramanc) > 0)
		{
			$count = $row2['vno'];
			$newstring = substr($count,12);
			$newstr = ++$newstring;
			$expid=$user2.$newstr;
		}
		else
		{
			$count2=1;
			$expid=$user2.$count2;
		}

$chk_vou_no = Qry($conn,"SELECT id FROM mk_venf WHERE vno='$expid'");
if(!$chk_vou_no){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
			Redirect("Error while processing Request","./");
			exit();
		}
		
if(numRows($chk_vou_no)>0)
{
	echo "<script>
		alert('Duplicate Voucher No: $expid. Try again !');
		$('#loadicon').hide();
		$('#expbtn').attr('disabled',false);
	</script>";
	exit();
}

	$check_bal=Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$branch'");
	if(!$check_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$row_bal = fetchArray($check_bal);

	$rrpl_cash=$row_bal['balance'];
	$rr_cash=$row_bal['balance2'];
	$email=$row_bal['email'];

if($payment_mode=='CASH')
{
	if($company=='RRPL' && $rrpl_cash>=$amount)
	{
		$new_amount = $rrpl_cash-$amount;
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
	{
		$new_amount = $rr_cash-$amount;
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Insufficient balance in company : $company.');
			window.location.href='./fexp.php';
		</script>";
		exit();
	}
}
	
	
	if($company=='RRPL')
	{
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Invalid company : $company.');
			window.location.href='./fexp.php';
		</script>";
		exit();
	}
	
// echo "ALL OK";	 
// $debit_col;
// exit();
	
StartCommit($conn);
$flag = true;
$rtgs_flag = false;
	
if($payment_mode=='CASH')
{
	$update_balance = Qry($conn,"update user set `$balance_col`='$new_amount' where username='$branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$branch','$date','$vou_date','$company','$expid','Expense_Voucher','$exp_name','$amount','$new_amount','$timestamp')");

	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='CHEQUE')
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit_col`,timestamp) VALUES 
	('$branch','$expid','$date','$vou_date','$company','Expense_Voucher','$exp_name','$cheque_no','$amount','$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$insert_cheque_book=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) VALUES 
('$expid','Expense_Voucher','$vou_date','$amount','$cheque_no','$date','$branch','$timestamp')");

	if(!$insert_cheque_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='NEFT')
{
	$chk_neft = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$expid'");
	if(!$chk_neft){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	else
	{
		$get_Crn = GetCRN("RR-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	
if(numRows($chk_neft)==0)		
{
	$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,type,email,crn,timestamp) 
		VALUES ('$expid','$branch','$company','$amount','$amount','$acname','$acno','$bank_name','$ifsc','$pan','$date','$vou_date',
		'EXPENSE_VOU','$email','$crnnew','$timestamp')");			

	if(!$qry_rtgs){
		$flag = false;
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Duplicate Rtgs Voucher : $expid",$conn,$page_name,__LINE__);
}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode selected.",$conn,$page_name,__LINE__);
}

	
$check_ac = Qry($conn,"SELECT id FROM exp_ac WHERE pan='$pan'");
if(!$check_ac){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

if(numRows($check_ac)==0)
{
	$insert_ac = Qry($conn,"INSERT INTO exp_ac(name,acno,bank,ifsc,pan,branch,timestamp) VALUES ('$acname','$acno','$bank_name','$ifsc',
	'$pan','$branch','$timestamp')");
	
	if(!$insert_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}

file_put_contents('./sign/exp_vou/cashier/'.$expid.'.png',$data1);
file_put_contents('./sign/exp_vou/rcvr/'.$expid.'.png',$data2);
$cash_sign="sign/exp_vou/cashier/$expid.png";
$rcvr_sign="sign/exp_vou/rcvr/$expid.png";	
	
for($i=0; $i<count($_FILES['vou_file']['name']);$i++) 
{
    $sourcePath = $_FILES['vou_file']['tmp_name'][$i];

        if($sourcePath!="")
		{
			$fix_name=date('dmYHis').mt_rand().$i;
			$targetPath = "upload/".$fix_name.".".pathinfo($_FILES['vou_file']['name'][$i],PATHINFO_EXTENSION);
			ImageUpload(1000,1000,$_FILES['vou_file']['tmp_name'][$i]);
			
            if(move_uploaded_file($sourcePath, $targetPath))
			{
				$files[] = $targetPath;
            }
			else
			{
				$flag = false;
				errorLog("No attachment found. $expid",$conn,$page_name,__LINE__);
			}
		}
		else
		{
			$flag = false;
			errorLog("No attachment found. $expid",$conn,$page_name,__LINE__);
		}
}

$file_name=implode(',',$files);
	
$insert_voucher = Qry($conn,"INSERT INTO mk_venf(user,branch_user,vno,newdate,date,comp,des,desid,amt,amt_w,chq,chq_no,chq_bnk_n,neft_bank,
neft_acname,neft_acno,neft_ifsc,pan,narrat,empcode,vehno,cash_sign,rcvr_sign,timestamp,upload) VALUES ('$branch','$_SESSION[user_code]','$expid','$vou_date',
'$date','$company','$exp_name','$exp_id','$amount','$amount_word','$payment_mode','$cheque_no','$bank','$bank_name','$acname','$acno',
'$ifsc','$pan','$narration','$emp_code','$veh_no','$cash_sign','$rcvr_sign','$timestamp','$file_name')");


if(!$insert_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$TodayData = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou+1,exp_vou_amount=exp_vou_amount+'$amount' WHERE branch='$branch'");

if(!$TodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Voucher: $expid Created Successfully !!');
		window.location.href='./fexp.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#expbtn').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#expbtn').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
?>