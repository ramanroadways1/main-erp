<?php 
require_once './connection.php';

$date = date('Y-m-d');
$timestamp = date('Y-m-d H:i:s');

$tno =  escapeString($conn,strtoupper($_POST['truck_no']));        
$start_point =  escapeString($conn,strtoupper($_POST['start_point']));        
$end_point =  escapeString($conn,strtoupper($_POST['end_point']));        
$current_location =  escapeString($conn,strtoupper($_POST['current_location']));        

$pathoffile=$_FILES["file_name"]['tmp_name'];
$filename=$_FILES["file_name"]['name'];
$filetype=$_FILES["file_name"]['type'];

$valid_types = array("image/jpg", "image/jpeg", "image/png");
	
if(!in_array($filetype, $valid_types))
{
	echo "<script>
		alert('Warning : Only Image Upload Allowed. JPG or PNG');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
  
StartCommit($conn);
$flag = true;
	
$sourcePath = $_FILES['file_name']['tmp_name'];

$md5_string = md5($tno."9999");

$fix_name = $tno."_".mt_rand();

$targetPath="../veh_location/".$fix_name.".".pathinfo($_FILES['file_name']['name'],PATHINFO_EXTENSION);
$targetPath2="veh_location/".$fix_name.".".pathinfo($_FILES['file_name']['name'],PATHINFO_EXTENSION);

ImageUpload(800,800,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	$flag = false;
	errorLog("File not uploaded. Veh_No: $tno.",$conn,$page_name,__LINE__);
}

$chk_veh = Qry($conn,"SELECT id FROM _vehicle_live_location WHERE tno='$tno'");

if(!$chk_veh){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_veh)>0)
{
	$update_location = Qry($conn,"UPDATE _vehicle_live_location SET start_point='$start_point',end_point='$end_point',
	current_location='$current_location',img='$targetPath2',tno_unq='$md5_string',timestamp='$timestamp' WHERE tno='$tno'");

	if(!$update_location){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$insert_location = Qry($conn,"INSERT INTO _vehicle_live_location(tno,start_point,end_point,current_location,img,tno_unq,timestamp) 
	VALUES ('$tno','$start_point','$end_point','$current_location','$targetPath2','$md5_string','$timestamp')");

	if(!$insert_location){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Location Updated Successfully !!');
		window.location.href='./upload_veh_data.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#button_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>