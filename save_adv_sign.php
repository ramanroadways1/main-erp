<?php
require_once 'connection.php';

$sign = $_POST['tsign'];
$vou_no = escapeString($conn,strtoupper($_POST['idmemo']));

if($sign=='')
{
	echo '<script>
			alert("Please Save Signature First !");
		</script>';
	HideLoadicon();
	exit();
}

$img = $sign; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);

if(!file_put_contents('sign/fm/adv/'.$vou_no.'.png',$data))
{
	echo '<script>
			alert("Unable to Save Signature !");
		</script>';
	HideLoadicon();
	exit();
}

$adv_sign="sign/fm/adv/$vou_no.png";
	
$updateSign = Qry($conn,"UPDATE freight_form set sign_adv='$adv_sign' WHERE frno='$vou_no'");

if(!$updateSign){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	echo "<form action='smemo2.php' id='myFormSign' method='POST'>
		<input type='hidden' value='FM' name='key'>		
		<input type='hidden' value='$vou_no' name='idmemo'>		
	</form>";
	
	echo "<script type='text/javascript'>
			alert('Signature Saved Successfully !!'); 
			document.getElementById('myFormSign').submit();
		</script>";
	closeConnection($conn);
	exit();
?>