<?php
require_once("./connection.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
?>
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Vou_No</th>
					<th>Vehicle_No</th>
					<th>Owner_Name</th>
					<th>Broker_Name</th>
					<th>LR_Date</th>
					<th>Created_On</th>
					<th>LR_Number</th>
					<th>Rcv_POD</th>
				</tr>	
				
<?php
$get_pods = Qry($conn,"SELECT f1.id,f1.vou_id,f1.vou_no,f1.lr_date,f1.create_date,f1.lrno,b.name as broker_name,o.name as owner_name,o.tno 
FROM _pending_lr_list AS f1 
LEFT OUTER JOIN mk_broker AS b ON b.id=f1.bid 
LEFT OUTER JOIN mk_truck AS o ON o.id=f1.oid 
WHERE DATEDIFF('$date',f1.lr_date)>45 AND f1.branch='$branch' AND bid not in(SELECT party_id from _by_pass_pod_lock WHERE is_active='1') 
AND oid not in(SELECT party_id from _by_pass_pod_lock WHERE is_active='1')");

if(!$get_pods){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_pods)>0)
{
	$sn=1;
	
	while($row = fetchArray($get_pods))
	{
		echo "<tr>
			<td>$sn</td>
			<td>$row[vou_no]</td>
			<td>$row[tno]</td>
			<td>$row[owner_name]</td>
			<td>$row[broker_name]</td>
			<td>".date("d-M-Y",strtotime($row['lr_date']))."</td>
			<td>".date("d-M-Y",strtotime($row['create_date']))."</td>
			<td>$row[lrno]</td>
			<td>
			<input type='hidden' value='$row[lrno]' id='lrno_$row[id]'>
			<input type='hidden' value='$row[vou_no]' id='frno_$row[id]'>
			";
			echo "<button type='button' onclick=RcvPOD('$row[id]','$row[vou_id]') class='btn btn-xs btn-default'>Rcv POD</button>";
		echo "</td>
		<tr>";
		
		$sn++;	
	}
}
else
{
	echo "<tr><td colspan='14'>No records found.</td></tr>";
}
			?>			
			</table>

<script>
$("#loadicon").fadeOut(800);
</script>