<?php 
require_once 'connection.php';

$tno = escapeString($conn,strtoupper($_POST['tno_market'])); 
$lic_no = escapeString($conn,strtoupper($_POST['lic_no'])); 
$driver_mobile = escapeString($conn,strtoupper($_POST['dl_mobile_no'])); 

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if(strlen($driver_mobile) != 10)
{
	echo "<script>
			alert('Check Driver\'s Mobile Number !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}

$chk_request = Qry($conn,"SELECT id FROM vehicle_placement_req WHERE tno='$tno' AND date(timestamp)='$date'");

if(!$chk_request){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_request)>0)
{
	echo "<script>
			alert('You can not generate duplicate request in same day !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}
	
StartCommit($conn);
$flag = true;

$insert_req = Qry($conn,"INSERT INTO vehicle_placement_req(tno,dl_no,driver_mobile,branch,branch_user,timestamp) VALUES('$tno','$lic_no',
'$driver_mobile','$branch','$branch_sub_user','$timestamp')");

if(!$insert_req){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Created Successfully');
		window.location.href='./vehicle_placement_req.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Error While Processing Request !');
			$('#button_sub').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";	
	exit();
}	
?>