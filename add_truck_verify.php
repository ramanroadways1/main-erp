<?php
require_once './connection.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

// echo "<script>
			// alert('On hold');
			// $('#verify_market_vehicle_btn').attr('disabled',false);
			// $('verify_market_vehicle_btn').attr('disabled',true);
			// $('verify_market_vehicle_btn').hide();
			// $('#loadicon').fadeOut('slow');
		// </script>";
	// exit();
	
$tno_1 = trim(escapeString($conn,strtoupper($_POST['tno_1'])));
$tno_2 = trim(escapeString($conn,strtoupper($_POST['tno_2'])));
$tno_3 = trim(escapeString($conn,strtoupper($_POST['tno_3'])));

$tno_final = $tno_1.$tno_2.$tno_3;

$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = trim(escapeString($conn,strtoupper($_POST['mobile'])));
$mobile2 = trim(escapeString($conn,strtoupper($_POST['mobile2'])));
$pan_no = trim(escapeString($conn,strtoupper($_POST['pan_no'])));
$wheeler = trim(escapeString($conn,strtoupper($_POST['wheeler'])));
$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));

$chk_vehicle = Qry($conn,"SELECT id FROM mk_truck WHERE tno='$tno_final'");

if(!$chk_vehicle)
{
	echo "<script>
			alert('Error while processing request !');
			$('#verify_market_vehicle_btn').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if(numRows($chk_vehicle)>0)
{
	echo "<script>
			alert('Error : Duplicate vehicle number !');
			$('#verify_market_vehicle_btn').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if(strlen($pan_no) != 10)
{
	echo "<script>
			alert('Check PAN number !');
			$('#verify_market_vehicle_btn').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}
	
if(strlen($mobile)!=10)
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($mobile,'0')===0)
{
	echo "<script>
		alert('Error: Check mobile number.');
		$('verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strpos($mobile2,'0')===0 AND $mobile2!='')
{
	echo "<script>
		alert('Error: Check mobile number-2.');
		$('verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile2!='' AND strlen($mobile2)!=10)
{
	echo "<script>
		alert('Invalid Alternate Mobile No.');
		$('verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan_no))
{
	echo "<script>
			alert('Invalid PAN nummber !');
			$('#verify_market_vehicle_btn').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

$sourcePath = $_FILES['pan_copy']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['pan_copy']['type'], $valid_types))
{
	echo "<script>
		alert('PAN Error : Only Image Upload Allowed !');
		$('#verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// vehicle rc-front

$sourcePath_RcFront = $_FILES['rc_front']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['rc_front']['type'], $valid_types))
{
	echo "<script>
		alert('RC front Error : Only Image Upload Allowed.');
		$('#verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// vehicle rc-front

// vehicle rc-rear

$sourcePath_RcRear = $_FILES['rc_rear']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['rc_rear']['type'], $valid_types))
{
	echo "<script>
		alert('RC rear Error : Only Image Upload Allowed.');
		$('#verify_market_vehicle_btn').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// vehicle rc-rear

// vehicle declaration

$sourcePath_Declaration = $_FILES['dec_copy']['tmp_name'];

if($sourcePath_Declaration!='')
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

	if(!in_array($_FILES['dec_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed.');
			$('#verify_market_vehicle_btn').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
	$dec_time = $timestamp;
}
else
{
	$dec_time = "";
}

// vehicle declaration

$check_ocr_active = Qry($conn,"SELECT id FROM _ocr_active WHERE branch='$branch' AND active='1'");
		
if(!$check_ocr_active){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo '<script>
		alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}
	
if(numRows($check_ocr_active) >0 )
{	
	$ocr_pan = VerifyOCRPan($aadhar_api_url,$_FILES['pan_copy']['tmp_name'],$aadhar_api_token);

	if($ocr_pan[0]=="ERROR")
	{
		$error_code = $ocr_pan[1];
		$error_msg = $ocr_pan[2];
		$api_response = escapeString($conn,($ocr_pan[3]));
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
		('OCR_PAN','1','$api_response','$branch','$branch_sub_user','$timestamp')");
			
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		echo '<script>
			alert("OCR Error_Code '.$error_code.' : '.$error_msg.'");
			$("#verify_market_vehicle_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	else
	{
		$name_ocr = $ocr_pan[1];
		$name_ocr_confd = $ocr_pan[2];
		
		$pan_no_ocr = $ocr_pan[3];
		$pan_no_ocr_confd = $ocr_pan[4];
		
		$father_name_ocr = $ocr_pan[5];
		$father_name_ocr_confd = $ocr_pan[6];
		
		$dob_ocr = $ocr_pan[7];
		$dob_ocr_confd = $ocr_pan[8];
		
		$api_response = escapeString($conn,($ocr_pan[9]));
		
		$chk_ocr_record = Qry($conn,"SELECT id FROM _data_ocr_pan WHERE pan_no='$pan_no_ocr'");
		
		if(strlen($pan_no_ocr)!=10)
		{
			$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,res_narration,branch,branch_user,timestamp) VALUES 
			('OCR_PAN','1','$api_response','Pan length is invalid.','$branch','$branch_sub_user','$timestamp')");
				
			if(!$insert_ocr_api_call){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				echo '<script>
					alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
				</script>';
				exit();
			}
		
			echo '<script>
				alert("Pan number: '.$pan_no_ocr.' is not valid !");
				$("#verify_market_vehicle_btn").attr("disabled",false);
				$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		if(numRows($chk_ocr_record)==0)
		{
			$insert_ocr = Qry($conn,"INSERT INTO _data_ocr_pan(pan_no,pan_no_confd,name_ocr,name_ocr_confd,father_name,father_name_confd,dob_ocr,
			dob_ocr_confd,branch,branch_user,timestamp) VALUES ('$pan_no_ocr','$pan_no_ocr_confd','$name_ocr','$name_ocr_confd','$father_name_ocr',
			'$father_name_ocr_confd','$dob_ocr','$dob_ocr_confd','$branch','$branch_sub_user','$timestamp')");
			
			if(!$insert_ocr){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				echo '<script>
					alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
				</script>';
				exit();
			}
		}
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
		('OCR_PAN','$api_response','$branch','$branch_sub_user','$timestamp')");
				
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	}

	if($pan_no_ocr != $pan_no)
	{
		$insert_ocr_doc_no_mismatch = Qry($conn,"INSERT INTO _data_ocr_doc_no_mismatch(doc_type,api_res,user_input,branch,branch_user,timestamp) VALUES 
		('OCR_PAN','$pan_no_ocr','$pan_no','$branch','$branch_sub_user','$timestamp')");
				
		if(!$insert_ocr_doc_no_mismatch){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		// alert("Invalid PAN number ! Pan number should be '.$pan_no_ocr.' as per attached document.");
		
		echo '<script>
			alert("PAN number not matching with document !");
			$("#verify_market_vehicle_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}

	$verify_pan = VerifyPAN($pan_no,$aadhar_api_url,$aadhar_api_token);

	if($verify_pan[0]=="ERROR")
	{
		$error_code = $verify_pan[1];
		$error_msg = $verify_pan[2];
		$api_response = escapeString($conn,($verify_pan[3]));
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,is_error,response,branch,branch_user,timestamp) VALUES 
		('VERIFY_PAN','1','$api_response','$branch','$branch_sub_user','$timestamp')");
			
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
		
		echo '<script>
			alert("Verification Error_Code '.$error_code.' : '.$error_msg.'");
			$("#verify_market_vehicle_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
	else
	{
		$full_name_api = $verify_pan[1];
		$name_length = strlen($full_name_api);
		$api_response = escapeString($conn,$verify_pan[3]);
		
		$insert_ocr_api_call = Qry($conn,"INSERT INTO _data_verification_api_log(doc_type,response,branch,branch_user,timestamp) VALUES 
		('VERIFY_PAN','$api_response','$branch','$branch_sub_user','$timestamp')");
				
		if(!$insert_ocr_api_call){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			echo '<script>
				alert("Error while processing request !");$("#verify_market_vehicle_btn").attr("disabled",false);$("#loadicon").fadeOut("slow");
			</script>';
			exit();
		}
	}
}
else
{
	$full_name_api = $name;
}

$fix_name_pan = mt_rand().date('dmYHis');
$fix_name_rc_front = mt_rand().date('dmYHis');
$fix_name_rc_rear = mt_rand().date('dmYHis');
$fix_name_rc_decl = mt_rand().date('dmYHis');

$targetPathPan = "owner_doc_temp/".$fix_name_pan.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath_Pan1 = $fix_name_pan.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);

$targetPath_RC_Front = "owner_doc_temp/".$fix_name_rc_front.".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);
$targetPath_RC_Front1 = $fix_name_rc_front.".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);

$targetPath_RC_Rear = "owner_doc_temp/".$fix_name_rc_rear.".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);
$targetPath_RC_Rear1 = $fix_name_rc_rear.".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);

if($sourcePath_Declaration!='')
{
	$targetPath_Declaration = "owner_doc_temp/".$fix_name_rc_decl.".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);
	$targetPath_Declaration1 = $fix_name_rc_decl.".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);
}
else
{
	$targetPath_Declaration1 = "";
}

// ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPathPan))
{
	echo '<script>
		alert("Error while uploading document : PAN !");
		$("#verify_market_vehicle_btn").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

if(!move_uploaded_file($sourcePath_RcFront,$targetPath_RC_Front))
{
	echo '<script>
		alert("Error while uploading document : Rc Front !");
		$("#verify_market_vehicle_btn").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

if(!move_uploaded_file($sourcePath_RcRear,$targetPath_RC_Rear))
{
	echo '<script>
		alert("Error while uploading document : Rc Rear !");
		$("#verify_market_vehicle_btn").attr("disabled",false);
		$("#loadicon").fadeOut("slow");
	</script>';
	exit();
}

if($sourcePath_Declaration!='')
{
	if(!move_uploaded_file($sourcePath_Declaration,$targetPath_Declaration))
	{
		echo '<script>
			alert("Error while uploading document : Declaration !");
			$("#verify_market_vehicle_btn").attr("disabled",false);
			$("#loadicon").fadeOut("slow");
		</script>';
		exit();
	}
}

$_SESSION['add_truck_pan_filepath'] = $targetPath_Pan1;	
$_SESSION['add_truck_rc_front_filepath'] = $targetPath_RC_Front1;	
$_SESSION['add_truck_rc_rear_filepath'] = $targetPath_RC_Rear1;	
$_SESSION['add_truck_del_filepath'] = $targetPath_Declaration1;	
$_SESSION['add_truck_tno_no'] = $tno_final;
$_SESSION['add_truck_tno_wheeler'] = $wheeler;
$_SESSION['add_truck_pan_no'] = $pan_no;
$_SESSION['add_truck_pan_holder'] = $full_name_api;
$_SESSION['add_truck_dec_timestamp'] = $dec_time;
	
	echo "<script type='text/javascript'> 
		$('.tno_1_sel_opt').attr('disabled',true);
		$('.tno_2_sel_opt').attr('disabled',true);
		$('.market_vehicle_wheeler_sel').attr('disabled',true);
		$('#tno_3').attr('readonly',true);
		
		$('#market_veh_owner_name').attr('readonly',true);
		$('#market_tno_mobile').attr('readonly',true);
		$('#market_tno_mobile2').attr('readonly',true);
		$('#pan_no2').attr('readonly',true);
		$('#market_veh_addr').attr('readonly',true);
		
		$('#market_vehicle_pan_copy').attr('disabled',true);
		$('#market_vehicle_rc_copy_front').attr('disabled',true);
		$('#market_vehicle_rc_copy_rear').attr('disabled',true);
		$('#market_vehicle_dec_copy').attr('disabled',true);
		
		$('#verify_market_vehicle_btn').attr('disabled',true);
		$('#verify_market_vehicle_btn').hide();
		
		$('#add_owner_button').attr('disabled',false);
		$('#add_owner_button').show();
		
		$('#pan_holder_name_vehicle').val('$full_name_api');
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();

?>