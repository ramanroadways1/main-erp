<?php
require_once 'connection.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$asset_cat = escapeString($conn,strtoupper($_POST['asset_cat']));
$maker_name = trim(escapeString($conn,strtoupper($_POST['maker_name'])));
$model = trim(escapeString($conn,strtoupper($_POST['model'])));
$gst_no = trim(escapeString($conn,strtoupper($_POST['gst_no'])));
$invoice_date = escapeString($conn,strtoupper($_POST['invoice_date']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$gst_selection = escapeString($conn,strtoupper($_POST['gst_selection']));
$gst_type = escapeString($conn,strtoupper($_POST['gst_type']));
$gst_value = escapeString($conn,strtoupper($_POST['gst_value']));
$gst_amount = escapeString($conn,strtoupper($_POST['gst_amount']));
$total_amount = escapeString($conn,strtoupper($_POST['total_amount']));
$asset_for = escapeString($conn,strtoupper($_POST['asset_for']));
$discount = escapeString($conn,($_POST['discount']));

	if($gst_amount<0)
	{
		echo "<script>
			alert('Error : Please Check GST Amount !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
if($discount<0)
	{
		echo "<script>
			alert('Error : Please Check Discount Amount !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
	if($amount<=0)
	{
		echo "<script>
			alert('Error : Please Check Amount !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
if($gst_selection=='YES'){
	
	if($gst_value<=0)
	{
		echo "<script>
			alert('Error : Please Check GST Value !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
	$gst_amountDb = sprintf("%0.2f",($amount*$gst_value/100));
}
else{
	$gst_amountDb = 0;
}

if($gst_amountDb!=$gst_amount)
{
	echo "<script>
			alert('Error : Please Check GST Amount !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if($total_amount != (($amount+$gst_amountDb)-$discount))
{
	echo "<script>
			alert('Error : Please Check Total Amount !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	exit();
}

if($asset_for=="SPECIFIC"){
	$employee = escapeString($conn,strtoupper($_POST['employee']));
}
else{
	$employee = $branch;
}

	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png","application/pdf");
	
	if(!in_array($_FILES['invoice_copy']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image and PDF uploads allowed.');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}

$getReqCode = GetOldAssetReqCode(substr($branch,0,3),$conn);

if(!$getReqCode || $getReqCode=="0" || $getReqCode==""){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
		
$reg_code = $getReqCode;

// echo "<script>
			// alert('$reg_code');
			// $('#btn_old_asset_add').attr('disabled', false);
			// $('#loadicon').fadeOut('slow');
		// </script>";
		// exit();

$sourcePath = $_FILES['invoice_copy']['tmp_name'];
$targetPath = "asset_upload/asset_invoice/".date('dmYHis').mt_rand().".".pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['invoice_copy']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,700,$sourcePath);
}

	if(!move_uploaded_file($sourcePath, $targetPath)) 
	{
		echo "<script>
			alert('Error : Unable to upload Invoice.');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
StartCommit($conn);
$flag = true;
	
$insert_Asset = Qry($conn,"INSERT INTO asset_main(req_code,category,asset_company,asset_model,invoice_date,typeof,holder,branch,
branch_user,invoice,gst_no,amount,gst_invoice,gst_type,gst_value,gst_amount,discount,total_amount,add_type,timestamp) VALUES ('$reg_code','$asset_cat',
'$maker_name','$model','$invoice_date','$asset_for','$employee','$branch','$branch_sub_user','$targetPath','$gst_no','$amount','$gst_selection',
'$gst_type','$gst_value','$gst_amount','$discount','$total_amount','2','$timestamp')");

if(!$insert_Asset){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
			alert('Asset Added Successfully !');
			window.location.href='assets_view.php';
		</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
			alert('Error While Processing Request !');
			$('#btn_old_asset_add').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
	// Redirect("","./assets_view.php");
	exit();
}
?>