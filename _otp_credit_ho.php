<?php
require_once './connection.php';

$company = escapeString($conn,strtoupper($_POST['company']));
$amount = escapeString($conn,strtoupper($_POST['amount']));
$username = escapeString($conn,strtoupper($_POST['name']));
$type = escapeString($conn,($_POST['type']));

$qry_fetch=Qry($conn,"SELECT mobile,name FROM ho_confirm WHERE username='$username'");

if(!$qry_fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
				
if(numRows($qry_fetch)>0)
{
	$row_fetch = fetchArray($qry_fetch);
	
	$mobile=$row_fetch['mobile'];
	// $mobile=9024281599;
	$msg_to=$row_fetch['name'];
}
else
{
	echo "<script>
		alert('NO approval authority found.');
		$('#cr_ho_button').attr('disabled',true);
		$('#confirm_sel').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}

if(strlen($mobile)!='10')
{
	echo "<script>
		alert('Invalid mobile number.');
		$('#confirm_sel').val('');
		$('#cr_ho_button').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$date = date('Y-m-d');

$otp = rand(100000,999999);

// $message="Branch: $branch wants to $type HO. Amount: $amount/- to $company. OTP is $otp.";

MsgHeadOfcOTP($mobile,$branch,$otp,$type,$amount,$company);

$_SESSION['session_otp'] = $otp;

		echo "<script>
			alert('OTP Successfully sent !');
			$('#confirm_sel').attr('disabled',true);
			$('#cr_ho_button').attr('disabled',false);
			$('#dr_ho_button').attr('disabled',false);
			$('#confirm_by_db').val('$username');
			$('#loadicon').hide();
		</script>";	

?>